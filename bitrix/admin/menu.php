<?php

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

$aMenu[] = array(
    'parent_menu' => 'global_menu_content',
    'sort'        => 400,
    'text'        => 'Каталог книг',
    'title'       => 'Каталог книг',
    'url'         => 'bookcatalog_index.php',
    'items_id'    => 'menu_bookcatalog',
    'items'       => array()
);

return $aMenu;

?>