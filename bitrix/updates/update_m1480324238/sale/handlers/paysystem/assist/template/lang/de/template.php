<?
$MESS["SALE_HANDLERS_PAY_SYSTEM_ASSIST_DESCRIPTION"] = "Dieser Service wird von <b>&laquo;Yandes.Kassa&raquo; angeboten</b>.<br /><br />Zu bezahlender Betrag: ";
$MESS["SALE_HANDLERS_PAY_SYSTEM_ASSIST_WARNING_RETURN"] = "<b>Bitte beachten:</b> werden Sie auf den Kauf verzichten, müssen Sie sich an den Onlineshop wenden, um Rückerstattung zu bekommen.";
$MESS["SALE_HANDLERS_PAY_SYSTEM_ASSIST_BUTTON_PAID"] = "Bezahlen";
$MESS["SALE_HANDLERS_PAY_SYSTEM_ASSIST_PROMT"] = "Sie möchten über <b>www.assist.ru</b> bezahlen.";
$MESS["SALE_HANDLERS_PAY_SYSTEM_ASSIST_ACCOUNT_NO"] = "Rechnung Nr.";
$MESS["SALE_HANDLERS_PAY_SYSTEM_ASSIST_ORDER_FROM"] = "von ";
$MESS["SALE_HANDLERS_PAY_SYSTEM_ASSIST_ORDER_SUM"] = "Rechnungsbetrag:";
$MESS["SALE_HANDLERS_PAY_SYSTEM_ASSIST_ACTION"] = "Bezahlen";
$MESS["SALE_HANDLERS_PAY_SYSTEM_ASSIST_NOTES_TITLE"] = "Bestellbearbeitung";
$MESS["SALE_HANDLERS_PAY_SYSTEM_ASSIST_NOTES"] = "Der Onlineshop behält sich vor, eine zusätzliche telefonische Autorisierung
Der Bezahlung durchzuführen. Alle Bestellung werden nicht automatisch bearbeitet: an Werktagen
von 10.00 bis 18.00 Uhr (Moskauer Zit).<br><br>Aufgrund der verstärkten Kontrollen über 
Bezahlungen werden Transaktionen via Visa und Eurocard/Mastercard nicht akzeptiert, wenn das Feld für CVV2/CVC2 (eine Kontrollzahl auf der Rückseite der Kreditkarte) nicht ausgefüllt ist.<br><br>Im System <a href=\"http://www.assist.ru/\">Assist</a> wird die Zahlungssicherheit durch den Einsatz des
SSL-Protokolls sichergestellt, wenn Kundeninformationen an den Server von <a href=\"http://www.assist.ru/\">Assist</a> weitergeleitet werden.<br><br>Die spätere
Weiterleitung der Informationen erfolgt über geschlossene bankinterne Kanäle, welche
Absolut sicher sind.<br><br>Die Verarbeitung der vertraulichen Kundeninformationen erfolgt
im Verarbeitungscenter von <a href=\"http://www.alfabank.ru/\">Alfa-Bank</a>. Auf diese Weise wird 
sichergestellt, dass niemand auf vertrauliche Kundendaten zugreifen kann.<br><br>
";
$MESS["SALE_HANDLERS_PAY_SYSTEM_ASSIST_NOTES_TITLE1"] = "Sicherheit der zu übermittelnden Informationen

";
$MESS["SALE_HANDLERS_PAY_SYSTEM_ASSIST_NOTES1"] = "Zum Schutz aller Kundeninformationen, welche übermittelt werden, wird
das Protokoll SSL 3.0 verwendet, das entsprechende Zertifikat wurde 
durch <a href=\"https://www.thawte.com\">Thawte</a> ausgestellte, ein Unternehmen, welches auf 
diesem Bereich spezialisiert ist.<br><br>Sie können hier <a href=\"https://sealinfo.thawte.com/
thawtesplash?form_file=fdf/thawtesplash.fdf&dn=WWW.ASSIST.RU&lang=en\">die Gültigkeit des Zertifikats von</a> Assist-Server überprüfen.
";
?>