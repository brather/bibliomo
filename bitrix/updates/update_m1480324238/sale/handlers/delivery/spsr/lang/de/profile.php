<?
$MESS["SALE_DLV_SRV_SPSR_PROFILE_TITLE"] = "SPSR EXPRESS Service";
$MESS["SALE_DLV_SRV_SPSR_PROFILE_MAIN_TITLE"] = "Einstellungen";
$MESS["SALE_DLV_SRV_SPSR_PROFILE_MAIN_DSCR"] = "Service-Einstellungen";
$MESS["SALE_DLV_SRV_SPSR_PROFILE_ST"] = "Servicetyp";
$MESS["SALE_DLV_SRV_SPSR_INNER_DESCR"] = "Interne Beschreibung";
$MESS["SALE_DLV_SRV_SPSR_ERROR_CONFIG_SRV_TYPE"] = "Konfigurationsfehler: es wurde kein Servicetyp ausgewählt";
$MESS["SALE_DLV_SRV_SPSR_NATURE"] = "Frachtguttyp";
$MESS["SALE_DLV_SRV_SPSR_NATURE_1"] = "Dokumente";
$MESS["SALE_DLV_SRV_SPSR_NATURE_2"] = "Konsumgüter (Ausrüstung)";
$MESS["SALE_DLV_SRV_SPSR_NATURE_17"] = "Ausrüstung und Unterhaltungselektronik ohne Brennstoff bzw. Batterien";
$MESS["SALE_DLV_SRV_SPSR_NATURE_18"] = "Schmuck";
$MESS["SALE_DLV_SRV_SPSR_NATURE_19"] = "Medikamente und Nahrungsergänzungsmittel";
$MESS["SALE_DLV_SRV_SPSR_NATURE_20"] = "Kosmetik und Parfüm";
$MESS["SALE_DLV_SRV_SPSR_NATURE_21"] = "Lebensmittel";
$MESS["SALE_DLV_SRV_SPSR_NATURE_22"] = "Ausrüstung und Unterhaltungselektronik ohne Brennstoff mit Batterien";
$MESS["SALE_DLV_SRV_SPSR_NATURE_23"] = "Gefahrgüter";
$MESS["SALE_DLV_SRV_SPSR_NATURE_24"] = "Konsumgüter (Charge, ohne Ausrüstung)";
$MESS["SALE_DLV_SRV_SPSR_AMOUNT_CHECK"] = "Versicherung oder Wert";
$MESS["SALE_DLV_SRV_SPSR_AMOUNT_CHECK_0"] = "Versicherung";
$MESS["SALE_DLV_SRV_SPSR_AMOUNT_CHECK_1"] = "Erklärter Wert";
?>