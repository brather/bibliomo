<?
$MESS["SALE_DLV_SRV_SPSR_PROFILE_TITLE"] = "SPSR EXPRESS Service";
$MESS["SALE_DLV_SRV_SPSR_PROFILE_MAIN_TITLE"] = "Settings";
$MESS["SALE_DLV_SRV_SPSR_PROFILE_MAIN_DSCR"] = "Service settings";
$MESS["SALE_DLV_SRV_SPSR_PROFILE_ST"] = "Service type";
$MESS["SALE_DLV_SRV_SPSR_INNER_DESCR"] = "Internal description";
$MESS["SALE_DLV_SRV_SPSR_ERROR_CONFIG_SRV_TYPE"] = "Configuration error: no service type selected";
$MESS["SALE_DLV_SRV_SPSR_NATURE"] = "Type of cargo";
$MESS["SALE_DLV_SRV_SPSR_NATURE_1"] = "Documents";
$MESS["SALE_DLV_SRV_SPSR_NATURE_2"] = "Consumer goods (ex. equipment)";
$MESS["SALE_DLV_SRV_SPSR_NATURE_17"] = "Consumer equipment and electronics w\\o fuel or batteries";
$MESS["SALE_DLV_SRV_SPSR_NATURE_18"] = "Jewelry";
$MESS["SALE_DLV_SRV_SPSR_NATURE_19"] = "Medicines and supplements";
$MESS["SALE_DLV_SRV_SPSR_NATURE_20"] = "Cosmetics and perfume";
$MESS["SALE_DLV_SRV_SPSR_NATURE_21"] = "Food";
$MESS["SALE_DLV_SRV_SPSR_NATURE_22"] = "Equipment and electronics w\\o fuel, with batteries";
$MESS["SALE_DLV_SRV_SPSR_NATURE_23"] = "Dangerous items";
$MESS["SALE_DLV_SRV_SPSR_NATURE_24"] = "Consumer goods (batch, ex. equipment)";
$MESS["SALE_DLV_SRV_SPSR_AMOUNT_CHECK"] = "Insurance or value";
$MESS["SALE_DLV_SRV_SPSR_AMOUNT_CHECK_0"] = "Insurance";
$MESS["SALE_DLV_SRV_SPSR_AMOUNT_CHECK_1"] = "Declared value";
?>