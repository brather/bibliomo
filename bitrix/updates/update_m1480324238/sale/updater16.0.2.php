<?
if(IsModuleInstalled('sale'))
{
	$updater->CopyFiles("install/admin", "admin");
	$updater->CopyFiles("install/components", "components");
	$updater->CopyFiles("install/js", "js");
	$updater->CopyFiles("install/tools", "tools");
	$updater->CopyFiles("install/themes", "themes");
}

if ($updater->CanUpdateDatabase() && $updater->TableExists('B_SALE_AUXILIARY'))
{
		if ($updater->TableExists('b_sale_bizval'))
		{
		if ($DB->Query('SELECT CODE_ID FROM b_sale_bizval WHERE 1=0', true))
		{
			if ($updater->TableExists('b_sale_bizval_old'))
			{
				$updater->Query(array(
						'MYSQL' => "drop table if exists b_sale_bizval_old;",
						'MSSQL' => "DROP TABLE B_SALE_BIZVAL_OLD",
						'ORACLE' => "DROP TABLE B_SALE_BIZVAL_OLD CASCADE CONSTRAINTS",
				));
			}

			if (!$updater->TableExists('b_sale_bizval_old'))
			{
			$updater->Query(array(
				'MYSQL' => "ALTER TABLE b_sale_bizval RENAME b_sale_bizval_old",
				'MSSQL' => "sp_rename B_SALE_BIZVAL, B_SALE_BIZVAL_OLD",
				'ORACLE' => "ALTER TABLE B_SALE_BIZVAL RENAME TO B_SALE_BIZVAL_OLD",
			));
			
			if ($DB->type == 'MSSQL')
				$DB->Query('ALTER TABLE B_SALE_BIZVAL DROP CONSTRAINT PK_B_SALE_BIZVAL', true);
		}

		if (!$updater->TableExists('b_sale_bizval'))
		{
			if ($DB->type == 'MYSQL')
			{
				$DB->Query('create table if not exists b_sale_bizval
				(
					CODE_KEY varchar(50) not null,
					CONSUMER_KEY varchar(50) not null,
					PERSON_TYPE_ID int not null,
					PROVIDER_KEY varchar(50) not null,
					PROVIDER_VALUE varchar(255) null,
					primary key(CODE_KEY, CONSUMER_KEY, PERSON_TYPE_ID)
				)');
			}

			if ($DB->type == 'MSSQL')
			{
				$DB->Query('CREATE TABLE B_SALE_BIZVAL
				(
					CODE_KEY VARCHAR(50) NOT NULL,
					CONSUMER_KEY VARCHAR(50) NOT NULL,
					PERSON_TYPE_ID INT NOT NULL,
					PROVIDER_KEY VARCHAR(50) NOT NULL,
					PROVIDER_VALUE VARCHAR(255) NULL
				)');

				$DB->Query('ALTER TABLE B_SALE_BIZVAL ADD CONSTRAINT PK_B_SALE_BIZVAL PRIMARY KEY (CODE_KEY, CONSUMER_KEY, PERSON_TYPE_ID)');
			}

			if ($DB->type == 'ORACLE')
			{
				$DB->Query('CREATE TABLE B_SALE_BIZVAL
				(
					CODE_KEY VARCHAR(50 CHAR) NOT NULL,
					CONSUMER_KEY VARCHAR(50 CHAR) NOT NULL,
					PERSON_TYPE_ID NUMBER(18) NOT NULL,
					PROVIDER_KEY VARCHAR(50 CHAR) NOT NULL,
					PROVIDER_VALUE VARCHAR(255 CHAR) NULL,
					PRIMARY KEY (CODE_KEY, CONSUMER_KEY, PERSON_TYPE_ID)
				)');
			}
		}
	}
	}

	if ($updater->TableExists('b_sale_bizval_code'))
	{
		$updater->Query(array(
			'MySQL'  => 'drop table b_sale_bizval_code',
			'MSSQL'  => 'DROP TABLE B_SALE_BIZVAL_CODE',
			'ORACLE' => 'DROP TABLE B_SALE_BIZVAL_CODE CASCADE CONSTRAINTS',
		));
		$updater->Query(array('ORACLE' => 'DROP SEQUENCE SQ_B_SALE_BIZVAL_CODE'));
	}

	if ($updater->TableExists('b_sale_bizval_group'))
	{
		$updater->Query(array(
			'MySQL'  => 'drop table b_sale_bizval_group',
			'MSSQL'  => 'DROP TABLE B_SALE_BIZVAL_GROUP',
			'ORACLE' => 'DROP TABLE B_SALE_BIZVAL_GROUP CASCADE CONSTRAINTS',
		));
		$updater->Query(array('ORACLE' => 'DROP SEQUENCE SQ_B_SALE_BIZVAL_GROUP'));
	}

	if ($updater->TableExists('b_sale_bizval_parent'))
	{
		$updater->Query(array(
			'MySQL'  => 'drop table b_sale_bizval_parent',
			'MSSQL'  => 'DROP TABLE B_SALE_BIZVAL_PARENT',
			'ORACLE' => 'DROP TABLE B_SALE_BIZVAL_PARENT CASCADE CONSTRAINTS',
		));
		$updater->Query(array('ORACLE' => 'DROP SEQUENCE SQ_B_SALE_BIZVAL_PARENT'));
	}

	if ($updater->TableExists('b_sale_bizval_codeparent'))
	{
		$updater->Query(array(
			'MySQL'  => 'drop table b_sale_bizval_codeparent',
			'MSSQL'  => 'DROP TABLE B_SALE_BIZVAL_CODEPARENT',
			'ORACLE' => 'DROP TABLE B_SALE_BIZVAL_CODEPARENT CASCADE CONSTRAINTS',
		));
	}

	if (! $updater->TableExists('b_sale_bizval_persondomain'))
	{
		$updater->Query(array(
			'MySQL' => '
				create table b_sale_bizval_persondomain
				(
					PERSON_TYPE_ID int not null,
					DOMAIN char(1) not null,
					primary key(PERSON_TYPE_ID, DOMAIN)
				)
			',
			'MSSQL' => '
				CREATE TABLE B_SALE_BIZVAL_PERSONDOMAIN
				(
					PERSON_TYPE_ID INT NOT NULL,
					DOMAIN CHAR(1) NOT NULL
				)
			',
			'ORACLE' => '
				CREATE TABLE B_SALE_BIZVAL_PERSONDOMAIN
				(
					PERSON_TYPE_ID NUMBER(18) NOT NULL,
					DOMAIN CHAR(1 CHAR) NOT NULL,
					PRIMARY KEY (PERSON_TYPE_ID, DOMAIN)
				)
			',
		));
		$updater->Query(array('MSSQL' => 'ALTER TABLE B_SALE_BIZVAL_PERSONDOMAIN ADD CONSTRAINT PK_B_SALE_BIZVAL_PERSONDOMAIN PRIMARY KEY (PERSON_TYPE_ID, DOMAIN)'));
	}

	// prepare 1C-integration to new business-value schema

	if (!$DB->TableExists('b_sale_bizval_code_1C'))
	{
		$updater->Query(array(
			'MySQL' => '
				create table b_sale_bizval_code_1C
				(
					PERSON_TYPE_ID int not null,
					CODE_INDEX int not null,
					NAME varchar(255) not null,
					primary key(PERSON_TYPE_ID, CODE_INDEX)
				)
			',
			'MSSQL' => '
				CREATE TABLE B_SALE_BIZVAL_CODE_1C
				(
					PERSON_TYPE_ID INT NOT NULL,
					CODE_INDEX INT NOT NULL,
					NAME VARCHAR(255) NOT NULL
				)
			',
			'ORACLE' => '
				CREATE TABLE B_SALE_BIZVAL_CODE_1C
				(
					PERSON_TYPE_ID NUMBER(18) NOT NULL,
					CODE_INDEX NUMBER(18) NOT NULL,
					NAME VARCHAR(255 CHAR) NOT NULL,
					PRIMARY KEY (PERSON_TYPE_ID, CODE_INDEX)
				)
			',
		));
		$updater->Query(array('MSSQL' => 'ALTER TABLE B_SALE_BIZVAL_CODE_1C ADD CONSTRAINT PK_B_SALE_BIZVAL_CODE_1C PRIMARY KEY (PERSON_TYPE_ID, CODE_INDEX)'));
	}

	// the rest

	if ($DB->type == "MSSQL")
	{
		if ($updater->TableExists("B_SALE_PAY_SYSTEM_ACTION"))
		{
			if ($DB->Query("SELECT PAY_SYSTEM_ID FROM B_SALE_PAY_SYSTEM_ACTION WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE B_SALE_PAY_SYSTEM_ACTION ALTER COLUMN PAY_SYSTEM_ID int NULL");
			}
			if ($DB->Query("SELECT PERSON_TYPE_ID FROM B_SALE_PAY_SYSTEM_ACTION WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE B_SALE_PAY_SYSTEM_ACTION ALTER COLUMN PERSON_TYPE_ID int NULL");
			}
			if (!$DB->Query("SELECT SORT FROM b_sale_pay_system_action WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE B_SALE_PAY_SYSTEM_ACTION ADD SORT int NOT NULL DEFAULT 100");
			}
			if (!$DB->Query("SELECT DESCRIPTION FROM b_sale_pay_system_action WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE B_SALE_PAY_SYSTEM_ACTION ADD DESCRIPTION varchar(2000) NULL");
			}
			if (!$DB->Query("SELECT PSA_NAME FROM b_sale_pay_system_action WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE B_SALE_PAY_SYSTEM_ACTION ADD PSA_NAME VARCHAR(255) NULL");
			}
			if (!$DB->Query("SELECT CODE FROM b_sale_pay_system_action WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE B_SALE_PAY_SYSTEM_ACTION ADD CODE VARCHAR(50) NULL");
			}
			if (!$DB->Query("SELECT HAVE_PRICE FROM b_sale_pay_system_action WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE B_SALE_PAY_SYSTEM_ACTION ADD HAVE_PRICE char(1) NOT NULL DEFAULT 'N'");
			}
			if (!$DB->Query("SELECT IS_CASH FROM b_sale_pay_system_action WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE B_SALE_PAY_SYSTEM_ACTION ADD IS_CASH char(1) NOT NULL DEFAULT 'N'");
			}
			if (!$DB->Query("select ACTIVE from b_sale_pay_system_action WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE B_SALE_PAY_SYSTEM_ACTION ADD ACTIVE char(1) NOT NULL DEFAULT 'Y'");
			}
			if (!$DB->Query("SELECT ALLOW_EDIT_PAYMENT FROM b_sale_pay_system_action WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE B_SALE_PAY_SYSTEM_ACTION ADD ALLOW_EDIT_PAYMENT char(1) NOT NULL DEFAULT 'Y'");
			}
			if ($DB->IndexExists("B_SALE_PAY_SYSTEM_ACTION", array("PERSON_TYPE_ID", )))
			{
				$DB->Query("DROP INDEX IX_B_SALE_PAY_SYSTEM_ACTION_1 ON B_SALE_PAY_SYSTEM_ACTION");
			}
			if ($DB->IndexExists("B_SALE_PAY_SYSTEM_ACTION", array("PAY_SYSTEM_ID", "PERSON_TYPE_ID", )))
			{
				$DB->Query("DROP INDEX UX_B_SALE_PAY_SYSTEM_ACTION_1 ON B_SALE_PAY_SYSTEM_ACTION");
			}
		}
		if ($updater->TableExists("B_SALE_ORDER_PAYMENT"))
		{
			if (!$DB->Query("SELECT PS_INVOICE_ID FROM b_sale_order_payment WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE B_SALE_ORDER_PAYMENT ADD PS_INVOICE_ID VARCHAR(250) NULL");
			}
			if (!$DB->Query("SELECT PRICE_COD FROM b_sale_order_payment WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE B_SALE_ORDER_PAYMENT ADD PRICE_COD DECIMAL(18,4) NOT NULL DEFAULT 0");
			}
			if ($DB->Query("SELECT PS_STATUS_CODE FROM B_SALE_ORDER_PAYMENT WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE B_SALE_ORDER_PAYMENT ALTER COLUMN PS_STATUS_CODE VARCHAR(255)");
			}
		}
		if (!$updater->TableExists("B_SALE_PAY_SYSTEM_ES"))
		{
			$DB->Query("
				CREATE TABLE B_SALE_PAY_SYSTEM_ES(
					ID INT NOT NULL IDENTITY(1,1),
					CODE VARCHAR(50) NULL,
					NAME VARCHAR(255) NOT NULL,
					DESCRIPTION VARCHAR(255) NULL,
					CLASS_NAME VARCHAR(255) NOT NULL,
					PARAMS TEXT NULL,
					SHOW_MODE CHAR(1) NULL,
					PAY_SYSTEM_ID INT NOT NULL,
					DEFAULT_VALUE VARCHAR(255) NULL,
					ACTIVE CHAR(1) NOT NULL,
					SORT INT DEFAULT 100
				)
			");
			$DB->Query("
				ALTER TABLE B_SALE_PAY_SYSTEM_ES ADD CONSTRAINT PK_B_SALE_PAY_SYSTEM_ES PRIMARY KEY (ID)
			");
		}
		if ($updater->TableExists("B_SALE_PAY_SYSTEM_ES"))
		{
			if (!$DB->IndexExists("B_SALE_PAY_SYSTEM_ES", array("PAY_SYSTEM_ID", )))
			{
				$DB->Query("CREATE INDEX IX_BSPS_ES_PAY_SYSTEM_ID ON B_SALE_PAY_SYSTEM_ES(PAY_SYSTEM_ID)");
			}
		}
		if (!$updater->TableExists("B_SALE_ORDER_PAYMENT_ES"))
		{
			$DB->Query("
				CREATE TABLE B_SALE_ORDER_PAYMENT_ES(
					ID INT NOT NULL IDENTITY(1,1),
					PAYMENT_ID INT NOT NULL,
					EXTRA_SERVICE_ID INT NOT NULL,
					VALUE VARCHAR(255) NULL
				)
			");
			$DB->Query("
				ALTER TABLE B_SALE_ORDER_PAYMENT_ES ADD CONSTRAINT PK_B_SALE_ORDER_PAYMENT_ES PRIMARY KEY (ID)
			");
		}
		if ($updater->TableExists("B_SALE_ORDER_PAYMENT_ES"))
		{
			if (!$DB->IndexExists("B_SALE_ORDER_PAYMENT_ES", array("PAYMENT_ID", )))
			{
				$DB->Query("CREATE INDEX IX_BSOP_ES_PAYMENT_ID ON B_SALE_ORDER_PAYMENT_ES(PAYMENT_ID)");
			}
			if (!$DB->IndexExists("B_SALE_ORDER_PAYMENT_ES", array("EXTRA_SERVICE_ID", )))
			{
				$DB->Query("CREATE INDEX IX_BSOP_ES_EXTRA_SERVICE_ID ON B_SALE_ORDER_PAYMENT_ES(EXTRA_SERVICE_ID)");
			}
		}
		if (!$updater->TableExists("B_SALE_PAY_SYSTEM_ERR_LOG"))
		{
			$DB->Query("
				CREATE TABLE B_SALE_PAY_SYSTEM_ERR_LOG(
					ID INT NOT NULL IDENTITY(1,1),
					MESSAGE TEXT NOT NULL,
					DATE_INSERT datetime NOT NULL,
					ACTION VARCHAR(255) NOT NULL
				)
			");
			$DB->Query("
				ALTER TABLE B_SALE_PAY_SYSTEM_ERR_LOG ADD CONSTRAINT PK_B_SALE_PAY_SYSTEM_LOG PRIMARY KEY (ID)
			");
		}
		
	}
}

if ($updater->CanUpdateDatabase() && $updater->TableExists('b_sale_auxiliary'))
{
	if ($DB->type == "MYSQL")
	{
		if ($updater->TableExists("b_sale_pay_system_action"))
		{
			if ($DB->Query("SELECT PAY_SYSTEM_ID FROM b_sale_pay_system_action WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sale_pay_system_action CHANGE PAY_SYSTEM_ID PAY_SYSTEM_ID int null");
			}
			if ($DB->Query("SELECT PERSON_TYPE_ID FROM b_sale_pay_system_action WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sale_pay_system_action CHANGE PERSON_TYPE_ID PERSON_TYPE_ID int null");
			}
			if (!$DB->Query("SELECT CODE FROM b_sale_pay_system_action WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sale_pay_system_action ADD CODE varchar(50) NULL");
			}
			if (!$DB->Query("SELECT PSA_NAME FROM b_sale_pay_system_action WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sale_pay_system_action ADD PSA_NAME varchar(255) NULL");
			}
			if (!$DB->Query("SELECT SORT FROM b_sale_pay_system_action WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sale_pay_system_action ADD SORT int not null default '100'");
			}
			if (!$DB->Query("SELECT DESCRIPTION FROM b_sale_pay_system_action WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sale_pay_system_action ADD DESCRIPTION varchar(2000) null");
			}
			if (!$DB->Query("SELECT HAVE_PRICE FROM b_sale_pay_system_action WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sale_pay_system_action ADD HAVE_PRICE char(1) not null default 'N'");
			}
			if (!$DB->Query("SELECT ALLOW_EDIT_PAYMENT FROM b_sale_pay_system_action WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sale_pay_system_action ADD ALLOW_EDIT_PAYMENT char(1) not null default 'Y'");
			}
			if (!$DB->Query("select ACTIVE from b_sale_pay_system_action WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sale_pay_system_action ADD ACTIVE char(1) not null default 'Y'");
			}
			if (!$DB->Query("SELECT IS_CASH FROM b_sale_pay_system_action WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sale_pay_system_action ADD IS_CASH char(1) not null default 'N'");
			}
			if ($DB->IndexExists("b_sale_pay_system_action", array("PAY_SYSTEM_ID", "PERSON_TYPE_ID", )))
			{
				$DB->Query("DROP INDEX IX_SPSA_PSPT_UNI ON b_sale_pay_system_action");
			}
			if ($DB->IndexExists("b_sale_pay_system_action", array("PERSON_TYPE_ID", )))
			{
				$DB->Query("DROP INDEX IXS_PAY_SYSTEM_ACTION_PERSON_TYPE_ID ON b_sale_pay_system_action");
			}
		}
		if ($updater->TableExists("b_sale_order_payment"))
		{
			if (!$DB->Query("SELECT PS_INVOICE_ID FROM b_sale_order_payment WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sale_order_payment ADD PS_INVOICE_ID VARCHAR(250) NULL");
			}
			if (!$DB->Query("SELECT PRICE_COD FROM b_sale_order_payment WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sale_order_payment ADD PRICE_COD DECIMAL(18,4) NOT NULL DEFAULT 0");
			}
			if ($DB->Query("SELECT PS_STATUS_CODE FROM b_sale_order_payment WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sale_order_payment CHANGE PS_STATUS_CODE PS_STATUS_CODE varchar(255) null");
			}
		}
		if (!$updater->TableExists("b_sale_pay_system_es"))
		{
			$DB->Query("
				CREATE TABLE b_sale_pay_system_es(
					ID int NOT NULL AUTO_INCREMENT,
					CODE varchar(50) NULL,
					NAME varchar(255) NOT NULL,
					DESCRIPTION varchar(255) NULL,
					CLASS_NAME varchar(255) NOT NULL,
					PARAMS text NULL,
					SHOW_MODE char(1) NULL,
					PAY_SYSTEM_ID int NOT NULL,
					DEFAULT_VALUE varchar(255) NULL,
					ACTIVE char(1) NOT NULL,
					SORT int DEFAULT 100,
					primary key (ID)
				)
			");
		}
		if ($updater->TableExists("b_sale_pay_system_es"))
		{
			if (!$DB->IndexExists("b_sale_pay_system_es", array("PAY_SYSTEM_ID", )))
			{
				$DB->Query("CREATE INDEX IX_BSPS_ES_PAY_SYSTEM_ID ON b_sale_pay_system_es(PAY_SYSTEM_ID)");
			}
		}
		if (!$updater->TableExists("b_sale_order_payment_es"))
		{
			$DB->Query("
				CREATE TABLE b_sale_order_payment_es(
					ID INT NOT NULL AUTO_INCREMENT,
					PAYMENT_ID INT NOT NULL,
					EXTRA_SERVICE_ID INT NOT NULL,
					VALUE VARCHAR (255) NULL,
					PRIMARY KEY (ID)
				)
			");
		}
		if ($updater->TableExists("b_sale_order_payment_es"))
		{
			if (!$DB->IndexExists("b_sale_order_payment_es", array("PAYMENT_ID", )))
			{
				$DB->Query("CREATE INDEX IX_BSOP_ES_PAYMENT_ID ON b_sale_order_payment_es(PAYMENT_ID)");
			}
			if (!$DB->IndexExists("b_sale_order_payment_es", array("EXTRA_SERVICE_ID", )))
			{
				$DB->Query("CREATE INDEX IX_BSOP_ES_EXTRA_SERVICE_ID ON b_sale_order_payment_es(EXTRA_SERVICE_ID)");
			}
		}
		if (!$updater->TableExists("b_sale_pay_system_err_log"))
		{
			$DB->Query("
				CREATE TABLE b_sale_pay_system_err_log(
					ID int NOT NULL AUTO_INCREMENT,
					MESSAGE TEXT NOT NULL,
					ACTION varchar(255) NOT NULL,
					DATE_INSERT datetime NOT NULL,
					primary key (ID)
				)
			");
		}
		
/*		if ($updater->TableExists('b_sale_order'))
		{
			$DB->Query("UPDATE b_sale_order bso
				INNER JOIN b_sale_pay_system_action bpsa ON bpsa.PAY_SYSTEM_ID=bso.PAY_SYSTEM_ID AND bpsa.PERSON_TYPE_ID=bso.PERSON_TYPE_ID
				SET bso.PAY_SYSTEM_ID=bpsa.ID"
			);
		}

		if ($updater->TableExists('b_sale_order_payment'))
		{
			$DB->Query("UPDATE b_sale_order_payment bsop
				INNER JOIN b_sale_order bso ON bso.ID=bsop.ORDER_ID
				INNER JOIN b_sale_pay_system_action bpsa ON bpsa.PAY_SYSTEM_ID=bsop.PAY_SYSTEM_ID AND bpsa.PERSON_TYPE_ID=bso.PERSON_TYPE_ID
				SET bsop.PAY_SYSTEM_ID=bpsa.ID"
			);
		}*/
	}
}

if ($updater->CanUpdateDatabase() && $updater->TableExists('B_SALE_AUXILIARY'))
{
	if ($DB->type == "ORACLE")
	{
		$dbNextVal = $DB->Query("SELECT SQ_SALE_PAY_SYSTEM_ACTION.NEXTVAL FROM DUAL", true);
		if ($dbNextVal)
		{
			$data = $dbNextVal->Fetch();
			$id = intval($data["NEXTVAL"]);

			$DB->Query('DROP SEQUENCE SQ_SALE_PAY_SYSTEM_ACTION', true);
			$DB->Query('CREATE SEQUENCE SQ_B_SALE_PAY_SYSTEM_ACTION START WITH '.$id.' INCREMENT BY 1 NOMAXVALUE NOCYCLE NOCACHE NOORDER', true);
		}
		
		$id = 1;
		$dbNextVal = $DB->Query("SELECT SQ_B_SALE_DELIVERY_RSTR.NEXTVAL FROM DUAL", true);
		if ($dbNextVal)
		{
			$data = $dbNextVal->Fetch();
			$id = intval($data["NEXTVAL"]);

			$DB->Query('DROP SEQUENCE SQ_B_SALE_DELIVERY_RSTR', true);
			$DB->Query('DROP TRIGGER B_SALE_DELIVERY_RSTR_INSERT', true);
		}

		$DB->Query('CREATE SEQUENCE SQ_B_SALE_SERVICE_RSTR START WITH '.$id.' INCREMENT BY 1 NOMAXVALUE NOCYCLE NOCACHE NOORDER', true);
		$DB->Query('
			CREATE OR REPLACE TRIGGER B_SALE_SERVICE_RSTR_INSERT
			BEFORE INSERT
			ON B_SALE_SERVICE_RSTR
			FOR EACH ROW
			BEGIN
				IF :NEW.ID IS NULL THEN
					SELECT SQ_B_SALE_SERVICE_RSTR.NEXTVAL INTO :NEW.ID FROM dual;
				END IF;
			END;', true
		);		
		
		$DB->Query("CREATE SEQUENCE SQ_B_SALE_PAY_SYSTEM_ES INCREMENT BY 1 NOMAXVALUE NOCYCLE NOCACHE NOORDER", true);
		$DB->Query("CREATE SEQUENCE SQ_B_SALE_ORDER_PAYMENT_ES INCREMENT BY 1 NOMAXVALUE NOCYCLE NOCACHE NOORDER", true);
		$DB->Query("CREATE SEQUENCE SQ_B_SALE_PAY_SYSTEM_ERR_LOG INCREMENT BY 1 NOMAXVALUE NOCYCLE NOCACHE NOORDER", true);
		if ($updater->TableExists("B_SALE_PAY_SYSTEM_ACTION"))
		{
			if ($DB->Query("SELECT PAY_SYSTEM_ID FROM B_SALE_PAY_SYSTEM_ACTION WHERE 1=0", true))
			{
				$DB->Query("
					declare
						l_nullable varchar2(1);
					begin
						select nullable into l_nullable
						from user_tab_columns
						where table_name = 'B_SALE_PAY_SYSTEM_ACTION'
						and   column_name = 'PAY_SYSTEM_ID';
						if l_nullable = 'N' then
							execute immediate 'alter table B_SALE_PAY_SYSTEM_ACTION modify (PAY_SYSTEM_ID NULL)';
						end if;
					end;
				");
			}
			if ($DB->Query("SELECT PERSON_TYPE_ID FROM B_SALE_PAY_SYSTEM_ACTION WHERE 1=0", true))
			{
				$DB->Query("
					declare
						l_nullable varchar2(1);
					begin
						select nullable into l_nullable
						from user_tab_columns
						where table_name = 'B_SALE_PAY_SYSTEM_ACTION'
						and   column_name = 'PERSON_TYPE_ID';
						if l_nullable = 'N' then
							execute immediate 'alter table B_SALE_PAY_SYSTEM_ACTION modify (PERSON_TYPE_ID NULL)';
						end if;
					end;
				");
			}
			if (!$DB->Query("SELECT SORT FROM b_sale_pay_system_action WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE B_SALE_PAY_SYSTEM_ACTION ADD (SORT NUMBER(18) DEFAULT '100' NOT NULL)");
			}
			if (!$DB->Query("SELECT DESCRIPTION FROM b_sale_pay_system_action WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE B_SALE_PAY_SYSTEM_ACTION ADD (DESCRIPTION VARCHAR2(2000 CHAR) NULL)");
			}
			if (!$DB->Query("select HAVE_PRICE from b_sale_pay_system_action WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sale_pay_system_action ADD HAVE_PRICE CHAR(1 CHAR) DEFAULT 'N' NOT NULL");
			}
			if (!$DB->Query("select ACTIVE from b_sale_pay_system_action WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sale_pay_system_action ADD ACTIVE CHAR(1 CHAR) DEFAULT 'Y' NOT NULL");
			}
			if (!$DB->Query("SELECT PSA_NAME FROM b_sale_pay_system_action WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE B_SALE_PAY_SYSTEM_ACTION ADD (PSA_NAME VARCHAR(255 CHAR) NULL)");
			}
			if (!$DB->Query("SELECT CODE FROM b_sale_pay_system_action WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE B_SALE_PAY_SYSTEM_ACTION ADD (CODE VARCHAR(50 CHAR) NULL)");
			}
			if (!$DB->Query("SELECT ALLOW_EDIT_PAYMENT FROM b_sale_pay_system_action WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE B_SALE_PAY_SYSTEM_ACTION ADD (ALLOW_EDIT_PAYMENT CHAR(1 CHAR) DEFAULT 'Y' NOT NULL)");
			}
			if (!$DB->Query("SELECT IS_CASH FROM b_sale_pay_system_action WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE B_SALE_PAY_SYSTEM_ACTION ADD (IS_CASH CHAR(1 CHAR) DEFAULT 'N' NOT NULL)");
			}
			if ($DB->IndexExists("B_SALE_PAY_SYSTEM_ACTION", array("PERSON_TYPE_ID", )))
			{
				$DB->Query("DROP INDEX IXS_PSA_PERSON_TYPE_ID");
			}
			if ($DB->IndexExists("B_SALE_PAY_SYSTEM_ACTION", array("PAY_SYSTEM_ID", "PERSON_TYPE_ID", )))
			{
				$DB->Query("DROP INDEX IX_SPSA_PSPT_UNI");
			}
			$DB->Query("DROP TRIGGER B_SALE_PS_ACTION_INSERT", true);
			$DB->Query("CREATE OR REPLACE TRIGGER B_SALE_PS_ACTION_INSERT
BEFORE INSERT
ON B_SALE_PAY_SYSTEM_ACTION
FOR EACH ROW
BEGIN
	IF :NEW.ID IS NULL THEN
 		SELECT SQ_B_SALE_PAY_SYSTEM_ACTION.NEXTVAL INTO :NEW.ID FROM dual;
	END IF;
END;", true);
		}

		if ($updater->TableExists("B_SALE_DELIVERY_ES"))
		{
			$DB->Query("CREATE OR REPLACE TRIGGER B_SALE_PAY_SYSTEM_ES_INSERT
BEFORE INSERT
ON B_SALE_DELIVERY_ES
FOR EACH ROW
BEGIN
	IF :NEW.ID IS NULL THEN
		SELECT SQ_B_SALE_PAY_SYSTEM_ES.NEXTVAL INTO :NEW.ID FROM dual;
	END IF;
END;", true);
		}
		if ($updater->TableExists("B_SALE_ORDER_PAYMENT"))
		{
			if (!$DB->Query("SELECT PS_INVOICE_ID FROM b_sale_order_payment WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE B_SALE_ORDER_PAYMENT ADD (PS_INVOICE_ID VARCHAR2(250 CHAR) NULL)");
			}
			if (!$DB->Query("SELECT PRICE_COD FROM b_sale_order_payment WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE B_SALE_ORDER_PAYMENT ADD (PRICE_COD DECIMAL(18,4) DEFAULT 0 NOT NULL)");
			}
			if ($DB->Query("SELECT PS_STATUS_CODE FROM B_SALE_ORDER_PAYMENT WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE B_SALE_ORDER_PAYMENT MODIFY PS_STATUS_CODE VARCHAR2(255 CHAR)");
			}
		}
		if (!$updater->TableExists("B_SALE_PAY_SYSTEM_ES"))
		{
			$DB->Query("
				CREATE TABLE B_SALE_PAY_SYSTEM_ES(
					ID NUMBER(18) NOT NULL,
					CODE VARCHAR(50 CHAR) NULL,
					NAME VARCHAR(255 CHAR) NULL,
					DESCRIPTION VARCHAR(255 CHAR) NULL,
					CLASS_NAME VARCHAR(255 CHAR) NULL,
					PARAMS VARCHAR(4000 CHAR) NULL,
					SHOW_MODE CHAR(1 CHAR) NULL,
					PAY_SYSTEM_ID NUMBER(18) NOT NULL,
					DEFAULT_VALUE VARCHAR(255 CHAR) NULL,
					ACTIVE CHAR(1 CHAR) NOT NULL,
					SORT NUMBER(11) DEFAULT 100 NOT NULL,
					PRIMARY KEY (ID)
				)
			");
		}
		if ($updater->TableExists("B_SALE_PAY_SYSTEM_ES"))
		{
			if (!$DB->IndexExists("B_SALE_PAY_SYSTEM_ES", array("PAY_SYSTEM_ID", )))
			{
				$DB->Query("CREATE INDEX IX_BSPS_ES_PAY_SYSTEM_ID ON B_SALE_PAY_SYSTEM_ES(PAY_SYSTEM_ID)");
			}
		}
		if (!$updater->TableExists("B_SALE_ORDER_PAYMENT_ES"))
		{
			$DB->Query("
				CREATE TABLE B_SALE_ORDER_PAYMENT_ES(
					ID NUMBER(18) NOT NULL,
					PAYMENT_ID NUMBER(18) NOT NULL,
					EXTRA_SERVICE_ID NUMBER(18) NOT NULL,
					VALUE VARCHAR(255 CHAR) NULL,
					PRIMARY KEY (ID)
				)
			");
		}
		if ($updater->TableExists("B_SALE_ORDER_PAYMENT_ES"))
		{
			if (!$DB->IndexExists("B_SALE_ORDER_PAYMENT_ES", array("PAYMENT_ID", )))
			{
				$DB->Query("CREATE INDEX IX_BSOP_ES_PAYMENT_ID ON B_SALE_ORDER_PAYMENT_ES(PAYMENT_ID)");
			}
			if (!$DB->IndexExists("B_SALE_ORDER_PAYMENT_ES", array("EXTRA_SERVICE_ID", )))
			{
				$DB->Query("CREATE INDEX IX_BSOP_ES_EXTRA_SERVICE_ID ON B_SALE_ORDER_PAYMENT_ES(EXTRA_SERVICE_ID)");
			}
			$DB->Query("CREATE OR REPLACE TRIGGER BSO_PAYMENT_ES_INSERT
BEFORE INSERT
ON B_SALE_ORDER_PAYMENT_ES
FOR EACH ROW
BEGIN
	IF :NEW.ID IS NULL THEN
		SELECT SQ_B_SALE_ORDER_PAYMENT_ES.NEXTVAL INTO :NEW.ID FROM dual;
	END IF;
END;", true);
		}
		if (!$updater->TableExists("B_SALE_PAY_SYSTEM_ERR_LOG"))
		{
			$DB->Query("
				CREATE TABLE B_SALE_PAY_SYSTEM_ERR_LOG(
					ID NUMBER(18) NOT NULL,
					ACTION VARCHAR(255 CHAR) NOT NULL,
					MESSAGE CLOB NOT NULL,
					DATE_INSERT DATE NOT NULL,
					PRIMARY KEY (ID)
				)
			");
		}
		if ($updater->TableExists("B_SALE_PAY_SYSTEM_ERR_LOG"))
		{
			$DB->Query("CREATE OR REPLACE TRIGGER B_SALE_PAY_SYSTEM_LOG_INSERT
BEFORE INSERT
ON B_SALE_PAY_SYSTEM_ERR_LOG
FOR EACH ROW
BEGIN
	IF :NEW.ID IS NULL THEN
		SELECT SQ_B_SALE_PAY_SYSTEM_ERR_LOG.NEXTVAL INTO :NEW.ID FROM dual;
	END IF;
END;", true);
		}
	}
}

if($updater->CanUpdateDatabase())
{
	if ($DB->TableExists("b_sale_loc_ext") && !$DB->IndexExists('b_sale_loc_ext', array('LOCATION_ID', 'SERVICE_ID')))
	{
		$DB->query("CREATE INDEX IX_B_SALE_LOC_EXT_LID_SID ON b_sale_loc_ext (LOCATION_ID, SERVICE_ID)");
	}
}

if($updater->CanUpdateKernel())
{
	$arToDelete = array(
		"modules/sale/lib/internals/businessvalue_code.php",
		"modules/sale/lib/internals/businessvalue_codeparent.php",
		"modules/sale/lib/internals/businessvalue_group.php",
		"modules/sale/lib/internals/businessvalue_parent.php",
		"modules/sale/lang/de/lib/delivery/services/simple.php",
		"modules/sale/lang/en/lib/delivery/services/simple.php",
		"modules/sale/lang/ru/lib/delivery/services/simple.php",
		"modules/sale/lib/delivery/services/simple.php",
		"modules/sale/lang/ru/lib/delivery/services/table.php",
		"modules/sale/handlers/delivery/simple/lang/de/simple.php",
		"modules/sale/handlers/delivery/simple/lang/en/simple.php",
		"modules/sale/lang/ru/lib/internals/delivery.php",
	);
	foreach($arToDelete as $file)
		CUpdateSystem::DeleteDirFilesEx($_SERVER["DOCUMENT_ROOT"].$updater->kernelPath."/".$file);
}
if(IsModuleInstalled('sale') && $updater->CanUpdateDatabase())
{
	RegisterModuleDependences("sale", "OnCondSaleActionsControlBuildList", "sale", "CSaleActionGiftCtrlGroup", "GetControlDescr", 200);
	
	$eventManager = \Bitrix\Main\EventManager::getInstance();
	$eventManager->registerEventHandler('sale', 'OnGetBusinessValueGroups', 'sale', '\Bitrix\Sale\PaySystem\Manager', 'getBusValueGroups');
	$eventManager->registerEventHandler('sale', 'OnGetBusinessValueConsumers', 'sale', '\Bitrix\Sale\PaySystem\Manager', 'getConsumersList');

	$isExpirationProcessingEvents = COption::GetOptionString("sale", "expiration_processing_events", 'N');
	if ($isExpirationProcessingEvents == "Y")
	{
		$eventManager->registerEventHandler('sale', 'OnBeforeSaleBasketItemEntityDeleted', 'sale', '\Bitrix\Sale\Compatible\EventCompatibility', 'OnBeforeBasketDelete');
		$eventManager->registerEventHandler('sale', 'OnSaleBasketItemDeleted', 'sale', '\Bitrix\Sale\Compatible\EventCompatibility', 'OnBasketDelete');
		$eventManager->registerEventHandler('sale', 'OnShipmentAllowDelivery', 'sale', '\Bitrix\Sale\Compatible\EventCompatibility', 'onShipmentAllowDelivery');
	}

	UnRegisterModuleDependences("sale", "OnCondSaleActionsControlBuildList", "sale", "CSaleActionGiftCtrlGroup", "GetControlDescr", 200);

}

if(IsModuleInstalled('sale'))
{
	if ($updater->TableExists('b_sale_pay_system_action'))
	{
	if (!$DB->Query("SELECT PS_MODE FROM b_sale_pay_system_action WHERE 1=0", true))
	{
			if ($DB->Query("SELECT ACTIVE FROM b_sale_pay_system_action WHERE 1=0", true))
			{
				if ($DB->type == 'MYSQL' || $DB->type == 'ORACLE')
				{
					$DB->Query('UPDATE b_sale_pay_system_action psa
						SET psa.ACTIVE=(SELECT ACTIVE FROM b_sale_pay_system ps WHERE ps.ID=psa.PAY_SYSTEM_ID)'
					);
				}
				else if ($DB->type == 'MSSQL')
				{
					$DB->Query('UPDATE psa
						SET psa.ACTIVE=(SELECT ACTIVE FROM b_sale_pay_system ps WHERE ps.ID=psa.PAY_SYSTEM_ID)
						FROM b_sale_pay_system_action psa'
					);
				}
			}
			
			if ($DB->Query("SELECT DESCRIPTION FROM b_sale_pay_system_action WHERE 1=0", true))
			{
				if ($DB->type == 'MYSQL' || $DB->type == 'ORACLE')
				{
					$DB->Query('UPDATE b_sale_pay_system_action psa
						SET psa.DESCRIPTION=(SELECT DESCRIPTION FROM b_sale_pay_system ps WHERE ps.ID=psa.PAY_SYSTEM_ID)'
					);
				}
				else if ($DB->type == 'MSSQL')
				{
					$DB->Query('UPDATE psa
						SET psa.DESCRIPTION=(SELECT DESCRIPTION FROM b_sale_pay_system ps WHERE ps.ID=psa.PAY_SYSTEM_ID)
						FROM b_sale_pay_system_action psa'
					);
				}
			}
			
			if ($DB->Query("SELECT SORT FROM b_sale_pay_system_action WHERE 1=0", true))
			{
				if ($DB->type == 'MYSQL' || $DB->type == 'ORACLE')
				{
					$DB->Query('UPDATE b_sale_pay_system_action psa
						SET psa.SORT=(SELECT SORT FROM b_sale_pay_system ps WHERE ps.ID=psa.PAY_SYSTEM_ID)'
					);
				}
				else if ($DB->type == 'MSSQL')
				{
					$DB->Query('UPDATE psa
						SET psa.SORT=(SELECT SORT FROM b_sale_pay_system ps WHERE ps.ID=psa.PAY_SYSTEM_ID)
						FROM b_sale_pay_system_action psa'
					);
				}
			}

			if ($DB->Query("SELECT psa_name FROM b_sale_pay_system_action WHERE 1=0", true))
			{
				$DB->Query('UPDATE b_sale_pay_system_action SET psa_name=name');
			}
			
		CAgent::AddAgent('CSalePaySystemAction::convertPsBusVal();', "sale", "N");
		CAgent::AddAgent('CSaleExport::migrateToBusinessValues();', "sale", "N");
	}
}
}

if ($updater->CanUpdateDatabase() && $updater->TableExists('B_SALE_AUXILIARY'))
{
	if ($DB->type == "MSSQL")
	{
		if ($DB->TableExists("B_SALE_PAY_SYSTEM_ACTION"))
		{
			if (!$DB->Query("SELECT PS_MODE FROM b_sale_pay_system_action WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE B_SALE_PAY_SYSTEM_ACTION ADD PS_MODE varchar(20) NULL");
			}
		}
	}
}

if ($updater->CanUpdateDatabase() && $updater->TableExists('b_sale_auxiliary'))
{
	if ($DB->type == "MYSQL")
	{
		if ($DB->TableExists("b_sale_pay_system_action"))
		{
			if (!$DB->Query("SELECT PS_MODE FROM b_sale_pay_system_action WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sale_pay_system_action ADD PS_MODE VARCHAR(20) NULL");
			}
		}
	}
}

if ($updater->CanUpdateDatabase() && $updater->TableExists('B_SALE_AUXILIARY'))
{
	if ($DB->type == "ORACLE")
	{
		if ($DB->TableExists("B_SALE_PAY_SYSTEM_ACTION"))
		{
			if (!$DB->Query("SELECT PS_MODE FROM b_sale_pay_system_action WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE B_SALE_PAY_SYSTEM_ACTION ADD (PS_MODE VARCHAR(20 CHAR) NULL)");
			}
		}
	}
}

if ($updater->CanUpdateDatabase() && $updater->TableExists('b_sale_order'))
{
	if ($DB->type == "MYSQL")
	{
		if ($updater->TableExists("b_sale_order"))
		{
			if ($DB->Query("SELECT ID_1C FROM b_sale_order WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sale_order CHANGE ID_1C ID_1C VARCHAR(36) DEFAULT NULL");
			}
		}
	}
	if ($DB->type == "MSSQL")
	{
		if ($updater->TableExists("b_sale_order"))
		{
			if ($DB->Query("SELECT ID_1C FROM b_sale_order WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sale_order ALTER COLUMN ID_1C VARCHAR(36) NULL");
			}
		}
	}
	if ($DB->type == "ORACLE")
	{
		if ($updater->TableExists("b_sale_order"))
		{
			if ($DB->Query("SELECT ID_1C FROM b_sale_order WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sale_order MODIFY (ID_1C VARCHAR(36))");
			}
		}
	}
}

if ($updater->CanUpdateDatabase() && $updater->TableExists('b_sale_auxiliary'))
{
	if ($DB->type == "MYSQL")
	{
		if ($updater->TableExists("b_sale_order_payment"))
		{
			if (!$DB->Query("SELECT ID_1C FROM b_sale_order_payment WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sale_order_payment ADD ID_1C VARCHAR(36) NULL");
			}
			if (!$DB->Query("SELECT VERSION_1C FROM b_sale_order_payment WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sale_order_payment ADD VERSION_1C VARCHAR(15) NULL");
			}
			
			if ($DB->Query("SELECT EXTERNAL FROM b_sale_order_payment WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sale_order_payment CHANGE COLUMN EXTERNAL EXTERNAL_PAYMENT CHAR(1)");
			}
			elseif (!$DB->Query("SELECT EXTERNAL_PAYMENT FROM b_sale_order_payment WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sale_order_payment ADD EXTERNAL_PAYMENT CHAR(1) DEFAULT 'N' NOT NULL");
			}
			
			if (!$DB->Query("SELECT UPDATED_1C FROM b_sale_order_payment WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sale_order_payment ADD UPDATED_1C CHAR(1) DEFAULT 'N' NOT NULL");
			}
		}

		if ($updater->TableExists("b_sale_order_delivery"))
		{
			if (!$DB->Query("SELECT ID_1C FROM b_sale_order_delivery WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sale_order_delivery ADD ID_1C VARCHAR(36) NULL");
			}
			if (!$DB->Query("SELECT VERSION_1C FROM b_sale_order_delivery WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sale_order_delivery ADD VERSION_1C VARCHAR(15) NULL");
			}
			
			if ($DB->Query("SELECT EXTERNAL FROM b_sale_order_delivery WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sale_order_delivery CHANGE COLUMN EXTERNAL EXTERNAL_DELIVERY CHAR(1)");
			}
			elseif (!$DB->Query("SELECT EXTERNAL_DELIVERY FROM b_sale_order_delivery WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sale_order_delivery ADD EXTERNAL_DELIVERY CHAR(1) DEFAULT 'N' NOT NULL");
			}			
			
			if (!$DB->Query("SELECT UPDATED_1C FROM b_sale_order_delivery WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sale_order_delivery ADD UPDATED_1C CHAR(1) DEFAULT 'N' NOT NULL");
			}
		}

		if ($updater->TableExists("b_sale_order"))
		{
			if ($DB->Query("SELECT STATUS_ID FROM b_sale_order WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sale_order CHANGE STATUS_ID STATUS_ID varchar(2) not null");
			}
		}
		if ($updater->TableExists("b_sale_status"))
		{
			if ($DB->Query("SELECT ID FROM b_sale_status WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sale_status CHANGE ID ID varchar(2) not null");
			}
		}
		if ($updater->TableExists("b_sale_status_lang"))
		{
			if ($DB->Query("SELECT STATUS_ID FROM b_sale_status_lang WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sale_status_lang CHANGE STATUS_ID STATUS_ID varchar(2) not null");
			}
		}
		if ($updater->TableExists("b_sale_status_group_task"))
		{
			if ($DB->Query("SELECT STATUS_ID FROM b_sale_status_group_task WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sale_status_group_task CHANGE STATUS_ID STATUS_ID varchar(2) not null");
			}
		}
		if ($updater->TableExists("b_sale_order_delivery"))
		{
			if ($DB->Query("SELECT STATUS_ID FROM b_sale_order_delivery WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sale_order_delivery CHANGE STATUS_ID STATUS_ID VARCHAR(2) NOT NULL");
			}
		}
		if ($updater->TableExists("b_sale_order_history"))
		{
			if ($DB->Query("SELECT STATUS_ID FROM b_sale_order_history WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sale_order_history CHANGE STATUS_ID STATUS_ID varchar(2) not null");
			}
		}
	}

	if ($DB->type == "ORACLE")
	{
		if ($updater->TableExists("b_sale_order_payment"))
		{
			if (!$DB->Query("SELECT ID_1C FROM b_sale_order_payment WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sale_order_payment ADD ID_1C VARCHAR2(36 CHAR) NULL");
			}
			if (!$DB->Query("SELECT VERSION_1C FROM b_sale_order_payment WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sale_order_payment ADD VERSION_1C VARCHAR2(15 CHAR) NULL");
			}
			if (!$DB->Query("SELECT EXTERNAL_PAYMENT FROM b_sale_order_payment WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sale_order_payment ADD EXTERNAL_PAYMENT CHAR(1 CHAR) DEFAULT 'N' NOT NULL");
			}
			if (!$DB->Query("SELECT UPDATED_1C FROM b_sale_order_payment WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sale_order_payment ADD UPDATED_1C CHAR(1 CHAR) DEFAULT 'N' NOT NULL");
			}
		}
		if ($updater->TableExists("b_sale_order_delivery"))
		{
			if (!$DB->Query("SELECT ID_1C FROM b_sale_order_delivery WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sale_order_delivery ADD ID_1C VARCHAR2(36 CHAR) NULL");
			}
			if (!$DB->Query("SELECT VERSION_1C FROM b_sale_order_delivery WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sale_order_delivery ADD VERSION_1C VARCHAR2(15 CHAR) NULL");
			}
			if (!$DB->Query("SELECT EXTERNAL_DELIVERY FROM b_sale_order_delivery WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sale_order_delivery ADD EXTERNAL_DELIVERY CHAR(1 CHAR) DEFAULT 'N' NOT NULL");
			}
			if (!$DB->Query("SELECT UPDATED_1C FROM b_sale_order_delivery WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sale_order_delivery ADD UPDATED_1C CHAR(1 CHAR) DEFAULT 'N' NOT NULL");
			}
		}

		$DB->Query("DROP SEQUENCE SQ_SALE_FUSER", true);
		$DB->Query("CREATE SEQUENCE SQ_B_SALE_FUSER INCREMENT BY 1 NOMAXVALUE NOCYCLE NOCACHE NOORDER", true);
		if ($updater->TableExists("B_SALE_FUSER"))
		{
			$DB->Query("DROP TRIGGER B_SALE_FUSER_INSERT", true);
			$DB->Query("CREATE OR REPLACE TRIGGER B_SALE_FUSER_INSERT
BEFORE INSERT
ON B_SALE_FUSER
FOR EACH ROW
BEGIN
	IF :NEW.ID IS NULL THEN
 		SELECT SQ_B_SALE_FUSER.NEXTVAL INTO :NEW.ID FROM dual;
	END IF;
END;", true);
		}
		if ($updater->TableExists("B_SALE_ORDER"))
		{
			if ($DB->Query("SELECT STATUS_ID FROM B_SALE_ORDER WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE B_SALE_ORDER MODIFY (STATUS_ID VARCHAR2(2))");
				$DB->Query("UPDATE B_SALE_ORDER SET STATUS_ID = TRIM(STATUS_ID)");
			}
		}
		if ($updater->TableExists("B_SALE_STATUS"))
		{
			if ($DB->Query("SELECT ID FROM B_SALE_STATUS WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE B_SALE_STATUS MODIFY (ID VARCHAR2(2))");
				$DB->Query("UPDATE B_SALE_STATUS SET ID = TRIM(ID)");
			}
		}
		if ($updater->TableExists("B_SALE_STATUS_LANG"))
		{
			if ($DB->Query("SELECT STATUS_ID FROM B_SALE_STATUS_LANG WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE B_SALE_STATUS_LANG MODIFY (STATUS_ID VARCHAR2(2))");
				$DB->Query("UPDATE B_SALE_STATUS_LANG SET STATUS_ID = TRIM(STATUS_ID)");
			}
		}
		if ($updater->TableExists("B_SALE_STATUS_GROUP_TASK"))
		{
			if ($DB->Query("SELECT STATUS_ID FROM B_SALE_STATUS_GROUP_TASK WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE B_SALE_STATUS_GROUP_TASK MODIFY (STATUS_ID VARCHAR2(2))");
				$DB->Query("UPDATE B_SALE_STATUS_GROUP_TASK SET STATUS_ID = TRIM(STATUS_ID)");
			}
		}
		if ($updater->TableExists("B_SALE_ORDER_DELIVERY"))
		{
			if ($DB->Query("SELECT STATUS_ID FROM B_SALE_ORDER_DELIVERY WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE B_SALE_ORDER_DELIVERY MODIFY (STATUS_ID VARCHAR2(2))");
				$DB->Query("UPDATE B_SALE_ORDER_DELIVERY SET STATUS_ID = TRIM(STATUS_ID)");
			}
		}
		if ($updater->TableExists("B_SALE_ORDER_HISTORY"))
		{
			if ($DB->Query("SELECT STATUS_ID FROM B_SALE_ORDER_HISTORY WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE B_SALE_ORDER_HISTORY MODIFY (STATUS_ID VARCHAR2(2))");
				$DB->Query("UPDATE B_SALE_ORDER_HISTORY SET STATUS_ID = TRIM(STATUS_ID)");
			}
		}
	}
	if ($DB->type == "MSSQL")
	{
		if ($updater->TableExists("b_sale_order_payment"))
		{
			if (!$DB->Query("SELECT ID_1C FROM b_sale_order_payment WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sale_order_payment ADD ID_1C VARCHAR(36) NULL");
			}
			if (!$DB->Query("SELECT VERSION_1C FROM b_sale_order_payment WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sale_order_payment ADD VERSION_1C VARCHAR(15) NULL");
			}
			if (!$DB->Query("SELECT EXTERNAL_PAYMENT FROM b_sale_order_payment WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sale_order_payment ADD EXTERNAL_PAYMENT CHAR(1) NOT NULL DEFAULT 'N'");
			}
			if (!$DB->Query("SELECT UPDATED_1C FROM b_sale_order_payment WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sale_order_payment ADD UPDATED_1C CHAR(1) NOT NULL DEFAULT 'N'");
			}
		}
		if ($updater->TableExists("b_sale_order_delivery"))
		{
			if (!$DB->Query("SELECT ID_1C FROM b_sale_order_delivery WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sale_order_delivery ADD ID_1C VARCHAR(36) NULL");
			}
			if (!$DB->Query("SELECT VERSION_1C FROM b_sale_order_delivery WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sale_order_delivery ADD VERSION_1C VARCHAR(15) NULL");
			}
			if (!$DB->Query("SELECT EXTERNAL_DELIVERY FROM b_sale_order_delivery WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sale_order_delivery ADD EXTERNAL_DELIVERY CHAR(1) NOT NULL DEFAULT 'N'");
			}
			if (!$DB->Query("SELECT UPDATED_1C FROM b_sale_order_delivery WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE b_sale_order_delivery ADD UPDATED_1C CHAR(1) NOT NULL DEFAULT 'N'");
			}
		}

		if ($updater->TableExists("B_SALE_ORDER"))
		{
			if ($DB->Query("SELECT STATUS_ID FROM B_SALE_ORDER WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE B_SALE_ORDER DROP CONSTRAINT DF_B_SALE_ORDER_STATUS_ID", true);

				if ($DB->IndexExists("B_SALE_ORDER", array("STATUS_ID")))
				{
					$DB->Query("DROP INDEX [B_SALE_ORDER].IX_B_SALE_ORDER_6");
				}

				$DB->Query("ALTER TABLE B_SALE_ORDER ALTER COLUMN STATUS_ID VARCHAR(2) NOT NULL");
				$DB->Query("UPDATE B_SALE_ORDER SET STATUS_ID = LTRIM(RTRIM(STATUS_ID))");

				if (!$DB->IndexExists("B_SALE_ORDER", array("STATUS_ID")))
				{
					$DB->Query("CREATE INDEX IX_B_SALE_ORDER_6 ON B_SALE_ORDER(STATUS_ID)");
				}

				$DB->Query("ALTER TABLE B_SALE_ORDER ADD CONSTRAINT DF_B_SALE_ORDER_STATUS_ID DEFAULT 'N' FOR STATUS_ID", true);
			}
		}

		if ($updater->TableExists("B_SALE_STATUS"))
		{
			if ($DB->Query("SELECT ID FROM B_SALE_STATUS WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE B_SALE_STATUS DROP CONSTRAINT PK_B_SALE_STATUS");
				$DB->Query("ALTER TABLE B_SALE_STATUS ALTER COLUMN ID VARCHAR(2) NOT NULL");
				$DB->Query("UPDATE B_SALE_STATUS SET ID = LTRIM(RTRIM(ID))");
				$DB->Query("ALTER TABLE B_SALE_STATUS ADD CONSTRAINT PK_B_SALE_STATUS PRIMARY KEY (ID)");
			}
		}
		if ($updater->TableExists("B_SALE_STATUS_LANG"))
		{
			if ($DB->Query("SELECT STATUS_ID FROM B_SALE_STATUS_LANG WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE B_SALE_STATUS_LANG DROP CONSTRAINT PK_B_SALE_STATUS_LANG");
				$DB->Query("ALTER TABLE B_SALE_STATUS_LANG ALTER COLUMN STATUS_ID VARCHAR(2) NOT NULL");
				$DB->Query("UPDATE B_SALE_STATUS_LANG SET STATUS_ID = LTRIM(RTRIM(STATUS_ID))");
				$DB->Query("ALTER TABLE B_SALE_STATUS_LANG ADD CONSTRAINT PK_B_SALE_STATUS_LANG PRIMARY KEY (STATUS_ID, LID)");
			}
		}
		if ($updater->TableExists("B_SALE_STATUS_GROUP_TASK"))
		{
			if ($DB->Query("SELECT STATUS_ID FROM B_SALE_STATUS_GROUP_TASK WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE B_SALE_STATUS_GROUP_TASK DROP CONSTRAINT PK_B_SALE_STATUS_GROUP_TASK");
				$DB->Query("ALTER TABLE B_SALE_STATUS_GROUP_TASK ALTER COLUMN STATUS_ID VARCHAR(2) NOT NULL");
				$DB->Query("UPDATE B_SALE_STATUS_GROUP_TASK SET STATUS_ID = LTRIM(RTRIM(STATUS_ID))");
				$DB->Query("ALTER TABLE B_SALE_STATUS_GROUP_TASK ADD CONSTRAINT PK_B_SALE_STATUS_GROUP_TASK PRIMARY KEY (STATUS_ID, GROUP_ID, TASK_ID)");
			}
		}
		if ($updater->TableExists("B_SALE_ORDER_DELIVERY"))
		{
			if ($DB->Query("SELECT STATUS_ID FROM B_SALE_ORDER_DELIVERY WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE B_SALE_ORDER_DELIVERY ALTER COLUMN STATUS_ID VARCHAR(2) NOT NULL");
				$DB->Query("UPDATE B_SALE_ORDER_DELIVERY SET STATUS_ID = LTRIM(RTRIM(STATUS_ID))");
			}
		}
		if ($updater->TableExists("B_SALE_ORDER_HISTORY"))
		{
			if ($DB->Query("SELECT STATUS_ID FROM B_SALE_ORDER_HISTORY WHERE 1=0", true))
			{
				$DB->Query("ALTER TABLE B_SALE_ORDER_HISTORY ALTER COLUMN STATUS_ID VARCHAR(2)");
				$DB->Query("UPDATE B_SALE_ORDER_HISTORY SET STATUS_ID = LTRIM(RTRIM(STATUS_ID))");
			}
		}
	}
}

if ($updater->CanUpdateDatabase() && !$updater->TableExists("b_sale_order_props_rel_old") && $updater->TableExists("b_sale_order_props_relation"))
{
	if ($DB->type == 'MYSQL')
	{
		$DB->Query("ALTER TABLE b_sale_order_props_relation RENAME b_sale_order_props_rel_old", true);

		$DB->Query('
			create table if not exists b_sale_order_props_relation
			(
				PROPERTY_ID INT NOT NULL,
				ENTITY_ID VARCHAR(35) NOT NULL,
				ENTITY_TYPE CHAR(1) NOT NULL,
				PRIMARY KEY (PROPERTY_ID, ENTITY_ID, ENTITY_TYPE),
				index `IX_PROPERTY` (`PROPERTY_ID`),
				index `IX_ENTITY_ID` (`ENTITY_ID`)
			)'
		);

		if($updater->TableExists("b_sale_delivery_srv") && $updater->TableExists("b_sale_order_props_relation") && $updater->TableExists("b_sale_order_props_rel_old"))
		{
			$DB->Query("
				INSERT INTO
				  b_sale_order_props_relation (PROPERTY_ID, ENTITY_ID, ENTITY_TYPE)
				SELECT
				  PROPERTY_ID, b_sale_delivery_srv.ID, ENTITY_TYPE
				FROM
				  b_sale_order_props_rel_old
				LEFT JOIN
				  b_sale_delivery_srv ON
				  b_sale_order_props_rel_old.ENTITY_ID = b_sale_delivery_srv.CODE
				WHERE
				  b_sale_order_props_rel_old.ENTITY_TYPE = 'D'
				  AND LENGTH(b_sale_delivery_srv.CODE) > 0
				  AND NOT EXISTS (
					SELECT
					  PROPERTY_ID
					FROM
					  b_sale_order_props_relation
					WHERE
					  b_sale_order_props_relation.PROPERTY_ID = b_sale_order_props_rel_old.PROPERTY_ID
					  AND b_sale_order_props_relation.ENTITY_ID = CAST(b_sale_delivery_srv.ID AS CHAR)
					  AND b_sale_order_props_relation.ENTITY_TYPE = b_sale_order_props_rel_old.ENTITY_TYPE
				  )
			");
		}
	}

	if ($DB->type == 'MSSQL')
	{
		if ($DB->IndexExists("b_sale_order_props_relation", array("PROPERTY_ID")))
			$DB->Query("DROP INDEX IX_PROPERTY on B_SALE_ORDER_PROPS_RELATION");

		if ($DB->IndexExists("b_sale_order_props_relation", array("ENTITY_ID")))
			$DB->Query("DROP INDEX IX_ENTITY_ID on B_SALE_ORDER_PROPS_RELATION");

		$DB->Query("ALTER TABLE B_SALE_ORDER_PROPS_RELATION DROP CONSTRAINT PK_B_SALE_ORDER_PROPS_RELATION");

		$DB->Query("sp_rename B_SALE_ORDER_PROPS_RELATION, B_SALE_ORDER_PROPS_REL_OLD", true);

		$DB->Query('
			CREATE TABLE B_SALE_ORDER_PROPS_RELATION
			(
				PROPERTY_ID INT NOT NULL,
				ENTITY_ID VARCHAR(35) NOT NULL,
				ENTITY_TYPE CHAR(1) NOT NULL,
			)'
		);

		$DB->Query('ALTER TABLE B_SALE_ORDER_PROPS_RELATION ADD CONSTRAINT PK_B_SALE_ORDER_PROPS_RELATION PRIMARY KEY (PROPERTY_ID, ENTITY_ID, ENTITY_TYPE)');
		$DB->Query('CREATE INDEX IX_PROPERTY ON B_SALE_ORDER_PROPS_RELATION (PROPERTY_ID)');
		$DB->Query('CREATE INDEX IX_ENTITY_ID ON B_SALE_ORDER_PROPS_RELATION (ENTITY_ID)');

		if($updater->TableExists("b_sale_delivery_srv") && $updater->TableExists("b_sale_order_props_relation") && $updater->TableExists("b_sale_order_props_rel_old"))
		{
			$DB->Query("
				INSERT INTO
				  B_SALE_ORDER_PROPS_RELATION (PROPERTY_ID, ENTITY_ID, ENTITY_TYPE)
				  SELECT
					PROPERTY_ID, B_SALE_DELIVERY_SRV.ID, ENTITY_TYPE
				  FROM
					B_SALE_ORDER_PROPS_REL_OLD
				  LEFT JOIN
					B_SALE_DELIVERY_SRV ON
					B_SALE_ORDER_PROPS_REL_OLD.ENTITY_ID = B_SALE_DELIVERY_SRV.CODE
				  WHERE
					B_SALE_ORDER_PROPS_REL_OLD.ENTITY_TYPE = 'D'
					AND LEN(B_SALE_DELIVERY_SRV.CODE) > 0
					AND NOT EXISTS (
						SELECT
						  PROPERTY_ID
						FROM
						  B_SALE_ORDER_PROPS_RELATION
						WHERE
						  B_SALE_ORDER_PROPS_RELATION.PROPERTY_ID = B_SALE_ORDER_PROPS_REL_OLD.PROPERTY_ID
						  AND B_SALE_ORDER_PROPS_RELATION.ENTITY_ID = CAST(b_sale_delivery_srv.ID AS CHAR)
						  AND B_SALE_ORDER_PROPS_RELATION.ENTITY_TYPE = B_SALE_ORDER_PROPS_REL_OLD.ENTITY_TYPE
					)
			");
		}
	}

	if ($DB->type == 'ORACLE')
	{
		if ($DB->IndexExists("b_sale_order_props_relation", array("PROPERTY_ID")))
			$DB->Query("DROP INDEX IX_PROPERTY");

		if ($DB->IndexExists("b_sale_order_props_relation", array("ENTITY_ID")))
			$DB->Query("DROP INDEX IX_ENTITY_ID");

		$DB->Query("ALTER TABLE B_SALE_ORDER_PROPS_RELATION RENAME TO B_SALE_ORDER_PROPS_REL_OLD", true);

		$DB->Query('
			CREATE TABLE B_SALE_ORDER_PROPS_RELATION
			(
				PROPERTY_ID NUMBER(18) NOT NULL,
				ENTITY_ID VARCHAR2(35 CHAR) NOT NULL,
				ENTITY_TYPE CHAR(1 CHAR) NOT NULL,
				PRIMARY KEY (PROPERTY_ID, ENTITY_ID, ENTITY_TYPE)
			)'
		);

		$DB->Query('CREATE INDEX IX_PROPERTY ON B_SALE_ORDER_PROPS_RELATION (PROPERTY_ID)');
		$DB->Query('CREATE INDEX IX_ENTITY_ID ON B_SALE_ORDER_PROPS_RELATION (ENTITY_ID)');

		if($updater->TableExists("b_sale_delivery_srv") && $updater->TableExists("b_sale_order_props_relation") && $updater->TableExists("b_sale_order_props_rel_old"))
		{
			$DB->Query("
				INSERT INTO
				  B_SALE_ORDER_PROPS_RELATION (PROPERTY_ID, ENTITY_ID, ENTITY_TYPE)
					SELECT
						PROPERTY_ID, B_SALE_DELIVERY_SRV.ID, ENTITY_TYPE
					FROM
						B_SALE_ORDER_PROPS_REL_OLD
					LEFT JOIN
						B_SALE_DELIVERY_SRV ON
						B_SALE_ORDER_PROPS_REL_OLD.ENTITY_ID = B_SALE_DELIVERY_SRV.CODE
					WHERE
						B_SALE_ORDER_PROPS_REL_OLD.ENTITY_TYPE = 'D'
						AND LENGTH(B_SALE_DELIVERY_SRV.CODE) > 0
						AND NOT EXISTS (
								SELECT
								  PROPERTY_ID
								FROM
								  B_SALE_ORDER_PROPS_RELATION
								WHERE
								  B_SALE_ORDER_PROPS_RELATION.PROPERTY_ID = B_SALE_ORDER_PROPS_REL_OLD.PROPERTY_ID
								  AND B_SALE_ORDER_PROPS_RELATION.ENTITY_ID = CAST(b_sale_delivery_srv.ID AS VARCHAR2(18))
								  AND B_SALE_ORDER_PROPS_RELATION.ENTITY_TYPE = B_SALE_ORDER_PROPS_REL_OLD.ENTITY_TYPE
						)
			");
		}
	}
	
	if($updater->TableExists("b_sale_order_props_relation") && $updater->TableExists("b_sale_order_props_rel_old"))
	{
		$DB->Query('
			INSERT INTO b_sale_order_props_relation (PROPERTY_ID, ENTITY_ID, ENTITY_TYPE)
			SELECT PROPERTY_ID, ENTITY_ID, ENTITY_TYPE
			FROM b_sale_order_props_rel_old pro
			WHERE pro.ENTITY_TYPE <> \'D\'
		');
	}
}
if($updater->CanUpdateKernel())
{
	$arToDelete = array(
		"modules/sale/handlers/paysystem/qiwi/template/tempate.php",
		"modules/sale/lang/ru/admin/pay_system_edit_old.php",
		"modules/sale/lib/paysystemservice.php",
	);
	foreach($arToDelete as $file)
		CUpdateSystem::DeleteDirFilesEx($_SERVER["DOCUMENT_ROOT"].$updater->kernelPath."/".$file);
}
?>
