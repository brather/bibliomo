<?
$MESS["SOC_EMPTY_ORDER_ID"] = "Order ID is missing when updating the order.";
$MESS["SOC_EMPTY_USER_ID"] = "User ID is missing when updating the order.";
$MESS["SOC_EMPTY_TYPE"] = "Record type is missing when updating the order.";
$MESS["SOC_BASKET_ADDED"] = "Product added";
$MESS["SOC_BASKET_ADDED_INFO"] = "#QUANTITY# pcs. of #NAME# (##PRODUCT_ID#) has been added to your shopping cart.";
$MESS["SOC_BASKET_REMOVED"] = "Product deleted";
$MESS["SOC_BASKET_REMOVED_INFO"] = "\"#NAME#\" (##PRODUCT_ID#) has been removed.";
$MESS["SOC_BASKET_QUANTITY_CHANGED"] = "Product quantity changed";
$MESS["SOC_BASKET_QUANTITY_CHANGED_INFO"] = "Quantity of \"#NAME#\" (##PRODUCT_ID#) changed to #QUANTITY#.";
$MESS["SOC_BASKET_PRICE_CHANGED"] = "Price change";
$MESS["SOC_BASKET_PRICE_CHANGED_INFO"] = "The price of \"#NAME#\" (##PRODUCT_ID#) changed to #AMOUNT#.";
$MESS["SOC_ORDER_CANCELED"] = "Order canceled";
$MESS["SOC_ORDER_CANCELED_Y"] = "The order was canceled. Reason: #REASON_CANCELED#";
$MESS["SOC_ORDER_CANCELED_N"] = "Uncancel order";
$MESS["SOC_ORDER_RESERVED"] = "Order reserved";
$MESS["SOC_ORDER_RESERVED_Y"] = "Order was reserved.";
$MESS["SOC_ORDER_RESERVED_N"] = "Cancel order reservation";
$MESS["SOC_ORDER_DEDUCTED"] = "Order shipped";
$MESS["SOC_ORDER_DEDUCTED_N"] = "Order shipment was canceled. Reason: #REASON_UNDO_DEDUCTED#";
$MESS["SOC_ORDER_DEDUCTED_Y"] = "Order fulfilled";
$MESS["SOC_ORDER_MARKED"] = "Order problem";
$MESS["SOC_ORDER_MARKED_INFO"] = "Problem description: #REASON_MARKED#";
$MESS["SOC_ORDER_NOT_MARKED"] = "Order problem mark was revoked.";
$MESS["SOC_ORDER_COMMENTED"] = "Order comments";
$MESS["SOC_ORDER_COMMENTED_INFO"] = "#COMMENTS#";
$MESS["SOC_ORDER_STATUS_CHANGED"] = "Order status changed";
$MESS["SOC_ORDER_STATUS_CHANGED_INFO"] = "Status changed to: #STATUS_ID#";
$MESS["SOC_ORDER_DELIVERY_ALLOWED"] = "Shipment approved";
$MESS["SOC_ORDER_DELIVERY_ALLOWED_Y"] = "Shipment approved";
$MESS["SOC_ORDER_DELIVERY_ALLOWED_N"] = "Shipment approval was revoked.";
$MESS["SOC_ORDER_DELIVERY_DOC_CHANGED"] = "Delivery documentation changed";
$MESS["SOC_ORDER_DELIVERY_DOC_CHANGED_INFO"] = "Documentation ref.: #DELIVERY_DOC_NUM#. Date: #DELIVERY_DOC_DATE#";
$MESS["SOC_ORDER_DELIVERY_SYSTEM_CHANGED"] = "Delivery service changed";
$MESS["SOC_ORDER_DELIVERY_SYSTEM_CHANGED_INFO"] = "Changed to: #DELIVERY_ID#";
$MESS["SOC_ORDER_PAYMENT_SYSTEM_CHANGED"] = "Payment system change";
$MESS["SOC_ORDER_PAYMENT_SYSTEM_CHANGED_INFO"] = "Changed to: #PAY_SYSTEM_ID#";
$MESS["SOC_ORDER_PERSON_TYPE_CHANGED"] = "Payer type change";
$MESS["SOC_ORDER_PERSON_TYPE_CHANGED_INFO"] = "Payer type changed to: #PERSON_TYPE_ID#";
$MESS["SOC_ORDER_PAYMENT_VOUCHER_CHANGED"] = "Invoice change";
$MESS["SOC_ORDER_PAYMENT_VOUCHER_CHANGED_INFO"] = "Invoice ref.: #PAY_VOUCHER_NUM#. Date: #PAY_VOUCHER_DATE#";
$MESS["SOC_ORDER_PAYED"] = "Order payment";
$MESS["SOC_ORDER_PAYED_Y"] = "Order paid";
$MESS["SOC_ORDER_PAYED_N"] = "Order payment was revoked";
$MESS["SOC_ORDER_TRACKING_NUMBER_CHANGED"] = "Tracking number change";
$MESS["SOC_ORDER_TRACKING_NUMBER_CHANGED_INFO"] = "#TRACKING_NUMBER#";
$MESS["SOC_ORDER_USER_DESCRIPTION_CHANGED"] = "User comment change";
$MESS["SOC_ORDER_USER_DESCRIPTION_CHANGED_INFO"] = "#USER_DESCRIPTION#";
$MESS["SOC_ORDER_PRICE_DELIVERY_CHANGED"] = "Delivery cost change";
$MESS["SOC_ORDER_PRICE_DELIVERY_CHANGED_INFO"] = "Delivery cost: #AMOUNT#";
$MESS["SOC_ORDER_PRICE_CHANGED"] = "Order total change";
$MESS["SOC_ORDER_PRICE_CHANGED_INFO"] = "Order total: #AMOUNT#";
$MESS["SOC_ORDER_1C_IMPORT"] = "Import from 1C";
$MESS["SOC_ORDER_ADDED"] = "Order created";
$MESS["SOC_ORDER_DELIVERY_REQUEST_SENT_ERROR"] = "Error";
$MESS["SOC_ORDER_DELIVERY_REQUEST_SENT_SUCCESS"] = "Sent successfully";
$MESS["SOC_ORDER_DELIVERY_REQUEST_SENT_ADD_INFO"] = "Additional information";
$MESS["SOC_ORDER_DELIVERY_REQUEST_SENT"] = "Delivery service request";
$MESS["SOC_PAYMENT_PAID"] = "Partial order payment";
$MESS["SOC_PAYMENT_PAID_Y"] = "Payment completed";
$MESS["SOC_PAYMENT_PAID_N"] = "Cancel partial payment";
$MESS["SOC_PAYMENT_CREATE_INFO"] = "Payment system '#PAY_SYSTEM#'";
$MESS["SOC_PAYMENT_CREATE"] = "Create payment";
$MESS["SOC_PAYMENT_DELETE"] = "Delete payment";
$MESS["SOC_PAYMENT_DELETE_INFO"] = "Payment #PAY_SYSTEM# for #SUM# USD deleted";
$MESS["SOC_PAYMENT_PAY_SYSTEM_CHANGE"] = "Edit payment system";
$MESS["SOC_PAYMENT_PAY_SYSTEM_CHANGE_INFO"] = "Payment system #PAY_SYSTEM_ID#";
$MESS["SOC_SHIPMENT_ALLOWED"] = "Delivery approval status";
$MESS["SOC_SHIPMENT_ALLOWED_Y"] = "Delivery approved";
$MESS["SOC_SHIPMENT_ALLOWED_N"] = "Delivery approval revoked";
$MESS["SOC_SHIPMENT_CREATE_INFO"] = "'#DELIVERY_SERVICE#'";
$MESS["SOC_SHIPMENT_CREATE"] = "Create shipment";
$MESS["SOC_SHIPMENT_DEDUCTED"] = "Shipment";
$MESS["SOC_SHIPMENT_DEDUCTED_Y"] = "Shipped";
$MESS["SOC_SHIPMENT_DEDUCTED_N"] = "Shipment canceled";
$MESS["SOC_SHIPMENT_RESERVED"] = "Reserve shipment";
$MESS["SOC_SHIPMENT_RESERVED_Y"] = "Shipment reserved";
$MESS["SOC_SHIPMENT_RESERVED_N"] = "Calcel shipment reservation";
$MESS["SOC_SHIPMENT_PRICE_DELIVERY_CHANGED"] = "Delivery cost change";
$MESS["SOC_SHIPMENT_PRICE_DELIVERY_CHANGED_INFO"] = "Delivery cost #PRICE_DELIVERY#";
$MESS["SOC_SHIPMENT_STATUS_CHANGE"] = "Delivery status";
$MESS["SOC_SHIPMENT_STATUS_CHANGE_INFO"] = "Delivery service #STATUS_ID#";
$MESS["SOC_SHIPMENT_ITEM_BASKET_ADDED_INFO"] = "#QUANTITY# pcs. of #NAME# (##PRODUCT_ID#) has been added to your shopping cart.";
$MESS["SOC_SHIPMENT_ITEM_BASKET_ADDED"] = "New product";
$MESS["SOC_SHIPMENT_ITEM_BASKET_REMOVED_INFO"] = "\"#NAME#\" (##PRODUCT_ID#) has been removed.";
$MESS["SOC_SHIPMENT_ITEM_BASKET_REMOVED"] = "Delete product";
$MESS["SOC_PAYMENT_REMOVED"] = "Delete payment";
$MESS["SOC_PAYMENT_REMOVED_INFO"] = "Partial payment through \"#PAY_SYSTEM_NAME#\" (##ID#) of #SUM# has been deleted.";
$MESS["SOC_SHIPMENT_REMOVED_INFO"] = "Shipment \"#DELIVERY_NAME#\" (##ID#) has been deleted.";
$MESS["SOC_SHIPMENT_REMOVED"] = "Delete shipment";
$MESS["SOC_SHIPMENT_ITEM_QUANTITY_CHANGE_INFO"] = "Quantity of \"#NAME#\" (##PRODUCT_ID#) in shipment (##ORDER_DELIVERY_ID#) has been changed to #QUANTITY#.";
$MESS["SOC_SHIPMENT_ITEM_QUANTITY_CHANGE"] = "Change quantity of product in shipment";
$MESS["SOC_ORDER_LOG_CHANGE"] = "Order changed";
$MESS["SOC_BASKET_LOG_CHANGE"] = "Basket changed";
$MESS["SOC_BASKET_ITEM_UPDATE_LOG_CHANGE"] = "Basket item changed";
$MESS["SOC_PAYMENT_LOG_CHANGE"] = "Payment status changed";
$MESS["SOC_SHIPMENT_LOG_CHANGE"] = "Shipment status changed";
$MESS["SOC_SHIPMENT_ITEM_LOG_CHANGE"] = "Shipment item status changed";
$MESS["SOC_TAX_LOG_CHANGE"] = "Tax changed";
$MESS["SOC_PROPERTY_LOG_CHANGE"] = "Order property changed";
$MESS["SOC_DISCOUNT_LOG_CHANGE"] = "Discount changed";
$MESS["SOC_BASKET_ITEM_ADD_LOG_CHANGE"] = "Basket item added";
$MESS["SOC_BASKET_ITEM_DELETE_BUNDLE_LOG_CHANGE"] = "Basket bundle deleted";
$MESS["SOC_BASKET_ITEM_DELETED_LOG_CHANGE"] = "Basket item deleted";
$MESS["SOC_PAYMENT_ADD_LOG_CHANGE"] = "Funds deposited";
$MESS["SOC_SHIPMENT_ADD_LOG_CHANGE"] = "Shipment method added";
$MESS["SOC_SHIPMENT_ITEM_ADD_LOG_CHANGE"] = "Shipment item added";
$MESS["SOC_PROPERTY_ADD_LOG_CHANGE"] = "Order property added";
$MESS["SOC_PROPERTY_REMOVE_LOG_CHANGE"] = "Order property deleted";
$MESS["SOC_TAX_DELETED_LOG_CHANGE"] = "Tax deleted";
$MESS["SOC_TAX_DUPLICATE_DELETED_LOG_CHANGE"] = "Duplicate tax deleted";
$MESS["SOC_DISCOUNT_SAVED_LOG_CHANGE"] = "Discount saved";
?>