<?
$MESS["SALE_OA_SHIPMENT_STATUS_ERROR"] = "Fehler";
$MESS["SALE_OA_ERROR_HAPPENED"] = "Bei Verarbeitung der Bestellung ist in Fehler aufgetreten";
$MESS["SALE_OA_ERROR_DELIVERY_SERVICE"] = "Einschränkungen treffen beim Lieferservice nicht zu";
$MESS["SALE_OA_ERROR_PAY_SYSTEM_INFO"] = "Nicht korrekte Antwort vom Zahlungssystem";
$MESS["SALE_OA_ERROR_INCORRECT_DATE"] = "Datumformat ist nicht korrekt.";
$MESS["SALE_OA_ERROR_PAYSYSTEM_SERVICE"] = "F?r das Zahlungssystem gelten keine Einschr?nkungen.";
$MESS["SALE_OA_ERROR_CONNECT_PAY_SYS"] = "Fehler bei Verbindung mit dem Zahlungssystem";
$MESS["SALE_OA_ERROR_CANCEL_ORDER"] = "Sie haben nicht gen?gend Rechte, um Bestellung zu stornieren";
$MESS["SALE_OA_ERROR_LOAD_ORDER"] = "Die Bestellung ID # kann nicht gefunden werden";
$MESS["SALE_OA_ERROR_CANCEL_ORDER_ALREADY"] = "Die Bestellung wurde bereits storniert.";
$MESS["SALE_OA_ERROR_CANCEL_ORDER_NOT_YET"] = "Die Bestellung wurde noch nicht storniert.";
$MESS["SALE_OA_ERROR_CREATE_ORDER"] = "Bestellung kann nicht erstellt werden.";
$MESS["SALE_OA_ERROR_ORDER_ID_WRONG"] = "ID der Bestellung ist ung?ltig.";
$MESS["SALE_OA_ERROR_SHIPMENT_ID_WRONG"] = "ID der Auslieferung ist ung?ltig.";
$MESS["SALE_OA_ERROR_PAYMENT_ID_WRONG"] = "ID der Auslieferung ist ung?ltig.";
$MESS["SALE_OA_ERROR_UNMARK_RIGHTS"] = "Sie haben nicht gen?gend Rechte, um die Markierung der Bestellung als problematisch aufzuheben.";
$MESS["SALE_OA_ERROR_LOAD_PAYMENT"] = "Zahlung ID # kann nicht gefunden werden";
$MESS["SALE_OA_ERROR_LOAD_SHIPMENT"] = "Auslieferung ID # kann nicht gefunden werden";
?>