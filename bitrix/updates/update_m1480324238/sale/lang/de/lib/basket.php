<?
$MESS["SALE_BASKET_AVAILABLE_FOR_PURCHASE_QUANTITY"] = "Verfügbare Produktmenge \"#PRODUCT_NAME#\": #AVAILABLE_QUANTITY#";
$MESS["SALE_BASKET_AVAILABLE_FOR_DECREASE_QUANTITY"] = "Verfügbare Produktmenge (zum Abschreiben) \"#PRODUCT_NAME#\": #AVAILABLE_QUANTITY#";
$MESS["SALE_BASKET_ITEM_WRONG_AVAILABLE_QUANTITY"] = "Fehler beim Prüfen der Verfügbarkeit für \"#PRODUCT_NAME#\"";
$MESS["SALE_BASKET_ITEM_WRONG_PRICE"] = "Der Preis für \"#PRODUCT_NAME#\" ist nicht korrekt.";
?>