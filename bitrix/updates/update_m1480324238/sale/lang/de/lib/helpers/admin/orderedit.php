<?
$MESS["SALE_ORDEREDIT_ORDER_PROBLEM"] = "Probleme mit Bestellung";
$MESS["SALE_ORDEREDIT_NAVIGATION"] = "Schnelle Navigation";
$MESS["SALE_ORDEREDIT_NAME_NULL"] = "Nicht definiert";
$MESS["SALE_ORDEREDIT_ERROR_NO_PRODUCTS"] = "Mindestens ein Produkt muss ausgewählt werden";
$MESS["SALE_ORDEREDIT_CLOSE"] = "Löschen";
$MESS["SALE_ORDEREDIT_DISCOUNT_UNKNOWN"] = "unbekannt";
$MESS["SALE_ORDEREDIT_REFRESHING_DATA"] = "Wird aktualisiert...";
$MESS["SALE_ORDEREDIT_NOT_USE"] = "Nicht ausgewählt";
$MESS["SALE_ORDEREDIT_FIX"] = "Panel fixieren";
$MESS["SALE_ORDEREDIT_UNFIX"] = "Panel lösen";
$MESS["SALE_ORDEREDIT_PROFILE_ERROR_SAVE"] = "Fehler beim Speichern des Nutzerprofils.";
$MESS["SALE_ORDEREDIT_BASKET_ITEM_DEL_ERROR"] = "Fehler beim Löschen des Produktes";
$MESS["SALE_ORDEREDIT_UNMARK_ERROR"] = "Fehler beim Aufheben der Markierung der Bestellung als problematisch";
$MESS["SALE_ORDEREDIT_LOCKED"] = "Die Bestellung wurde vor?bergehend vom Kunden #ID# (#DATE#) gesperrt";
?>