<?
$MESS["SALE_EVENT_COMPATIBILITY_WRONG_ORDER"] = "Objekt der Bestellung ist nicht korrekt";
$MESS["SALE_EVENT_COMPATIBILITY_WRONG_SHIPMENT"] = "Objekt der Auslieferung ist nicht korrekt";
$MESS["SALE_EVENT_COMPATIBILITY_WRONG_BASKET"] = "Objekt des Warenkorbs ist nicht korrekt";
$MESS["SALE_EVENT_COMPATIBILITY_WRONG_BASKET_ITEM"] = "Element des Warenkorbs ist nicht korrekt";
?>