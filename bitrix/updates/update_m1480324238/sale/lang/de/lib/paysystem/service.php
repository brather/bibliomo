<?
$MESS["SALE_PS_SERVICE_ORDER_ID_ERROR"] = "ORDER ID nicht korrekt";
$MESS["SALE_PS_SERVICE_PAYMENT_ID_ERROR"] = "PAYMENT ID nicht korrekt";
$MESS["SALE_PS_SERVICE_ORDER_ERROR"] = "Bestellung ##ORDER_ID# existiert nicht";
$MESS["SALE_PS_SERVICE_PAYMENT_ERROR"] = "Teilzahlung ##PAYMENT_ID# existiert nicht";
$MESS["SALE_PS_SERVICE_ORDER_CANCELED"] = "Bestellung ##ORDER_ID# storniert";
?>