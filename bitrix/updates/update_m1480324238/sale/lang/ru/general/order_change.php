<?
$MESS["SOC_EMPTY_ORDER_ID"] = "При сохранении информации об изменении заказа не указан ID заказа.";
$MESS["SOC_EMPTY_USER_ID"] = "При сохранении информации об изменении заказа не указан ID пользователя.";
$MESS["SOC_EMPTY_TYPE"] = "При сохранении информации об изменении заказа не указан тип записи.";
$MESS["SOC_BASKET_ADDED"] = "Добавление товара";
$MESS["SOC_BASKET_ADDED_INFO"] = "Добавлен товар \"#NAME#\" (##PRODUCT_ID#) в количестве #QUANTITY#.";
$MESS["SOC_BASKET_REMOVED"] = "Удаление товара";
$MESS["SOC_BASKET_REMOVED_INFO"] = "Удален товар \"#NAME#\" (##PRODUCT_ID#).";
$MESS["SOC_BASKET_QUANTITY_CHANGED"] = "Изменение количества товара";
$MESS["SOC_BASKET_QUANTITY_CHANGED_INFO"] = "Количество товара \"#NAME#\" (##PRODUCT_ID#) изменено на #QUANTITY#";
$MESS["SOC_BASKET_PRICE_CHANGED"] = "Изменение цены товара";
$MESS["SOC_BASKET_PRICE_CHANGED_INFO"] = "Цена товара \"#NAME#\" (##PRODUCT_ID#) изменена на #AMOUNT#";
$MESS["SOC_ORDER_CANCELED"] = "Отмена заказа";
$MESS["SOC_ORDER_CANCELED_Y"] = "Заказ отменен. Причина: #REASON_CANCELED#";
$MESS["SOC_ORDER_CANCELED_N"] = "Снятие отмены заказа.";
$MESS["SOC_ORDER_RESERVED"] = "Резервирование заказа";
$MESS["SOC_ORDER_RESERVED_Y"] = "Заказ зарезервирован";
$MESS["SOC_ORDER_RESERVED_N"] = "Отмена резервирования заказа";
$MESS["SOC_ORDER_DEDUCTED"] = "Отгрузка заказа";
$MESS["SOC_ORDER_DEDUCTED_N"] = "Отмена отгрузки. Причина: #REASON_UNDO_DEDUCTED#";
$MESS["SOC_ORDER_DEDUCTED_Y"] = "Заказ отгружен";
$MESS["SOC_ORDER_MARKED"] = "Проблема с заказом";
$MESS["SOC_ORDER_MARKED_INFO"] = "Описание проблемы: #REASON_MARKED#";
$MESS["SOC_ORDER_NOT_MARKED"] = "Отметка о проблеме убрана";
$MESS["SOC_ORDER_COMMENTED"] = "Комментарий к заказу";
$MESS["SOC_ORDER_COMMENTED_INFO"] = "#COMMENTS#";
$MESS["SOC_ORDER_STATUS_CHANGED"] = "Изменение статуса заказа";
$MESS["SOC_ORDER_STATUS_CHANGED_INFO"] = "Статус изменен на: #STATUS_ID#";
$MESS["SOC_ORDER_DELIVERY_ALLOWED"] = "Разрешение доставки";
$MESS["SOC_ORDER_DELIVERY_ALLOWED_Y"] = "Доставка разрешена";
$MESS["SOC_ORDER_DELIVERY_ALLOWED_N"] = "Отмена разрешения доставки";
$MESS["SOC_ORDER_DELIVERY_DOC_CHANGED"] = "Изменение документа доставки";
$MESS["SOC_ORDER_DELIVERY_DOC_CHANGED_INFO"] = "Номер документа: #DELIVERY_DOC_NUM#. Дата документа: #DELIVERY_DOC_DATE#";
$MESS["SOC_ORDER_DELIVERY_SYSTEM_CHANGED"] = "Изменение службы доставки";
$MESS["SOC_ORDER_DELIVERY_SYSTEM_CHANGED_INFO"] = "Изменена на: #DELIVERY_ID#";
$MESS["SOC_ORDER_PAYMENT_SYSTEM_CHANGED"] = "Изменение платежной системы";
$MESS["SOC_ORDER_PAYMENT_SYSTEM_CHANGED_INFO"] = "Изменена на: #PAY_SYSTEM_ID#";
$MESS["SOC_ORDER_PERSON_TYPE_CHANGED"] = "Изменение типа плательщика";
$MESS["SOC_ORDER_PERSON_TYPE_CHANGED_INFO"] = "Тип плательщика изменен на: #PERSON_TYPE_ID#";
$MESS["SOC_ORDER_PAYMENT_VOUCHER_CHANGED"] = "Изменение документа оплаты";
$MESS["SOC_ORDER_PAYMENT_VOUCHER_CHANGED_INFO"] = "Номер документа: #PAY_VOUCHER_NUM#. Дата документа: #PAY_VOUCHER_DATE#";
$MESS["SOC_ORDER_PAYED"] = "Оплата заказа";
$MESS["SOC_ORDER_PAYED_Y"] = "Заказ оплачен";
$MESS["SOC_ORDER_PAYED_N"] = "Отмена оплаты заказа";
$MESS["SOC_ORDER_TRACKING_NUMBER_CHANGED"] = "Изменение идентификатора отправления";
$MESS["SOC_ORDER_TRACKING_NUMBER_CHANGED_INFO"] = "#TRACKING_NUMBER#";
$MESS["SOC_ORDER_USER_DESCRIPTION_CHANGED"] = "Изменение комментария пользователя";
$MESS["SOC_ORDER_USER_DESCRIPTION_CHANGED_INFO"] = "#USER_DESCRIPTION#";
$MESS["SOC_ORDER_PRICE_DELIVERY_CHANGED"] = "Изменение стоимости доставки";
$MESS["SOC_ORDER_PRICE_DELIVERY_CHANGED_INFO"] = "Стоимость доставки #AMOUNT#";
$MESS["SOC_ORDER_PRICE_CHANGED"] = "Изменение стоимости заказа";
$MESS["SOC_ORDER_PRICE_CHANGED_INFO"] = "Стоимость заказа изменилась с #OLD_AMOUNT# на #AMOUNT#";
$MESS["SOC_ORDER_1C_IMPORT"] = "Импорт сведений о заказе из 1C";
$MESS["SOC_ORDER_ADDED"] = "Создание заказа";
$MESS["SOC_ORDER_DELIVERY_REQUEST_SENT"] = "Заявка в ТК";
$MESS["SOC_ORDER_DELIVERY_REQUEST_SENT_ERROR"] = "Ошибка";
$MESS["SOC_ORDER_DELIVERY_REQUEST_SENT_SUCCESS"] = "Успешно отправлен";
$MESS["SOC_ORDER_DELIVERY_REQUEST_SENT_ADD_INFO"] = "Дополнительная информация";
$MESS["SOC_PAYMENT_DELETE"] = "Удаление оплаты";
$MESS["SOC_PAYMENT_DELETE_INFO"] = "Оплата \"#PAY_SYSTEM_NAME#\" на сумму #SUM# руб. удалена";
$MESS["SOC_ORDER_UPDATED"] = "Обновление заказа";

$MESS["SOC_PAYMENT_PAID"] = "Оплата заказа";
$MESS["SOC_PAYMENT_PAID_Y"] = "Оплата \"#PAY_SYSTEM_NAME#\" (##ID#) произведена";
$MESS["SOC_PAYMENT_PAID_N"] = "Отмена оплаты \"#PAY_SYSTEM_NAME#\" (##ID#)";
$MESS["SOC_PAYMENT_CREATE_INFO"] = "Создана оплата с платежной системой \"#PAY_SYSTEM_NAME#\"";
$MESS["SOC_PAYMENT_CREATE"] = "Создание оплаты";
$MESS["SOC_PAYMENT_REMOVED"] = "Удаление оплаты";
$MESS["SOC_PAYMENT_REMOVED_INFO"] = "Оплата \"#PAY_SYSTEM_NAME#\" (##ID#) на сумму #SUM# руб. удалена";
$MESS["SOC_PAYMENT_PAY_SYSTEM_CHANGE"] = "Изменение платежной системы";
$MESS["SOC_PAYMENT_PAY_SYSTEM_CHANGE_INFO"] = "Платежная система #PAY_SYSTEM_ID#";

$MESS["SOC_SHIPMENT_ALLOWED"] = "Разрешение доставки";
$MESS["SOC_SHIPMENT_ALLOWED_Y"] = "Доставка разрешена";
$MESS["SOC_SHIPMENT_ALLOWED_N"] = "Отмена разрешения доставки";
$MESS["SOC_SHIPMENT_CREATE_INFO"] = "Создана отгрузка \"#DELIVERY_NAME#\"";
$MESS["SOC_SHIPMENT_CREATE"] = "Создание отгрузки";
$MESS["SOC_SHIPMENT_DEDUCTED"] = "Отгрузка";
$MESS["SOC_SHIPMENT_DEDUCTED_Y"] = "Отгружено";
$MESS["SOC_SHIPMENT_DEDUCTED_N"] = "Отмена отгрузки";
$MESS["SOC_SHIPMENT_RESERVED"] = "Резервирование отгрузки";
$MESS["SOC_SHIPMENT_RESERVED_Y"] = "Отгрузка зарезервирована";
$MESS["SOC_SHIPMENT_RESERVED_N"] = "Отмена резервирования отгрузки";
$MESS["SOC_SHIPMENT_PRICE_DELIVERY_CHANGED"] = "Изменение стоимости доставки";
$MESS["SOC_SHIPMENT_PRICE_DELIVERY_CHANGED_INFO"] = "Стоимость доставки #PRICE_DELIVERY#";
$MESS["SOC_SHIPMENT_STATUS_CHANGE"] = "Статус доставки";
$MESS["SOC_SHIPMENT_STATUS_CHANGE_INFO"] = "Служба доставки #STATUS_ID#";

$MESS["SOC_SHIPMENT_REMOVED_INFO"] = "Удалена отгрузка \"#DELIVERY_NAME#\" (##ID#)";
$MESS["SOC_SHIPMENT_REMOVED"] = "Удаление отгрузки";

$MESS["SOC_SHIPMENT_CANCELED"] = "Отмена отгрузки";
$MESS["SOC_SHIPMENT_CANCELED_Y"] = "Отгрузка отменена. Причина: #REASON_CANCELED#";
$MESS["SOC_SHIPMENT_CANCELED_N"] = "Снятие отмены отгрузки.";

$MESS["SOC_SHIPMENT_MARKED"] = "Проблема с отгрузкой";
$MESS["SOC_SHIPMENT_MARKED_INFO"] = "Описание проблемы: #REASON_MARKED#";


$MESS["SOC_SHIPMENT_ITEM_BASKET_ADDED_INFO"] = "В отгрузку добавлен товар \"#NAME#\" (##PRODUCT_ID#) в количестве #QUANTITY#.";
$MESS["SOC_SHIPMENT_ITEM_BASKET_ADDED"] = "Добавление товара в отгрузку";
$MESS["SOC_SHIPMENT_ITEM_BASKET_REMOVED_INFO"] = "Из отгрузки удален товар \"#NAME#\" (##PRODUCT_ID#).";
$MESS["SOC_SHIPMENT_ITEM_BASKET_REMOVED"] = "Удаление товара из отгрузки";

$MESS["SOC_SHIPMENT_ITEM_QUANTITY_CHANGE_INFO"] = "Количество товара \"#NAME#\" (##PRODUCT_ID#) в отгрузке (##ORDER_DELIVERY_ID#) изменено на #QUANTITY#";
$MESS["SOC_SHIPMENT_ITEM_QUANTITY_CHANGE"] = "Изменение количества товара в отгрузке";


$MESS["SOC_ORDER_LOG_CHANGE"] = "Изменение заказа";
$MESS["SOC_BASKET_LOG_CHANGE"] = "Изменение корзины";

$MESS["SOC_BASKET_ITEM_UPDATE_LOG_CHANGE"] = "Изменение элемента корзины";
$MESS["SOC_PAYMENT_LOG_CHANGE"] = "Изменение оплаты";
$MESS["SOC_SHIPMENT_LOG_CHANGE"] = "Изменение отгрузки";
$MESS["SOC_SHIPMENT_ITEM_LOG_CHANGE"] = "Изменение элемента отгрузки";
$MESS["SOC_TAX_LOG_CHANGE"] = "Изменение налогов";
$MESS["SOC_PROPERTY_LOG_CHANGE"] = "Изменение свойства заказа";
$MESS["SOC_DISCOUNT_LOG_CHANGE"] = "Изменение скидки";

$MESS["SOC_BASKET_ITEM_ADD_LOG_CHANGE"] = "Добавление элемента корзины";
$MESS["SOC_BASKET_ITEM_DELETE_BUNDLE_LOG_CHANGE"] = "Удаление комплекта";
$MESS["SOC_BASKET_ITEM_DELETED_LOG_CHANGE"] = "Удаление элемента корзины";
$MESS["SOC_PAYMENT_ADD_LOG_CHANGE"] = "Добавление оплаты";
$MESS["SOC_SHIPMENT_ADD_LOG_CHANGE"] = "Добавление отгрузки";
$MESS["SOC_SHIPMENT_ITEM_ADD_LOG_CHANGE"] = "Добавление элемента отгрузки";
$MESS["SOC_PROPERTY_ADD_LOG_CHANGE"] = "Добавление свойства заказа";
$MESS["SOC_PROPERTY_REMOVE_LOG_CHANGE"] = "Удаление свойства заказа";
$MESS["SOC_TAX_DELETED_LOG_CHANGE"] = "Удаление налога";
$MESS["SOC_TAX_DUPLICATE_DELETED_LOG_CHANGE"] = "Удаление дубликата налога";
$MESS["SOC_DISCOUNT_SAVED_LOG_CHANGE"] = "Сохранение скидок";
?>