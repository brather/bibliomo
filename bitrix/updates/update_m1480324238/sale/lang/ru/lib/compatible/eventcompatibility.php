<?php

$MESS["SALE_EVENT_COMPATIBILITY_WRONG_ORDER"] = "Получен некорректный объект заказа";
$MESS["SALE_EVENT_COMPATIBILITY_WRONG_SHIPMENT"] = "Получен некорректный объект отгрузки";
$MESS["SALE_EVENT_COMPATIBILITY_WRONG_BASKET"] = "Получен некорректный объект корзины";
$MESS["SALE_EVENT_COMPATIBILITY_WRONG_BASKET_ITEM"] = "Получен некорректный объект элемента корзины";

