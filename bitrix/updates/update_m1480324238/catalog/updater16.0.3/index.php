<?php
if ($updater->CanUpdateDatabase())
{
	if ($updater->TableExists("b_catalog_product"))
	{
		if (!$DB->Query("select AVAILABLE from b_catalog_product where 1=0", true))
		{
			$updater->Query(array(
				'MYSQL' => "alter table b_catalog_product add AVAILABLE char(1) null",
				'MSSQL' => "alter table B_CATALOG_PRODUCT add AVAILABLE char(1) null",
				'ORACLE' => "alter table B_CATALOG_PRODUCT add AVAILABLE CHAR(1 CHAR) NULL"
			));
		}

		if (!$DB->Query("select BUNDLE from b_catalog_product where 1=0", true))
		{
			$updater->Query(array(
				'MYSQL' => "alter table b_catalog_product add BUNDLE char(1) null",
				'MSSQL' => "alter table B_CATALOG_PRODUCT add BUNDLE char(1) null",
				'ORACLE' => "alter table B_CATALOG_PRODUCT add BUNDLE CHAR(1 CHAR) NULL"
			));
		}
		if ($DB->Query("select BUNDLE from b_catalog_product where 1=0", true))
		{
			$updater->Query(array(
				'MYSQL' => "update b_catalog_product set BUNDLE = 'N'",
				'MSSQL' => "update B_CATALOG_PRODUCT set BUNDLE = 'N'",
				'ORACLE' => "update B_CATALOG_PRODUCT set BUNDLE = 'N'"
			));
			$updater->Query(array(
				"MySQL" => "update b_catalog_product CP
					inner join b_catalog_product_sets CPS on CPS.OWNER_ID = CP.ID and CPS.ITEM_ID = CPS.OWNER_ID and CPS.TYPE = 2
					set CP.BUNDLE = 'Y'",
				"MSSQL" => "update B_CATALOG_PRODUCT set BUNDLE = 'Y'
					where ID in (select CPS.OWNER_ID from B_CATALOG_PRODUCT_SETS CPS where CPS.ITEM_ID = CPS.OWNER_ID and CPS.TYPE = 2)",
				"ORACLE" => "update B_CATALOG_PRODUCT
					set BUNDLE = 'Y' where ID in (select CPS.OWNER_ID from B_CATALOG_PRODUCT_SETS CPS
					where CPS.ITEM_ID = CPS.OWNER_ID and CPS.TYPE = 2)",
			));
		}
	}

	if ($updater->TableExists("b_catalog_price"))
	{
		if (!$DB->Query("select PRICE_SCALE from b_catalog_price where 1=0", true))
		{
			$updater->Query(array(
				'MYSQL' => "alter table b_catalog_price add PRICE_SCALE decimal(26,12) null",
				'MSSQL' => "alter table B_CATALOG_PRICE add PRICE_SCALE decimal(26,12) NULL",
				'ORACLE' => "alter table B_CATALOG_PRICE add PRICE_SCALE NUMBER(26,12) NULL"
			));
		}
		if ($DB->Query("select PRICE_SCALE from b_catalog_price where 1=0", true))
		{
			$updater->Query(array(
				"MYSQL" => "update b_catalog_price CPR
					INNER JOIN b_catalog_currency CC ON CC.CURRENCY = CPR.CURRENCY
					set CPR.PRICE_SCALE = CPR.PRICE*CC.CURRENT_BASE_RATE",
				"MSSQL" => "update B_CATALOG_PRICE set B_CATALOG_PRICE.PRICE_SCALE = (
					select B_CATALOG_PRICE.PRICE*CC.CURRENT_BASE_RATE from B_CATALOG_CURRENCY CC
					where CC.CURRENCY = B_CATALOG_PRICE.CURRENCY)",
				"ORACLE" => "update B_CATALOG_PRICE CPR
					set CPR.PRICE_SCALE = (select CPR.PRICE*CC.CURRENT_BASE_RATE from B_CATALOG_CURRENCY CC
					where CC.CURRENCY = CPR.CURRENCY)"
			));
			if (!\Bitrix\Main\Application::getConnection()->isIndexExists('b_catalog_price', array('PRICE_SCALE')))
			{
				$updater->Query(array(
						'MYSQL' => "create index IXS_CAT_PRICE_SCALE on b_catalog_price(PRICE_SCALE)",
						'MSSQL' => "create index IXS_CAT_PRICE_SCALE on B_CATALOG_PRICE(PRICE_SCALE)",
						'ORACLE' => "create index IXS_CAT_PRICE_SCALE on B_CATALOG_PRICE(PRICE_SCALE)"
				));
			}
		}
	}

	if ($updater->TableExists("b_catalog_store"))
	{
		if ($DB->Query("select XML_ID from b_catalog_store where 1=0", true))
		{
			$updater->Query(array(
				'MYSQL' => "alter table b_catalog_store modify XML_ID VARCHAR(255)",
				'MSSQL' => "alter table B_CATALOG_STORE alter column XML_ID VARCHAR(255)",
				'ORACLE' => "alter table B_CATALOG_STORE modify XML_ID VARCHAR2(255 CHAR)"
			));
		}
	}
}

if (\Bitrix\Main\ModuleManager::isModuleInstalled('catalog'))
{
	if ($updater->CanUpdateDatabase())
	{
		RegisterModuleDependences('iblock', 'OnIBlockElementAdd', 'catalog', '\Bitrix\Catalog\Product\Sku', 'handlerIblockElementAdd');
		RegisterModuleDependences('iblock', 'OnAfterIBlockElementAdd', 'catalog', '\Bitrix\Catalog\Product\Sku', 'handlerAfterIblockElementAdd');
		RegisterModuleDependences('iblock', 'OnIBlockElementUpdate', 'catalog', '\Bitrix\Catalog\Product\Sku', 'handlerIblockElementUpdate');
		RegisterModuleDependences('iblock', 'OnAfterIBlockElementUpdate', 'catalog', '\Bitrix\Catalog\Product\Sku', 'handlerAfterIblockElementUpdate');
		RegisterModuleDependences('iblock', 'OnIBlockElementDelete', 'catalog', '\Bitrix\Catalog\Product\Sku', 'handlerIblockElementDelete');
		RegisterModuleDependences('iblock', 'OnAfterIBlockElementDelete', 'catalog', '\Bitrix\Catalog\Product\Sku', 'handlerAfterIblockElementDelete');
		RegisterModuleDependences('iblock', 'OnIBlockElementSetPropertyValues', 'catalog', '\Bitrix\Catalog\Product\Sku', 'handlerIblockElementSetPropertyValues');
		RegisterModuleDependences('iblock', 'OnAfterIBlockElementSetPropertyValues', 'catalog', '\Bitrix\Catalog\Product\Sku', 'handlerAfterIBlockElementSetPropertyValues');
		RegisterModuleDependences('search', 'BeforeIndex', 'catalog', '\Bitrix\Catalog\SearchHandlers', 'onBeforeIndex');

		$eventManager = \Bitrix\Main\EventManager::getInstance();
		$eventManager->registerEventHandler('currency', 'onAfterUpdateCurrencyBaseRate', 'catalog', '\Bitrix\Catalog\Product\Price', 'handlerAfterUpdateCurrencyBaseRate');
	}

	$updater->CopyFiles("install/components", "components");
	$updater->CopyFiles("install/js", "js");
	$updater->CopyFiles("install/tools", "tools");
	$updater->CopyFiles("install/panel", "panel");

	if (!\Bitrix\Main\ModuleManager::isModuleInstalled('bitrix24'))
	{
		$notifyId = 'CATALOG_16';
		$rsAdminNotify = CAdminNotify::GetList(array(), array('MODULE_ID' => 'catalog', 'TAG' => $notifyId));
		if ($rsAdminNotify)
		{
			if ($arAdminNotify = $rsAdminNotify->Fetch())
				CAdminNotify::DeleteByTag($notifyId);
			unset($arAdminNotify);
		}
		unset($rsAdminNotify);

		$messages = array();
		$firstLang = '';
		$defaultLang = '';
		$languageIterator = \Bitrix\Main\Localization\LanguageTable::getList(array(
			'select' => array('ID', 'DEF'),
			'filter' => array('ACTIVE' => 'Y')
		));
		while ($oneLanguage = $languageIterator->fetch())
		{
			if ($oneLanguage['DEF'] == 'Y')
				$defaultLang = $oneLanguage['ID'];
			if ($firstLang == '')
				$firstLang = $oneLanguage['ID'];;
			$languageId = $oneLanguage['ID'];
			\Bitrix\Main\Localization\Loc::loadLanguageFile(
				__FILE__,
				$languageId
			);
			$messages[$languageId] = \Bitrix\Main\Localization\Loc::getMessage(
				'CATALOG_16_CATALOG_AVAILABLE',
				array(
						'#LINK#' => '/bitrix/admin/settings.php?lang='.$languageId.'&mid=catalog&mid_menu=1',
				),
				$languageId
			);
		}
		unset($oneLanguage, $languageIterator);
		if ($defaultLang == '')
			$defaultLang = $firstLang;

		if (!empty($messages[$defaultLang]))
		{
			$fields = array(
				'MESSAGE' => $messages[$defaultLang],
				'TAG' => $notifyId,
				'MODULE_ID' => 'catalog',
				'ENABLE_CLOSE' => 'Y',
				'PUBLIC_SECTION' => 'N',
				'LANG' => $messages
			);
			CAdminNotify::Add($fields);
		}
		unset($defaultLang, $firstLang, $messages);
	}
}
if ($updater->CanUpdateKernel())
{
	$arToDelete = array(
		"modules/catalog/install/components/bitrix/catalog.viewed.products.mail/",
		"components/bitrix/catalog.viewed.products.mail/",
	);
	foreach($arToDelete as $file)
		CUpdateSystem::DeleteDirFilesEx($_SERVER["DOCUMENT_ROOT"].$updater->kernelPath."/".$file);
}