<?
define("NEED_AUTH", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

if (isset($_REQUEST["backurl"]) && strlen($_REQUEST["backurl"])>0)
	LocalRedirect($_REQUEST["backurl"]);

// Если backurl пуст - тогда редиректим на главную
LocalRedirect('/');

$APPLICATION->SetTitle("Авторизация");
?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>