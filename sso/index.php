<?
	require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

	$bDesignMode = $APPLICATION->GetShowIncludeAreas() && is_object($USER) && $USER->IsAdmin();

	if($bDesignMode)
		require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

	$APPLICATION->IncludeComponent(
		"neb:sso", 
		".default", 
		array(
			"HOURS_LIMIT" => "24",
			"SEF_MODE" => "Y",
			"SEF_FOLDER" => "/sso/",
			"SEF_URL_TEMPLATES" => array(
				"list" => "/",
				"auth" => "auth/",
				"check_token" => "check_token/",
			)
		),
		false
	);
	
	if($bDesignMode)
		require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>