<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>
<?
	//require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
	//global $APPLICATION;
	$APPLICATION->SetTitle("Тест API");
	CJSCore::Init('jquery');
?>
<script type="text/javascript">
	$(document).ready(function(){
		$('#api_dir').change(function(){
			$('form#ftest').attr('action', $(this).val());
		});

		$('#api_method').change(function(){
			$('form#ftest').attr('method', $(this).val());
		});

		$('form#ftest').submit(function(){
			$.ajax({
				url: 		$(this).attr('action'),
				//method: 	$(this).attr('method'),
				type: 		$(this).attr('method'),
				//dataType: 	'json',
				data: 		$(this).serialize(),
			}).always(function(data){
				if(typeof(data.responseJSON) != 'undefined')
					$('#response').html(JSON.stringify(data.responseJSON, null, 2));
				else
					$('#response').html(JSON.stringify(data, null, 2));
			});

			return false;
		});
	});
</script>

<select name="api_dir" id="api_dir">
	<option>Выберите api</option>
	<option value="/rest_api/bookmark/">Закладки</option>
	<option value="/rest_api/notes/">Заметки</option>
	<option value="/rest_api/favorites/">Избранное</option>
</select>
<select id="api_method">
	<option>Выберите REQUEST_METHOD</option>
	<option value="GET">GET</option>
	<option value="POST">POST</option>
	<option value="PUT">PUT</option>
	<option value="DELETE">DELETE</option>
</select>

<form action="" method="" id="ftest">
	<label>Token: <input name="token" value="" /></label>
	<?
		if(empty($_REQUEST['del'])){
		?>
		<label>BookID: <input name="book_id" value="" /></label>
		<label>NumPage: <input name="num_page" value="" /></label>
		<label>Text: <textarea name="text"></textarea></label>
		<?
		}
	?>
	<label>ID: <input name="ID" value="" /></label>

	<input type="submit" />
</form>
<br/>
<span>Ответ: <span id="response"></span></span>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>