<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule('nota.exalead');

use Nota\Exalead\SearchQuery;
use Nota\Exalead\SearchClient;
error_reporting(E_ERROR);


//8.1. Число изданий по библиотеке на заданную дату.
/*$query = new SearchQuery('all#'.rand(1, 1000));

$date = date('Y/m/d', AddToTimeStamp(array('MM' => -1)));
//$query = new SearchQuery('all&dateindex<=' . $date);
$query->setParam('dateindex=', $date);
//$query = new SearchQuery('dateindex>='.$date);

$arParam = $query->getParameters();
$query->setQueryType('raw');

//pre($arParam, true);

$client = new SearchClient();
$result = $client->getResult($query);

$xml = simplexml_load_string($result);
//pre($xml, true);
$attr = $xml->attributes();
preg_match("/nbdocs=(\\d+),/", $attr['ellql'], $book_count);
$book_count = $book_count[1];

pre($book_count, true);*/


//Список названий библиотек
/*$query = new SearchQuery();
$query->setQuery('#all');
$query->setParam('sl', 'sl_statistic');
$query->setParam('use_logic_hit_metas', 'false');
$query->setParam('use_logic_facets', 'library');
$query->setParam('rng', rand(1, 1000));
//$query->setPageLimit(10);
$query->setQueryType('raw');

$query->port = '10010';

$arParam = $query->getParameters();

//pre($arParam, true);

$client = new SearchClient();
$result = $client->getResult($query);

$xml = simplexml_load_string($result);
//pre($xml, true);
foreach($xml->groups->AnswerGroup->categories->Category as $category){
    $attr = $category->attributes();
    pre($attr['title'], true);
}*/

//12.11 Количество издательств за дату по библиотекам.
//11.1. Количество авторов за дату по библиотекам.
$query = new SearchQuery();
//$query->setQuery('#all');
$query->setParam('q', 'library:"Донская государственная публичная библиотека" OR library:"Российская государственная детская библиотека (РГДБ)"');
$query->setParam('sl', 'sl_statistic');
$query->setParam('use_logic_hit_metas', 'false');
$query->setParam('use_logic_facets', 'publisher'); //кол-во издательств
//$query->setParam('use_logic_facets', 'authorbook'); //кол-во авторов
$query->setParam('rng', rand(1, 1000));
//$query->setPageLimit(10);
$query->setQueryType('raw');

$query->port = '10010';

$arParam = $query->getParameters();

$client = new SearchClient();
$result = $client->getResult($query);

$xml = simplexml_load_string($result);
echo count($xml->groups->AnswerGroup->categories->Category);

/*$query = new SearchQuery();
//$query->setQuery('#all');
$query->setParam('q', 'library:"Российская государственная детская библиотека (РГДБ)"');
$query->setParam('sl', 'sl_statistic');
$query->setParam('use_logic_hit_metas', 'false');
$query->setParam('use_logic_facets', 'authorbook');
//$query->setParam('use_logic_facets', 'library');
$query->setParam('rng', rand(1, 1000));
//$query->setPageLimit(10);
$query->setQueryType('raw');

$query->port = '10010';

$arParam = $query->getParameters();

//pre($arParam, true);

$client = new SearchClient();
$result = $client->getResult($query);

echo $result;
//pre($result, true);*/