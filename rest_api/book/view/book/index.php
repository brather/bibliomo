<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>
<?
//require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
//global $APPLICATION;
$APPLICATION->SetTitle("Книги. Просмотры.");
?><?$APPLICATION->IncludeComponent(
    "rest:book.view.book",
    "",
    array(
        "SEF_MODE" => "Y",
        "SEF_FOLDER" => "/rest_api/book/view/book/",
        "CACHE_TYPE" => "N",
        "CACHE_TIME" => "3600",
        "DOCUMENTATION" => "Y",
        "SERVICE_NAME" => "Книги. Просмотры.",
        "SEF_URL_TEMPLATES" => array(
            "processRequest" => "",
        )
    ),
    false
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>