<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$PrevId = '';
global $USER;
$idUser = $USER->GetId();
$arSelect = Array("ID", "IBLOCK_ID", "NAME", "PROPERTY_USER", "PROPERTY_DEFAULT");
$arFilter = Array("IBLOCK_ID" => IBLOCK_ID_IMPORT_PROFILE, "ACTIVE" => "Y", "PROPERTY_USER" => $idUser, "PROPERTY_DEFAULT_VALUE" => "Y");
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);

$arOption = array();
while($ob = $res->GetNextElement()){
    $arFields = $ob->GetFields();
    $PrevId = $arFields["ID"];
}
if($PrevId){
    $ELEMENT_ID = $PrevId;
    $PROPERTY_CODE = "DEFAULT";
    $PROPERTY_VALUE = "";
    CIBlockElement::SetPropertyValuesEx($ELEMENT_ID, false, array($PROPERTY_CODE => $PROPERTY_VALUE));
}
if($_GET['idDef']){
    $ELEMENT_ID = $_GET['idDef'];
    $PROPERTY_CODE = "DEFAULT";
    $PROPERTY_VALUE = "29";
    CIBlockElement::SetPropertyValuesEx($ELEMENT_ID, false, array($PROPERTY_CODE => $PROPERTY_VALUE));
}
