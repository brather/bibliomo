<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
global $APPLICATION;
$APPLICATION->SetTitle("Изменения сохранены");
?>
<section class="innersection innerwrapper searchempty clearfix">
    <div class="b-mainblock left">
        <div class="b-plaintext">
            <h2>Удаление заказа</h2>
            <div class="b-form b-form_common b-feedbackform">
                <div class="feedback-contacts" style="height: 240px;" id="resultDel">
                    <h3>Вы действительно хотите удалить заказ?</h3>
                    <div class="feedback-address">
                        <span style="display:inline-block; color: #999;padding-left:0px">Автор: </span>
                        <?=$_GET['author'];?>
                    </div>
                    <div class="feedback-address" style="margin-top: 20px">
                        <span style="display:inline-block; color: #999;padding-left:0px">Название книги: </span>
                        <?=$_GET['title'];?>
                    </div>
                    <div style="float: right">
                    <button rel="<?=$_GET['order_id'];?>" class="formbutton bt_typelt DelOrder" value="1" type="submit" name="web_form_submit">Удалить</button>
                    </div>
                </div>
            </div>
        </div><!-- /.b-plaintext -->
    </div>
</section>