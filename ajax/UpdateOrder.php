<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

OrderTable::UpdateOrder($_GET['id'], $_GET['revoked'], $_GET['desire']);
$APPLICATION->IncludeComponent(
    "neb:orders",
    ".default",
    array(
        "BOOK_IBLOCK_ID" => "10",
        "BOOK_IBLOCK_TYPE" => "library"
    ),
    false
);?>