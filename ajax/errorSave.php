<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
global $APPLICATION;
$APPLICATION->SetTitle("Ошибка");
?>
<section class="innersection innerwrapper searchempty clearfix">
    <div class="b-mainblock left">
        <div class="b-plaintext">
            <h2>Ошибка</h2>
            <div class="b-form b-form_common b-feedbackform">
                <div class="feedback-contacts">
                    <h3>Вы не можете изменить заказ, который уже одобрен</h3>
                </div>
            </div>
        </div><!-- /.b-plaintext -->
    </div>
</section>