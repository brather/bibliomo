<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
if (!empty($_GET['query'])) {
    $filter = Array
    (
        "ACTIVE" => "Y",
        "=EMAIL" => $_GET['query'],
    );
    $order = array('email' => 'desc');
    $tmp = 'sort';
    $rsUsers = CUser::GetList($order, $tmp, $filter); // выбираем пользователей
    $is_filtered = $rsUsers->is_filtered; // отфильтрована ли выборка ?
    $arSearch = array();
    $key = 1;
    while ($rsUsers->NavNext(true, "f_") && $key <= 1):
        $arSearch['EMAIL'] = $f_EMAIL;
        $arSearch['HREF'] = '/forum/user/' . $f_ID . '/';
        $arSearch['NAME'] = $f_NAME.' '.$f_LAST_NAME;
        $arSearch['ID'] = $f_ID;
        $key++;
    endwhile;
    if(strlen($arSearch['HREF']) > 0){
        echo json_encode($arSearch);
    }
    else{
        echo 'ERROR';
    }

}
exit();