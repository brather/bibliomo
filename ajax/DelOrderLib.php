<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$_REQUEST['uid'] = $_GET['uid'];
OrderTable::DelOrderLib($_GET['id']);
$APPLICATION->IncludeComponent(
    "neb:orders",
    "library",
    array(
        "BOOK_IBLOCK_ID" => "10",
        "BOOK_IBLOCK_TYPE" => "library",
    ),
    false
);?>