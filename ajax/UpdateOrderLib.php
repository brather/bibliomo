<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$_REQUEST['uid'] = $_GET['uid'];
OrderTable::UpdateOrderLib($_GET['id'], $_GET['revoked'], $_GET['date_issue'], $_GET['date_return'], $_GET['comment'], $_GET['date']);
$APPLICATION->IncludeComponent(
    "neb:orders",
    "library",
    array(
        "BOOK_IBLOCK_ID" => "10",
        "BOOK_IBLOCK_TYPE" => "library",
    ),
    false
);?>