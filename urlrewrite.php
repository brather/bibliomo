<?
$arUrlRewrite = array(
	array(
		"CONDITION" => "#^/online/([\\.\\-0-9a-zA-Z]+)(/?)([^/]*)#",
		"RULE" => "alias=\$1",
		"ID" => "bitrix:im.router",
		"PATH" => "/desktop_app/router.php",
	),
	array(
		"CONDITION" => "#^/bitrix/services/ymarket/#",
		"RULE" => "",
		"ID" => "",
		"PATH" => "/bitrix/services/ymarket/index.php",
	),
	array(
		"CONDITION" => "#^/rest_api/favorites/#",
		"RULE" => "",
		"ID" => "rest:favorites",
		"PATH" => "/rest_api/favorites/index.php",
	),
	array(
		"CONDITION" => "#^/online/(/?)([^/]*)#",
		"RULE" => "",
		"ID" => "bitrix:im.router",
		"PATH" => "/desktop_app/router.php",
	),
	array(
		"CONDITION" => "#^/rest_api/bookmark/#",
		"RULE" => "",
		"ID" => "rest:bookmark",
		"PATH" => "/rest_api/bookmark/index.php",
	),
	array(
		"CONDITION" => "#^/rest_api/notes/#",
		"RULE" => "",
		"ID" => "rest:notes",
		"PATH" => "/rest_api/notes/index.php",
	),
	array(
		"CONDITION" => "#^/registration/#",
		"RULE" => "",
		"ID" => "neb:registration",
		"PATH" => "/registration/index.php",
	),
	array(
		"CONDITION" => "#^/collections/#",
		"RULE" => "",
		"ID" => "neb:collections",
		"PATH" => "/collections/index.php",
	),
	array(
		"CONDITION" => "#^/catalog_bbk/#",
		"RULE" => "",
		"ID" => "neb:catalog_bbk",
		"PATH" => "/catalog_bbk/index.php",
	),
	array(
		"CONDITION" => "#^/en/forum/#",
		"RULE" => "",
		"ID" => "bitrix:forum",
		"PATH" => "/en/forum/index.php",
	),
	array(
		"CONDITION" => "#^/catalog/#",
		"RULE" => "",
		"ID" => "neb:search.page.detail",
		"PATH" => "/catalog/index.php",
	),
	array(
		"CONDITION" => "#^/library/#",
		"RULE" => "",
		"ID" => "neb:library",
		"PATH" => "/library/index.php",
	),
	array(
		"CONDITION" => "#^/profile/#",
		"RULE" => "",
		"ID" => "neb:profile",
		"PATH" => "/profile/index.php",
	),
	array(
		"CONDITION" => "#^/forum/#",
		"RULE" => "",
		"ID" => "bitrix:forum",
		"PATH" => "/forum/index.php",
	),
	array(
		"CONDITION" => "#^/sso/#",
		"RULE" => "",
		"ID" => "neb:sso",
		"PATH" => "/sso/index.php",
	),
);

?>