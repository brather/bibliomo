<?
define('STATIC_PAGE', true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Часто задаваемые вопросы");
?><p>
	 Прежде чем заполнить форму с вашим вопросом, предлагаем изучить наиболее часто задаваемые вопросы и ответы на них от наших специалистов.
</p>
<h3>Работа с изданиями</h3>
<p>
 <b><span style="background-image: initial; background-color: #fbfcfd;">Вопрос:</span></b><span style="background-image: initial; background-color: #fbfcfd;"><b>&nbsp;Как прочитать книгу?</b></span>
</p>
<p>
 <i>Ответ: Для поиска книги с оцифрованным текстом необходимо установить галочку «Искать по текстам&nbsp;изданий» (установлена по умолчанию). Тогда в результатах поиска&nbsp;</i><span style="background-image: initial; background-color: #fbfcfd;"><i>под каждой книгой будет расположена кнопка «Читать».</i></span>
</p>
<p>
 <i>Издания, находящиеся в открытом доступе, можно читать на сайте с любого IP-адреса.</i>
</p>
 <b><span style="background-image: initial; background-color: #fbfcfd;"><br>
 </span></b><b><span style="background-image: initial; background-color: #fbfcfd;"><br>
 </span></b><b><span style="background-image: initial; background-color: #fbfcfd;">Вопрос:</span></b><span style="background-image: initial; background-color: #fbfcfd;"><b>&nbsp;Как скачать книгу?</b></span><br>
<p>
</p>
<p>
 <i>Ответ: Скачать можно только открытые издания, не защищенные авторским правом. Издания, защищенные авторским правом, скачивать нельзя.</i>&nbsp;<i>Для скачивания открытых изданий необходимо открыть книгу на просмотр (кнопка «Читать»). На странице чтения книги под названием и автором книги расположена кнопка «Скачать». При нажатии на нее на Ваш компьютер будет загружена pdf-версия выбранного издания.</i>
</p>
<p>
 <br>
</p>
<p>
 <b><span style="background-image: initial; background-color: #fbfcfd;">Вопрос:</span></b><span style="background-image: initial; background-color: #fbfcfd;"><b>&nbsp;Возможен ли просмотр книг на планшетах?</b></span>
</p>
<p>
</p>
<p>
 <i>Ответ:&nbsp;</i><span style="background-image: initial; background-color: #fbfcfd;"><i>На планшетах через браузер возможен просмотр книг, не защищенных авторским правом. В ближайшее время будут доступны приложения для IOS и Android для просмотра книг, защищенных авторским правом.</i></span>
</p>
 <br>
<h3>Вопросы регистрации на портале ЕИСУБФ МО</h3>
<p>
 <strong>Вопрос: Для пользования фондами библиотеки, мне нужно проходить регистрацию?</strong>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p align="justify">
 <em>Ответ: 80% фонда ЕИСУБФ МО (за исключением диссертаций)&nbsp;</em><em><span style="color: #000000;">предоставляется пользователям без каких-либо ограничений и регистраций. Однако, для просмотра фонда, размещенного с учетом соглашений о соблюдении авторских прав, регистрация требуется. Зарегистрированным пользователям предоставляется возможность использовать личный кабинет и входящие в него дополнительные сервисы</span>.</em>
</p>
<p>
</p>
<p>
 <em><br>
 </em>
</p>
<p>
 <b><span style="background-image: initial; background-color: #fbfcfd;">Вопрос:</span></b><span style="background-image: initial; background-color: #fbfcfd;"><b>
	Зарегистрировался по упрощенной схеме, но в профиле стоит ограниченный доступ, что это значит?</b></span>
</p>
<p>
</p>
<p>
 <i>Ответ: </i>
</p>
<p align="justify">
 <i>Это значит, что нет доступа к закрытым изданиям, защищенным авторским правом. Для получения доступа к закрытым изданиям необходимо:</i>
</p>
<p>
</p>
 <i>&nbsp;-&nbsp;пройти полную регистрацию на портале ЕИСУБФ МО;</i><br>
 <i>&nbsp;- <a href="/distribs/setup.exe">установить программу просмотра</a></i><i>.</i><br>
<p>
	 &nbsp;
</p>
<p>
 <b><span style="background-image: initial; background-color: #fbfcfd;">Вопрос:</span></b><span style="background-image: initial; background-color: #fbfcfd;"><b>
	Чем обосновано требование такого количества личных данных при регистрации? Вплоть до скана паспорта?</b></span>
</p>
<p>
 <i>Ответ: </i><i>Для чтения открытых изданий не требуется регистрация. Для чтения закрытых изданий, </i><span style="background-image: initial; background-color: #fbfcfd;"><i>ограниченных авторским правом, необходимо быть читателем библиотеки. Для этого требуется пройти полную регистрацию с предоставлением персональных данных в соответствии с законодательством.</i></span>
</p>
<p>
 <br>
</p>
<p>
 <b><span style="background-image: initial; background-color: #fbfcfd;">Вопрос:</span></b><span style="background-image: initial; background-color: #fbfcfd;"><b>
	После прохождения полной регистрации или регистрации с использованием номера читательского билета РГБ*, не получен доступ к закрытым изданиям, защищенным авторским правом. </b></span><span style="border-color: windowtext; border-width: 1pt; background-image: initial; background-color: #fbfcfd;"><b>П</b></span><span style="background-image: initial; background-color: #fbfcfd;"><b>ри попытке чтения книги появляется сообщение, что для чтения необходимо пройти полную регистрацию.</b></span>
</p>
<p>
</p>
<p>
 <i>Ответ: </i><span style="background-image: initial; background-color: #fbfcfd;"><i>Доступ к изданиям предоставляется оператором ЕИСУБФ МО, Российской Государственной Библиотекой. В ближайшие дни будет решен вопрос предоставления доступа в соответствии с законодательством об авторском праве для читателей ЕИСУБФ МО в РГБ. Сообщение об этом будет размещено на сайте.</i></span>
</p>
<p>
</p>
<p>
</p>
<p>
 <i>*При выполнении регистрации на портале ЕИСУБФ МО «как читатель РГБ» следует соблюдать следующие правила:&nbsp;</i>
</p>
<p>
</p>
 <i> </i>
<p>
 <i> </i>
</p>
 <i> </i>
<p>
 <i>
	- номер читательского билета РГБ вводится полностью без пробелов, включая номер зала (цифра, - выделенная цветом, например, синяя или красная); </i>
</p>
 <i> </i>
<p>
	 -&nbsp;<i>в поле «Пароль» указывается значение действующего пароля от личного кабинета РГБ. </i>
</p>
 &nbsp;
<p>
</p>
<p>
 <b><span style="background-image: initial; background-color: #fbfcfd;">Вопрос</span></b><span style="background-image: initial; background-color: #fbfcfd;"><b>: Необходимо ли регистрироваться как «читатель РГБ» для того, чтобы читать книги?</b></span>
</p>
<p>
</p>
 <b> </b>
<p>
 <span style="background-image: initial; background-color: #fbfcfd;"><i>Ответ</i></span><span style="background-image: initial; background-color: #fbfcfd;"><i>: Для чтения открытых изданий регистрация не требуется.</i></span><i> </i>
</p>
 <i> </i>
<p>
 <span style="background-image: initial; background-color: #fbfcfd;"><i>Для чтения закрытых изданий, защищенных авторским правом, требуется пройти любую полную регистрацию:</i></span>
</p>
<p>
 <i>&nbsp;-&nbsp;регистрацию для обычных пользователей;</i>
</p>
<p>
 <i>&nbsp;-&nbsp;регистрацию для читателей РГБ;</i>
</p>
<p>
 <i>&nbsp;-&nbsp;регистрацию через ЕСИА.</i>
</p>
<p>
</p>
<p>
 <i> </i>
</p>
 <i> </i>
<p>
 <i> </i><span style="background-image: initial; background-color: #fbfcfd;"><i>Если читатель регистрируется как «читатель РГБ», то доступ к закрытым изданиям будет предоставлен сразу. В остальных случаях выполняется проверка данных анкеты читателя.</i></span>
</p>
 <br>
<h3>Читательский билет ЕИСУБФ МО</h3>
<p>
 <strong>Вопрос: Действует ли мой читательский билет ЕИСУБФ МО в нашей областной библиотеке?</strong>
</p>
<p>
</p>
<p>
 <em>Ответ: Если областная библиотека является участником ЕИСУБФ МО, то вы можете получить читательский билет библиотеки при личном посещении и по упрощенной процедуре. Для этого вам необходимо иметь паспорт и знать ваш номер читательского билета ЕИСУБФ МО.</em>
</p>
 <br>
<h3></h3>
<p>
</p>
<p>
</p>
<h3>Разное</h3>
<p>
 <strong>Вопрос: Какая скорость соединения с Internet требуется, чтобы при просмотре книги компьютер не "тормозил"?</strong>
</p>
<p>
 <em>Ответ: Задержки при листании книги могут быть вызваны не только скоростью соединения, но и конфигурацией компьютера. При работе на современном компьютере вы не будете испытывать затруднений с просмотром при скорости соединения от 2 Мбит/сек и выше.</em><br>
</p>
<p>
 <em><br>
 </em>
</p>
<p>
 <b><span style="background-image: initial; background-color: #fbfcfd;">Вопрос:</span></b><span style="background-image: initial; background-color: #fbfcfd;"><b>
	Как можно предоставить электронной библиотеке личный архив отсканированных книг?</b></span>
</p>
<p>
</p>
 <b> </b>
<p>
 <i>Ответ: Если Вы являетесь автором этих книг, то необходимо зарегистрироваться на портале ЕИСУБФ МО с правами правообладателя и заключить договор на безвозмездное предоставление своих произведений. После этого на портале ЕИСУБФ МО будет предоставлена возможность самостоятельного размещения произведений. </i>
</p>
 <i> </i>
<p>
 <i>
	Если Вы не являетесь автором отсканированных книг, то обратитесь в </i><span style="background-image: initial; background-color: #fbfcfd;"><i>Отдел книги и чтения РГБ (адрес&nbsp;</i></span><a href="mailto:listneb@gmail.com" target="_blank"><span style="color: #007894; border-color: windowtext; border-width: 1pt;"><i>listneb@gmail.com</i></span></a><i>) с Вашим предложением.</i>
</p>
<p>
 <i><br>
 </i>
</p>
<p>
 <b><span style="background-image: initial; background-color: #fbfcfd;">Вопрос: Предусмотрен ли создателями проекта ЕИСУБФ МО учёт мнений и потребностей читателей в отношении тех книг, которые им необходимы в оцифрованном виде?</span></b> Например, читателю нужна книга Чуковского, Федорова и Батюшкова "Принципы художественного перевода" 1919 или 1920 года. Кому можно направить запрос на сканирование?
</p>
<p>
 <br>
</p>
<p>
 <i>Ответ: М</i><span style="background-image: initial; background-color: #fbfcfd;"><i>ожно заполнить&nbsp;</i></span><a href="/questions.php" target="_blank"><span style="color: #007894; border-color: windowtext; border-width: 1pt;"><i>анкету.</i></span></a><span style="background-image: initial; background-color: #fbfcfd;"><i>&nbsp;Данные анкеты учитываются при составлении списков на оцифровку. Также можно написать в Отдел книги и чтения РГБ, в котором составляются ежемесячные списки на оцифровку, на адрес&nbsp;</i></span><a href="mailto:listneb@gmail.com" target="_blank"><span style="color: #007894; border-color: windowtext; border-width: 1pt;"><i>listneb@gmail.com</i></span></a><i>. </i>
</p>
 <i> </i>
<p>
 <i> </i>
</p>
 <i> </i>
<p>
 <i>
	В соответствии с законодательством разрешена оцифровка книг, которые не переиздавались более 10 лет. Например, если выбранная к оцифровке </i><span style="background-image: initial; background-color: #fbfcfd;"><i>книга была издана в 2004 году и более не переиздавалась, то, скорее всего, она будет одобрена и оцифрована.</i></span>
</p>
<p>
 <span style="background-image: initial; background-color: #fbfcfd;"><i><br>
 </i></span>
</p>
<p>
</p>
<h3>
<p>
 <b><span style="border-width: 1pt; border-color: windowtext;">Технические требования</span></b>
</p>
 </h3>
<p>
	 Для просмотра сайта рекомендуется использовать браузеры:&nbsp;
</p>
<p>
	 Mozilla Firefox 12+
</p>
<p>
	 Opera 10.0+
</p>
<p>
	 Internet Explorer 10+
</p>
<p>
	 Google Chrome 11+
</p>
<p>
 <br>
</p>
<p>
</p>
<p>
	 Оптимальным для просмотра сайта является разрешение экрана 1024х768 пикселей.&nbsp;
</p>
<p>
 <br>
	 Для просмотра большинства страниц сайта и имеющихся файлов требуется стандартное программное обеспечение.&nbsp;<br>
 <br>
	 Ряд документов и разделов сайта представлены в графическом формате (png, jpeg) и в текстовых форматах (html, doc, pdf)&nbsp;<br>
 <br>
	 В случае если с корректным отображением некоторых динамических элементов страниц сайта или реализацией возможности просмотра документов в электронной форме возникли сложности, пользователю следует убедиться в наличии на его компьютере необходимого программного обеспечения.&nbsp;<br>
 <br>
	 При выявлении факта отсутствия программного обеспечения для просмотра файлов в определённом формате, пользователю может потребоваться установка соответствующего программного обеспечения.&nbsp;<br>
 <br>
	 Так же рекомендуем установить или обновить следующее ПО*:&nbsp;<br>
 <br>
</p>
<p>
 <a href="http://www.adobe.com/ru/" target="_blank"><span style="color: windowtext; border-color: windowtext; border-width: 1pt;">Adobe Reader</span></a>
</p>
<p>
 <a href="http://www.adobe.com/ru/" target="_blank"><span style="color: windowtext; border-color: windowtext; border-width: 1pt;">Adobe Flash Player</span></a>
</p>
<p>
	<br>
</p><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>