<?
define('STATIC_PAGE', true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("F.A.Q. (Frequently asked questions)");
?>

    <p>Before you start writing your question, we suggest you to look through most frequently asked questions and answer to them of our specialists.</p>
    <br/>
    <h3>Questions on the registration on the NEL-website</h3>
    <p><strong>Question: Should I sign in to use the funds of the library?</strong></p>
    <p><em>Answer: 80% of the NEL-fund can be viewed by users without any limitations and registration. But to view a fund that is placed taking into account an agreement on observance of author’s rights, the registration is needed. Signed in users get a possibility to use their private account and with additional services.</em></p>
    <br/>
    <h3>Library pass of the NEL</h3>
    <p><strong>Question: Does my NEL library pass function in our regional library?</strong></p>
    <p><em>Answer: If a library is a participant of the NEL, you can get a library pass when you personally visit the library and on a simplified point of order. To do this you need to have an ID and know the number of your NEL library pass.</em></p>
    <br/>
    <h3>Work with documents</h3>
    <p><strong>Question: What speed of the Internet connection is needed to avoid freezing up of the computer when I view books?</strong></p>
    <p><em>Answer: Delays when turning the leaves of a book can be caused not only by the speed of connection, but also by the computer configuration. If you use a modern PC, you won’t have problems with viewing on the speed of connection from 2 Mbit/s and faster.</em></p>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>