<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 15.12.2016
 * Time: 17:15
 */
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
use \Bitrix\Main\Localization\Loc as Loc;
Loc::loadMessages(__FILE__);

global $APPLICATION, $USER;

if (!$USER->IsAdmin())
{
    LocalRedirect('/');
    return;
} ?>
<div class="container" style="margin: auto; width: 1200px; padding: 0">
    <div id="" data-title="Статистика">
        <? $APPLICATION->IncludeComponent(
            'neb:stats.monthly.common',
            '',
            Array(
                'form-hash' => '#tabs-4',
            )
            ,
            false
        ); ?>
    </div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>