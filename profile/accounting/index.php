<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 12.12.2016
 * Time: 16:57
 */
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
use \Bitrix\Main\Localization\Loc as Loc;
Loc::loadMessages(__FILE__);

global $APPLICATION, $USER;

if (!CSite::InGroup(array(1,6)))
{
    LocalRedirect('/');
    return;
}

CJSCore::RegisterExt("bootstrap_ext", Array(
    "js"    =>  "/bitrix/js/elr.useraccess/bootstrap.js",
    "css"   =>  "/bitrix/css/elr.useraccess/bootstrap.min.css",
    "rel"   =>  array('jquery2'),
    "skip_core" => "N"
));
CJSCore::RegisterExt("chosen_css", Array(
    "css" => "/bitrix/css/elr.useraccess/chosen.css",
    "skip_core" => "Y"
));
CJSCore::RegisterExt("mask", Array(
    "js" => "/local/components/elr/accounting.services/templates/.default/jquery.mask.js",
    "skip_core" => "Y"
));
CJSCore::RegisterExt("bs_validator", Array(
    "js" => "/local/components/elr/accounting.services/templates/.default/validator.js",
    "skip_core" => "N"
));
CJSCore::Init(array("bootstrap_ext", "chosen_css", "date", "mask", "bs_validator"));

$APPLICATION->SetTitle("Учет услуг");
?><div class="container" style="width: 1200px; padding: 0"><div class="menu-top"><?
$APPLICATION->IncludeComponent("bitrix:menu", "lk_left", Array(
    "ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
    "MENU_CACHE_TYPE" => "N",	// Тип кеширования
    "MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
    "MENU_CACHE_USE_GROUPS" => "N",	// Учитывать права доступа
    "MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
                                       0 => "",
    ),
    "MAX_LEVEL" => "2",	// Уровень вложенности меню
    "CHILD_MENU_TYPE" => "",	// Тип меню для остальных уровней
    "USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
    "DELAY" => "N",	// Откладывать выполнение шаблона меню
    "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
),
    false
);
?></div></div>
<? $APPLICATION->IncludeComponent(
    "elr:accounting.services",
    ".default"
);
?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
