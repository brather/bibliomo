<?php global $USER;

if ( !$USER->IsAuthorized() )
{
    return false;
}

$obNUser = new nebUser();
$obNUser->getRole();

if ( $obNUser->role == 'user' )
{
    /* массиы меню пользователя*/
    if ( SITE_TEMPLATE_ID == "evk" )
    {
        $aMenuLinksExt = array(
            array( 'Избранное', '#tabs-1', array(), array() ),
            array( 'Заказы', '#tabs-2', array(), array() ),
            array( 'Поисковые запросы', '#tabs-3', array(), array() ),
            array( 'Сообщения',
                   sprintf( '/forum/user/%d/post/all/', $obNUser->GetID() ),
                   array(), array(
                       "NnTabs" => "Y",
                   ),
            ),
        );
    }
    else
    {
        $aMenuLinksExt = array(
            array( ' ', '/profile/', array(),
                   array( 'FROM_IBLOCK' => true, 'IS_PARENT' => true,
                          'DEPTH_LEVEL' => 1,
                          "CLASS"       => "b-profile_navlk js_profilemenu" ) ),
            array( 'Заметки', '/profile/note/', array(),
                   array( 'FROM_IBLOCK' => true, 'IS_PARENT' => false,
                          'DEPTH_LEVEL' => 1,
                          "CLASS"       => "b-profile_nav_notes" ) ),
            array( 'Настройки профиля', '/profile/edit/', array(),
                   array( 'FROM_IBLOCK' => true, 'IS_PARENT' => false,
                          'DEPTH_LEVEL' => 2, "CLASS" => "" ) ),
            array( 'Помощь', '/faq/', array(),
                   array( 'FROM_IBLOCK' => true, 'IS_PARENT' => false,
                          'DEPTH_LEVEL' => 2, "CLASS" => "" ) ),
            #array('Выйти', '/profile/?logout=yes', array(), 					array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, "CLASS"=>"", 'STRONG' => true)),

            array( 'Личный кабинет', '/profile/', array(),
                   array( 'FROM_IBLOCK' => true, 'IS_PARENT' => false,
                          'DEPTH_LEVEL' => 1, "CLASS" => "" ) ),

            array( 'Мои заказы', '/profile/orders/', array(),
                   array( 'FROM_IBLOCK' => true, 'IS_PARENT' => false,
                          'DEPTH_LEVEL' => 1, "CLASS" => "" ) ),
            array( 'Избранное', '/profile/my-library/', array(),
                   array( 'FROM_IBLOCK' => true, 'IS_PARENT' => false,
                          'DEPTH_LEVEL' => 1, "CLASS" => "b-profile_nav_lb" ) ),
            array( 'Поисковые запросы', '/profile/searches/', array(),
                   array( 'FROM_IBLOCK' => true, 'IS_PARENT' => false,
                          'DEPTH_LEVEL' => 1,
                          "CLASS"       => "b-profile_nav_search" ) ),

            array( 'Цитаты', '/profile/quote/', array(),
                   array( 'FROM_IBLOCK' => true, 'IS_PARENT' => false,
                          'DEPTH_LEVEL' => 1, "CLASS" => "b-profile_nav_qt" ) ),
            array( 'Закладки', '/profile/bookmark/', array(),
                   array( 'FROM_IBLOCK' => true, 'IS_PARENT' => false,
                          'DEPTH_LEVEL' => 1, "CLASS" => "b-profile_nav_bm" ) ),
            array( 'Заметки', '/profile/note/', array(),
                   array( 'FROM_IBLOCK' => true, 'IS_PARENT' => false,
                          'DEPTH_LEVEL' => 1,
                          "CLASS"       => "b-profile_nav_notes" ) ),
        );
    }
}
else if ( $obNUser->role == UGROUP_LIB_CODE_ADMIN or $obNUser->role == UGROUP_LIB_CODE_EDITOR)
{
    /* массиы меню библиотекаря*/
    if ( SITE_TEMPLATE_ID == "evk" )
    {
        $aMenuLinksExt = array(
            array( 'Кабинет', '#tab-lk', array(), array() ),
            array( 'Новости',
                   '/bitrix/admin/iblock_list_admin.php?IBLOCK_ID=3&type=library',
                   array(), array() ),
            array( 'Заказы', '#tab-orders', array(), array() ),
            array( 'Фонды', '#tab-funds', array(), array() ),
            array( 'Коллекции', '#tab-collection', array(), array() ),
            array( 'Сообщения', '/forum/pm/folders/', array(), array() ),
        );
    }
    else
    {
        $aMenuLinksExt = array(
            array( ' ', '/profile/', array(),
                   array( 'FROM_IBLOCK' => true, 'IS_PARENT' => true,
                          'DEPTH_LEVEL' => 1,
                          "CLASS"       => "b-profile_navlk js_profilemenu" ) ),
            array( 'Настройки профиля', '/profile/edit/', array(),
                   array( 'FROM_IBLOCK' => true, 'IS_PARENT' => false,
                          'DEPTH_LEVEL' => 2, "CLASS" => "" ) ),
            array( 'Помощь', '/faq/', array(),
                   array( 'FROM_IBLOCK' => true, 'IS_PARENT' => false,
                          'DEPTH_LEVEL' => 2, "CLASS" => "", 'MSGNUM' => 0 ) ),
            array( 'Личная переписка', '/forum/pm/folders/', array(),
                   array( 'FROM_IBLOCK' => true, 'IS_PARENT' => false,
                          'DEPTH_LEVEL' => 2, "CLASS" => "" ) ),

            array( 'Кабинет', '/profile/', array(),
                   array( 'FROM_IBLOCK' => true, 'IS_PARENT' => false,
                          'DEPTH_LEVEL' => 1, "CLASS" => "" ) ),
            array( 'Заказы', '/profile/orders/', array(),
                   array( 'FROM_IBLOCK' => true, 'IS_PARENT' => false,
                          'DEPTH_LEVEL' => 1, "CLASS" => "" ) ),
            //array('Новости', '/profile/news/', array(), 				array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, "CLASS"=>"")),
            #array('Выйти', '/profile/?logout=yes', array(), 			array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, "CLASS"=>"", 'STRONG' => true)),
            array( 'Статистика', '/profile/statistics/', array(),
                   array( 'FROM_IBLOCK' => true, 'IS_PARENT' => false,
                          'DEPTH_LEVEL' => 1, "CLASS" => "" ) ),
            array( 'Фонды', '/profile/funds/manage/', array(),
                   array( 'FROM_IBLOCK' => true, 'IS_PARENT' => false,
                          'DEPTH_LEVEL' => 1, "CLASS" => "" ) ),
            array( 'Читатели', '/profile/readers/', array(),
                   array( 'FROM_IBLOCK' => true, 'IS_PARENT' => false,
                          'DEPTH_LEVEL' => 1, "CLASS" => "" ) ),
            #array('Коллекции', '/profile/collection/', array(), 				array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),
            #array('Новости', '/bitrix/admin/iblock_list_admin.php?IBLOCK_ID=3&type=news&lang=ru', array(), 				array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),
            array( 'Коллекции', '/profile/collection/', array(),
                   array( 'FROM_IBLOCK' => true, 'IS_PARENT' => false,
                          'DEPTH_LEVEL' => 1, "CLASS" => "" ) ),
            array( 'Новости', '/profile/news/', array(),
                   array( 'FROM_IBLOCK' => true, 'IS_PARENT' => false,
                          'DEPTH_LEVEL' => 1, "CLASS" => "" ) ),

            // array('Оцифровка', '/profile/plan_digitization/', array(), 	array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),
            // array('ЕЭЧБ', '/profile/check_eechb/', array(), 			array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),
        );
    }
}
else if ( $obNUser->role == UGROUP_LIB_CODE_CONTORLLER )
{
    /* массиы меню библиотекаря-контролера*/
    $aMenuLinksExt = array(
        array( ' ', '/profile/', array(),
               array( 'FROM_IBLOCK' => true, 'IS_PARENT' => true,
                      'DEPTH_LEVEL' => 1,
                      "CLASS"       => "b-profile_navlk js_profilemenu" ) ),
        array( 'Настройки профиля', '/profile/edit/', array(),
               array( 'FROM_IBLOCK' => true, 'IS_PARENT' => false,
                      'DEPTH_LEVEL' => 2, "CLASS" => "" ) ),
        array( 'Помощь', '/faq/', array(),
               array( 'FROM_IBLOCK' => true, 'IS_PARENT' => false,
                      'DEPTH_LEVEL' => 2, "CLASS" => "", 'MSGNUM' => 0 ) ),
        #array('Выйти', '/profile/?logout=yes', array(), 			array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 2, "CLASS"=>"", 'STRONG' => true)),
        array( 'Кабинет', '/profile/', array(),
               array( 'FROM_IBLOCK' => true, 'IS_PARENT' => false,
                      'DEPTH_LEVEL' => 1, "CLASS" => "" ) ),
        array( 'Статистика', '/profile/statistics/', array(),
               array( 'FROM_IBLOCK' => true, 'IS_PARENT' => false,
                      'DEPTH_LEVEL' => 1, "CLASS" => "" ) ),
        // array('ЕЭЧБ', '/profile/check_eechb/', array(), 			array('FROM_IBLOCK' => true, 'IS_PARENT' => false, 'DEPTH_LEVEL' => 1, "CLASS"=>"")),
    );
}

if (CSite::InGroup( array(1,6) ))
{
    $aMenuLinksExt[] = array('Статистика', '/profile/statistic/', array(), array());
    $aMenuLinksExt[] = array('Учет услуг', '/profile/accounting/', array(), array());
    $aMenuLinksExt[] = array('Пользователи', '#tabs-5', array(), array());
    $aMenuLinksExt[] = array('Форма 6НК', '/profile/form-6nk/', array(), array() );

    if ($USER->IsAdmin()) // Администратор поратала
    {
        $aMenuLinksExt[] = array('Справочник услуг', '#tabs-7', array(), array() );
    }

    $aMenuLinksExt[] = array('Библиотеки', '#tabs-6', array(), array());
}

$aMenuLinks = array_merge($aMenuLinks, $aMenuLinksExt);