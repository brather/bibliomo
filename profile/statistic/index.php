<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 12.12.2016
 * Time: 16:57
 */
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
use \Bitrix\Main\Localization\Loc as Loc;
Loc::loadMessages(__FILE__);

global $APPLICATION, $USER;

if (!CSite::InGroup(array(1,6)))
{
    LocalRedirect('/');
    return;
}


$APPLICATION->SetTitle("Мониоринг АБИС");
?><div class="container" style="width: 1200px; padding: 0"><div class="menu-top"><?
$APPLICATION->IncludeComponent("bitrix:menu", "lk_left", Array(
    "ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
    "MENU_CACHE_TYPE" => "N",	// Тип кеширования
    "MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
    "MENU_CACHE_USE_GROUPS" => "N",	// Учитывать права доступа
    "MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
                                       0 => "",
    ),
    "MAX_LEVEL" => "2",	// Уровень вложенности меню
    "CHILD_MENU_TYPE" => "",	// Тип меню для остальных уровней
    "USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
    "DELAY" => "N",	// Откладывать выполнение шаблона меню
    "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
),
    false
);
?></div></div>
<? $APPLICATION->IncludeComponent(
    "elr:info.monitoring",
    ".default"
);
?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
