<?
define('STATIC_PAGE', true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Для читателей");
?><p style="text-align: justify;">
	 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;ЕИСУБФ МО предназначена для повышения качества информационного обслуживания жителей Московской области. Она даёт Вам доступ к сводному каталогу библиотек Подмосковья и электронным копиям книг (не защищенных авторским правом).
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 Здесь Вы сможете:
</p>
<p style="text-align: justify;">
</p>
<ul>
	<li>
	<p style="text-align: justify;">
		 узнать, в каких библиотеках Вы можете заказать интересующую Вас книгу,
	</p>
 </li>
	<li>
	<p style="text-align: justify;">
		 найти, где находится ближайшая к Вам библиотека,
	</p>
 </li>
	<li>
	<p style="text-align: justify;">
		 увидеть актуальную контактную информацию по всем библиотекам и пунктам выдачи,
	</p>
 </li>
	<li>
	<p style="text-align: justify;">
		 заказать нужную Вам книгу,
	</p>
 </li>
	<li>
	<p style="text-align: justify;">
		 читать он-лайн оцифрованную литературу,
	</p>
 </li>
</ul>
<p style="text-align: justify;">
</p>
<ul>
	<li><span style="text-align: justify;">ознакомиться с редкими фондами библиотек Подмосковья,</span><span style="text-align: justify;">&nbsp;</span></li>
	<li><span style="text-align: justify;">просматривать тематические коллекции книг,</span></li>
	<li><span style="text-align: justify;">создавать в личном кабинете собственные подборки литературы,</span></li>
	<li><span style="text-align: justify;">быть в курсе последних новостей и событий библиотечного сообщества,</span></li>
	<li><span style="text-align: justify;">принять участие в обсуждении интересующих Вас вопросов на форуме портала.</span></li>
</ul>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 &nbsp; &nbsp; &nbsp;Каталог библиографических описаний и оцифрованных изданий открытого доступа пополняется ежедневно. Постоянно создаются новые тематические коллекции.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Портал имеет связь с социальными сетями Facebook, Вконтакте и Одноклассники.
</p>
<p style="text-align: justify;">
	 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Работа проекта ориентирована, в первую очередь, на улучшение обслуживания читателей. Вы можете помочь нам сделать его лучше, используя Форум и Обратную связь.&nbsp;
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 &nbsp;
</p>
<p style="text-align: justify;">
</p>
<p>
</p><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>