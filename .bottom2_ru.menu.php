<?
if(SITE_TEMPLATE_ID == "evk")
{
	$aMenuLinks = Array(
		Array(
			"Участники",
			"/participants/",
			Array(),
			Array(),
			""
		),
		Array(
			"Обратная связь",
			"/feedback/",
			Array(),
			Array(),
			""
		),
		Array(
			"Форум",
			"/forum/",
			Array(),
			Array(),
			""
		),
	);
	return;
}
$aMenuLinks = Array(
	Array(
		"Обратная связь", 
		"/feedback/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Часто задаваемые вопросы", 
		"/faq/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Пользовательское соглашение", 
		"/user-agreement/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Правовая информация", 
		"/legal-information/", 
		Array(), 
		Array(), 
		"" 
	),
);
?>