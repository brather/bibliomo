<?
define('STATIC_PAGE', true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Terms of Use");
?>
    <h3>Права и обязанности пользователей </h3>

    <ol class="b-commonlist">
        <li>
            <strong>1.  Общие условия</strong>
            <ol class="b-commonlist">
                <li>1.1.  Данный договор является официальным предложением (публичной офертой)  Национальной Электронной Библиотеки, именуемого в дальнейшем «ЕИСУБ»,  и содержит все существенные условия предоставления доступа к библиотечным информационным электронным ресурсам Национальной Электронной Библиотеки, находящихся по адресу: http://_________ (далее – Сайт).</li>
                <li>1.2. В соответствии с пунктом 2 статьи 437 Гражданского кодекса Российской Федерации (далее – ГК РФ) в случае принятия изложенных ниже условий, физическое лицо, производящее акцепт этой оферты становится «Пользователе» (в соответствии с пунктом 3 статьи 438 ГК РФ акцепт оферты равносилен заключению договора на условиях, изложенных в оферте), а ЕИСУБ и Пользователь, совместно или в отдельности – «Стороны»,  «Сторона» Договора на условиях оферты.</li>
                <li>1.3. Акцепт условий, изложенных в настоящей публичной Оферте, является факт подписания и/или подтверждения личных данных и согласия с условиями Договора публичной оферты, при регистрации в сети Интернет через сайт ________________ ЕИСУБ.
                При регистрации Пользователь самостоятельно вводит предоставленные ему Имя (логин) и  Пароль в зашифрованном виде. Пользователь несет полную ответственность за безопасность  Имени пользователя (логина) и Пароля, в частности за отсутствие к ним доступа у третьих лиц.</li>
                <li>1.4.  ЕИСУБ оставляет за собой право изменять или дополнять настоящий Договор в любой момент без предварительного или последующего уведомления.
                Пользователь самостоятельно отслеживает изменения Договора и знакомится с действующей редакцией Договора. Продолжение использования Сайта Пользователем после внесения изменений и/или дополнений в настоящий Договор означает принятие и согласие Пользователя с такими изменениями и/или дополнениями.</li>
                <li>1.5. При регистрации Пользователь обязан сообщить ЕИСУБ и проверить точность своих данных: фамилию, имя и отчество, e-mail, пароль, иные данные. В случае ошибок или несоответствия части данных следует обратиться  в Службу технической поддержки ЕИСУБ  по адресу электронной почты __________________.</li>
                <li>1.6. Принятием настоящего Договора Пользователь дает согласие ЕИСУБ на обработку своих персональных данных, переданных им ЕИСУБ,  в соответствии с условиями настоящего Договора.</li>
                <li>1.7. Используя Сайт, в том числе, путем просмотра страниц и материалов Сайта, Пользователь подтверждает, что он ознакомлен и согласен с правилами доступа к библиотечным электронным информационным ресурсам Сайта и означает присоединение Пользователя к настоящему Договору и безоговорочное принятие его условий</li>
            </ol>
        </li>
        <li>
            <strong>2. Основные понятия, используемые в целях настоящего Договора на условиях Оферты</strong>
            <ol class="b-commonlist">
                <li>2.1. В целях настоящей Оферты нижеприведенные термины используются в следующем значении:
                Оферта – предложение ЕИСУБ, адресованное любому физическому лицу, являющегося зарегистрированным читателем Национальной Электронной Библиотеки.
                Акцепт Оферты – полное и безоговорочное принятие Оферты путем осуществления действий, указанных в  настоящем Договоре на условиях оферты. Акцепт Оферты создает Договор на условиях оферты.
                Пользователь – физическое лицо, являющееся зарегистрированным читателем Национальной Электронной Библиотеки, осуществившее Акцепт Оферты по заключенному Договору на условиях оферты.</li>
            </ol>
        </li>
        <li>
            <strong>3. Предмет Договора на условиях Оферты</strong>
            <ol class="b-commonlist">
                <li>3.1.  ЕИСУБ оказывает Пользователю услугу предоставления доступа к библиотечным электронным информационным ресурсам  Национальной Электронной Библиотеки по уникальному имени Пользователя и паролю в течение 3 (трех) календарных дней с момента Акцепта Оферты  (далее Услуга) в объеме, имеющегося на Сайте библиотечного электронного информационного ресурса.</li>
                <li>3.2.  НЕИСУБЭБ оказывает Услугу по сети Интернет через сайт _____.</li>
                <li>3.3.  Публичная Оферта является официальным документом  и ее действующая редакция размещается на Интернет-сайте ЕИСУБ по адресу  ________</li>
            </ol>
        </li>
        <li>
            <strong>4. Акцепт Оферты и заключение Договора на условиях Оферты</strong>
            <ol class="b-commonlist">
                <li>4.1. Пользователь производит Акцепт Оферты путем подписания настоящего Договора на условиях Оферты и/или подтверждения личных данных и согласия с условиями Договора на условиях Оферты, при регистрации по сети Интернет через сайт ______.  Подтверждение личных, персональных данных и согласие с условиями настоящего Договора на условиях Оферты, при регистрации по сети Интернет через сайт ____________ приравнивается к подписанию настоящего договора на условиях Оферты.</li>
                <li>4.2.  Дата акцепта оферты является датой заключения настоящего Договора на условиях Оферты.</li>
                <li>4.3.   Услуга оказывается безвозмездно.</li>
            </ol>
        </li>
        <li>
            <strong>5.   Права, обязанности и ответственность Сторон</strong>
            <ol class="b-commonlist">
                <li>
                5.1.  ЕИСУБ обязуется:
                    <ol class="b-commonlist">
                        <li>5.1.1. Оказать Услугу в соответствии с настоящим Договором на условиях Оферты.</li>
                        <li>5.1.2. Предоставить Пользователю уникальное имя пользователя и пароль.</li>
                    </ol>
                </li>
                <li>
                5.2.    Пользователь обязуется:
                    <ol class="b-commonlist">
                        <li>5.2.1. Использовать предоставленную услугу по доступу к библиотечным электронным информационным ресурсам, только в личных целях и не использовать ее в предпринимательских целях или для получения личной выгоды.</li>
                        <li>5.2.2. Не передавать доступ к имеющимся у него  в соответствии с Договором на условиях Оферты электронным информационным ресурсам третьим лицам, не имеет право модифицировать, продавать, распространять материалы Сайта целиком либо по частям, если это  нарушает исключительные права третьих лиц, в частности авторские и смежные с ними права, а также исключительные права на изобретение, полезную модель, промышленный образец или товарный знак.</li>
                        <li>5.2.3. Пользователю запрещается копирование, скачивание, воспроизводство, распространение и перевод на другие языки в электронной или печатной форме любого произведения, находящегося на Сайте, если это  нарушает исключительные права третьих лиц, в частности авторские и смежные с ними права, а также исключительные права на изобретение, полезную модель, промышленный образец или товарный знак.</li>
                        <li>5.2.4. Пользователь признает и соглашается с тем, что находящиеся на Сайте материалы и  необходимые программы, связанные с работой Сайта, защищены законами об интеллектуальной собственности РФ и прочими международными законами.
                        Пользователь несет ответственность в случае нарушения им интеллектуальных прав третьих лиц, в частности авторских и смежных с ними прав, а также исключительных права на изобретение, полезную модель, промышленный образец или товарный знак.</li>
                    </ol>
                </li>
                <li>
                5.3.  ЕИСУБ имеет право:
                    <ol class="b-commonlist">
                        <li>5.3.1. Ограничивать или блокировать доступ к электронным информационным ресурсам или принимать иные меры в отношении Пользователя, нарушившего условия настоящего Договора, либо нормы действующего законодательства, либо охраняемые законом права третьих лиц.</li>
                        <li>5.3.2. Модифицировать Сайт по своему усмотрению. Вносить изменения в данный Договор в одностороннем порядке без какого-либо специального уведомления. Новая редакция Договора  вступает в силу с момента ее размещения на Сайте.</li>
                    </ol>
                </li>
                <li>
                5.4. Пользователь имеет право:
                    <ol class="b-commonlist">
                        <li>5.4.1.  Отказаться от заключения Договора на условиях оферты в любое время до момента, оговоренного в пункте 4.2. настоящего Договора на условиях Оферты.</li>
                        <li>5.4.2. Пользователь вправе свободно в соответствии с ГК РФ часть 4, глава 70, ст.1273, 1274 воспроизводить и использовать  произведения, находящиеся на Сайте и предоставленные в соответствии с настоящим Договором на условиях Оферты.</li>
                    </ol>
                </li>
                <li>
                5.5.  ЕИСУБ не несет ответственность за ненадлежащее предоставление Услуги и/или выполнение Заказа по претензиям Пользователя к качеству соединения с сетью Интернет, связанным с качеством функционирования сетей Интернет-провайдеров, политикой обмена трафиком между провайдерами, и другими обстоятельствами, находящимися вне зоны компетенции, влияния и контроля ЕИСУБ.
                </li>
                <li>
                5.6.  Все претензии по предоставлению Услуги в письменном виде направляются ЕИСУБ по адресу: __________________. Вся поступившая информация обрабатывается в сроки, установленные ГК РФ.
                </li>
            </ol>
        </li>
        <li>
            <strong>6.  Условия и порядок оказания услуг</strong>
            <ol class="b-commonlist">
                <li>6.1. Пользователь знакомится с правилами и объемом предоставляемых ЕИСУБ Услуг на сайте __________.</li>
                <li>6.2.  Услуга предоставляется Пользователю с момента вступления Договора на условиях Оферты в силу.</li>
                <li>6.3. Услуга считается оказанной надлежащим образом и в полном объеме, если до окончания срока услуги исполнителем не получена и удовлетворена ЕИСУБ претензия от Пользователя.</li>
            </ol>
        </li>
        <li>
            <strong>7.  Прочие условия</strong>
            <ol class="b-commonlist">
                <li>7.1.Все изменения, дополнения, приложения к настоящему Договору на условиях оферты или иная информация являются неотъемлемой его частью и являются обязательными для исполнения с момента опубликования их на Сайте, если иное не установлено ЕИСУБ.</li>
                <li>7.2.. Споры между Сторонами разрешаются путем переговоров, а в случае невозможности такого урегулирования в судебном порядке по месту нахождения ЕИСУБ в соответствии с законодательством Российской Федерации.</li>
                <li>7.3. Отношения сторон по настоящему Соглашению регулируются действующим законодательством Российской Федерации.</li>
            </ol>
        </li>
    </ol>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>