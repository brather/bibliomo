<?php
function curl($url, $array_post, $port, $key){

	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; ru; rv:1.9.0.17) Gecko/2009122116 Firefox/3.0.17");
	
	if (!empty($key)){
	
		$arHeader[] = 'DLIB-KEY: '.$key;
		curl_setopt($ch, CURLOPT_HTTPHEADER, $arHeader);
	}
	
	if(!empty($port))
				curl_setopt($ch, CURLOPT_PORT , $port);
				
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLINFO_HEADER_OUT, false);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($array_post));
	$result = curl_exec($ch);
	
	curl_close($ch);
	return $result;


}

?>