<?php
/*

скрипт для агрегирования данных статики по просмотру изданий библиотек
запускается раз в день и формирует пул данных за предыдущий день

*/

$_SERVER["DOCUMENT_ROOT"] = '/home/bitrix/www';

require_once('functions.php');
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
ini_set('memory_limit', '700M');
AddMessage2Log("Скрипт формирования статистики запущен");
// проверка было ли обновление 

$strSql = "SELECT * FROM neb_stat_log WHERE DT ='".date("Y-m-d", time() - 86400)."'";
$rs = $DB->Query($strSql, false, $err_mess.__LINE__);

if($rs->SelectedRowsCount() == 0){
			// токен
			$array_post = array(
				'email' =>  'pushkin@elar.ru',
				'password' =>  '111111'
			);
			
			$json_res = curl('10.4.130.2/sso/auth/',$array_post);
			$result = json_decode($json_res);
			$token = $result->{'result'}->{'TOKEN'};

			//параметры для получения статистики 
			$array_post2 = array(
				'from' => date("Y-m-d",time() - 86400),
				'to' => date("Y-m-d",time() - 86400),
				//'from' => '2014-12-29',
				//'to' => '2014-12-29',
				'token' => $token
			);
			
			$json_res = curl('10.4.130.4/stat/',$array_post2, '8081', '99Apaf8YwtAskq5sdfyNMnbD3SDU7qUH');
			
			// удаление аргумента args. дополнительные действия, чтобы распарсить json результат 
			$pattern = '/"args":\s{[^}]*},/';
			$replacement = "";
			$resu = preg_replace($pattern, $replacement, $json_res);
			$resu = str_replace(']','}]',$resu);

			unset($json_res);

			var_dump( ini_get('memory_limit'));
			echo memory_get_usage();
			$mass_stat = json_decode($resu,true);
			
			var_dump(count($mass_stat['data']));echo memory_get_usage();
			/*  подключение к бд neb  */
			require_once('config.php');
			$link = mysql_connect ($DBHost, $DBLogin, $DBPassword); 

			if (! $link){ 
				echo ( "Unable to connect to db" ); 
			exit(); 
			} 
			
			/* Выбор БД */ 
			if (!mysql_select_db ($DBName, $link) ){ 
			exit (); 
			} 
			
			mysql_set_charset("utf8");
			
			
			foreach ($mass_stat['data'] as $value){
				if (($value['status_code'] == 200 || $value['status_code'] == 304) ){
					$id_book = urldecode($value['document']);
					// определение библиотеки по id книги 
					if (!isset($res[$id_book]['id_lib'])){
					
						$id_lib = 0;
						$query = "SELECT * FROM neb.vw_common_biblio_card_full_id WHERE FullSymbolicId='".$id_book."';"; 
					
						$sql = mysql_query ( $query ,$link); 
						$n = mysql_num_rows ( $sql );
					
						if ( $sql ) { 
							while ( $result = mysql_fetch_array($sql ) ) { 
								$id_lib = $result['LibraryId'];
								
							}
						}
						$res[$id_book]['id_lib'] = $id_lib;
					}
				
					
					// фиксирование количества просмотра книги
					if ($value['method'] == 'DocumentInfo'){
						$res[$id_book]['view'] = ($res[$value['document']]['view'] > 0 ? $res[$value['document']]['view']+1 : 1);
					// фиксирование количества чтения книги
					}elseif ($value['method'] == 'RenderPage' || $value['method'] == 'PageGeometry' ||$value['method'] == 'PageSearchRender'){
						$res[$id_book]['read'] = ($res[$value['document']]['read'] > 0 ? $res[$value['document']]['read']+1 : 1);
						}
				}
			}
			var_dump($res);
			
			// запись статистики в таблицу
			foreach ($res as $id_book => $book){
				if ($book['read'] > 0 || $book['view'] > 0){
					$arFields = array(
						//'DT' => "28.12.2014",
						'DT' => date("d.m.Y", time() - 86400),
						'ID_BOOK' => $id_book,
						'ID_LIB' => $book['id_lib'],
						'CNT_READ' => $book['read'],
						'CNT_VIEW' => $book['view']
					);
					$arInsert = $DB->PrepareInsert("neb_stat_log", $arFields, "form");
					$strSql = "INSERT INTO neb_stat_log (".$arInsert[0].") VALUES (".$arInsert[1].")";
					$DB->Query($strSql, false, $err_mess.__LINE__);
				}
			}	
	mysql_close($link);
	AddMessage2Log("Статистика успешно сформирована");		
}else{

	AddMessage2Log("Статистика была сформирована ранее");		
}
?>