<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
function ThisUserProfile(){
    global $USER;
    $idUser = $USER->GetId();
    $arSelect = Array("ID", "IBLOCK_ID", "NAME", "PROPERTY_USER");
    $arFilter = Array("IBLOCK_ID" => IBLOCK_ID_IMPORT_PROFILE, "ACTIVE" => "Y", "PROPERTY_USER" => $idUser);
    $res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
    $key = 0;
    $arOption = array();
    while($ob = $res->GetNextElement()){
        $arFields = $ob->GetFields();
        $arOption[$key]["ID"] = $arFields["ID"];
        $arOption[$key]["NAME"] = $arFields["NAME"];
        $arOption[$key]["USER"] = $arFields["PROPERTY_USER_VALUE"];
        $key++;
    }
    return $arOption;
}
