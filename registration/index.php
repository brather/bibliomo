<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php"); 
global $APPLICATION;  
?>
<?/*$APPLICATION->IncludeComponent(
	"neb:registration",
	"",
	Array(
		"SEF_MODE" => "Y",
		"SEF_FOLDER" => "/registration/",
		"SEF_URL_TEMPLATES" => Array("main"=>"/","full"=>"full/","rgb"=>"rgb/","ecia"=>"ecia/","simple"=>"simple/"),
		"VARIABLE_ALIASES" => Array("main"=>Array(),"full"=>Array(),"rgb"=>Array(),"ecia"=>Array(),"simple"=>Array(),),
		"VARIABLE_ALIASES" => Array(
			"main" => Array(),
			"full" => Array(),
			"rgb" => Array(),
			"ecia" => Array(),
			"simple" => Array(),
		)
	)
);*/?>
<?$APPLICATION->IncludeComponent(
	"neb:main.register",
	"simple", 
	array(
		"USER_PROPERTY_NAME" => "",
		"SHOW_FIELDS" => array(
			0 => "EMAIL",
			1 => "NAME",
			2 => "SECOND_NAME",
			3 => "LAST_NAME",
		),
		"REQUIRED_FIELDS" => array(
			0 => "EMAIL",
			1 => "NAME",
			3 => "LAST_NAME",
		),
		"AUTH" => "Y",
		"USE_BACKURL" => "Y",
		"SUCCESS_PAGE" => "",
		"SET_TITLE" => "N",
		"USER_PROPERTY" => array(
		)
	),
	false
);?>
