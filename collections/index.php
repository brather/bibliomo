<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
use \Bitrix\Main\Localization\Loc as Loc;
Loc::loadMessages(__FILE__);
$APPLICATION->SetTitle(Loc::getMessage('COLLECTION_PAGE_TITLE'));
?>
<?$APPLICATION->IncludeComponent(
	"neb:collections", 
	".default", 
	array(
		"IBLOCK_TYPE" => "collections",
		"IBLOCK_ID" => "6",
		"SEF_MODE" => "Y",
		"SEF_FOLDER" => "/collections/",
		"SEF_URL_TEMPLATES" => array(
			"list" => "",
			"seaction" => "#ID#_#CODE#/",
		)
	),
	false
);?>
    <br>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>