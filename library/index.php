<?
define('MARGIN_MAP', 1);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
use \Bitrix\Main\Localization\Loc as Loc;
Loc::loadMessages(__FILE__);
$APPLICATION->SetTitle(Loc::getMessage('LIBRARY_PAGE_TITLE'));
?>
<?$APPLICATION->IncludeComponent(
	"neb:library", 
	".default", 
	array(
		"ITEMS_LIMIT" => "10",
		"SEF_MODE" => "Y",
		"SEF_FOLDER" => "/library/",
		"IBLOCK_TYPE" => "library",
		"IBLOCK_ID" => "4",
		"SEF_URL_TEMPLATES" => array(
			"list" => "/",
			"detail" => "#CODE#/",
			"funds" => "#CODE#/funds/",
			"collections" => "#CODE#/collections/",
			"news_detail" => "#CODE#/news-#ELEMENT_ID#-#ELEMENT_CODE#/",
		)
	),
	false
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>