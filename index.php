<?
	global $GLOBAL, $APPLICATION;
	$GLOBAL['PAGE'] = 'MAIN';
	define('MARGIN_MAP', 1);
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
	$APPLICATION->SetTitle("ЕИСУБ МО");
?><div class="wrapper">
	 <?$APPLICATION->IncludeComponent(
	"neb:main.slider",
	".default",
	Array(
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"IBLOCK_ID" => "6",
		"IBLOCK_TYPE" => "collections",
		"PAGE_URL" => "/catalog/#BOOK_ID#/"
	)
);?> <section class="b-publishing clearfix">
	<?
	$user = new nebUser();
	$user->getRole();
	$APPLICATION->IncludeComponent(
	"neb:collections.list.new",
	"",
	Array(
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"SHOW_SKBR"  => $user->role == UGROUP_LIB_CODE_ADMIN
			or $user->role == UGROUP_LIB_CODE_EDITOR,
	)
);?> <?$APPLICATION->IncludeComponent(
	"neb:new.list",
	".default",
	Array(
		"CACHE_TIME" => "36001",
		"CACHE_TYPE" => "N",
		"COMPONENT_TEMPLATE" => ".default",
		"COUNT" => "",
		"DATE" => "30",
		"NUM_PAGE" => "1",
		"PAGE_URL" => "/catalog/#BOOK_ID#/"
	)
);?> </section> <section class="b-newssection">
	<?$APPLICATION->IncludeComponent(
	"neb:main.stat", 
	".default", 
	array(
		"BOOKS_WITHOUT_FILES" => "4235700",
		"BOOKS_WITH_FILES" => "3180",
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"COMPONENT_TEMPLATE" => ".default",
		"USERS" => "2385"
	),
	false,
	array(
		"ACTIVE_COMPONENT" => "Y"
	)
);?> <?$APPLICATION->IncludeComponent(
	"neb:main.news",
	"",
	Array(
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"COMPONENT_TEMPLATE" => "news",
		"COUNT" => "5"
	)
);?> </section>
	<?$APPLICATION->IncludeComponent(
	"neb:library.list",
	"main",
	Array(
		"CACHE_TIME" => $arParams["CACHE_TIME"],
		"IBLOCK_ID" => 4,
		"IBLOCK_TYPE" => "library",
		"ROWS_PER_PAGE" => "20"
	)
);?>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>