<?php
session_start();

require_once $_SERVER['DOCUMENT_ROOT'] . '/local/tools/esia/psaml/_toolkit_loader.php';

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/socialservices/classes/general/authmanager.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/socialservices/classes/general/openid.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/socialservices/classes/mysql/authmanager.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/local/php_interface/include/event_handlers/esia_auth.php');

$auth = new OneLogin_Saml2_Auth($settingsInfo);

$action = $_GET['act'];

//авторизоваться
if($action == 'do_login'){
    //$auth->login('https://demo.neb.elar.ru');
    $auth->login();
}

//ответ с данными об авторизации
if($action == 'login'){
    //логин или создание аккаунта в битрикс
    $auth->processResponse();
    $errors = $auth->getErrors();

    //есть ошибки?
    if (!empty($errors)) {
        //print_r('<p>'.implode(', ', $errors).'</p>');
        file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/WebServices/log.txt', print_r($errors, true), FILE_APPEND);
    }

    $_SESSION['samlUserdata'] = $auth->getAttributes();
    $_SESSION['samlNameId'] = $auth->getNameId();
    $_SESSION['samlSessionIndex'] = $auth->getSessionIndex();

    if($auth->isAuthenticated()){
        $attributes = $_SESSION['samlUserdata'];

        $arFields = array(
            'EXTERNAL_AUTH_ID' => 'Esia',
            'XML_ID' => $attributes['urn:mace:dir:attribute:userId'][0],
            'LOGIN' => $attributes['urn:esia:personEMail'][0],
            'EMAIL' => $attributes['urn:esia:personEMail'][0],
            'NAME'=> $attributes['urn:mace:dir:attribute:firstName'][0],
            'LAST_NAME'=> $attributes['urn:mace:dir:attribute:lastName'][0],
            'SECOND_NAME'=> $attributes['urn:mace:dir:attribute:middleName'][0],
            'PERSONAL_BIRTHDAY' => date('d.m.Y', strtotime($attributes['urn:esia:birthDate'][0])),
            'PERSONAL_GENDER' => $attributes['urn:esia:gender'][0] == 'FEMALE' ? 'F' : 'M',
            'UF_REGISTER_TYPE' => '40'
        );

        //file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/WebServices/log.txt', print_r($arFields, true), FILE_APPEND);

        $esiaAuth = new CSocServEsia();
        $esiaAuth->Authorize($arFields);
    }
}

//вызыв логаута
if($action == 'do_logout'){
    $returnTo = null;
    $params = array();
    $nameId = null;
    $sessionIndex = null;
    if (isset($_SESSION['samlNameId'])) {
        $nameId = $_SESSION['samlNameId'];
    }
    if (isset($_SESSION['samlSessionIndex'])) {
        $sessionIndex = $_SESSION['samlSessionIndex'];
    }

    $auth->logout($returnTo, $params, $nameId, $sessionIndex);
}

if($action == 'logout'){
    /*echo $auth->getSSOurl();
    print_r($auth->getAttributes());*/
}



