<?
	include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

	CHTTP::SetStatus("404 Not Found");
	@define("ERROR_404","Y");

	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

	$APPLICATION->SetTitle("Страница не найдена");

?>
<section class="innersection innerwrapper clearfix page404">
	<p>страница не найдена</p>
	<img src="<?=MARKUP?>i/page404.jpg" alt="страница не найдена">
	<div class="b_backlink">Перейдите <a href="/">на главную страницу</a> или воспользуйтесь поиском</div>
</section>
<?
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>