<??>
<script type="text/javascript">
	$(function() {
		//$('.b-result-docitem .b-result-type_txt.closed_book a, .b-onebookinfo .aformbutton.closed_book').click(function(){
		DOCUMENT.on('click', '.b-result-docitem .b-result-type_txt.closed_book a, .b-onebookinfo .aformbutton.closed_book', function(e){
            if(device.mobile()){
                messageOpen('.b-message');
                return false;
            } else {
				if(true/*$(this).attr('href') == '#'*/)
                {
                    $('.popup_autoreg_form .closed-link').attr('href', $(this).data('link'));
                    $('.popup_autoreg_form').autoregPopup();
                    return false;
                }
            }
		});

		$('.popup_autoreg_form .closed-link').click(function(){
			$('.closepopup').click();
		});

		var sendForm = false;

		$('form.b-formautoreg').submit(function( event ) {

			var form = this;
			if(sendForm == true)
				return false;

			$('em.error', form).addClass('hidden');

			if($('.autoreglogin', form).val().length <=0 || $('.autoregpass', form).val().length <=0)
				return false

			sendForm = true;				

			BX.showWait();

			$.ajax({
				type: "POST",
				url: form.action,
				data: $(form).serialize(),
				cache:false,
				timeout:120000,
				dataType: 'json'
			})
			.done(function( resp ) {
				BX.closeWait();
				sendForm = false;
				console.log(resp);
				if(resp.error == true)
				{
					$('.error', form).removeClass('hidden');
				}
				else
				{
                    if(resp.redirect){
                        window.location.replace(resp.redirect);
                    }

					$(form).addClass('hidden');
					$('.popup_autoreg_form .ok.tcenter').removeClass('hidden');

					$('.closed_book').each(function() {
						$( this ).removeClass( "closed_book" );
						var aLink = $( this ).find('a');
						if(aLink.length <= 0)
							aLink = this;
						$(aLink).attr('href', $(aLink).data('link'));
						/*$(aLink).click(function() {
							messageOpen($(this).attr('href'), '.b-message_warning'); 
							return false;
						});*/
					});


				}				
			});
			return false;
		});
	});

	(function() { //create closure
		$.fn.autoregPopup = function() { // попап регистрации
			this.each(function(){
				var popup = $(this).clone(true);
				popup.removeClass('hidden');
				popup.dialog({
					closeOnEscape: true,	                   
					modal: true,
					draggable: false,
					resizable: false,             
					width: 670,
					dialogClass: 'autoreg_popup',
					position: "center",
					open: function(){
						$('.ui-widget-overlay').addClass('black');
						popup.find('.closepopup').click(function() {				
							//Close the dialog
							popup.dialog("close").remove();
							$('.ui-widget-overlay').remove();
							return false;
						});  
					},
					close: function() {
						popup.dialog("close").remove();
					}
				});

				$('.ui-widget-overlay').click(function() {				
					//Close the dialog
					popup.dialog("close");

				});   

			});
		}
		//end of closure
	})(jQuery);

</script>

<!--только с мобильных устройств.-->
<div class="b-message hidden">
    <a href="#" class="closepopup">Закрыть окно</a>
    <p>Уважаемый читатель. В связи с изменением регламента доступа к изданиям, защищенным авторским правом, их просмотр возможен только со стационарных компьютеров до момента выхода новой версии ЕИСУБ МО для мобильных устройств. Приносим Вам свои извинения.</p>
</div>
<!--только с мобильных устройств.-->

<!--popup доступ к закрытому изданию-->
<div class="popup_autoreg_form hidden">
	<a href="#" class="closepopup">Закрыть окно</a>
	<div class="ok tcenter hidden">
		<p>Вы успешно зарегистрированы как читатель РГБ</p>
		<p><a class="closed-link" href="#" <?/*?>onclick="messageOpen(this.href, '.b-message_warning'); return false;"<?*/?> target="_blank"><button type="submit" value="1" class="formbutton">Читать</button></a></p>
	</div>

	<form action="/local/tools/access_closed_books.php" method="post" class="b-form b-formautoreg">
		<?=bitrix_sessid_post()?>
		<?/*?><p>Доступ к закрытому изданию возможен только для читателей РГБ. Если Вы являетесь читателем РГБ, то введите номер читательского билета и пароль РГБ. Иначе пройдите полную регистрацию.</p><?*/?>
		<p>Издание, которое Вы пытаетесь открыть, защищено авторским правом. Право на доступ к защищённым изданиям имеют только пользователи, прошедшие полную регистрацию на ЕИСУБ МО и находящиеся в интернет-зале библиотеки, участвующей в проекте.</p>
		<div class="fieldrow nowrap">
			<div class="fieldcell iblock">
				<label for="settings11">E-mail</label>
				<div class="field validate">
					<input type="text"  value="" id="settings11"  name="login" class="input autoreglogin" data-minlength="2" data-maxlength="30" data-required="required" autocomplete="off">
				</div>
			</div>
		</div>
		<div class="fieldrow nowrap">
			<div class="fieldcell iblock">
				<label for="settings10">Пароль</label>
				<div class="field validate">
					<input type="password"  value="" id="settings10" name="password" class="input autoregpass" data-minlength="2" data-maxlength="30" data-required="required" autocomplete="off">
				</div>
			</div>
			<a href="/registration/" class="formbutton iblock">Зарегистрироваться</a>
		</div>
		<em class="error hidden">Неверный логин или пароль</em>
		<div class="fieldrow nowrap fieldrowaction">
			<div class="fieldcell ">
				<div class="field clearfix">
					<button class="formbutton left" value="1" type="submit">Войти</button>
				</div>
			</div>
		</div>
	</form>
</div><!-- /.popup_autoreg_form -->
<!--/popup доступ к закрытому изданию-->


<!--предупреждение о необходимости доп программы.-->
<div class="b-message_warning hidden">
	<a href="#" class="closepopup">Закрыть окно</a>
	<p>Программа, нужная для просмотра, не установлена.</p>
	<p>Скачайте её здесь:
		<?
			$userAgent = strtolower($_SERVER['HTTP_USER_AGENT']);
			if(strpos($userAgent, 'windows') !== false)
			{
			?>
			<br/>Для Windows: <a target="_blank" href="http://нэб.рф/distribs/viewer_windows.rar">http://нэб.рф/distribs/viewer_windows.rar</a>
			<?
			}
			elseif(strpos($userAgent, 'android') !== false)
			{
			?>
			<br/>Для Android: <a target="_blank" href="http://нэб.рф/distribs/viewer_Android.rar">http://нэб.рф/distribs/viewer_Android.rar</a>
			<?
			}
			elseif(strpos($userAgent, 'mac') !== false)
			{
			?>
			<br/>Для IOS: <a target="_blank" href="http://нэб.рф/distribs/viewer_IOS.rar">http://нэб.рф/distribs/viewer_IOS.rar</a>
			<?
			}
			else
			{
			?>
			<br/>Для Windows: <a target="_blank" href="http://нэб.рф/distribs/viewer_windows.rar">http://нэб.рф/distribs/viewer_windows.rar</a>
			<br/>Для IOS: <a target="_blank" href="http://нэб.рф/distribs/viewer_IOS.rar">http://нэб.рф/distribs/viewer_IOS.rar</a>
			<br/>Для Android: <a target="_blank" href="http://нэб.рф/distribs/viewer_Android.rar">http://нэб.рф/distribs/viewer_Android.rar</a>
			<?
			}
		?>
	</p>
</div>
<!--/предупреждение о необходимости доп программы.-->
