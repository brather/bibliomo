<?
	if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
	use \Bitrix\Main\Localization\Loc;
	Loc::loadMessages(__FILE__);
	
	$url = 'http://'.$_SERVER['SERVER_NAME'].$APPLICATION->GetCurPage();
?>

<a title="<?=$APPLICATION->GetTitle(false)?>" href="<?=$url?>" class="b-login_slnk vklink iblock"><?=Loc::getMessage('VKONTAKTE')?></a>
<a title="<?=$APPLICATION->GetTitle(false)?>" href="<?=$url?>" class="b-login_slnk odnlink iblock"><?=Loc::getMessage('ODNOKLASSNIKI')?></a>
<a title="<?=$APPLICATION->GetTitle(false)?>" href="<?=$url?>" class="b-login_slnk fblink iblock"><?=Loc::getMessage('FACEBOOK')?></a>
