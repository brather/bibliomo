<?
    Use \Bitrix\Main\Localization\Loc;
    Loc::loadMessages(__FILE__);
?>
<ul class="b-footer_nav">
	<li><?=Loc::getMessage('INCLUDE_MINISTR_TEXT')?></li>
	<li><a href="http://mk.mosreg.ru/" target="_blank"><img src="<?=MARKUP?>i/ministr.png" alt="Министерство культуры Московской области"></a></li>
</ul>