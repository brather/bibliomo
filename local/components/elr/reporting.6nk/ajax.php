<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/bitrix/modules/main/include/prolog_before.php';
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 12.01.2017
 * Time: 16:41
 */

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use \Elr\Reporting\Report6nkTable;
use \Elr\Reporting\Report6nkValuesTable;
use Bitrix\Main\Application;
use \PhpOffice\PhpWord\IOFactory;
use \PhpOffice\PhpWord\PhpWord;
use \PhpOffice\PhpWord\TemplateProcessor;


$rq = Application::getInstance()->getContext()->getRequest();

if (!$rq->isAjaxRequest()) die(); /* не AJAX запрос */
if (!CSite::InGroup(array(1,6))) die(); /* не библиотекарь и не администратор*/
if (!Loader::includeModule('elr.reporting')) die(); /* не загружен модуль */

/* Процедура работы с документом */
if (!empty($rq->getPost('type')))
{
    /* Подтверждение библиотекарем */
    if ($rq->getPost('type') == 'confirm' && !empty($rq->getPost('id')))
    {
        Report6nkTable::update((int)$rq->getPost('id'), array('CONFIRMED' => 1));
        die();
    }

    /* Возврат на редактирвоание, только Администратором */
    if (CSite::InGroup(array(1)) && $rq->getPost('type') == 're-confirm' && !empty($rq->getPost('id')))
    {
        Report6nkTable::update((int)$rq->getPost('id'), array('CONFIRMED' => 0));
        die();
    }
}

class ExportToFile extends stdClass
{
    private $type = "";
    private $idReport = null;
    private $oldIdFile = null;

    public function __construct($idReport, $type)
    {
        $this->idReport = $idReport;
        $this->type = $type;
    }

    public function doProcess()
    {
        switch ($this->type)
        {
            case 'pdf':
                require_once $_SERVER['DOCUMENT_ROOT'] . "/local/php_interface/include/helpers/bootstrap.php";
                $path = $this->_toPDF();
                break;
            case 'word':
                require_once $_SERVER['DOCUMENT_ROOT'] . "/local/php_interface/include/helpers/bootstrap.php";
                $path = $this->_toWord();
                break;
            case 'excel':
                require_once $_SERVER['DOCUMENT_ROOT'] . "/local/modules/elr.monitoring/lib/excel/PHPExcel.php";
                $path = $this->_toExcel();
                break;
            default:
                throw new Exception(
                    Loc::getMessage( "EXC_NOT_ECSIST_EXT_FILE" ));
        }

        $idFile = $this->_makeFile($path);


        return $idFile;
    }

    private function _makeFile($tmpPath)
    {
        $arFile = CFile::MakeFileArray($tmpPath);
        $arFile["MODULE_ID"] = 'elr.reporting';

        if ($this->oldIdFile !== null)
        {
            $arFile["del"] = 'Y';
            $arFile["old_file"] = $this->oldIdFile;
        }

        $f = CFile::SaveFile(
            $arFile,
            "elr.reporting"
        );

        if(is_file($tmpPath))
            unlink($tmpPath);

        if ((int)$f > 0)
            Report6nkTable::update((int)$this->idReport, array(
                    "ID_" . strtoupper($this->type) => $f
                )
            );

        return $f;
    }

    private function _toExcel ()
    {
        $arResult = [];
        $arMap = [
            'YEAR' => "BV12",
            'D' => [
                'D' => "DF40",
                'M' => "DM40",
                'Y' => "DY40"
            ],
            '00' => [
                1 => "AV43",
                2 => "S45",
                3 => "BS2",
                4 => "BS3",
                5 => "BS4",
                6 => "BS5",
                20 => "BX36",
                21 => "AR36",
                22 => "AR40",
                23 => "CF40",
                30 => "R50",
                31 => "BL50",
                32 => "DF50"
            ],
            '01' => [
                2 => "I13",
                3 => "S13",
                4 => "AC13",
                5 => "AL13",
                6 => "AU13",
                7 => "BF13",
                8 => "BO13",
                9 => "BZ13",
                10 => "CO13",
                11 => "DD13",
                12 => "DP13",
                13 => "EB13",
                14 => "EQ13",
                15 => "L18",
                16 => "AK18",
                17 => "AZ18",
                18 => "BX18",
                19 => "CO18",
                20 => "DM18",
                21 => "EB18",
                22 => "EQ18",
                23 => "L23",
                24 => "AK23",
                25 => "BK23",
                26 => "CL23",
                27 => "DM23",
                28 => "EI23"
            ],
            '02' => [
                3 => "AT6",
                4 => "BJ6",
                5 => "CA6",
                6 => "CO6",
                7 => "DC6",
                8 => "DQ6",
                9 => "EF6",
                10 => "ES6"
            ],
            '03' => [
                3 => "AT7",
                4 => "BJ7",
                5 => "CA7",
                6 => "CO7",
                7 => "DC7",
                8 => "DQ7",
                9 => "EF7",
                10 => "ES7"
            ],
            '04' => [
                3 => "AT8",
                4 => "BJ8",
                5 => "CA8",
                6 => "CO8",
                7 => "DC8",
                8 => "DQ8",
                9 => "EF8",
                10 => "ES8"
            ],
            '05' => [
                3 => "AT9",
                4 => "BJ9",
                6 => "CO9",
                7 => "DC9",
                9 => "EF9",
                10 => "ES9"
            ],
            '06' => [
                3 => "AO16",
                4 => "BC16",
                5 => "BS16",
                6 => "CI16",
                7 => "DB16",
                8 => "DO16",
                9 => "ED16",
                10 => "EQ16"
            ],
            '07' => [
                3 => "AO17",
                4 => "BC17",
                5 => "BS17",
                6 => "CI17",
                7 => "DB17",
                8 => "DO17",
                9 => "ED17",
                10 => "EQ17"
            ],
            '08' => [
                3 => "AO18",
                4 => "BC18",
                5 => "BS18",
                6 => "CI18",
                7 => "DB18",
                8 => "DO18",
                9 => "ED18",
                10 => "EQ18"
            ],
            '09' => [ 1 => "AU2"],
            '10' => [ 1 => "BV3"],
            '11' => [ 1 => "CK4"],
            '11а'=> [ 1 => "CZ5"], // 11а - "а" кирилица
            '12' => [
                2 => "J13",
                3 => "W13",
                4 => "AJ13",
                5 => "AZ13",
                6 => "BO13",
                7 => "CE13",
                8 => "CR13",
                9 => "DI13",
                10 => "DY13",
                11 => "EL13"
            ],
            '13' => [
                3 => "Z21",
                4 => "AL21",
                5 => "AY21",
                6 => "BL21",
                7 => "BY21",
                8 => "CN21",
                9 => "CZ21",
                10 => "DM21",
                11 => "DZ21",
                12 => "EQ21"
            ],
            '14' => [
                3 => "Z22",
                4 => "AL22",
                5 => "AY22",
                6 => "BL22",
                7 => "BY22",
                8 => "CN22",
                9 => "CZ22",
                10 => "DM22",
                11 => "DZ22",
                12 => "EQ22"
            ],
            '15' => [
                3 => "Z23",
                4 => "AL23",
                5 => "AY23",
                6 => "BL23",
                7 => "BY23",
                8 => "CN23",
                9 => "CZ23",
                10 => "DM23",
                11 => "DZ23",
                12 => "EQ23"
            ],
            '16' => [
                3 => "Z24",
                4 => "AL24",
                5 => "AY24",
                7 => "BY24",
                11 => "DZ24",
                12 => "EQ24"
            ],
            '17' => [
                3 => "Z25",
                4 => "AL25",
                5 => "AY25",
                6 => "BL25",
                7 => "BY25",
                8 => "CN25",
                9 => "CZ25",
                10 => "DM25",
                11 => "DZ25",
                12 => "EQ25"
            ],
            '18' => [
                2 => "I9",
                3 => "U9",
                4 => "AE9",
                5 => "AO9",
                6 => "AY9",
                7 => "BL9",
                8 => "BV9",
                9 => "CF9",
                10 => "CP9",
                11 => "CZ9",
                12 => "DJ9",
                13 => "DT9",
                14 => "EC9",
                15 => "EM9",
                16 => "EW9"
            ],
            '19' => [
                2 => "J17",
                3 => "Y17",
                4 => "AT17",
                5 => "BO17",
                6 => "CJ17",
                7 => "DB17",
                8 => "DU17",
                9 => "EO17",
                10 => "J23",
                11 => "AF23",
                12 => "BB23",
                13 => "BX23",
                14 => "CS23",
                15 => "DO23",
                16 => "EK23",
                17 => "J29",
                18 => "AA29",
                19 => "AR29",
                20 => "BI29",
                21 => "BY29",
                22 => "CP29",
                23 => "DG29",
                24 => "DX29",
                25 => "EO29"
            ],
        ];
        $monthRus = [
            "January" => "января",
            "February" => "февраля",
            "March" => "марта",
            "April" => "апреля",
            "June" => "июня",
            "July" => "июля",
            "August" => "августа",
            "September" => "сентября",
            "October" => "октября",
            "November" => "ноября",
            "December" => "декабря",
        ];

        $db = Report6nkTable::getById($this->idReport);

        /* @var Bitrix\Main\Type\DateTime $res['DATE_CREATE'] */
        if ($res = $db->fetch())
        {
            $arResult[ $arMap['YEAR'] ]     = $res['YEAR'];
            $arResult[ $arMap['D']['D'] ]   = date("d", $res['DATE_CREATE']->getTimestamp());
            $arResult[ $arMap['D']['M'] ]   = " " . $monthRus[date("F", $res['DATE_CREATE']->getTimestamp())] . " ";
            $arResult[ $arMap['D']['Y'] ]   = date("Y", $res['DATE_CREATE']->getTimestamp());
            $this->oldIdFile = $res['ID_' . strtoupper($this->type)];
        }

        $db = Report6nkValuesTable::getList(
            [
                'filter' => [ 'REPORT_ID' => $this->idReport ],
            ]
        );


        while ($res = $db->fetch())
        {
            $arResult[ $arMap[ $res['STRING'] ][ $res['NUMBER'] ] ] = (string)$res['VALUE'];
        }

        /* Заполнить отсутсвующие значения arResult пустыми значениями */
        foreach ( $arMap as $item )
        {
            if (is_array($item))
            {
                foreach ( $item as $val )
                {
                    if (!isset($arResult[ $val ]))
                        $arResult[ $val ] = "";
                }
            }
            else
            {
                if (!isset($arResult[ $item ]))
                    $arResult[ $item ] = "";
            }
        }

        $fileTemplate = $_SERVER['DOCUMENT_ROOT'] . '/local/components/elr/reporting.6nk/6NK.xlsx';

        $excel = PHPExcel_IOFactory::createReader('Excel2007');
        /* @var PHPExcel $xlsx */
        $xlsx = $excel->load($fileTemplate); // Empty Sheet

        unset($excel);

        $arSheet = [
            0 => ['YEAR','00_1','00_2','00_30','00_31','00_32'],
            1 => ['00_3','00_4','00_5','00_6','01'],
            2 => ['02','03','04','05','06','07','08'],
            3 => ['09','10','11','11а','12','13','14','15','16','17'], // 11а - "а" кирилица
            4 => ['D','00_20','00_21','00_22','00_23','19','18'],
        ];

        foreach ( $arSheet as $sheet => $item )
        {
            $xlsx->setActiveSheetIndex($sheet);
            $aSheet = $xlsx->getActiveSheet();

            foreach ($item as $vv)
            {
                /* Для не групповых значений */
                if (isset($arMap[ $vv ]) && !is_array($arMap[ $vv ]))
                    $aSheet->setCellValue( $arMap[ $vv ], $arResult[ $arMap[ $vv ] ] );

                /* Для групповых значений */
                if (isset($arMap[ $vv ]) && is_array($arMap[ $vv ]))
                    foreach ($arMap[ $vv ] as $v)
                        $aSheet->setCellValue( $v, $arResult[ $v ]);

                if (strpos($vv, "_") > 0)
                {
                    $arTmp = explode("_", $vv);

                    $aSheet->setCellValue( $arMap[ $arTmp[0] ][ $arTmp[1] ], $arResult[ $arMap[ $arTmp[0] ][ $arTmp[1] ] ] );
                }
            }
        }

        $objWriter = PHPExcel_IOFactory::createWriter($xlsx, 'Excel2007');
        $fileName = $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR .  "6NK_" . $this->idReport .'.xlsx';
        $objWriter->save($fileName);

        return $fileName;
    }

    private function _toPDF()
    {
        $pathToWordFile = $this->_toWord();

        exec("unoconv -f pdf " . $pathToWordFile);


        unlink($pathToWordFile);

        return preg_replace("#\.docx#", ".pdf", $pathToWordFile);
    }

    private function _toWord()
    {
        $arResult = [
            'YEAR' => "",
            'D' => ['D' => " ", 'M' => " ", 'Y' => " " ],
            '00' => [ 1 => "", 2 => "", 3 => "", 4 => "", 5 => "", 6 => "", 20 => "",
                      21 => "", 22 => "", 23 => "", 30 => "", 31 => "", 32 => "" ],
            '01' => [
                2 => "", 3 => "", 4 => "", 5 => "", 6 => "", 7 => "", 8 => "", 9 => "",
                10 => "", 11 => "", 12 => "", 13 => "", 14 => "", 15 => "", 16 => "",
                17 => "", 18 => "", 19 => "", 20 => "", 21 => "", 22 => "", 23 => "",
                24 => "", 25 => "", 26 => "", 27 => "", 28 => "" ],
            '02' => [ 3 => "", 4 => "", 5 => "", 6 => "", 7 => "", 8 => "", 9 => "", 10 => "" ],
            '03' => [ 3 => "", 4 => "", 5 => "", 6 => "", 7 => "", 8 => "", 9 => "", 10 => "" ],
            '04' => [ 3 => "", 4 => "", 5 => "", 6 => "", 7 => "", 8 => "", 9 => "", 10 => "" ],
            '05' => [ 3 => "", 4 => "", 6 => "", 7 => "", 9 => "", 10 => "" ],
            '06' => [ 3 => "", 4 => "", 5 => "", 6 => "", 7 => "", 8 => "", 9 => "", 10 => "" ],
            '07' => [ 3 => "", 4 => "", 5 => "", 6 => "", 7 => "", 8 => "", 9 => "", 10 => "" ],
            '08' => [ 3 => "", 4 => "", 5 => "", 6 => "", 7 => "", 8 => "", 9 => "", 10 => "" ],
            '09' => [ 1 => ""],
            '10' => [ 1 => ""],
            '11а'=> [ 1 => ""],
            "11" => [ 1 => " "],
            '12' => [ 2 => "", 3 => "", 4 => "", 5 => "", 6 => "", 7 => "", 8 => "", 9 => "", 10 => "", 11 => "" ],
            '13' => [ 3 => "", 4 => "", 5 => "", 6 => "", 7 => "", 8 => "", 9 => "", 10 => "", 11 => "", 12 => "" ],
            '14' => [ 3 => "", 4 => "", 5 => "", 6 => "", 7 => "", 8 => "", 9 => "", 10 => "", 11 => "", 12 => "" ],
            '15' => [ 3 => "", 4 => "", 5 => "", 6 => "", 7 => "", 8 => "", 9 => "", 10 => "", 11 => "", 12 => "" ],
            '16' => [ 3 => "", 4 => "", 5 => "", 7 => "", 11 => "", 12 => "" ],
            '17' => [ 3 => "", 4 => "", 5 => "", 6 => "", 7 => "", 8 => "", 9 => "", 10 => "", 11 => "", 12 => "" ],
            '18' => [ 2 => "", 3 => "", 4 => "", 5 => "", 6 => "", 7 => "", 8 => "", 9 => "",
                      10 => "", 11 => "", 12 => "", 13 => "", 14 => "", 15 => "", 16 => "" ],
            '19' => [ 2 => "", 3 => "", 4 => "", 5 => "", 6 => "", 7 => "", 8 => "", 9 => "",
                      10 => "", 11 => "", 12 => "", 13 => "", 14 => "", 15 => "", 16 => "",
                      17 => "", 18 => "", 19 => "", 20 => "", 21 => "", 22 => "", 23 => "",
                      24 => "", 25 => "" ],
        ];

        $monthRus = [
            "January" => "января",
            "February" => "февраля",
            "March" => "марта",
            "April" => "апреля",
            "June" => "июня",
            "July" => "июля",
            "August" => "августа",
            "September" => "сентября",
            "October" => "октября",
            "November" => "ноября",
            "December" => "декабря",
        ];

        $db = Report6nkTable::getById($this->idReport);

        /* @var Bitrix\Main\Type\DateTime $res['DATE_CREATE'] */
        if ($res = $db->fetch())
        {
            $arResult['YEAR'] = $res['YEAR'];
            $arResult['D']['D'] = date("d", $res['DATE_CREATE']->getTimestamp());
            $arResult['D']['M'] = " " . $monthRus[date("F", $res['DATE_CREATE']->getTimestamp())] . " ";
            $arResult['D']['Y'] = date("Y", $res['DATE_CREATE']->getTimestamp());
            $this->oldIdFile = $res['ID_' . strtoupper($this->type)];
        }

        $rsElements = Report6nkValuesTable::getList([
            'filter' => ['REPORT_ID' => $this->idReport]
        ]);
        while ($arFields = $rsElements->fetch()) {
            $string = (string)$arFields['STRING'];
            $number = (string)$arFields['NUMBER'];
            $arResult[$string][$number] = (string)$arFields['VALUE'];
        }

        $fileTemplate = $_SERVER['DOCUMENT_ROOT'] . '/local/components/elr/reporting.6nk/6NK.docx';
        $templateProcessor = new TemplateProcessor($fileTemplate);

        foreach ($arResult as $line => $item)
        {
            if (is_array($item))
            {
                foreach ($item as $cell => $val)
                {
                    $templateProcessor->setValue((string)$line . "_" . (string)$cell, $val);
                }
            }
            else
            {
                $templateProcessor->setValue((string)$line, (string)$item);
            }
        }

        $filePath = $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "6NK_" . $this->idReport. ".docx";

        $templateProcessor->saveAs($filePath);

        return $filePath;
    }
}

$obj = new ExportToFile($rq->getPost('id'), $rq->getPost('document'));

$idFile = $obj->doProcess();

echo CFile::GetPath($idFile);