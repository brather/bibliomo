<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}

use \Bitrix\Main\Entity;
use \Bitrix\Main\Loader;
use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Type\DateTime;
use \Bitrix\Main\GroupTable;
use \Bitrix\Main\UserTable;
use \Bitrix\Iblock\ElementTable;

use \Elr\Reporting\Report6nkTable;
use \Elr\Reporting\Report6nkValuesTable;

Loc::loadMessages(__FILE__);

class Reporting6nk extends CBitrixComponent
{
	private $arUser = [];

	public function onPrepareComponentParams($arParams)
	{
		return static::getUriParams($arParams);
	}

	/**
	 * Получает массив из ЧПУ
	 */
	private static function getUriParams($arParams) {

		$arDefaultUrlTemplates404 = array(
			"template" => "",
			"detail"   => "#ID#/",
		);

		$arDefaultVariableAliases404 = array();

		$arDefaultVariableAliases = array();

		$arComponentVariables = array("ID");

		$SEF_FOLDER = "";
		$arUrlTemplates = array();

		if ($arParams["SEF_MODE"] == "Y")
		{
			$arVariables = array();

			$arUrlTemplates = CComponentEngine::makeComponentUrlTemplates($arDefaultUrlTemplates404, $arParams["SEF_URL_TEMPLATES"]);
			$arVariableAliases = CComponentEngine::makeComponentVariableAliases($arDefaultVariableAliases404, $arParams["VARIABLE_ALIASES"]);

			$componentPage = CComponentEngine::parseComponentPath($arParams["SEF_FOLDER"], $arUrlTemplates, $arVariables);

			if (StrLen($componentPage) <= 0)
				$componentPage = "template";

			CComponentEngine::initComponentVariables($componentPage, $arComponentVariables, $arVariableAliases, $arVariables);

			$SEF_FOLDER = $arParams["SEF_FOLDER"];

		} else {

			$arVariables = array();

			$arVariableAliases = CComponentEngine::makeComponentVariableAliases($arDefaultVariableAliases, $arParams["VARIABLE_ALIASES"]);
			CComponentEngine::initComponentVariables(false, $arComponentVariables, $arVariableAliases, $arVariables);

			$componentPage = "";
			if (intval($arVariables["ELEMENT_ID"]) > 0) {
				$componentPage = "detail";
			} else {
				$componentPage = "template";
			}
		}

		$arParams['VARIABLES'] = $arVariables;
		$arParams['COMPONENT_PAGE'] = $componentPage;

		return $arParams;
	}

	public function executeComponent() {

		$this->getUser();

		// детальная страница
		if ($this->arParams['COMPONENT_PAGE'] == 'detail') {
			$this->detailPage(intval($this->arParams['VARIABLES']['ID']));
		}
		// страница списка
		else {
			$this->listPage();
		}
	}

	private function detailPage($iReport) {

		Loader::includeModule('elr.reporting');

		$this->arResult = $this->getReportById($iReport);

		if ($_REQUEST['save'] == 'Сохранить') {
			$this->saveReport($iReport, $_REQUEST['R']);
		}
		//debugPre($_REQUEST); debugPre($this->arResult);

        if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_FILES) && count($_FILES) > 0)
        {
            $ext = substr($_FILES['SCAN_FILE']['name'], strripos($_FILES['SCAN_FILE']['name'], "."));
            $file = $_FILES['SCAN_FILE'];
            $file['name'] = date("Y_m_d_H_i_s_" . $iReport) . $ext;
            if ($file['size'] < (10 * 1024 * 1024) )
            {
                if ($this->arResult['SCAN_FILE'] !== null)
                {
                    $file['old_file'] = $this->arResult['SCAN_FILE'];
                    $file['del'] = "Y";
                }

                $file['MODULE_ID'] = "elr.reporting";

                $this->arResult['SCAN_FILE'] = CFile::SaveFile($file, "elr.reporting");

                Report6nkTable::update($iReport, array("SCAN_FILE" => $this->arResult['SCAN_FILE']));
            }
        }

		$this->includeComponentTemplate('detail');
	}

	private function listPage() {

		Loader::includeModule('elr.reporting');

		$iUserId = $this->getUserId();

		$this->arResult               = $this->getFilterData();
		$this->arResult['ITEMS']      = $this->getReportList($iUserId);
		$this->arResult['USER']       = $this->arUser;
		$this->arResult['IS_LIBRARY'] = $iUserId > 0;

		//debugPre($this->arResult['LIBS']);

		$this->includeComponentTemplate();
	}

	/** ============================================================================================================= */

	/**
	 * Получение списка отчетов
	 *
	 * @return array
	 * @throws \Bitrix\Main\ArgumentException
	 */
	public static function getReportList($iUserId = 0)
	{
		$arResult = $arFilter = [];

		if (intval($_REQUEST['LIBRARY']))
			$arFilter['LIBRARY_ID'] = intval($_REQUEST['LIBRARY']);
		if (intval($_REQUEST['USER']))
			$arFilter['USER_ID'] = intval($_REQUEST['USER']);
		if (intval($_REQUEST['YEAR']))
			$arFilter['YEAR'] = intval($_REQUEST['YEAR']);

		$arFilter = [
			'filter' => $arFilter,
			'order'  => ['ID' => 'DESC'],
			'select' => ['*'],
		];
		if ($iUserId) {
			$arFilter['filter']['USER_ID'] = $iUserId;
		}

		$rsElements = Report6nkTable::getList($arFilter);
		while ($arFields = $rsElements->fetch()) {
			$arResult[$arFields['ID']] = $arFields;
		}

		return $arResult;
	}

	/**
	 * Получение отчета по его идентификатору
	 *
	 * @param $iId
	 * @param bool $bProps
	 * @return array|false
	 * @throws \Bitrix\Main\ArgumentException
	 */
	public static function getReportById($iId, $bProps = true)
	{
		$arResult = [];

		$iId = intval($iId);
		if ($iId <= 0)
			return $arResult;

		$arResult = Report6nkTable::getList([
			'filter' => ['ID' => $iId]
		])->fetch();

		if ($arResult['ID'] > 0 && $bProps)
			$arResult['VALUES'] = self::getReportValues($iId);

		return $arResult;
	}

	/**
	 * Получение свойств отчета
	 *
	 * @param $iId
	 * @return array
	 * @throws \Bitrix\Main\ArgumentException
	 */
	public static function getReportValues($iId) {

		$arResult = [];

		$iId = intval($iId);
		if ($iId <= 0)
			return $arResult;

		$rsElements = Report6nkValuesTable::getList([
			'filter' => ['REPORT_ID' => $iId]
		]);
		while ($arFields = $rsElements->fetch()) {
			$arResult[$arFields['STRING']][$arFields['NUMBER']] = $arFields;
		}

		return $arResult;
	}

	public function saveReport($iId, $arRequest) {

		global $USER;

		$bAdd = false;
		$arAdd = $arUpdate = $arDelete = [];

		if (empty($_REQUEST['save']))
			return $arAdd;

		$arReport = self::getReportById($iId);

		// добавление отчета
		if (!$arReport['ID']) {
			$iId = $arReport['ID'] = Report6nkTable::add([
				'LIBRARY_ID'  => $this->arUser['UF_LIBRARY'],
				'USER_ID'     => $this->arUser['ID'],
				'YEAR'        => $arRequest['YEAR'],
				'DATE_CREATE' => new DateTime(),
				'COMMENT'     => $arRequest['COMMENT']
			])->getId();
			$bAdd = true;
		}
		// обновление отчета
		else {
			$arUpd = [];
			if (intval($arRequest['YEAR']) != intval($arReport['YEAR']))
				$arUpd['YEAR'] =$arRequest['YEAR'];
			if ($arRequest['COMMENT'] != $arReport['COMMENT'])
				$arUpd['COMMENT'] = $arRequest['COMMENT'];

			// укажем время обновления
            $arUpd["DATE_UPDATE"] = new Bitrix\Main\Type\DateTime();

			if (!empty($arUpd))
				Report6nkTable::update($iId, $arUpd);
		}

		// добавление/изменение свойств отчета
		foreach ($arRequest['V'] as $sColumnn => $arItem) {
			foreach ($arItem as $sRow => $sVal) {

				if (mb_strlen($sVal) > 0 && $sVal != $arReport['VALUES'][$sColumnn][$sRow]['VALUE']) {

					// обновить
					if ($arReport['VALUES'][$sColumnn][$sRow]['ID']) {

						$arNewVal = $arReport['VALUES'][$sColumnn][$sRow];
						$arNewVal['VALUE'] = $sVal;

						$arUpdate[$arReport['VALUES'][$sColumnn][$sRow]['ID']] = $arNewVal;
					}
					// добавить
					else {
						$arAdd[] = [
							'REPORT_ID' => $arReport['ID'],
							'STRING'    => $sColumnn,
							'NUMBER'    => $sRow,
							'VALUE'     => $sVal
						];
					}
				}
			}
		}

		// удаление пустых свойств отчета
		foreach ($arReport['VALUES'] as $sColumnn => $arItem) {
			foreach ($arItem as $sRow => $arVal) {
				if (empty($arRequest['V'][$sColumnn][$sRow]) && mb_strlen($arVal['VALUE']) > 0) {
					$arDelete[] = $arVal['ID'];
				}
			}
		}

		// действия с базой
		foreach ($arAdd as $arItem)
			Report6nkValuesTable::add($arItem);
		foreach ($arUpdate as $iId => $arItem)
			Report6nkValuesTable::update($iId, $arItem);
		foreach ($arDelete as $iId)
			Report6nkValuesTable::delete($iId);

		if ($bAdd) {
			LocalRedirect('/profile/form-6nk/' . intval($iId) . '/');
		}

		//debugPre($arReport); debugPre($arRequest);
	}

	private function getUser() {
		global $USER;
		$this->arUser = $USER->GetByID($USER->GetID())->Fetch();
	}

	/**
	 * Получение групп пользователя
	 *
	 * @return int
	 * @throws \Bitrix\Main\ArgumentException
	 */
	public function getUserId() {

		global $USER;

		$arGroups = [];

		if (!$USER->IsAuthorized())
			return 0;

		$rsUserGroups = GroupTable::getList([
			'filter' => ['ID' => $USER->GetUserGroupArray(), 'ACTIVE' => 'Y'],
			'select' => ['ID', 'STRING_ID']
		]);
		while ($arFields = $rsUserGroups->fetch())
			$arGroups[] = $arFields['STRING_ID'];

		if ($this->arUser['UF_LIBRARY'] && in_array('library_admin', $arGroups) && !in_array('admin', $arGroups))
			return $this->arUser['ID'];
		else
			return 0;
	}

	private function getFilterData() {

		$arResult = $arLibs = $arUsers = $arYear = [];

		// получение данных для фильтра
		$rsElements = Report6nkTable::getList(['select' => ['LIBRARY_ID', 'USER_ID', 'YEAR']]);
		while ($arFields = $rsElements->fetch()) {
			$arLibs[$arFields['LIBRARY_ID']] = $arFields['LIBRARY_ID'];
			$arUsers[$arFields['USER_ID']] = $arFields['USER_ID'];
			$arYear[$arFields['YEAR']] = $arFields['YEAR'];
		}

		// получение библиотек
		if (!empty($arLibs)) {
			Loader::includeModule('iblock');
			$rsElements = ElementTable::getList([
				"order"  => ["ID"],
				"filter" => ['IBLOCK_ID' => IBLOCK_ID_LIBRARY, 'ID' => array_values($arLibs), 'ACTIVE' => 'Y'],
				"select" => ["ID", "NAME"],
			]);
			while ($arFields = $rsElements->fetch())
				$arResult['LIBS'][$arFields['ID']] = $arFields;
		}

		// получение пользователей
		if (!empty($arUsers)) {
			$rsUsers = UserTable::getList([
				'filter' => ['ID' => array_values($arUsers)],
				'select' => ['ID', 'EMAIL', 'ACTIVE', 'SECOND_NAME', 'NAME', 'LAST_NAME']
			]);
			while ($arItem = $rsUsers->fetch()) {

				$arItem['FULL_NAME'] = trim($arItem['SECOND_NAME'] . ' ' . $arItem['NAME'] . ' ' . $arItem['LAST_NAME']);
				if (empty($arItem['FULL_NAME']))
					$arItem['FULL_NAME'] = $arItem['EMAIL'];
				$arItem['FULL_NAME'] .= ' [' . $arItem['ID'] . ']';

				$arResult['USERS'][$arItem['ID']] = $arItem;
			}
		}

		// получение лет
		sort($arYear);
		$arResult['YEARS'] = array_values($arYear);


		return $arResult;
	}
}