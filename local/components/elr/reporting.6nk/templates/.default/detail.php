<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

if ($_SERVER['REQUEST_METHOD'] == 'POST')
    LocalRedirect('?');

/* Библиотекарям подтвержденную форму смотреть нельзя */
global $USER, $APPLICATION;

CJSCore::RegisterExt("bootstrap_ext", Array(
    "js"    =>  "/bitrix/js/elr.useraccess/bootstrap.min.js",
    "css"   =>  "/bitrix/css/elr.useraccess/bootstrap.min.css",
    "rel"   =>  array('jquery2'),
    "skip_core" => "Y"
));
CJSCore::Init(array("bootstrap_ext"));

function inputText($sColumnn, $sRow, $sAttr = ''){
    return '<input type="text" '. $sAttr.' name="R[V]['.$sColumnn.']['.$sRow.']"'
        .'value="' . (false === strpos($sAttr, 'placeholder="X"') ? $_REQUEST['R']['V'][$sColumnn][$sRow] : '') .'" />';
}

//debugPre($arResult);

foreach ($arResult['VALUES'] as $sColumnn => $arItem)
    foreach ($arItem as $sRow => $arVal)
        if (!isset($_REQUEST['R']['V'][$sColumnn][$sRow]))
            $_REQUEST['R']['V'][$sColumnn][$sRow] = $arVal['VALUE'];

?>

<div class="wrapper">
    <div class="menu-top">
        <?$APPLICATION->IncludeComponent("bitrix:menu", "lk_left", Array(
            "ROOT_MENU_TYPE" => "left",	// Тип меню для первого уровня
            "MENU_CACHE_TYPE" => "N",	// Тип кеширования
            "MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
            "MENU_CACHE_USE_GROUPS" => "N",	// Учитывать права доступа
            "MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
                0 => "",
            ),
            "MAX_LEVEL" => "1",	// Уровень вложенности меню
            "CHILD_MENU_TYPE" => "",	// Тип меню для остальных уровней
            "USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
            "DELAY" => "N",	// Откладывать выполнение шаблона меню
            "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
        ),
            false
        );?>
    </div>

    <div class="container bootstrap">

    <h1 style="margin: 60px 0;">Форма 6-НК</h1>
    <div class="row" style="margin-top: 15px;">
        <div class="informer"></div>
        <div class="row">
            <form action="?" method="post" class="col-lg-12 reporting">

                <input type="hidden" name="R[ID]" value="<?=$arResult['ID']?>" />

                <table cellpadding="4" cellspacing="0" width="100%">
                    <col width="40%" />
                    <col width="60%" />
                    <tr>
                        <td style="text-align: left;">Отчетный год (4 цифры)</td>
                        <td><input type="text" name="R[YEAR]" value="<?=$_REQUEST['R']['YEAR'] ? : ($arResult['YEAR']?:date('Y'))?>" /></td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Комментарий к отчету</td>
                        <td><textarea name="R[COMMENT]"><?=$_REQUEST['R']['COMMENT'] ? : $arResult['COMMENT']?></textarea></td>
                    </tr>
                </table>

                <table cellpadding="4" cellspacing="0" width="100%">
                    <col width="40%" />
                    <col width="60%" />
                    <tr>
                        <td style="text-align: left;">Наименование отчитывающейся организации</td>
                        <td><?=inputText('00', '1')?></td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Почтовый адрес</td>
                        <td><?=inputText('00', '2')?></td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Наименование учредителя</td>
                        <td><?=inputText('00', '3')?></td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Наименование централизованной системы, в которую входит библиотека</td>
                        <td><?=inputText('00', '4')?></td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Наименование и тип библиотеки (библиотеки-филиала)</td>
                        <td><?=inputText('00', '5')?></td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Направление основной деятельности головной организации</td>
                        <td><?=inputText('00', '6')?></td>
                    </tr>
                </table>

                <table cellpadding="4" cellspacing="0" width="100%">
                    <col width="10%" />
                    <col width="30%" />
                    <col width="30%" />
                    <col width="30%" />
                    <tr>
                        <th rowspan="2">Код формы по ОКУД</th>
                        <th colspan="3">Код</th>
                    </tr>
                    <tr>
                        <th>отчитывающейся организации по ОКПО</th>
                        <th></th>
                        <th></th>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>2</td>
                        <td>3</td>
                        <td>4</td>
                    </tr>
                    <tr>
                        <td>0609521</td>
                        <td><?=inputText('00', '30')?></td>
                        <td><?=inputText('00', '31')?></td>
                        <td><?=inputText('00', '32')?></td>
                    </tr>
                </table>

                <h2>1. Материально-техническая база</h2>

                <p class="table-comment">Коды по ОКЕИ: единица - 642; квадратный метр - 055</p>
                <table cellpadding="4" cellspacing="0" width="100%">
                    <tr>
                        <th rowspan="2">№ строки</th>
                        <th colspan="2">Объекты культурного наследия</th>
                        <th colspan="3">Здания (помещения), доступные для лиц с нарушениями</th>
                        <th colspan="3">Площадь помещений, кв. м</th>
                        <th colspan="3">Площадь помещений по форме пользования (из гр. 7), кв. м</th>
                        <th colspan="2">Техническое состояние помещений (из гр. 10), кв. м</th>
                    </tr>
                    <tr>
                        <th>феде- рального значения (да - 1, нет - 0)</th>
                        <th>регио- нального значения (да - 1, нет - 0)</th>
                        <th>зрения (да - 1, нет - 0) </th>
                        <th>слуха (да - 1, нет - </th>
                        <th>опорно-двигатель-ного аппарата (да - 1,  нет - 0)</th>
                        <th>всего</th>
                        <th>для хранения фондов (из гр. 7)</th>
                        <th>для обслуживания пользователей (из гр. 7)</th>
                        <th>в оперативном управлении</th>
                        <th>по договору аренды</th>
                        <th>прочие</th>
                        <th>требует капитального ремонта</th>
                        <th>аварийное</th>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>2</td>
                        <td>3</td>
                        <td>4</td>
                        <td>5</td>
                        <td>6</td>
                        <td>7</td>
                        <td>8</td>
                        <td>9</td>
                        <td>10</td>
                        <td>11</td>
                        <td>12</td>
                        <td>13</td>
                        <td>14</td>
                    </tr>
                    <tr>
                        <td>01</td>
                        <td><?=inputText('01', '2')?></td>
                        <td><?=inputText('01', '3')?></td>
                        <td><?=inputText('01', '4')?></td>
                        <td><?=inputText('01', '5')?></td>
                        <td><?=inputText('01', '6')?></td>
                        <td><?=inputText('01', '7')?></td>
                        <td><?=inputText('01', '8')?></td>
                        <td><?=inputText('01', '9')?></td>
                        <td><?=inputText('01', '10')?></td>
                        <td><?=inputText('01', '11')?></td>
                        <td><?=inputText('01', '12')?></td>
                        <td><?=inputText('01', '13')?></td>
                        <td><?=inputText('01', '14')?></td>
                    </tr>
                </table>

                <table cellpadding="4" cellspacing="0" width="100%">
                    <tr>
                        <th rowspan="2">№ строки</th>
                        <th rowspan="2">Число пунктов вне стационарного обслуживания пользователей библиотеки, единиц</th>
                        <th colspan="3">Число посадочных мест для пользователей, единиц</th>
                        <th colspan="4">Наличие автоматизированных технологий (да - 1, нет - 0)</th>
                    </tr>
                    <tr>
                        <th>всего</th>
                        <th>из них (из гр.16) компьютеризованных, с возможностью доступа к электронным ресурсам библиотеки</th>
                        <th>из них (из гр.16) с возможностью выхода в Интернет</th>
                        <th>обработки поступлений и ведения электронного каталога (каталогизация и научная обработка)</th>
                        <th>организации и учета выдачи фондов (книговыдача)</th>
                        <th>организации и учета доступа посетителей (обслуживание)</th>
                        <th>учета документов библиотечного фонда (учет фондов)</th>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>15</td>
                        <td>16</td>
                        <td>17</td>
                        <td>18</td>
                        <td>19</td>
                        <td>20</td>
                        <td>21</td>
                        <td>22</td>
                    </tr>
                    <tr>
                        <td>01</td>
                        <td><?=inputText('01', '15')?></td>
                        <td><?=inputText('01', '16')?></td>
                        <td><?=inputText('01', '17')?></td>
                        <td><?=inputText('01', '18')?></td>
                        <td><?=inputText('01', '19')?></td>
                        <td><?=inputText('01', '20')?></td>
                        <td><?=inputText('01', '21')?></td>
                        <td><?=inputText('01', '22')?></td>
                    </tr>
                </table>

                <table cellpadding="4" cellspacing="0" width="100%">
                    <tr>
                        <th rowspan="2">№ строки</th>
                        <th rowspan="2">Число единиц специализированного оборудования для инвалидов</th>
                        <th colspan="3">число единиц копировально-множительной техники</th>
                        <th colspan="2">число транспортных средств, единиц</th>
                    </tr>
                    <tr>
                        <th>всего</th>
                        <th>из них для пользователей библиотеки(из гр. 24)</th>
                        <th>из них для оцифровки фонда (из гр. 24)</th>
                        <th>всего</th>
                        <th>из них число специализированных транспортных средств(из гр. 27)</th>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>23</td>
                        <td>24</td>
                        <td>25</td>
                        <td>26</td>
                        <td>27</td>
                        <td>28</td>
                    </tr>
                    <tr>
                        <td>01</td>
                        <td><?=inputText('01', '23')?></td>
                        <td><?=inputText('01', '24')?></td>
                        <td><?=inputText('01', '25')?></td>
                        <td><?=inputText('01', '26')?></td>
                        <td><?=inputText('01', '27')?></td>
                        <td><?=inputText('01', '28')?></td>
                    </tr>
                </table>


                <h2>2. Формирование библиотечного фонда на физических (материальных) носителях</h2>

                <p class="table-comment">Код по ОКЕИ: единица - 642</p>
                <table cellpadding="4" cellspacing="0" width="100%">
                    <tr>
                        <th rowspan="2">Наименование показателей</th>
                        <th rowspan="2">№ строки</th>
                        <th rowspan="2">Всего (сумма гр. 4 - 7) единиц</th>
                        <th colspan="4">В том числе (из гр. 3), единиц</th>
                        <th rowspan="2">Документы в специальных форматах для слепых и слабовидящих, единиц (из гр. 3)</th>
                        <th colspan="2">из общего объема (из гр. 3), единиц</th>
                    </tr>
                    <tr>
                        <th>печатные  издания и неопубликован-ные документы</th>
                        <th>электронные документы на съемных носителях</th>
                        <th>документы на микроформах</th>
                        <th>документы на других видах носителей</th>
                        <th>на языках народов России</th>
                        <th>на иностранных языках</th>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>2</td>
                        <td>3</td>
                        <td>4</td>
                        <td>5</td>
                        <td>6</td>
                        <td>7</td>
                        <td>8</td>
                        <td>9</td>
                        <td>10</td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Поступило документов за отчетный год, единиц</td>
                        <td>02</td>
                        <td><?=inputText('02', '3')?></td>
                        <td><?=inputText('02', '4')?></td>
                        <td><?=inputText('02', '5')?></td>
                        <td><?=inputText('02', '6')?></td>
                        <td><?=inputText('02', '7')?></td>
                        <td><?=inputText('02', '8')?></td>
                        <td><?=inputText('02', '9')?></td>
                        <td><?=inputText('02', '10')?></td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Выбыло документов за отчетный год, единиц</td>
                        <td>03</td>
                        <td><?=inputText('03', '3')?></td>
                        <td><?=inputText('03', '4')?></td>
                        <td><?=inputText('03', '5')?></td>
                        <td><?=inputText('03', '6')?></td>
                        <td><?=inputText('03', '7')?></td>
                        <td><?=inputText('03', '8')?></td>
                        <td><?=inputText('03', '9')?></td>
                        <td><?=inputText('03', '10')?></td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Состоит документов на конец отчетного года, единиц</td>
                        <td>04</td>
                        <td><?=inputText('04', '3')?></td>
                        <td><?=inputText('04', '4')?></td>
                        <td><?=inputText('04', '5')?></td>
                        <td><?=inputText('04', '6')?></td>
                        <td><?=inputText('04', '7')?></td>
                        <td><?=inputText('04', '8')?></td>
                        <td><?=inputText('04', '9')?></td>
                        <td><?=inputText('04', '10')?></td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Переведено в электронную форму за отчетный год, единиц</td>
                        <td>05</td>
                        <td><?=inputText('05', '3')?></td>
                        <td><?=inputText('05', '4')?></td>
                        <td><?=inputText('05', '5', 'placeholder="X" disabled="true"')?></td>
                        <td><?=inputText('05', '6')?></td>
                        <td><?=inputText('05', '7')?></td>
                        <td><?=inputText('05', '8', 'placeholder="X" disabled="true"')?></td>
                        <td><?=inputText('05', '9')?></td>
                        <td><?=inputText('05', '10')?></td>
                    </tr>
                </table>


                <h2>3. Формирование библиотечного фонда на физических (материальных) носителях</h2>

                <p class="table-comment">Код по ОКЕИ: единица - 642</p>
                <table cellpadding="4" cellspacing="0" width="100%">
                    <tr>
                        <th rowspan="2">Наименование показателей</th>
                        <th rowspan="2">№ строки</th>
                        <th colspan="2">Объем электронного каталога</th>
                        <th colspan="2">Объем электронной (цифровой) библиотеки</th>
                        <th colspan="2">Инсталированные документы</th>
                        <th colspan="2">Сетевые удаленные лицензионные документы</th>
                    </tr>
                    <tr>
                        <th>общее число записей, единиц</th>
                        <th>из них (из гр. 3) число записей, доступных в Интернете, единиц</th>
                        <th>общее число сетевых локальных документов, единиц</th>
                        <th>из них (из гр. 5) число документов в открытом доступе, единиц</th>
                        <th>число баз данных, единиц</th>
                        <th>в них полнотексто-вых документов, единиц</th>
                        <th>число баз данных, единиц</th>
                        <th>в них полнотексто-вых документов, единиц</th>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>2</td>
                        <td>3</td>
                        <td>4</td>
                        <td>5</td>
                        <td>6</td>
                        <td>7</td>
                        <td>8</td>
                        <td>9</td>
                        <td>10</td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Создано, приобретено за отчетный год, единиц</td>
                        <td>06</td>
                        <td><?=inputText('06', '3')?></td>
                        <td><?=inputText('06', '4')?></td>
                        <td><?=inputText('06', '5')?></td>
                        <td><?=inputText('06', '6')?></td>
                        <td><?=inputText('06', '7')?></td>
                        <td><?=inputText('06', '8')?></td>
                        <td><?=inputText('06', '9')?></td>
                        <td><?=inputText('06', '10')?></td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Выбыло за отчетный год, единиц</td>
                        <td>07</td>
                        <td><?=inputText('07', '3')?></td>
                        <td><?=inputText('07', '4')?></td>
                        <td><?=inputText('07', '5')?></td>
                        <td><?=inputText('07', '6')?></td>
                        <td><?=inputText('07', '7')?></td>
                        <td><?=inputText('07', '8')?></td>
                        <td><?=inputText('07', '9')?></td>
                        <td><?=inputText('07', '10')?></td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Объем на конец отчетного года, единиц</td>
                        <td>08</td>
                        <td><?=inputText('08', '3')?></td>
                        <td><?=inputText('08', '4')?></td>
                        <td><?=inputText('08', '5')?></td>
                        <td><?=inputText('08', '6')?></td>
                        <td><?=inputText('08', '7')?></td>
                        <td><?=inputText('08', '8')?></td>
                        <td><?=inputText('08', '9')?></td>
                        <td><?=inputText('08', '10')?></td>
                    </tr>
                </table>


                <table cellpadding="4" cellspacing="0" width="100%">
                    <col width="40%" />
                    <col width="60%" />
                    <tr>
                        <td style="text-align: left;">Наличие доступа в Интернет (да - 1, нет - 0) (09)</td>
                        <td><?=inputText('09', '1')?></td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Наличие возможности доступа в Интернет для посетителей (да - 1, нет - 0) (10)</td>
                        <td><?=inputText('10', '1')?></td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Наличие собственного Интернет-сайта или Интернет-страницы библиотеки (да - 1, нет - 0) (11)</td>
                        <td><?=inputText('11', '1')?></td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Наличие Интернет-сайта или Интернет-страницы, доступного для слепых и слабовидящих (да - 1, нет - 0) (11а)</td>
                        <td><?=inputText('11а', '1') // а - русская ?></td>
                    </tr>
                </table>


                <h2>4. Число пользователей и посещений библиотеки</h2>

                <p class="table-comment">Код по ОКЕИ: единица - 642; человек - 792; посещение - 5451</p>
                <table cellpadding="4" cellspacing="0" width="100%">
                    <tr>
                        <th rowspan="3">№ строки</th>
                        <th colspan="3">Число зарегистрированных пользователей библиотеки, человек</th>
                        <th colspan="3">Число посещений библиотеки, посещений</th>
                        <th colspan="2">Число обращений к библиотеке удаленных пользователей, единиц</th>
                    </tr>
                    <tr>
                        <th rowspan="2">всего</th>
                        <th colspan="3">в том числе пользователей, обслуженных в стенах библиотеки (из гр. 2)</th>
                        <th rowspan="2">в том числе удаленных пользователей (из гр. 2)</th>
                        <th rowspan="2">всего</th>
                        <th rowspan="2">из них для получения библиотечно-информацион-ных услуг (из гр. 7)</th>
                        <th rowspan="2">число посещений массовых мероприятий (из гр. 7)</th>
                        <th rowspan="2">всего</th>
                        <th rowspan="2">из них обращений к веб-сайту (из гр. 10)</th>
                    </tr>
                    <tr>
                        <th>всего</th>
                        <th>из них (из гр. 3) дети до 14 лет включительно</th>
                        <th>из них (из гр. 3) молодежь 15 - 30 лет</th>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>2</td>
                        <td>3</td>
                        <td>4</td>
                        <td>5</td>
                        <td>6</td>
                        <td>7</td>
                        <td>8</td>
                        <td>9</td>
                        <td>10</td>
                        <td>11</td>
                    </tr>
                    <tr>
                        <td>12</td>
                        <td><?=inputText('12', '2')?></td>
                        <td><?=inputText('12', '3')?></td>
                        <td><?=inputText('12', '4')?></td>
                        <td><?=inputText('12', '5')?></td>
                        <td><?=inputText('12', '6')?></td>
                        <td><?=inputText('12', '7')?></td>
                        <td><?=inputText('12', '8')?></td>
                        <td><?=inputText('12', '9')?></td>
                        <td><?=inputText('12', '10')?></td>
                        <td><?=inputText('12', '11')?></td>
                    </tr>
                </table>

                <h2>5. Библиотечно-информационное обслуживание пользователей</h2>

                <p class="table-comment">Код по ОКЕИ: единица - 642</p>
                <table cellpadding="4" cellspacing="0" width="100%">
                    <tr>
                        <th rowspan="3">Режимы обслуживания</th>
                        <th rowspan="3">№ строки</th>
                        <th colspan="5">Выдано (просмотрено) документов из фондов данной библиотеки, единиц</th>
                        <th colspan="3">Выдано (просмотрено) документов из фондов других библиотек, единиц</th>
                        <th rowspan="3">Изготовлено для пользователей и выдано копий, единиц</th>
                        <th rowspan="3">Выполнено справок и консультаций, единиц</th>
                    </tr>
                    <tr>
                        <th rowspan="2">всего</th>
                        <th colspan="4">в том числе (из гр. 3)</th>
                        <th rowspan="2">всего</th>
                        <th colspan="2">в том числе (из гр. 8)</th>
                    </tr>
                    <tr>
                        <th>из фонда на физических носителях</th>
                        <th>из элек-тронной (цифровой) библиотеки</th>
                        <th>инсталлиро-ванных документов</th>
                        <th>сетевых удаленных лицензионных документов</th>
                        <th>полученных по системе МБА и ММБА</th>
                        <th>доступных в виртуальных читальных залах</th>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>2</td>
                        <td>3</td>
                        <td>4</td>
                        <td>5</td>
                        <td>6</td>
                        <td>7</td>
                        <td>8</td>
                        <td>9</td>
                        <td>10</td>
                        <td>11</td>
                        <td>12</td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">В стационарном режиме</td>
                        <td>13</td>
                        <td><?=inputText('13', '3')?></td>
                        <td><?=inputText('13', '4')?></td>
                        <td><?=inputText('13', '5')?></td>
                        <td><?=inputText('13', '6')?></td>
                        <td><?=inputText('13', '7')?></td>
                        <td><?=inputText('13', '8')?></td>
                        <td><?=inputText('13', '9')?></td>
                        <td><?=inputText('13', '10')?></td>
                        <td><?=inputText('13', '11')?></td>
                        <td><?=inputText('13', '12')?></td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">в том числе детей до 14 лет включительно</td>
                        <td>14</td>
                        <td><?=inputText('14', '3')?></td>
                        <td><?=inputText('14', '4')?></td>
                        <td><?=inputText('14', '5')?></td>
                        <td><?=inputText('14', '6')?></td>
                        <td><?=inputText('14', '7')?></td>
                        <td><?=inputText('14', '8')?></td>
                        <td><?=inputText('14', '9')?></td>
                        <td><?=inputText('14', '10')?></td>
                        <td><?=inputText('14', '11')?></td>
                        <td><?=inputText('14', '12')?></td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">в том числе молодежь 15 - 30 лет</td>
                        <td>15</td>
                        <td><?=inputText('15', '3')?></td>
                        <td><?=inputText('15', '4')?></td>
                        <td><?=inputText('15', '5')?></td>
                        <td><?=inputText('15', '6')?></td>
                        <td><?=inputText('15', '7')?></td>
                        <td><?=inputText('15', '8')?></td>
                        <td><?=inputText('15', '9')?></td>
                        <td><?=inputText('15', '10')?></td>
                        <td><?=inputText('15', '11')?></td>
                        <td><?=inputText('15', '12')?></td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">В удаленном режиме</td>
                        <td>16</td>
                        <td><?=inputText('16', '3')?></td>
                        <td><?=inputText('16', '4')?></td>
                        <td><?=inputText('16', '5')?></td>
                        <td><?=inputText('16', '6', 'placeholder="X" disabled="true"')?></td>
                        <td><?=inputText('16', '7')?></td>
                        <td><?=inputText('16', '8', 'placeholder="X" disabled="true"')?></td>
                        <td><?=inputText('16', '9', 'placeholder="X" disabled="true"')?></td>
                        <td><?=inputText('16', '10', 'placeholder="X" disabled="true"')?></td>
                        <td><?=inputText('16', '11')?></td>
                        <td><?=inputText('16', '12')?></td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Всего (сумма строк 13 и 16)</td>
                        <td>17</td>
                        <td><?=inputText('17', '3')?></td>
                        <td><?=inputText('17', '4')?></td>
                        <td><?=inputText('17', '5')?></td>
                        <td><?=inputText('17', '6')?></td>
                        <td><?=inputText('17', '7')?></td>
                        <td><?=inputText('17', '8')?></td>
                        <td><?=inputText('17', '9')?></td>
                        <td><?=inputText('17', '10')?></td>
                        <td><?=inputText('17', '11')?></td>
                        <td><?=inputText('17', '12')?></td>
                    </tr>
                </table>

                <h2>6. Персонал библиотеки</h2>

                <p class="table-comment">Код по ОКЕИ: человек - 792</p>
                <table cellpadding="4" cellspacing="0" width="100%">
                    <tr>
                        <th rowspan="5">№ строки</th>
                        <th rowspan="5">Штат библиотеки на конец отчетного года, единиц</th>
                        <th colspan="14">Численность работников, человек</th>
                    </tr>
                    <tr>
                        <th rowspan="4">всего</th>
                        <th rowspan="4">имеют инвалид-ность (из гр. 3)</th>
                        <th colspan="12">из них (из гр. 3) основной персонал библиотеки</th>
                    </tr>
                    <tr>
                        <th rowspan="3">всего</th>
                        <th rowspan="3">из них прошли обучение (инструкти-рование) по вопросам, связанным с предоставле-нием услуг инвалидам (из гр. 3)</th>
                        <th colspan="4">из них имеют образование (из гр. 5)</th>
                        <th colspan="3" rowspan="2">в том числе со стажем работы в библиотеках (из гр. 5)</th>
                        <th colspan="3" rowspan="2">в том числе по возрасту (из гр. 5)</th>
                    </tr>
                    <tr>
                        <th colspan="2">высшее</th>
                        <th colspan="2">среднее профессиональное</th>
                    </tr>
                    <tr>
                        <th>высшее</th>
                        <th>из них библио-течное (из гр. 7)</th>
                        <th>высшее</th>
                        <th>из них библио-течное (из гр. 9)</th>

                        <th>от 0 до 3 лет</th>
                        <th>от 3 до 10 лет</th>
                        <th>свыше 10 лет </th>

                        <th>до 30 лет</th>
                        <th>от 30 до 55 лет</th>
                        <th>55 лет и старше</th>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>2</td>
                        <td>3</td>
                        <td>4</td>
                        <td>5</td>
                        <td>6</td>
                        <td>7</td>
                        <td>8</td>
                        <td>9</td>
                        <td>10</td>
                        <td>11</td>
                        <td>12</td>
                        <td>13</td>
                        <td>14</td>
                        <td>15</td>
                        <td>16</td>
                    </tr>
                    <tr>
                        <td>18</td>
                        <td><?=inputText('18', '2')?></td>
                        <td><?=inputText('18', '3')?></td>
                        <td><?=inputText('18', '4')?></td>
                        <td><?=inputText('18', '5')?></td>
                        <td><?=inputText('18', '6')?></td>
                        <td><?=inputText('18', '7')?></td>
                        <td><?=inputText('18', '8')?></td>
                        <td><?=inputText('18', '9')?></td>
                        <td><?=inputText('18', '10')?></td>
                        <td><?=inputText('18', '11')?></td>
                        <td><?=inputText('18', '12')?></td>
                        <td><?=inputText('18', '13')?></td>
                        <td><?=inputText('18', '14')?></td>
                        <td><?=inputText('18', '15')?></td>
                        <td><?=inputText('18', '16')?></td>
                    </tr>
                </table>


                <h2>7. Поступление и использование финансовых средств</h2>

                <p class="table-comment">Код по ОКЕИ: тысяча рублей - 384</p>
                <table cellpadding="4" cellspacing="0" width="100%">
                    <tr>
                        <th rowspan="3">№ строки</th>
                        <th rowspan="3">Поступило за год всего (сумма граф 3, 4, 5, 9)</th>
                        <th colspan="7">из них (из гр. 2)</th>
                    </tr>
                    <tr>
                        <th rowspan="2">бюджетные ассигнования  учредителя</th>
                        <th rowspan="2">финансирование из бюджетов других уровней</th>
                        <th rowspan="2">от приносящей доход деятельности</th>
                        <th colspan="3">из них (из гр. 5)</th>
                        <th rowspan="2">от сдачи имущества в аренду</th>
                    </tr>
                    <tr>
                        <th>от основных видов уставной деятельности</th>
                        <th>благотворительные и спонсорские вклады</th>
                        <th>от иной приносящей доход деятельности</th>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>2</td>
                        <td>3</td>
                        <td>4</td>
                        <td>5</td>
                        <td>6</td>
                        <td>7</td>
                        <td>8</td>
                        <td>9</td>
                    </tr>
                    <tr>
                        <td>19</td>
                        <td><?=inputText('19', '2')?></td>
                        <td><?=inputText('19', '3')?></td>
                        <td><?=inputText('19', '4')?></td>
                        <td><?=inputText('19', '5')?></td>
                        <td><?=inputText('19', '6')?></td>
                        <td><?=inputText('19', '7')?></td>
                        <td><?=inputText('19', '8')?></td>
                        <td><?=inputText('19', '9')?></td>
                    </tr>
                </table>

                <table cellpadding="4" cellspacing="0" width="100%">
                    <tr>
                        <th rowspan="3">№ строки</th>
                        <th rowspan="3">Израсходовано, всего</th>
                        <th colspan="8">из них (из гр. 10)</th>
                    </tr>
                    <tr>
                        <th colspan="3">расходы на оплату труда</th>
                        <th colspan="3">на комплектование фонда</th>
                    </tr>
                    <tr>
                        <th>всего</th>
                        <th>из общих расходов на оплату труда (из гр. 11) основному персоналу</th>
                        <th>из них за счет собственных средств (из гр. 12)</th>

                        <th>всего (из гр. 10)</th>
                        <th>из них на подписку на доступ к удаленным сетевым ресурсам (из гр. 14)</th>
                        <th>из них за счет собственных средств (из гр. 15)</th>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>10</td>
                        <td>11</td>
                        <td>12</td>
                        <td>13</td>
                        <td>14</td>
                        <td>15</td>
                        <td>16</td>
                    </tr>
                    <tr>
                        <td>19</td>
                        <td><?=inputText('19', '10')?></td>
                        <td><?=inputText('19', '11')?></td>
                        <td><?=inputText('19', '12')?></td>
                        <td><?=inputText('19', '13')?></td>
                        <td><?=inputText('19', '14')?></td>
                        <td><?=inputText('19', '15')?></td>
                        <td><?=inputText('19', '16')?></td>
                    </tr>
                </table>



                <table cellpadding="4" cellspacing="0" width="100%">
                    <tr>
                        <th rowspan="3">№ строки</th>
                        <th colspan="9">из них</th>
                    </tr>
                    <tr>
                        <th colspan="2">расходы на капитальный ремонт и реконструкцию</th>
                        <th colspan="3">расходы на приобретение (замену) оборудования</th>
                        <th colspan="2">на организацию и проведение мероприятий</th>
                        <th colspan="2">на информатизацию библиотечной деятельности, в т.ч. создание электронных каталогов и оцифровку библиотечного фонда</th>
                    </tr>
                    <tr>
                        <th>всего (из гр. 10)</th>
                        <th>из них за счет собственных средств</th>
                        <th>всего (из гр. 10)</th>
                        <th>из них для улучшения условий доступности для инвалидов и лиц с ОВЗ</th>
                        <th>из них за счет собственных средств (из гр. 19)</th>
                        <th>всего (из гр. 10)</th>
                        <th>из них за счет собственных средств</th>
                        <th>всего (из гр. 10)</th>
                        <th>из них за счет собственных средств</th>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>17</td>
                        <td>18</td>
                        <td>19</td>
                        <td>20</td>
                        <td>21</td>
                        <td>22</td>
                        <td>23</td>
                        <td>24</td>
                        <td>25</td>
                    </tr>
                    <tr>
                        <td>19</td>
                        <td><?=inputText('19', '17')?></td>
                        <td><?=inputText('19', '18')?></td>
                        <td><?=inputText('19', '19')?></td>
                        <td><?=inputText('19', '20')?></td>
                        <td><?=inputText('19', '21')?></td>
                        <td><?=inputText('19', '22')?></td>
                        <td><?=inputText('19', '23')?></td>
                        <td><?=inputText('19', '24')?></td>
                        <td><?=inputText('19', '25')?></td>
                    </tr>
                </table>

                <table cellpadding="4" cellspacing="0" width="100%">
                    <col width="40%" />
                    <col width="60%" />
                    <tr>
                        <td style="text-align: left;">Фамилия Имя Отчетство</td>
                        <td><?=inputText('00', '20')?></td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Должность</td>
                        <td><?=inputText('00', '21')?></td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Номер контактного телефона</td>
                        <td><?=inputText('00', '22')?></td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Электронная почта</td>
                        <td><?=inputText('00', '23')?></td>
                    </tr>
                </table>

                <div class="clearfix"></div>
                <div class="col-lg-3">
                    <input class="form-control" type="submit" name="save" value="Сохранить" />
                </div>
            </form>
        </div>
    </div>
        <div class="clearfix"></div>
        <div class="row">
<?
if (count($arResult) > 0)
{

    $du = $arResult['DATE_UPDATE']->toString();

    if ($arResult['ID_PDF'] !== null)
    {
        $filePDF = CFile::GetByID($arResult['ID_PDF'])->Fetch();

        /* @var DateInterval $res */
        $res = date_diff(new DateTime($du), new DateTime($filePDF['TIMESTAMP_X']));
        if ($res->invert === 0)
            $pathPdf = CFile::GetPath($arResult['ID_PDF']);
    }

    if ($arResult['ID_WORD'] !== null)
    {
        $fileWORD = CFile::GetByID($arResult['ID_WORD'])->Fetch();

        /* @var DateInterval $res */
        $res = date_diff(new DateTime($du), new DateTime($fileWORD['TIMESTAMP_X']));
        if ($res->invert === 0)
            $pathWord = CFile::GetPath($arResult['ID_WORD']);
    }

    if ($arResult['ID_EXCEL'] !== null)
    {
        $fileEXCEL = CFile::GetByID($arResult['ID_EXCEL'])->Fetch();

        /* @var DateInterval $res */
        $res = date_diff(new DateTime($du), new DateTime($fileEXCEL['TIMESTAMP_X']));
        if ($res->invert === 0)
            $pathExcel = CFile::GetPath($arResult['ID_EXCEL']);
    }

    unset($du, $filePDF, $fileWORD, $fileEXCEL, $res);

?>

            <div class="col-lg-4 col-lg-offset-3" style="text-align: right;">
                <? if ($arResult['SCAN_FILE'] == NULL): ?>
                <form enctype="multipart/form-data" method="post" id="upload-scan" action="?">
                    <label for="scan-file" class="btn btn-default">Загрузить скан &nbsp;<span class="glyphicon glyphicon-cloud-upload"></span></label>
                    <input type="file" id="scan-file" name="SCAN_FILE" />
                </form>
                <? else: ?>
                    <div class="btn-group">
                        <a download href="<?=CFile::GetPath($arResult['SCAN_FILE']); ?>" class="btn btn-default"><span class="glyphicon glyphicon-cloud-download"></span>&nbsp; Скачать</a>
                        <label for="scan-file" class="btn btn-default">Загрузить новый &nbsp;<span class="glyphicon glyphicon-cloud-upload"></span></label>
                    </div>
                    <form enctype="multipart/form-data" method="post" id="upload-scan" action="?">
                        <input type="file" id="scan-file" name="SCAN_FILE" />
                    </form>
                <? endif; ?>
            </div>
            <div class="col-lg-4 col-lg-offset-1" style="text-align: right">
                <div class="btn-group export-doc-to-file">
                    <button data-type="excel" type="button" class="export-document btn btn-green btn-default btn-success" >Excel</button>
            <? if (isset($pathExcel)):?>
                    <a href="<?=$pathExcel ?>" class="btn btn-green btn-default btn-success" ><span class="glyphicon glyphicon-download-alt"></span></a>
                </div>
                <div class="btn-group export-doc-to-file">
            <? endif; ?>
                    <button data-type="word" type="button" class="export-document btn btn-green btn-default btn-info" >Word</button>
            <? if (isset($pathWord)):?>
                    <a href="<?=$pathWord ?>" class="btn btn-green btn-default btn-info" ><span class="glyphicon glyphicon-download-alt"></span></a>
                </div>
                <div class="btn-group export-doc-to-file">
            <? endif; ?>
                    <button data-type="pdf" type="button" class="export-document btn btn-green btn-default btn-danger" >PDF</button>
            <? if (isset($pathPdf)):?>
                    <a href="<?=$pathPdf ?>" class="btn btn-green btn-default btn-danger" ><span class="glyphicon glyphicon-download-alt"></span></a>
            <? endif; ?>
                </div>
            </div>
 <? } ?>
        </div>
</div>
</div>
</div>
<? if (CSite::InGroup(array(6)) && isset($arResult['CONFIRMED']) && $arResult['CONFIRMED'] == 1): ?>
<script type="text/javascript">
    document.addEventListener("DOMContentLoaded", function(){
        $('div.informer').addClass('alert alert-danger').text('Вы не можете редактировать утвержденный документ.');
        var input = $('form.reporting input');
        for (var i = 0; input.length > i; i++){
            $(input[i]).attr('disabled', 'true');
        }

        $('form.reporting textarea').attr('disabled', 'true');
    });
</script>
<? endif; ?>