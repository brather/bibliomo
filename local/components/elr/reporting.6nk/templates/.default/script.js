/**
 * Created by AChechel on 12.01.2017.
 */
$(document).ready(function () {
    var reportId = $('input[name="R[ID]"]').val();

    $('button.export-document').on('click', function () {
        var iText = this.innerText,
            $this = $(this),
            type  = $this.data('type');

        $.ajax({
            url: '/local/components/elr/reporting.6nk/ajax.php',
            method: 'POST',
            data: {
                id: reportId,
                document: type
            },
            beforeSend: function () {
                $this.html('<img width="15" src="/bitrix/components/bitrix/sale.order.payment.change/templates/.default/images/loader.gif">');
            },
            success: function (res) {
                $this.text(iText);
                window.open(window.location.origin + res, '_blank');
            },
            error: function (st, msg) {
                console.error(st, msg);
            }
        });
    });

    $('span.confirmed').on('click', function(){
        var $this = $(this),
            id = $this.data('id');

        $.ajax({
            url:'/local/components/elr/reporting.6nk/ajax.php',
            method: 'post',
            data: {
                id: id,
                type: 'confirm'
            },
            beforeSend: function () {
                $this.parent().html('<img width="15" src="/bitrix/components/bitrix/sale.order.payment.change/templates/.default/images/loader.gif">');
            }
        }).done(function(){
            window.location.reload();
        });
    });

    $('span.re-confirm').on('click', function(){
        var $this = $(this),
            id = $this.data('id');

        $.ajax({
            url:'/local/components/elr/reporting.6nk/ajax.php',
            method: 'post',
            data: {
                id: id,
                type: 're-confirm'
            },
            beforeSend: function () {
                $this.parent().html('<img width="15" src="/bitrix/components/bitrix/sale.order.payment.change/templates/.default/images/loader.gif">');
            }
        }).done(function(){
            window.location.reload();
        });
    });

    $('#scan-file').on('change', function(){
        $(this).parents('form').trigger('submit');
    });
});