<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

global $APPLICATION;

CJSCore::RegisterExt("bootstrap_ext", Array(
    "js"    =>  "/bitrix/js/elr.useraccess/bootstrap.min.js",
    "css"   =>  "/bitrix/css/elr.useraccess/bootstrap.min.css",
    "rel"   =>  array('jquery2'),
    "skip_core" => "Y"
));
CJSCore::RegisterExt("chosen_css", Array(
    "css" => "/bitrix/css/elr.useraccess/chosen.css",
    "skip_core" => "Y"
));
CJSCore::Init(array("bootstrap_ext"));
CJSCore::Init(array("chosen_css"));


?>

<div class="wrapper">
    <div class="menu-top">
        <?$APPLICATION->IncludeComponent("bitrix:menu", "lk_left", Array(
            "ROOT_MENU_TYPE" => "left",	// Тип меню для первого уровня
            "MENU_CACHE_TYPE" => "N",	// Тип кеширования
            "MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
            "MENU_CACHE_USE_GROUPS" => "N",	// Учитывать права доступа
            "MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
                0 => "",
            ),
            "MAX_LEVEL" => "1",	// Уровень вложенности меню
            "CHILD_MENU_TYPE" => "",	// Тип меню для остальных уровней
            "USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
            "DELAY" => "N",	// Откладывать выполнение шаблона меню
            "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
        ),
            false
        );?>
    </div>

    <div class=" bootstrap">

        <h1 style="margin: 60px 0;">Отчеты по форме 6-НК</h1>

        <div id="tab-collection">
            <div id="tab-collection" class="b-createcoll">

                <a href="new/" class="b-btcreatecoll b-btcreate bbox"><span></span>+ Создать новый отчет</a>
            </div>
        </div>
    </div>


    <div id="search-services" class="bootstrap">
        <form role="form" name="searchServices" method="get" id="search-services-form" onsubmit="$(this).goToPageService(1, true); return false;">
            <div class="row">
                <? if (!$arResult['IS_LIBRARY'] && !empty($arResult['LIBS'])): ?>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label for="nameService">Библиотека</label>
                            <select name="LIBRARY" class="form-control col-4-md chosen_library">
                                <option value="">Все</option>
                                <? foreach ($arResult['LIBS'] as $arLib): ?>
                                    <option value="<?=$arLib['ID']?>" <? if ($_REQUEST['LIBRARY'] == $arLib['ID']):
                                        ?> selected="selected"<? endif; ?>><?=$arLib['NAME']?></option>
                                <? endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="clearfix visible-xs"></div>

                    <script src="/bitrix/js/elr.useraccess/chosen.jquery.js" type="text/javascript"></script>
                    <script type="text/javascript">
                        $('.chosen_library').chosen({
                            no_results_text: "Нет результатов поиска",
                            disable_search_threshold: 5
                        });
                    </script>
                <? endif; ?>

                <? if (!$arResult['IS_LIBRARY'] && !empty($arResult['USERS'])): ?>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label for="service-type">Пользователь</label>
                            <select name="USER" class="form-control col-4-md">
                                <option value="">Все</option>
                                <? foreach ($arResult['USERS'] as $arUser): ?>
                                    <option value="<?=$arUser['ID']?>" <? if ($_REQUEST['USER'] == $arUser['ID']):
                                        ?> selected="selected"<? endif; ?>><?=$arUser['FULL_NAME']?></option>
                                <? endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="clearfix visible-xs"></div>
                <? endif; ?>

                <? if (!empty($arResult['YEARS'])): ?>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label for="service-type">Отчетный год</label>
                            <select name="YEAR" class="form-control col-4-md">
                                <option value="">Все</option>
                                <? foreach ($arResult['YEARS'] as $iYear): ?>
                                    <option value="<?=$iYear?>" <? if ($_REQUEST['YEAR'] == $iYear):
                                        ?> selected="selected"<? endif; ?>><?=$iYear?></option>
                                <? endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="clearfix visible-sm visible-xs"></div>
                <? endif; ?>

                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 col-lg-offset-1 col-md-offset-1" style="min-height: 66px; line-height: 66px;">
                    <button type="submit" class="btn bt_typelt btn-lg no-radius">Найти</button>
                </div>
            </div>
        </form>
    </div>

    <div id="tableService">
        <? if (!empty($arResult['ITEMS'])):?>
            <div class="table-responsive" id="resultServicesList">
                <table class="table table-striped table-bordered" style="font-size: 0.8em">
                    <thead>
                    <tr>
                        <th class="middle">#</th>
                        <th class="middle">Библиотека</th>
                        <th class="middle">Пользователь</th>
                        <th class="middle">Дата создания</th>
                        <th class="middle">Отчетный год</th>
                        <th class="middle">Комментарий</th>
                        <th <?= (CSite::InGroup(array(1)))? "colspan='2'": "" ?>>&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                        <? foreach ($arResult['ITEMS'] as $id => $arItem): ?>
                            <tr <?=(CSite::InGroup(array(1)) && $arItem['CONFIRMED'] == 0)?"class='not-confirm'":"" ?>>
                                <td class="middle center label-edit"><?=$arItem['ID']?></td>
                                <td class="middle">
                                    <a href="<?=$arItem['ID']?>/"><?=$arResult['LIBS'][$arItem['LIBRARY_ID']]['NAME']?></a>
                                </td>
                                <td class="middle"><?=$arResult['USERS'][$arItem['USER_ID']]['FULL_NAME']?></td>
                                <td class="middle"><?=$arItem['DATE_CREATE']?></td>
                                <td class="middle"><?=$arItem['YEAR']?></td>
                                <td class="middle"><?=(CSite::InGroup(array(1)) && $arItem['CONFIRMED'] == 0)? "[<i>не утвержден</i>]<br/>": ""?><?=$arItem['COMMENT']?></td>
                                <? if (CSite::InGroup(array(1))): ?>
                                    <? if ($arItem['CONFIRMED'] == 0): ?>
                                        <td class="middle valign-center" title="Не подтверждено"><span class="not-confirm glyphicon glyphicon glyphicon-edit"></span></td>
                                    <? else: ?>

                                        <td class="middle valign-center" title="Вернуть на доработку"><span data-id="<?=$arItem['ID']?>" class="re-confirm glyphicon glyphicon-share"></span></td>
                                    <? endif; ?>
                                <? endif; ?>
                                <? if (CSite::InGroup(array(6))): ?>
                                    <? if ($arItem['CONFIRMED'] == 0): ?>
                                        <td class="middle valign-center" title="Подтвердить"><span data-id="<?=$arItem['ID']?>" class="confirmed glyphicon glyphicon-share"></span></td>
                                    <? else: ?>
                                        <td class="middle valign-center" title="Подтверждено"><span class="check-this glyphicon glyphicon-ok"></span></td>
                                    <? endif; ?>
                                <? endif; ?>
                            </tr>
                        <? endforeach; ?>
                    </tbody>
                </table>
            </div>
        <? endif; ?>
    </div>
</div>