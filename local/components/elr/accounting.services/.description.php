<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Localization\Loc as Loc;

Loc::loadMessages(__FILE__);

$arComponentDescription = array(
	"NAME" => "Учет услуг",
	"DESCRIPTION" => "Учет услуг, каждой из библиотек.",
	"ICON" => '/images/icon.gif',
	"SORT" => 100,
	"PATH" => array(
		"ID" => 'ELR_INFO_ACCOUNTING_SERVICES',
		"NAME" => '',
		"SORT" => 10,
		"CHILD" => array(
			"ID" => 'acccountingservices',
			"NAME" => "Учет услуг",
			"SORT" => 10
		)
	),
);