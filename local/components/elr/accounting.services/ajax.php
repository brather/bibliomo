<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/bitrix/modules/main/include/prolog_before.php';

/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 19.12.2016
 * Time: 9:33
 */
use Bitrix\Main\Loader,
    Bitrix\Main\Localization\Loc,
    Elr\AccountingServicesTable,
    Bitrix\Main\Application;

$rq = Application::getInstance()->getContext()->getRequest();

if ($rq->isAjaxRequest())
{
    if ($rq->getPost('action') == "checkEchb")
    {
        global $DB;

        $db = $DB->Query("SELECT `VALUE_ID` FROM `b_uts_user` WHERE `UF_NUM_ECHB` LIKE '" . trim($rq->getPost('data')) . "%' LIMIT 15");

        if ($db->AffectedRowsCount() == 1)
        {
            $u = CUser::GetByID((int)$db->Fetch()["VALUE_ID"])->Fetch();
            $user = array(
                "FULL_NAME" => trim($u['LAST_NAME'] . " " . $u['NAME'] . " " . $u['SECOND_NAME']),
                "ECHB_NUM" => $u['UF_NUM_ECHB']
            );
            die( json_encode(array("user" => $user, "status" => "done")) );
        }

        if ($db->AffectedRowsCount() == 0)
        {
            die(json_encode(array("status" => "empty")));
        }

        $result = array();

        while ($res = $db->Fetch())
        {
            $u = CUser::GetByID((int)$res['VALUE_ID'])->Fetch();
            $result[] = array(
                "FULL_NAME" => trim($u['LAST_NAME'] . " " . $u['NAME'] . " " . $u['SECOND_NAME']),
                "ECHB_NUM" => $u['UF_NUM_ECHB']
            );
        }

        die( json_encode(array("status" => "more", "users" => $result)) );
    }

    if ($rq->getPost('action') == "getAccountingByID")
    {
        Loader::includeModule("elr.monitoring");
        $db = AccountingServicesTable::getById((int)$rq->getPost('data'));

        if($res = $db->fetch()){
            $res['DATE_PROVISION'] = date("d.m.Y", $res['DATE_PROVISION']->getTimestamp());
            die(json_encode(array("status" => "ok", "acc" => $res)));
        }
        else
            die(json_encode(array("status" => "error", "msg" => $db->getTrackerQuery())));
    }
}

global  $APPLICATION;


$APPLICATION->IncludeComponent("elr:accounting.services", ".default");