<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 19.12.2016
 * Time: 10:30
 *
 * @var $arResult array
 */
use Bitrix\Main\Loader;

Loader::includeModule("currency");
?>
<div class="container bootstrap">
    <div class="row">
        <form class="form" method="get">
        <div class="col-lg-4 form-group">
            <label>Период оказания услуг</label>
            <div class="input-group">
                <input type="text" class="form-control" id="DATE_PROVISION_FROM" name="DATE_PROVISION_FROM" placeholder="с" value="<?=$_REQUEST['DATE_PROVISION_FROM']?>" />
                <span class="input-group-addon" onclick="BX.calendar({node: $('#DATE_PROVISION_FROM')[0], field: $('#DATE_PROVISION_FROM')[0], bTime: false });"><span class="glyphicon glyphicon-calendar"></span></span>
                <input type="text" class="form-control" id="DATE_PROVISION_TO" name="DATE_PROVISION_TO" placeholder="по"  value="<?=$_REQUEST['DATE_PROVISION_TO']?>"/>
                <span class="input-group-addon" onclick="BX.calendar({node: $('#DATE_PROVISION_TO')[0], field: $('#DATE_PROVISION_TO')[0], bTime: false });"><span class="glyphicon glyphicon-calendar"></span></span>
            </div>
        </div>
        <div class="col-lg-6 form-group">
            <label for="LIBRARIES">Наименование библиотеки</label>
            <select name="libraries" id="LIBRARIES" class="form-control chosen_library"  data-placeholder="Выбрать из списка">
                <option value=""></option>
            <? foreach ($arResult['LIBRARIES'] as $lib):?>
                <option <?=($_REQUEST['libraries'] == $lib['ID'])? 'selected="selected"': "";?> value="<?= $lib['ID']; ?>"><?= $lib['NAME']?></option>
            <? endforeach;?>
            </select>
            <script src="/bitrix/js/elr.useraccess/chosen.jquery.js" type="text/javascript"></script>
            <script type="text/javascript">
                $('.chosen_library').chosen({
                    no_results_text: "Нет результатов поиска",
                    disable_search_threshold: 5
                });
            </script>
        </div>
        <div class="col-lg-2 form-group" style="line-height: 65px; height: 50px;">
            <button type="submit" class="btn btn-default">Применить</button>
            <a class="pull-right" href="<?=preg_replace("/\?(.+)$/", "", $_SERVER['REQUEST_URI'])?>"><span class="glyphicon glyphicon-refresh"></span></a>
        </div>
        </form>
    </div>
    <div class="row">
        <? if (count($arResult['ACC_SERVICE']) > 0) :?>
        <div class="table-responsive">
            <table class="table table-striped table-bordered" style="font-size: 0.8em">
                <thead>
                    <tr>
                        <th>№</th>
                        <th>Дата услуги</th>
                        <th>Код</th>
                        <th>Вид услуги</th>
                        <th>Наименование улуги</th>
                        <th>Библиотека</th>
                        <th>ФИО польз-я</th>
                        <th>№ ЧБ</th>
                        <th>Стоимость</th>
                        <th>Примечание</th>
                        <th colspan="2">&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                <? foreach ($arResult['ACC_SERVICE'] as $service):?>
                    <tr>
                        <td><?= $service['ID']?></td>
                        <td><?= date("d.m.Y", strtotime($service['DATE_PROVISION']))?></td>
                        <td><?= $service['SERVICE_CODE']?></td>
                        <td><?= $service['SERVICE_TYPE']?></td>
                        <td><?= $service['SERVICE_NAME']?></td>
                        <td><?= $service['LIBRARY_NAME']?></td>
                        <td><?= $service['FULL_NAME']?></td>
                        <td><?= $service['ECHB_NUM']?></td>
                        <td><?= CurrencyFormat($service['PRICE'], 'RUB')?></td>
                        <td><?= $service['COMMENT']?></td>
                        <td class="middle center label-edit"><a href="javascript: void(0);" data-toggle="modal" data-target="#modalAU" data-id="<?=$service['ID']?>" onclick="$().updateAccounting(this); return false;"><span class="adm-list-table-popup glyphicon glyphicon-pencil"></span></a></td>
                        <td class="middle"><a data-id="<?=$service['ID']?>" href="javascript: void(0);" onclick="$().removeAccounting(this); return false;"><span class="glyphicon glyphicon-trash"></span></a></td>
                    </tr>
                <? endforeach;?>
                </tbody>
            </table>
        </div>
        <? else: ?>
        <h2>Список учета услуг пуст</h2>
        <? endif; ?>
    </div>
    <div class="row">
        <div class="col-lg-9 elr_nav_num_page">
        <? if ( $arResult["COUNT_PAGES"] > 1 ) {
            $baseLink = preg_replace("/(PAGE=[\d]+)/", "", $_SERVER['REQUEST_URI']);
            $baseLink = (preg_match("/\?/", $baseLink))? $baseLink . "&" : $baseLink . "?";

            if ( !isset( $arResult['PAGE'] ) || $arResult['PAGE'] < 2 ):?>
                <span class="glyphicon glyphicon-fast-backward">&nbsp;</span>
                <span class="glyphicon glyphicon-step-backward">&nbsp;</span>
            <? else: ?>
                <a class="glyphicon glyphicon-fast-backward" href="<?=$baseLink ?>" >&nbsp;</a>
                <a class="glyphicon glyphicon-step-backward" href="<?=$baseLink . "PAGE=" . ($arResult["PAGE"] - 1);?>" >&nbsp;</a>
            <? endif;
            /* Входные параметры */
            $count_pages = $arResult['COUNT_PAGES'];
            $active = $arResult['PAGE'];
            $count_show_pages = 5;



            $left = $active - 1;
            $right = $count_pages - $active;
            if ( $left < floor( $count_show_pages / 2 ) )
            {
            $start = 1;
            }
            else
            {
            $start = $active - floor( $count_show_pages / 2 );
            }

            $end = $start + $count_show_pages - 1;

            if ( $end > $count_pages )
            {
            $start -= ( $end - $count_pages );
            $end = $count_pages;

            if ( $start < 1 )
            {
            $start = 1;
            }
            }

            for ( $i = $start; $i <= $end; $i++ )
            {
            if ( $i == $active )
            {
            ?><span class="page-num"><?= $arResult['PAGE']; ?></span><?php
            }
            else
            {
                ?><a href="<?=$baseLink . "PAGE=" . $i?>"><?= $i ?></a><?php
            }
            } ?>
            <? if ( isset( $arResult['PAGE'] ) && $arResult['PAGE'] == ( $arResult["COUNT_PAGES"] ) ):?>
                <span class="glyphicon glyphicon-fast-forward">&nbsp;</span>
                <span class="glyphicon glyphicon-step-forward">&nbsp;</span>
            <? else: ?>
            <a class="glyphicon glyphicon-step-forward" href="<?=$baseLink . "PAGE=" . ($arResult["PAGE"] + 1);?>" >&nbsp;</a>
            <a class="glyphicon glyphicon-fast-forward" href="<?=$baseLink . "PAGE=" . $arResult["COUNT_PAGES"];?>" >&nbsp;</a>
            <? endif; ?>
        <? } ?>
        </div>
        <div class="col-lg-3">
            <a id="show_modal_window" type="button" class="btn btn-default btn-green btn-primary pull-right" data-toggle="modal" data-target="#modalAU">+ Добавить</a>
        </div>
    </div>
    <div class="modal fade" id="modalAU" tabindex="-1" role="dialog" aria-labelledby="modalAULabel" aria-hidden="true">
        <div class="modal-dialog">
            <form role="form" class="form" name="auAccSer" method="post" id="auAccSer">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="modalAULabel">Учет услуг</h4>
                </div>
                <div class="modal-body" id="containerModalMerged">

                    <input type="hidden" name="action" value="addAccounting">
                    <input type="hidden" name="ID" value="">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-2">
                                <label for="DATE_PROVISION">Дата услуги</label>
                                <div class="input-group form-group">
                                    <input type="text" id="DATE_PROVISION" class="form-control" name="dateProvision" required>
                                    <span class="input-group-addon" onclick="BX.calendar({node: $('#DATE_PROVISION')[0], field: $('#DATE_PROVISION')[0], bTime: false });"><span class="glyphicon glyphicon-calendar"></span></span>
                                </div>
                            </div>
                            <div class="col-lg-5 form-group">
                                <label for="CODE_ID">Услуга</label>
                                <div class="clearfix"></div>
                                <select data-placeholder="Выбрать" class="form-control modal-code-id" name="codeId" id="CODE_ID">
                                    <option value=""></option>
                                <? foreach ($arResult['SERVICES'] as $service):?>
                                    <option value="<?=$service['ID']?>"><?= $service['UF_NAME']?></option>
                                <? endforeach;?>
                                </select>
                                <script type="text/javascript">
                                    $('.modal-code-id').chosen({
                                        no_results_text: "Нет результатов поиска",
                                        disable_search_threshold: 5
                                    });
                                </script>
                                <span class="help-block sr-only">Не указана услуга</span>
                            </div>
                            <div class="col-lg-5 form-group">
                                <label for="LIBRARY_ID">Библиотека</label>
                                <div class="clearfix"></div>
                                <select data-placeholder="Выбрать" name="libraryId" id="LIBRARY_ID" class="form-control chosen_modal_library">
                                    <option value=""></option>
                                    <? foreach ($arResult['LIBRARIES'] as $lib):?>
                                        <option <?=(count($arResult['LIBRARIES']) == 1)? 'selected="selected"': ''?> value="<?= $lib['ID']; ?>"><?= $lib['NAME']?></option>
                                    <? endforeach;?>
                                </select>
                                <script type="text/javascript">
                                    $('.chosen_modal_library').chosen({
                                        no_results_text: "Нет результатов поиска",
                                        disable_search_threshold: 5
                                    });
                                </script>
                                <span class="help-block sr-only">Не указана Библиотека</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4" style="position: relative;">
                                <label for="ECHB_NUM">№ ЕЧБ</label>
                                <input type="text" id="ECHB_NUM" class="form-control" name="echbNum" autocomplete="off" />
                            </div>
                            <div class="col-lg-4">
                                <label for="FULL_NAME">Ф.И.О. пользователя</label>
                                <input type="text" id="FULL_NAME" class="form-control" name="fullName">
                            </div>
                            <div class="col-lg-4 form-group">
                                <label for="PRICE">Стоимость</label>
                                <input type="text" id="PRICE" class="form-control" name="price" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <label for="COMMENT">Примечание</label>
                                <textarea cols="10" rows="5" id="COMMENT" class="form-control" name="comment"></textarea>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer control-btn">
                    <div class="form-inline">
                        <div class="form-group">
                            <button type="submit" class="btn btn-lg btn-primary disabled" >Сохранить</button>
                        </div>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
