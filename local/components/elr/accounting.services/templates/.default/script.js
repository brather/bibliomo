/**
 * Created by AChechel on 19.12.2016.
 */
$(document).ready(function () {
    $('.menu-top a[href="/profile/accounting/"]').parent().addClass('active');

    $('#PRICE').mask('000 000 000 000 000.00 руб.', {reverse: true});

    $('#ECHB_NUM').on('blur', function(){
        $('.smart-search').remove();
    });

    $('#ECHB_NUM').on('keyup', function () {
        var t = this;

        if (t.value.length < 3) return true;

        $.ajax({
            url: '/local/components/elr/accounting.services/ajax.php',
            dataType: 'JSON',
            method: 'post',
            data: {
                action: 'checkEchb',
                data: t.value
            },
            success: function (json){
                var j = json;

                $('.smart-search').remove();

                /* Найден ровно один пользователь */
                if (j.status == "done"){
                    t.value = j.user.ECHB_NUM;
                    document.getElementById('FULL_NAME').value = j.user.FULL_NAME;
                }

                if (j.status == 'more'){
                    $(t).parent().append("<div class='smart-search'></div>");
                    var smart = $('.smart-search');
                    $.each(j.users, function (k, v) {
                        smart.append("<div class='user-item'><span class='user-item-echb'>"+v.ECHB_NUM+"</span> <span class='user-item-full-name'>"+v.FULL_NAME+"</span></div>")
                    })
                }

                /* Пустой результат ответа */
                if (j.status == "empty"){
                    $(t).parent().append('<span class="help-block" style="position: absolute; bottom: -27px; font-size: 0.7em; color: #830a00">Не найдено пользователей с таким номером ЧБ</span>');

                    setTimeout(function () {
                        $(t).siblings(".help-block").fadeOut(800).remove();
                    }, 3100)
                }
            },
            error: function (st, msg){
                console.error(st, msg);
            }
        });
    });

    $(document).on('mousedown', '.user-item', function(e){
        var t = e.currentTarget;

        document.getElementById('FULL_NAME').value = $(t).find('.user-item-full-name').text();
        document.getElementById('ECHB_NUM').value = $(t).find('.user-item-echb').text();

        $(t).parent().remove();
    });

    $(document).on("click", "#show_modal_window", function(){
        document.forms.auAccSer.action.value = "addAccounting";
        document.forms.auAccSer.ID.value = "";
        $('#CODE_ID').val('').trigger('chosen:updated');
        $('#LIBRARY_ID').val('').trigger('chosen:updated');
        document.forms.auAccSer.reset();
    });

});

$(function () {
    $.fn.updateAccounting = function (t) {
        document.forms.auAccSer.reset();

        document.forms.auAccSer.action.value = "updateAccounting";

        $.ajax({
            url: '/local/components/elr/accounting.services/ajax.php',
            dataType: 'json',
            method: 'post',
            data: {
                action: 'getAccountingByID',
                data: $(t).data("id")
            },
            success: function (json) {
                var j = json;

                if (j.status == 'error'){
                    alert("Возникла непредвиденная ошибка.");
                    return false;
                }

                document.getElementById('DATE_PROVISION').value = j.acc.DATE_PROVISION;
                $('#CODE_ID').val(j.acc.CODE_ID).trigger('chosen:updated');
                $('#LIBRARY_ID').val(j.acc.LIBRARY_ID).trigger('chosen:updated');
                document.getElementById('ECHB_NUM').value = j.acc.ECHB_NUM;
                document.getElementById('FULL_NAME').value = j.acc.FULL_NAME;
                document.getElementById('PRICE').value = $('#PRICE').masked(j.acc.PRICE);
                document.getElementById('COMMENT').value = j.acc.COMMENT;
                document.forms.auAccSer.ID.value = j.acc.ID;
            },
            error: function (st, msg) {
                console.error(st, msg);
            }
        });
    };
    $.fn.removeAccounting = function (t){
        if (!confirm("Вы уверены ?")){
            return false;
        }

        $.ajax({
            url: '/local/components/elr/accounting.services/ajax.php',
            dataType: "json",
            method: 'post',
            data: {
                action: "removeAccounting",
                data: $(t).data("id")
            },
            success: function (json) {
                if (json.status == "ok")
                    window.location.reload();
                else
                    console.error(json.err);
            },
            error: function(st, msg){
                console.error(st, msg);
            }
        });
    };

    $('#auAccSer').validator().on("submit", function (e) {
        e.preventDefault(e);
        e.stopPropagation();
        var t = e.currentTarget,
            action = t.action.value,
            data = {
                DATE_PROVISION: t.dateProvision.value,
                CODE_ID: t.codeId.value,
                LIBRARY_ID: t.libraryId.value,
                FULL_NAME: t.fullName.value,
                ECHB_NUM: t.echbNum.value,
                PRICE: parseFloat(t.price.value.replace(" ", "")),
                COMMENT: t.comment.value
            };

        $(t.codeId).siblings('span').addClass('sr-only').parent().removeClass('has-error');
        $(t.libraryId).siblings('span').addClass('sr-only').parent().removeClass('has-error');

        if (!data.CODE_ID || !data.LIBRARY_ID || !data.DATE_PROVISION ){
            if (!data.CODE_ID){
                $(t.codeId).siblings('span').removeClass('sr-only').parent().addClass('has-error');
            }
            if (!data.LIBRARY_ID){
                $(t.libraryId).siblings('span').removeClass('sr-only').parent().addClass('has-error');
            }

            return false;
        }

        if (action == "updateAccounting")
            data.ID = t.ID.value;

        $.ajax({
            url: '/local/components/elr/accounting.services/ajax.php',
            data: {
                action: action,
                data: data
            },
            dateType: 'html',
            method: 'post',
            beforeSend: function () {
                $('#modalAU .modal-footer .form-inline').prepend('<div class="form-group"><img height="42" id="modal-loader" src="/bitrix/components/bitrix/sale.order.payment.change/templates/.default/images/loader.gif" /></div>');
            },
            success: function () {
                window.location.reload();
            },
            error: function (st, msg) {
                console.error(st, msg);
            }
        });
    });
});

