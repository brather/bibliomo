<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 19.12.2016
 * Time: 9:33
 */
use Bitrix\Main\Loader,
    Bitrix\Main\Localization\Loc,
    Elr\AccountingServicesTable,
    Bitrix\Main\Application,
    Bitrix\Highloadblock\HighloadBlockTable,
    Bitrix\Main\Type\DateTime;

if (!Loader::includeModule("elr.monitoring")) return false;

class accounting_services extends CBitrixComponent
{
    private $limit = 10;
    private $page = 1;
    private $filter = array();
    private $isBiblioAdmin;
    const IBLOCK_LIBRARY_ID = 4;

    public function executeComponent()
    {
        $this->isBiblioAdmin = (CSite::InGroup(array(1)) === false); // return true if biblioAdmin

        $rq = $this->getRq();

        /* Action:  Add, Update, Remove */
        if ($rq->isAjaxRequest())
        {
            $this->ajaxReq($rq);
        }

        if ( $PAGE = $rq->getPost( "PAGE" ) )
        {
            $this->setPage( $PAGE );
        }

        $this->arResult['LIBRARIES'] = $this->getCollectionLibrary();
        $this->arResult['SERVICES'] = $this->getCollectionService($this->arResult['LIBRARIES']);
        $this->arResult['ACC_SERVICE'] = $this->getResult();
        $this->arResult['LIMIT'] = $this->getLimit();
        $this->arResult['PAGE'] = $this->getPage();

        $this->includeComponentTemplate();
    }

    /**
     * @param int $page
     */
    public function setPage( $page )
    {
        $this->page = $page;
    }

    private function getResult()
    {
        $limit  = $this->getLimit();
        $offset = ($this->getPage() - 1) * $this->getLimit();

        $filter = array();

        if ($this->isBiblioAdmin === true)
        {
            $filter["=LIBRARY_ID"] = (int)$this->arResult["LIBRARIES"][0]['ID'];
        }

        /* Search section */
        if (!empty($this->getRq()->get("libraries")))
        {
            $filter["=LIBRARY_ID"] = (int)$this->getRq()->get("libraries");
        }

        if (!empty($this->getRq()->get("DATE_PROVISION_FROM")))
        {
            $filter[">=DATE_PROVISION"] = DateTime::createFromTimestamp(strtotime($this->getRq()->get("DATE_PROVISION_FROM")));
        }

        if (!empty($this->getRq()->get("DATE_PROVISION_TO")))
        {
            $filter["<=DATE_PROVISION"] = DateTime::createFromTimestamp(strtotime($this->getRq()->get("DATE_PROVISION_TO") . " 23:59:59"));
        }
        /* End Search section */

        if (!empty($this->getRq()->get("PAGE")))
        {
            $this->setPage( (int)$this->getRq()->get("PAGE"));

            $offset = ($this->getPage() - 1) * $this->getLimit();
        }

        $this->arResult['COUNT_ROWS'] = AccountingServicesTable::getCount($filter);

        $this->arResult['COUNT_PAGES'] = ceil($this->arResult['COUNT_ROWS'] / $this->limit);

        $db = AccountingServicesTable::getList(array(
                "select" => array(
                    "ID", "DATE_PROVISION", "SERVICE_CODE", "SERVICE_NAME",
                    "SERVICE_TYPE", "LIBRARY_NAME", "FULL_NAME", "ECHB_NUM",
                    "PRICE", "COMMENT"
                ),
                "order" => array("DATE_PROVISION" => 'desc'),
                "limit" => $limit,
                "offset" => $offset,
                "filter" => $filter
            )
        );

        return $db->fetchAll();

    }

    private function getCollectionService($myLibrary)
    {
        Loader::includeModule("highloadblock");

        $db = HighloadBlockTable::getList(array(
            "select" => array(
                "ID"
            ),
            "filter" => array(
                "=NAME" => "LibraryServices"
            )
        ));

        $hb = $db->fetch();

        $hlblock = HighloadBlockTable::getById($hb["ID"])->fetch();
        $entity = HighloadBlockTable::compileEntity( $hlblock );
        $libraryServiceClass = $entity->getDataClass(); // string "\LibraryServicesTable"

        $db = $libraryServiceClass::getList(array(
            "order" => array(
                "UF_NAME" => "asc"
            ),
        ));

        $res = $db->fetchAll();

        if ($this->isBiblioAdmin === true)
            $this->_filteredServiceList($myLibrary[0]['ID'], $res);

        return $res;
    }

    public function getCollectionLibrary(  )
    {
        global $USER;

        if ( $this->isBiblioAdmin === true )
        {
            $thisUser = CUser::GetByID( $USER->GetID() )->Fetch();

            $filter = array("ID" => (int)$thisUser['UF_LIBRARY'], "IBLOCK_ID" => self::IBLOCK_LIBRARY_ID, "ACTIVE" => "Y");
        }
        else
        {
            $filter = array("IBLOCK_ID" => self::IBLOCK_LIBRARY_ID, "ACTIVE" => "Y");
        }

        $dbRes = \Bitrix\Iblock\ElementTable::getList(
            array(
                "filter" => $filter,
                "order" => array("NAME" => "ASC")
            )
        );

        $library = $dbRes->fetchAll();

        return $library;
    }

    private function _filteredServiceList( $ID_LIBRARY, &$arrService )
    {
        foreach ($arrService as $key => $service)
        {
            if (in_array($ID_LIBRARY, $service['UF_LIBRARY_LIST'])) continue;

            unset($arrService[$key]);
        }
    }

    /**
     * @param $rq \Bitrix\Main\HttpRequest
     */
    private function ajaxReq($rq)
    {
        if ( is_array($rq->getPost("data")) && count($rq->getPost("data")) > 0 )
        {
            if ($rq->getPost("action") === 'addAccounting')
                $this->addAccountingService( $rq->getPost("data"));

            if ($rq->getPost("action") === 'updateAccounting')
                $this->updateAccountingService( $rq->getPost("data") );
        }

        if ($rq->getPost("action") === 'removeAccounting' && !empty($rq->getPost("data")))
        {
            if ($rq->getPost("action") === 'removeAccounting')
                $this->removeAccountingService( $rq->getPost("data") );
        }
    }

    private function isAdminLib()
    {
        return CSite::InGroup(array(6));
    }

    private function getRq()
    {
        return Application::getInstance()->getContext()->getRequest();
    }

    private function getLimit()
    {
        return $this->limit;
    }

    private function getPage()
    {
        return $this->page;
    }

    private function addAccountingService( $getPost )
    {
        global $USER;

        if (!isset($getPost['PRICE']))
            $getPost['PRICE'] = 0.00;

        $getPost['USER_TOUCH_ID'] = $USER->GetID();
        $getPost['DATE_PROVISION'] = new Bitrix\Main\Type\DateTime( $getPost['DATE_PROVISION'] );
        $db = AccountingServicesTable::add($getPost);

        if (!$db->isSuccess())
            return $db->getErrorMessages();
        else
            return (int)$db->getId();
    }

    private function updateAccountingService( $getPost )
    {
        $ID = $getPost['ID'];
        unset($getPost['ID']);

        global $USER;

        $getPost['USER_TOUCH_ID'] = $USER->GetID();
        $getPost['DATE_PROVISION'] = new Bitrix\Main\Type\DateTime( $getPost['DATE_PROVISION'] );

        $db = AccountingServicesTable::update($ID, $getPost);

        return $db->isSuccess();
    }


    private function removeAccountingService( $id )
    {
        $db = AccountingServicesTable::delete($id);

        if (!$db->isSuccess())
            die(json_encode(array("err" => $db->getErrorMessages())));
        else
            die(json_encode(array("status" => "ok")));
    }
}