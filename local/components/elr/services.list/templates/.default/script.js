/**
 * Created by AChechel on 07.12.2016.
 */
$(function () {
        $.fn.goToPageService = function (num, search) {
            var data = {PAGE: num},
                valNAME = document.forms.searchServices.elements.UF_NAME.value,
                valTYPE = document.forms.searchServices.elements.UF_SERVICE_TYPE.value,
                valCODE = document.forms.searchServices.elements.UF_SERVICE_CODE.value,
                action = (search === true) ? 'searchServices' : 'getServicesList';

            if (valNAME || valTYPE || valCODE) {
                data = {
                    PAGE: num,
                    action: action,
                    search: {
                        UF_NAME: valNAME,
                        UF_SERVICE_TYPE: valTYPE,
                        UF_SERVICE_CODE: valCODE
                    }
                };
            }
            var $serviceTable = $("#resultServicesList");

            $.ajax({
                url: '/local/components/elr/services.list/templates/.default/ajax.php',
                data: data,
                dataType: "html",
                method: 'POST',
                beforeSend: function () {
                    $serviceTable.toggle().html('<div class="center"><img src="/local/templates/rnb/images/bx_loader.gif" /></div>').toggle();
                },
                success: function (html) {
                    $pag = $(html).find('div.elr_nav_num_page').html();
                    $table = $(html).find('#resultServicesList').html();
                    if(!$table)
                        $table = "<h2>Нет услуги, соответствующих вашему запросу</h2>";
                    if(!$pag)
                        $pag = "";

                    $('#tableService div.elr_nav_num_page').html($pag);
                    $serviceTable.toggle().html($table).toggle();
                    document.scrollToElement('tabs-7');
                },
                error: function (st, er) {
                    console.error(st, er);
                }
            });
        };
});
