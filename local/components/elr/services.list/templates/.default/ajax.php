<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/bitrix/modules/main/include/prolog_before.php';
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 07.12.2016
 * Time: 12:52
 */
global $APPLICATION, $USER;
if (!$USER->IsAdmin() )
{
    if ( !in_array(6, $USER->GetUserGroupArray()) ) die("Access denied");
}

$APPLICATION->IncludeComponent("elr:services.list", ".default");
