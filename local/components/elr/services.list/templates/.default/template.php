<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 09.12.2016
 * Time: 17:06
 */
global $USER, $APPLICATION;

if(!$USER->IsAdmin()) return;

CJSCore::RegisterExt("bootstrap_ext", Array(
    "js"    =>  "/bitrix/js/elr.useraccess/bootstrap.min.js",
    "css"   =>  "/bitrix/css/elr.useraccess/bootstrap.min.css",
    "rel"   =>  array('jquery2'),
    "skip_core" => "Y"
));
CJSCore::Init(array("bootstrap_ext"));
?>
<div id="search-services" class="bootstrap">
    <form role="form" name="searchServices" method="post" id="search-services-form" onsubmit="$(this).goToPageService(1, true); return false;">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                    <div class="form-group">
                        <label for="nameService">Наименование услуги</label>
                        <input id="nameService" type="text" class="form-control col-4-md" value="" placeholder="Название или часть названия" name="UF_NAME"/>
                    </div>
                </div>
                <div class="clearfix visible-xs"></div>
                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                    <div class="form-group">
                        <label for="service-type">Вид услуги</label>
                        <input id="service-type" type="text" class="form-control col-4-md" name="UF_SERVICE_TYPE" />
                    </div>
                </div>
                <div class="clearfix visible-xs"></div>
                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                    <div class="form-group">
                        <label for="service-type">Код услуги</label>
                        <input id="service-type" type="text" class="form-control col-4-md" name="UF_SERVICE_CODE" />
                    </div>
                </div>
                <div class="clearfix visible-sm visible-xs"></div>
                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 col-lg-offset-1 col-md-offset-1" style="min-height: 66px; line-height: 66px;">
                    <button type="submit" class="btn bt_typelt btn-lg no-radius">Найти</button>
                </div>
            </div>
        </div>
    </form>
</div>
<div id="tableService">
    <? if (count($arResult['SERVICES']) > 0):?>
        <div class="table-responsive" id="resultServicesList">
            <table class="table table-striped table-bordered" style="font-size: 0.8em">
                <thead>
                <tr>
                    <th class="middle">#</th>
                    <th class="middle">Код услуги</th>
                    <th class="middle">Наименование услуги</th>
                    <th class="middle">Вид услуги</th>
                    <th class="middle">Описание услуги</th>
                </tr>
                </thead>
                <tbody>
                <? foreach ($arResult['SERVICES'] as $id => $service): ?>
                    <tr>
                        <td class="middle center label-edit"><a href="/bitrix/admin/highloadblock_row_edit.php?ENTITY_ID=<?=$component->getHbID()?>&ID=<?=$id?>&lang=ru" target="_blank"><span class="adm-list-table-popup glyphicon glyphicon-pencil"></span></a></td>
                        <td class="middle"><?=$service["UF_SERVICE_CODE"]?></td>
                        <td class="middle"><?=$service["UF_NAME"];?></td>
                        <td class="middle"><?=$service["UF_SERVICE_TYPE"];?></td>
                        <td class="middle"><?=$service["UF_DESCRIPTION"];?></td>
                    </tr>
                <? endforeach; ?>
                </tbody>
            </table>
        </div>
        <div class="container" style="box-sizing: border-box;">
            <div class="row">
                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-11">
                    <div class="elr_nav_num_page">
                        <? if ( ($arResult["COUNT_ROWS"] / $arResult["LIMIT"]) > 1 ):
                            if ( !isset( $arResult['PAGE'] ) || $arResult['PAGE'] < 2 ):?>
                                <span class="glyphicon glyphicon-fast-backward">&nbsp;</span>
                                <span class="glyphicon glyphicon-step-backward">&nbsp;</span>
                            <? else: ?>
                                <a class="glyphicon glyphicon-fast-backward" href="#" onclick="$(this).goToPageService(1); return false;" >&nbsp;</a>
                                <a class="glyphicon glyphicon-step-backward" href="#" onclick="$(this).goToPageService(<?= ( $arResult["PAGE"] - 1 ) ?>); return false;">&nbsp;</a>
                            <? endif;

                            /* Входные параметры */
                            $count_pages = ceil($arResult['COUNT_ROWS'] / $arResult['LIMIT']);
                            $active = $arResult['PAGE'];
                            $count_show_pages = 5;

                            $left = $active - 1;
                            $right = $count_pages - $active;
                            if ( $left < floor( $count_show_pages / 2 ) )
                            {
                                $start = 1;
                            }
                            else
                            {
                                $start = $active - floor( $count_show_pages / 2 );
                            }

                            $end = $start + $count_show_pages - 1;

                            if ( $end > $count_pages )
                            {
                                $start -= ( $end - $count_pages );
                                $end = $count_pages;

                                if ( $start < 1 )
                                {
                                    $start = 1;
                                }
                            }

                            for ( $i = $start; $i <= $end; $i++ )
                            {
                                if ( $i == $active )
                                {
                                    ?><span class="page-num"><?= $arResult['PAGE']; ?></span><?php
                                }
                                else
                                {
                                    ?><a onclick="$(this).goToPageService(<?= $i; ?>); return false;" href="#"><?= $i ?></a><?php
                                }
                            } ?>
                            <? if ( isset( $arResult['PAGE'] )
                            && $arResult['PAGE'] == ( ceil($arResult['COUNT_ROWS'] / $arResult['LIMIT']) )
                        ):?>
                            <span class="glyphicon glyphicon-fast-forward">&nbsp;</span>
                            <span class="glyphicon glyphicon-step-forward">&nbsp;</span>
                        <? else: ?>
                            <a class="glyphicon glyphicon-step-forward" href="#" onclick="$(this).goToPageService(<?= ( $arResult["PAGE"] + 1 ) ?>); return false;">&nbsp;</a>
                            <a class="glyphicon glyphicon-fast-forward" href="#" onclick="$(this).goToPageService(<?= ( ceil($arResult['COUNT_ROWS'] / $arResult['LIMIT']) ) ?>); return false;">
                                &nbsp;</a>
                        <? endif; ?>
                            <div class="pull-right">
                                <span><?= ( (int)$arResult['PAGE'] * $arResult["LIMIT"] ) - $arResult["LIMIT"] + 1 ?> - <?= ( ( (int)$arResult['PAGE'] + 1 ) * $arResult["LIMIT"] ) - $arResult["LIMIT"] ?> из <?= $arResult['COUNT_ROWS'] ?></span>
                            </div>
                        <? endif; ?>
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-1 control-btn">
                    <a class="pull-right btn btn-lg btn-default btn-green" href="/bitrix/admin/highloadblock_row_edit.php?ENTITY_ID=<?=$component->getHbID();?>&lang=ru" target="_blank">+ Добавить</a>
                </div>
            </div>
        </div>
    <? endif; ?>
</div>