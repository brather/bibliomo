<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Localization\Loc as Loc;

Loc::loadMessages(__FILE__);

$arComponentDescription = array(
	"NAME" => "Список услуг",
	"DESCRIPTION" => "Сформированны табличный список услуг",
	"ICON" => '',
	"SORT" => 10,
	"PATH" => array(
		"ID" => 'ELR_SERVICES_LIBRARY',
		"NAME" => '',
		"SORT" => 10,
	),
);

?>