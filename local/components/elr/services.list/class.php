<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 25.11.2016
 * Time: 9:22
 */
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Highloadblock\HighloadBlockTable,
    \Bitrix\Main\Loader;

class services_list extends CBitrixComponent
{
    private $limit = 10;
    private $page = 1;
    private $filterExt = array();
    private $tableName;
    private $hbID;

    function executeComponent()
    {
        $rq = \Bitrix\Main\Application::getInstance()->getContext()->getRequest(
        );

        if ( $PAGE = $rq->getPost( "PAGE" ) )
        {
            $this->setPage( $PAGE );
        }

        if ( $LIMIT = $rq->getPost( "LIMIT" ) )
        {
            $this->setLimit( $LIMIT );
        }

        if ( $search = $rq->getPost( "search" ) )
        {
            if (is_array($search) && count($search) > 0)
            {
                if ($search["UF_NAME"])
                    $this->setFilterExt("UF_NAME", $search["UF_NAME"]);

                if ($search["UF_SERVICE_TYPE"])
                    $this->setFilterExt("UF_SERVICE_TYPE", $search["UF_SERVICE_TYPE"]);

                if ($search["UF_SERVICE_CODE"])
                    $this->setFilterExt("UF_SERVICE_CODE", $search["UF_SERVICE_CODE"]);
            }
        }


        $this->setTableName();

        $this->arResult['SERVICES'] = $this->_getResult();
        $this->arResult['LIMIT'] = $this->getLimit();
        $this->arResult['PAGE'] = $this->getPage();

        $this->includeComponentTemplate();
    }

    private function _getResult()
    {
        $arResult = array();

        global $APPLICATION, $DB, $USER;

        if (!$USER->IsAdmin()) return $arResult;

        /* Start cached */
        $cacheId = $this->getPage() . $this->getLimit() . $this->getTemplateName() . $this->_filterToStr( $this->filterExt );

        $cache = Bitrix\Main\Data\Cache::createInstance();

        if ($cache->initCache($this->arParams['CACHE_TIME'], $cacheId, "MySQL/LibraryServices/"))
        {
            $arResult = $cache->getVars();
        }
        elseif ($cache->startDataCache())
        {
            $limit = $this->getLimit();
            $offset = ($this->getPage() - 1) * $this->getLimit();

            if (count($this->filterExt) > 0)
            {
                $filter = array();

                foreach ($this->filterExt as $rowName => $value)
                    $filter["%".$rowName] = $value;

                $this->filterExt = $filter;
                unset($filter);
            }

            $hlblock = HighloadBlockTable::getById($this->hbID)->fetch();
            $entity = HighloadBlockTable::compileEntity( $hlblock );
            $libraryServiceClass = $entity->getDataClass(); // string "\LibraryServicesTable"

            $this->arResult['COUNT_ROWS'] = $libraryServiceClass::getCount($this->filterExt);

            $db = $libraryServiceClass::getList(array(
                "limit" => (int)$limit,
                "offset" => (int)$offset,
                "order" => array(
                    "UF_NAME" => "asc"
                ),
                "filter" => $this->filterExt
            ));

            if ($this->arResult["COUNT_ROWS"] == 0)
            {
                $cache->abortDataCache();
            }
            else
            {
                while ($res = $db->fetch())
                {
                    $arResult[$res['ID']] = $res;
                }
            }
            $cache->endDataCache($arResult);
        }
        /* End cached */

        return $arResult;
    }
    /**
     * @return array
     */
    public function isFilterExt()
    {
        return $this->filterExt;
    }

    /**
     * @param bool $filterExt
     */
    public function setFilterExt($rowName, $value )
    {
        $this->filterExt[$rowName] = $value;
    }

    /**
     * @return int
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @param int $page
     */
    public function setPage( $page )
    {
        $this->page = $page;
    }

    /**
     * @return int
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     */
    public function setLimit( $limit )
    {
        $this->limit = $limit;
    }

    /**
     * @return mixed
     */
    public function getTableName()
    {
        return $this->tableName;
    }

    private function _filterToStr( $filter )
    {
        $str = "";

        foreach ($filter as $i => $v)
            $str .= $i . "|" . $v;

        return $str;
    }

    /**
     * @param mixed $tableName
     */
    private function setTableName( )
    {
        Loader::includeModule("highloadblock");

        global $APPLICATION;

        $db = HighloadBlockTable::getList(array(
            "select" => array(
                "TABLE_NAME", "ID"
            ),
            "filter" => array(
                "=NAME" => "LibraryServices"
            )
        ));

        if ($table = $db->fetch())
        {
            $this->tableName = $table['TABLE_NAME'];
            $this->hbID = $table['ID'];
        }
        else
        {
            $APPLICATION->ThrowException("Таблицы для <i>LibraryServices</i> не существует");
        }
    }

    /**
     * @return mixed
     */
    public function getHbID()
    {
        return $this->hbID;
    }
}