/**
 * Created by AChechel on 12.12.2016.
 */
(function($){$.fn.dcAccordion=function(options){var defaults={classParent:'dcjq-parent',classActive:'active',classArrow:'dcjq-icon',classCount:'dcjq-count',classExpand:'dcjq-current-parent',eventType:'click',hoverDelay:300,menuClose:true,autoClose:true,autoExpand:false,speed:'slow',saveState:true,disableLink:true,showCount:false,cookie:'dcjq-accordion'};var options=$.extend(defaults,options);this.each(function(options){var obj=this;setUpAccordion();if(defaults.saveState==true){checkCookie(defaults.cookie,obj)}if(defaults.autoExpand==true){$('li.'+defaults.classExpand+' > a').addClass(defaults.classActive)}resetAccordion();if(defaults.eventType=='hover'){var config={sensitivity:2,interval:defaults.hoverDelay,over:linkOver,timeout:defaults.hoverDelay,out:linkOut};$('li a',obj).hoverIntent(config);var configMenu={sensitivity:2,interval:1000,over:menuOver,timeout:1000,out:menuOut};$(obj).hoverIntent(configMenu);if(defaults.disableLink==true){$('li a',obj).click(function(e){if($(this).siblings('ul').length>0){e.preventDefault()}})}}else{$('li a',obj).click(function(e){$activeLi=$(this).parent('li');$parentsLi=$activeLi.parents('li');$parentsUl=$activeLi.parents('ul');if(defaults.disableLink==true){if($(this).siblings('ul').length>0){e.preventDefault()}}if(defaults.autoClose==true){autoCloseAccordion($parentsLi,$parentsUl)}if($('> ul',$activeLi).is(':visible')){$('ul',$activeLi).slideUp(defaults.speed);$('a',$activeLi).removeClass(defaults.classActive)}else{$(this).siblings('ul').slideToggle(defaults.speed);$('> a',$activeLi).addClass(defaults.classActive)}if(defaults.saveState==true){createCookie(defaults.cookie,obj)}})}function setUpAccordion(){$arrow='<span class="'+defaults.classArrow+'"></span>';var classParentLi=defaults.classParent+'-li';$('> ul',obj).show();$('li',obj).each(function(){if($('> ul',this).length>0){$(this).addClass(classParentLi);$('> a',this).addClass(defaults.classParent).append($arrow)}});$('> ul',obj).hide();if(defaults.showCount==true){$('li.'+classParentLi,obj).each(function(){if(defaults.disableLink==true){var getCount=parseInt($('ul a:not(.'+defaults.classParent+')',this).length)}else{var getCount=parseInt($('ul a',this).length)}$('> a',this).append(' <span class="'+defaults.classCount+'">('+getCount+')</span>')})}}function linkOver(){$activeLi=$(this).parent('li');$parentsLi=$activeLi.parents('li');$parentsUl=$activeLi.parents('ul');if(defaults.autoClose==true){autoCloseAccordion($parentsLi,$parentsUl)}if($('> ul',$activeLi).is(':visible')){$('ul',$activeLi).slideUp(defaults.speed);$('a',$activeLi).removeClass(defaults.classActive)}else{$(this).siblings('ul').slideToggle(defaults.speed);$('> a',$activeLi).addClass(defaults.classActive)}if(defaults.saveState==true){createCookie(defaults.cookie,obj)}}function linkOut(){}function menuOver(){}function menuOut(){if(defaults.menuClose==true){$('ul',obj).slideUp(defaults.speed);$('a',obj).removeClass(defaults.classActive);createCookie(defaults.cookie,obj)}}function autoCloseAccordion($parentsLi,$parentsUl){$('ul',obj).not($parentsUl).slideUp(defaults.speed);$('a',obj).removeClass(defaults.classActive);$('> a',$parentsLi).addClass(defaults.classActive)}function resetAccordion(){$('ul',obj).hide();$allActiveLi=$('a.'+defaults.classActive,obj);$allActiveLi.siblings('ul').show()}});function checkCookie(cookieId,obj){var cookieVal=$.cookie(cookieId);if(cookieVal!=null){var activeArray=cookieVal.split(',');$.each(activeArray,function(index,value){var $cookieLi=$('li:eq('+value+')',obj);$('> a',$cookieLi).addClass(defaults.classActive);var $parentsLi=$cookieLi.parents('li');$('> a',$parentsLi).addClass(defaults.classActive)})}}function createCookie(cookieId,obj){var activeIndex=[];$('li a.'+defaults.classActive,obj).each(function(i){var $arrayItem=$(this).parent('li');var itemIndex=$('li',obj).index($arrayItem);activeIndex.push(itemIndex)});$.cookie(cookieId,activeIndex,{path:'/'})}}})(jQuery);

$(document).ready(function () {
    $('ul.uleditstat').dcAccordion({
        classParent: 'active-parent',
        classArrow: 'icon',
        eventType: 'click',
        autoClose: true,
        saveState: false,
        disableLink: true,
        showCount: false,
        speed: 'fast',
        cookie: 'uleditstat'
    });

    var Types = {
        infografregister: [document.jqplotOption.registerReader],
        infografvisits: [document.jqplotOption.visitsReader],
        infografactive: [document.jqplotOption.activeReader],
        infograffund: [document.jqplotOption.fundIn, document.jqplotOption.fundOut],
        infografaccounting: [document.jqplotOption.accounting],
        infografregisterMax: document.jqplotOption.maxNumRegisterReaders * 1,
        infografvisitsMax: document.jqplotOption.maxNumVisitsReaders * 1,
        infografactiveMax: document.jqplotOption.maxNumActiveReaders * 1,
        infograffundMax: document.jqplotOption.maxFund * 1,
        infografaccountingMax: document.jqplotOption.maxAccountig,
        interval: document.jqplotOption.intervalDays,
        intervalAcc: document.jqplotOption.intervalDaysAccounting,
        dateStart: document.jqplotOption.dateStart ,
        dateEnd: document.jqplotOption.dateEnd
    };

    function showJQplotStart(elem) {
        var type = 'infograf-register',
            interVal = Types.interval;

        if (Types.infografregister[0]){
            Types.dateStart = Types.infografregister[0][0][0];
            Types.dateEnd   = Types.infografregister[0][Types.infografregister[0].length - 1][0];
        }
        else
            return false;

        if (elem) type = elem;

        if (type == 'infograf-accounting'){
            interVal = Types.intervalAcc;
            Types.dateStart = Types.infografaccounting[0][0][0];
            Types.dateEnd   = Types.infografaccounting[0][Types.infografaccounting[0].length - 1][0];
        }

        var sDesc = $('select#typeReport option[value="' + type +'"]').text(),
            title = sDesc;

        $('.stat-description').text(sDesc);
        $('#' + type).html('');
        $.jqplot._noToImageButton = true;
        $.jqplot("" + type, Types[type.replace('-', '')], {
            seriesColors: ["rgba(78, 135, 194, 0.7)", "rgb(159, 40, 56)"],
            title: title,
            highlighter: {
                show: true,
                sizeAdjust: 1,
                tooltipOffset: 9
            },
            grid: {
                background: 'rgba(0,47,57,0.1)',
                drawBorder: false,
                shadow: false,
                gridLineColor: '#e0dfe3',
                gridLineWidth: 1
            },
            legend: {
                show: (type == "infograf-fund"),
                placement: 'inside'
            },
            seriesDefaults: {
                rendererOptions: {
                    smooth: false,
                    animation: {
                        show: true
                    }
                },
                showMarker: false
            },
            series: [
                {
                    label: 'Возвращено',
                    rendererOptions: {
                        smooth: true
                    }
                },
                {
                    label: 'Выдано',
                    rendererOptions: {
                        smooth: true
                    }
                }
            ],
            axesDefaults: {
                rendererOptions: {
                    baselineWidth: 1.5,
                    baselineColor: '#757575',
                    drawBaseline: false
                }
            },
            axes: {
                xaxis: {
                    renderer: $.jqplot.DateAxisRenderer,
                    tickRenderer: $.jqplot.CanvasAxisTickRenderer,
                    tickOptions: {
                        formatString: "%d %b %Y",
                        angle: 30,
                        textColor: '#000000'
                    },

                    min: Types.dateStart,
                    max: Types.dateEnd,
                    tickInterval: "" + interVal + " days",
                    drawMajorGridlines: true,
                    showTicks: true
                },
                yaxis: {
                    max: Types["" + type.replace('-', '') + "Max"],
                    rendererOptions: {
                        minorTicks: 1
                    },
                    min: 0,
                    tickOptions: {
                        formatString: "%'d",
                        showMark: true
                    }
                }
            }
        });
        $('.jqplot-highlighter-tooltip').addClass('ui-corner-all');
    }

    showJQplotStart();
    var previousSelectValue = $('select#typeReport').val();
    $(document).on('focus', 'select#typeReport', function () {
        previousSelectValue = this.value;
    });

    $(document).on('change', 'select#typeReport', function (env) {
        var value = env.currentTarget.value;

        $('div#' + previousSelectValue).attr('id', value);

        previousSelectValue = value;

        showJQplotStart(value);
    });

    $(document).on('click', 'input#reload_graf', function(){
        var data = {
            PERIOD_START: "",
            PERIOD_END: "",
            LIBRARY: ""
        };

        data.PERIOD_START = $('input#PERIOD_START').val();
        data.PERIOD_END = $('input#PERIOD_END').val();
        data.LIBRARY = $('#LIBRARY').val();

        $.ajax({
            url: '/local/components/elr/info.monitoring/templates/.default/ajax.php',
            method: 'POST',
            dataType: 'JSON',
            data: data,
            success: function (json) {
                if (json['cnt'] == 0){
                    $('div.jqplot-target').html('<h2>Нет данных для отображения</h2>');

                    return false;
                }

                Types.intervalAcc = json.accounting.interval;
                Types.interval = json['interval'];
                Types.infografregister = [json['register']];
                Types.infografaccounting = [json.accounting.jQplot];
                Types.infografvisits = [json['visits']];
                Types.infografactive = [json['active']];
                Types.infograffund = [json['fund_in'], json['fund_out']];
                Types.infografaccountingMax = json.accounting.max_price;
                Types.infografregisterMax = json.max_register;
                Types.infografvisitsMax = json.max_visits;
                Types.infografactiveMax = json.max_active;
                Types.infograffundMax = json.max_fund;

                showJQplotStart(previousSelectValue);
            }
        });
    });

    $('.menu-top a[href="/profile/statistic/"]').parent().addClass('active');

    /* Событие на сохраниение данных в Excel */
    $(document).on('click', 'a.save-excel-file', function (env) {
        var $this = $(env.currentTarget),
            excel = {},
            title = "Общий",
            type = $this.siblings('div').attr('id'),
            libValue = $('#LIBRARY').val(),
            dStart = $('#PERIOD_START').val(),
            dEnd = $('#PERIOD_END').val(),
            actionFile = 'excel';

        if (document.getElementById('typeReport').value == 'infograf-accounting')
            actionFile = 'accounting_sum';

        if (libValue != "")
            title = libValue;

        $.ajax({
            url: '/local/components/elr/info.monitoring/templates/.default/' + actionFile + '.php',
            method: 'post',
            data: {
                dateStart: dStart,
                dateEnd: dEnd,
                library: libValue,
                title: title
            },
            success: function (res) {
                window.open( window.location.origin + res);
                console.log(res);
            },
            error: function (st, err) {
                console.error(st, err);
            }
        });

    })
});