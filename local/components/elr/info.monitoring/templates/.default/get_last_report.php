<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/bitrix/modules/main/include/prolog_before.php';
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 13.12.2016
 * Time: 14:10
 */
use Bitrix\Main\Loader,
    Bitrix\Main\Application;

global $USER, $APPLICATION;

Loader::includeModule("elr.monitoring");

$rq = Application::getInstance()->getContext()->getRequest();

$s2 = $USER->GetUserGroupArray();

if (!$USER->IsAdmin())
{
    if ( !in_array(6, $s2) ) die();
}

if ($rq->isAjaxRequest() === false ) die();

$lastDay = $rq->getPost('lastDay');
$step = $rq->getPost('step');

Elr\Monitoring\Agent::lastPeriodPerDay((int)$lastDay, (int)$step);

exit(0);