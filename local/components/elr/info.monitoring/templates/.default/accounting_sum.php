<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/bitrix/modules/main/include/prolog_before.php';
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 15.12.2016
 * Time: 9:13
 */
use Bitrix\Main\Application,
    Bitrix\Main\Loader,
    Elr\Monitoring\MonitoringTable,
    Bitrix\Main\Entity,
    Bitrix\Main\Type;
use Elr\AccountingServicesTable;
use Elr\CollectionLibraries;

global $USER;

$rq = Application::getInstance()->getContext()->getRequest();

if (!$rq->isAjaxRequest()) die();

if (
    !Loader::includeModule("elr.monitoring") ||
    !Loader::includeModule('elr.useraccess') ||
    !Loader::includeModule("iblock")
) die();


$dateStart = date("d.m.Y", strtotime($rq->getPost("dateStart")));
$dateEnd = date("d.m.Y", strtotime($rq->getPost("dateEnd")));

$objPHPExcel = new PHPExcel();

$objPHPExcel->getProperties()->setCreator("BiblioMO")
    ->setLastModifiedBy("BiblioMO")
    ->setTitle("Accounting " . date("d-m-Y"))
    ->setSubject("Accounting " . date("d-m-Y"))
    ->setDescription("Report library accounting")
    ->setKeywords("office bibliomo")
    ->setCategory("Grafik result excel");


$sheet = $objPHPExcel->setActiveSheetIndex(0);
/*Объединяем ячейки*/
$sheet->mergeCells("A2:E2");

/*Авто ширина столбцов */
$sheet->getColumnDimension('A')->setAutoSize(true);
$sheet->getColumnDimension('B')->setAutoSize(true);
$sheet->getColumnDimension('C')->setAutoSize(true);

$sheet->setCellValue('A2', 'Данные по обору средств библиотек Московской области в период с ' . $dateStart ." - " . $dateEnd );

$sheet->setCellValue('A4', 'ISIL');
$sheet->setCellValue('B4', 'Наименование библиотеки');
$sheet->setCellValue('C4', 'Сумма оборота');

$filter = array();

if (CSite::InGroup(array(6)) && !$USER->IsAdmin())
{
    $user = CUser::GetByID( (int)$USER->GetID() )->Fetch();

    $UF_LIBRARY = $user['UF_LIBRARY'];
    $filter['=LIBRARY_ID'] = (int)$UF_LIBRARY;

    unset($user);
}

if ($dateStart)
    $filter['>=DATE_PROVISION'] = Type\Date::createFromTimestamp(strtotime($rq->getPost("dateStart")));

if ($dateEnd)
    $filter['<=DATE_PROVISION'] = Type\Date::createFromTimestamp(strtotime($rq->getPost("dateEnd")));

$coll = CollectionLibraries::getInstance(true);
$collLIb = $coll->getCollectionLibraries();

if ($USER->IsAdmin() && !empty($rq->getPost('library')))
{
    foreach ($collLIb as $xmlId => $lib)
    {
        if ($rq->getPost('library') === $xmlId)
        {
            $filter['=LIBRARY_ID'] = (int)$lib['ID'];

            continue;
        }
    }
}

$i = 4;
$sumPrice = 0.00;

$db = AccountingServicesTable::getList(array(
    'select'    => array(new Entity\ExpressionField('SUM_PRICE', 'SUM(`PRICE`)'), "LIBRARY_ID"),
    'group'     => array("LIBRARY_ID"),
    'filter'    => $filter,
));

while ( $res = $db->fetch() )
{
    $i++;
    foreach ($collLIb as $lib)
    {
        if ( (int)$lib['ID'] === (int)$res['LIBRARY_ID'] )
        {
            $sheet->setCellValue('A' . $i, $lib['XML_ID']);
            $sheet->setCellValue('B' . $i, $lib['NAME']);

            break;
        }
    }

    $sheet->setCellValue('C' . $i, (number_format($res['SUM_PRICE'], 2, ".", " ") . ' руб.'));

    $sumPrice += (float)$res['SUM_PRICE'];
}

$i++;
$sheet->mergeCells("A". $i .":B" . $i);

$sheet->setCellValue("A". $i, 'Итого');
$sheet->setCellValue("C". $i, (number_format($sumPrice, 2, ".", " ") . ' руб.'));

$objPHPExcel->getActiveSheet()->setTitle($rq->getPost('title'));

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

$dirSave = $_SERVER['DOCUMENT_ROOT'] . '/upload/report';
if (!is_dir($_SERVER['DOCUMENT_ROOT'] . '/upload/report'))
    mkdir($dirSave, 0755, true);
$fileName = 'accounting_' . date("Y-m-d_H-i-s") . ".xlsx";
$fileSave = $dirSave . '/' . $fileName;
$objWriter->save($fileSave);

echo ("/upload/report/" . $fileName);