<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 12.12.2016
 * Time: 16:01
 */
global $USER;

CJSCore::RegisterExt("bootstrap_ext", Array(
    "js"    =>  "/bitrix/js/elr.useraccess/bootstrap.min.js",
    "css"   =>  "/bitrix/css/elr.useraccess/bootstrap.min.css",
    "rel"   =>  array('jquery2'),
    "skip_core" => "Y"
));
CJSCore::RegisterExt("jqplot", Array(
    "css"    =>  "/local/components/elr/info.monitoring/templates/.default/css/jquery.jqplot.css",
    "js"   =>  "/local/components/elr/info.monitoring/templates/.default/js/jquery.jqplot.js",
    "rel"   =>  array('jquery'),
));
CJSCore::RegisterExt("jqplotdaterxisrenderer", Array(
        "js" => "/local/components/elr/info.monitoring/templates/.default/js/plugins/jqplot.dateAxisRenderer.js",
));
CJSCore::RegisterExt("jqplotlogaxisrenderer", Array(
        "js" => "/local/components/elr/info.monitoring/templates/.default/js/plugins/jqplot.logAxisRenderer.js",
));
CJSCore::RegisterExt("jqplotcanvastextrenderer", Array(
        "js" => "/local/components/elr/info.monitoring/templates/.default/js/plugins/jqplot.canvasTextRenderer.js",
));
CJSCore::RegisterExt("jqplotcanvasaxistickrenderer", Array(
        "js" => "/local/components/elr/info.monitoring/templates/.default/js/plugins/jqplot.canvasAxisTickRenderer.js",
));
CJSCore::RegisterExt("jqplothighlighter", Array(
        "js" => "/local/components/elr/info.monitoring/templates/.default/js/plugins/jqplot.highlighter.js",
));
CJSCore::RegisterExt("chosen_css", Array(
    "css" => "/bitrix/css/elr.useraccess/chosen.css",
    "skip_core" => "Y"
));
CJSCore::Init(array("bootstrap_ext", "jqplot", "jqplotdaterxisrenderer",
                    "jqplotlogaxisrenderer", "jqplotcanvastextrenderer",
                    "jqplotcanvasaxistickrenderer", "jqplothighlighter",
                    "chosen_css", "date")); ?>

<script type="text/javascript">
    document.jqplotOption = {};

    document.jqplotOption.activeReader = <?= CUtil::PhpToJSObject($arResult['GRAFIK']['ACTIVE']); ?>;
    document.jqplotOption.visitsReader = <?= CUtil::PhpToJSObject($arResult['GRAFIK']['VISITS']); ?>;
    document.jqplotOption.registerReader = <?= CUtil::PhpToJSObject($arResult['GRAFIK']['REGISTER']); ?>;
    document.jqplotOption.fundIn = <?= CUtil::PhpToJSObject($arResult['GRAFIK']['FUND_IN']); ?>;
    document.jqplotOption.fundOut = <?= CUtil::PhpToJSObject($arResult['GRAFIK']['FUND_OUT']); ?>;
    document.jqplotOption.accounting = <?= CUtil::PhpToJSObject($arResult['ACCOUNTING']['jQplot']); ?>;
    document.jqplotOption.maxNumActiveReaders = <?= $arResult['GRAFIK']['MAX_ACTIVE_READERS']; ?>;
    document.jqplotOption.maxNumVisitsReaders = <?= $arResult['GRAFIK']['MAX_VISITS']; ?>;
    document.jqplotOption.maxNumRegisterReaders = <?= $arResult['GRAFIK']['MAX_REGISTER_READERS']; ?>;
    document.jqplotOption.maxFund = <?= $arResult['GRAFIK']['MAX_FUND']; ?>;
    document.jqplotOption.maxAccountig = <?= $arResult['ACCOUNTING']['MAX_PRICE']; ?>;
    document.jqplotOption.intervalDays = <?= $arResult['GRAFIK']['INTERVAL']; ?>;
    document.jqplotOption.intervalDaysAccounting = <?= $arResult['ACCOUNTING']['INTERVAL']; ?>;


</script>
<div class="container bootstrap">
    <ul class="b-profile_menu">
        <li>
            <a href="/profile/edit/" class="ico_edit">Настройки профиля</a>
        </li>
        <li>
            <a href="/faq/" class="ico_help">Помощь</a>
        </li>
    </ul>
    <h1 style="margin: 60px 0;">Административный портал</h1>
    <div class="row" style="margin-top: 15px;">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

            <div class="row">
                <div class="col-lg-5 form-group">
                    <h4>Выберите период</h4>
                    <div style="display: inline-block; width: 150px">
                        <div class="input-group">
                            <input type="text" class="form-control" id="PERIOD_START" name="PERIOD_START" placeholder="с" value="<?=date('d.m.Y', (time() - (86400 * 30)));?>" />
                            <span class="input-group-addon" onclick="BX.calendar({node: $('#PERIOD_START')[0], field: $('#PERIOD_START')[0], bTime: false });"><span class="glyphicon glyphicon-calendar"></span></span>
                        </div>
                    </div>
                    <div style="display: inline-block; width: 150px">
                        <div class="input-group">
                            <input type="text" class="form-control" id="PERIOD_END" name="PERIOD_END" placeholder="по" value="<?=date('d.m.Y');?>" />
                            <span class="input-group-addon" onclick="BX.calendar({node: $('#PERIOD_END')[0], field: $('#PERIOD_END')[0], bTime: false });"><span class="glyphicon glyphicon-calendar"></span></span>
                        </div>
                    </div>

                </div>


            <div class="col-lg-5 form-group">
                <h4>Отображенная статистика</h4>
                <select class="form-control" id="typeReport" style="width: 500px;">
                    <option selected="selected" value="infograf-register">Количество зарегистрированных читателей в библиотеках</option>
                    <option value="infograf-visits">Количество посещений библиотек</option>
                    <option value="infograf-active">Количество активных читателей в библиотеках</option>
                    <option value="infograf-fund">Обращаемость фонда</option>
                    <option value="infograf-accounting">Денежный оборот</option>
                </select>
            </div>
            <div style="display: inline-block; width: 150px; float: right; height: 60px; line-height: 80px;" class="col-lg-2">
                <input id="reload_graf" type="button" class="btn btn-primary" value="Применить">
            </div>
                <div class="row">
                    <div class="col-lg-5 col-offset-lg-5 form-group">
                        <h4>Библиотека</h4>
                        <select name="LIBRARY" id="LIBRARY" style="width: 500px;" data-placeholder="Выбрать из списка">
                            <option value=""></option>
                            <? foreach ($arResult['LIBRARIES'] as $lib) :?>
                                <option value="<?=$lib['XML_ID']?>"><?=$lib['NAME']?></option>
                            <? endforeach; ?>
                        </select>
                        <script src="/bitrix/js/elr.useraccess/chosen.jquery.js" type="text/javascript"></script>
                        <script type="text/javascript">
                            $('#LIBRARY').chosen({
                                no_results_text: "Нет результатов поиска",
                                disable_search_threshold: 5
                            });
                        </script>
                    </div>
                </div>
            </div>
        </div>
            <div class="row">
                <div class="col-lg-12">
                    <div style="background: #fff; padding: 25px;" >
                        <span class="stat-description">Количество зарегистрированных читателей</span>
                        <div id="infograf-register" data-chart-type="line" style="width: 1150px; height: 400px; margin: 0; padding: 15px; background: #fff"></div>
                        <a style="position:relative; z-index: 100;"
                           target="_blank"
                           class="save-excel-file"
                           href="javascript:void(0);">
                            <img src="/local/components/elr/info.monitoring/templates/.default/img/excel.png"/>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <? if (CSite::InGroup(array(6))):?>
        <div class="row">
            <div class="pull-right" style="margin-top: 15px">
                <a href="/profile/statistics/" target="_blank">Старая форма статистики</a>
            </div>
        </div>
        <? endif; ?>
    </div>