<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/bitrix/modules/main/include/prolog_before.php';
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 13.12.2016
 * Time: 14:10
 */
use Bitrix\Main\Loader,
    Bitrix\Main\Application,
    Elr\Monitoring\MonitoringTable,
    Bitrix\Main\Type\DateTime,
    Bitrix\Main\Type\Date,
    Bitrix\Main\Entity,
    Elr\CollectionLibraries;
use Elr\AccountingServicesTable;

global $USER, $APPLICATION;

$rq = Application::getInstance()->getContext()->getRequest();

if (!CSite::InGroup(array(1, 6))) die();

if ($rq->isAjaxRequest() === false || !Loader::includeModule("elr.useraccess") || !Loader::includeModule("elr.monitoring") ) die();

$arr = array();

$periodStart    = $rq->getPost("PERIOD_START");
$periodEnd      = $rq->getPost("PERIOD_END");
$library        = $rq->getPost("LIBRARY");

$filter     = array();
$filterAcc  = array();

$coll = CollectionLibraries::getInstance(true);
$collLIb = $coll->getCollectionLibraries();

if (CSite::InGroup(array(6)) && !$USER->IsAdmin())
{
    $user = CUser::GetByID( (int)$USER->GetID() )->Fetch();

    $UF_LIBRARY = $user['UF_LIBRARY'];

    $filterAcc["=LIBRARY_ID"] = (int)$UF_LIBRARY;

    unset($user);

    /* Для библиотекаря оставляем в коллекции только его библиотеку */
    foreach ($collLIb as $xmlId => $lib)
    {
        if (!empty($library) && $xmlId == $library)
            $filterAcc['=LIBRARY_ID'] = (int)$lib['ID'];

        if ($lib['ID'] === $UF_LIBRARY)
        {
            $filter['=LIBRARY'] = $xmlId;
            continue;
        }
    }
}

if (!empty($library))
{
    foreach ($collLIb as $xmlId => $lib)
    {
        if (!empty($library) && $xmlId == $library)
            $filterAcc['=LIBRARY_ID'] = (int)$lib['ID'];

        continue;
    }
}

unset($coll, $collLIb);


if (!empty($periodStart))
{
    $filter['>=PERIOD_START'] = DateTime::createFromTimestamp(strtotime($periodStart));
    $filterAcc['>=DATE_PROVISION'] = Date::createFromTimestamp(strtotime($periodStart));
}

if (!empty($periodEnd))
{
    $filter['<=PERIOD_END'] = DateTime::createFromTimestamp(strtotime($periodEnd));
    $filterAcc['<=DATE_PROVISION'] = Date::createFromTimestamp(strtotime($periodEnd));
}

if (!empty($library))
    $filter['=LIBRARY'] = $library;

$db = AccountingServicesTable::getList(array(
    'select' => array(
        new Entity\ExpressionField('CNT', 'COUNT(DISTINCT `DATE_PROVISION`)')
    ),
    'filter' => $filterAcc
));

$arr['accounting'] = $db->fetch();
$arr['accounting']['interval'] = ceil($arr['accounting']['CNT'] / 30);

$db = AccountingServicesTable::getList(array(
    'select'    => array("SUM_PRICE", "DATE_PROVISION"),
    'order'     => array("DATE_PROVISION" => "ASC"),
    'group'     => array("DATE_PROVISION"),
    'filter'    => $filterAcc,
    'runtime'   => array(
        new Entity\ExpressionField('SUM_PRICE', 'SUM(`PRICE`)'),
    )
));

$arr['accounting']['max_price'] = 0;

while ($res = $db->fetch())
{
    if ((float)$res['SUM_PRICE'] > $arr['accounting']['max_price'])
        $arr['accounting']['max_price'] = $res['SUM_PRICE'];

    $arr['accounting']['jQplot'][] = array(date("Y-m-d", strtotime($res['DATE_PROVISION'])), $res['SUM_PRICE']);
}

$arr['accounting']['max_price'] += 100;

$db = MonitoringTable::getList(array(
    'select' => array(
        new Entity\ExpressionField('CNT', 'COUNT(DISTINCT `PERIOD_START`)')
    ),
    'filter' => $filter
));

/* Количество согласно фильтру */
if ($cnt = $db->fetch()) $arr['cnt'] = (int)$cnt['CNT'];

/* Интервал */
if ($arr['cnt']) $arr['interval'] = ceil($arr['cnt'] / 30);

$db = MonitoringTable::getList(
    array(
        'select' => array(
            'NUM_REG', 'NUM_ACT', 'NUM_VIS', 'NUM_FUND_OUT', 'NUM_FUND_IN', 'PERIOD_START'
        ),
        'order' => array("PERIOD_START" => "ASC"),
        'group' => array('PERIOD_START'),
        'filter' => $filter,
        'runtime' => array(
            new Entity\ExpressionField('NUM_REG', 'SUM(`NUM_REGISTERED_READERS`)'),
            new Entity\ExpressionField('NUM_ACT', 'SUM(`NUM_ACTIVE_READERS`)'),
            new Entity\ExpressionField('NUM_VIS', 'SUM(`NUM_VISITS`)'),
            new Entity\ExpressionField('NUM_FUND_OUT', 'SUM(`NUM_CHECKOUTS`)'),
            new Entity\ExpressionField('NUM_FUND_IN', 'SUM(`NUM_CHECKINS`)'),
        )
    )
);

$arr['max_active']      = 0;
$arr['max_visits']      = 0;
$arr['max_register']    = 0;
$arr['max_fund']        = 0;

while ($res = $db->fetch())
{
    if ((int)$res['NUM_ACT'] > $arr['max_active'])
        $arr['max_active'] = (int)$res['NUM_ACT'];

    if ((int)$res['NUM_VIS'] > $arr['max_visits'])
        $arr['max_visits'] = (int)$res['NUM_VIS'];

    if ((int)$res['NUM_REG'] > $arr['max_register'])
        $arr['max_register'] = (int)$res['NUM_REG'];

    if ((int)$res['NUM_FUND_OUT'] > (int)$res['NUM_FUND_IN'] && (int)$res['NUM_FUND_OUT'] > $arr['max_fund'])
        $arr['max_fund'] = (int)$res['NUM_FUND_OUT'];

    if ((int)$res['NUM_FUND_OUT'] < (int)$res['NUM_FUND_IN'] && (int)$res['NUM_FUND_IN'] > $arr['max_fund'])
        $arr['max_fund'] = (int)$res['NUM_FUND_IN'];

    $arr['active'][]    = array(date("Y-m-d", strtotime($res['PERIOD_START'])), (int)$res['NUM_ACT']);
    $arr['visits'][]    = array(date("Y-m-d", strtotime($res['PERIOD_START'])), (int)$res['NUM_VIS']);
    $arr['register'][]  = array(date("Y-m-d", strtotime($res['PERIOD_START'])), (int)$res['NUM_REG']);
    $arr['fund_in'][]  = array(date("Y-m-d", strtotime($res['PERIOD_START'])), (int)$res['NUM_FUND_IN']);
    $arr['fund_out'][]  = array(date("Y-m-d", strtotime($res['PERIOD_START'])), (int)$res['NUM_FUND_OUT']);
}

$arr['date_start']      = $arr['active'][0][0];
$arr['date_end']        = $arr['active'][count($arr['active']) - 1][0];

/* Максимальное значение графика + 10 */
$arr['max_active']      = $arr['max_active'] + 10;
$arr['max_visits']      = $arr['max_visits']  + 10;
$arr['max_register']    = $arr['max_register'] + 10;
$arr['max_fund']        = $arr['max_fund'] + 10;

die(json_encode($arr));
