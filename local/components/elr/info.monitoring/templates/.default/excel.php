<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/bitrix/modules/main/include/prolog_before.php';
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 15.12.2016
 * Time: 9:13
 */
use Bitrix\Main\Application,
    Bitrix\Main\Loader,
    Elr\Monitoring\MonitoringTable,
    Bitrix\Main\Entity,
    Bitrix\Main\Type;
use Elr\AccountingServicesTable;
use Elr\CollectionLibraries;

global $USER;

$rq = Application::getInstance()->getContext()->getRequest();

if (!$rq->isAjaxRequest()) die();

if (
    !Loader::includeModule("elr.monitoring") ||
    !Loader::includeModule('elr.useraccess') ||
    !Loader::includeModule("iblock")
) die();


$dateStart = date("d.m.Y", strtotime($rq->getPost("dateStart")));
$dateEnd = date("d.m.Y", strtotime($rq->getPost("dateEnd")));

$objPHPExcel = new PHPExcel();

$objPHPExcel->getProperties()->setCreator("BiblioMO")
    ->setLastModifiedBy("BiblioMO")
    ->setTitle("Report " . date("d-m-Y"))
    ->setSubject("Report " . date("d-m-Y"))
    ->setDescription("Report library")
    ->setKeywords("office bibliomo")
    ->setCategory("Grafik result file");


$sheet = $objPHPExcel->setActiveSheetIndex(0);
/*Объединяем ячейки*/
$sheet->mergeCells("A2:E2");

/*Авто ширина столбцов */
$sheet->getColumnDimension('A')->setAutoSize(true);
$sheet->getColumnDimension('B')->setAutoSize(true);
$sheet->getColumnDimension('C')->setAutoSize(true);
$sheet->getColumnDimension('D')->setAutoSize(true);
$sheet->getColumnDimension('E')->setAutoSize(true);
$sheet->getColumnDimension('F')->setAutoSize(true);
$sheet->getColumnDimension('G')->setAutoSize(true);

$sheet->setCellValue('A2', 'Данные о посещаемости библиотек Московской области в период с ' . $dateStart ." - " . $dateEnd );

$sheet->setCellValue('A4', 'ISIL');
$sheet->setCellValue('B4', 'Наименование библиотеки');
$sheet->setCellValue('C4', 'Количество зарегистрированных читателей');
$sheet->setCellValue('D4', 'Количество активных читателей');
$sheet->setCellValue('E4', 'Количество посещений');
$sheet->setCellValue('F4', 'Количество возвратов книг');
$sheet->setCellValue('G4', 'Количество выдач книг');

$filter = array();

$coll = CollectionLibraries::getInstance(true);
$collLIb = $coll->getCollectionLibraries();

if (CSite::InGroup(array(6)) && !$USER->IsAdmin())
{
    $user = CUser::GetByID( (int)$USER->GetID() )->Fetch();

    $UF_LIBRARY = $user['UF_LIBRARY'];
    $filterAcc['=LIBRARY_ID'] = $UF_LIBRARY;

    unset($user);

    /* Для библиотекаря оставляем в коллекции только его библиотеку */
    foreach ($collLIb as $xmlId => $lib)
    {
        if ($lib['ID'] === $UF_LIBRARY)
        {
            $filter['=LIBRARY'] = $xmlId;
            continue;
        }
    }

    unset($coll, $collLIb);
}

if ($dateStart)
    $filter['>=PERIOD_START'] = Type\DateTime::createFromTimestamp(strtotime($rq->getPost("dateStart")));

if ($dateEnd)
    $filter['<=PERIOD_END'] = Type\DateTime::createFromTimestamp(strtotime($rq->getPost("dateEnd")));

if ($rq->getPost('library'))
    $filter['=LIBRARY'] = trim($rq->getPost('library'));

$db = MonitoringTable::getList(array(
    'select' => array(
        'NUM_REG', 'NUM_ACT', 'NUM_VIS', 'NUM_FUND_OUT', 'NUM_FUND_IN', 'LIBRARY', "NAME" => 'LIBRARY_NAME.NAME'
    ),
    'filter' => $filter,
    'group' => array('LIBRARY'),
    'runtime' => array(
        new Entity\ExpressionField('NUM_REG', 'SUM(`NUM_REGISTERED_READERS`)'),
        new Entity\ExpressionField('NUM_ACT', 'SUM(`NUM_ACTIVE_READERS`)'),
        new Entity\ExpressionField('NUM_VIS', 'SUM(`NUM_VISITS`)'),
        new Entity\ExpressionField('NUM_FUND_OUT', 'SUM(`NUM_CHECKOUTS`)'),
        new Entity\ExpressionField('NUM_FUND_IN', 'SUM(`NUM_CHECKINS`)'),
    )
));

$i = 4;
$sumReg     = 0;
$sumAct     = 0;
$sumVis     = 0;
$sumFIn     = 0;
$sumFOut    = 0;

while ($res = $db->fetch())
{
    $i++;
    $sheet->setCellValue('A' . $i, $res['LIBRARY']);
    $sheet->setCellValue('B' . $i, $res['NAME']);
    $sheet->setCellValue('C' . $i, $res['NUM_REG']);
    $sheet->setCellValue('D' . $i, $res['NUM_ACT']);
    $sheet->setCellValue('E' . $i, $res['NUM_VIS']);
    $sheet->setCellValue('F' . $i, $res['NUM_FUND_IN']);
    $sheet->setCellValue('G' . $i, $res['NUM_FUND_OUT']);

    $sumReg  += $res['NUM_REG'];
    $sumAct  += $res['NUM_ACT'];
    $sumVis  += $res['NUM_VIS'];
    $sumFIn  += $res['NUM_FUND_IN'];
    $sumFOut += $res['NUM_FUND_OUT'];
}


$i++;
$sheet->mergeCells("A". $i .":B" . $i);

$sheet->setCellValue("A". $i, 'Итого');
$sheet->setCellValue("C". $i, $sumReg);
$sheet->setCellValue("D". $i, $sumAct);
$sheet->setCellValue("E". $i, $sumVis);
$sheet->setCellValue("F". $i, $sumFIn);
$sheet->setCellValue("G". $i, $sumFOut);


$objPHPExcel->getActiveSheet()->setTitle($rq->getPost('title'));

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

$dirSave = $_SERVER['DOCUMENT_ROOT'] . '/upload/report';
if (!is_dir($_SERVER['DOCUMENT_ROOT'] . '/upload/report'))
    mkdir($dirSave, 0755, true);
$fileName = 'report_' . date("Y-m-d_H-i-s") . ".xlsx";
$fileSave = $dirSave . '/' . $fileName;
$objWriter->save($fileSave);

echo ("/upload/report/" . $fileName);