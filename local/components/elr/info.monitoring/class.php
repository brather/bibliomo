<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 25.11.2016
 * Time: 9:22
 */
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader,
    Bitrix\Main\Application,
    Elr\Monitoring\MonitoringTable,
    Bitrix\Main\Entity,
    Bitrix\Main\Type\DateTime,
    Bitrix\Main\Type\Date,
    Elr\CollectionLibraries;
use Elr\AccountingServicesTable;

if (!Loader::includeModule('elr.useraccess') || !Loader::includeModule('elr.monitoring')) return false;

class info_monitoring extends CBitrixComponent
{
    private $limit = 10;
    private $page = 1;
    private $filterEx = false;
    private $isLibraryAdmin = false;
    private $libraryCollection = null;


    public function executeComponent()
    {
        global $USER, $APPLICATION;

        if (!$USER->IsAdmin() || ( !in_array(6, $USER->GetUserGroupArray())  ) )
            $APPLICATION->ThrowException("Доступ только Администратору портала");

        $this->arResult['LIBRARIES'] = $this->_libraryCollection();

        if (CSite::InGroup(array(6)) && !$USER->IsAdmin())
            $this->isLibraryAdmin = true;

        $this->gerResult();

        $this->includeComponentTemplate();
    }

    public function gerResult()
    {
        global $USER;


        $filterAcc = array(">=DATE_PROVISION" => Date::createFromTimestamp(time() - (86400 * 30)));
        $filter = array(">=PERIOD_START" => DateTime::createFromTimestamp(time() - (86400 * 30)));

        if ($this->isLibraryAdmin === true)
        {
            $user = CUser::GetByID( (int)$USER->GetID() )->Fetch();

            $UF_LIBRARY = $user['UF_LIBRARY'];

            $filterAcc["=LIBRARY_ID"] = (int)$UF_LIBRARY;

            unset($user);

            /* Для библиотекаря оставляем в коллекции только его библиотеку */
            foreach ($this->arResult['LIBRARIES'] as $xmlId => $lib)
            {
                if ($lib['ID'] === $UF_LIBRARY){
                    $filter['=LIBRARY'] = $xmlId;
                    continue;
                }

                unset($this->arResult['LIBRARIES'][$xmlId]);
            }

        }

        $db = AccountingServicesTable::getList(array(
            'select' => array(
                new Entity\ExpressionField('CNT', 'COUNT(DISTINCT `DATE_PROVISION`)')
            ),
            'filter' => $filterAcc
        ));

        $this->arResult['ACCOUNTING'] = $db->fetch();

        $this->arResult['ACCOUNTING']['INTERVAL'] = ceil($this->arResult['ACCOUNTING']['CNT'] / 30);

        $db = AccountingServicesTable::getList(array(
            'select'    => array("SUM_PRICE", "DATE_PROVISION"),
            'order'     => array("DATE_PROVISION" => "ASC"),
            'group'     => array("DATE_PROVISION"),
            'filter'    => $filterAcc,
            'runtime'   => array(
                new Entity\ExpressionField('SUM_PRICE', 'SUM(`PRICE`)'),
            )
        ));

        $this->arResult['ACCOUNTING']['MAX_PRICE'] = 0;

        while ($arr = $db->fetch())
        {
            if ((float)$arr['SUM_PRICE'] > $this->arResult['ACCOUNTING']['MAX_PRICE'])
                $this->arResult['ACCOUNTING']['MAX_PRICE'] = $arr['SUM_PRICE'];

            $this->arResult['ACCOUNTING']['jQplot'][] = array(date("Y-m-d", strtotime($arr['DATE_PROVISION'])), (float)$arr['SUM_PRICE']);
        }

        $this->arResult['ACCOUNTING']['MAX_PRICE'] += 100;


        $db = MonitoringTable::getList(array(
            'select' => array(
                new Entity\ExpressionField('CNT', 'COUNT(DISTINCT `PERIOD_START`)')
            ),
            'filter' => $filter
        ));

        $arr = $db->fetch();
        $this->arResult['GRAFIK']['CNT'] = $arr['CNT'];

        $db = MonitoringTable::getList(
            array(
                'select' => array(
                    'NUM_REG', 'NUM_ACT', 'NUM_VIS', 'NUM_FUND_OUT', 'NUM_FUND_IN', 'PERIOD_START'
                ),
                'order' => array("PERIOD_END" => "ASC"),
                'group' => array('PERIOD_START'),
                'filter' => $filter,
                'runtime' => array(
                    new Entity\ExpressionField('NUM_REG', 'SUM(`NUM_REGISTERED_READERS`)'),
                    new Entity\ExpressionField('NUM_ACT', 'SUM(`NUM_ACTIVE_READERS`)'),
                    new Entity\ExpressionField('NUM_VIS', 'SUM(`NUM_VISITS`)'),
                    new Entity\ExpressionField('NUM_FUND_OUT', 'SUM(`NUM_CHECKOUTS`)'),
                    new Entity\ExpressionField('NUM_FUND_IN', 'SUM(`NUM_CHECKINS`)'),
                )
            )
        );

        /* Базовые значения для высоты графика */
        $this->arResult['GRAFIK']['MAX_ACTIVE_READERS'] = 0;
        $this->arResult['GRAFIK']['MAX_VISITS'] = 0;
        $this->arResult['GRAFIK']['MAX_REGISTER_READERS'] = 0;
        $this->arResult['GRAFIK']['MAX_FUND'] = 0;

        while ($arr = $db->fetch())
        {
            if ((int)$arr['NUM_ACT'] > $this->arResult['GRAFIK']['MAX_ACTIVE_READERS'])
                $this->arResult['GRAFIK']['MAX_ACTIVE_READERS'] = (int)$arr['NUM_ACT'];

            if ((int)$arr['NUM_VIS'] > $this->arResult['GRAFIK']['MAX_VISITS'])
                $this->arResult['GRAFIK']['MAX_VISITS'] = (int)$arr['NUM_VIS'];

            if ((int)$arr['NUM_REG'] > $this->arResult['GRAFIK']['MAX_REGISTER_READERS'])
                $this->arResult['GRAFIK']['MAX_REGISTER_READERS'] = (int)$arr['NUM_REG'];

            if ((int)$arr['NUM_FUND_OUT'] > (int)$arr['NUM_FUND_IN'] && (int)$arr['NUM_FUND_OUT'] > $this->arResult['GRAFIK']['MAX_FUND'])
                $this->arResult['GRAFIK']['MAX_FUND'] = (int)$arr['NUM_FUND_OUT'];
            elseif ((int)$arr['NUM_FUND_OUT'] < (int)$arr['NUM_FUND_IN'] && (int)$arr['NUM_FUND_IN'] > $this->arResult['GRAFIK']['MAX_FUND'])
                $this->arResult['GRAFIK']['MAX_FUND'] = (int)$arr['NUM_FUND_IN'];

            $this->arResult['GRAFIK']['ACTIVE'][]       = array(date("Y-m-d", strtotime($arr['PERIOD_START'])), (int)$arr['NUM_ACT']);
            $this->arResult['GRAFIK']['VISITS'][]       = array(date("Y-m-d", strtotime($arr['PERIOD_START'])), (int)$arr['NUM_VIS']);
            $this->arResult['GRAFIK']['REGISTER'][]     = array(date("Y-m-d", strtotime($arr['PERIOD_START'])), (int)$arr['NUM_REG']);
            $this->arResult['GRAFIK']['FUND_IN'][]     = array(date("Y-m-d", strtotime($arr['PERIOD_START'])), (int)$arr['NUM_FUND_IN']);
            $this->arResult['GRAFIK']['FUND_OUT'][]     = array(date("Y-m-d", strtotime($arr['PERIOD_START'])), (int)$arr['NUM_FUND_OUT']);
        }

        /* Максимальное значение графика + 10 */
        $this->arResult['GRAFIK']['MAX_ACTIVE_READERS']     = $this->arResult['GRAFIK']['MAX_ACTIVE_READERS'] + 10;
        $this->arResult['GRAFIK']['MAX_VISITS']             = $this->arResult['GRAFIK']['MAX_VISITS'] + 10;
        $this->arResult['GRAFIK']['MAX_REGISTER_READERS']   = $this->arResult['GRAFIK']['MAX_REGISTER_READERS'] + 10;
        $this->arResult['GRAFIK']['MAX_FUND']               = $this->arResult['GRAFIK']['MAX_FUND'] + 10;

        /* Целое число интервала в днях */
        $this->arResult['GRAFIK']['INTERVAL'] = ceil($this->arResult['GRAFIK']['CNT'] / 30);
    }

    private function _libraryCollection()
    {
        $coll = CollectionLibraries::getInstance(true);

        return $coll->getCollectionLibraries();
    }
}