<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Localization\Loc as Loc;

Loc::loadMessages(__FILE__);

$arComponentDescription = array(
	"NAME" => "Инфографики мониторинг",
	"DESCRIPTION" => "Статистика и мониторинг данных. Отображение графиков данных.",
	"ICON" => '/images/icon.gif',
	"SORT" => 100,
	"PATH" => array(
		"ID" => 'ELR_INFO_MONITORING',
		"NAME" => '',
		"SORT" => 10,
		"CHILD" => array(
			"ID" => 'monitoring',
			"NAME" => "Мониторинг",
			"SORT" => 10
		)
	),
);