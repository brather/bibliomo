<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 25.11.2016
 * Time: 9:22
 */
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader,
    Bitrix\Main\Application;
if (!Loader::includeModule('elr.useraccess')) return false;

class user_list extends CBitrixComponent
{
    private $arMapSearchKey = array();
    private $arUF_LIBRARY = null;
    private $thisUser = null;

    public function executeComponent()
    {
        /* запись в объект массива с инвормацией о UF_LIBRARY поле */
        $this->entityUfLibrary();

        $this->arResult['USERS'] = $this->getResult();
        $this->includeComponentTemplate();
    }

    private function getResult()
    {
        $request = Application::getInstance()->getContext()->getRequest();

        global $USER, $DB;
        $arFilter = array();

        $arParams = array(
            "SELECT" => array(
                "UF_ABIS_ID", "UF_NUM_ECHB", "UF_MERGED_ID", "UF_LIBRARY",
                "UF_DATE_OF_FINISH", "UF_ISIL", "UF_CHANGED"
            ),
            "FIELDS" => array(
                "ID", "LOGIN", "EMAIL", "DATE_REGISTER", "EXTERNAL_AUTH_ID",
                "UF_ABIS_ID", "UF_NUM_ECHB", "UF_MERGED_ID", "UF_LIBRARY",
                "UF_DATE_OF_FINISH", "NAME", "SECOND_NAME", "LAST_NAME",
                "ACTIVE", "UF_ISIL", "UF_CHANGED"
            )
        );

        /* Получение информации по пользователю "Библиотекарь" */
        if ((new CSite)->InGroup(array(6)))
        {
            /* Массив данных для текущего пользователя, ID и UF_LIBRARY */
            $arrUser = $USER->GetByID( $USER->GetID() )->Fetch();

            /* Массив данных по доп. пользовательскому полю UF_LIBRARY, нужен его IBLOCK_ID */
            $arET_UF_LIBRARY = (new CUserTypeEntity)->GetList(array(), array("CODE" => "UF_LIBRARY"))->Fetch();

            /* получить библиотеку из справочка и выбрать ID и XML_ID (USER.UF_ISIL),  */
            $dbRes = CIBlockElement::GetList(
                array("SORT" => "ASC"),
                array(
                    "IBLOCK_ID" => $this->arUF_LIBRARY["SETTINGS"]["IBLOCK_ID"],
                    "ID" => (int)$arrUser['UF_LIBRARY']
                ),
                false,
                false,
                array("ID", "XML_ID")
            );

            /* Массив ID пользователей библиотеки */
            $arUserID = array();

            while ($ibe = $dbRes->Fetch())
            {
                if ($ibe["ID"] == $arrUser['UF_LIBRARY'])
                {
                    global $DB;
                    /* Т.к. нельзя отфильтровать выборку у CUser по UF_* полю, выбрать ID пользователей на прямую из таблицы */
                    $conn = $DB->Query("SELECT `VALUE_ID` FROM b_uts_user WHERE `UF_ISIL` = '{$ibe['XML_ID']}' AND `VALUE_ID` != '{$arrUser["ID"]}'");

                    /* Заполнить массив полученнымти VALUE_ID, эквивалентны USER.ID */
                    while ($res = $conn->Fetch())
                        $arUserID[] = $res["VALUE_ID"];

                }
            }
            /* Удалить из памяти не нужные данные */
            unset($arrUser,$arET_UF_LIBRARY, $conn);

            /* Получить библиотеку Библиотекаря */
            $this->arResult['LIBRARIES'] = $this->getListLibrary(false);

            /* Перевод массива с USER.ID в строку для фиотрации 1|2|...|n */
            if ( count($arUserID) > 0 )
                $arFilter = array("ID" => implode("|", $arUserID));
        }
        else
        {
            /* Получить все библиотеки для Администратора */
            $this->arResult['LIBRARIES'] = $this->getListLibrary();
        }


        /* инициализация запроса на поиск */
        if ( $request->get("action") === "SEARCH" )
        {
            $this->setArMapSearchKey();

            $arrSearch = array();

            foreach ( $this->arMapSearchKey as $field )
            {
                if ( $request->get($field) !== "" )
                {
                    switch ($field)
                    {
                        case "UF_ABIS_ID":
                        case "UF_NUM_ECHB":
                        case "UF_LIBRARY":
                            $arrSearch["%".$field] = '' . $request->get($field). '';
                            break;
                        case "DATE_INPUT_FROM":
                            $arrSearch["DATE_REGISTER_1"] = $request->get($field);
                            break;
                        case "DATE_INPUT_TO":
                            $arrSearch["DATE_REGISTER_2"] = $request->get($field);
                            break;
                        case "UF_DATE_OF_FINISH_FROM":
                            $arrSearch[">UF_DATE_OF_FINISH"] = $request->get($field);
                            break;
                        case "UF_DATE_OF_FINISH_TO":
                            $arrSearch["<UF_DATE_OF_FINISH"] = $request->get($field);
                            break;
                        case "UF_REGISTER_TYPE":
                            $RT = (int)$request->get($field);
                            if ($RT > 0)
                                $arrSearch[] = array(
                                    "LOGIC" => "OR",
                                    array("=UF_REGISTER_TYPE" => $RT),
                                    array("=UF_REGISTER_TYPE" => -1)
                                );
                            else
                                $arrSearch[] = array(
                                    "LOGIC" => "OR",
                                    array("=UF_REGISTER_TYPE" => null),
                                    array("=UF_REGISTER_TYPE" => 0)
                                );
                            break;
                        case "UF_CHANGED":
                            if ($request->get($field) == 1)
                                $arrSearch["=UF_CHANGED"] = 1;
                            else
                                $arrSearch[] = array(
                                    "LOGIC" => "OR",
                                    array("=UF_CHANGED" => 0),
                                    array("=UF_CHANGED" => "")
                                );
                            break;
                        default:
                            $arrSearch[$field] = $request->get($field);
                    }
                }
            }

            if ( count($arFilter) > 0 )
                $arFilter = array_merge_recursive($arFilter, $arrSearch);
            else
                $arFilter = $arrSearch;
        }

        /* Получить список пользователей по фильтру и параметрам */
        $dbRes = CUser::GetList($by = "ID", $order = "DESC", $arFilter, $arParams);
        $dbRes->NavStart(10, false);

        /* В буфер занести HTML вывода навигационной панели Пагинации */
        ob_start();
        $dbRes->NavPrint("Пользователи", true, "elr_nav_num_page", __DIR__.DIRECTORY_SEPARATOR . "templates" . DIRECTORY_SEPARATOR . $this->getTemplateName(). DIRECTORY_SEPARATOR . "nav_string.php");
        $this->arResult["NAV_PRINT"] = ob_get_contents();
        ob_end_clean();

        $arUsers = [];

        while ($user = $dbRes->NavNext(false))
            $arUsers[$user["ID"]] = $user;

        return $arUsers;
    }

    /**
     * @param array $arMapSearchKey
     */
    public function setArMapSearchKey( )
    {
        $this->arMapSearchKey = array(
            "ID", "EMAIL", "NAME", "UF_LIBRARY", "UF_ABIS_ID", "LOGIN",
            "ACTIVE", "UF_NUM_ECHB", "DATE_INPUT_FROM", "DATE_INPUT_TO",
            "UF_REGISTER_TYPE", "UF_DATE_OF_FINISH_FROM",
            "UF_DATE_OF_FINISH_TO", "UF_CHANGED"
        );
    }

    private function getListLibrary ( $all = true )
    {
        global $USER;

        $library = array();

        if ( $all === false )
        {
            if (!isset($this->thisUser) || count($this->thisUser) < 1 )
                $this->thisUser = $USER->GetByID( $USER->GetID() )->Fetch();

            $filter = array("ID" => (int)$this->thisUser['UF_LIBRARY'], "IBLOCK_ID" => $this->arUF_LIBRARY["SETTINGS"]["IBLOCK_ID"], "ACTIVE" => "Y");
        }
        else
        {
            $filter = array("IBLOCK_ID" => $this->arUF_LIBRARY["SETTINGS"]["IBLOCK_ID"], "ACTIVE" => "Y");
        }

        $dbRes = \Bitrix\Iblock\ElementTable::getList(
            array(
                "filter" => $filter,
                "order" => array("NAME" => "ASC")
            )
        );

        while ($lib = $dbRes->fetch())
            $library[ $lib["ID"] ] = $lib;

        return $library;
    }

    private function entityUfLibrary ()
    {
        if (!isset($this->arUF_LIBRARY) || count($this->arUF_LIBRARY) < 1)
            $this->arUF_LIBRARY = (new CUserTypeEntity)->GetList(array(), array("FIELD_NAME" => "UF_LIBRARY"))->Fetch();
    }
}