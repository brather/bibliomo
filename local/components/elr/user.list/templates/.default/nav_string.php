<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 25.11.2016
 * Time: 16:45
 */?>
<div class="content">
<div class="row">
    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
    <? if($this->bFirstPrintNav)
{?>
    <a name="nav_start<?=$this->add_anchor?>"></a>
    <? $this->bFirstPrintNav = false;
} ?>

<div class="pull-right">
<font class="<?=$StyleText?>"><?

if($this->bDescPageNumbering === true)
{
    $makeweight = ($this->NavRecordCount % $this->NavPageSize);
    $NavFirstRecordShow = 0;
    if($this->NavPageNomer != $this->NavPageCount)
        $NavFirstRecordShow += $makeweight;

    $NavFirstRecordShow += ($this->NavPageCount - $this->NavPageNomer) * $this->NavPageSize + 1;

    if ($this->NavPageCount == 1)
        $NavLastRecordShow = $this->NavRecordCount;
    else
        $NavLastRecordShow = $makeweight + ($this->NavPageCount - $this->NavPageNomer + 1) * $this->NavPageSize;

    echo $NavFirstRecordShow;
    echo ' - '.$NavLastRecordShow;
    echo ' '.GetMessage("nav_of").' ';
    echo $this->NavRecordCount;
    echo "</font></div>";?>
    <font class="<?=$StyleText?>">

    <? if($this->NavPageNomer < $this->NavPageCount)
        echo '<a href="'.$sUrlPath.'?PAGEN_'.$this->NavNum.'='.$this->NavPageCount.$strNavQueryString.'">'.$sBegin.'</a>&nbsp;|&nbsp;<a href="'.$sUrlPath.'?PAGEN_'.$this->NavNum.'='.($this->NavPageNomer+1).$strNavQueryString.'">'.$sPrev.'</a>';
    else
        echo '<span class="begin"></span><span class="prev"></span>';

    $NavRecordGroup = $nStartPage;
    while($NavRecordGroup >= $nEndPage)
    {
        $NavRecordGroupPrint = $this->NavPageCount - $NavRecordGroup + 1;
        if($NavRecordGroup == $this->NavPageNomer)
            echo '<span class="page-num">'.$NavRecordGroupPrint.'</span>';
        else
            echo '<a class="page-num" href="'.$sUrlPath.'?PAGEN_'.$this->NavNum.'='.$NavRecordGroup.$strNavQueryString.'">'.$NavRecordGroupPrint.'</a>';
        $NavRecordGroup--;
    }
    echo '|&nbsp;';
    if($this->NavPageNomer > 1)
        echo '<a class="end " href="'.$sUrlPath.'?PAGEN_'.$this->NavNum.'='.($this->NavPageNomer-1).$strNavQueryString.'">'.$sNext.'</a>&nbsp;|&nbsp;<a href="'.$sUrlPath.'?PAGEN_'.$this->NavNum.'=1'.$strNavQueryString.'"></a>';
    else
        echo '<span class="next"></span><span class="end"></span>';
    }
else
{
    echo ($this->NavPageNomer-1)*$this->NavPageSize+1;
    echo ' - ';
    if($this->NavPageNomer != $this->NavPageCount)
        echo $this->NavPageNomer * $this->NavPageSize;
    else
        echo $this->NavRecordCount;
    echo ' '.GetMessage("nav_of").' ';
    echo $this->NavRecordCount;
    echo "</font></div>";?>

    <font class="<?=$StyleText?>">

    <? if($this->NavPageNomer > 1)
        echo '<a class="glyphicon glyphicon-fast-backward href="'.$sUrlPath.'?PAGEN_'.$this->NavNum.'=1'.$strNavQueryString.'">&nbsp;</a><a class="glyphicon glyphicon-step-backward" href="'.$sUrlPath.'?PAGEN_'.$this->NavNum.'='.($this->NavPageNomer-1).$strNavQueryString.'">&nbsp;</a>';
    else
        echo '<span class="glyphicon glyphicon-fast-backward">&nbsp;</span><span class="glyphicon glyphicon-step-backward">&nbsp;</span>';

    $NavRecordGroup = $nStartPage;
    while($NavRecordGroup <= $nEndPage)
    {
        if($NavRecordGroup == $this->NavPageNomer)
            echo '<span class="page-num">'.$NavRecordGroup.'</span>';
        else
            echo '<a class="page-num" href="'.$sUrlPath.'?PAGEN_'.$this->NavNum.'='.$NavRecordGroup.$strNavQueryString.'">'.$NavRecordGroup.'</a>';
        $NavRecordGroup++;
    }
    if($this->NavPageNomer < $this->NavPageCount)
        echo '<a class="glyphicon glyphicon-step-forward" href="'.$sUrlPath.'?PAGEN_'.$this->NavNum.'='.($this->NavPageNomer+1).$strNavQueryString.'">&nbsp;</a><a href="'.$sUrlPath.'?PAGEN_'.$this->NavNum.'='.$this->NavPageCount.$strNavQueryString.'" class="glyphicon glyphicon-fast-forward">&nbsp;</a>';
    else
        echo '<span class="glyphicon glyphicon-step-forward">&nbsp;</span><span class="glyphicon glyphicon-fast-forward">&nbsp;</span>';
}

if($this->bShowAll)
    echo $this->NavShowAll? '|&nbsp;<a href="'.$sUrlPath.'?SHOWALL_'.$this->NavNum.'=0'.$strNavQueryString.'">'.$sPaged.'</a>&nbsp;' : '|&nbsp;<a href="'.$sUrlPath.'?SHOWALL_'.$this->NavNum.'=1'.$strNavQueryString.'">'.$sAll.'</a>&nbsp;';

echo '</font></div>';?>
</div>
</div>