<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 25.11.2016
 * Time: 9:50
 */
global $USER, $APPLICATION;

if ( $USER->IsAdmin() )
    CJSCore::Init("admin_interface");

CJSCore::RegisterExt("bootstrap_ext", Array(
    "js"    =>  "/bitrix/js/elr.useraccess/bootstrap.min.js",
    "css"   =>  "/bitrix/css/elr.useraccess/bootstrap.min.css",
    "rel"   =>  array('jquery2'),
    "skip_core" => "Y"
));

CJSCore::RegisterExt("jquery16", Array(
    "js"    =>  "https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js",
    "skip_core" => "N"
));

CJSCore::RegisterExt("chosen_css", Array(
    "css" => "/bitrix/css/elr.useraccess/chosen.css",
    "skip_core" => "Y"
));
CJSCore::Init(array("bootstrap_ext", "chosen_css"));
?>
<section role="search" class="bootstrap">
    <form role="form" action="<? $APPLICATION->GetCurPage();?>" method="GET" onsubmit="searchUsers(this); return false;" >
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 form-group">
                    <label class="label-" for="USER_ID">ID
                        <input type="text" class="form-control" id="USER_ID" name="ID" placeholder="ID" value="<?=$_REQUEST['USER_ID']?>"/>
                    </label>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 form-group">
                    <label for="EMAIL">Email
                        <input type="text" class="form-control" id="EMAIL" name="EMAIL" placeholder="email" value="<?=$_REQUEST['EMAIL']?>" />
                    </label>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 form-group">
                    <label for="NAME">ФИО
                        <input type="text" class="form-control" id="NAME" name="NAME" placeholder="Ф.И.О." value="<?=$_REQUEST['NAME']?>" />
                    </label>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 form-group">
                    <label for="UF_LIBRARY">Наименование библиотеки</label>
                    <select style="width: 100%;" data-placeholder="Выбрать библиотеку" type="text" class="form-control chosen_library" id="UF_LIBRARY" name="UF_LIBRARY">
                        <option value=""<?= (!isset($_REQUEST['UF_LIBRARY']) || empty($_REQUEST['UF_LIBRARY']))?"selected='selected'":""?>></option>
                        <? foreach ($arResult['LIBRARIES'] as $lib): ?>
                            <option value="<?=$lib["ID"] ?>" <?=($_REQUEST['UF_LIBRARY'] == $lib["ID"]) ? 'selected="selected"': ''?>><?=$lib["NAME"] ?></option>
                        <? endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 form-group">
                    <label for="UF_ABIS_ID">ID АБИС
                        <input type="text" class="form-control" id="UF_ABIS_ID" name="UF_ABIS_ID" placeholder="АБИС ID" value="<?=$_REQUEST['UF_ABIS_ID']?>" />
                    </label>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 form-group">
                    <label for="UF_ABIS_ID">Логин
                        <input type="text" class="form-control" id="LOGIN" name="LOGIN" value="<?=$_REQUEST['LOGIN']?>" />
                    </label>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 form-group">
                    <label>Активность
                        <select name="ACTIVE" type="text" class="form-control" id="ACTIVE"  >
                            <option value="" selected=""></option>
                            <option value="Y">Да</option>
                            <option value="N">Нет</option>
                        </select>
                    </label>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 form-group">
                    <label for="UF_NUM_ECHB">№ читательского билета
                        <input type="text" class="form-control" id="UF_NUM_ECHB" name="UF_NUM_ECHB" value="<?=$_REQUEST['UF_NUM_ECHB']?>" />
                    </label>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 form-group">
                    <label>Дата регистрации</label>
                    <div class="input-group">
                        <input type="text" class="form-control" id="DATE_INPUT_FROM" name="DATE_INPUT_FROM" placeholder="с" value="<?=$_REQUEST['DATE_INPUT_FROM']?>" />
                        <span class="input-group-addon" onclick="BX.calendar({node: $('#DATE_INPUT_FROM')[0], field: $('#DATE_INPUT_FROM')[0], bTime: false });"><span class="glyphicon glyphicon-calendar"></span></span>
                        <input type="text" class="form-control" id="DATE_INPUT_TO" name="DATE_INPUT_TO" placeholder="по"  value="<?=$_REQUEST['DATE_INPUT_TO']?>"/>
                        <span class="input-group-addon" onclick="BX.calendar({node: $('#DATE_INPUT_TO')[0], field: $('#DATE_INPUT_TO')[0], bTime: false });"><span class="glyphicon glyphicon-calendar"></span></span>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 form-group">
                    <label>Тип регистрации
                        <select name="UF_REGISTER_TYPE" type="text" class="form-control" id="UF_REGISTER_TYPE" >
                            <option value="" selected=""></option>
                            <option value="null">Регистрация через портал</option>
                            <option value="240">Регистрация через ОПАК</option>
                            <option value="241">Регистрация через ИРБИС</option>
                        </select>
                    </label>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 form-group">
                    <label>Измененные записи
                        <select name="UF_CHANGED" type="text" class="form-control" id="UF_CHANGED" >
                            <option value="" selected="selected"></option>
                            <option value="1">Да</option>
                            <option value="0">Нет</option>
                        </select>
                    </label>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 form-group">
                    <label>Срок читательского билета</label>
                    <div class="input-group">
                        <input type="text" class="form-control" id="UF_DATE_OF_FINISH_FROM" name="UF_DATE_OF_FINISH_FROM" placeholder="с" value="<?=$_REQUEST['UF_DATE_OF_FINISH_FROM']?>" />
                        <span class="input-group-addon" onclick="BX.calendar({node: $('#UF_DATE_OF_FINISH_FROM')[0], field: $('#UF_DATE_OF_FINISH_FROM')[0], bTime: false });"><span class="glyphicon glyphicon-calendar"></span></span>
                        <input type="text" class="form-control" id="UF_DATE_OF_FINISH_TO" name="UF_DATE_OF_FINISH_TO" placeholder="по" value="<?=$_REQUEST['UF_DATE_OF_FINISH_TO']?>" />
                        <span class="input-group-addon" onclick="BX.calendar({node: $('#UF_DATE_OF_FINISH_TO')[0], field: $('#UF_DATE_OF_FINISH_TO')[0], bTime: false });"><span class="glyphicon glyphicon-calendar"></span></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 col-lg-offset-8 col-md-offset-8 col-sm-offset-6 col-xs-offset-0 form-group pull-right" style="text-align: right">
                    <button id="reset-search-form" type="reset" class="btn-link btn-lg btn-reset" onmouseup="$('#UF_LIBRARY').trigger('chosen:updated');">Сбросить все</button>
                    <button type="submit" class="btn bt_typelt btn-lg no-radius">Найти</button>
                </div>
            </div>
        </div>
    </form>
    <script src="/bitrix/js/elr.useraccess/chosen.jquery.js" type="text/javascript"></script>
    <script type="text/javascript">
        $('.chosen_library').chosen({
            no_results_text: "Нет результатов поиска",
            disable_search_threshold: 5
        });
    </script>
</section>
<div id="tableUsers">
<?
if (count($arResult['USERS']) > 0)
{?>
    <div class="table-responsive" id="resultTableUsers">
        <table class="table table-striped table-bordered" style="font-size: 0.8em">
        <thead>
            <tr>
                <? if ($USER->IsAdmin()):?>
                    <th colspan="2">&nbsp;</th>
                    <!--<th>&nbsp;</th>-->
                <? endif;?>
                <th class="middle">Логин / Email</th>
                <th class="middle">ФИО</th>
                <th class="middle">ID АБИС</th>
                <th class="middle">Наименование библиотеки</th>
                <th class="middle">№ читательского билета</th>
                <th class="middle">Дата регистрации</th>
                <th class="middle">Активен</th>
            </tr>
        </thead>
        <tbody>
            <? foreach ($arResult['USERS'] as $id => $user):?>
                <? $user["UF_DATE_OF_FINISH"] = ($user["UF_DATE_OF_FINISH"] == "01.01.1970")? "-" : $user["UF_DATE_OF_FINISH"]; ?>
                <tr <?=($user["UF_CHANGED"] > 0)? 'class="user_changed"':'';?>>
                    <? if ($USER->IsAdmin()):?>
                        <td class="middle center">
                            <? if ($user["UF_MERGED_ID"] > 0 || $user["UF_MERGED_ID"] == -1):?>
                                <span title="Является частью объединения с ID <?=$user["UF_MERGED_ID"]?>" class="glyphicon glyphicon-ok-sign" style="font-size: 1.5em;color: #cdbe93;"></span>
                            <? else: ?>
                            <input class="from-merge" data-id="<?=$id?>" type="checkbox" name="users[<?=$id?>]" value="off" />
                            <? endif; ?>
                            </td>
                        <td class="middle center label-edit"><a href="/bitrix/admin/user_edit.php?lang=ru&ID=<?=$id?>" target="_blank"><span class="adm-list-table-popup glyphicon glyphicon-pencil"></span></a></td>
                    <? endif;?>
                    <td class="middle">
                        <? if ($user["UF_MERGED_ID"] >= 0): ?>
                        <div class="span-login"><?=$user['LOGIN']?></div>
                        <div class="span-email"><?=$user["EMAIL"]?></div>
                        <? else: ?>
                        <div class="span-login">[системный пользователь]</div>
                        <? endif; ?>
                    </td>
                    <td class="middle elr-ua-fio"><?=$user["LAST_NAME"]?><br><?=$user["NAME"]?><br><?=$user["SECOND_NAME"]?></td>
                    <td class="middle"><span class="for-edit"><?=$user["UF_ABIS_ID"]?></span></td>
                    <td><?=$arResult["LIBRARIES"][$user["UF_LIBRARY"]]["NAME"]?></td>
                    <td class="middle center">
                        <?=$user["UF_NUM_ECHB"]?>
                        <div class="small"><?=$user["UF_DATE_OF_FINISH"]?></div>
                    </td>
                    <td class="middle center"><?=substr($user["DATE_REGISTER"], 0, 10)?></td>
                    <td class="middle center"><?=($user["ACTIVE"] === "Y") ? "Да" : "Нет"?></td>
                </tr>
            <? endforeach; ?>
        </tbody>
    </table>
    </div>
    <? if ($USER->IsAdmin()):?>
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                <?=$arResult["NAV_PRINT"];?>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 control-btn">
                    <a disabled="disabled" class="btn btn-lg btn-default" id="do-merge" data-toggle="modal" data-target="#mergedModal"><span class="hidden-xs hidden-sm">Объеденить выбранные</span><span class="visible-sm">Объединение</span><span class="visible-xs glyphicon glyphicon-th-list"></span></a>
                    <a type="button" class="btn btn-lg btn-default btn-green" href="/bitrix/admin/user_edit.php?lang=ru" target="_blank">+ Добавить</a>
            </div>
        </div>
    <div class="modal fade" id="mergedModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Объединение выбранных</h4>
                </div>
                <div class="modal-body" id="containerModalMerged">
                    <div  class="table-responsive">
                        <table  class="table table-striped table-bordered" style="font-size: 0.8em">
                            <thead>
                            <tr>
                                <th class="middle">Исп. ФИО</th>
                                <th class="middle">Логин / Email</th>
                                <th class="middle">ФИО</th>
                                <th class="middle">ID АБИС</th>
                                <th class="middle">Наименование библиотеки</th>
                                <th class="middle">№ читательского билета</th>
                                <th class="middle">Дата регистрации</th>
                                <th class="middle">Активен</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>

                </div>
                <div class="modal-footer control-btn">
                    <div class="form-inline">
                        <div class="form-group">
                            <button type="button" class="btn btn-lg btn-primary" disabled="disabled">Объеденить</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <? else:?>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <?=$arResult["NAV_PRINT"];?>
        </div>
    </div>
    <? endif; ?>
<?} else { ?>

    <div id="resultTableUsers">
        <h2>Ничего не найдено.</h2>
    </div>

<? } ?>
</div>
