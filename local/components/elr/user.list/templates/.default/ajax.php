<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 06.12.2016
 * Time: 10:20
 */
require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php';
global $USER;

\Bitrix\Main\Loader::includeModule("elr.useraccess");

$rq = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();

if ($rq->isAjaxRequest() && $USER->IsAdmin())
{
    $post = $rq->getPostList();
    switch ($post->get('action'))
    {
        case "addGluingUser":
            $au = new Elr\AddUser($post->get("data"), $post->get('ids'), $post->get('main'));
            $res = $au->gluingUser();
            die(json_encode($res));
            break;
    }
}

die();
