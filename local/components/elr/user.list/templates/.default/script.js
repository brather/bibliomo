/**
 * Created by AChechel on 25.11.2016.
 */
$(function(){
    putHashToPagination = function () {
        var hashID = $('div.useraccess').attr('id');
        $.each($('font.elr_nav_num_page a'), function(index, value){
            var $this = $(value),
                href = $this.attr('href');
            $this.attr('href', href + "#" + hashID);
        });
    };

    putHashToPagination();

    document.searchUsers = function (t){
        var data = {};

        $.each(t, function(i, val){
            if (val.name !== "")
                data[val.name] = val.value;
        });

        data['action'] = "SEARCH";

        $.ajax({
            url: location.origin + location.pathname,
            data: data,
            dataType: "html",
            method: "get",
            beforeSend: function(){
                $("#tableUsers").html('<div class="center"><img src="/local/templates/rnb/images/bx_loader.gif" /></div>');
            },
            success: function(html){
                var tableUsers = $(html).find("#tableUsers");
                delete $(html);


                $("#tableUsers").html( tableUsers.html() );

                putHashToPagination();

                tableUsers = undefined;
            }
        });
    };

    $(document).on('click', '#reset-search-form', function(event){

        event.preventDefault();

        $(this).closest('form').get(0).reset();

        $(this).siblings('button').trigger('click');
    });

    /* отмечаем для склеивания */
    $(document).on('click', '#resultTableUsers input.from-merge', function(e){
        var checked = $('#resultTableUsers input.from-merge:checked'),
            btnMerged = $('#do-merge'),
            tableBody = $('#containerModalMerged table tbody');
        if (checked.length > 1){
            btnMerged.removeAttr('disabled');
        } else {
            btnMerged.attr('disabled', 'disabled');
        }
        tableBody.html("");
        var tr = checked.parents('tr').clone();
        tr.children('.label-edit').remove();
        tableBody.html(tr);
        tableBody.find('input:checked').attr({'checked': false, type: 'radio', name: 'MAIN'});
    });

    $(document).on('click', '#containerModalMerged input.from-merge', function(){
        $('#mergedModal button').removeAttr('disabled');
    });

    $(document).on('click', '#mergedModal div.control-btn button', function(){
        var validate = false,
            ids = [];

        $.each($('#mergedModal input[type=text]'), function(k,v){
            if (v.value == ""){
                validate = true;
                $(v).parent().addClass('has-error');
            }else{
                $(v).parent().removeClass('has-error');
                $(v).parent().addClass('has-success');
            }
        });

        if (validate){
            return alert("Не все поля заполнены");
        }

        $.each($('#mergedModal input[type=radio]'), function (k,v) {
            ids.push($(v).data("id"));
        });

        var fullName = $('#mergedModal input[type=radio]:checked').parent().parent().find('.elr-ua-fio').html().split('<br>');

        $.ajax({
            url: '/local/components/elr/user.list/templates/.default/ajax.php',
            data: {
                action: 'addGluingUser',
                main: $('#mergedModal input[type=radio]:checked').data("id"),
                ids: ids,
                data: {
                    NAME: fullName[1],
                    LAST_NAME: fullName[0],
                    SECOND_NAME: fullName[2]
                }
            },
            dateType: 'json',
            method: 'post',
            beforeSend: function () {
                $('#mergedModal .modal-footer .form-inline').prepend('<div class="form-group"><img height="42" id="modal-loader" src="/bitrix/components/bitrix/sale.order.payment.change/templates/.default/images/loader.gif" /></div>');
            },
            success: function (res) {
                $('#modal-loader').parent().parent().html('<h1>Пользователь успешно добавлен!</h1>');
                setTimeout(function (){
                    $("#mergedModal").find('button.close').trigger('click');
                    $("section[role=search] form").trigger('submit');
                }, 2000);
                console.log(res);
            },
            error: function (st, er) {
                $('#modal-loader').parent().text(er);
                console.error(st, er);
            }
        });
    });
});