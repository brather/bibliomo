<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 14.12.2016
 * Time: 12:22
 */

if (!is_array($arResult['LIB_SERVICES']) || count($arResult['LIB_SERVICES']) < 1) die("<tr class=\"center\"><td colspan=\"5\">У этой библиотеки нет связаных услуг</td></tr>");

foreach ($arResult['LIB_SERVICES'] as $libService):
?>
<tr>
    <td class="middle"><?= $libService['UF_SERVICE_CODE']?></td>
    <td class="middle"><?= $libService['UF_NAME']?></td>
    <td class="middle"><?= $libService['UF_SERVICE_TYPE']?></td>
    <td class="middle"><?= $libService['UF_DESCRIPTION']?></td>
    <td class="middle"><span class="remove-service-lib glyphicon glyphicon-trash" data-id="<?=$libService['ID']?>"></span></td>
</tr>
<? endforeach;