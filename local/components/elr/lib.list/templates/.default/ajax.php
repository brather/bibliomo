<?php require_once $_SERVER["DOCUMENT_ROOT"] . '/bitrix/modules/main/include/prolog_before.php';
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 07.12.2016
 * Time: 12:52
 */
global $APPLICATION, $USER;
if(!CSite::InGroup(array(1,6))) die("Access denied");

if($_REQUEST['action'] === "getServiceLib")
{
    $APPLICATION->IncludeComponent("elr:lib.list", "tbody");
}
elseif ($_REQUEST['action'] === "updateLibraryID")
{
    echo require_once "./action_update.php";
}
elseif ($_REQUEST['action'] === "removeLibraryID")
{
    echo require_once "./action_remove.php";
}
else
{
    $APPLICATION->IncludeComponent("elr:lib.list", ".default");
}


