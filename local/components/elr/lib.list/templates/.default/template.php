<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 06.12.2016
 * Time: 17:01
 */
global $USER, $APPLICATION;
CJSCore::RegisterExt("bootstrap_ext", Array(
    "js"    =>  "/bitrix/js/elr.useraccess/bootstrap.min.js",
    "css"   =>  "/bitrix/css/elr.useraccess/bootstrap.min.css",
    "rel"   =>  array('jquery2'),
    "skip_core" => "Y"
));
CJSCore::RegisterExt("chosen_css", Array(
    "css" => "/bitrix/css/elr.useraccess/chosen.css",
    "skip_core" => "Y"
));
CJSCore::Init(array("bootstrap_ext", "chosen_css"));
?>
<div id="search-library" class="bootstrap">
    <form role="form" name="searchLibrary" method="post" id="search-library-form" onsubmit="$(this).goToPage(1, true); return false;">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label for="nameLibrary">Наименование библиотеки</label>
                        <input id="nameLibrary" type="text" class="form-control col-4-md" value="" placeholder="Название или часть названия" name="NAME"/>
                    </div>
                </div>
                <div class="clearfix visible-xs"></div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label for="active">Активность</label>
                        <select id="active" type="text" class="form-control col-4-md" name="ACTIVE">
                            <option value="" selected="selected"></option>
                            <option value="Y">Да</option>
                            <option value="N">Нет</option>
                        </select>
                    </div>
                </div>
                <div class="clearfix visible-sm visible-xs"></div>
                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 col-lg-offset-4 col-md-offset-4 col-sm-offset-2">
                    <button type="submit" class="btn bt_typelt btn-lg no-radius">Найти</button>
                </div>
            </div>
        </div>
    </form>
</div>
<div id="tableLibrary">
    <? if (count($arResult['LIBRARIES']) > 0):?>
        <div class="table-responsive" id="resultLibraryList">
            <table class="table table-striped table-bordered" style="font-size: 0.8em">
                <thead>
                <tr>
                    <? if (isset($arResult['ORDER_BY']))
                    {
                        foreach ($arResult['ORDER_BY'] as $key => $val)
                        {
                            $order = $key;
                            $by = $val;
                        }
                    } else {
                        $order = "NAME";
                        $by = "ASC";
                    }
                    ?>
                    <th class="middle" colspan="<?= CSite::InGroup(array(1)) ? '2' : '1';?>">#</th>
                    <th class="middle">Наименование бибилиотеки<span id="orderByLibName" data-type="ORDER" data-by="<?=$by?>" data-order="<?=$order?>" class="glyphicon glyphicon-sort-by-attributes<?=($by == "DESC")?"-alt":""?> pull-right" onclick="$(this).goToPage(1)"></span></th>
                    <th class="middle">Адресс</th>
                    <th class="middle" width="130">Телефон</th>
                    <th class="middle">Email</th>
                    <th class="middle">Сайт</th>
                    <th class="middle">Акт.</th>
                </tr>
                </thead>
                <tbody>
                    <? foreach ($arResult['LIBRARIES'] as $id => $library): ?>
                        <tr>
                        <? if (CSite::InGroup(array(1))):?>
                            <td class="middle center label-edit"><a href="/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=4&type=library&ID=<?=$id?>&lang=ru&find_section_section=-1&WF=Y" target="_blank"><span class="adm-list-table-popup glyphicon glyphicon-pencil"></span></a></td>
                            <? endif; ?>
                            <td class="middle"><a data-toggle="modal" data-target="#listServicesLib" onclick="$(this).loadLibServices(<?=$id?>)" title="Доступные услуги"><span class="glyphicon glyphicon-briefcase"></span></a></td>
                            <td class="middle"><?=$library["NAME"]?></td>
                            <td class="middle"><?=$library["PROPS"]["PROPERTY_5"];?></td>
                            <td class="middle"><?=implode("<br>", $library["PROPS"]["PROPERTY_7"]["VALUE"]);?></td>
                            <td class="middle"><?=$library["PROPS"]["PROPERTY_8"];?></td>
                            <td class="middle"><?=$library["PROPS"]["PROPERTY_10"];?></td>
                            <td class="middle center"><?=($library["ACTIVE"] === "Y") ? "Да" : "Нет"?></td>
                        </tr>
                    <? endforeach; ?>
                </tbody>
            </table>
        </div>
        <div class="container" style="box-sizing: border-box;">
            <div class="row">
                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-11">
                    <div class="elr_nav_num_page">
                        <? if ( $arResult["COUNT_PAGES"] > 1 ):
                        if ( !isset( $arResult['PAGE'] ) || $arResult['PAGE'] < 2 ):?>
                            <span class="glyphicon glyphicon-fast-backward">&nbsp;</span>
                            <span class="glyphicon glyphicon-step-backward">&nbsp;</span>
                        <? else: ?>
                            <a class="glyphicon glyphicon-fast-backward" href="#" onclick="$(this).goToPage(1); return false;" >&nbsp;</a>
                            <a class="glyphicon glyphicon-step-backward" href="#" onclick="$(this).goToPage(<?= ( $arResult["PAGE"] - 1 ) ?>); return false;">&nbsp;</a>
                        <? endif;

                        /* Входные параметры */
                        $count_pages = $arResult['COUNT_PAGES'];
                        $active = $arResult['PAGE'];
                        $count_show_pages = 5;

                        $left = $active - 1;
                        $right = $count_pages - $active;
                        if ( $left < floor( $count_show_pages / 2 ) )
                        {
                            $start = 1;
                        }
                        else
                        {
                            $start = $active - floor( $count_show_pages / 2 );
                        }

                        $end = $start + $count_show_pages - 1;

                        if ( $end > $count_pages )
                        {
                            $start -= ( $end - $count_pages );
                            $end = $count_pages;

                            if ( $start < 1 )
                            {
                                $start = 1;
                            }
                        }

                        for ( $i = $start; $i <= $end; $i++ )
                        {
                            if ( $i == $active )
                            {
                                ?><span class="page-num"><?= $arResult['PAGE']; ?></span><?php
                            }
                            else
                            {
                                ?><a onclick="$(this).goToPage(<?= $i; ?>); return false;" href="#"><?= $i ?></a><?php
                            }
                        } ?>
                        <? if ( isset( $arResult['PAGE'] )
                            && $arResult['PAGE'] == ( $arResult["COUNT_PAGES"] )
                        ):?>
                            <span class="glyphicon glyphicon-fast-forward">&nbsp;</span>
                            <span class="glyphicon glyphicon-step-forward">&nbsp;</span>
                        <? else: ?>
                            <a class="glyphicon glyphicon-step-forward" href="#" onclick="$(this).goToPage(<?= ( $arResult["PAGE"] + 1 ) ?>); return false;">&nbsp;</a>
                            <a class="glyphicon glyphicon-fast-forward" href="#" onclick="$(this).goToPage(<?= ( $arResult["COUNT_PAGES"] ) ?>); return false;">
                                &nbsp;</a>
                        <? endif; ?>
                        <div class="pull-right">
                            <span><?= ( (int)$arResult['PAGE'] * $arResult["LIMIT"] ) - $arResult["LIMIT"] + 1 ?> - <?= ( ( (int)$arResult['PAGE'] + 1 ) * $arResult["LIMIT"] ) - $arResult["LIMIT"] ?> из <?= $arResult['COUNT_ROWS'] ?></span>
                        </div>
                        <? endif; ?>
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-1 control-btn">
                    <? if (CSite::InGroup(array(1))):?>
                    <a class="pull-right btn btn-lg btn-default btn-green" href="/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=4&type=library&ID=0&lang=ru&IBLOCK_SECTION_ID=-1&find_section_section=-1&from=iblock_list_admin" target="_blank">+ Добавить</a>
                    <? endif; ?>
                </div>                
            </div>
        </div>
        <div class="modal fade" id="listServicesLib" tabindex="-1" role="dialog" aria-labelledby="libServicesLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="libServicesLabel">Доступные услуги</h4>
                    </div>
                    <div class="modal-body" id="containerModalServicesLib">
                        <div class="table-responsive">
                            <table class="table table-striped table-border" style="font-size: 0.8em">
                                <thead>
                                    <tr>
                                        <th class="middle">Код услуги</th>
                                        <th class="middle">Наименование услуги</th>
                                        <th class="middle">Вид услуги</th>
                                        <th class="middle">Описание услуги</th>
                                        <th class="middle">&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="modal-footer control-btn container">
                        <div class="row">
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-6">
                                <div class="pull-left" id="loaderContainer"></div>
                                <label for="libraryServicesSelect">Добавить услугу: </label>
                                    <select multiple class="chosen-services-list form-control"   data-placeholder="Услуги" name="SERVICES" id="libraryServicesSelect">
                                        <option value=""></option>
                                        <? foreach ($arResult['LIB_SERVICES_ALL'] as $key => $libServices):?>
                                            <option value="<?=$libServices['ID']?>"><?=$libServices['UF_NAME']?></option>
                                        <? endforeach;?>
                                    </select>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6">
                                <div class="form-inline">
                                    <div class="form-group">
                                        <button type="button" class="btn btn-lg btn-primary" >Применить</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="/bitrix/js/elr.useraccess/chosen.jquery.js" type="text/javascript"></script>
        <script type="text/javascript">
            $('.chosen-services-list').chosen();
        </script>
    <? endif; ?>
</div>