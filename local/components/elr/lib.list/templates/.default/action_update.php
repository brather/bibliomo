<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 14.12.2016
 * Time: 13:38
 */
use Bitrix\Main\Loader,
    Bitrix\Highloadblock\HighloadBlockTable,
    Bitrix\Main\Application;

Loader::includeModule("highloadblock");

global $APPLICATION;

$rq = Application::getInstance()->getContext()->getRequest();

$db = HighloadBlockTable::getList(array(
    "select" => array(
        "TABLE_NAME", "ID"
    ),
    "filter" => array(
        "=NAME" => "LibraryServices"
    )
));

if ($table = $db->fetch())
{
    $hbID = $table['ID'];

    $hlblock = HighloadBlockTable::getById( $hbID )->fetch();
    $entity = HighloadBlockTable::compileEntity( $hlblock );
    $class = $entity->getDataClass(); // string "\LibraryServicesTable"

    $libID = $rq->getPost("libID");
    $values = $rq->getPost('values');

    if (is_array($values) && count($values) > 0)
    {
        foreach ($values as $val)
        {
            $UF_LIBRARY_LIST = array();
            $ufLS = $class::getById((int)$val)->fetch();

            if (is_array($ufLS['UF_LIBRARY_LIST']))
                $UF_LIBRARY_LIST = array_merge(array($libID), $ufLS['UF_LIBRARY_LIST']);
            else
                $UF_LIBRARY_LIST = array($libID);

            $UF_LIBRARY_LIST = array_unique($UF_LIBRARY_LIST);

            $class::update((int)$val, array("UF_LIBRARY_LIST" => $UF_LIBRARY_LIST));
        }

        return json_encode(array("status" => "ok"));
    }
    else
        return json_encode(array("status" => "err"));
}

