<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 14.12.2016
 * Time: 15:53
 */
use Bitrix\Main\Loader,
    Bitrix\Highloadblock\HighloadBlockTable,
    Bitrix\Main\Application;

Loader::includeModule("highloadblock");

global $APPLICATION;

$rq = Application::getInstance()->getContext()->getRequest();

$db = HighloadBlockTable::getList(array(
    "select" => array(
        "TABLE_NAME", "ID"
    ),
    "filter" => array(
        "=NAME" => "LibraryServices"
    )
));

if ($table = $db->fetch())
{
    $hbID = $table['ID'];

    $hlblock = HighloadBlockTable::getById( $hbID )->fetch();
    $entity = HighloadBlockTable::compileEntity( $hlblock );
    $class = $entity->getDataClass(); // string "\LibraryServicesTable"

    $libID = $rq->getPost("libID");
    $values = $rq->getPost('values');

    if ( !empty( $values ) )
    {
        $UF_LIBRARY_LIST = array();
        $ufLS = $class::getById( (int)$values )->fetch();

        if ( is_array( $ufLS['UF_LIBRARY_LIST'] ) )
        {
            foreach ( $ufLS['UF_LIBRARY_LIST'] as $lib )
            {
                if ($lib == $libID) continue;

                $UF_LIBRARY_LIST[] = $lib;
            }
        }

        $class::update((int)$values, array( "UF_LIBRARY_LIST" => $UF_LIBRARY_LIST )
        );

        return json_encode( array( "status" => "ok" ) );
    }
    else
        return json_encode(array("status" => "err"));
}