/**
 * Created by AChechel on 07.12.2016.
 */
document.scrollToElement = function (theElement) {
    if (typeof theElement === "string") theElement = document.getElementById(theElement);

    var selectedPosX = 0;
    var selectedPosY = 0;

    while (theElement != null) {
        selectedPosX += theElement.offsetLeft;
        selectedPosY += theElement.offsetTop;
        theElement = theElement.offsetParent;
    }

    window.scrollTo(selectedPosX, selectedPosY);
};

$(function () {
        $.fn.goToPage = function (num, search) {
            var data = {PAGE: num},
                valNAME = document.forms.searchLibrary.elements.NAME.value,
                valACTIVE = document.forms.searchLibrary.elements.ACTIVE.value,
                action = (search === true) ? 'searchLib' : 'getLibList',
                spanOrder = $('#orderByLibName'),
                target = this.data('type');

            if (valNAME || valACTIVE) {
                data = {
                    PAGE: num,
                    action: action,
                    search: {
                        NAME: valNAME,
                        ACTIVE: valACTIVE
                    }
                };
            }
            data.ORDER = {};
            data.ORDER[ spanOrder.data('order') ] = spanOrder.data('by');

            if (target == 'ORDER'){
                if ($('#orderByLibName').data('by') == 'DESC') {
                    data.ORDER[spanOrder.data('order')] = 'ASC';
                }else{
                    data.ORDER[spanOrder.data('order')] = 'DESC';
                }
            }

            var $libTable = $("#resultLibraryList");

            $.ajax({
                url: '/local/components/elr/lib.list/templates/.default/ajax.php',
                data: data,
                dataType: "html",
                method: 'POST',
                beforeSend: function () {
                    $libTable.toggle().html('<div class="center"><img src="/local/templates/rnb/images/bx_loader.gif" /></div>').toggle();
                },
                success: function (html) {
                    $pag = $(html).find('div.elr_nav_num_page').html();
                    $table = $(html).find('#resultLibraryList').html();
                    if(!$table)
                        $table = "<h2>Нет библиотек, соответствующих вашему запросу</h2>";
                    if(!$pag)
                        $pag = "";

                    $('#tableLibrary div.elr_nav_num_page').html($pag);
                    $libTable.toggle().html($table).toggle();
                    document.scrollToElement('tabs-6');
                },
                error: function (st, er) {
                    console.error(st, er);
                }
            });
        };

        document.libIdToUpdate = 0;

        $.fn.loadLibServices = function (libID) {
            var tableBody = $('#listServicesLib .modal-body table tbody');
            tableBody.html('<tr class="center"><td colspan="5"><img src="/local/templates/rnb/images/bx_loader.gif" /></td></tr>');

            $.ajax({
                url: '/local/components/elr/lib.list/templates/.default/ajax.php',
                data: {
                    action: 'getServiceLib',
                    libID: libID
                },
                dataType: "html",
                method: 'POST',
                success: function (html){
                    document.libIdToUpdate = libID;

                    var ich = {};
                    $(html).find('span.remove-service-lib').each(function (k, v) {
                        ich[$(v).attr('data-id')] = true;
                    });

                    $('#libraryServicesSelect option').each(function (k, v) {
                        var val = $(v);

                        val.removeAttr('disabled');

                        if (ich[val.val()] === true)
                            val.attr('disabled', 'disabled');
                    });

                    $('#libraryServicesSelect').val('').trigger('chosen:updated');

                    tableBody.html(html);
                }
            });
        };

        $(document).on('click', '#listServicesLib .modal-footer button', function () {
            var values = $('#libraryServicesSelect').val();

            if (values){
                $.ajax({
                    url: '/local/components/elr/lib.list/templates/.default/ajax.php',
                    data: {
                        action: 'updateLibraryID',
                        libID: document.libIdToUpdate,
                        values: values
                    },
                    dataType: "json",
                    method: 'POST',
                    beforeSend: function (){
                        $('#loaderContainer').html('<img src="/local/templates/rnb/images/bx_loader.gif" />');
                    },
                    success: function (json){
                        $('#loaderContainer').html('');
                        if (json.status == "ok")
                            $().loadLibServices(document.libIdToUpdate);
                        else
                            $('#listServicesLib .modal-body table tbody').html('<tr class="center"><td colspan="5">Что то пошло не так</td></tr>');
                    }
                });
            }
        });


    $(document).on('click', '#listServicesLib .modal-body tbody .remove-service-lib', function () {
        var values = $(this);

        if (values){
            $.ajax({
                url: '/local/components/elr/lib.list/templates/.default/ajax.php',
                data: {
                    action: 'removeLibraryID',
                    libID: document.libIdToUpdate,
                    values: values.data('id')
                },
                dataType: "json",
                method: 'POST',
                beforeSend: function (){
                    $('#loaderContainer').html('<img src="/local/templates/rnb/images/bx_loader.gif" />');
                },
                success: function (json){
                    $('#loaderContainer').html('');
                    if (json.status == "ok")
                        values.parent().parent().toggle(800).remove();

                }
            });
        }
    });
});
