<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Localization\Loc as Loc;

Loc::loadMessages(__FILE__);

$arComponentDescription = array(
	"NAME" => Loc::getMessage('ELR_LIB_LIST_DESCRIPTION_NAME'),
	"DESCRIPTION" => Loc::getMessage('ELR_LIB_LIST_DESCRIPTION_DESCRIPTION'),
	"ICON" => '/images/icon.gif',
	"SORT" => 10,
	"PATH" => array(
		"ID" => 'ELR_LIB_LIST',
		"NAME" => '',
		"SORT" => 10,
		"CHILD" => array(
			"ID" => 'useraccess',
			"NAME" => Loc::getMessage('ELR_LIB_LIST_DESCRIPTION_GROUP'),
			"SORT" => 10
		)
	),
);

?>