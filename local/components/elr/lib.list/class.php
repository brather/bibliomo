<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 25.11.2016
 * Time: 9:22
 */
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader,
    Bitrix\Main\Application,
    Bitrix\Iblock\ElementTable,
    Elr\Iblock\IblockElementPropertyS4Table,
    Bitrix\Highloadblock\HighloadBlockTable;

if (!Loader::includeModule('elr.useraccess')) return false;

class lib_list extends CBitrixComponent
{
    const IBLOCK_ID = 4;
    private $limit = 10;
    private $page = 1;
    private $filterEx = false;
    private $isLibAdmin = false;


    public function executeComponent()
    {
        global $USER, $APPLICATION;

        if (CSite::InGroup(array(6)))
            $this->isLibAdmin = true;

        if ($_REQUEST['action'] == "getServiceLib")
            $this->_getListServices($_REQUEST['libID']);
        else
        {
            $this->arResult['LIBRARIES'] = $this->_getListLibrary();
            $this->arResult['LIMIT'] = $this->limit;
            $this->arResult['PAGE'] = $this->page;
            $this->arResult["COUNT_ROWS"] = $this->_maxRecord($this->filterEx);
            $this->arResult["COUNT_PAGES"] = ceil($this->arResult["COUNT_ROWS"] / $this->limit);
        }

        $this->_getAllListServices();

        $this->includeComponentTemplate();
    }

    /**
     * @param array $arMapSearchKey
     */
    public function setArMapSearchKey( )
    {
        $this->arMapSearchKey = array(
            "NAME", "ACTIVE"
        );
    }

    /**
     * @param int $limit
     */
    public function setLimit( $limit )
    {
        $this->limit = $limit;
    }

    private function _getListLibrary ( )
    {
        $rq = Application::getInstance()->getContext()->getRequest();

        if ( $rq->isAjaxRequest() )
        {
            $this->page = $rq->getPost("PAGE");
            $this->arResult['PAGE'] = $this->page;

            if(
                ($rq->getPost("action") == "searchLib" && $search = $rq->getPost("search") ) ||
                $search = $rq->getPost("search")
            )
            {

                if ($search["ACTIVE"] !== "")
                    $filterExt['=ACTIVE'] = $search["ACTIVE"];
                if ($search["NAME"] !== "")
                    $filterExt['%NAME'] = $search["NAME"];

                $this->filterEx = (isset($filterExt) && count($filterExt) ) ? $filterExt : false ;

                unset($filterExt);
            }
        }

        if ($this->isLibAdmin === true)
        {
            global $USER;

            $user = CUser::GetByID($USER->GetID())->Fetch();

            $this->filterEx['=ID'] = (int)$user['UF_LIBRARY'];
        }

        $filter = array(
            "=IBLOCK_ID" => static::IBLOCK_ID
        );

        if (is_array($this->filterEx) && count($this->filterEx))
        {
            $filter = array_merge_recursive($filter, $this->filterEx);
        }

        $order =  array(
            "NAME" => "asc"
        );

        if ($orderExt = $rq->getPost("ORDER"))
        {
            $this->arResult['ORDER_BY'] = $orderExt;
            $order = $orderExt;
            unset($orderExt);
        }

        $dbRes = ElementTable::getList(
            array(
                "filter" => $filter,
                "limit" => $this->limit,
                "offset" => ($this->page - 1) * $this->limit,
                "order" => $order
            )
        );

        $library = array();

        while ($lib = $dbRes->fetch() )
        {
            $library[ $lib["ID"] ] = $lib;

            $propRes = IblockElementPropertyS4Table::getList(
                array(
                    "filter" => array(
                        "IBLOCK_ELEMENT_ID" => $lib["ID"]
                    )
                )
            )->fetch();

            $propRes['PROPERTY_7'] = unserialize($propRes['PROPERTY_7']);
            $propRes['PROPERTY_19'] = unserialize($propRes['PROPERTY_19']);

            $library[ $lib["ID"] ]['PROPS'] = $propRes;
        }

        return $library;
    }

    private function _maxRecord($filterEx = false)
    {
        $filter = array(
            "=IBLOCK_ID" => self::IBLOCK_ID
        );


        if (is_array($filterEx) && count($filterEx) > 0)
            $filter = array_merge_recursive($filter, $filterEx);

        return \Bitrix\Iblock\ElementTable::getCount($filter);
    }

    /* Список услуг для селектора */
    private function _getAllListServices()
    {
        $class = $this->_entityListServices();

        /* Start cached */
        $cacheId = md5( $class . __METHOD__ );

        $cache = Bitrix\Main\Data\Cache::createInstance();

        if ($cache->initCache($this->arParams['CACHE_TIME'], $cacheId, "MySQL/LibraryServices/All/"))
        {
            $this->arResult['LIB_SERVICES_ALL'] = $cache->getVars();
        }
        elseif ($cache->startDataCache())
        {
            $filter = array();

            $db = $class::getList(array(
                "filter" => $filter,
                "order" => array(
                    "UF_NAME" => "asc"
                )
            ));

            if ($db->getSelectedRowsCount() == 0)
            {
                $cache->abortDataCache();
            }
            else
            {
                while ($res = $db->fetch())
                {
                    $this->arResult['LIB_SERVICES_ALL'][$res['ID']] = $res;
                }
            }
            $cache->endDataCache($this->arResult['LIB_SERVICES_ALL']);
        }
        /* End cached */
    }

    /* Список услуг для конкретной библиотеки */
    private function _getListServices($LIBRARY_ID)
    {
        if (!is_array($this->arResult['LIB_SERVICES_ALL']) || count($this->arResult['LIB_SERVICES_ALL']) < 1 )
            $this->_getAllListServices();

        $cntLibServiceList = count($this->arResult['LIB_SERVICES_ALL']);

        /* Start cached */
        $cacheId = md5(__METHOD__ . $LIBRARY_ID . $cntLibServiceList);

        $cache = Bitrix\Main\Data\Cache::createInstance();

        if ($cache->initCache($this->arParams['CACHE_TIME'], $cacheId, "MySQL/LibraryServices/fromLib/"))
        {
            $this->arResult['LIB_SERVICES'] = $cache->getVars();
        }
        elseif ($cache->startDataCache())
        {
            foreach ($this->arResult['LIB_SERVICES_ALL'] as $item)
            {
                    if ( !is_array($item['UF_LIBRARY_LIST']) || !in_array($LIBRARY_ID, $item['UF_LIBRARY_LIST']) ) continue;
                    $this->arResult['LIB_SERVICES'][] = $item;
            }

            $cache->endDataCache($this->arResult['LIB_SERVICES_ALL']);
        }
        /* End cached */
    }

    private function _entityListServices()
    {
        Loader::includeModule("highloadblock");

        global $APPLICATION;

        $db = HighloadBlockTable::getList(array(
            "select" => array(
                "TABLE_NAME", "ID"
            ),
            "filter" => array(
                "=NAME" => "LibraryServices"
            )
        ));

        if ($table = $db->fetch())
        {
            $hbID = $table['ID'];

            $hlblock = HighloadBlockTable::getById($hbID)->fetch();
            $entity = HighloadBlockTable::compileEntity( $hlblock );
            $libraryServiceClass = $entity->getDataClass(); // string "\LibraryServicesTable"

            return $libraryServiceClass;
        }
        else
        {
            $APPLICATION->ThrowException("Таблицы для <i>LibraryServices</i> не существует");
        }
    }
}