<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Localization\Loc as Loc;

Loc::loadMessages(__FILE__); 

	$arComponentParameters = array(
		'GROUPS' => array(
		),
		'PARAMETERS' => array(

			"PER_PAGE" => array(
				"PARENT" => "BASE",
				"NAME" => Loc::getMessage('ELR_LIB_LIST_PARAMETERS_PER_PAGE'),
				"TYPE" => "NUMBER",
				"DEFAULT" => 10,
			),
			'CACHE_TIME' => array(
				'DEFAULT' => 3600
			)
		)
	);
?>