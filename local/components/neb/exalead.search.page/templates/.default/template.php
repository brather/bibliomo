<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
	use \Bitrix\Main\Localization\Loc as Loc;
	Loc::loadMessages(__FILE__);
?>
<section class="innersection innerwrapper<?=empty($arResult['ITEMS'])? ' searchempty' : ''?> clearfix" id="search_page_block">
	<?
		if(empty($arResult['ITEMS']))
		{
		?>
		<div class="b-mainblock left">
			<?
				if(!empty($arParams['q']))
				{
				?>
				<div class="b-searchresult noborder">
					<span class="b-searchresult_label iblock"><?=Loc::getMessage('LIB_SEARCH_PAGE_TEMPLATE_FOUND_ZERO');?></span>
				</div><!-- /.b-searchresult-->
				<?
				}
			?>
			<div class="b-search_empty">
				<h2><?
					if(empty($arParams['q']))
						echo Loc::getMessage('LIB_SEARCH_PAGE_TEMPLATE_SEARCH_EMPTY');
					else
						echo Loc::getMessage('LIB_SEARCH_PAGE_TEMPLATE_NOTHING_FOUND');
				?></h2>
				<?
					if(!empty($arResult['REFINEMENT'])){
					?>
					<span><?=Loc::getMessage('LIB_SEARCH_PAGE_TEMPLATE_SUGGEST');?> <a href="<?=$APPLICATION->GetCurPageParam('q='.$arResult['REFINEMENT'], array('q','dop_filter'));?>"><?=$arResult['REFINEMENT']?></a></span>
					<?
					}
				?>
			</div>
		</div><!-- /.b-mainblock -->
		<?
		}

		if(!empty($arResult['ITEMS']))
		{
			#pre($arResult['GROUPS']['TYPEBOOK'],1);
		?>
		<div class="clearfix">
			<div class="b-mainblock left">
				<div class="b-searchresult">
					<span class="b-searchresult_label iblock">
						<?
							if($arResult['COUNT'] >= 300000)
							{
								echo Loc::getMessage('LIB_SEARCH_PAGE_FOUND_MORE_300000_RESULT');
							}
							else
							{
							?>
							<?=GetEnding($arResult['COUNT'], Loc::getMessage('LIB_SEARCH_PAGE_TEMPLATE_FOUND_5'), Loc::getMessage('LIB_SEARCH_PAGE_TEMPLATE_FOUND_1'), Loc::getMessage('LIB_SEARCH_PAGE_TEMPLATE_FOUND_2'))?>
							<?=number_format($arResult['COUNT'], 0, '', ' ')?>
							<?=GetEnding($arResult['COUNT'], Loc::getMessage('LIB_SEARCH_PAGE_TEMPLATE_DOCUMENT_5'), Loc::getMessage('LIB_SEARCH_PAGE_TEMPLATE_DOCUMENT_2'), Loc::getMessage('LIB_SEARCH_PAGE_TEMPLATE_DOCUMENT_1'))?>

							<?/*
								if(empty($_REQUEST['slparam']))
								{
								?>
								(<?=getmessage('LIB_SEARCH_PAGE_INCLUDING');?> <a href="<?=$APPLICATION->GetCurPageParam('slparam=sl_nofuzzy', array('slparam'))?>"><?=number_format($arResult['COUNT']-$arResult['NHITS'], 0, '', ' ')?></a> <?=getmessage('LIB_SEARCH_PAGE_DUPLICATED');?> )
								<?
								}
								*/
							}
						?>
					</span>

					<?
						# клиент попросил убрать этот блок, пока
						/*
						if(!empty($arResult['GROUPS']['TYPEBOOK']))
						{
						?>
						<ul class="b-searchresult_list iblock">
						<?
						$f_ft = empty($_REQUEST['f_ft']) ? '' : urldecode($_REQUEST['f_ft']);
						foreach($arResult['GROUPS']['TYPEBOOK'] as $type)
						{
						?>
						<li><a <?=$type['id'] ==  $f_ft? 'class="current"' : ''?> href="<?=$APPLICATION->GetCurPageParam('f_ft='.urlencode($type['id']), array('f_ft'))?>"><?=$type['count']?> <span class="under"><?=$type['title']?></span></a></li>
						<?
						}
						?>
						</ul>
						<?
						}
						*/
					?>
				</div><!-- /.b-searchresult-->
				<?
					if(!empty($arResult['RESULT_PARAMS']) or !empty($arParams['~q2']))
					{
					?>
					<div class="b-searchresult_info rel">
						<?
							if ($arParams['IS_SAVE_QUERY'] === true) {
							?>
							<div class="b-search_save">
								<form action="<?=$this->__folder.'/save_query.php'?>" class="searchsave_form" method="POST">
									<?=bitrix_sessid_post()?>
									<input type="hidden" name="url" value="<?=$APPLICATION->GetCurPageParam("", array("clear_cache", "debug")); ?>">
									<input type="hidden" name="q" value="<?=htmlspecialcharsbx($arParams['~q2'])?>">
									<input type="hidden" name="more_options" value="<?=!empty($arResult['RESULT_PARAMS']) ? htmlspecialcharsbx(serialize($arResult['RESULT_PARAMS'])) : ''?>">
									<input type="hidden" name="found" value="<?=$arResult['NHITS']?>">
									<input type="hidden" name="exalead_param" value="<?=htmlspecialcharsbx(serialize($arResult['EXALEAD_PARAMS']))?>">

									<span class="b-searchresult_save rel">
										<input type="submit" class="b-searchresult_bt" value="<?=Loc::getMessage('LIB_SEARCH_PAGE_TEMPLATE_SAVE_QUERY');?>">
									</span>
								</form>
								<div class="b-searchsave_popup">
									<p><?=Loc::getMessage('LIB_SEARCH_PAGE_TEMPLATE_QUERY_SAVED');?></p>
									<div class="b-search_tb">
										<input placeholder="<?=Loc::getMessage('LIB_SEARCH_PAGE_TEMPLATE_ADD_TITLE');?>" type="text" name="query_name" class="input" id="query_name">
									</div>

								</div>
							</div>
							<? } ?>

                        <div class="b-searchresult_line iblock"><?=$arResult["REQUEST"]["QUERY"]?></div>
                        <?
                        if(isset($arResult["REQUEST"]['ORIGINAL_QUERY'])):?>
                            <span class="b-portalinfo_num b-portalinfo_lb"><?echo GetMessage("CT_BSP_KEYBOARD_WARNING", array("#query#"=>$arResult["REQUEST"]['ORIGINAL_QUERY']))?></span>
                        <?php elseif($arResult["spellcheckSuggestion"]): ?>

                            <script type="text/javascript">
                                $(function() {
                                    var originalQuery = '<?php echo urlencode($arResult["REQUEST"]['QUERY']) ?>';
                                    var spellcheckSuggestion = '<?php echo urlencode($arResult["spellcheckSuggestion"]) ?>';
                                    url = window.location.search.replace(originalQuery, spellcheckSuggestion);
                                    $('#spellcheckSuggestion').attr('href', url);
                                });
                            </script>

                            <span class="b-portalinfo_num b-portalinfo_lb"><?echo GetMessage("LIB_SEARCH_PAGE_TEMPLATE_SUGGEST")?>
                                    <a href="#" id="spellcheckSuggestion"> <?php echo htmlspecialcharsEx($arResult["spellcheckSuggestion"]) ?> </a>

                            </span>
                        <?php endif; ?>

						<? /*?>
						<a href="#" onclick="$('.b-search_ex').addClass('open'); return false;" class="b-searchresult_more"><?=Loc::getMessage('LIB_SEARCH_PAGE_TEMPLATE_SPECIFY_SEARCH');?></a>
						<?*/
							if(!empty($arResult['RESULT_PARAMS'])){
							?>
							<div class="b-searchresult_portal">
								<?=Loc::getMessage('LIB_SEARCH_PAGE_TEMPLATE_SEARCH_ON_WEBSITE');?>:
								<?
									$i = 0;

									foreach($arResult['RESULT_PARAMS'] as $param)
									{
										$i++;
									?>
									<?=(/*($i == 1 and !empty($arParams['~q2'])) or*/ $i > 1) ? $param['logic'].' ' : ''?> <span class="b-searchresult_tag"><?=$param['text']?> <a href="<?=$param['url']?>" class="del"></a></span>
									<?

									}
								?>
							</div>
							<?
							}
						?>
					</div><!-- /.b-searchresult_info -->
					<?
					}
				?>
			</div>

			<a href="#" class="set_opener iblock right"><?=Loc::getMessage('LIB_SEARCH_PAGE_SETTINGS');?></a>
			<div class="b-side right">
					<?/*?><div class="b-searchparam">
					<div class="checkwrapper">
						<label for="cb12" class="black"><?=Loc::getMessage('LIB_SEARCH_PAGE_TEMPLATE_IN_WEBSITE');?></label><input class="checkbox right js_btnsub" type="checkbox" name="s_tp" value="portal" <?=$_REQUEST['s_tp'] == 'Y' || (empty($_REQUEST['s_tp'])  && $_REQUEST['s_tc'] != 'N')? 'checked="checked"':''?> id="cb12">
					</div>

						<div class="checkwrapper">
						<label for="cb2" class="black"><?=Loc::getMessage('LIB_SEARCH_PAGE_TEMPLATE_IN_LIBRARY');?></label><input onchange="chenge_st(this)" class="checkbox right" type="checkbox" name="s_tl" value="library" <?=$_REQUEST['s_tl'] == 'Y'? 'checked="checked"':''?> id="cb2">
						</div>
					<?
				</div><!-- /.b-searchparam -->
				*/?>
			</div>
		</div>
		<div class="b-mainblock left" id=111>

			<?
				$APPLICATION->IncludeComponent(
					"exalead:search.page.viewer.left",
					"",
					Array(
						"PARAMS" => $arParams,
						"RESULT" => $arResult,
					),
					$component
				);
			?>
		</div><!-- /.b-mainblock -->
		<?
		}
	?>


	<div class="b-side right">
		<?
			if(empty($arResult['ITEMS']))
			{
			?>
			<?/*?>
			<div class="b-side right">
				<div class="b-searchparam">
					<div class="checkwrapper">
						<label for="cb1" class="black"><?=Loc::getMessage('LIB_SEARCH_PAGE_TEMPLATE_IN_WEBSITE');?></label><input class="checkbox right js_btnsub" type="checkbox" name="s_tp" value="portal" <?=$_REQUEST['s_tp'] == 'Y' || (empty($_REQUEST['s_tp'])  && $_REQUEST['s_tc'] != 'N')? 'checked="checked"':''?> id="cb1">
					</div>
						<div class="checkwrapper">
						<label for="cb2" class="black"><?=Loc::getMessage('LIB_SEARCH_PAGE_TEMPLATE_IN_LIBRARY');?></label><input onchange="chenge_st(this)" class="checkbox right" type="checkbox" name="s_tl" value="library" <?=$_REQUEST['s_tl'] == 'Y'? 'checked="checked"':''?> id="cb2">
					</div><?
				</div><!-- /.b-searchparam -->
			</div>
			*/?>
			<?
			}


			$APPLICATION->IncludeComponent(
				"exalead:search.page.viewer.right",
				"",
				Array(
					"PARAMS" => $arParams,
					"RESULT" => $arResult,
				),
				$component
			);
		?>

	</div><!-- /.b-side -->
</section>
