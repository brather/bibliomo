<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
?>
<?if (!empty($arParams['q']))
{
	if($arResult['COUNT'] > 0)
	{
	?>
		<div class="b-digitizing_mass_action">
			<a href="#" class="">
				<span class="plus_ico"></span>
				<span class="wAdd">Добавить</span>
			</a>
			все книги <span class="wAdd2">в</span> план на оцифровку
		</div>

		<div class="b-add_digital js_digital">
			<table class="b-usertable tsize">
				<tbody>
					<tr>
						<th class="autor_cell"><a <?=SortingExalead("document_authorsort")?>>Автор</a></th>
						<th class="namedig_cell"><a <?=SortingExalead("document_titlesort")?>>Название / Описание</a></th>
						<th class="plan_cell">&nbsp;</th>
					</tr>

					<? 
					//echo $arParams['ID_LIBRARY'];
					//print_r($arParams['BOOK_ID']);
					//print_r($arParams);
					//print_r($arResult['ITEMS']);

					//print_r($arParams['BOOK_IN_PLAN']);
					$libname="";
						if(!empty($arResult['ITEMS']))
						{
							foreach($arResult['ITEMS'] as $arItem)
							{
							?>
							<tr class="search-result" id="<?=$arItem['~id']?>">
								<td class="pl15"><?=$arItem['authorbook']?> <? echo $arParams['BOOK_ID'][$arItem['~id']];?></td>
								<td class="pl15"><?=$arItem['title']?>
                                
                                <? 
								$serchkey = array_search(urldecode($arItem['~id']), $arParams['BOOK_IN_PLAN']);
								if(!empty($arParams['BOOK_IN_PLAN']) and $serchkey and $arParams['BOOK_ID'][$serchkey]!=$arParams['LIB_ID'])
								{
								  $calsname = "rel plusico_wrap plan-digitalization state";
								  $mes="Книга оцифровыется";
								  $libname=$arParams['LIBRARY_NAME'][$serchkey];
								}
								elseif(!empty($arParams['BOOK_IN_PLAN']) and $serchkey and $arParams['BOOK_ID'][$serchkey]==$arParams['LIB_ID'])
								{
								 $calsname = "rel plusico_wrap plan-digitalization minus";
								 $mes="Удалить из Плана оцифровки";
								}
								else
								{
								  $calsname = "rel plusico_wrap plan-digitalization";
								  $mes="Добавить в План оцифровки";
								}
								?>
									<div class="b-digital_act">
										<a href="#" class="b-digital_desc">Описание</a>
									
                                    <? if(!empty($libname)){?>
                                   
										<a href="#" class="b-digital_desc b-digital">На оцифровке</a>
									
                                    </div>
                                    <? }?>
								</td>
								<td>                           
                                
									<div class="<?=$calsname?>">  
										<div class="plus_ico plus_digital"></div>
										<div class="b-hint rel" data-state="Книга уже находится в процессе оцифровки" data-minus="Удалить из Плана оцифровки" data-plus="Добавить в План оцифровки"><?=$mes?></div>
									</div>
								</td>
							</tr>
							<tr class="scrolled">
								<td colspan="3"><? if(!empty($libname)){?>
                                <div class="b-infobox rel b-infoboxdescr" data-link="digital">
										<a href="#" class="close"></a>
										<span class="tit iblock">Оцифровывает</span><br><strong><?=$libname?></strong>
									</div><!-- /b-infobox --><? } $libname="";?>
									<div class="b-infobox rel b-infoboxdescr" data-link="descr">
										<a href="#" class="close"></a>
										<? if(!empty($arItem['authorbook'])){?><div class="b-infoboxitem"><span class="tit iblock">Автор: </span><span class="iblock val"><?=$arItem['authorbook']?></span></div><?}?>
										<?if(!empty($arItem['title'])){?><div class="b-infoboxitem"><span class="tit iblock">Заглавие: </span><span class="iblock val"><?=$arItem['title']?></span></div><?}?>
										<?if(!empty($arItem['publishyear'])){?><div class="b-infoboxitem"><span class="tit iblock">Дата пубдликации: </span><span class="iblock val"><?=$arItem['publishyear']?></span></div><?}?>
										<?if(!empty($arItem['library'])){?><div class="b-infoboxitem"><span class="tit iblock">Библиотека: </span><span class="iblock val"><?=$arItem['library']?></span></div><?}?>
										<?if(!empty($arItem['countpages'])){?><div class="b-infoboxitem"><span class="tit iblock">Страниц: </span><span class="iblock val"><?=$arItem['countpages']?></span></div><?}?>
									</div><!-- /b-infobox -->
								</td>
							</tr>
							<?
							}
						}
					?>
				</tbody>
			</table>
		</div>
        <script> $('.state').click(function() { return false; });</script>
		<?=$arResult['STR_NAV']?>
	<?
	}
	else
	{?>
		<div class="b-search_empty">
			<h2>К сожалению, ничего не найдено</h2>
			<span>Попробуйте изменить критерии поиска.</span>
		</div>
	<?}
}?>