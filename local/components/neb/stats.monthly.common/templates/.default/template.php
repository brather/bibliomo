<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

foreach ($arResult['errors'] as $error) {
    ShowError($error);
}
?>
<style>
    .admin-stats table {
        border-collapse: collapse;
        margin-bottom: 20px;
    }

    .admin-stats td, .admin-stats th {
        border: 1px solid;
        padding: 3px;
    }
</style>
<form method="post" action="<?=$arParams['form-hash']?>">
    <label>
        Год:
        <select name="year">
            <? foreach ($arResult['years'] as $year) { ?>
                <option value="<?= $year ?>" <?= $year == $arParams['year'] ? 'selected' : '' ?>>
                    <?= $year ?>
                </option>
            <? } ?>
        </select>
    </label>
    <label>
        Месяц:
        <select name="month">
            <? foreach ($arResult['months'] as $month) { ?>
                <option value="<?= $month ?>" <?= $month == $arParams['month'] ? 'selected' : '' ?>>
                    <?= $month ?>
                </option>
            <? } ?>
        </select>
    </label>
    <input type="submit" value="Применить"/>
</form>
<br>
<div class="admin-stats">
    <table>
        <tr>
            <th width="10%">Месяц</th>
            <th>Всего посещений</th>
            <th>Всего обращений к библиотекам МО</th>
            <th>Всего коллекций</th>

            <th>Обращений к коллекциям</th>
            <th>Всего обращений к электронным изданиям</th>
            <th>Всего заказанных книг в библиотеках МО</th>
            <th>Всего выданных книг в библиотеках МО</th>
            <th>Просмотров новостей библиотек МО</th>
        </tr>
        <? foreach ($arResult['items'] as $item) { ?>
            <tr>
                <td>
                    <?= $item['month']; ?>
                </td>
                <td>
                    <?= $item['count_visitors']; ?>
                </td>
                <td>
                    <?= $item['count_library_hit']; ?>
                </td>
                <td>
                    <?= $item['count_collections']; ?>
                </td>
                <td>
                    <?= $item['count_collection_hit']; ?>
                </td>
                <td>
                    <?= $item['count_viewer_hit']; ?>
                </td>
                <td>
                    <?= $item['count_ordered_books']; ?>
                </td>
                <td>
                    <?= $item['count_issued_books']; ?>
                </td>
                <td>
                    <?= $item['count_library_news_hit']; ?>
                </td>
            </tr>
        <? } ?>
    </table>
</div>