<?php

/**
 * Class StatsMonthlyCommon
 */
class StatsMonthlyCommon extends CBitrixComponent
{

    /**
     * @param $arParams
     *
     * @return array
     */
    public function onPrepareComponentParams($arParams)
    {
        $this->arResult['errors'] = [];

        $arParams = array_replace_recursive(
            $arParams,
            [
                'year'  => null,
                'month' => null,
            ]
        );
        if (isset($_REQUEST['year']) && $year = (integer)$_REQUEST['year']) {
            $arParams['year'] = $year;
        }
        if (isset($_REQUEST['month']) && $month = (integer)$_REQUEST['month']) {
            $arParams['month'] = $month;
        }
        if (!isset($arParams['year'])) {
            $arParams['year'] = (integer)date('Y');
        }
        if (!isset($arParams['month'])) {
            $arParams['month'] = (integer)date('m');
        }

        $where = '';
        $filter = [];
        if (isset($arParams['year'])) {
            $year = $arParams['year'];
            if (isset($arParams['month'])) {
                $month = $arParams['month'];
                $dateFrom = $year . '-' . str_pad((string)$month, 2, '0', STR_PAD_LEFT) . '-01';
                $filter['date'] = ConvertTimeStamp(strtotime($dateFrom));
                if ($month >= 12) {
                    $month = 1;
                    $year++;
                }
                $dateTo = $year . '-' . str_pad((string)($month + 1), 2, '0', STR_PAD_LEFT) . '-01';
            } else {
                $dateFrom = $year . '-01-01';
                $dateTo = ($year + 1) . '-01-01';
                $filter['>=date'] = ConvertTimeStamp(strtotime($dateFrom));
                $filter['<=date'] = ConvertTimeStamp(strtotime($dateTo));
            }
            $where = "WHERE timestamp BETWEEN '$dateFrom' AND '$dateTo'";
            $arParams['dateFrom'] = $dateFrom;
            $arParams['dateTo'] = $dateTo;
        }
        $arParams['listFilter'] = $filter;
        $arParams['eventWhere'] = $where;

        return $arParams;
    }

    /**
     *
     */
    public function executeComponent()
    {

        try {
            $this->_loadCommonMonthlyStats()
                ->_loadEventStats()
                ->_loadVisitorsCount()
                ->_loadCollectionsCount();

            $startYear = 2015;
            $this->arResult['years'] = array_keys(array_fill($startYear, (date('Y') - ($startYear - 1)), null));
            $this->arResult['months'] = array_keys(array_fill(1, 12, null));

        } catch (Exception $e) {
            $this->arResult['errors'][] = $e->getMessage();
        }
        $this->includeComponentTemplate();

    }

    /**
     * @return $this
     * @throws Exception
     * @throws \Bitrix\Main\ArgumentException
     */
    private function _loadCommonMonthlyStats()
    {
        if (!CModule::IncludeModule('evk.books')) {
            throw new Exception('Module evk.books not installed');
        }

        $parameters = [
            'order' => [
                'date' => 'desc',
            ]
        ];
        if (!empty($this->arParams['listFilter'])) {
            $parameters['filter'] = $this->arParams['listFilter'];
        }

        $dbItems = \Evk\Books\Tables\MoCommonMonthlyStatisticsTable::getList($parameters);
        $this->arResult['items'] = [];
        while ($item = $dbItems->fetch()) {
            $month = strtotime($item['date']);
            $month = date('Y-m', $month);
            $item['month'] = $month;
            $this->arResult['items'][$month] = $item;
        }

        return $this;
    }

    /**
     * @return $this
     */
    private function _loadEventStats()
    {
        $query
            = "
SELECT
  COUNT(*)                        cnt,
  event,
  DATE_FORMAT(timestamp, '%%Y-%%m') month
FROM mo_stats_event
%s
GROUP BY month, event;";
        $query = sprintf($query, $this->arParams['eventWhere']);
        $eventStatsCountMap = [
            'collection_page'   => 'count_collection_hit',
            'library_page'      => 'count_library_hit',
            'viewer_page'       => 'count_viewer_hit',
            'library_news_page' => 'count_library_news_hit',
        ];
        $dbItems = \Evk\Books\Tables\MoStatsEventTable::getEntity()->getConnection()->query($query);
        while ($item = $dbItems->fetch()) {
            if (isset($this->arResult['items'][$item['month']]) && isset($eventStatsCountMap[$item['event']])) {
                $this->arResult['items'][$item['month']][$eventStatsCountMap[$item['event']]] += $item['cnt'];
            }
        }

        return $this;
    }

    /**
     * @return $this
     */
    private function _loadVisitorsCount()
    {
        $query
            = "
SELECT
  SUM(GUESTS)                     guests,
  DATE_FORMAT(DATE_STAT, '%%Y-%%m') month
FROM b_stat_day
  %s
GROUP BY month;";
        $where = '';
        if (isset($this->arParams['dateFrom']) && isset($this->arParams['dateTo'])) {
            $where
                = "WHERE DATE_STAT >= '{$this->arParams['dateFrom']}' AND DATE_STAT <= '{$this->arParams['dateTo']}'";
        }
        $query = sprintf($query, $where);
        /** @var \Bitrix\Main\DB\Result $dbItems */
        $dbItems = Bitrix\Main\Application::getInstance()->getConnectionPool()->getConnection()->query($query);
        while ($item = $dbItems->fetch()) {
            if (isset($this->arResult['items'][$item['month']])) {
                $this->arResult['items'][$item['month']]['count_visitors'] += $item['guests'];
            }
        }

        return $this;
    }


    /**
     * @return $this
     */
    private function _loadCollectionsCount()
    {
        $query
            = "
SELECT
  COUNT(*)                          cnt,
  DATE_FORMAT(DATE_CREATE, '%Y-%m') month
FROM b_iblock_section
  WHERE IBLOCK_ID = 6
GROUP BY month
ORDER BY month;
";
        /** @var \Bitrix\Main\DB\Result $dbItems */
        $dbItems = Bitrix\Main\Application::getInstance()->getConnectionPool()->getConnection()->query($query);
        $months = array_keys($this->arResult['items']);
        $months = array_map(
            function ($value) {
                return (integer)str_replace('-', '', $value);
            }, $months
        );
        asort($months);
        $firstMonth = current($months);
        $total = 0;
        $collections = [];

        while ($item = $dbItems->fetch()) {
            $month = (integer)str_replace('-', '', $item['month']);
            if ($month <= $firstMonth) {
                $total += (integer)$item['cnt'];
            } else {
                $collections[$item['month']] = $item['cnt'];
            }
        }
        $months = array_keys($this->arResult['items']);
        asort($months);
        foreach ($months as $month) {
            if (isset($collections[$month])) {
                $total += (integer)$collections[$month];
            }
            $this->arResult['items'][$month]['count_collections'] += $total;
        }

        return $this;
    }
}