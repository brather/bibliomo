<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<div class="b-usercheck">
	<h2 class="mode">Проверка ЕЭЧБ</h2>
	<form action="<?=$APPLICATION->GetCurPage();?>" class="b-form b-form_common b-checkuserform" method="post">
		<input type="hidden" name="sessid" value="<?=bitrix_sessid();?>"/>
		<div class="b-form_header"><span class="iblock">Предоставленные данные</span></div>

		<div class="fieldrow nowrap mt35">
			<div class="fieldcell iblock">
				<label for="settings04">ФИО</label>
				<div class="field validate">
					<input type="text" class="input" name="name" data-minlength="5" data-maxlength="30" data-validate="fio" data-required="required" id="settings04" value="<?=$arParams['name']?>">
					<em class="error required">Поле обязательно для заполнения</em>
					<em class="error validate">Поле заполнено неверно</em>
				</div>
			</div>
		</div>

		<div class="fieldrow nowrap">
			<div class="fieldcell iblock mt10">
				<label for="settings01">Номер Единого Читательского билета</label>
				<div class="field validate">
					<input type="text" class="input" name="eechb" data-minlength="8" data-maxlength="30" data-validate="number" data-required="required" id="settings01" value="<?=$arParams['eechb']?>"> <em class="error">Поле обязательно для заполнения</em>
                    <em class="error required">Поле обязательно для заполнения</em>
                    <em class="error validate">Поле заполнено неверно</em>
				</div>
			</div>
		</div>
		<div class="fieldrow nowrap fieldrowaction">
			<div class="fieldcell w700  mt25">
				<div class="field clearfix">
					<button class="formbutton" value="1" type="submit" >Проверить</button>
				</div>
			</div>
		</div>
	</form>

	<?
		if($_SERVER['REQUEST_METHOD'] == 'POST'){
			if(empty($arResult['ITEMS'])){
			?>
			<div class="b-checkuserfail">
				<h3>В базе данных ЕИСУБ не найдено соответствий.</h3>
				<p>Проверьте правильность введенных даннных или <a href="/profile/readers/new/">заведите нового читателя</a>.</p>
			</div>
			<?
			}
			else
			{
			?>
			<table class="b-usertable tsize">
				<tr>
					<th class="num_cell"><a href="#" onclick="return false;" class="sort ">№</a></th>
					<th class="name_cell"><a href="#" onclick="return false;" class="sort ">Фамилия <br />Имя Отчество</a></th>
					<th class="ecbnum_cell"><a href="#" onclick="return false;" class="sort ">Номер ЕЧБ</a></th>
					<th class="date_cell"><a href="#" onclick="return false;" class="sort ">Дата <br>регистрации </a>	</th>
					<th class="status_cell"><a href="#" onclick="return false;" class="sort ">Статус</a></th>
				</tr>
				<?
					$i=0;
					foreach($arResult['ITEMS'] as $arItem)
					{
						$i++;
					?>
					<tr>
						<td><?=$i?></td>
						<td><?=$arItem['LAST_NAME']?> <?=$arItem['NAME']?> <?=$arItem['SECOND_NAME']?></td>
						<td><?=$arItem['UF_NUM_ECHB']?></td>
						<td><span class="date"><?=ConvertDateTime($arItem['DATE_REGISTER'], "DD.MM.YYY"); ?></span></td>
						<td><span class="status"><?=!empty($arItem['UF_STATUS']) ? 'Верифицированный' : ''?></span></td>
					</tr>
					<?
					}
				?>

			</table>
			<?
			}
		}
	?>
</div>