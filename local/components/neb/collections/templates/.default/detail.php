<?
	if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

	$APPLICATION->IncludeComponent(
		"neb:collections.detail",
		"",
		Array(
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"SECTION_ID" => $arResult["VARIABLES"]["ID"],
			"SECTION_CODE" => $arResult["VARIABLES"]["CODE"],
			"CACHE_TIME" => $arParams["CACHE_TIME"],
			"LIST_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]['list'],
		),
		$component
	);

?>