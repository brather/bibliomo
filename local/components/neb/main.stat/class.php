<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}
CModule::IncludeModule("evk.books");
class NebMainStatComponent extends CBitrixComponent
{
	
	public function getResult()
	{
		global $APPLICATION;
		$arCounts = \Evk\Books\Books::GetAllCount();
		$arCounts["BOOKS"] = $arCounts["BOOKS_WITHOUT_FILES"] + $arCounts["BOOKS_WITH_FILES"];
		if(!empty($arCounts["BOOKS_WITH_READY_FILES"]))
			$arCounts["BOOKS_WITH_FILES"] = intval($arCounts["BOOKS_WITH_READY_FILES"]);
		$arCounts["USERS"] = $this->GetUserCount();
		return $arCounts;
	}
	public function GetUserCount()
	{
		$userCount = 0;
		$obCache = new \CPHPCache();
		$cache_time = 36000000;
		$cache_dir = sprintf("/%s/%s/", SITE_ID, "users_get_count");
		$cache_id = md5($cache_dir);
		if($obCache->InitCache($cache_time, $cache_id, $cache_dir))
		{
			extract($obCache->GetVars());
		}
		else
		{
			if($obCache->StartDataCache($cache_time, $cache_id, $cache_dir))
			{
				$userCount = CUser::GetCount();
				if($userCount > 0)
				{
					/**
					 * @var \CCacheManager $CACHE_MANAGER
					 */
					global $CACHE_MANAGER;
					$CACHE_MANAGER->StartTagCache($cache_dir);
					$CACHE_MANAGER->RegisterTag($cache_id);
					$CACHE_MANAGER->EndTagCache();
					$obCache->EndDataCache(compact("userCount"));
				}
				else
					$obCache->AbortDataCache();
			}
		}
		return $userCount;
	}

	public function executeComponent()
	{

		$this->arResult = $this->getResult();
		if(!empty($this->arParams["BOOKS_WITHOUT_FILES"]))
			$this->arResult["BOOKS_WITHOUT_FILES"] = $this->arParams["BOOKS_WITHOUT_FILES"];
		if(!empty($this->arParams["BOOKS_WITH_FILES"]))
			$this->arResult["BOOKS_WITH_FILES"] = $this->arParams["BOOKS_WITH_FILES"];
		if(!empty($this->arParams["USERS"]))
			$this->arResult["USERS"] = $this->arParams["USERS"];
		
		$this->arResult["BOOKS"]= number_format($this->arResult["BOOKS"], 0, '.', ' ');
		$this->arResult["USERS"]= number_format($this->arResult["USERS"], 0, '.', ' ');
		$this->arResult["BOOKS_WITH_FILES"]= number_format($this->arResult["BOOKS_WITH_FILES"], 0, '.', ' ');
		$this->arResult["BOOKS_WITHOUT_FILES"]= number_format($this->arResult["BOOKS_WITHOUT_FILES"], 0, '.', ' ');
		
		$this->includeComponentTemplate();
	}
}

?>