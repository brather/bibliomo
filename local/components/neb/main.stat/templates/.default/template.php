<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
<div class="b-portalinfo">
	<h3>всего на портале</h3>
	<ul class="b-portal-minfo">
		<li>
			<div class="b-portal-count bnotes"><?=$arResult["BOOKS_WITHOUT_FILES"]?></div>
			<p>библиографических записей</p>
		</li>
		<li>
			<div class="b-portal-count ebooks"><?=$arResult["BOOKS_WITH_FILES"]?></div>
			<p>электронных копий изданий</p>
		</li>
		<li>
			<div class="b-portal-count users"><?=$arResult["USERS"]?></div>
			<p>зарегистрированных пользователей</p>
		</li>
	</ul>
</div><!-- /.b-portalinfo -->