<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main;
use \Bitrix\Main\Localization\Loc as Loc;

Loc::loadMessages(__FILE__); 

$arComponentParameters = array(
	'GROUPS' => array(
	),
	'PARAMETERS' => array(

		"BOOKS_WITHOUT_FILES" => array(

			"PARENT" => "BASE",
			"NAME" => "Кол-во записей без электронных изданий",
			"TYPE" => "NUMBER",
			"DEFAULT" => '',
		),

		"BOOKS_WITH_FILES" => array(

			"PARENT" => "BASE",
			"NAME" => "Кол-во электронных изданий",
			"TYPE" => "STRING",
			"DEFAULT" => '',
		),

		"USERS" => array(

			"PARENT" => "BASE",
			"NAME" => "Кол-во зарегистрированных пользователей",
			"TYPE" => "STRING",
			"DEFAULT" => '',
		),

		'CACHE_TIME' => array(
			'DEFAULT' => 3600
		)
	)
);
?>