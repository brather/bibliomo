<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}

if (!CModule::IncludeModule('evk.books') || !CModule::IncludeModule('iblock')) return false;
use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;
use Evk\Books\Stat\BookRead;
use Evk\Books\SearchQuery;
//use Nota\Exalead\SearchQuery;
//use Nota\Exalead\SearchClient;

Loc::loadMessages(__FILE__);

class NebPopularListCBitrixComponent extends CBitrixComponent
{
	public function onPrepareComponentParams($arParams)
	{
		$this->arParams['COUNT'] = intval($arParams['COUNT']);
		return $arParams;
	}
	
	public function getResult()
	{
		$arReadBooks = BookRead::GetList(array(
			"select" => array("CNT", "BOOK_ID"),
			"group" => array("BOOK_ID"),
			"order" => array("CNT"=>"DESC"),
			"runtime" => array(
				new Entity\ExpressionField("CNT", "COUNT(BOOK_ID)")
			),
			"limit" => $this->arParams["COUNT"],
		));
		if(is_array($arReadBooks) && count($arReadBooks))
		{
			$arCode = array();
			foreach($arReadBooks as $arRB)
			{
				$arCode[] = $arRB["BOOK_ID"];
			}
			$query = new SearchQuery();
			$query->setPageLimit($this->arParams['COUNT']);
			$result = $query->FindBooksSimple(array(),
				array(
					"=ID" => $arCode
			), false, false,
				array(
					"NAME",
					"PROPERTY_AUTHOR",
					"PROPERTY_LIBRARIES",
				)
			);
			if(is_array($result) && count($result))
			{
				foreach($result as $arElement)
				{
					$arElement = array_merge($arElement, array(
						'TITLE' => TruncateText($arElement['NAME'], 45),
						'AUTHOR' => $arElement['authorbook'],
						'AUTH_LINK' => $arElement['r_authorbook'],
						'URL' => $arElement['DETAIL_PAGE_URL'],
						'LINK' => $arElement['DETAIL_PAGE_URL'],
					));
					$this->arResult['ITEMS'][] = $arElement;
				}
			}
		}
	}

	public function executeComponent()
	{
		if ($this->StartResultCache(false, array()))
		{
			if (!CModule::includeModule('evk.books')) {
				
				$this->AbortResultCache();
				throw new Exception(Loc::getMessage('BOOK_CATALOG_CATALOG_PROMO_CLASS_ERROR'));
			}
			$this->getResult();
		$this->includeComponentTemplate();
		}
	}
}

?>