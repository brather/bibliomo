<?
	if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
		die();
	use \Bitrix\Main\Localization\Loc as Loc;
	Loc::loadMessages(__FILE__);
	if(empty($arResult['ITEMS']))
		return false;
?>

<?if(!$arResult['AJAX']){?>
	<div class="b-bookinfo_new iblock">
		<div class="b-bookinfo_tit">
			<h2><?=Loc::getMessage('NEW_LIST_TEMPLATE_NEW'); ?></h2> <a href="/search/?date1=<?=urlencode($arResult['DATE_1'])?>" class="b-searchpage_lnk"><?=$arResult['MONTH']?>: <?=Loc::getMessage('NEW_LIST_TEMPLATE_ADDED'); ?> <strong><?=$arResult['COUNT']?></strong></a>
		</div>
		<?}?>
	<ul class="b-bookboard_list">
		<?
			foreach($arResult['ITEMS'] as $arElement)
			{
			?>
			<li class="b-result-docitem" id="<?=urlencode($arElement['ID'])?>">
				<div class="b-bookhover bbox">

					<?
						if(collectionUser::isAdd())
						{
							if(!empty($arResult['USER_BOOKS'][urlencode($arElement['ID'])]))
							{
							?>
							<div class="meta minus">
								<div class="b-hint rel"><?=Loc::getMessage('REMOVE_MY_LIB')?></div>
								<a href="#" data-normitem="Y" data-url="<?=ADD_COLLECTION_URL?>removeBook.php?id=<?=urlencode($arElement['ID'])?>" data-collection="<?=ADD_COLLECTION_URL?>list.php?t=books&id=<?=urlencode($arElement['ID'])?>" class="b-bookadd fav"></a>
							</div>
							<?
							}
							else
							{
							?>
							<div class="meta">
								<div class="b-hint rel"><?=Loc::getMessage('ADD_MY_LIB')?></div>
								<a href="#" data-collection="<?=ADD_COLLECTION_URL?>list.php?t=books&id=<?=urlencode($arElement['ID'])?>" class="b-bookadd"></a>
							</div>
							<?
							}
						}
					?>					
					<div class="b-bookhover_tit black"><a href="<?=$arElement['LINK']?>" class="popup_opener ajax_opener coverlay" data-width="955" ><?=$arElement['TITLE']?></a></div>
					<span class="b-autor"><a class="lite" href="<?=$arElement['AUTHOR_LINK']?>"><?=$arElement['AUTHOR']?></a></span>
				</div>
				<a href="<?=$arElement['LINK']?>" class="b-bookboardphoto iblock popup_opener ajax_opener coverlay" data-width="955"><img src="<?=$arElement['IMAGE_URL']?>&width=83&height=133" data-autor="<?=$arElement['AUTHOR']?>" data-title="<?=$arElement['TITLE']?>"  class="loadingimg" ></a>
			</li>
			<?
			}
		?>
	</ul>
	<a class="button_mode js_ajax_morebook" href="<?=$this->__folder?>/ajax.php?cnt=<?=$arParams['COUNT']?>&date=<?=$arParams['DATE']?>&next_book=<?=$arParams['NEXT_BOOK']?>&page_url=<?=urlencode($arParams['PAGE_URL'])?>"><?=Loc::getMessage('NEW_LIST_TEMPLATE_NEXT'); ?></a>
	<?
		if(!$arResult['AJAX'])
		{
		?>
	</div> <!-- /.b-bookinfo_new -->
	<?
	}
?>
