<?
	if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
		die();
	use \Bitrix\Main\Localization\Loc as Loc;
	Loc::loadMessages(__FILE__);
	if(empty($arResult['ITEMS']))
		return false;
?>

<? if(!$arResult['AJAX']) : ?>
	<div class="b-bookinfo_new iblock">
		<div class="b-bookinfo_tit">
			<h2><?=Loc::getMessage('NEW_LIST_TEMPLATE_NEW'); ?></h2> <span><?=$arResult['MONTH']?>: <?=Loc::getMessage('NEW_LIST_TEMPLATE_ADDED'); ?> <strong><?=$arResult['COUNT']?></strong></span>
		</div>
<? endif; ?>
		<ul class="b-bookboard_list">
			<? foreach($arResult['ITEMS'] as $arElement) : ?>
				<li id="<?=urlencode($arElement['ID'])?>">
					<div class="b_popular_img">
						<img src="<?=$arElement['IMAGE_URL']?>"/>
					</div>
					<div class="b_popular_descr">
						<h4 class="iblock"><a href="<?=$arElement['LINK']?>" class="popup_opener ajax_opener coverlay" data-width="955" ><?=$arElement['NAME']?></a></h4>
							<? if(!empty($arElement['PROPERTY_AUTHOR_VALUE'])) : ?>
								<span class="b-autor iblock"><?=Loc::getMessage('NEW_LIST_TEMPLATE_AUTHOR'); ?>: <?=$arElement['PROPERTY_AUTHOR_VALUE']?></span>
							<? endif; ?>
							<? if(!empty($arElement['PROPERTY_PUBLISH_YEAR_VALUE'])) : ?>
								<span class="b-autor iblock">Год публикации: <?=$arElement['PROPERTY_PUBLISH_YEAR_VALUE']?></span>
							<? endif; ?>
							<? if(!empty($arElement['LIBS'])) : ?>
								<div class="b-result_sorce_info iblock">
									<em><?=Loc::getMessage('NEW_LIST_TEMPLATE_SOURCE'); ?>: </em>
									<? foreach($arElement['LIBS'] as $library) :?>
										<a href="<?=$library['LINK']?>" class="b-sorcelibrary"><?=$library['NAME']?></a>
									<? endforeach; ?>
								</div>
							<? endif; ?>
					</div>
				</li>
			<? endforeach; ?>
		</ul>
		<a class="button_mode js_ajax_morebook" id="js_ajax_morebook" href="<?=$this->__folder?>/ajax.php?cnt=<?=$arParams['COUNT']?>&date=<?=$arParams['DATE']?>&next_page=<?=$arResult['NUM_PAGE']?>"><?=Loc::getMessage('NEW_LIST_TEMPLATE_NEXT'); ?></a>
<? if(!$arResult['AJAX']) : ?>
	</div> <!-- /.b-bookinfo_new -->
<? endif; ?>
