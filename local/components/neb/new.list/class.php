<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}
if (!CModule::IncludeModule('evk.books')) die();

use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

use Evk\Books\SearchQuery;

class NewList extends CBitrixComponent
{

	public function onPrepareComponentParams($arParams)
	{
		$this->arParams['DATE'] = intval($arParams['DATE']);
		$this->arParams['COUNT'] = intval($arParams['COUNT']);
		$this->arParams['NEXT_BOOK'] = intval($arParams['NEXT_BOOK']);
		$this->arParams['LIMIT'] = intval($arParams['LIMIT']);
		return $arParams;
	}


	public function getResult()
	{
		if($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')
			$this->arResult['AJAX'] = true;
		$obSearch = new SearchQuery($this->arResult, $this->arParams);
		$arSearch = $obSearch->FindBooksSimple(array('CREATED' => 'DESC'),
			array(
				"!PROPERTY_FILE"=> false,
				"PROPERTY_STATUS" => 2,
				"!PROPERTY_COUNT_PAGES"=>false,
			), false, array("nPageSize" => 8),
			array(
				"PROPERTY_AUTHOR",
				"PROPERTY_LIBRARIES",
				"PROPERTY_FILE",
				"PROPERTY_STATUS",
				"NAME",
				"PROPERTY_PUBLISH_YEAR",
			)
		);
		$this->arResult["ITEMS"] = $arSearch;
	}

	public function executeComponent()
	{
		CModule::IncludeModule('iblock');
		$this->getResult();
		$this->includeComponentTemplate();
	}
}

?>
