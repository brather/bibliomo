<?
	if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

	if(!CModule::IncludeModule("iblock"))
		return;

	$arIBlockType = CIBlockParameters::GetIBlockTypes();

	$arIBlock=array();
	$rsIBlock = CIBlock::GetList(Array("sort" => "asc"), Array("TYPE" => $arCurrentValues["IBLOCK_TYPE"], "ACTIVE"=>"Y"));
	while($arr=$rsIBlock->Fetch())
	{
		$arIBlock[$arr["ID"]] = "[".$arr["ID"]."] ".$arr["NAME"];
	}


	$arComponentParameters = array(
		"GROUPS" => array(
		),
		"PARAMETERS" => array(
			"IBLOCK_TYPE" => array(
				"PARENT" => "BASE",
				"NAME" => 'IBLOCK_TYPE',
				"TYPE" => "LIST",
				"VALUES" => $arIBlockType,
				"REFRESH" => "Y",
			),
			"IBLOCK_ID" => array(
				"PARENT" => "BASE",
				"NAME" => 'IBLOCK_ID',
				"TYPE" => "LIST",
				"VALUES" => $arIBlock,
				"REFRESH" => "Y",
				"ADDITIONAL_VALUES" => "Y",
			),
			"CACHE_TIME"  =>  Array("DEFAULT"=>3600),
			"DETAIL_URL" => CIBlockParameters::GetPathTemplateParam(
				"DETAIL",
				"DETAIL_URL",
				'Шаблон пути детальной страницы',
				"",
				"URL_TEMPLATES"
			),
		),
	);
?>