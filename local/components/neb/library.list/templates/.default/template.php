<?
	if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
	use \Bitrix\Main\Localization\Loc as Loc;
	Loc::loadMessages(__FILE__);
?>
<div class="b-map rel mappage">
	<form method="get" action="." class="b-map_search_filtr">
		<div class="b-map_search">
			<div class="wrapper">
				<div class="b-cytyinfo">
					<span class="b-map_search_txt"><?=Loc::getMessage('LIBRARY_LIST_FIND_CLOSEST');?></span>
					<a href="#" class="b-citycheck setLinkPoint" data-lat="<?=$arResult["CITY"]["UF_POS"][1]?>" data-lng="<?=$arResult["CITY"]["UF_POS"][0]?>"><?=$arResult["CITY"]["UF_CITY_NAME"]?></a>
					<a href="#" class="b-changecity js_changecity"><?=Loc::getMessage('LIBRARY_LIST_CHANGE_CITY');?></a>
				</div>
				<input type="text" class="b-map_tb ui-autocomplete-input searchonmap" name="q" id="qcity" autocomplete="off" placeholder="<?=Loc::getMessage('LIBRARY_LIST_ENTER_ADDRESS');?>">	  <input type="submit" class="button_b" value="<?=Loc::getMessage('LIBRARY_LIST_FIND_BUTTON');?>" name="submit">
			</div>
		</div><!-- /.b-map_search -->
	</form>

	<div class="ymap" id="ymap" data-path="<?=$APPLICATION->GetCurPage();?>?json=Y" data-lat="<?=$arResult["CITY"]["UF_POS"][1]?>" data-lng="<?=$arResult["CITY"]["UF_POS"][0]?>" data-zoom="10"></div>
	<span class="marker_lib hidden">
		<a href="#" class="b-elar_name_txt"></a><br />
		<span class="b-elar_status"></span><br />
		<span class="b-map_elar_info">
			<span class="b-map_elar_infoitem addr"><span><?=Loc::getMessage('LIBRARY_LIST_POPUP_ADDRESS');?>:</span></span><br />
			<span class="b-map_elar_infoitem graf"><span><?=Loc::getMessage('LIBRARY_LIST_POPUP_WORK');?>:</span></span>
		</span>
		<span class="b-mapcard_act clearfix">
			<span class="right neb b-mapcard_status"><?=Loc::getMessage('LIBRARY_LIST_POPUP_USER');?></span>
			<a href="#" class="button_mode"><?=Loc::getMessage('LIBRARY_LIST_POPUP_URL');?></a>
		</span>
	</span>
</div><!-- /.b-map -->

<section class="rel innerwrapper clearfix">
	<div class="b-elar_usrs">
		<h2><?=Loc::getMessage('LIBRARY_LIST_LIBS');?></h2>
		<ol class="b-elar_usrs_list">
			<?
				if(isset($arResult['ITEMS_PAGEN'])){
					$i = $arResult["NAV_OFFSET"];
					foreach($arResult['ITEMS_PAGEN'] as $arItem)
					{
						$i++;
					?>
					<li>
						<span class="num"><?=$i?>.</span>
						<div class="b-elar_name iblock">
							<?
								if(!empty($arItem['DETAIL_PAGE_URL']))
								{
								?>
								<a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="b-elar_name_txt"><?=$arItem['UF_NAME']?></a>
								<?
								}
								else
								{
								?>
								<span class="b-elar_name_txt"><?=$arItem['UF_NAME']?></span>
								<?
								}
							?>

							<?/*if(isset($arItem['IS_NEB'])):?>
								<div class="b-elar_status neb">
									<div class="icon"><div class="b-hint"><?=Loc::getMessage('LIBRARY_LIST_MEMBER_NEL');?></div></div><?=$arItem['PROPERTY_STATUS_VALUE']?>
									
								</div>
							<?endif;*/?>
						</div>
						<div class="b-elar_address iblock">
							<div class="b-elar_address_txt">
								<div class="icon"><div class="b-hint"><?=Loc::getMessage('LIBRARY_LIST_ADDRESS');?></div></div><?=$arItem['UF_ADRESS']?>
								
							</div>
							<a href="#" class="b-elar_address_link" data-lat="<?=$arItem["UF_POS"][1]?>" data-lng="<?=$arItem["UF_POS"][0]?>"><?=Loc::getMessage('LIBRARY_LIST_SHOW_ON_MAP');?></a>
						</div>
					</li>
					<?
					}
				}
			?>
		</ol>
		<?=$arResult["NAVIGATION"]?>
	</div><!-- /.b-elar_usrs -->
</section>
<script src="http://api-maps.yandex.ru/2.0-stable/?load=package.full&lang=ru-RU" type="text/javascript"></script>

<?$this->SetViewTarget("library_city_select");?>
<?if(!$arResult["IS_COOKIE_SET"]):?>
	<div class="b-citypopup">
		<div class="wrapper">
			<a href="#" class="close"></a>
			<span class="b-citylabel"><?=Loc::getMessage('LIBRARY_LIST_YOUR_CITY');?>
				<a href="#" class="b-citycheck setLinkPoint" data-lat="<?=$arResult["CITY"]["UF_POS"][1]?>" data-lng="<?=$arResult["CITY"]["UF_POS"][0]?>"><?=$arResult["CITY"]["UF_CITY_NAME"]?></a> ? </span>
			<span class="b-citycheckbt">
				<a href="#" class="button_b setLinkPoint setPointAndReload" data-city-id="<?=$arResult["CITY"]["ID"]?>" data-lat="<?=$arResult["CITY"]["UF_POS"][1]?>" data-lng="<?=$arResult["CITY"]["UF_POS"][0]?>"><?=Loc::getMessage('LIBRARY_LIST_YES');?></a>
				<a href="#" class="button_gray js_changecity"><?=Loc::getMessage('LIBRARY_LIST_EDIT');?></a>
			</span>
		</div>
	</div>
	<?endif;?>
<div class="b-citychangepopup" data-width="498">
	<a href="#" class="closepopup"></a>
	<form class="b-map_search_form " action="." method="get">
		<p><?=Loc::getMessage('LIBRARY_LIST_ENTER_CITY');?></p>
		<input type="text" name="" id="" data-path="<?=$APPLICATION->GetCurPage();?>?json=Y&type=city" class="input ui-autocomplete-input" autocomplete="off" placeholder="">
		<div class="b-choosecity"><div class="iblock b-citylist">
				<p><?=Loc::getMessage('LIBRARY_LIST_CHOOSE');?></p>
				<div class="b-handcity">
					<?foreach($arResult["CITY_LIST"] as $areaName => $cityList):?>
						<div class="b-handcityitem">
							<div class="b-handcityregion"><?=$areaName?></div>
							<ul class="b-cityquisklist">
								<?foreach($cityList as $k => $city):?>
									<li><a class="b-city" href="#" data-city-id="<?=$city["ID"]?>"><?=$city["UF_CITY_NAME"]?></a></li>
									<?endforeach;?>
							</ul>
						</div>
						<?endforeach;?>

				</div>
			</div><div class="iblock b-cityquisk">
				<ul class="b-cityquisklist">
					<?foreach($arResult["CITY_FAVORITES"] as $k => $city):?>
						<li><a class="b-city" href="#" data-city-id="<?=$city["ID"]?>"><?=$city["UF_CITY_NAME"]?></a></li>
						<?endforeach;?>
				</ul>
			</div><div class="b-citycation">
				<input type="submit" name="submit" value="<?=Loc::getMessage('LIBRARY_LIST_ACCEPT');?>" class="formbutton">
			</div></div>
	</form>
</div>
<?$this->EndViewTarget();?>
