<?php
$MESS['LIBRARY_LIST_FIND_CLOSEST'] = 'FIND THE NEAREST LIBRARY';
$MESS['LIBRARY_LIST_FIND_BUTTON'] = 'Find';
$MESS['LIBRARY_LIST_ENTER_ADDRESS'] = 'Enter address';
$MESS['LIBRARY_LIST_CHANGE_CITY'] = 'изменить город';
$MESS['LIBRARY_LIST_LIBS'] = 'Библиотеки';
$MESS['LIBRARY_LIST_SHOW_ON_MAP'] = 'Show on the map';
$MESS['LIBRARY_LIST_YOUR_CITY'] = 'Ваш город';
$MESS['LIBRARY_LIST_YES'] = 'Да, верно';
$MESS['LIBRARY_LIST_EDIT'] = 'Изменить';
$MESS['LIBRARY_LIST_ENTER_CITY'] = 'Введите название города';
$MESS['LIBRARY_LIST_CHOOSE'] = 'или выберете вручную';
$MESS['LIBRARY_LIST_FAST_CHOOSE'] = 'Быстрый выбор';
$MESS['LIBRARY_LIST_ACCEPT'] = 'Принять';

//попап на карте
$MESS['LIBRARY_LIST_POPUP_ADDRESS'] = 'Address';
$MESS['LIBRARY_LIST_POPUP_WORK'] = 'Working schedule';
$MESS['LIBRARY_LIST_POPUP_USER'] = 'Участник';
$MESS['LIBRARY_LIST_POPUP_URL'] = 'Go to the library';