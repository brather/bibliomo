<?
	if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
	use \Bitrix\Main\Localization\Loc as Loc;
	Loc::loadMessages(__FILE__);
?>
<section class="rel innersection innerwrapper clearfix">
    <h1>Библиотеки</h1>
    <form method="get" action="." class="b-map_search_filtr">
        <div class="b-map_search">
            <div class="wrapper">
                <div class="b-cytyinfo">
                    <span class="b-map_search_txt"><?=Loc::getMessage('LIBRARY_LIST_FIND_CLOSEST');?></span>
                    <a href="#" class="b-citycheck setLinkPoint" data-lat="<?=$arResult["CITY"]["UF_POS"][1]?>" data-lng="<?=$arResult["CITY"]["UF_POS"][0]?>"><?=$arResult["CITY"]["UF_CITY_NAME"]?></a>
                    <a href="#" class="b-changecity js_changecity"><?=Loc::getMessage('LIBRARY_LIST_CHANGE_CITY');?></a>
                </div>
                <input type="text" class="b-map_tb ui-autocomplete-input searchonmap" name="q" id="qcity" autocomplete="off" placeholder="<?=Loc::getMessage('LIBRARY_LIST_ENTER_ADDRESS');?>">	  <input type="submit" class="button_b" value="<?=Loc::getMessage('LIBRARY_LIST_FIND_BUTTON');?>" name="submit">
            </div>
        </div><!-- /.b-map_search -->
    </form>
    <div class="b-elar_usrs">
        <ol class="b-elar_usrs_list">
            <?$i = $arResult["NAV_OFFSET"];?>
            <?foreach($arResult['ITEMS_PAGEN'] as $lib):?>
                <li>
                    <span class="num"><?=++$i?>.</span>
                    <div class="b-elar_name iblock">
                        <span class="b-elar_name_txt"><?=$lib['UF_NAME']?></span>
                        <?if($lib['IS_NEB']):?><div class="b-elar_status">Федеральная библиотека</div><?endif;?>
                    </div>
                    <div class="b-elar_address iblock">
                        <div class="b-elar_address_txt"><?=$lib['UF_ADRESS']?></div>
                    </div>
                </li>
            <?endforeach;?>
        </ol>
    </div><!-- /.b-elar_usrs -->

    <?=$arResult['NAVIGATION']?>
</section>
<?$this->SetViewTarget("library_city_select");?>
    <?if(!$arResult["IS_COOKIE_SET"]):?>
        <div class="b-citypopup">
            <div class="wrapper">
                <a href="#" class="close"></a>
                <span class="b-citylabel"><?=Loc::getMessage('LIBRARY_LIST_YOUR_CITY');?>
                    <a href="#" class="b-citycheck setLinkPoint" data-lat="<?=$arResult["CITY"]["UF_POS"][1]?>" data-lng="<?=$arResult["CITY"]["UF_POS"][0]?>"><?=$arResult["CITY"]["UF_CITY_NAME"]?></a> ? </span>
                <span class="b-citycheckbt">
                    <a href="#" class="button_b setLinkPoint setPointAndReload" data-city-id="<?=$arResult["CITY"]["ID"]?>" data-lat="<?=$arResult["CITY"]["UF_POS"][1]?>" data-lng="<?=$arResult["CITY"]["UF_POS"][0]?>"><?=Loc::getMessage('LIBRARY_LIST_YES');?></a>
                    <a href="#" class="button_gray js_changecity"><?=Loc::getMessage('LIBRARY_LIST_EDIT');?></a>
                </span>
            </div>
        </div>
    <?endif;?>
    <div class="b-citychangepopup" data-width="498">
        <a href="#" class="closepopup"></a>
        <form class="b-map_search_form " action="." method="get">
            <p><?=Loc::getMessage('LIBRARY_LIST_ENTER_CITY');?></p>
            <input type="text" name="" id="" data-path="<?=$APPLICATION->GetCurPage();?>?json=Y&type=city" class="input ui-autocomplete-input" autocomplete="off" placeholder="">
            <div class="b-choosecity"><div class="iblock b-citylist">
                    <p><?=Loc::getMessage('LIBRARY_LIST_CHOOSE');?></p>
                    <div class="b-handcity">
                        <?foreach($arResult["CITY_LIST"] as $areaName => $cityList):?>
                            <div class="b-handcityitem">
                                <div class="b-handcityregion"><?=$areaName?></div>
                                <ul class="b-cityquisklist">
                                    <?foreach($cityList as $k => $city):?>
                                        <li><a class="b-city" href="#" data-city-id="<?=$city["ID"]?>"><?=$city["UF_CITY_NAME"]?></a></li>
                                    <?endforeach;?>
                                </ul>
                            </div>
                        <?endforeach;?>
                    </div>
                </div><div class="iblock b-cityquisk">
                    <ul class="b-cityquisklist">
                        <?foreach($arResult["CITY_FAVORITES"] as $k => $city):?>
                            <li><a class="b-city" href="#" data-city-id="<?=$city["ID"]?>"><?=$city["UF_CITY_NAME"]?></a></li>
                        <?endforeach;?>
                    </ul>
                </div><div class="b-citycation">
                    <input type="submit" name="submit" value="<?=Loc::getMessage('LIBRARY_LIST_ACCEPT');?>" class="formbutton">
                </div></div>
        </form>
    </div>
<?$this->EndViewTarget();?>