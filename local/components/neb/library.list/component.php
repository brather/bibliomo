<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/**
* @var CNebLibraryListComponent $this
 * @global $APPLICATION
*/
CPageOption::SetOptionString("main", "nav_page_in_session", "N");
\CModule::IncludeModule("highloadblock");
\CModule::IncludeModule("evk.books");
//use Bitrix\Highloadblock as HL;
//use Bitrix\Main\Entity;


$arResult["JSON2"] = isset($_REQUEST['json2']) && ($_REQUEST['json2'] == 'Y');
$arParams["JSON"] = ((isset($_REQUEST['json']) && $_REQUEST['json']=="Y") || (isset($arParams["JSON"]) && (boolean)$arParams["JSON"]));

if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 3600;



$arNavParams = array(
	"nPageSize" => 4,
);
if(!$arResult["JSON2"])
	$arNavParams["iNumPage"] = 1;

$arResult["NAV_OFFSET"] = ((is_set($_GET['PAGEN_1']) ? $_GET['PAGEN_1'] : 1)-1) * intval($arParams['ROWS_PER_PAGE']);

//$this->clearComponentCache($this->__name);

if($arParams["JSON"])
	$this->GetLibraryCoordinates($arNavParams);

$this->GetLibraryList($arNavParams);

/*
if(!$arResult["JSON2"])
{

	$arNavigation = CDBResult::GetNavParams(array("nPageSize" => $arParams['ROWS_PER_PAGE']));
	if($arNavigation["PAGEN"]==0 && $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"]>0) $arParams["CACHE_TIME"] = $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"];

	//37.619899 - 0
	//55.753676 - 1




	$arResult['AJAX_PAGE'] = $componentPath."/ajax.php?json=Y";

	if($arParams["JSON"])
	{
		$this->GetLibraryCoordinates();
	}
	unset($arResult['ITEMS']);
}
*/

if($arResult["JSON2"])
	$APPLICATION->RestartBuffer();

$this->IncludeComponentTemplate();

if($arResult["JSON2"])
	exit;
?>