<?php
CModule::IncludeModule("highloadblock");
CModule::IncludeModule("evk.books");
use Evk\Books\IBlock\Element as IB;
use Evk\Books\Tables\NebLibsCityTable as cityTable;
class CNebLibraryListComponent extends CBitrixComponent
{
	public function GetLibraryCoordinates($arNavParams)
	{
		global $APPLICATION, $CACHE_MANAGER;

		$obCache = new CPHPCache();
		$cache_id = md5(__CLASS__)."_coordinates";
		$cache_time = 3600000000;
		$cache_path = sprintf('%s/%s', SITE_ID, $cache_id);

		$arJson = array();
		if($obCache->InitCache($cache_time, $cache_id, $cache_path))
		{
			extract($obCache->GetVars());
		}
		else
		{
			if($obCache->StartDataCache($cache_time, $cache_id, $cache_path))
			{
				$this->GetLibraryList($arNavParams);
				$arPoints = array();
				if(isset($this->arResult['LIBRARY_INFO']['ITEMS']))
				{
					foreach($this->arResult['LIBRARY_INFO']['ITEMS'] as $arItem)
					{
						$arPoint = array(
							'id' => $arItem['ID'],
							'address' => $arItem['PROPERTY_ADDRESS_VALUE'],
							'name' => $arItem['NAME'],
							'status' => $arItem['PROPERTY_STATUS_VALUE'], //есть не всегда
							'parten' => !empty($arItem['PROPERTY_STATUS_VALUE']),
							'link' => $arItem['DETAIL_PAGE_URL'], //есть не всегда
							'available' => $arItem['~PROPERTY_SCHEDULE_VALUE'], //есть не всегда
							'coordinates' => array($arItem["UF_POS"][1], $arItem["UF_POS"][0]),
						);
						if(isset($arItem["DETAIL_PICTURE"]) && isset($arItem["DETAIL_PICTURE"]["src"]))
							$arPoint["img"] = $arItem["DETAIL_PICTURE"]["src"];
						else
							$arPoint["img"] = "/local/templates/.default/markup/pic/slidenew_pic13.jpg";
						$arPoints[] = $arPoint;
					}
					$arJson['libs'][] = array(
						'id' => 1,
						'placemarksize' => array(34, 48),
						'placemarkicon' => '/local/templates/.default/markup/i/pin.png',
						'regions' =>array(
							array(
								'current' => true,
								'city' 	=> $this->arResult["CITY"]["UF_CITY_NAME"],
								'id'	=> $this->arResult["CITY"]["ID"],
								'coordinates'	=> array($this->arResult["CITY"]['UF_POS'][1], $this->arResult["CITY"]['UF_POS'][0]),
								'metro' =>
									array(
										array(
											'station' => '',
											'id' => 1,
											'points' => $arPoints,
										)
									)
							)
						)
					);
					if(!empty($arJson))
					{
						$CACHE_MANAGER->StartTagCache($cache_path);
						$CACHE_MANAGER->RegisterTag('neb_library_list_coordinates');
						$CACHE_MANAGER->EndTagCache();
						$obCache->EndDataCache(compact('arJson'));
					}
					else
						$obCache->AbortDataCache();
				}
			}
		}
		$APPLICATION->RestartBuffer();
		header('Content-Type: application/json');
		echo json_encode($arJson);
		exit();
	}
	public function GetLibraryList($arNavParams)
	{
		global $APPLICATION, $CACHE_MANAGER;

		$obCache = new CPHPCache();
		$cache_id = md5(__CLASS__)."_library_list";
		$cache_time = 3600000000;
		$cache_path = sprintf('%s/%s', SITE_ID, $cache_id);

		$arReturn = array();
		if($obCache->InitCache($cache_time, $cache_id, $cache_path))
		{
			extract($obCache->GetVars());
		}
		else
		{
			if($obCache->StartDataCache($cache_time, $cache_id, $cache_path))
			{
				$currentCity = LIBRARY_DEFAULT_CITY;
				$cityData = cityTable::getByID($currentCity);
				$cityData = $cityData->Fetch();
				$cityData["UF_POS"] = explode(" ", $cityData["UF_POS"]);
				$arResult["CITY"] = $cityData;
				$arLibraryInfo = IB::getList(
					array('IBLOCK_ID' => 4),
					false,
					array(
						'PREVIEW_TEXT',
						'DETAIL_PICTURE',
						'PROPERTY_MAP',
						'PROPERTY_STATUS',
						'PROPERTY_ADDRESS',
						'PROPERTY_SCHEDULE',
						'PROPERTY_PHONE',
						'PROPERTY_EMAIL',
						'PROPERTY_SKYPE',
						'PROPERTY_URL',
						'PROPERTY_COLLECTIONS',
						'PROPERTY_CONTACTS',
						'PROPERTY_VIEW_TEMPLATE',
						'PROPERTY_VIEW_THEME',
					)
				);

				foreach($arLibraryInfo["ITEMS"] as &$arBook)
				{
					$arPos = explode(",", $arBook["PROPERTY_MAP_VALUE"]);
					$arBook["UF_POS"][] = $arPos[1];
					$arBook["UF_POS"][] = $arPos[0];
					if(isset($arBook["DETAIL_PICTURE"]) && ($arBook["DETAIL_PICTURE"]=intval($arBook["DETAIL_PICTURE"]))>0)
					{
						$file = new CFile();
						$arBook["DETAIL_PICTURE"] = $file->ResizeImageGet($arBook["DETAIL_PICTURE"], array('width'=>240, 'height'=>140), BX_RESIZE_IMAGE_PROPORTIONAL, true);
					}
				}
				unset($arBook);
				$arReturn["LIBRARY_INFO"] = $arLibraryInfo;
				$arReturn["SHOW_MORE_BUTTON"] = ($arReturn["LIBRARY_INFO"]["NAV_RESULT"]->NavPageNomer < $arLibraryInfo["NAV_RESULT"]->NavPageCount);
				$arReturn["SHOW_MORE_PAGE"] = $APPLICATION->GetCurPageParam(sprintf('PAGEN_1=%d&json2=Y',
					$arReturn["LIBRARY_INFO"]["NAV_RESULT"]->NavPageNomer+1)
					, array('PAGEN_1', 'json2')
				);
				if(!empty($arLibraryInfo))
				{
					$CACHE_MANAGER->StartTagCache($cache_path);
					$CACHE_MANAGER->RegisterTag('neb_library_list_coordinates');
					$CACHE_MANAGER->EndTagCache();
					$obCache->EndDataCache(compact('arReturn'));
				}
				else
					$obCache->AbortDataCache();
			}
		}
		$this->arResult = array_merge($this->arResult, $arReturn);
	}
}