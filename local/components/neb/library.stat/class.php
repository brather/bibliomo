<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
if(!CModule::IncludeModule('evk.books'))
	return false;
use Evk\Books\Stat\BookStat;

class CBitrixComponentNebLibraryStat extends CBitrixComponent
{
	public function GetStatistics($libID=0, $from=0, $to=0, $timePeriod=null, $export=false)
	{
		$arResult = array();
		$period = BookStat::GetPeriod($from, $to);
		if(!is_null($timePeriod))
		{
			$period["period"] = $timePeriod;
		}
		$stat = BookStat::GetStatFromDB("neb_stat_book_view", $libID, $from, $to, $period, $export);
		$arResult["views"] = array(
			"id" => "containerVW",
			"name" => $this->arResult["NAMES"]["views"],
			"stat" => $stat["stat"],
		);
		$stat = BookStat::GetStatFromDB("neb_stat_book_download", 0, $from, $to, $period, $export);
		$arResult["downloads"] = array(
			"id" => "containerDW",
			"name" => $this->arResult["NAMES"]["downloads"],
			"stat" => $stat["stat"],
		);
		$stat = BookStat::GetStatFromBX($this->__name, $libID, $from, $to, 0, $period, $export);
		$arResult["editions"] = array(
			"id" => "containerED",
			"name" => $this->arResult["NAMES"]["editions"],
			"stat" => $stat["stat"],
		);
		$stat = BookStat::GetStatFromBX($this->__name, $libID, $from, $to, 1, $period, $export);
		$arResult["digitized_editions"] = array(
			"id" => "containerDE",
			"name" => $this->arResult["NAMES"]["digitized_editions"],
			"stat" => $stat["stat"],
		);
		return $arResult;
	}
}