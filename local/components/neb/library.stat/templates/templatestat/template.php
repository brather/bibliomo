<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
} ?>
<?
/**
 * @var CMain $APPLICATION
 * @var array $arResult
 * @var array $arParams
 */
?>
<? $APPLICATION->SetTitle("Статистика"); ?>
<div class="iblock b-lib_fulldescr">
    <div class='b-add_digital js_digital'>
        <form
            action="<?= $APPLICATION->GetCurPage() ?><? if ($arParams['FORMAT']
                == 'view'
            ): ?>?form=view<? endif; ?>" method="GET" name="form1">
            C <? echo CalendarDate(
                "from", $arParams['DATE_FROM'], "form1", "10",
                "class=\"b-text\""
            ) ?>
            по <? echo CalendarDate(
                "to", $arParams['DATE_TO'], "form1", "10", "class=\"b-text\""
            ) ?>
            <input type='hidden' value='<?= $arParams['FORMAT'] ?>' name='form'>
            <div class="divchekblok">
                <span>Количество изданий</span>
                <input class="checkbox" id="check"
                       type="checkbox"
                       name="count_items"
                       value="Y"
                       checked="checked"><br>
                <span>Количество изданий c электронной копией</span> <input
                    class="checkbox" id="check" type="checkbox"
                    name="count_digitalitems" value="Y" checked="checked"><br>
                <span>Количество прочитанных книг библиотеки</span> <input
                    class="checkbox" id="check" type="checkbox"
                    name="count_readbook" value="Y" checked="checked"><br>
            </div>
			<input type="submit" class="b-search_bth bbox" value="<?=GetMessage("LIBRARY_STAT_REFRESH");?>"
                   style='float:none;'>
        </form>
        <br>
        <div class="b-digitizing_actions stat-buttons-block">
			<a class="button_mode" target="_blank" href="<?=$APPLICATION->GetCurPageParam('export=Y', array('export'));?>"><?=GetMessage('LIBRARY_STAT_EXPORT_EXCEL')?></a>
        </div>

        <?php if (isset($arResult['grphicData'])
            && !empty($arResult['grphicData'])
        ) { ?>
            <div style="width:100%; overflow:hidden;">
                <ul class="uleditstat">
                    <?php foreach ($arResult['grphicData'] as $dataItem) { ?>
                        <li><a href=''><?php echo $dataItem['name'] ?></a>
                            <ul class="submenu2">
                                <li>
                                    <div id="<?php echo $dataItem['id'] ?>"
                                         style="width:500px; height:400px;"></div>
                                </li>
                            </ul>
                        </li>
                    <?php } ?>
                </ul>
            </div>
            <script>
                <?php foreach ($arResult['grphicData'] as $key => $dataItem)
                {
                    if(!isset($dataItem['stat']))
                    {
                        continue;
                    }
                    $stat = $dataItem['stat'];
                 ?>
                $(function () {
                    $('#<?php echo $dataItem['id'] ?>').highcharts({
                        title: false,
                        xAxis: {
                            categories: <?php echo json_encode($stat['dates']); ?>
                        },
                        yAxis: {
                            title: {
                                text: '<?php echo $dataItem['name'] ?>'
                            },
                            plotLines: [{
                                value: 100,
                                width: 3,
                                color: '#808080'
                            }],
                            min: 0,
							labels: {
								formatter: function () {
									if (this.value >= 1000000000){
										return this.value/1000000000+' млрд';
									}else if (this.value >= 1000000){
										return this.value/1000000+' млн';
									}else if(this.value >= 1000){
										return this.value/1000+' тыс';
									}
									return this.value
								}
							}
                        },
                        legend: true,
                        series: [{
                            name: '<?php echo $dataItem['name'] ?>',
                            data: <?php echo json_encode($stat['values']); ?>
                        }]
                    });
                });
                <?php } unset($stat); ?>
            </script>
        <? } ?>
        <?php echo htmlspecialcharsBack($arResult['STR_NAV']) ?>
    </div>
    <br><br>
</div>


