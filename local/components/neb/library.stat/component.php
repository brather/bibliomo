<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/**
 * @var CBitrixComponentNebLibraryStat $this
 */
use Bitrix\Main\Localization\Loc;
use Evk\Books\IBlock\Element;
use Evk\Books\nebUser;
use Bitrix\Main\Entity;
Loc::loadLanguageFile(__FILE__);

$export = isset($_REQUEST['export']) && 'Y' === $_REQUEST['export'];

if(!CModule::IncludeModule('iblock') || !CModule::IncludeModule('evk.books'))
    return false;
$maxDates = 15;
$time = time();


$defaultTimes = 60*60*24*7;
$arResult = array(
    'daysCount'   => 0,
    'interval'    => 0,
    'grphicData' => array(),
);

$arResult["NAMES"] = array(
    'date'               => 'Дата',
    'editions'           => GetMessage('LIBRARY_STAT_NUMBER'),
    'digitized_editions' => GetMessage('LIBRARY_STAT_NUMBER_COPY'),
    'downloads'          => GetMessage('LIBRARY_STAT_NUMBER_DOWNLOADED'),
    'views'              => GetMessage('LIBRARY_STAT_NUMBER_READ'),
);

$arParams["dateFrom"] = $arParams["dateTo"] = false;

$arRegexp = array(
    "YYYY" => "([2]\d{3})",
    "MM" => "(0[1-9]|1[0-2])",
    "DD" => "(0[1-9]|[12][0-9]|3[01])",
    "." => "\\.",
);
$arResult['interval'] = 0;
$checkRegexp = sprintf("@%s@", str_replace(array_keys($arRegexp), array_values($arRegexp), FORMAT_DATE));
$from = $to = false;
if(preg_match($checkRegexp, $arParams["DATE_FROM"]))
    $from = MakeTimeStamp($arParams["DATE_FROM"], FORMAT_DATE);
if(preg_match($checkRegexp, $arParams["DATE_TO"]))
    $to = MakeTimeStamp($arParams["DATE_TO"], FORMAT_DATE);

if(!$from && !$to)
{
    $to = $time;
    $from = strtotime("midnight", $to-$defaultTimes);
}
elseif(!$from)
{
    $from = strtotime("midnight", $to-$defaultTimes);
    $to = strtotime("midnight +1 day -1 second", $to);
}
elseif(!$to)
{
    $to = strtotime("midnight +1 day -1 second", $from + $defaultTimes);
    $from = strtotime("midnight", $from);
}
else
{
    $to = strtotime("midnight +1 day -1 second", $to);
    $from = strtotime("midnight", $from);
}

if($from > $time)
{
    $from = strtotime("midnight", $time);
    $to = $time;
}
elseif($to > $time)
    $to = $time;
if($from > $to)
    $to = strtotime("midnight +1 day -1 second", $from);

//echo date("Y.m.d H:i:s", $from)." ".date("Y.m.d H:i:s", $to)."<br/>";

$arParams["DATE_TO"] = ConvertTimeStamp($to, "SHORT");
$arParams["dateTo"] = ConvertDateTime($arParams["DATE_TO"], "YYYY-MM-DD HH:II:SS");
$arParams["DATE_FROM"] = ConvertTimeStamp($from, "SHORT");
$arParams["dateFrom"] = ConvertDateTime($arParams["DATE_FROM"], "YYYY-MM-DD HH:II:SS");

$ost = ($to - $from) % 86400;
$obUser = new nebUser();
if($arLibrary = $obUser->getLibrary())
{
    $timePeriod = null;
    if($export) $timePeriod = \Evk\Books\Stat\BookStat::PERIOD_DAY;
    $arResult["grphicData"] = $this->GetStatistics($arLibrary["ID"], $from, $to, $timePeriod, $export);
}

$template = '';
if ($export)
{
    $arResult['statData'] = array(
        array(
            $arResult["NAMES"]["date"].": ",
            date("Y.m.d H:i:s"),
        ),
        array(
            "",
            $arResult["NAMES"]["editions"],
            $arResult["NAMES"]["digitized_editions"],
            $arResult["NAMES"]["downloads"],
            $arResult["NAMES"]["views"],
        ),
    );
    $arResult["grphicData"]["editions"]["stat"]["dates"] = array_reverse($arResult["grphicData"]["editions"]["stat"]["dates"]);
    $arResult["grphicData"]["editions"]["stat"]["values"] = array_reverse($arResult["grphicData"]["editions"]["stat"]["values"]);
    $arResult["grphicData"]["digitized_editions"]["stat"]["values"] = array_reverse($arResult["grphicData"]["digitized_editions"]["stat"]["values"]);
    $arResult["grphicData"]["downloads"]["stat"]["values"] = array_reverse($arResult["grphicData"]["downloads"]["stat"]["values"]);
    $arResult["grphicData"]["views"]["stat"]["values"] = array_reverse($arResult["grphicData"]["views"]["stat"]["values"]);
    foreach($arResult["grphicData"]["editions"]["stat"]["dates"] as $key=>$stat)
    {
        $arResult['statData'][] = array(
            $stat,
            $arResult["grphicData"]["editions"]["stat"]["values"][$key],
            $arResult["grphicData"]["digitized_editions"]["stat"]["values"][$key],
            $arResult["grphicData"]["downloads"]["stat"]["values"][$key],
            $arResult["grphicData"]["views"]["stat"]["values"][$key],
        );
    }
    $template = 'export';
}
//echo json_encode($arResult["STAT_VIEW"]["COUNTS"]);
$this->IncludeComponentTemplate($template);