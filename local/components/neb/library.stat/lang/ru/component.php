<?php
$MESS['LIBRARY_STAT_EXPORT_EXCEL'] = "Экспорт в Excel";
$MESS['LIBRARY_STAT_REFRESH'] = "Обновить";
$MESS['LIBRARY_STAT_NUMBER'] = "Количество изданий";
$MESS['LIBRARY_STAT_NUMBER_COPY'] = "Количество изданий с электронной копией";
$MESS['LIBRARY_STAT_NUMBER_DOWNLOADED'] = "Количество скаченных изданий";
$MESS['LIBRARY_STAT_NUMBER_READ'] = "Количество прочитанных изданий";
?>