<?
	if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)
		die();

    if($_REQUEST['action'] == 'changePasswordRequest' &&
        check_bitrix_sessid() &&
        isset($_REQUEST['NEW_PASSWORD']) &&
        isset($_REQUEST['NEW_PASSWORD_CONFIRM'])
    ){
        if(isset($arParams["USER_ID"]) && intval($arParams["USER_ID"]) > 0)
        {
            $arResult["CURRENT_USER"] = false;
            $USER_ID = intval($arParams["USER_ID"]);
        }
        else
        {
            $arResult["CURRENT_USER"] = true;
            $USER_ID = $USER->GetID();
        }

        if( !$arResult["CURRENT_USER"] ||
            (isset($_REQUEST['CURRENT_PASSWORD']) && $arResult["CURRENT_USER"])
        )
        {
            $APPLICATION->RestartBuffer();
            if($arResult["CURRENT_USER"])
            {
                $user = CUser::GetByID($USER_ID);
                $user = $user->Fetch();

                $password = $_REQUEST['CURRENT_PASSWORD'];
                $salt = substr($user["PASSWORD"], 0, strlen($user["PASSWORD"]) - 32);
                $password =  $salt . md5($salt.$password);
                $password == $user['PASSWORD'] or die('Текущий пароль указан не правильно.');
            }

            $_REQUEST['NEW_PASSWORD'] == $_REQUEST['NEW_PASSWORD_CONFIRM'] or die('Пароли не совпадают.');

            $passwordErrors = CAllUser::CheckPasswordAgainstPolicy($_REQUEST['NEW_PASSWORD'], CUser::GetGroupPolicy($USER_ID));
            if(!empty($passwordErrors)){
                if($arResult["CURRENT_USER"])
                {
                    die($passwordErrors[0]); //пароль не соответствует настройкам безопасности
                }
                else
                {
                    $arResult["ERROR_MESSAGE"] = $passwordErrors[0];
                }
            }

            if($arResult["CURRENT_USER"])
            {
                $userFields = Array(
                    'WORK_NOTES' => json_encode(Array('TIMESTAMP' => time(), 'PASSWORD' => $_REQUEST['NEW_PASSWORD'])),
                );

                $u = new CUser;
                if(!$u->Update($user['ID'], $userFields)){
                    die($u->LAST_ERROR);
                }

                CUser::SendUserInfo($user["ID"], SITE_ID, 'Смена пароля', false, 'NEB_USER_PASS_REQUEST');
                die("1");
            }
            else
            {
                if(!isset($arResult["ERROR_MESSAGE"]) || empty($arResult["ERROR_MESSAGE"]))
                {
                    $obUser = new CUser;
                    if($obUser->Update($USER_ID, array("PASSWORD"=>$_REQUEST['NEW_PASSWORD'])))
                    {
                        $arResult["SUCCESS_MESSAGE"] = "Пароль читателя успешно изменён.";
                    }
                    else
                    {
                        $arResult["ERROR_MESSAGE"] = "Не удалось изменить пароль читателя.";
                    }
                }
            }
        }
    }

    if($_REQUEST['action'] == 'changePasswordConfirm' &&
        isset($_REQUEST['CHECKWORD']) &&
        isset($_REQUEST['USER_ID'])
    ){
        $user = CUser::GetByID($_REQUEST['USER_ID']);
        $user = $user->Fetch();

        $user or die('Параметр USER_ID указан не правильно.');

        $userFields = json_decode($user['WORK_NOTES'], true);
        is_array($userFields) or die('Необходимо повторить запрос на смену пароля.');

        //прошло 24 часа?
        (time() < $userFields['TIMESTAMP'] + 24 * 60 * 60) or die('Необходимо повторить запрос на смену пароля.');


        global $USER;
        $result = $USER->ChangePassword($user['LOGIN'], $_REQUEST['CHECKWORD'], $userFields['PASSWORD'], $userFields['PASSWORD']);

        if($result['TYPE'] == 'OK'){
            if($USER->isAuthorized()) $USER->Logout();
            LocalRedirect('/?PASSWORD_CHANGED=Y');
        } else {
            die($result['MESSAGE']);
        }

    }

	$this->IncludeComponentTemplate($templatePage);
?>