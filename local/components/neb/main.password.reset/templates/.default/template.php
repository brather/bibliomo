<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();
?>
<?
if(isset($arResult["SUCCESS_MESSAGE"]))
{
    ShowNote($arResult["SUCCESS_MESSAGE"]);
    die();
}
if(isset($arResult["ERROR_MESSAGE"]))
{
    ShowError($arResult["ERROR_MESSAGE"]);
    die();
}
?>
<form method="post" name="form1" action="" class="b-form b-form_common b-profile_passform" id='regform' enctype="multipart/form-data">
    <?=bitrix_sessid_post()?>
    <input type="hidden" name="action" value="changePasswordRequest" />
    <?php# if($arResult["CURRENT_USER"]):?>
    <div class="b-form_header"><?=GetMessage('CHANGE_PASSW');?></div>
    <div class="fieldrow nowrap">
        <div class="fieldcell iblock">
            <label for="settings087">Текущий пароль</label>
            <div class="field validate">
                <input type="password" data-validate="password" data-required="true" value="" id="settings087" data-maxlength="30" data-minlength="2" name="CURRENT_PASSWORD" class="input">
                <em class="error required">Заполните</em>
            </div>
        </div>
    </div>
    <?php# endif;?>
    <div class="fieldrow nowrap">
        <div class="fieldcell iblock">
            <em class="hint">*</em>
            <label for="settings05"><?=GetMessage('NEW_PASS');?></label>
            <div class="field validate">
                <input type="password" data-validate="password" data-required="required" value="" id="settings05" data-maxlength="30" data-minlength="2" name="NEW_PASSWORD" class="pass_status input">
                <em class="error required"><?=GetMessage('REQUIRED_PASS');?></em>
            </div>
            <div class="passecurity">
                <div class="passecurity_lb"><?=GetMessage('DEFENSE_PASS');?></div>
            </div>
        </div>
    </div>
    <div class="fieldrow nowrap">
        <div class="fieldcell iblock">
            <em class="hint">*</em>
            <label for="settings55"><?=GetMessage('PASS_TWO');?></label>
            <div class="field validate">
                <input data-identity="#settings05" type="password" data-required="required" value="" id="settings55" data-maxlength="30" data-minlength="2" name="NEW_PASSWORD_CONFIRM" class="input" data-validate="password">
                <em class="error required"><?=GetMessage('REQUIRED_PASS');?></em>
                <em class="error identity"><?=GetMessage('NOT_MATCH');?></em>
            </div>
        </div>
    </div>
    <div class="fieldrow nowrap fieldrowaction">
        <div class="fieldcell ">
            <div class="field clearfix">
                <button name="save" class="formbtn" value="1" type="submit"><?=GetMessage('CONFIRM');?></button>
                <div class="messege_ok hidden">
                    <a href="#" class="closepopup">Закрыть окно</a>
                    <p>На Вашу электронную почту, указанную при регистрации, отправлено письмо. <br/>Для подтверждения нового пароля воспользуйтесь ссылкой из письма. <br/>Срок действия ссылки - 24 часа.</p>
                </div>
            </div>
        </div>
    </div>
</form>