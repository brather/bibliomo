<?
	if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

	$arComponentDescription = array(
		"NAME" => "Каталог ББК детальная страница",
		"DESCRIPTION" => "",
		"ICON" => "/images/icon.gif",
		"SORT" => 10,
		"CACHE_PATH" => "Y",
		"PATH" => array(
			"ID" => "catalog_bbk",
			"CHILD" => array(
				"ID" => "catalog_bbk_list",
				"NAME" => "Каталог ББК детальная страница",
				"SORT" => 30,
			)
		),
	);

?>