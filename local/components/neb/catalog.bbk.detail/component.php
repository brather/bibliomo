<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Main\Loader;
Loader::includeModule("iblock");
$arFilter = array(
	"IBLOCK_ID" => 15,
);
$arResult = $arParams["arResult"];
$obSection = new \Evk\Books\BBKIndexContainer();
$indexList = $obSection->GetBBKIndexList();
$arResult["INDEXES"] = $indexList['arSection'];
//$arResult["SECTION"] = array();
$arResult["BBK_AJAX"] = (isset($_REQUEST['bbk_ajax']) && $_REQUEST['bbk_ajax']=='Y');
$arResult["find_author_or_title"] = (isset($_REQUEST['find_author_or_title']) && !empty($_REQUEST['find_author_or_title'])) ? htmlspecialchars(trim($_REQUEST['find_author_or_title'])) : '';


if(intval($arResult["VARIABLES"]["CODE"]) > 0)
{
	$arFilter["ID"] = intval($arResult["VARIABLES"]["CODE"]);
	if($rsData = CIBlockSection::GetList(array(), $arFilter, false, array("UF_*")))
	{
		if($arData = $rsData->Fetch())
		{
			if(!empty($arData["UF_SEE"]))
			{
				$arData["SEE"] = array();
				foreach($arData["UF_SEE"] as $seeKey=>$seeVal)
				{
					if(preg_match('@^\^[a-z]([^\^]*)(\^[a-z]([^\^]*))?@', $seeVal, $seeMatch))
					{
						$arData["SEE"][$seeKey]["TEXT"] = $seeMatch[1];
						$arData["SEE"][$seeKey]["INDEX"] = $seeMatch[3];
						$arData["SEE"][$seeKey]["SECTION_ID"] = $obSection->GetSectionIDByIndexName($seeMatch[3]);
						if(!empty($arData["SEE"][$seeKey]["SECTION_ID"]))
						{
							$arData["SEE"][$seeKey]["LINK"] = sprintf('/catalog_bbk/%d/', $arData["SEE"][$seeKey]["SECTION_ID"]);
						}
					}
				}
			}
			if(!empty($arData["UF_SEE_ALSO"]))
			{
				$arData["SEE_ALSO"] = array();
				foreach($arData["UF_SEE_ALSO"] as $seeKey=>$seeVal)
				{
					if(preg_match('@^\^[a-z]([^\^]*)@', $seeVal, $seeMatch))
					{
						$arData["SEE_ALSO"][$seeKey]["INDEX"] = $seeMatch[1];
						$arData["SEE_ALSO"][$seeKey]["SECTION_ID"] = $obSection->GetSectionIDByIndexName($seeMatch[1]);
						if(!empty($arData["SEE"][$seeKey]["SECTION_ID"]))
						{
							$arData["SEE_ALSO"][$seeKey]["LINK"] = sprintf('/catalog_bbk/%d/', $arData["SEE_ALSO"][$seeKey]["SECTION_ID"]);
						}
					}
				}
			}
			$arResult["SECTION"] = $arData;
			$arFilterBook = array();
			$arFilterBook["=PROPERTY_BBK"] = $arData["XML_ID"];
			if(!empty($arResult["find_author_or_title"]))
			{
				$arFilterBook[] = array(
					"LOGIC" => "OR",
					array(
						"%NAME" => $arResult["find_author_or_title"],
					),
					array(
						"%PROPERTY_AUTHOR" => $arResult["find_author_or_title"],
					),
				);
			}

			$obSearch = new \Evk\Books\SearchQuery($arResult, $arParams);

			$arNavPageParams = array();
			$arNavPageParams["nPageSize"] = 6;
			//$arNavPageParams["iPageNum"];

			$arResult["SECTION"]["BOOKS"] = $obSearch->FindBooksSimple(array(), $arFilterBook, false, $arNavPageParams, array(
				"PROPERTY_FILE",
				"PROPERTY_AUTHOR",
				"PROPERTY_PUBLISH_YEAR",
				"NAME",
			));
			if($obSearch->obNavigate->NavPageNomer < $obSearch->obNavigate->NavPageCount)
			{
				$arResult["NEXT_BOOK_PAGE"] = $APPLICATION->GetCurPageParam(sprintf('PAGEN_1=%d&bbk_ajax=Y', $obSearch->obNavigate->NavPageNomer+1), array('PAGEN_1', 'bbk_ajax'));
			}
		}
	}
}
if($arResult["BBK_AJAX"])
	$APPLICATION->RestartBuffer();
$this->IncludeComponentTemplate();
if($arResult["BBK_AJAX"])
	exit;
?>