<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?php if(!$arResult["BBK_AJAX"]):?>
<div class="wrapper">
	<section class="b-inner clearfix map-inner">
		<div class="clearfix onelib js_oneheight">
			<div class="b-onelib_side bbox">
				<?php if(!empty($arResult["SECTION"]["SEE"])):?>
					<div class="b-libstatistic">
						<div class="title">Смотри</div>
						<?foreach($arResult["SECTION"]["SEE"] as $see):?>
							<div class="text"><?=$see["TEXT"]?> <a href="<?=$see["LINK"]?>"><?=$see["INDEX"]?></a></div>
						<?endforeach;?>
					</div>
				<?endif;?>
				<?php if(!empty($arResult["SECTION"]["SEE_ALSO"])):?>
					<div class="b-libstatistic">
						<div class="title">Смотри также</div>
						<?foreach($arResult["SECTION"]["SEE_ALSO"] as $see):?>
							<div class="text"><a href="<?=$see["LINK"]?>"><?=$see["INDEX"]?></a></div>
						<?endforeach;?>
					</div>
				<?endif;?>
				<?php if(!empty($arResult["SECTION"]["UF_RELATED"])):?>
					<div class="b-libstatistic">
						<div class="title">Смежные области</div>
						<?foreach($arResult["SECTION"]["UF_RELATED"] as $see):?>
							<div class="text"><?=$see?></div>
						<?endforeach;?>
					</div>
				<?endif;?>
			</div><!-- /.b-onelib_side -->

			<div class="b-onelib_info b-titlefz">
				<h1><span><?=$arResult["SECTION"]["XML_ID"]?></span> <?=$arResult["SECTION"]["NAME"]?></h1>
				<div class="b-libmain_info">
					<?if($arResult["SECTION"]["UF_EXT_RUBRIC"]):?>
						<div class="b-libmain_status iblock dark">
							<span><?=$arResult["SECTION"]["UF_EXT_RUBRIC"]?></span>
						</div>
					<?endif;?>
						<div class="b-libmain_info__search" style="position: relative;">
							<form>
								<input type="text" class="b-collection_tb iblock" placeholder="Поиск книги по названию или (и) автору" name="find_author_or_title", id="find_author_or_title" value="<?=$arResult["find_author_or_title"]?>">
								<input type="submit" class="b-collection_bt bt_typelt iblock" value="Искать">
							</form>
						</div>
				</div>
<?php endif;?>
				<?if(count($arResult["SECTION"]["BOOKS"]) > 0):?>
					<ul class="b-ebooklist">
						<?foreach($arResult["SECTION"]["BOOKS"] as $arBook):?>
							<li class="iblock">
								<a href="<?=$arBook["VIEWER_URL"]?>" class="b-ebook">
									<?php if(!empty($arBook["IMAGE_URL_PREVIEW"])):?>
									    <img src="<?=$arBook["IMAGE_URL_PREVIEW"]?>" alt="<?=$arBook["NAME"]?>">
									<?php else:?>
										<img src="/local/images/book_empty.jpg" alt="<?=$arBook["NAME"]?>">
									<?php endif;?>
									<div class="b-ebooktitle"><?=$arBook["NAME"]?></div>
									<div class="b-ebook_autor">
										<?php if(!empty($arBook["PROPERTY_PUBLISH_YEAR_VALUE"])):?>
											<span class="b-ebook_year"><?=$arBook["PROPERTY_PUBLISH_YEAR_VALUE"]?></span>
										<?php endif;?>
										<?php if(!empty($arBook["PROPERTY_AUTHOR_VALUE"])):?>
											<span class="b-ebook_name"><?=$arBook["PROPERTY_AUTHOR_VALUE"]?></span>
										<?php endif;?>
									</div>
								</a>
							</li>
						<?endforeach;?>
					</ul>
					<?php if(!empty($arResult["NEXT_BOOK_PAGE"])):?>
						<div class="b-wrap_more">
							<a href="<?=$arResult["NEXT_BOOK_PAGE"]?>" class="ajax_add b-morebuttton">Показать еще</a>
						</div>
					<?php endif;?>
				<?php else:?>
					<div class="b-wrap_more">
						Для данного раздела книги не найдены
					</div>
				<?php endif;?>
<?php if(!$arResult["BBK_AJAX"]):?>
			</div><!-- /.b-onelib_info -->
		</div><!-- /.b-clearfix -->
	</section>
</div>
<?php endif;?>