<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if($arResult["IS_ADMIN"]):?>
	<script type="text/javascript">
		document.addEventListener('ChatLoaded', function() {
			Chat.startConsultant({
				id: "<?=urlencode($arResult["USER"]["ID"])?>",
				fio: "<?=urlencode($arResult["USER"]["FULL_NAME"])?>"
			});
		}, false);
	</script>
<?else:?>
	<script type="text/javascript">
		document.addEventListener('ChatLoaded', function() {
			Chat.start({
				id: "<?=$arResult["USER"]["ID"]?>",
				fio: "<?=urlencode($arResult["USER"]["FULL_NAME"])?>",
				themeUrl: "<?=urlencode($arResult["URL"])?>",
				topicId: "<?=urlencode($arResult["TITLE"])?>"
			});
		}, false);
	</script>
<?endif;?>

<script data-main="http://neb.rozumsoft.by:1337/js/data-main.js" src="http://neb.rozumsoft.by:1337/js/dependencies/requirejs/require.js"></script>