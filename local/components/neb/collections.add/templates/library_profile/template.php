<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use \Bitrix\Main\Localization\Loc as Loc;
Loc::loadMessages(__FILE__);
?>

<div class="b-collectionformpage">
    <span class="right regfield_label">* – обязательные поля</span>
    <h2 class="mode">создание коллекции</h2>
    <form action="<?=$APPLICATION->GetCurPage()?>" method="POST" class="b-form b-form_common b-collectform">
        <?if($arResult['COLLECTION']):?>
            <input type="hidden" name="action" value="editCollection"/>
            <input type="hidden" name="id" value="<?=$arResult['COLLECTION']['ID']?>"/>
            <input type="hidden" name="code" value="<?=$arResult['COLLECTION']['CODE']?>"/>
        <?else:?>
            <input type="hidden" name="action" value="addCollection"/>
        <?endif;?>
        <?=bitrix_sessid_post()?>
        <div class="fieldrow nowrap">
            <div class="fieldcell iblock w700">
                <em class="hint">*</em>
                <label for="settings02">Название коллекции</label>
                <div class="field validate">
                    <input type="text" data-required="required" value="<?=$arResult['COLLECTION']['NAME']?>" id="settings02" data-maxlength="30" data-minlength="2" name="name" class="input" >
                </div>
            </div>
        </div>

        <?$APPLICATION->IncludeComponent(
            "notaext:plupload",
            "library_collection_add",
            array(
                "MAX_FILE_SIZE" => "24",
                "FILE_TYPES" => "jpg,jpeg,png",
                "DIR" => "tmp_collection",
                "FILES_FIELD_NAME" => "profile_file",
                "MULTI_SELECTION" => "N",
                "CLEANUP_DIR" => "Y",
                "UPLOAD_AUTO_START" => "Y",
                "RESIZE_IMAGES" => "Y",
                "RESIZE_WIDTH" => "4000",
                "RESIZE_HEIGHT" => "4000",
                "RESIZE_CROP" => "Y",
                "RESIZE_QUALITY" => "98",
                "UNIQUE_NAMES" => "Y",
                "FILE_PATH" => CFile::GetPath($arResult['COLLECTION']['DETAIL_PICTURE'])
            ),
            false
        );?>

        <div class="fieldrow nowrap">
            <div class="fieldcell iblock mt10">
                <label for="settings22">Описание коллекции</label>
                <div class="field validate">
                    <textarea name="description" id="" id="settings22" data-maxlength="1500" data-minlength="2" class="input" ><?=$arResult['COLLECTION']['DESCRIPTION']?></textarea>
                </div>
            </div>
        </div>

        <hr/>
        <div class="checkwrapper ">
            <input class="checkbox" type="checkbox" <?if($arResult['COLLECTION']['UF_SLIDER']):?>checked="checked"<?endif;?> name="is_show_in_slider" id="show_in_slider" value="1"><label for="show_in_slider" class="black">Показывать в слайдере</label>
        </div>
        <div class="checkwrapper mt10">
            <input class="checkbox" type="checkbox" <?if($arResult['COLLECTION']['UF_SHOW_NAME']):?>checked="checked"<?endif;?> name="is_show_name" id="show_name" value="1"><label for="show_name" class="black">Показывать название коллекции в слайдере</label>
        </div>

        <hr/>
        <div class="fieldrow nowrap fieldrowaction">
            <div class="fieldcell iblock w700">
                <div class="field clearfix">
                    <button class="formbutton" value="1" type="submit">сохранить</button>
                </div>
            </div>
        </div>
    </form>
</div><!-- /.b-newsform-->



</div><!-- /.b-mainblock -->
<div class="b-side right b-sidesmall">
    <a href="#" class="b-btlibpage">Страница библиотеки<br>на портале</a>

</div><!-- /.b-side -->



