<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

    CModule::IncludeModule('evk.books');
    //use Nota\Library\Collection;
    use Evk\Books\Collection;

    //\Evk\Books\Books::p($arParams);

    if($_REQUEST['action'] == 'addCollection' && isset($_REQUEST['name']) && check_bitrix_sessid()){
        $return = Collection::add($_REQUEST['name'], $_REQUEST['description'], $_REQUEST['background'], $arParams['LIBRARY']["ID"], isset($_REQUEST['is_show_in_slider']), isset($_REQUEST['is_show_name']));
        if($return === true){
            LocalRedirect('/profile/collection/');
        } else {
            ShowError($return);
        }
    }

    if($_REQUEST['action'] == 'editCollection' && isset($_REQUEST['id']) && check_bitrix_sessid()){
        $return = Collection::edit($_REQUEST['id'], $_REQUEST['code'], $_REQUEST['name'], $_REQUEST['description'], $_REQUEST['background'], $arParams['LIBRARY_ID'], isset($_REQUEST['is_show_in_slider']), isset($_REQUEST['is_show_name']));
        if($return === true){
            LocalRedirect('/profile/collection/');
        } else {
            ShowError($return);
        }
    }

    if((int)$arParams['COLLECTION'] > 0){
        $result = $arResult['COLLECTION'] = Collection::getByID($arParams['COLLECTION']);
        //редирект на добавление, если коллекция не сущестует
        if(!$result)
            LocalRedirect('/profile/collection/add/');
    }

    $this->IncludeComponentTemplate();
?>