<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

CModule::IncludeModule("evk.books");

if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 3600;

$arParams['LIBRARY_ID'] = intval($arParams['LIBRARY_ID']);
if(empty($arParams['LIBRARY_ID']))
	return false;

$arParams['COLLECTION_URL'] = trim($arParams['COLLECTION_URL']);

if(empty($arParams['IBLOCK_ID']))
	return false;

use Evk\Books\IBlock\Element;
$arResult = Element::getByID(
	$arParams['LIBRARY_ID'],
	array(
		'PREVIEW_PICTURE',
		'PROPERTY_CONTACTS',
		'PROPERTY_EMAIL',
		'PROPERTY_PHONE',
		//'PROPERTY_PUBLICATIONS',
		//'PROPERTY_COLLECTIONS',
		//'PROPERTY_USERS',
		//'PROPERTY_VIEWS',
		'PROPERTY_ADDRESS',
		'PROPERTY_SCHEDULE',
		'skip_other'
	)
);



$arResult["~PROPERTY_CONTACTS_VALUE"] = unserialize($arResult["~PROPERTY_CONTACTS_VALUE"]);
$arResult["~PROPERTY_PHONE_VALUE"] = unserialize($arResult["~PROPERTY_PHONE_VALUE"]);

$arResult["PROPERTY_VIEWS_VALUE"] = \Evk\Books\Stat\BookStat::CountBooksViewsForLibrary($arParams['LIBRARY_ID']);
$arResult["PROPERTY_USERS_VALUE"] = \Evk\Books\Stat\BookStat::CountUsersForLibrary($arParams['LIBRARY_ID']);
$arResult["PROPERTY_PUBLICATIONS_VALUE"] = \Evk\Books\Stat\BookStat::CountBooksForLibrary($arParams['LIBRARY_ID']);
$arCounts = \Evk\Books\Stat\BookStat::CountCollectionsForLibrary($arParams['LIBRARY_ID']);
$arResult["PROPERTY_COLLECTIONS_VALUE"] = $arCounts["countAll"];

if(!empty($arResult['PREVIEW_PICTURE']))
{
	$arResult['PREVIEW_PICTURE'] = CFile::ResizeImageGet($arResult['PREVIEW_PICTURE'], array('width'=>150, 'height'=>150), BX_RESIZE_IMAGE_PROPORTIONAL, true);
}

$this->IncludeComponentTemplate();
?>