<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use \Bitrix\Main\Localization\Loc as Loc;
Loc::loadMessages(__FILE__);
//$arResult['PROPERTY_COLLECTIONS_VALUE'] = 2;
?>
<div class="b-libfond <?=$arParams['CLASS']?>">
	<?
		if(!empty($arResult['PREVIEW_PICTURE']))
		{
		?>
		<div class="b-fond_img">
			<img src="<?=$arResult['PREVIEW_PICTURE']['src']?>" alt="<?=$arResult['NAME']?>">
		</div>
		<?
		}
	?>
	<?
		if(!empty($arResult['PROPERTY_PUBLICATIONS_VALUE']))
		{
		?>
		<span class="b-fondinfo_number_lb"><?=Loc::getMessage('LIBRARY_RIGHT_COUNTER_FUND');?></span>
		<span class="b-portalinfo_numbers">
			<?=getNumberNeb($arResult['PROPERTY_PUBLICATIONS_VALUE']);?>
		</span>
		<span class="b-fondinfo_number_lb"><?=GetEnding($arResult['PROPERTY_PUBLICATIONS_VALUE'], Loc::getMessage('LIBRARY_RIGHT_COUNTER_PUB_5'), Loc::getMessage('LIBRARY_RIGHT_COUNTER_PUB_1'), Loc::getMessage('LIBRARY_RIGHT_COUNTER_PUB_2'))?></span>
		<?
		}
	?>
	<?
		if(!empty($arResult['PROPERTY_COLLECTIONS_VALUE']))
		{
			?>
			<div class="b-lib_collect">
				<span class="b-fondinfo_number_lb"><?=Loc::getMessage('LIBRARY_RIGHT_COUNTER_INC');?></span>
				<span class="b-portalinfo_numbers">
					<?=getNumberNeb($arResult['PROPERTY_COLLECTIONS_VALUE']);?>
				</span>
				<span class="b-fondinfo_number_lb"><?=GetEnding($arResult['PROPERTY_COLLECTIONS_VALUE'], Loc::getMessage('LIBRARY_RIGHT_COUNTER_COL_5'), Loc::getMessage('LIBRARY_RIGHT_COUNTER_COL_1'), Loc::getMessage('LIBRARY_RIGHT_COUNTER_COL_2'))?></span>
				<?
					if(!empty($arParams['COLLECTION_URL']))
					{
					?>
					<a href="<?=$arParams['COLLECTION_URL']?>" class="button_mode button_revers"><?=Loc::getMessage('LIBRARY_RIGHT_COUNTER_VIEW_ALL');?></a>
					<?
					}
				?>
			</div>
			<?
		}
	?>
	<?
		if(!empty($arResult['PROPERTY_USERS_VALUE']))
		{
		?>
		<div class="b-lib_counter">
			<div class="b-lib_counter_lb"><?=Loc::getMessage('LIBRARY_RIGHT_COUNTER_READER');?></div>
			<div class="icouser">х <?=number_format($arResult['PROPERTY_USERS_VALUE'], 0, '', ' ')?></div>
		</div>
		<?
		}
	?>
	<?
		if(!empty($arResult['PROPERTY_VIEWS_VALUE']))
		{
		?>
		<div class="b-lib_counter">
			<div class="b-lib_counter_lb"><?=Loc::getMessage('LIBRARY_RIGHT_COUNTER_VIEWERS');?></div>
			<div class="icoviews">х <?=number_format($arResult['PROPERTY_VIEWS_VALUE'], 0, '', ' ')?></div>
		</div>
		<?
		}
	?>
</div>
