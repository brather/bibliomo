<?
	$MESS['CATALOG_PROMO_TEMPLATE_COLLECTIONS'] = 'коллекции';
	$MESS['CATALOG_PROMO_TEMPLATE_MORE'] = 'посмотреть все';
	$MESS['ADD_MY_LIB'] = '<span>Добавить</span> в Мою библиотеку';
	$MESS['REMOVE_MY_LIB'] = '<span>Удалить</span> из Моей библиотеки';

    $MESS['COLLECTION_SLIDER_BOOK_5'] = 'книг';
    $MESS['COLLECTION_SLIDER_BOOK_1'] = 'книга';
    $MESS['COLLECTION_SLIDER_BOOK_2'] = 'книги';
    $MESS['COLLECTION_SLIDER_IN_COL'] = 'в подборке';