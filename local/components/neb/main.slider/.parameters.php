<?
	if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

	if(!CModule::IncludeModule('iblock')) die();

	use \Bitrix\Main;
	use \Bitrix\Main\Localization\Loc as Loc;
	Loc::loadMessages(__FILE__);


	$arIBlockType = CIBlockParameters::GetIBlockTypes();

	$arIBlock=array();
	$rsIBlock = CIBlock::GetList(Array("sort" => "asc"), Array("TYPE" => $arCurrentValues["IBLOCK_TYPE"], "ACTIVE"=>"Y"));
	while($arr=$rsIBlock->Fetch())
	{
		$arIBlock[$arr["ID"]] = "[".$arr["ID"]."] ".$arr["NAME"];
	}

	try
	{
		$arComponentParameters = array(
			'GROUPS' => array(
			),
			'PARAMETERS' => array(
				"IBLOCK_TYPE" => array(
					"PARENT" => "BASE",
					"NAME" => 'IBLOCK_TYPE',
					"TYPE" => "LIST",
					"VALUES" => $arIBlockType,
					"REFRESH" => "Y",
				),
				"IBLOCK_ID" => array(
					"PARENT" => "BASE",
					"NAME" => 'IBLOCK_ID',
					"TYPE" => "LIST",
					"VALUES" => $arIBlock,
					"REFRESH" => "Y",
					"ADDITIONAL_VALUES" => "Y",
				),
				"PAGE_URL" => array(
					"PARENT" => "BASE",
					"NAME" => Loc::getMessage('CATALOG_PROMO_PARAMETERS_PAGE_URL'),
					"TYPE" => "STRING",
					"DEFAULT" => '',
				),
				'CACHE_TIME' => array(
					'DEFAULT' => 3600
				)
			)
		);
	}
	catch (Main\LoaderException $e)
	{
		ShowError($e -> getMessage());
	}
?>