<?php
	if (!CModule::IncludeModule('evk.books')) return false;

	//use Nota\Exalead\SearchQuery;
	//use Nota\Exalead\SearchClient;
	//use Bitrix\NotaExt\Iblock\Section;
	//use Bitrix\NotaExt\Iblock\Element;
	use Evk\Books\SearchQuery;
	use Evk\Books\IBlock\Section;
	use Evk\Books\IBlock\Element;

	class MainSlider extends CBitrixComponent
	{
		public function onPrepareComponentParams($arParams)
		{
			$arParams['CACHE_TIME'] = intval($arParams['CACHE_TIME']);
			$arParams['LIBRARY_ID'] = intval($arParams['LIBRARY_ID']);
			$arParams['IBLOCK_ID'] = intval($arParams['IBLOCK_ID']);
			return $arParams;
		}

		public function getSection()
		{
			$arFilter = array('IBLOCK_ID' => $this->arParams['IBLOCK_ID'], '!UF_SLIDER' => false);

			if($this->arParams['LIBRARY_ID'] > 0)
				$arFilter['UF_LIBRARY'] = $this->arParams['LIBRARY_ID'];

			$arSections = Section::getList(
				$arFilter,
				array(
					'DETAIL_PICTURE',
					'SECTION_PAGE_URL',
					'UF_SHOW_NAME',
					'DESCRIPTION',
					'UF_FONT_SIZE',
					'UF_FONT_COLOR',
					'UF_FONT_SIZE_DESCR',
					'UF_FONT_COLOR_DESCR',
				),
				array('SORT' => 'ASC', 'NAME' => 'ASC'),
				true
			);
			if(!empty($arSections))
			{
				$i = 0;
				foreach($arSections as $arSection)
				{
					if($arSection['ELEMENT_CNT'] <= 0)
						continue;

					if($arSection['DETAIL_PICTURE'] > 0)
						$file = CFile::ResizeImageGet($arSection['DETAIL_PICTURE'], array('width'=>1500, 'height'=>1500), BX_RESIZE_IMAGE_PROPORTIONAL, true);
					else
					{
						$i++;
						if($i > 5) $i = 0;
						$file = array('src' => '/local/images/background/'.$i.'.jpg');
					}
					$obParser = new CTextParser();
					$arSection['TRUNCATE_DESCRIPTION'] = $obParser->html_cut($arSection["DESCRIPTION"], 160);
					$arResult[] = array(
						'ID' 				=> $arSection['ID'],
						'NAME' 				=> $obParser->html_cut($arSection["NAME"], 46),
						'SHOW_NAME' 		=> empty($arSection['UF_SHOW_NAME']) ? false : true,
						'CNT' 				=> $arSection['ELEMENT_CNT'],
						'SECTION_PAGE_URL' 	=> $arSection['SECTION_PAGE_URL'],
						'BACKGROUND' 		=> file_exists($_SERVER['DOCUMENT_ROOT'].$file['src']) ? $file['src'] : false,	
						'DESCRIPTION' 		=> $arSection['TRUNCATE_DESCRIPTION'],
						'UF_FONT_SIZE' 		=> $arSection['UF_FONT_SIZE'],
						'UF_FONT_COLOR' 		=> $arSection['UF_FONT_COLOR'],
						'UF_FONT_SIZE_DESCR' 	=> $arSection['UF_FONT_SIZE_DESCR'],
						'UF_FONT_COLOR_DESCR' 	=> $arSection['UF_FONT_COLOR_DESCR'],
					);
				}
			}
			return $arResult;
		}

		public function getBooks()
		{
			$arResult = array();
			if(empty($this->arResult['SECTIONS']))
				return false;
			foreach($this->arResult['SECTIONS'] as $arSection)
			{
				$arBooks = Element::getList (
					array('IBLOCK_ID' => $this->arParams['IBLOCK_ID'], 'SECTION_ID' => $arSection['ID'], '!PROPERTY_TYPE_IN_SLIDER' => false, '!PROPERTY_BOOK_ID_VALUE' => false), 
					3,
					array('PROPERTY_BOOK_ID'),
					array('PROPERTY_TYPE_IN_SLIDER' => 'ASC')
				);

				if(empty($arBooks['ITEMS']))
					continue;

				foreach($arBooks['ITEMS'] as $arItem)
				{
					$arItem['PROPERTY_BOOK_ID_VALUE'] = trim($arItem['PROPERTY_BOOK_ID_VALUE']);
					if(!empty($arItem['PROPERTY_BOOK_ID_VALUE']))
						$arResult['IDS'][$arSection['ID']][] = $arItem['PROPERTY_BOOK_ID_VALUE'];
				}

				if(empty($arResult['IDS']))
					continue;

				$query = new SearchQuery();
				$result["ITEMS"] = $query->FindBooksSimple(array(), array(
					"ID" => $arResult['IDS'][$arSection['ID']],
				), false, false, array(
					"PROPERTY_FILE",
					"NAME",
				));

				if(!empty($result['ITEMS']))
				{	
				
					$arResult['IDS'][$arSection['ID']] = array();
					$i = 0;
					foreach($result['ITEMS'] as $arElement)
					{
						if($i >= 7)
							break;
						$arResult['ITEMS'][$arElement['id']] = $arElement;
						$arResult['IDS'][$arSection['ID']][] = $arElement['id'];
						$i++;
					}
				}
				else
				{
					unset($arResult['IDS'][$arSection['ID']]);
				}
				unset($result);
			}
			return $arResult;
		}

		public function executeComponent()
		{
			global $CACHE_MANAGER;

			if ($this->StartResultCache(false, array()))
			{
				$CACHE_MANAGER->RegisterTag('IBLOCK_ID_'.$this->arParams['IBLOCK_ID']);
				#$CACHE_MANAGER->RegisterTag('collection_main_slider');

				$this->arResult['SECTIONS'] = $this->getSection();
				$this->arResult['BOOKS'] = $this->getBooks();

				$this->EndResultCache();
			}

			$this->includeComponentTemplate();
		}
	}

?>