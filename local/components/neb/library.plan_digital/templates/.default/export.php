<?
	$APPLICATION->RestartBuffer();

	$fname = basename($APPLICATION->GetCurPage(), ".php");
	// http response splitting defence
	$fname = str_replace(array("\r", "\n"), "", $fname);
	header("Content-Type: application/vnd.ms-excel");
	header("Content-Disposition: filename=".$fname.".xls");

	echo '
	<html>
	<head>
	<title>'.$APPLICATION->GetTitle().'</title>
	<meta http-equiv="Content-Type" content="text/html; charset='.LANG_CHARSET.'">
	<style>
	td {mso-number-format:\@;}
	.number0 {mso-number-format:0;}
	.number2 {mso-number-format:Fixed;}
	</style>
	</head>
	<body>';

	echo "<table border=\"1\">";
	echo "<tr>";
	echo '<td>Автор</td>';
	echo '<td>Название</td>';
	echo '<td>Дата оцифровки</td>';
	echo '<td>Год издания</td>';
	echo '<td>Библиотека</td>';
	echo '<td>В плане на оцифровке</td>';
	echo "</tr>";

	if(!empty($arResult['ITEMS']))
	{
		foreach($arResult['ITEMS'] as $arItem)
		{
			if(empty($arResult['BOOKS'][$arItem['id']]))
				continue;
				
			echo "<tr>";
			echo '<td>'.$arItem['authorbook'].'</td>';
			echo '<td>'.$arItem['title'].'</td>';
			echo '<td>'.$arResult['BOOKS'][$arItem['id']]['DATE_FINISH'].'</td>';
			echo '<td>'.$arItem['publishyear'].'</td>';
			echo '<td>'.$arItem['library'].'</td>';
			echo '<td>';
			if(!empty($arResult['BOOKS_OTHER'][$arItem['id']]))
			{
				foreach($arResult['BOOKS_OTHER'][$arItem['id']] as $arOther)
				{
					echo $arOther['NAME'].' до '.$arOther['DATE'].'<br />';
				}
			}
			echo '</td>';
			echo "</tr>";
		}
	}

	echo "</table>";
	echo '</body></html>';

	exit();
?>