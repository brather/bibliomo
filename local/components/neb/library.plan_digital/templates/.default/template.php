<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>

	<div class="b-mainblock left">
		<div class="b-searchresult">
			<?$APPLICATION->ShowViewContent('menu_top')?>
		</div><!-- /.b-searchresult-->

		<div class="b-breadcrumb">
			Книг в плане на оцифровку: <?=$arResult['NAV_COUNT']?>
		   <!--	<span>Разделы</span>
			<ul class="b-breadcrumblist">
				<li><a href="#">Фонды библиотеки для оцифровки</a></li>
				<li><a href="#" class="right warning">Внимание! У 12 произведений истекает дата оцифровки</a><a href="#" class="current">План оцифровки</a></li>
			</ul>-->
		</div><!-- /.b-breadcrumb-->


		<div class="b-digitizing_actions">
			<a class="button_mode" target="_blank" href="<?=$APPLICATION->GetCurPageParam('print=Y',array('print'));?>">Печать</a>
			<a class="button_mode" target="_blank" href="<?=$APPLICATION->GetCurPageParam('export=Y', array('export'));?>">Экспорт в Excel</a>
			<a class="button_mode add_books_to_digitizing" data-width="775" data-loadtxt="загрузка..." href="<?=$arParams['ADD_PAGE_URL']?>">Добавить книги</a>
			<script>
				$('a.add_books_to_digitizing').click(function(event){
					$('span.digitizing_popup_loading').css('visibility', 'hidden!important');
				})
			</script>
		</div>

		<!--<div class="b-search_field b-search_digital">
			<div class="clearfix">
				<form method="get" action="">
					<input type="text" name="name_q" class="b-search_fieldtb b-text" id="asearch" value="<?=htmlspecialcharsbx($_REQUEST['name_q'])?>" autocomplete="off" data-src="">
					<input type="submit" class="b-search_bth bbox" value="Найти">
				</form>
			</div>	
		</div>-->
		<!-- /.b-search_field-->

		<div class="b-add_digital js_digital">
			<?
				if($arParams['ajax'])
					$APPLICATION->RestartBuffer();
			?>
			<table class="b-usertable tsize">
				<tr>
					<th class="autor_cell"><a>Автор</a></th>
					<th class="namedig_cell"><a>Название / Описание / Есть ли в планах на оцифровку</a></th>
					<th class=""><a <?=SortingExalead("UF_DATE_ADD", false, 'by1', 'order1', 'nav_start', array('by', 'order'))?>>Оцифровать <br />до даты</a></th>
					<th class=""><a>Комментарий</a></th>
					<th class="plan_cell"><a  class="">Удалить <br>из плана </a></th>
				</tr>
				<?
					if(!empty($arResult['BOOKS'])){
						
						foreach($arResult['BOOKS'] as $arItem)
						{
						?>
						<tr class="search-result" id="<?=$arItem['BOOK_ID']?>">
							<td class="pl15"><?=$arResult['ITEMS'][$arItem['BOOK_ID']]['authorbook']?></td>
							<td class="pl15">
								<?if (isset($arResult['ITEMS'][$arItem['BOOK_ID']])): ?>
									<?=$arResult['ITEMS'][$arItem['BOOK_ID']]['title']?>
									<div class="b-digital_act">
										<a href="#" class="b-digital_desc">Описание</a>
										<?
											if(!empty($arResult['BOOKS_OTHER'][$arItem['BOOK_ID']])){
											?>
											<a href="#" class="b-digital_desc b-digital">На оцифровке</a>
											<?
											}
										?>
									</div>
								<?else:?>
								Книга не найдена
								<?endif;?>
							</td>
							<td class="pl15"><?=$arItem['DATE_FINISH']?></td>
							<td class="pl15"><?=$arItem['COMMENT']?></td>
							<td>
								<div class="rel plusico_wrap plan-digitalization minus">
									<div class="plus_ico"></div>
									<div class="b-hint del"><a href="#">Удалить</a> из Плана оцифровки</div>
								</div>
							</td>					
						</tr>
						<tr class="scrolled">
							<td colspan="5">
								<div class="b-infobox rel b-infoboxdescr"  data-link="descr">
									<a href="#" class="close"></a>
									<?
										if(!empty($arResult['ITEMS'][$arItem['BOOK_ID']]['authorbook'])){
										?>							
										<div class="b-infoboxitem">
											<span class="tit iblock">Автор: </span>
											<span class="iblock val"><?=$arResult['ITEMS'][$arItem['BOOK_ID']]['authorbook']?></span>
										</div>
										<?
										}
									?>
									<div class="b-infoboxitem">
										<span class="tit iblock">Заглавие: </span>
										<span class="iblock val"><?=$arResult['ITEMS'][$arItem['BOOK_ID']]['title']?></span>
									</div>
									<?
										if(!empty($arResult['ITEMS'][$arItem['BOOK_ID']]['year'])){
										?>
										<div class="b-infoboxitem">
											<span class="tit iblock">Выходные данные: </span>
											<span class="iblock val"><?=$arResult['ITEMS'][$arItem['BOOK_ID']]['year']?> г.</span>
										</div>
										<?
										}
										if(!empty($arResult['ITEMS'][$arItem['BOOK_ID']]['countpages']))
										{
										?>
										<div class="b-infoboxitem">
											<span class="tit iblock">Физическое описание: </span>
											<span class="iblock val"><?=$arResult['ITEMS'][$arItem['BOOK_ID']]['countpages']?> с.</span>
										</div>
										<?
										}
										if(!empty($arResult['ITEMS'][$arItem['BOOK_ID']]['library']))
										{
										?>
										<div class="b-infoboxitem">
											<span class="tit iblock">Библиотека: </span>
											<span class="iblock val"><?=$arResult['ITEMS'][$arItem['BOOK_ID']]['library']?></span>
										</div>
										<?
										}
									?>			
								</div><!-- /b-infobox -->
								<?
									if(!empty($arResult['BOOKS_OTHER'][$arItem['BOOK_ID']])){
									?>
									<div class="b-infobox rel" data-link="digital">
										<a href="#" class="close"></a>
										<?
											foreach($arResult['BOOKS_OTHER'][$arItem['BOOK_ID']] as $arOther)
											{
											?>
											<div class="b-infoboxitem b-infoboxmain">
												<span class="tit iblock">Оцифровывает</span>
												<span class="iblock val"><strong><?=$arOther['NAME']?></strong><span class="date">до <?=$arOther['DATE']?></span></span>
											</div>

											<?
											}
										?>
										<?
										}
									?>						
								</div><!-- /b-infobox -->
							</td>
						</tr>
						<?
						}
					}
				?>

			</table>
		</div><!-- /.b-add_digital-->
		<?=htmlspecialcharsBack($arResult['STR_NAV'])?>

	</div><!-- /.b-mainblock -->


