<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

	CPageOption::SetOptionString("main", "nav_page_in_session", "N");

	CModule::IncludeModule("highloadblock"); 
	use Bitrix\Highloadblock as HL; 
	use Bitrix\Main\Entity; 
	use Bitrix\Main\Type\DateTime;

	$hlblock = HL\HighloadBlockTable::getById(HIBLOCK_PLAN_DIGIT)->fetch();
	$entity = HL\HighloadBlockTable::compileEntity($hlblock); 
	$entity_data_class = $entity->getDataClass(); 

	$by = trim(htmlspecialcharsEx($_REQUEST['by']));
	$order = trim(htmlspecialcharsEx($_REQUEST['order']));
	$q = trim(htmlspecialcharsEx($_REQUEST['name_q']));

	if(empty($by))
	{
		global $by1, $order1;
		$by1 = trim(htmlspecialcharsEx($by1));
		$order1 = trim(htmlspecialcharsEx($order1));
	}

	$by1 = empty($by1) ? 'UF_DATE_ADD': $by1;
	$order1 = empty($order1) ? 'desc': $order1;	
	$arOrder = array($by1 => $order1);

	$rsData = $entity_data_class::getList(array(
		"runtime" => array(
			"other" => array(
				"data_type" => $entity_data_class,
				'reference' => array('=this.UF_EXALEAD_BOOK' => 'ref.UF_EXALEAD_BOOK'), #, '!=this.UF_LIBRARY' => 'ref.UF_LIBRARY'
			),
			'library' => array(
				"data_type" => "Bitrix\Iblock\ElementTable",
				'reference' => array('=this.PLAN_DIGITIZATION_other_UF_LIBRARY' => 'ref.ID'),
			)
		),
		"select" => array("ID", 'UF_EXALEAD_BOOK', 'UF_DATE_ADD', 'UF_DATE_FINISH', 'UF_LIBRARY', 'UF_COMMENT', 'other.UF_LIBRARY', 'other.UF_EXALEAD_BOOK', 'other.UF_DATE_FINISH', 'library.ID', 'library.NAME', 'other.UF_COMMENT'),
		"order" =>$arOrder,
		"filter" => array('UF_LIBRARY' => $arParams['LIBRARY_ID'])
	));

	
	$arBooks = array();
	$arBooksOther = array();
	$arBooksID = array();
	while($arData = $rsData->Fetch())
	{
		if($arData['UF_LIBRARY'] != $arData['PLAN_DIGITIZATION_other_UF_LIBRARY']){
			$date = '';
			if(!is_object($arData['PLAN_DIGITIZATION_other_UF_DATE_FINISH']))
				$date = $arData['PLAN_DIGITIZATION_other_UF_DATE_FINISH'];
			else
				$date = $arData['PLAN_DIGITIZATION_other_UF_DATE_FINISH']->toString();
			$arBooksOther[$arData['UF_EXALEAD_BOOK']][] = array('NAME' => $arData['PLAN_DIGITIZATION_library_NAME'], 'DATE' => $date);
		}
		$arBook = array(
			'BOOK_ID' => $arData['UF_EXALEAD_BOOK'],
			'DATE_ADD' => $arData['UF_DATE_ADD']->toString(),
			'DATE_FINISH' => $arData['UF_DATE_FINISH']->toString(),
			'COMMENT' => htmlspecialchars($arData['UF_COMMENT']),
		); 	
		
		$arRess[] = $arBook;
	}

	// постраничный вывод
	$arParams['ITEM_COUNT'] = 10;
	$rs = new CDBResult;
	$rs->InitFromArray($arRess);
	$rs->NavStart($arParams['ITEM_COUNT']);
	$arResult["STR_NAV"] = $rs->GetPageNavStringEx($navComponentObject, "", "");
	$arResult['NAV_COUNT'] = (int)$rs->NavRecordCount; 
	while($arRes = $rs->Fetch()){
		
		$arResult['ITEMS'][] = $arRes;
		$arBooksID[] = $arRes['BOOK_ID'];
		$arResult['BOOKS'][$arRes['BOOK_ID']] = $arRes;
	}
	
	if (!CModule::IncludeModule('nota.exalead')) return false;

	use Nota\Exalead\SearchQuery;
	use Nota\Exalead\SearchClient;

	if(!empty($arBooksID))
	{
		$query = new SearchQuery();
		$query->getByArIds($arBooksID);
		if(!empty($q))
		{
			$qp = $query->getParameter('q'); $query->setParam('q', '('.$qp.') AND ("'.$q.'")');
		}

		# сортировка
		if(!empty($by) and !empty($order))
		{
			$query->setParam('s', $order.'('.$by.')');
			unset($by1, $order1);
		}

		/* Уточнение */
		if(!empty($_REQUEST['f_field']))
		{
			$f_field = $_REQUEST['f_field'];
			if(!is_array($f_field))
				$f_field = array($f_field);

			foreach ($f_field as $k => $v)
			{
				$v = urldecode($v);
				$v = trim($v);
				$query->setParam('r', '+'.$v, true);
			}
		}			

		$query->setPageLimit(10000);

		$client = new SearchClient();
		$resEx = $client->getResult($query);
		
		
		foreach ($resEx['ITEMS'] as $item){
		
			$arResult['ITEMS'][$item['id']] = $item;
		}
		/*
		if(!empty($arResult['ITEMS']) and empty($_REQUEST['export']) and empty($_REQUEST['print']))
		{
			/*		
			Сортируем по дате 
			*/
			/*if(empty($by))
			{
				$arItems1 = array();
				foreach($arResult['ITEMS'] as $arItem)
					$arItems1[$arItem['id']] = $arItem;

				$arItems = array();
				foreach($arBooks as $v)
				{
					if(!empty($arItems1[$v['BOOK_ID']]))
						$arItems[] = $arItems1[$v['BOOK_ID']];
				}

				unset($arResult['ITEMS']);
				$arResult['ITEMS'] = $arItems;
			}

			$rs = new CDBResult;
			$rs->InitFromArray($arResult['ITEMS']);
			$rs->NavStart($arParams['ITEM_COUNT']);
			$arResult["STR_NAV"] = $rs->GetPageNavStringEx($navComponentObject, "", "");
			$arResult['NAV_COUNT'] = (int)$rs->NavRecordCount; 

			unset($arResult['ITEMS']);
			while($arRes = $rs->Fetch())
				$arResult['ITEMS'][] = $arRes;
		}
		*/
	}
	
	
	$arResult['BOOKS_OTHER'] = $arBooksOther;

	if(!empty($_REQUEST['dop_filter']) and $_REQUEST['dop_filter'] == 'Y' and $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')
		$APPLICATION->RestartBuffer();

	if($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' and empty($_REQUEST['dop_filter']))
		$arParams['ajax'] = true; 

	$template = '';
	if(!empty($_REQUEST['export']))
		$template = 'export';
	elseif(!empty($_REQUEST['print']))
		$template = 'print';

	$this->IncludeComponentTemplate($template);

	if($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')
		exit();

	$APPLICATION->SetTitle("Фонды библиотеки для оцифровки");
?>