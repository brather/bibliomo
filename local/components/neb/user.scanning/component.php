<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
	use Bitrix\Main\Loader;
	use \Bitrix\Main\Localization\Loc as Loc;
	Loc::loadMessages(__FILE__);

	$nebUser = new nebUser();
	$arUser = $nebUser->getUser();	

	$ID = (int)$_REQUEST['id'];
	$action = htmlspecialcharsEx($_REQUEST['action']);

	$arParams['TEMPLATE'] = '';
	$arParams['AJAX'] = fale;

	if($action == 'show')
	{
		$arParams['AJAX'] = true;
		$arParams['TEMPLATE'] = $action;
	}
	elseif($action == 'pay')
		$arParams['TEMPLATE'] = $action;

	global $by, $order;
	$by = trim(htmlspecialcharsEx($by));
	$order = trim(htmlspecialcharsEx($order));

	$by = empty($by) ? 'STARTDATE': $by;
	$order = empty($order) ? 'desc': $order;	
	$arOrder = array('PROPERTY_'.$by => $order);

	if(!empty($arUser['UF_NUM_ECHB'])){
	
		$arFilter = array(
			'IBLOCK_ID' => IBLOCK_PCC_ORDER, 
			'PROPERTY_USERRFID' => $arUser['UF_NUM_ECHB']
		);

		if(!empty($ID))
			$arFilter['ID'] = $ID;

		$arScan = Bitrix\NotaExt\Iblock\Element::getList(
			$arFilter, 
			false, 
			array(
				'ID',
				'PROPERTY_STARTDATE',
				'PROPERTY_IMAGECOUNT',
				'PROPERTY_FILES',
				'PROPERTY_PRICE',
				'PROPERTY_PAID',
				'PROPERTY_PAID_DATE',
				'skip_other'
			),
			$arOrder
		);
		
	}
	$arResult = array();
	if(!empty($arScan['ITEMS']))
	{
		foreach($arScan['ITEMS'] as $scan)
		{
			$arItem = array();
			$arItem['ID'] 			= $scan['ID'];
			$arItem['DATE'] 		= $scan['PROPERTY_STARTDATE_VALUE'];
			$arItem['IMAGECOUNT'] 	= $scan['PROPERTY_IMAGECOUNT_VALUE'];
			$arItem['PRICE'] 		= $scan['PROPERTY_PRICE_VALUE'];
			$arItem['PAID'] 		= $scan['PROPERTY_PAID_VALUE'];
			$arItem['PAID_DATE'] 	= $scan['PROPERTY_PAID_DATE_VALUE'];
			$arItem['PAY_UP_DATE'] 	= ConvertTimeStamp(AddToTimeStamp(array('DD' => 30), MakeTimeStamp($scan['PROPERTY_STARTDATE_VALUE'])));

			$srFiles = array();
			if(!empty($scan['PROPERTY_FILES_VALUE']))
			{
				foreach($scan['PROPERTY_FILES_VALUE'] as $file)
				{
					$rsFile = CFile::GetByID($file);
					$arFile = $rsFile->Fetch();
					$sizeFormated = CFile::FormatSize($arFile['FILE_SIZE']);

					$ResFile = CFile::ResizeImageGet($file, array('width' => 129, 'height' => 201), BX_RESIZE_IMAGE_PROPORTIONAL, true);
					$fileOriginal = CFile::GetPath($file);

					$srFiles[] = array(
						'src' => $ResFile['src'], 
						'link' => $fileOriginal, 
						'description' => $arFile['DESCRIPTION'], 
						'size' => $sizeFormated
					);
				}
			} 

			$arItem['FILES'] = $srFiles;

			if(!empty($ID))
				$arResult['ITEM'] = $arItem;
			else
				$arResult['ITEMS'][] = $arItem;
		}
	}

	/*	
	Скачать один скан
	*/
	if($action == 'download' and !empty($arResult['ITEM']['ID']))
	{
		$APPLICATION->RestartBuffer();

		$k = (int)$_REQUEST['file'];
		if(empty($arResult['ITEM']['FILES'][$k]) or !file_exists($_SERVER['DOCUMENT_ROOT'].$arResult['ITEM']['FILES'][$k]['link']))
			die('Error file download');

		header('X-Accel-Redirect: ' . $arResult['ITEM']['FILES'][$k]['link']);
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename=' . (empty($arResult['ITEM']['FILES'][$k]['description']) ? basename($arResult['ITEM']['FILES'][$k]['link']) : $arResult['ITEM']['FILES'][$k]['description']));
		exit;

	}
	/*	
	Скачать все сканы одним архивом
	*/
	elseif($action == 'all_download' and !empty($arResult['ITEM']['ID']))
	{
		CheckDirPath($_SERVER["DOCUMENT_ROOT"].'/upload/tmp/scan/'.$arResult['ITEM']['ID']);

		$archivPatch = '/upload/tmp/scan/'.$arResult['ITEM']['ID'].'/files.zip';
		$packarc = CBXArchive::GetArchive($_SERVER["DOCUMENT_ROOT"].$archivPatch, 'ZIP');

		if ($packarc instanceof IBXArchive)
		{
			$packarc->SetOptions(
				array(
					"COMPRESS"			=> true,
					"STEP_TIME"			=> 60,
					"ADD_PATH"			=> false,
					"REMOVE_PATH"		=> $_SERVER["DOCUMENT_ROOT"].'/upload/tmp/scan/'.$arResult['ITEM']['ID'],
					"CHECK_PERMISSIONS" => false
				)
			);

			$arPackFiles = array();
			foreach($arResult['ITEM']['FILES'] as $file)
			{
				$newTmpFile = $_SERVER["DOCUMENT_ROOT"].'/upload/tmp/scan/'.$arResult['ITEM']['ID'].'/'.$file['description'];
				CopyDirFiles($_SERVER["DOCUMENT_ROOT"].$file['link'], $newTmpFile);
				$arPackFiles[] = $newTmpFile;
			}

			$pRes = $packarc->Pack($arPackFiles);

			header('X-Accel-Redirect: ' . $archivPatch);
			header('Content-Type: application/octet-stream');
			header('Content-Disposition: attachment; filename=' .  basename($archivPatch));
			exit;
		}
	}


	if($action == 'pay')
	{

		if(!empty($arResult['ITEM']))
		{
            $productID = $ID;
            // $logF = new Bitrix\NotaExt\WriteLog($_SERVER['DOCUMENT_ROOT'].$componentPath.'/log.txt');

            if (Loader::includeModule("sale") && Loader::includeModule("catalog"))
            {

                $arFilter = array(
                    "ACTIVE" => "Y",
                    "PERSON_TYPE_ID" => 1,
                    "PSA_HAVE_PAYMENT" => "Y"
                );

                $dbPaySystem = CSalePaySystem::GetList(
                    array("SORT" => "ASC", "PSA_NAME" => "ASC"),
                    $arFilter
                );

                $arPaySystem = $dbPaySystem->Fetch();

                // $logF->addLog("arPaySystem " . print_r($arPaySystem, 1));

                $successfulAdd = true;
                if (!Add2BasketByProductID($productID))
                {
                    if ($ex = $APPLICATION->GetException())
                        $strError = $ex->GetString();
                    $successfulAdd = false;
                }

                $arResult["BASE_LANG_CURRENCY"] = CSaleLang::GetLangCurrency(SITE_ID);

                $dbBasketItems = CSaleBasket::GetList(
                    array("ID" => "ASC"),
                    array(
                        "FUSER_ID" => CSaleBasket::GetBasketUserID(),
                        "LID" => SITE_ID,
                        "ORDER_ID" => "NULL",
                    ),
                    false,
                    false,
                    array("ID", "CALLBACK_FUNC", "MODULE", "PRODUCT_ID", "QUANTITY", "DELAY", "CAN_BUY", "PRICE", "WEIGHT", "NAME", "DISCOUNT_PRICE", "VAT_RATE")
                );

                while ($arBasketItems = $dbBasketItems->GetNext()) {
                    if ($arBasketItems["DELAY"] == "N" && $arBasketItems["CAN_BUY"] == "Y") {
                        $arBasketItems["PRICE"] = roundEx($arBasketItems["PRICE"], SALE_VALUE_PRECISION);
                        $arBasketItems["QUANTITY"] = DoubleVal($arBasketItems["QUANTITY"]);
                        $arResult["ORDER_PRICE"] += $arBasketItems["PRICE"] * $arBasketItems["QUANTITY"];
                    }
                }

                // $logF->addLog("arBasketItems " . print_r($arBasketItems, 1));

                $arFields = array(
                    "LID" => SITE_ID,
                    "PERSON_TYPE_ID" => 1,
                    "PAYED" => "N",
                    "CANCELED" => "N",
                    "STATUS_ID" => "N",
                    "PAY_SYSTEM_ID" => $arPaySystem['ID'],
                    "PRICE" => $arResult["ORDER_PRICE"],
                    "CURRENCY" => $arResult["BASE_LANG_CURRENCY"],
                    "USER_ID" => IntVal($USER->GetID()),
                );

                $arResult["ORDER_ID"] = CSaleOrder::Add($arFields);

                if ($arResult["ORDER_ID"] === false) {
                    if ($ex = $APPLICATION->GetException())
                        $strError = $ex->GetString();
                }

                if ($arResult["ORDER_ID"] > 0)
				{
                    CSaleBasket::OrderBasket($arResult["ORDER_ID"], CSaleBasket::GetBasketUserID(), SITE_ID, false);
                    $payMarketParams = unserialize($arPaySystem["PSA_PARAMS"]);
                    $arResult["PaymentType"] = array(

                        "FIELDS" => array(
                            "shopId" => $payMarketParams["SHOP_ID"]["VALUE"],
                            "scid" => $payMarketParams["SCID"]["VALUE"],
                            "sum" => $arResult["ORDER_PRICE"],
                            "customerNumber" => IntVal($USER->GetID()),
                            "orderNumber" => $arResult["ORDER_ID"],
                        ),
                        "TYPES" => array(
                            "PC" => "Оплата из кошелька в Яндекс.Деньгах",
                            "AC" => "Оплата с банковской карты",
                        ),
                    );

					if($payMarketParams["SHOP_KEY"]["IS_TEST"]["VALUE"] == "Y")
					{
						$arResult["PaymentType"]["ACTION"] = "https://demomoney.yandex.ru/eshop.xml";
					}
					else
					{
						$arResult["PaymentType"]["ACTION"] = "https://money.yandex.ru/eshop.xml";
					}
                    CSaleBasket::OrderBasket($arResult["ORDER_ID"], CSaleBasket::GetBasketUserID(), SITE_ID, false);
                    $arParams['AJAX'] = true;
                    $arParams['TEMPLATE'] = "pay_type";
                }
            }
            /*
			CIBlockElement::SetPropertyValuesEx($arResult['ITEM']['ID'], IBLOCK_PCC_ORDER, array('PAID' => Bitrix\NotaExt\Iblock\IblockTools::getYesValueProperty(IBLOCK_PCC_ORDER, 'PAID')));
			CIBlockElement::SetPropertyValuesEx($arResult['ITEM']['ID'], IBLOCK_PCC_ORDER, array('PAID_DATE' => ConvertTimeStamp(time(), 'FULL')));
			global $CACHE_MANAGER;
			$CACHE_MANAGER->ClearByTag("iblock_id_".IBLOCK_PCC_ORDER);
            */
		}
	}

	if($arParams['AJAX'] === true)
		$APPLICATION->RestartBuffer();

	$this->IncludeComponentTemplate($arParams['TEMPLATE']);

	if($arParams['AJAX'] === true)
		exit();

	$APPLICATION->SetTitle(Loc::getMessage('PAGE_TITLE'));

?>
