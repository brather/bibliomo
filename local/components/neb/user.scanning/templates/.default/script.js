$(function(){
    $('body').delegate('a.payorderlinksubmit', 'click', function(){
        $(this).closest('form').submit();
    });
    $('body').delegate('a.payorderlinkcancel', 'click', function(){
        $(this).closest('#panel-formYandexMoney').remove();
    });
    $('a.payorderlink').click(function(){
        var href = $(this).attr("href");
        var params = href.substr(href.indexOf("?")+1,href.length);
        BX.showWait();
        $.post(window.location.href, params, function(data){
            if($('#panel-formYandexMoney').length > 0)
            {
                $('panel-formYandexMoney').replaceWith(data);
            }
            else
            {
                $('body').prepend(data);
            }
            BX.closeWait();
        }, "html");
        return false;
    });
});