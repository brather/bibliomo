<?
	if(empty($arResult['ITEM']['PAID']))
	{
	?>
	<div class="b-uspopup b-paidpopup">
		<div class="b-scanpopup">
			<h2>Предпросмотр неоплаченного заказа</h2>
			<span class="b-popupsubtitle">изображения в низком качестве</span>	
		</div>
		<ul class="b-scanlist">
			<?
				if(!empty($arResult['ITEM']['FILES']))
				{
					foreach($arResult['ITEM']['FILES'] as $arItem)
					{
					?>
					<li>
						<div class="b-scanlist_img">
							<img src="<?=image_to_base64($_SERVER['DOCUMENT_ROOT'].$arItem['src'])?>" alt="<?=$arItem['description']?>">
						</div>
						<div class="clearfix b-scanimg_act">				
							<span class="b-scanlist_imgsize"><?=$arItem['size']?></span>
						</div>
					</li>
					<?
					}
				}
			?>
		</ul>
	</div>
	<?
	}
	else
	{
	?>
	<div class="b-uspopup b-paidpopup">
		<div class="b-scanpopup">
			<h2>просмотр оплаченного заказа</h2>
			<span class="b-popupsubtitle">изображения в высоком качестве</span>
			<?
				if(count($arResult['ITEM']['FILES']) > 1)
				{
				?>
				<a href="<?=$APPLICATION->GetCurPage()?>?id=<?=$arResult['ITEM']['ID']?>&action=all_download" target="_blank" class="formbutton middle">скачать все</a>
				<?
				}
			?> 
		</div>
		<ul class="b-scanlist">

			<?
				if(!empty($arResult['ITEM']['FILES']))
				{
					foreach($arResult['ITEM']['FILES'] as $k => $arItem)
					{
					?>
					<li>
						<div class="b-scanlist_img">
							<img src="<?=$arItem['src']?>" alt="<?=$arItem['description']?>">
						</div>
						<div class="clearfix b-scanimg_act">	
							<a href="<?=$APPLICATION->GetCurPage()?>?id=<?=$arResult['ITEM']['ID']?>&file=<?=$k?>&action=download" target="blank" class="b-scanlist_imgload left">скачать</a>			
							<span class="b-scanlist_imgsize right"><?=$arItem['size']?></span>
						</div>
					</li>
					<?
					}
				}
			?>


		</ul>

	</div>
	<?
	}
?>