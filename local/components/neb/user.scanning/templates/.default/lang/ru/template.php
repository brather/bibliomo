<?
	$MESS['SCANNING_TITLE'] = 'Заказы на сканирование';
	$MESS['SCANNING_ORDER_DATE'] = 'Дата заказа';
	$MESS['SCANNING_NUMBER_IMAGES'] = 'Количество <br />образов';
	$MESS['SCANNING_ORDER_AMOUNT'] = 'Сумма <br />заказа';
	$MESS['SCANNING_PREVIEW'] = 'Предпросмотр';
	$MESS['SCANNING_STATUS'] = 'Статус';
	$MESS['SCANNING_CURRENCY'] = 'руб.';
	$MESS['SCANNING_SEE'] = 'Посмотреть';
	$MESS['SCANNING_PAY'] = 'Оплатить';
	$MESS['SCANNING_PAY_UP'] = 'до';
	$MESS['SCANNING_PAID'] = 'Оплачен';
?>