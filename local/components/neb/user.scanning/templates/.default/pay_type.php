<div id="panel-formYandexMoney">
<form action="<?php echo $arResult["PaymentType"]["ACTION"]?>" method="post">
    <?php foreach($arResult["PaymentType"]["FIELDS"] as $name=>$field):?>
        <input type="hidden" value="<?php echo $field;?>" name="<?php echo $name;?>"/>
    <?php endforeach;?>
    <select name="paymentType">
        <?php foreach($arResult["PaymentType"]["TYPES"] as $name=>$field):?>
            <option value="<?php echo $name;?>"><?php echo $field;?></option>
        <?php endforeach;?>
    </select>
    <a href="javascript:void(null);" class="formbutton middle payorderlinksubmit">Оплатить</a>
    <a href="javascript:void(null);" class="formbutton middle payorderlinkcancel">Закрыть</a>
</form>
</div>