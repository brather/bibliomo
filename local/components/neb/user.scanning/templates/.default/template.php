<?
	if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
?>
<h2><?=GetMessage("SCANNING_TITLE");?></h2>
<table class="b-usertable b-tbscan tsize">
	<tr>
		<th class="exs_cell"><a <?=SortingExalead("STARTDATE")?>><?=GetMessage("SCANNING_ORDER_DATE");?></a></th>
		<th class="num_cell"><a <?=SortingExalead("IMAGECOUNT")?>><?=GetMessage("SCANNING_NUMBER_IMAGES");?></a></th>
		<th class="sum_cell"><a <?=SortingExalead("PRICE")?>><?=GetMessage("SCANNING_ORDER_AMOUNT");?></a></th>
		<th class="view_cell"><a><?=GetMessage("SCANNING_PREVIEW");?></a></th>	
		<th class="status_cell"><a <?=SortingExalead("PAID")?>><?=GetMessage("SCANNING_STATUS");?></a></th>
	</tr>
	<?
		if(!empty($arResult['ITEMS']))
		{
			foreach($arResult['ITEMS'] as $arItem)
			{
			?>
			<tr>
				<td class="pl15"><?=$arItem['DATE']?></td>
				<td class="pl15"><?=$arItem['IMAGECOUNT']?></td>
				<td class="pl15"><?=$arItem['PRICE']?> <?=GetMessage("SCANNING_CURRENCY");?></td>
				<td class="pl15"><span class="nowrap"> <a href="<?=$APPLICATION->GetCurPage()?>?id=<?=$arItem['ID']?>&action=show" data-overlaybg="dark"  data-width="940" class=" popup_opener ajax_opener closein "><?=GetMessage("SCANNING_SEE");?></a></span></td>
				<td class="pl15"><?
						if(empty($arItem['PAID']))
                        {
							?><a href="<?=$APPLICATION->GetCurPage()?>?id=<?=$arItem['ID']?>&action=pay" class="formbutton middle payorderlink"><?=GetMessage("SCANNING_PAY");?></a>&nbsp;<span class="b-paytime"><?=GetMessage("SCANNING_PAY_UP");?>&nbsp;<?=$arItem['PAY_UP_DATE']?></span><?
                        }
						else
						{
						?><span class="b-paid"><?=GetMessage("SCANNING_PAID");?></span><span class="b-paytime"><?=$arItem['PAID_DATE']?></span><?
						}
					?></td>
			</tr>
			<?
			}
		}
	?>
</table>