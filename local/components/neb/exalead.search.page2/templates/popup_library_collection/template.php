<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
	use \Bitrix\Main\Localization\Loc as Loc;
	Loc::loadMessages(__FILE__);
?>

<form action="<?=$arParams['FORM_ACTION']?>" target="_top" method="post" class="b-form b-form_common b-addbookform">
	<input type="hidden" name="action" value="addBook">
	<input type="hidden" name="COLLECTION_ID" value="<?=$arParams['ID_COLLECTION']?>">
	<?=bitrix_sessid_post()?>
	<div class="fieldrow">
		<table class="b-result-doc">
			<?foreach($arResult["ITEMS"] as $item):?>
			<tr class="b-result-docitem">

				<td>
					<a class="b_bookpopular_photo" href="<?=$item['DETAIL_PAGE_URL']?>" target="_blank"><img class="loadingimg" data-title="<?=$item['title']?>" data-autor="<?=$item['authorbook']?>" alt="" src="<?=$item['IMAGE_URL']?>"></a>
				</td>
				<td>
					<h2><span class="num"><?=$item['NUM_ITEM']?>.</span><a href="<?=$item['DETAIL_PAGE_URL']?>" target="_blank"><?=$item['title']?></a></h2>
					<ul class="b-resultbook-info">
						<?if(!empty($item['authorbook'])):?><li><span>Автор:</span> <?=$item['authorbook']?></li><?endif;?>
						<?if(!empty($item['publishyear'])):?><li><span>Год публикации:</span> <?=$item['publishyear']?></li><?endif;?>
						<?if(!empty($item['countpages'])):?><li><span>Количество страниц:</span> <?=$item['countpages']?></li><?endif;?>
						<?if(!empty($item['publisher'])):?><li><span>Издательство:</span> <?=$item['publisher']?></li><?endif;?>
						<?if(!empty($item['library'])):?><li><span>	<em>Источник:</em> <?=$item['library']?></li><?endif;?>
					</ul>
				</td>
				<td>
					<div class="b-radio">
						<input type="radio" name="BOOK_NUM_ID" value="<?=$item['NUM_ITEM']?>" class="radio" id="libraryCollectionBooks">
						<input type="hidden" name="BOOK_ID_<?=$item['NUM_ITEM']?>" value="<?=$item['id']?>">
						<input type="hidden" name="BOOK_TITLE_<?=$item['NUM_ITEM']?>" value="<?=$item['title']?>">
						<input type="hidden" name="BOOK_AUTHOR_<?=$item['NUM_ITEM']?>" value="<?=$item['authorbook']?>">
						<input type="hidden" name="BOOK_YEAR_<?=$item['NUM_ITEM']?>" value="<?=$item['publishyear']?>">
					</div>
				</td>

			</tr>
			<?endforeach;?>
		</table><!-- /.b-result-doc -->
	</div>
	<?=$arResult['STR_NAV']?>
	<div class="fieldrow nowrap fieldrowaction">
		<div class="fieldcell ">
			<div class="field clearfix">
				<a href="#" class="formbutton gray right btrefuse">Отказаться</a>
				<button class="formbutton left" value="1" type="submit">Добавить произведение</button>
			</div>
		</div>
	</div>
</form>