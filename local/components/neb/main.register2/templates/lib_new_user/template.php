<?
	if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
		die();

	/*	пользователь зарегистрирован */
	if(!empty($arResult['VALUES']['USER_ID']))
	{
	?>
	<div class="b-useraddstep">
		<a href="<?=$APPLICATION->GetCurPage();?>" class="b-adduserimg right"><img src="<?=MARKUP?>i/adduser.png" alt=""></a>
		<h2 class="mode">Добавление нового читателя. Шаг 2 из 2</h2>
		<form action="" class="b-form b-form_common b-readerform">
			<div class="b-form_header"><span class="iblock">Договор</span></div>
			<div class="fieldrow nowrap fieldrowaction b-printdoc">
				<div class="fieldcell w700">
					<div class="field clearfix">
						<a href="<?=$APPLICATION->GetCurPage();?>?action=print&uid=<?=$arResult['VALUES']['USER_ID']?>" target="_blank"><button class="formbutton" value="1" type="button" >Печать договора</button></a>
						<a href="<?=$APPLICATION->GetCurPage();?>?action=save&uid=<?=$arResult['VALUES']['USER_ID']?>" target="_blank" class="blinkbutton">Сохранить договор</a>
						<a href="<?=$APPLICATION->GetCurPage();?>?action=send&uid=<?=$arResult['VALUES']['USER_ID']?>" target="_blank" class="blinkbutton">Отправить договор</a>
					</div>
				</div>
			</div>
			<div class="b-form_header"><span class="iblock">Читательский билет</span></div>
			<div class="b-userticket"><?=$arResult['VALUES']['UF_NUM_ECHB']?></div>
			<div class="b-form_header"><span class="iblock">Текст договора</span></div>
            <textarea disabled="disabled" rows="30">
                <?$APPLICATION->IncludeFile($this->__folder . '/agreement.php', Array(), Array('SHOW_BORDER' => false, 'MODE' => 'text'))?>
            </textarea>
		</form>
	</div>
	<?
		return false;
	}	

	if (count($arResult["ERRORS"]) > 0){
		foreach ($arResult["ERRORS"] as $key => $error)
			if (intval($key) == 0 && $key !== 0) 
				$arResult["ERRORS"][$key] = str_replace("#FIELD_NAME#", "&quot;".GetMessage("REGISTER_FIELD_".$key)."&quot;", $error);

			ShowError(implode("<br />", $arResult["ERRORS"]));
	}
?>
<div class="b-useraddstep">
	<a href="<?=$APPLICATION->GetCurPage();?>" class="b-adduserimg right"><img src="<?=MARKUP?>i/adduser.png" alt=""></a>
	<h2 class="mode">Добавление нового читателя. Шаг 1 из 2</h2>
	<script type="text/javascript">
		function setLogin(){
			$('#inpLOGIN').val($('#settings04').val());
			$('input#inpPERSONAL_NOTES').val($('input#settings05').val());
		}
	</script>
	<form method="post" action="<?=POST_FORM_ACTION_URI?>" name="regform" enctype="multipart/form-data" class="b-form b-form_common b-readerform" onsubmit="return setLogin();">

		<input type="hidden" name="UF_LIBRARY" value="<?=$arParams['LIBRARY_ID']?>"/>
		<input type="hidden" name="UF_STATUS" value="4"/>

		<input type="hidden" name="REGISTER[LOGIN]" value="" id="inpLOGIN"/>
		<input type="hidden" name="REGISTER[PERSONAL_NOTES]" value="" id="inpPERSONAL_NOTES"/>
		<?
			if(empty($arResult["VALUES"]['PASSWORD']))
			{
				$arResult["VALUES"]['PASSWORD'] = randString(7);
			}
		?>
		<input type="hidden" name="REGISTER[PASSWORD]" value="<?=$arResult["VALUES"]['PASSWORD']?>" />
		<input type="hidden" name="REGISTER[CONFIRM_PASSWORD]" value="<?=$arResult["VALUES"]['PASSWORD']?>"/>
		<input type="hidden" name="REGISTER[WORK_COMPANY]" value="<?=$arResult["VALUES"]['PASSWORD']?>"/>

		<div class="b-form_header"><span class="iblock">Паспортные данные</span></div>
		<div class="fieldrow nowrap">
			<div class="fieldcell iblock">
				<em class="hint">*</em>
				<label for="settings01">Фамилия</label>
				<div class="field validate">
					<input type="text" data-validate="fio" data-required="required" value="<?=$arResult["VALUES"]['LAST_NAME']?>" id="settings01" data-maxlength="30" data-minlength="2" name="REGISTER[LAST_NAME]" class="input">		
					<em class="error required">Поле обязательно для заполнения</em>		
					<em class="error validate">Неверный формат</em>	
					<em class="error maxlength">Более 30 символов</em>										
				</div>
			</div>
		</div>
		<div class="fieldrow nowrap">
			<div class="fieldcell iblock">
				<em class="hint">*</em>
				<label for="settings02">Имя</label>
				<div class="field validate">
					<input type="text" data-validate="fio" data-required="required" value="<?=$arResult["VALUES"]['NAME']?>" id="settings02" data-maxlength="30" data-minlength="2" name="REGISTER[NAME]" class="input" > 
					<em class="error required">Поле обязательно для заполнения</em>		
					<em class="error validate">Неверный формат</em>	
					<em class="error maxlength">Более 30 символов</em>
				</div>
			</div>
		</div>
		<div class="fieldrow nowrap">
			<div class="fieldcell iblock">
				<label for="settings22">Отчество</label>
				<div class="field validate">
					<input type="text" data-validate="fio" data-required="false" value="<?=$arResult["VALUES"]['SECOND_NAME']?>" id="settings22" data-maxlength="30" data-minlength="2" name="REGISTER[SECOND_NAME]" class="input" >			
					<em class="error required">Поле обязательно для заполнения</em>		
					<em class="error validate">Неверный формат</em>	
					<em class="error maxlength">Более 30 символов</em>							
				</div>
			</div>
		</div>
		<div class="fieldrow nowrap">
			<div class="fieldcell iblock">
				<em class="hint">*</em>
				<label for="settings06">Дата рождения</label>
				<div class="field validate iblock seldate">
					<em class="error dateformat">Неверный формат даты</em>
					<em class="error required">Заполните</em>
					<em class="error yearsrestrict">Вам должно быть<br>не меньше 12 лет</em>
					<input type="hidden" class="realseldate" data-required="true" value="<?=$arResult["VALUES"]['PERSONAL_BIRTHDAY']?>" id="settings06" name="REGISTER[PERSONAL_BIRTHDAY]">
					<select name="birthday" class="js_select w102 sel_day"  data-required="true">
						<option value="">день</option>
						<?
							for($i=1;$i<=31;$i++)
							{
							?>
							<option value="<?=$i?>"><?=$i?></option>
							<?
							}
						?>
					</select>

					<select name="birthmonth" class="js_select w165 sel_month"  data-required="true" >
						<option value="">месяц</option>
						<?
							for($i=1;$i<=12;$i++)
							{
							?>
							<option value="<?=$i?>"><?=FormatDate('f', MakeTimeStamp('01.'.$i.'.'.date('Y')))?></option>
							<?
							}
						?>
					</select>

					<select name="birthyear" class="js_select w102 sel_year"  data-required="true">
						<option value="">год</option>
						<?
							for($i=(date('Y')-12);$i>=(date('Y')-95);$i--)
							{
							?>
							<option value="<?=$i?>"><?=$i?></option>
							<?
							}
						?>
					</select>
				</div>									
			</div>
		</div>
		<?/*?>
			<div class="fieldrow nowrap">
			<div class="fieldcell iblock mt10">
			<em class="hint">*</em>
			<label for="settings12">Пол</label>
			<div class="field validate">
			<span class="b-radio">
			<input type="radio" name="REGISTER[PERSONAL_GENDER]" class="radio" id="rb1" value="M"<?=($arResult["VALUES"]['PERSONAL_GENDER'] == "M" or empty($arResult["VALUES"]['PERSONAL_GENDER'])) ? " checked=\"checked\"" : ""?>>
			<label for="rb1">мужской</label>
			</span>	
			<span class="b-radio">
			<input type="radio" name="REGISTER[PERSONAL_GENDER]" class="radio" id="rb2" value="F"<?=$arResult["VALUES"]['PERSONAL_GENDER'] == "F" ? " checked=\"checked\"" : ""?>>
			<label for="rb2">женский</label>
			</span>							
			</div>
			</div>
			</div>

			<div class="fieldrow nowrap">
			<div class="fieldcell iblock">
			<em class="hint">*</em>
			<label for="settings04">Серия и номер паспорта</label>
			<div class="field validate">
			<input type="text" class="input tbpasport" name="UF_PASSPORT_NUMBER" data-minlength="2" data-maxlength="30" data-required="true" id="settings04" value="<?=htmlspecialcharsEx($_REQUEST['UF_PASSPORT_NUMBER'])?>">	
			<em class="error required">Поле обязательно для заполнения</em>	
			<em class="error validate ">Поле заполнено неверно</em>									
			</div>
			</div>
			</div>
		<?*/?>
		<div class="fieldrow nowrap">
			<div class="fieldcell iblock">
				<em class="hint">*</em>
				<label for="settings011">Серия и номер паспорта</label>
				<div class="field validate">
					<em class="error required">Поле обязательно для заполнения</em>
					<em class="error validate ">Поле заполнено неверно</em>		
					<div class="field iblock"><input type="text"  data-required="required" value="<?=htmlspecialcharsEx($_REQUEST['UF_PASSPORT_SERIES'])?>" id="settings011" name="UF_PASSPORT_SERIES" class="input w110"></div>					  
					<div class="field iblock"><input type="text"  data-required="required" value="<?=htmlspecialcharsEx($_REQUEST['UF_PASSPORT_NUMBER'])?>" id="settings022" name="UF_PASSPORT_NUMBER" class="input w190"></div>						  
				</div>
			</div>
		</div>

		<?/*?>
			<div class="fieldrow nowrap">
			<div class="fieldcell iblock mt10 w700">
			<em class="hint">*</em>
			<label for="settings05">Кем выдан / как в паспорте</label>
			<div class="field validate">
			<input type="text" class="input" name="UF_ISSUED" data-minlength="2" data-maxlength="30" data-required="true"  id="settings05" value="<?=htmlspecialcharsEx($_REQUEST['UF_ISSUED'])?>">
			<em class="error required">Поле обязательно для заполнения</em>	
			<em class="error validate ">Поле заполнено неверно</em>									
			</div>
			</div>
			</div>
		<?*/?>
		<div class="b-form_header"><span class="iblock">Сфера специализации</span></div>

		<div class="fieldrow nowrap">
			<div class="fieldcell iblock">
				<em class="hint">*</em>
				<label for="settings10">Место работы/учебы</label>
				<div class="field validate">
					<em class="error required">Заполните</em>
					<input type="text" value="<?=$arResult["VALUES"]['WORK_COMPANY']?>" id="settings10" data-maxlength="30" data-minlength="2" data-required="true" name="REGISTER[WORK_COMPANY]" class="input" >
				</div>
			</div>
		</div>
		<div class="fieldrow nowrap">
			<div class="fieldcell iblock">
				<em class="hint">*</em>
				<label for="settings08">Отрасль знаний</label>
				<div class="field validate iblock">
					<em class="error required">Заполните</em>
					<select name="UF_BRANCH_KNOWLEDGE" id="settings08"  class="js_select w370"  data-required="true">
						<option value="">выберете</option>
						<?
							$arUF_BRANCH_KNOWLEDGE = nebUser::getFieldEnum(array('USER_FIELD_NAME' => 'UF_BRANCH_KNOWLEDGE'));
							if(!empty($arUF_BRANCH_KNOWLEDGE))
							{
								foreach($arUF_BRANCH_KNOWLEDGE as $arItem)
								{
								?>
								<option value="<?=$arItem['ID']?>"><?=$arItem['VALUE']?></option>
								<?
								}
							}
						?>
					</select>
				</div>				
			</div> 
		</div>
		<div class="fieldrow nowrap">
			<div class="fieldcell iblock">
				<em class="hint">*</em>
				<label for="settings09">Образование. учёная степень</label>
				<div class="field validate iblock">
					<em class="error required">Заполните</em>
					<select name="UF_EDUCATION" id="settings09"  class="js_select w370"  data-required="true">
						<option value="">выберете</option>
						<?
							$arUF_EDUCATION = nebUser::getFieldEnum(array('USER_FIELD_NAME' => 'UF_EDUCATION'));
							if(!empty($arUF_EDUCATION))
							{
								foreach($arUF_EDUCATION as $arItem)
								{
								?>
								<option <?=$arResult["VALUES"]['UF_EDUCATION'] == $arItem['ID'] ? 'selected="selected"':''?> value="<?=$arItem['ID']?>"><?=$arItem['VALUE']?></option>
								<?
								}
							}
						?>
					</select>
				</div>				
			</div> 
		</div>
		<div class="fieldrow nowrap">
			<div class="fieldcell iblock">
				<em class="hint">*</em>
				<label for="settings08">Гражданство</label>
				<div class="field validate iblock">
					<em class="error required">Заполните</em>
					<select name="UF_CITIZENSHIP" id="settings13"  class="js_select w370"  data-required="true">
						<?
						$arUF_CITIZENSHIP = nebUser::getFieldEnum(array('USER_FIELD_NAME' => 'UF_CITIZENSHIP'),array("XML_ID"=>"ASC"));
						if(!empty($arUF_CITIZENSHIP))
						{
							foreach($arUF_CITIZENSHIP as $arItem)
							{
								if($arItem['DEF']=="Y")
								{
									$ruCitizenship = $arItem['ID'];
								}
								?>
								<option value="<?=$arItem['ID']?>"<?=($arItem['DEF']=="Y")?' selected="selected"':'';?>><?=$arItem['VALUE']?></option>
							<?php
							}
						}
						?>
					</select>
					<script type="text/javascript">
						$(function(){
							var copyEducation = $('select[name="UF_EDUCATION"]').clone().removeClass("custom");
							function correctEducationField()
							{
								var cit = $('select[name="UF_CITIZENSHIP"] option:selected').val();
								var parentEducation = $('select[name="UF_EDUCATION"]').closest("div.field");
								parentEducation.children("span").remove();
								parentEducation.append(copyEducation.clone());
								$('#UF_EDUCATIONopts').remove();
								if(cit == <?php echo $ruCitizenship;?>)
								{
									$('select[name="UF_EDUCATION"] option').filter('[VALUE="33"],[VALUE="34"],[VALUE="35"]').remove();
								}
								else
								{
									$('select[name="UF_EDUCATION"] option').not('[VALUE=""],[VALUE="33"],[VALUE="34"],[VALUE="35"]').remove();
								}
								$('select[name="UF_EDUCATION"]').selectReplace();
							}
							correctEducationField();
							$('select[name="UF_CITIZENSHIP"]').change(function(){
								correctEducationField();
							});
						});
					</script>
				</div>
			</div>
		</div>

		<div class="b-form_header">Вход на сайт</div>

		<div class="fieldrow nowrap">
			<div class="fieldcell iblock">
				<em class="hint">*</em>
				<label for="settings04">Электронная почта </label>
				<div class="field validate">
					<em class="error required">Заполните</em>						
					<em class="error validate">Некорректный e-mail</em>
					<input type="text" class="input" name="REGISTER[EMAIL]" data-minlength="2" data-maxlength="30" id="settings04" data-validate="email" value="<?=$arResult["VALUES"]['EMAIL']?>" data-required="true">									
				</div>
			</div>
		</div>
		<div class="fieldrow nowrap">
			<div class="fieldcell iblock">
				<em class="hint">*</em>
				<div class="passecurity">
					<div class="passecurity_lb">Защищенность пароля</div>

				</div>
				<label for="settings05"><span>не менее 6 символов</span>Пароль</label>
				<div class="field validate">
					<input type="password" data-validate="password" data-required="true" value="" id="settings05" data-maxlength="30" data-minlength="6" name="REGISTER[PASSWORD]" class="pass_status input">
					<em class="error">Заполните</em>
				</div>
			</div>
		</div>
		<div class="fieldrow nowrap">
			<div class="fieldcell iblock">
				<em class="hint">*</em>
				<label for="settings55">Подтвердить пароль</label>
				<div class="field validate">
					<em class="error required">Заполните</em>	
					<em class="error validate">Пароль не совпадает с введенным ранее</em>
					<input data-identity="#settings05" type="password" data-required="true" value="" id="settings55" data-maxlength="30" data-minlength="2" name="REGISTER[CONFIRM_PASSWORD]" class="input" data-validate="password">
				</div>
			</div>
		</div>
		<div class="fieldrow nowrap">
			<div class="fieldcell iblock">
				<label for="settings11">Мобильный телефон</label>
				<div class="field validate">
					<input type="text" class="input maskphone" name="REGISTER[PERSONAL_MOBILE]"  id="settings11"  value="<?=$arResult["VALUES"]['PERSONAL_MOBILE']?>">		
					<span class="phone_note">для смс-уведомлений о статусе Ваших заявок  на сайте РГБ</span>
				</div>
			</div>
		</div>



		<div class="b-form_header">Адрес регистрации</div>
		<div class="fieldrow nowrap">
			<div class="fieldcell iblock w143">
				<em class="hint">*</em>
				<label for="settings06">Индекс</label>
				<div class="field validate">	
					<input type="text" class="input" name="REGISTER[PERSONAL_ZIP]" data-minlength="2" data-maxlength="30"  data-required="true" id="settings06" value="<?=$arResult["VALUES"]['PERSONAL_ZIP']?>">									
				</div>
			</div>
			<div class="fieldcell iblock w285">
				<em class="hint">*</em>
				<label for="settings07">Город</label>
				<div class="field validate">
					<em class="error required">Заполните</em>	
					<input type="text" class="input" name="REGISTER[PERSONAL_CITY]" data-minlength="2" data-maxlength="30" data-required="true" id="settings07" value="<?=$arResult["VALUES"]['PERSONAL_CITY']?>">									
				</div>
			</div>
		</div>
		<div class="fieldrow nowrap">
			<div class="fieldcell iblock mt10 w445">
				<em class="hint">*</em>
				<label for="settings08">Улица</label>
				<div class="field validate">
					<input type="text" class="input" name="REGISTER[PERSONAL_STREET]" data-minlength="2" data-maxlength="30" data-required="true" id="settings08" value="<?=$arResult["VALUES"]['PERSONAL_STREET']?>">									
				</div>
			</div>
			<div class="fieldcell iblock mt10 w66">
				<em class="hint">*</em>
				<label for="settings09">Дом</label>
				<div class="field validate">
					<em class="error required">Заполните</em>	
					<input type="text" class="input" name="UF_CORPUS" data-minlength="2" data-maxlength="30" data-required="true" id="settings09" value="<?=htmlspecialcharsEx($_REQUEST['UF_CORPUS'])?>">									
				</div>
			</div>
			<div class="fieldcell iblock mt10 w66">

				<label for="settings10">Строение</label>
				<div class="field validate">
					<input type="text" class="input" name="UF_STRUCTURE" data-minlength="2" data-maxlength="30"  id="settings10" value="<?=htmlspecialcharsEx($_REQUEST['UF_STRUCTURE'])?>">									
				</div>
			</div>
			<div class="fieldcell iblock w66 mt10">

				<label for="settings11">Квартира</label>
				<div class="field validate">
					<input type="text" class="input" name="UF_FLAT" data-minlength="2" data-maxlength="30"  id="settings11" value="<?=htmlspecialcharsEx($_REQUEST['UF_FLAT'])?>">									
				</div>
			</div>
		</div>

		<div class="b-form_header">Адрес проживания</div>
		<div class="fieldrow nowrap">
			<div class="fieldcell iblock w143 inly">
				<em class="hint">*</em>
				<label for="settings06">Индекс</label>
				<div class="field validate">	
					<input type="text" class="input" name="REGISTER[WORK_ZIP]" data-minlength="2" data-maxlength="30" data-required="true" id="settings06" value="<?=$arResult["VALUES"]['WORK_ZIP']?>">									
				</div>
			</div>
			<div class="fieldcell iblock w285 inly">
				<em class="hint">*</em>
				<label for="settings07">Город</label>
				<div class="field validate">
					<input type="text" class="input" name="REGISTER[WORK_CITY]" data-minlength="2" data-maxlength="30" data-required="true" id="settings07" value="<?=$arResult["VALUES"]['WORK_CITY']?>">									
				</div>
			</div>
			<div class="fieldcell iblock w200">
				<div class="checkwrapper">
					<input class="checkbox addrindently" type="addr" name="UF_PLACE_REGISTR" value="1" id="cb3"><label for="cb3" class="black">по месту регистрации</label>
				</div>
			</div>
		</div>
		<div class="fieldrow nowrap">
			<div class="fieldcell iblock mt10 w445 inly">
				<em class="hint">*</em>
				<label for="settings08">Улица</label>
				<div class="field validate">
					<input type="text" class="input" name="REGISTER[WORK_STREET]" data-minlength="2" data-maxlength="30"  data-required="true" id="settings08" value="<?=$arResult["VALUES"]['WORK_STREET']?>">									
				</div>
			</div>
			<div class="fieldcell iblock mt10 w66 inly">
				<em class="hint">*</em>
				<label for="settings09">Дом</label>
				<div class="field validate">	
					<em class="error required">Заполните</em>	
					<input type="text" class="input" name="UF_HOUSE2" data-minlength="2" data-maxlength="30"  data-required="true" id="settings09" value="<?=$arResult["VALUES"]['UF_HOUSE2']?>">									
				</div>
			</div>
			<div class="fieldcell iblock mt10 w66 inly">
				<label for="settings10">Строение</label>
				<div class="field validate">
					<input type="text" class="input" name="UF_STRUCTURE2" data-minlength="2" data-maxlength="30"  id="settings10" value="<?=$arResult["VALUES"]['UF_STRUCTURE2']?>">									
				</div>
			</div>
			<div class="fieldcell iblock w66 mt10 inly">
				<label for="settings11">Квартира</label>
				<div class="field validate">
					<input type="text" class="input" name="UF_FLAT2" data-minlength="2" data-maxlength="30"  id="settings11" value="<?=$arResult["VALUES"]['UF_FLAT2']?>">									
				</div>
			</div>
		</div>

		<hr>

		<?$APPLICATION->IncludeComponent(
				"notaext:plupload", 
				"scan_passport1_reg", 
				array(
					"MAX_FILE_SIZE" => "24",
					"FILE_TYPES" => "jpg,jpeg,png",
					"DIR" => "tmp_register",
					"FILES_FIELD_NAME" => "profile_file",
					"MULTI_SELECTION" => "N",
					"CLEANUP_DIR" => "Y",
					"UPLOAD_AUTO_START" => "Y",
					"RESIZE_IMAGES" => "Y",
					"RESIZE_WIDTH" => "4000",
					"RESIZE_HEIGHT" => "4000",
					"RESIZE_CROP" => "Y",
					"RESIZE_QUALITY" => "98",
					"UNIQUE_NAMES" => "Y",
				),
				false
			);?>


		<?$APPLICATION->IncludeComponent(
				"notaext:plupload", 
				"scan_passport2_reg", 
				array(
					"MAX_FILE_SIZE" => "24",
					"FILE_TYPES" => "jpg,jpeg,png",
					"DIR" => "tmp_register",
					"FILES_FIELD_NAME" => "profile_file",
					"MULTI_SELECTION" => "N",
					"CLEANUP_DIR" => "Y",
					"UPLOAD_AUTO_START" => "Y",
					"RESIZE_IMAGES" => "Y",
					"RESIZE_WIDTH" => "4000",
					"RESIZE_HEIGHT" => "4000",
					"RESIZE_CROP" => "Y",
					"RESIZE_QUALITY" => "98",
					"UNIQUE_NAMES" => "Y",
				),
				false
			);?>


		<div class="fieldrow nowrap fieldrowaction mt35">
			<div class="fieldcell w700">
				<div class="field clearfix">
					<button class="formbutton" value="1" type="submit" name="register_submit_button">Отправить данные</button>
					<span class="bttext">в реестр читательских билетов</span>

				</div>
			</div>
		</div>

	</form>
</div>