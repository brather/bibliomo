<?
	if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

	$arComponentParameters = array(
		"PARAMETERS" => array(
			"SEF_MODE" => array(
				"list" => array(
					"NAME" => "LIST PAGE",
					"DEFAULT" => "/",
					"VARIABLES" => array()
				),
				"detail" => array(
					"NAME" => "DETAIL PAGE",
					"DEFAULT" => "#CODE#/",
					"VARIABLES" => array("CODE")
				),
			),
		),
	);
?>