<?
	if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

	$arComponentDescription = array(
		"NAME" => "Библиотеки",
		"DESCRIPTION" => "",
		"ICON" => "/images/icon.gif",
		"SORT" => 10,
		"CACHE_PATH" => "Y",
		"PATH" => array(
			"ID" => "library",
			"CHILD" => array(
				"ID" => "library",
				"NAME" => "Библиотеки",
				"SORT" => 30,
			)
		),
		"COMPLEX" => "Y",
	);

?>