<?
	if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

	if(!CModule::IncludeModule("iblock"))
		return;

	$arIBlockType = CIBlockParameters::GetIBlockTypes();

	$arIBlock=array();
	$rsIBlock = CIBlock::GetList(Array("sort" => "asc"), Array("TYPE" => $arCurrentValues["IBLOCK_TYPE"], "ACTIVE"=>"Y"));
	while($arr=$rsIBlock->Fetch())
	{
		$arIBlock[$arr["ID"]] = "[".$arr["ID"]."] ".$arr["NAME"];
	}

	$arComponentParameters = array(
		"GROUPS" => array(
		),
		"PARAMETERS" => array(
			"IBLOCK_TYPE" => array(
				"PARENT" => "BASE",
				"NAME" => 'IBLOCK_TYPE',
				"TYPE" => "LIST",
				"VALUES" => $arIBlockType,
				"REFRESH" => "Y",
			),
			"IBLOCK_ID" => array(
				"PARENT" => "BASE",
				"NAME" => 'IBLOCK_ID',
				"TYPE" => "LIST",
				"VALUES" => $arIBlock,
				"REFRESH" => "Y",
				"ADDITIONAL_VALUES" => "Y",
			),
			"SEF_MODE" => array(
				"list" => array(
					"NAME" => "LIST PAGE",
					"DEFAULT" => "/",
					"VARIABLES" => array()
				),
				"detail" => array(
					"NAME" => "DETAIL PAGE",
					"DEFAULT" => "#CODE#/",
					"VARIABLES" => array("CODE")
				),
				"funds" => array(
					"NAME" => "FUNDS PAGE",
					"DEFAULT" => "#CODE#/funds/",
					"VARIABLES" => array("CODE")
				),
				"collections" => array(
					"NAME" => "COLLECTIONS PAGE",
					"DEFAULT" => "#CODE#/collections/",
					"VARIABLES" => array("CODE")
				),
				"news_detail" => array(
					"NAME" => "NEWS DETAIL PAGE",
					"DEFAULT" => "#CODE#/news-#ELEMENT_ID#-#ELEMENT_CODE#/",
					"VARIABLES" => array("CODE")
				),
			),
		),
	);
?>