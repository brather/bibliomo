<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?
	$arMenu = array();
	foreach($arResult['URL_TEMPLATES'] as $page => $url){
		if($page == 'list')
			continue;
		$arMenu[$page] = $arResult['FOLDER'].str_replace('#CODE#', $arResult['VARIABLES']['CODE'], $url);	
	}
?>
<nav class="b-commonnav noborder">
	<a href="<?=$arMenu['detail']?>" <?=$this->__page == 'detail' ? 'class="current"' : ''?>>О библиотеке</a>
	<?
		if(nebLibrary::showPublication($arResult['VARIABLES']['CODE']) === true){
		?>
		<a href="<?=$arMenu['funds']?>" id="menu_funds" <?=$this->__page == 'funds' ? 'class="current"' : ''?>>фонды</a>
		<?
		}
		if(nebLibrary::showCollection($arResult['VARIABLES']['CODE']) === true){
		?>
		<a href="<?=$arMenu['collections']?>" id="menu_collections" <?=$this->__page == 'collections' ? 'class="current"' : ''?>>коллекции</a>
		<?
		}
	?>
</nav>