<?
	if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
	$APPLICATION->IncludeComponent(
		"neb:library.list",
		"",
		Array(
			"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"CACHE_TIME" => $arParams["CACHE_TIME"],
			"ROWS_PER_PAGE" => "20",
			"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]['detail'],
		),
		$component
	);
?>