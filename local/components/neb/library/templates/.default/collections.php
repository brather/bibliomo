<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<section class="innersection innerwrapper clearfix searchempty ">
	<?
		include_once('menu.php');

		$library_code = trim($arResult['VARIABLES']['CODE']);

		$COLLECTIONS_IBLOCK_ID = Bitrix\NotaExt\Iblock\IblockTools::getIBlockId('kollektsii_6');
		$arLibrary = Bitrix\NotaExt\Iblock\Element::getList(array('IBLOCK_ID' => $arParams['IBLOCK_ID'], 'CODE' => $library_code), 1, array('ID', 'skip_other'));
		if(empty($arLibrary['ITEM']))
		{
			@define("ERROR_404", "Y");
			CHTTP::SetStatus("404 Not Found");
			return '';
		}
		
		$APPLICATION->SetTitle($arLibrary['ITEM']['NAME']);
		
		#pre($arTemp,1);
		$APPLICATION->IncludeComponent(
			"neb:main.slider",
			"collections", 
			array(
				"PAGE_URL" => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['seaction'],
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "3600",
				"IBLOCK_ID" => $COLLECTIONS_IBLOCK_ID,
				"LIBRARY_ID" => $arLibrary['ITEM']['ID']
			),
			$component
		);

		$APPLICATION->IncludeComponent(
			"neb:collections.list", 
			"library", 
			array(
				"PAGE_URL" => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['seaction'],
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "3600",
				"IBLOCK_ID" => $COLLECTIONS_IBLOCK_ID,
				"LIBRARY_ID" => $arLibrary['ITEM']['ID']
			),
			$component
		);
		
	?>
	<div class="b-side right mtm10">
		<?
			$APPLICATION->IncludeComponent(
				"neb:library.right_counter",
				"",
				Array(
					"IBLOCK_ID" => $arParams['IBLOCK_ID'],
					"LIBRARY_ID" => $arLibrary['ITEM']['ID'],
					"CACHE_TIME" => $arParams["CACHE_TIME"],
					"CLASS" => 'nomargin',
				),
				$component
			);
		?>
	</div>
</section>