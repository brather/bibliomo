<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<div class="b-addreaders">
	<div class="b-readersearch clearfix">
		<form>
			<input type="text" name="find" class="left b-text " value="<?=$arParams['FIND']?>">
			<input type="submit" value="Найти" class="b-search_bth bbox">
		</form>
	</div>
	<table class="b-usertable">
		<tr>
			<th class="num_cell"><a <?=SortingExalead("ID");?>>№</a></th>
			<th class="name_cell"><a <?=SortingExalead("LAST_NAME");?>>Фамилия <br />Имя Отчество</a></th>
			<th class="ecbnum_cell"><a <?=SortingExalead("UF_NUM_ECHB");?>>Номер ЕЭЧБ</a></th>
			<th class="date_cell"><a <?=SortingExalead("DATE_REGISTER");?>>Дата <br>регистрации </a>	</th>
			<th class="status_cell"><a <?=SortingExalead("UF_STATUS");?>>Статус</a></th>
		</tr>
		<?
		
			$countOnPage = 30;// выводим 10 элементов
			if($arParams['COUNTONPAGE'])
			$countOnPage=$arParams['COUNTONPAGE'];
			$page=1;			
			if(isset($_GET['PAGEN_1'])) 
			$page = intval($_GET['PAGEN_1']);
			$startarr=$page*$countOnPage-$countOnPage;
			//$stoparr=$page*$countOnPage-1;
			$arSubRes=$arResult['USERS'];
		    $arResult['USERS'] = array_slice($arResult['USERS'], $startarr,  $countOnPage);
			
			
			if(!empty($arResult['USERS'])){
				foreach($arResult['USERS'] as $arUser)
				{
				?>
				<tr>
					<td><?=$arUser['ID']?></td>
					<td><?=$arUser['LAST_NAME']?> <?=$arUser['NAME']?> <?=$arUser['SECOND_NAME']?></td>
					<td><?=$arUser['UF_NUM_ECHB']?></td>
					<td><span class="date"><?=FormatDateFromDB($arUser['DATE_REGISTER'], 'SHORT')?></span></td>
					<td><span class="status"><?=$arUser['UF_STATUS']?></span></td>
				</tr>

				<?
				}
			}
		?>
	</table>
</div><!-- /.b-addreaders-->
<?

$elements=$arSubRes;
$elementsPage = array_slice($elements, $page * $countOnPage, $countOnPage);
//echo renderElementsPage($elementsPage);
$navResult = new CDBResult();
$navResult->NavPageCount = ceil(count($elements) / $countOnPage);
$navResult->NavPageNomer = $page;
$navResult->NavNum = 1;
$navResult->NavPageSize = $countOnPage;
$navResult->NavRecordCount = count($elements);

$APPLICATION->IncludeComponent('bitrix:system.pagenavigation', '', array(
    'NAV_RESULT' => $navResult,
));
//htmlspecialcharsBack($arResult['STR_NAV']);

?>