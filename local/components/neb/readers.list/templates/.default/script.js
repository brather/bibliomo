$(function(){
    $('.action-unbind').click(function(){
        var reader_id = $(this).closest("tr").find("td:first").text();
        if(confirm("Удалить читателя №"+reader_id+" из библиотеки?"))
        {
            var params = {
                'sessid' : $('.action_cell').data("sessid")
            };
            var sender = $(this);
            $.post($(this).data("url"), params, function(data){
                if(data["error"]==0)
                {
                    sender.closest("tr").remove();
                }
                else
                {
                    alert(data["message"]);
                }
            },'json');
        }
    });
});
