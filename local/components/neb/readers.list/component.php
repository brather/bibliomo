<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
	if(empty($arParams['LIBRARY_ID']))
		return false;

	$arResult = array();
	#$arResult = Bitrix\NotaExt\Iblock\Element::getByID($arParams['LIBRARY_ID'], array('ID', 'NAME', 'DETAIL_PAGE_URL', 'PROPERTY_STATUS', 'skip_other'));

	$arSkipUID = array();
	$rsUsers = nebUser::GetList(($by="id"), ($order="desc"), array('ACTIVE' => 'Y', 'GROUPS_ID' => array(6, 7, 8, 9)), array('FIELDS' => array('ID')));
	while($arUser = $rsUsers->Fetch())
		$arSkipUID[] = $arUser['ID'];

	$arFilter = array(
		'ACTIVE' => 'Y',
		'UF_LIBRARY' => $arParams['LIBRARY_ID'],
	);

	$arParams['FIND'] = htmlspecialcharsbx($_REQUEST['find']);
	if(!empty($arParams['FIND']))
		$arFilter['NAME'] = $arParams['FIND'];

	global $by, $order;
	$by = trim(htmlspecialcharsEx($by));
	$order = trim(htmlspecialcharsEx($order));

	$by = empty($by) ? 'ID': $by;
	$order = empty($order) ? 'desc': $order;

	$rsUsers = nebUser::GetList($by, $order, $arFilter, array(
		'SELECT' => array('UF_NUM_ECHB', 'UF_STATUS'), 
		'FIELDS' => array('ID', 'NAME', 'LAST_NAME', 'SECOND_NAME', 'DATE_REGISTER') 
	)); 
	while($arUser = $rsUsers->Fetch())
	{
		if(in_array($arUser['ID'], $arSkipUID))
			continue;

		if(!empty($arUser['UF_STATUS']))
		{
			$rsStatus = CUserFieldEnum::GetList(array(), array("ID" => $arUser["UF_STATUS"]));		
			if($arStatus = $rsStatus->GetNext())
				$arUser['UF_STATUS'] = $arStatus['VALUE']; 

		}
		else
		{
			$arUser['UF_STATUS'] = 'Не&nbsp;верифицированный';
		}

		$arResult['USERS'][] = $arUser;
	}

	$this->IncludeComponentTemplate();

	$APPLICATION->SetTitle("Читатели");

?>