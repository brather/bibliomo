<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
global $APPLICATION;?>
<? include_once $_SERVER['DOCUMENT_ROOT'] . '/function/select.php';?>
<?
$arOptions = ThisUserProfile();?>
<div class="fieldrow nowrap" style="margin-top: 15px;">
	<div class="fieldcell iblock left-50">
<select  class="SelectProfile" id="SelectProfile">
	<option value="0">Новый профиль</option>
	<? foreach ($arOptions as $Option): ?>
		<option <?if($Option['ID'] == $_REQUEST['id']):?>selected="selected"<?endif;?> value="<?= $Option['ID']; ?>"><?= $Option['NAME']; ?></option>
	<? endforeach; ?>
</select>
</div>
	<div class="fieldcell iblock right-50">
		<a href="">Обновить профили настроек на странице импорта</a>
	<?
	if ($arResult["SUCCESS"] == true)
	{?>

	<?}?>
<?/*if($arResult['PROPERTY_DEFAULT_VALUE'] == 'Y'):?>
		<span style="color: #3B4A38">Профиль по умолчанию</span>
	<?else:?>
		<a style="text-decoration: underline;" href="" id="DefaultPr" rel="<?=$_REQUEST['id'];?>">Сделать профилем по умолчанию</a>
	<?endif;*/?>

</div>
</div>

<form method="post" action="?updateSettings=yes" class="b-form b-form_common b-addbookform">
	<?=bitrix_sessid_post()?>
	<input type="hidden" name="action" value="updateSettings"/>
	<input type="hidden" name="id" value="<?=$_REQUEST['id'];?>"/>
	<div class="fieldrow nowrap">
		<div class="fieldcell iblock left-50">
			<label for="settings02">Название профиля</label>
			<div class="field validate">
				<input type="text" data-required="required" value="<?=$arResult['NAME']?>" id="settings02" data-minlength="2" name="Name" class="input" >
				<em class="error required">Поле обязательно для заполнения</em>
			</div>
		</div>
		<div class="fieldcell iblock right-50">
			<label for="settings03">Автор (сведения об интеллектуальной ответственности)</label>
			<div class="field validate">
				<input type="text" value="<?=$arResult['PROPERTY_AUTHOR_VALUE']?>" id="settings03" data-minlength="2" name="Author" class="input" >
			</div>
		</div>
	</div>
	<div class="fieldrow nowrap ">
		<div class="fieldcell iblock left-50">
			<label for="settings22">Заглавие</label>
			<div class="field validate">
				<input type="text" value="<?=$arResult['PROPERTY_TITLE_VALUE']?>" id="settings22" data-minlength="2" name="Title" class="input" >
			</div>
		</div>
		<div class="fieldcell iblock right-50">
			<label for="settings22">Заглавие части</label>
			<div class="field validate">
				<input type="text" value="<?=$arResult['PROPERTY_PART_TITLE_VALUE']?>" id="settings22" data-minlength="2" name="PartTitle" class="input" >
			</div>
		</div>
	</div>
	<div class="fieldrow nowrap">
		<div class="fieldcell iblock left-50">
			<label for="settings22">Серия (название, выпуск)</label>
			<div class="field validate">
				<input type="text" value="<?=$arResult['PROPERTY_SERIA_VALUE']?>" id="settings22" data-minlength="2" name="SERIA" class="input" >
			</div>
		</div>
		<div class="fieldcell iblock right-50">
			<label for="settings25">Обьём</label>
			<div class="field validate">
				<input type="text" value="<?=$arResult['PROPERTY_VOLUME_VALUE']?>" id="settings25" data-minlength="2" name="VOLUME" class="input" >
			</div>
		</div>
	</div>
	<div class="fieldrow nowrap">
		<div class="fieldcell iblock left-50">
			<label for="settings22">Номер части</label>
			<div class="field validate">
				<input type="text" value="<?=$arResult['PROPERTY_PART_NO_VALUE']?>" id="settings22" data-minlength="2" name="PartNumber" class="input" >
			</div>
		</div>
		<div class="fieldcell iblock right-50">
			<label for="settings25">Год издания</label>
			<div class="field validate">
				<input type="text" value="<?=$arResult['PROPERTY_PUBLISH_YEAR_VALUE']?>" id="settings25" data-minlength="2" name="PublishYear" class="input" >
			</div>
		</div>
	</div>
	<div class="fieldrow nowrap">
		<div class="fieldcell iblock left-50">
			<label for="settings20">Издательство или издающая организация</label>
			<div class="field validate">
				<input type="text" value="<?=$arResult['PROPERTY_PUBLISH_VALUE']?>" id="settings20" data-minlength="2" name="Publisher" class="input" >
			</div>
		</div>
		<div class="fieldcell iblock right-50">
			<label for="settings19">Место издания</label>
			<div class="field validate">
				<input type="text" value="<?=$arResult['PROPERTY_PUBLISH_PLACE_VALUE']?>" id="settings19" data-minlength="2" name="PublishPlace" class="input" >
			</div>
		</div>
	</div>
	<div class="fieldrow nowrap ">
		<div class="fieldcell iblock left-50">
			<label for="settings18">Международные стандартные книжные номера (ISBN)</label>
			<div class="field validate">
				<input type="text" value="<?=$arResult['PROPERTY_ISBN_VALUE']?>" id="settings18" data-minlength="2" name="ISBN" class="input" >
			</div>
		</div>
		<div class="fieldcell iblock right-50">
			<label for="settings07">Код ББК</label>
			<div class="field validate">
				<input type="text" value="<?=$arResult['PROPERTY_BBK_VALUE']?>" id="settings07" data-minlength="2" name="BBK" class="input" >
			</div>
		</div>
	</div>
	<div class="fieldrow nowrap fieldrowaction">
		<div class="fieldcell left-50">
			<div class="field clearfix">
				<button class="formbutton left bt_typelt" value="1" type="submit">Сохранить изменения</button>
			</div>
		</div>
	</div>
</form>
