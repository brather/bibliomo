<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

class ImportSettingsFormComponent extends \CBitrixComponent
{
    protected $user;
    protected $success = false;
    protected $userLibrary;

	protected function checkAccess()
    {
        $this->user = new nebUser();
        $this->userLibrary = $this->user->getLibrary();
		
        global $APPLICATION;
        if (!is_array($this->userLibrary) || !intval($this->userLibrary['ID']))
        {
			ShowError("У вас нет права доступа к данному файлу.");
			die();
		}
    }
	
	protected function checkActions()
    {
        $action = (isset($_REQUEST['action']) ? trim($_REQUEST['action']) : '');
        if (($action == 'updateSettings') && check_bitrix_sessid())
        {
            $this->updateSettings($_REQUEST);				// Обновляем или создаем профиль
			$this->arResult = $this->fillFields();			// Потом выводим его в шаблоне
			$this->arResult["SUCCESS"] = $this->success;
        }
		else
		{
			$this->arResult = $this->fillFields();
		}
    }
	
	protected function fillFields()
    {

		$arSelect = array(
			"IBLOCK_ID",
			"ID",
			"NAME",
			"PROPERTY_TITLE",
			"PROPERTY_AUTHOR",
			"PROPERTY_PART_NO",
			"PROPERTY_PART_TITLE",
			"PROPERTY_PUBLISH_YEAR",
			"PROPERTY_PUBLISH_PLACE",
			"PROPERTY_PUBLISH",
			"PROPERTY_BBK",
			"PROPERTY_ISBN",
			"PROPERTY_LIBRARIES",
			"PROPERTY_USER",
			"PROPERTY_SERIA",
			"PROPERTY_VOLUME",
			"PROPERTY_DEFAULT"
		);

		if(strlen($_REQUEST['id']) > 0 || $_REQUEST['id'] != 0){
			$arFilter = Array("IBLOCK_ID" => 14, "ACTIVE" => "Y");
			$arFilter["ID"] = $_REQUEST['id'];
			$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
			if ($ob = $res->GetNextElement(true,false))
			{
				$arFields = $ob->GetFields();
				return $arFields;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
		
	}
	
	protected function updateSettings($request)
    {
		global $USER;
		$properties = array(
			"AUTHOR" => trim($request["Author"]),
			"PART_NO" => trim($request["PartNumber"]),
			"PART_TITLE" => trim($request["PartTitle"]),
			"PUBLISH_YEAR" => trim($request["PublishYear"]),
			"PUBLISH_PLACE" => trim($request["PublishPlace"]),
			"PUBLISH" => trim($request["Publisher"]),
			"BBK" => trim($request["BBK"]),
			"ISBN" => trim($request["ISBN"]),
			"TITLE" => trim($request["Title"]),
			//"LIBRARIES" => array($this->userLibrary['ID']),
			"USER" => $USER->GetID(),
			"SERIA" => trim($request["SERIA"]),
			"VOLUME" => trim($request["VOLUME"]),
		);
		$arLoadProductArray = Array(
			"IBLOCK_ID"			=> 14,
			"PROPERTY_VALUES"	=> $properties,
			"NAME"				=> trim($request["Name"]),
			"ACTIVE"			=> "Y",
		);
		$el = new CIBlockElement;
		if ($arFields = $this->fillFields())												// Если профиль уже имеется, обновляем
		{
			$res = $el->Update(trim($request["id"]), $arLoadProductArray);
			if ($res)
			{
				ShowMessage(Array("TYPE"=>"OK", "MESSAGE"=>"Профиль успешно обновлен"));
				$this->success = true;
			}
			else
				ShowError("Не удалось сохранить изменения в профиле. ".$el->LAST_ERROR);
		}
		else																				// Если профиля еще нет - создаем
		{
			if($res = $el->Add($arLoadProductArray))
			{
				ShowMessage(Array("TYPE"=>"OK", "MESSAGE"=>"Профиль успешно создан"));
				$this->success = true;
			}
			else
				ShowError("Не удалось создать профиль. Ошибка: ".$el->LAST_ERROR);
		}
	}

    public function executeComponent()
    {
        try
        {
            $this->checkAccess();
            $this->checkActions();

            $this->IncludeComponentTemplate();
        }
        catch (Exception $e)
        {
            ShowError($e -> getMessage());
        }
    }
}