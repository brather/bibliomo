<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use \Bitrix\Main\Localization\Loc as Loc;
Loc::loadMessages(__FILE__);
?>
<section class="innersection innerwrapper clearfix searchempty" id="search_page_block">
	<div class="b-mainblock left">
		<br /><br />
		<!--<nav class="b-commonnav">
		<a href="#" class="current">новые</a>
		<a href="#">популярные</a>
		<a href="#">рекомендованные</a>
		</nav>-->
		<div class="b-collection_descr">
			<div class="b-collection_act">
				<?
					if(collectionUser::isAdd()){
					?>
					<div class="b-addcollecion rel">
						<span class="metalabel"><?=Loc::getMessage('COLLECTION_DETAIL_ADD_TO_MY_COL'); ?></span>
						<div class="meta"><a class="b-bookadd" data-collection="<?=ADD_COLLECTION_URL?>list.php?t=books_collection&id=<?=$arResult['SECTION']['ID']?>" data-plus="<?=Loc::getMessage('COLLECTION_DETAIL_ADD_TO_MY_COL'); ?>" data-minus="<?=Loc::getMessage('COLLECTION_DETAIL_REMOVE_FROM_MY_COL'); ?>" href="#" data-normitem="Y" data-url="<?=ADD_COLLECTION_URL?>removeBook.php?t=books_collection&id=<?=$arResult['SECTION']['ID']?>"></a></div>
					</div>
					<?
					}
				?>
				<div class="b-collupdate"><em><?=Loc::getMessage('COLLECTION_DETAIL_UPDATED'); ?>:</em> <?=$arResult['SECTION']['TIMESTAMP_X']?></div>
			</div>
			<h1><?=$arResult['SECTION']['NAME']?></h1>
			<?
				if(!empty($arResult['LIBRARY']))
				{
				?>
				<div class="b-result_sorce_info"><em><?=Loc::getMessage('COLLECTION_DETAIL_AUTHOR'); ?>:</em> <a class="b-sorcelibrary" href="<?=$arResult['LIBRARY']['DETAIL_PAGE_URL']?>"><?=$arResult['LIBRARY']['NAME']?></a></div>
				<?
				}
			?>
			<p><?=$arResult['SECTION']['~DESCRIPTION']?></p>
			<!--<a class="b-lib_photoslink" href="#">Фотогалерея</a>-->
			<a href="#" class="set_opener iblock right"><?=Loc::getMessage('COLLECTION_DETAIL_SETTINGS'); ?></a>
		</div>

		<?
			$APPLICATION->IncludeComponent(
				"exalead:search.page.viewer.left",
				"",
				Array(
					"PARAMS" => array_merge($arParams, array('TITLE_MODE_BLOCK' => 'Отобразить книги коллекции плиткой', 'TITLE_MODE_LIST' => 'Отобразить книги коллекции списком')),
					"RESULT" => $arResult,
				),
				$component
			);
		?>
	</div><!-- /.b-mainblock -->

	<div class="b-side right">
		<div class="b-favside mtop">
			<div class="b-favside_img js_flexbackground">
				<img src="<?=$arResult['SECTION']['DETAIL_PICTURE']?>"  data-bgposition="50% 0" class="js_flex_bgimage" alt="">
				<?
					if(!empty($arResult['COVER_IMAGES'])){
					?>
					<img src="<?=$arResult['COVER_IMAGES']?>&width=131&height=194" class="real loadingimg" alt="">
					<?
					}
				?>
			</div>
			<a class="b-favside_books"><?=$arResult['SECTION']['CNT']?> <?=GetEnding($arResult['SECTION']['CNT'], Loc::getMessage('COLLECTION_DETAIL_BOOK_5'), Loc::getMessage('COLLECTION_DETAIL_BOOK_1'), Loc::getMessage('COLLECTION_DETAIL_BOOK_2'))?> <?=Loc::getMessage('COLLECTION_DETAIL_IN_COL'); ?></a>
			<hr>
			<?
				if(!empty($arResult['SECTION']['UF_ADDED_FAVORITES']))
				{
				?>
				<div class="b-lib_counter">
					<div class="b-lib_counter_lb"><?=Loc::getMessage('COLLECTION_DETAIL_ADDED_TO_FAV'); ?></div>
					<div class="icofavs">х <?=number_format($arResult['SECTION']['UF_ADDED_FAVORITES'], 0, '', ' ')?></div>
				</div>
				<?
				}
				if(!empty($arResult['SECTION']['UF_VIEWS']))
				{
				?>
				<div class="b-lib_counter">
					<div class="b-lib_counter_lb"><?=Loc::getMessage('COLLECTION_DETAIL_VIEWS'); ?></div>
					<div class="icoviews">х <?=number_format($arResult['SECTION']['UF_VIEWS'], 0, '', ' ')?></div>
				</div>
				<?
				}
			?>
		</div>
		<?
			$APPLICATION->IncludeComponent(
				"neb:search.page.viewer.right",
				"",
				Array(
					"PARAMS" => $arParams,
					"RESULT" => $arResult,
				),
				$component
			);
		?>

	</div><!-- /.b-side -->

</section>