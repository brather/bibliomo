<?php
$MESS['COLLECTION_DETAIL_ADD_TO_MY_COL'] = 'Добавить в мои подборки';
$MESS['COLLECTION_DETAIL_REMOVE_FROM_MY_COL'] = 'Удалить из моих подборок';
$MESS['COLLECTION_DETAIL_UPDATED'] = 'Обновлено';
$MESS['COLLECTION_DETAIL_AUTHOR'] = 'Автор';
$MESS['COLLECTION_DETAIL_BOOK_5'] = 'книг';
$MESS['COLLECTION_DETAIL_BOOK_1'] = 'книга';
$MESS['COLLECTION_DETAIL_BOOK_2'] = 'книги';
$MESS['COLLECTION_DETAIL_IN_COL'] = 'в подборке';
$MESS['COLLECTION_DETAIL_ADDED_TO_FAV'] = 'Добавленно в избранное';
$MESS['COLLECTION_DETAIL_VIEWS'] = 'Количество просмотров';
$MESS['COLLECTION_DETAIL_SETTINGS'] = 'Настройки';