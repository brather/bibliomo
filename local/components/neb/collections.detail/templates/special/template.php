<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use \Bitrix\Main\Localization\Loc as Loc;
Loc::loadMessages(__FILE__);
?>

<section class="innersection innerwrapper clearfix searchempty ">
    <!--<nav class="b-commonnav noborder">
        <a href="#" title="новые" class="current">новые</a>
        <a href="#" title="популярные" >популярные</a>
        <a href="#" title="рекомендованные" >рекомендованные</a>
    </nav>-->

    <div class="b-collection_descr">
        <h1><?=$arResult['SECTION']['NAME']?></h1>
        <div><span class="button_mode b-bookincoll"><?=$arResult['SECTION']['CNT']?> <?=GetEnding($arResult['SECTION']['CNT'], 'книг', 'книга', 'книги')?> в коллекции</span></div>
        <div class="b-result_sorce_info"><em>Автор:</em> <?=$arResult['LIBRARY']['NAME']?></div>
        <div class="b-collupdate"><em>Обновлено:</em> <?=$arResult['SECTION']['TIMESTAMP_X']?></div>
        <p><?=$arResult['SECTION']['DESCRIPTION']?></p>
    </div>
    <div class="b-filter_list_wrapper">
        <div class="b-result-doc">
            <?foreach($arResult['ITEMS'] as $book):?>
                <div class="b-result-docitem">

                    <div class="iblock b-result-docphoto">
                        <span class="b_bookpopular_photo iblock"><img alt="" class="loadingimg" src="<?=$book['IMAGE_URL']?>"></span>
                        <!--<div class="b-result-doclabel">
                            Требуется авторизация
                        </div>-->
                    </div>
                    <div class="iblock b-result-docinfo">
                        <span class="num"><?=$book['NUM_ITEM']?>.</span>
                        <h2><a href="/special<?=$book['DETAIL_PAGE_URL']?>" title="Сумма технологии"><?=$book['title']?></a></h2>
                        <ul class="b-resultbook-info">
                            <?if($book['authorbook']):?><li><span>Автор:</span> <?=$book['authorbook']?></li><?endif;?>
                            <?if($book['year']):?><li><span>Год публикации:</span> <?=$book['year']?></li><?endif;?>
                            <?if($book['countpages']):?><li><span>Количество страниц:</span> <?=$book['countpages']?></li><?endif;?>
                            <?if($book['publisher']):?><li><span>Издательство:</span> <?=$book['publisher']?></li><?endif;?>
                        </ul>
                        <p><?=$book['text']?></p>
                    </div>

                </div>
            <?endforeach;?>

            <?=$arResult['STR_NAV']?>

        </div><!-- /.b-result-doc -->
    </div><!-- /.b-filter_list_wrapper -->

</section>