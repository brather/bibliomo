<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

CModule::IncludeModule('nota.library');
use Nota\Library\Collection;

if($_REQUEST['action'] == 'sortBooks' && $_REQUEST['AJAX'] == 'Y' && check_bitrix_sessid()){
    Collection::sortBooks($_REQUEST['data']);
    //LocalRedirect($APPLICATION->GetCurPage());
    //print_r($_REQUEST['data']);
}