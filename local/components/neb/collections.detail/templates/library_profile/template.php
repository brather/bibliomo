<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use \Bitrix\Main\Localization\Loc as Loc;
Loc::loadMessages(__FILE__);
?>
        <div class="b-collection_booklist">
            <div class="b-collection-item">
                <h2><?=$arResult['SECTION']['NAME']?></h2>
                <a href="/collections/<?=$arResult['SECTION']['ID']?>_<?=$arResult['SECTION']['CODE']?>/"><b><?=Loc::getMessage('COLLECTION_DETAIL_LIB_COL_PAGE'); ?></b></a>
                <p class="total">
                    <a class="button_blue btadd closein add_books_to_fond" data-loadtxt="загрузка..." href="<?=$this->__folder . '/addBookForm.php?'.bitrix_sessid_get().'&COLLECTION_ID=' . $arResult['SECTION']['ID']?>"  data-width="635" >Добавить книгу</a>
                    <span class="right"><?=Loc::getMessage('COLLECTION_DETAIL_LIB_TOTAL'); ?> <?=$arResult['SECTION']['CNT']?> <?=GetEnding($arResult['SECTION']['CNT'], Loc::getMessage('COLLECTION_DETAIL_LIB_BOOK_5'), Loc::getMessage('COLLECTION_DETAIL_LIB_BOOK_1'), Loc::getMessage('COLLECTION_DETAIL_LIB_BOOK_2'))?> <?=Loc::getMessage('COLLECTION_DETAIL_LIB_IN_COL'); ?></span>
                </p>
            </div><!-- /.b-collection-item -->

        </div><!-- /.b-collection_list -->

    </div>

    <div class="b-side right b-sidesmall">
        <a href="<?=$arResult['LIBRARY']['DETAIL_PAGE_URL']?>" class="b-btlibpage"><?=Loc::getMessage('COLLECTION_DETAIL_LIB_LIB_PAGE'); ?></a>
    </div><!-- /.b-side -->

    <div class="b-mainblock left">

        <a name="nav_start"></a>
        <div class="b-filter">
            <div class="b-filter_wrapper">
                <a href="#" class="sort sort_opener"><?=Loc::getMessage('COLLECTION_DETAIL_LIB_SORT'); ?></a>
                <span class="sort_wrap">
                    <a <?=SortingExalead("property_author")?>><?=Loc::getMessage('COLLECTION_DETAIL_LIB_BY_AUTHOR'); ?></a>
                    <a <?=SortingExalead("name")?>><?=Loc::getMessage('COLLECTION_DETAIL_LIB_BY_NAME'); ?></a>
                    <a <?=SortingExalead("property_publish_year")?>><?=Loc::getMessage('COLLECTION_DETAIL_LIB_BY_DATE'); ?></a>
                </span>
                <span class="b-filter_act">
                    <span class="b-filter_show"><?=Loc::getMessage('COLLECTION_DETAIL_LIB_SHOW'); ?></span>
                    <a href="<?=$APPLICATION->GetCurPageParam("pagen=15", array("pagen", "dop_filter", 'longpage'));?>" class="b-filter_num b-filter_num_paging<?=($arParams['ITEM_COUNT'] == 15 and !$arParams['LONG_PAGE']) ? ' current' : ''?>">15</a>
                    <a href="<?=$APPLICATION->GetCurPageParam("pagen=30", array("pagen", "dop_filter", 'longpage'));?>" class="b-filter_num b-filter_num_paging<?=$arParams['ITEM_COUNT'] == 30 ? ' current' : ''?>">30</a>
                    <a href="<?=$APPLICATION->GetCurPageParam("pagen=45", array("pagen", "dop_filter", 'longpage'));?>" class="b-filter_num b-filter_num_paging<?=$arParams['ITEM_COUNT'] == 45 ? ' current' : ''?>">45</a>
                </span>
            </div>
        </div><!-- /.b-filter -->

        <div class="b-result-doc b-collection_booklist js_sortable" data-page-items-count="<?=$arParams['ITEM_COUNT']?>" data-ajax-path="<?=$this->__folder . '/sortBooksAjax.php'?>">
            <?foreach($arResult['ITEMS'] as $book):?>
                <div class="b-result-docitem" data-id="<?=$book['BITRIX']['ID']?>">
                    <div class="iblock b-result-docphoto">
                        <a class="b_bookpopular_photo iblock coverlay popup_opener ajax_opener" data-width="955" href="<?=$book['DETAIL_PAGE_URL']?>"><img alt="<?=$book['title']?>" class="loadingimg" src="<?=$book['IMAGE_URL']?>"></a>
                    </div>
                    <div class="iblock b-result-docinfo">
                        <h2><a data-width="955" href="/catalog/<?=$book['~id']?>/" class="coverlay popup_opener ajax_opener"><?=$book['title']?></a></h2>
                        <ul class="b-resultbook-info">
                            <li><span><?=Loc::getMessage('COLLECTION_DETAIL_LIB_AUTHOR'); ?>:</span> <a href="<?=$book['r_authorbook']?>"><?=$book['authorbook']?></a></li>
                        </ul>
                        <div class="b-collstat">
                            <div class="b-lib_counter">
                                <span class="b-lib_counter_lb"><?=Loc::getMessage('COLLECTION_DETAIL_LIB_READ'); ?></span>
                                <span class="icouser">х <?=number_format($book['STAT']['READ_CNT'], 0, '', ' ');?></span>
                            </div>
                            <div class="b-lib_counter">
                                <span class="b-lib_counter_lb"><?=Loc::getMessage('COLLECTION_DETAIL_LIB_VIEW'); ?></span>
                                <span class="icoviews">х <?=number_format($book['STAT']['VIEW_CNT'], 0, '', ' ');?></span>
                            </div>
                            <div class="b-lib_counter">
                                <span class="b-lib_counter_lb"><?=Loc::getMessage('COLLECTION_DETAIL_LIB_FAV'); ?></span>
                                <span class="icofav">х <?=number_format($book['STAT']['FAV_CNT'], 0, '', ' ');?></span>
                            </div>
                        </div>
                        <p><?=$book['text']?></p>
                        <div class="b-result_sorce clearfix">
                            <div class="b-result_sorce_info left"><em><?=Loc::getMessage('COLLECTION_DETAIL_LIB_SOURCE'); ?>:</em>
                                <?foreach($book['LIBS'] as $LIB):?>
                                    <span class="b-sorcelibrary"><?=$LIB["NAME"]?></span>&nbsp;
                                <?endforeach;?>
                            </div>
                        </div>
                    </div>
                    <div class="b-collaction">
                        <a href="#" class="item_up"></a>
                        <a href="#" class="item_down"></a>
                        <a onclick="return confirm('Книга <?=$book['title']?> будет удалена из коллекции <?=$arResult['SECTION']['NAME']?>. Вы уверены?')" href="<?=$APPLICATION->GetCurPage() . '?'.bitrix_sessid_get().'&COLLECTION_ID=' . $arResult['SECTION']['ID'] . '&BOOK_ID=' . $book['BITRIX']['ID'] . '&action=removeBook'?>" class="right button_red" data-width="330"><?=Loc::getMessage('COLLECTION_DETAIL_LIB_REMOVE'); ?></a>
                        <a href="<?=$APPLICATION->GetCurPage() . '?'.bitrix_sessid_get().'&BOOK_ID=' . $book['BITRIX']['ID'] . '&IS_COVER=' . $book['BITRIX']['IS_COVER'] . '&action=toggleBookCover'?>" <?if($book['BITRIX']['IS_COVER']):?>class="button_red"<?endif;?>><?if($book['BITRIX']['IS_COVER']):?><?=Loc::getMessage('COLLECTION_DETAIL_LIB_UNSET_COVER'); ?><?else:?><?=Loc::getMessage('COLLECTION_DETAIL_LIB_SET_COVER'); ?><?endif;?></a>
                    </div>
                </div>
            <?endforeach;?>
        </div><!-- /.b-result-doc -->
        <?=$arResult['STR_NAV']?>
	</div><!-- /.b-mainblock -->

	<div class="b-side right b-sidesmall">
        <?
        $APPLICATION->IncludeComponent(
            "neb:search.page.viewer.right",
            "",
            Array(
                "PARAMS" => $arParams,
                "RESULT" => $arResult,
            ),
            $component
        );
        ?>
	</div><!-- /.b-side -->

