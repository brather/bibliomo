$(function(){
    $('.item_down').click(function(e){
        e.preventDefault();

        moveDown($(this).parents('.b-result-docitem'));
    });

    $('.item_up').click(function(e){
        e.preventDefault();

        moveUp($(this).parents('.b-result-docitem'));
    });

    $( ".b-collection_booklist.js_sortable" ).sortable({
        update: function(event, ui) {
            sort();
        }
    });
});

function sort(){
    var items = $('.b-result-docitem');
    var page_count = $( ".b-collection_booklist.js_sortable").attr('data-page-items-count');
    var page = urlParams.PAGEN_1 == undefined ? 1 : urlParams.PAGEN_1;
    var ajax_path = $( ".b-collection_booklist.js_sortable").attr('data-ajax-path');

    var data = [];
    items.each(function(i, v){
        var item = $(this);

        item.attr('data-sort', ((page-1) * page_count + i) + 1);
        data.push({id: item.attr('data-id'), sort: item.attr('data-sort')});
    });

    $.post(ajax_path, {AJAX: 'Y', action: 'sortBooks', sessid: BX.bitrix_sessid(), data: data}, function(response){
        //console.log(response);
    });
}

function moveUp(item) {
    var prev = item.prev();
    if (prev.length == 0)
        return;
    prev.css('z-index', 999).css('position','relative').animate({ top: item.height() }, 250);
    item.css('z-index', 1000).css('position', 'relative').animate({ top: '-' + prev.height() }, 300, function () {
        prev.css('z-index', '').css('top', '').css('position', '');
        item.css('z-index', '').css('top', '').css('position', '');
        item.insertBefore(prev);

        sort();
    });


}
function moveDown(item) {
    var next = item.next();
    if (next.length == 0)
        return;
    next.css('z-index', 999).css('position', 'relative').animate({ top: '-' + item.height() }, 250);
    item.css('z-index', 1000).css('position', 'relative').animate({ top: next.height() }, 300, function () {
        next.css('z-index', '').css('top', '').css('position', '');
        item.css('z-index', '').css('top', '').css('position', '');
        item.insertAfter(next);

        sort();
    });
}

var urlParams;
(window.onpopstate = function () {
    var match,
        pl     = /\+/g,
        search = /([^&=]+)=?([^&]*)/g,
        decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
        query  = window.location.search.substring(1);

    urlParams = {};
    while (match = search.exec(query))
        urlParams[decode(match[1])] = decode(match[2]);
})();