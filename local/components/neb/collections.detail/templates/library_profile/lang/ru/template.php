<?php
$MESS['COLLECTION_DETAIL_LIB_COL_PAGE'] = 'Страница коллекции на портале';
$MESS['COLLECTION_DETAIL_LIB_ADD_BOOK'] = 'Добавить книгу';
$MESS['COLLECTION_DETAIL_LIB_BOOK_5'] = 'книг';
$MESS['COLLECTION_DETAIL_LIB_BOOK_1'] = 'книга';
$MESS['COLLECTION_DETAIL_LIB_BOOK_2'] = 'книги';
$MESS['COLLECTION_DETAIL_LIB_IN_COL'] = 'в коллекции';
$MESS['COLLECTION_DETAIL_LIB_TOTAL'] = 'всего';
$MESS['COLLECTION_DETAIL_LIB_LIB_PAGE'] = 'Страница библиотеки<br>на портале';
$MESS['COLLECTION_DETAIL_LIB_SORT'] = 'Сортировать';
$MESS['COLLECTION_DETAIL_LIB_BY_AUTHOR'] = 'По автору';
$MESS['COLLECTION_DETAIL_LIB_BY_NAME'] = 'По названию';
$MESS['COLLECTION_DETAIL_LIB_BY_DATE'] = 'По дате';
$MESS['COLLECTION_DETAIL_LIB_SHOW'] = 'Показать';
$MESS['COLLECTION_DETAIL_LIB_AUTHOR'] = 'Автор';
$MESS['COLLECTION_DETAIL_LIB_READ'] = 'Читатели';
$MESS['COLLECTION_DETAIL_LIB_VIEW'] = 'Просмотры';
$MESS['COLLECTION_DETAIL_LIB_FAV'] = 'В избранном';
$MESS['COLLECTION_DETAIL_LIB_SOURCE'] = 'Источник';
$MESS['COLLECTION_DETAIL_LIB_REMOVE'] = 'Убрать из коллекции';
$MESS['COLLECTION_DETAIL_LIB_UNSET_COVER'] = 'Убрать с обложки коллекции';
$MESS['COLLECTION_DETAIL_LIB_SET_COVER'] = 'Сделать обложкой коллекции';
