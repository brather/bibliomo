<?	require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
	//var_dump($arResult);
?>
<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="ru"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="ru"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="ru"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="ru"> <!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<script src="<?=MARKUP?>js/libs/modernizr.min.js"></script>
		<meta name="viewport" content="width=device-width"/>

		<title><?$APPLICATION->ShowTitle()?></title>
		<?$APPLICATION->ShowHead();?>
		<link rel="icon" href="<?=MARKUP?>favicon.ico" type="image/x-icon" />
		<?$APPLICATION->SetAdditionalCSS(MARKUP.'css/style.css');?>
		<?$APPLICATION->AddHeadScript(MARKUP.'js/libs/jquery.min.js');?>
		<?$APPLICATION->AddHeadScript('/local/templates/.default/js/script.js');?>
		<?$APPLICATION->AddHeadScript(MARKUP.'js/slick.min.js');?>
		<?$APPLICATION->AddHeadScript(MARKUP.'js/plugins.js');?>
		<?$APPLICATION->AddHeadScript(MARKUP.'js/jquery.knob.js');?>
		<?$APPLICATION->AddHeadScript(MARKUP.'js/script.js');?>
		<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
		<?if($APPLICATION->GetCurPage() == '/'){?>
			<script>
				$(function() {
					$('html').addClass('mainhtml'); // это для морды
				}); // DOM loaded
			</script>
			<?}?>
		<script type="text/javascript" src="//vk.com/js/api/openapi.js?115"></script>
		<script type="text/javascript">VK.init({apiId: 4525156, onlyWidgets: true});</script>

	</head>

	<body>
		<base target="_self">
		<div class="b_bookfond_popup_content js_scroll">
			<?
				global $navParent;
				$navParent = '_self';

                $requestParams['COLLECTION_ID'] = (int)$_REQUEST['COLLECTION_ID'];
			?>
			<?$APPLICATION->IncludeComponent(
					"neb:search.form",
					"",
					Array(
						"PAGE" => (''),
						"POPUP_VIEW" => 'Y',
						"ACTION_URL" => $APPLICATION->GetCurPage(),
                        "REQUEST_PARAMS" => $requestParams
					)
				);?>

			<?
				$nebUser = new nebUser();
				$lib = $nebUser->getLibrary();
				
				CModule::IncludeModule("highloadblock"); 
				use Bitrix\Highloadblock as HL; 
				use Bitrix\Main\Entity; 

				if(!empty($lib['PROPERTY_LIBRARY_LINK_VALUE']))
				{
					$hlblock = HL\HighloadBlockTable::getById(HIBLOCK_LIBRARY)->fetch();
					$entity = HL\HighloadBlockTable::compileEntity($hlblock); 
					$entity_data_class = $entity->getDataClass(); 

					$rsData = $entity_data_class::getById($lib['PROPERTY_LIBRARY_LINK_VALUE']);
					$arData = $rsData->Fetch();
				}


			?>
			<?if(isset($_REQUEST['q'])):?>
				<?$APPLICATION->IncludeComponent(
						"neb:catalog.search",
						"popup_library_collection",
						Array(
							'ID_LIBRARY' => $arData['UF_ID'],
							'ID_COLLECTION' => (int)$_REQUEST['COLLECTION_ID'],
                            //'FORM_ACTION' => '/profile/collection/edit/' . $_REQUEST['COLLECTION_ID'] . '/'
							'FORM_ACTION' => '/profile/collection/'
						),
						false
					);?>
				<?endif;?>
		</div>
	</body>
</html>