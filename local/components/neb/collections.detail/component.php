<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (!CModule::IncludeModule('evk.books')) return false;
//use Nota\Library\Collection;
use Evk\Books\Collection;

CPageOption::SetOptionString("main", "nav_page_in_session", "N");

if($arParams["IBLOCK_ID"] < 1) {
	ShowError("IBLOCK_ID IS NOT DEFINED");
	return false;
}

if(intval($arParams["SECTION_ID"]) <= 0)
	return false;

//use Bitrix\NotaExt\Iblock\Section;
//use Bitrix\NotaExt\Iblock\Element;
use Evk\Books\IBlock\Section;
use Evk\Books\IBlock\Element;

if($_REQUEST['action'] == 'addBook' && isset($_REQUEST['COLLECTION_ID']) && $_REQUEST['BOOK_NUM_ID'] > 0 && check_bitrix_sessid()){
	Collection::addBook($_REQUEST['COLLECTION_ID'], $_REQUEST['BOOK_ID_' . $_REQUEST['BOOK_NUM_ID']]);
	LocalRedirect($APPLICATION->GetCurPage());
}

if($_REQUEST['action'] == 'removeBook' && isset($_REQUEST['COLLECTION_ID']) && isset($_REQUEST['BOOK_ID']) && check_bitrix_sessid()){
	Collection::removeBook((int)$_REQUEST['BOOK_ID']);
	LocalRedirect($APPLICATION->GetCurPage());
}

if($_REQUEST['action'] == 'toggleBookCover' && isset($_REQUEST['BOOK_ID']) && check_bitrix_sessid()){
	Collection::toggleBookCover((int)$_REQUEST['BOOK_ID'], (bool)$_REQUEST['IS_COVER']);
	LocalRedirect($APPLICATION->GetCurPage());
}

$arResult = array();

$arResult['SECTION'] = Section::getByID(
	$arParams["SECTION_ID"],
	array(
		'TIMESTAMP_X',
		'CODE',
		'DESCRIPTION',
		'UF_GROUPS',
		'UF_ADDED_FAVORITES',
		'UF_VIEWS',
		'PICTURE'
	),
	$arParams["IBLOCK_ID"]
);
if(empty($arResult['SECTION']))
{
	@define("ERROR_404", "Y");
	CHTTP::SetStatus("404 Not Found");
	return '';
}

if($arResult['SECTION']['PICTURE'] > 0)
{
	$File = CFile::ResizeImageGet($arResult['SECTION']['PICTURE'], array('width'=>300, 'height'=>300), BX_RESIZE_IMAGE_PROPORTIONAL, true);
	$arResult['SECTION']['PICTURE'] = $File['src'];
}
else
	$arResult['SECTION']['PICTURE'] = '/local/templates/.default/markup/pic/pic_32.png';

$arResult['SECTION']['TIMESTAMP_X']  = FormatDateFromDB($arResult['SECTION']['TIMESTAMP_X'], 'SHORT');



if(!empty($arResult['SECTION']['UF_LIBRARY']))
{
	$arResult['LIBRARY'] = Element::getByID($arResult['SECTION']['UF_LIBRARY'], array('DETAIL_PAGE_URL'));
}

$arCover = Element::getList(
	array(
		'IBLOCK_ID' => $arParams['IBLOCK_ID'],
		'SECTION_ID' =>$arResult['SECTION']['ID'],
		'!PROPERTY_TYPE_IN_SLIDER' => false,
		'!PROPERTY_BOOK_ID_VALUE' => false
	),
	1,
	array('PROPERTY_BOOK_ID', 'skip_other'),
	array('PROPERTY_TYPE_IN_SLIDER' => 'ASC')
);

$arBooks = Element::getList(
	array(
		'IBLOCK_ID' => $arParams['IBLOCK_ID'],
		'SECTION_ID' => $arResult['SECTION']['ID'],
		'!PROPERTY_BOOK_ID_VALUE' => false),
	false,
	array('PROPERTY_BOOK_ID', 'skip_other', 'ID', "PROPERTY_TYPE_IN_SLIDER")
);

$arBitrixBooks = array();

$arBooksId = array();

if(!empty($arBooks['ITEMS']))
{
	foreach($arBooks['ITEMS'] as $arItem)
	{
		if(intval($arItem['PROPERTY_BOOK_ID_VALUE']) > 0)
			$arBooksId[] = intval($arItem['PROPERTY_BOOK_ID_VALUE']);

		$arBitrixBooks[$arItem['PROPERTY_BOOK_ID_VALUE']] = Array(
			'ID' => $arItem['ID'],
			'SORT' => $arItem['SORT'],
			'IS_COVER' => $arItem["PROPERTY_TYPE_IN_SLIDER_ENUM_ID"] == COLLECTION_SLIDER_TYPE_COVER_ENUM_ID,
		);
	}
}

$arResult['SECTION']['CNT'] = count($arBooksId);

use Evk\Books\SearchQuery;

if(intval($arCover['ITEM']['PROPERTY_BOOK_ID_VALUE']) > 0)
{
	$query = new SearchQuery();
	$arTmp = $query->getById(intval($arCover['ITEM']['PROPERTY_BOOK_ID_VALUE']), array("PROPERTY_FILE"));
	$arTmp = array_merge($arTmp, \Evk\Books\Books::GetBookUrls(intval($arCover['ITEM']['PROPERTY_BOOK_ID_VALUE'])));

	if(!empty($arTmp) && !empty($arTmp["PROPERTY_FILE_VALUE"]))
		$arResult['COVER_IMAGES'] = $arTmp['IMAGE_URL_PREVIEW'];
	unset($arTmp, $arCover);
}

$arParams['ITEM_COUNT'] = intval($_REQUEST['pagen']) > 0 ? intval($_REQUEST['pagen']) : 15;	if($arParams['ITEM_COUNT'] > 60) $arParams['ITEM_COUNT'] = 60;
$arParams['NEXT_ITEM'] = intval($_REQUEST['next']);
$arParams['MODE'] = empty($_REQUEST['mode']) ? 'list' : trim($_REQUEST['mode']);

$by = trim(htmlspecialcharsEx($_REQUEST['by']));
$order = trim(htmlspecialcharsEx($_REQUEST['order']));

$searchText = trim(htmlspecialcharsbx($_REQUEST["qbook"]));
$searchBy = trim(htmlspecialcharsbx($_REQUEST["search_by"]));
if(!empty($searchText))
{
	if ($searchBy == "author")
		$arFilter = array("%PROPERTY_AUTHOR" => $searchText);
	elseif ($searchBy == "title")
		$arFilter = array("%NAME" => $searchText);
}
else
	$arFilter = array();

$arResult["ITEMS"] = array();

if(count($arBooksId))
{
	$query = new SearchQuery($arResult, $arParams);
	$query->setPageLimit($arParams['ITEM_COUNT']);
	$result = $query->FindBooksSimple(array($by=>$order), array_merge($arFilter, array(
		"ID" => $arBooksId,
	)), false, false, array(
		"PROPERTY_FILE",
		"PROPERTY_LIBRARIES",
		"NAME",
		"PROPERTY_AUTHOR",
		"PROPERTY_PUBLISH",
		"PROPERTY_PUBLISH_YEAR",
		"PROPERTY_PUBLISH_PLACE",
	));
	
	$query->GetDataForSmartFilter();
	$query->FillFilterArray($_REQUEST, $arrFilter);
	$query->GetAllSearchElementsForSmartFilter($arFilter, $arrFilter);


	if(!empty($result))
	{
		$arResult["ITEMS"] = $result;
		$arResult["ALL_ITEM_IDS"] = array_keys($result);
	}

	if(intval($arResult['COUNT']) > 0)
	{
		$arResult['NEXT'] = $arResult['START_ITEM'] + $arParams['ITEM_COUNT'];
		if($arResult['NEXT'] > $arResult['COUNT'])
			$arResult['NEXT'] = false;

		if($arResult['NEXT'])
			$arResult['NEXT_URL_PARAMS'] = parse_url($APPLICATION->GetCurPageParam('next='.$arResult['NEXT'],array('next', 'dop_filter')), PHP_URL_QUERY);
	}

	foreach($arResult["ITEMS"] as &$item) {
		//получим кол-во просмотров для коллекций в библиотеках
		if($this->__templateName == "library_profile")
		{
			$item["STAT"] = Collection::getBookStat($item['id']);
		}
		$item['BITRIX'] = $arBitrixBooks[$item['id']];
	}
}

if(!empty($_REQUEST['dop_filter']) and $_REQUEST['dop_filter'] == 'Y' and $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')
	$APPLICATION->RestartBuffer();

//\Evk\Books\Books::p($arResult);

if($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' and empty($_REQUEST['dop_filter']))
	$arParams['ajax'] = true;

$this->IncludeComponentTemplate();

if($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')
	exit();

$APPLICATION->SetTitle($arResult['SECTION']['NAME']);

if (CModule::IncludeModule('evk.books')) {
	\Evk\Books\Tables\MoStatsEventTable::trackEvent('collection_page');
}
