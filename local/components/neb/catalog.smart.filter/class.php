<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use Bitrix\Main\Localization\Loc as Loc;
Loc::loadMessages(__DIR__."/component.php");
/*DEMO CODE for component inheritance
CBitrixComponent::includeComponentClass("bitrix::news.base");
class CBitrixCatalogSmartFilter extends CBitrixNewsBase
*/
class CBitrixCatalogSmartFilter extends CBitrixComponent
{
	var $IBLOCK_ID = 0;
	var $FILTER_NAME = "";
	var $SAFE_FILTER_NAME = '';

	public function ShowOnlyExistsRows()
	{
		global $APPLICATION;
		$this->arParams["EXISTS_COUNT"] = 0;

		foreach($this->arParams["ITEMS"] as $iItem=>&$arItem)
		{
			switch($iItem)
			{
				case 48:
					$arItem["TITLE"] = 'Авторы'; break;
				case 52:
					$arItem["TITLE"] = 'Место издания'; break;
				case 53:
					$arItem["TITLE"] = 'Издательство'; break;
				case 63:
					$arItem["TITLE"] = 'Библиотеки'; break;
				default:
					continue 2;
			}
			//echo "count: ".count($arItem["VALUES"]);
			$arItem["EXISTS_COUNT"] = count($arItem["VALUES"]);
			//\Evk\Books\Books::pe($this->arParams["ALL_ITGEM_IDS"]);

			foreach($arItem["VALUES"] as &$arItem2)
			{
				$arMerge = array_intersect($this->arParams["ALL_ITEM_IDS"], $arItem2["IBLOCK_ELEMENT_IDS"]);
				$arItem2["EXISTS_COUNT"] = count($arMerge);
				if($arItem2["EXISTS_COUNT"] > 0)
				{
					$arItem2["SHOW"] = "Y";
					$URL = $APPLICATION->GetCurPageParam();
					$PATTERN = sprintf("@(searchFilter_%d_\d+)@", $iItem);
					$REPLACE = "";
					if(preg_match($PATTERN, $URL, $MATCH))
					{
						$REPLACE = $MATCH[1];
					}
					$newParams = "";
					if($arItem2["CONTROL_NAME"] != $REPLACE)
						$newParams = sprintf('%s=%s', $arItem2["CONTROL_NAME"], $arItem2["HTML_VALUE"]);
					$arItem2["URL"] = $APPLICATION->GetCurPageParam($newParams, array($REPLACE));
				}
			}
			$this->arParams["EXISTS_COUNT"] += $arItem["EXISTS_COUNT"];

			uasort($arItem["VALUES"], function($a, $b){
				if($a["SHOW"] == "N" && $b["SHOW"] == "N")
					return 0;
				elseif($a["SHOW"] == "N" && $b["SHOW"] == "Y")
					return 1;
				elseif($a["SHOW"] == "Y" && $b["SHOW"] == "N")
					return -1;

				if($a["EXISTS_COUNT"] < $b["EXISTS_COUNT"])
					return 1;
				elseif($a["EXISTS_COUNT"] > $b["EXISTS_COUNT"])
					return -1;
				return 0;
			});

		}

	}
	public function GetCurPageParam($newParams='', $deleteParams=array(), $maskDelete = array())
	{
		$newLink = $GLOBALS["APPLICATION"]->GetCurPageParam($newParams, $deleteParams);

		if(is_array($maskDelete) && count($maskDelete) > 0 || is_string($maskDelete) && strlen($maskDelete) > 0)
		{
			$newLink = preg_replace($maskDelete, "", $newLink);
		}
		return $newLink;
	}
	public function onPrepareComponentParams($arParams)
	{
		$arParams["CACHE_TIME"] = isset($arParams["CACHE_TIME"]) ? $arParams["CACHE_TIME"]: 36000000;
		$arParams["IBLOCK_ID"] = (int)$arParams["IBLOCK_ID"];

		$arParams["SAVE_IN_SESSION"] = $arParams["SAVE_IN_SESSION"] == "Y";
		$arParams["CACHE_GROUPS"] = $arParams["CACHE_GROUPS"] !== "N";
		$arParams["INSTANT_RELOAD"] = $arParams["INSTANT_RELOAD"] === "Y";

		$arParams["FILTER_NAME"] = (isset($arParams["FILTER_NAME"]) ? (string)$arParams["FILTER_NAME"] : '');
		if(
			$arParams["FILTER_NAME"] == ''
			|| !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["FILTER_NAME"])
		)
		{
			$arParams["FILTER_NAME"] = "arrFilter";
		}

		return $arParams;
	}

	public function executeComponent()
	{
		$this->IBLOCK_ID = $this->arParams["IBLOCK_ID"];
		$this->FILTER_NAME = $this->arParams["FILTER_NAME"];
		$this->SAFE_FILTER_NAME = htmlspecialcharsbx($this->FILTER_NAME);
		return parent::executeComponent();
	}

	public function checkedItems($arItems)
	{
		$arCheckedItems = array();
		foreach($arItems as $arItem)
		{
			foreach($arItem["VALUES"] as $arValue)
			{
				if(isset($_REQUEST[$arValue["CONTROL_NAME"]]) && $_REQUEST[$arValue["CONTROL_NAME"]] == $arValue["HTML_VALUE"])
				{
					$arCheckedItems[] = $arValue["CONTROL_NAME"];
				}
			}
		}
		$arCheckedItems = array_map(function($a){
			return sprintf('input[name="%s"]', $a);
		}, $arCheckedItems);
		return $arCheckedItems;
	}

	public function p($array, $return=0)
	{
		$str = "<pre>". print_r($array, 1). "</pre>";
		if($return)
			return $str;
		echo $str;
	}
}