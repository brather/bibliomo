<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @var CBitrixCatalogSmartFilter $this */
/** @global CDatabase $DB */
/** @global CUser $USER */
/** @global CMain $APPLICATION */

$arParams["AJAX"] = (isset($arParams["AJAX"]))? $arParams["AJAX"] : ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest');

$this->ShowOnlyExistsRows();

$this->IncludeComponentTemplate();
