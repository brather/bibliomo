<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/**
 * @var CBitrixCatalogSmartFilter $component
 * @var CBitrixComponentTemplate $this
 */
use \Bitrix\Main\Localization\Loc as Loc;
Loc::loadMessages(__FILE__);
?>
<div class="b-side right">
	<?
	if($arParams["EXISTS_COUNT"] > 0)
	{
	?>
	<script type="text/javascript">
		$(function() {
			$(".b-sidenav input.checkbox").change(function() {
				var val = $(this).val();
				var strDopParam = $( "form#dop_filter" ).serialize();
				BX.showWait($(this).closest('li').attr('id'));
				$.get( "<?=$component->GetCurPageParam('', array('PAGEN_1'),array("@&?searchFilter_\d+_\d+=[^&]@"))?>", strDopParam, function( data ) {
					$('#search_page_block').remove();
					$('section.mainsection.innerpage').after(data);
					BX.closeWait();
				});
			});
			<?
			$items = $component->checkedItems($arParams["ITEMS"]);
			if(count($items) > 0)
			{
				?>
				$('<?=join(",",$items)?>').each(function () {
					if($(this).closest('ul').is(':hidden'))
					{
						$(this).closest('ul').show().prev().addClass('open');
					}
				});
				<?
			}

			if($arParams["AJAX"])
			{
				?>
				$('input.checkbox:not(".custom")').replaceCheckBox();
				$('.b-libfilter').libFilter();
				$('#search_page_block input.checkbox').replaceCheckBox();
				$('.js_select').selectReplace();
				$('.b-sidenav').sideAccord(); // аккордеон менюшки сайдбара
				$('.iblock').cleanWS();
				$('.js_filter').filterInit();
				$('.js_moreopen').opened();
				checkIsAvailablePdf();
				$('.js_flex_bgimage').flexbgimage(); // https://yadi.sk/i/AUQFqp8ucFNRA
				<?
			}
			?>
		});
	</script>
	<form method="post" id="dop_filter">
		<div class="b-sidenav <?=$arParams['sidenav_class']?>">
			<?
				if($arParams["ITEMS"][48]["EXISTS_COUNT"] > 0)
				{
					?>
					<a href="#" class="b-sidenav_title"><?=Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_RIGHT_AUTHORS');?></a>
					<ul class="b-sidenav_cont">
						<?
							$k = 0;
							foreach($arParams["ITEMS"][48]["VALUES"] as $key => $v)
							{
								if($v["SHOW"] == "Y")
								{
									?>
									<li class="clearfix<?=$k>4?' hidden':''?>" id="lia<?=$k?>">
										<div class="b-sidenav_value left"><?=$v['VALUE']?></div>
										<div class="checkwrapper type2 right">
											<label for="<?=$v["CONTROL_ID"]?>" class="black"><?=$v['EXISTS_COUNT']?></label><input<?=($v['CHECKED'])?' checked="checked"':''?> value="<?=$v["HTML_VALUE"]?>" class="checkbox" type="checkbox" name="<?=$v["CONTROL_NAME"]?>" id="<?=$v["CONTROL_ID"]?>" />
										</div>
									</li>
									<?
									$k++;
								}
							}
							if($arParams["ITEMS"][48]["EXISTS_COUNT"] > 5)
							{
								?>
								<li><a href="#" class="b_sidenav_contmore js_moreopen">+ <?=Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_RIGHT_MORE');?></a></li>
								<?
							}
						?>
					</ul>
					<?
				}
				if($arParams["ITEMS"][53]["EXISTS_COUNT"] > 0)
				{
					?>
					<a href="#" class="b-sidenav_title"><?=Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_RIGHT_PUBLISH');?></a>
					<ul class="b-sidenav_cont">
						<?
							$k = 0;
							foreach($arParams["ITEMS"][53]["VALUES"] as $key => $v)
							{
								if($v["SHOW"] == "Y")
								{
									?>
									<li class="clearfix<?=$k>4?' hidden':''?>" id="lip<?=$k?>">
										<div class="b-sidenav_value left"><?=$v['VALUE']?></div>
										<div class="checkwrapper type2 right">
											<label for="<?=$v["CONTROL_ID"]?>" class="black"><?=$v['EXISTS_COUNT']?></label><input<?=($v['CHECKED'])?' checked="checked"':''?> value="<?=$v["HTML_VALUE"]?>" class="checkbox" type="checkbox" name="<?=$v["CONTROL_NAME"]?>" id="<?=$v["CONTROL_ID"]?>" />
										</div>
									</li>
									<?
									$k++;
								}
							}
							if(count($arParams["ITEMS"][53]["VALUES"])>5)
							{
								?>
								<li><a href="#" class="b_sidenav_contmore js_moreopen">+ <?=Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_RIGHT_MORE');?></a></li>
								<?
							}
						?>
					</ul>
					<?
				}
				if($arParams["ITEMS"][63]["EXISTS_COUNT"] > 0)
				{
				?>
				<a href="#" class="b-sidenav_title"><?=Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_RIGHT_LIBS');?></a>
				<ul class="b-sidenav_cont">
					<?
						$k=0;
						foreach($arParams["ITEMS"][63]["VALUES"] as $key => $v)
						{
							if($v["SHOW"] == "Y")
							{
								?>
								<li class="clearfix<?=$k>4?' hidden':''?>" id="lib<?=$k?>">
									<div class="b-sidenav_value left"><?=$v['VALUE']?></div>
									<div class="checkwrapper type2 right">
										<label for="<?=$v["CONTROL_ID"]?>" class="black"><?=$v['EXISTS_COUNT']?></label><input<?=($v['CHECKED'])?' checked="checked"':''?> value="<?=$v["HTML_VALUE"]?>" class="checkbox" type="checkbox" name="<?=$v["CONTROL_NAME"]?>" id="<?=$v["CONTROL_ID"]?>"/>
									</div>
								</li>
								<?
								$k++;
							}
						}
						if(count($arParams["ITEMS"][63]["VALUES"])>5)
						{
							?>
							<li><a href="#" class="b_sidenav_contmore js_moreopen">+ <?=Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_RIGHT_MORE');?></a></li>
							<?
						}
					?>
				</ul>
				<?
				}
				if($arParams["ITEMS"][52]["EXISTS_COUNT"] > 0)
				{
				?>
				<a href="#" class="b-sidenav_title"><?=Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_RIGHT_PLACE');?></a>
				<ul class="b-sidenav_cont">
					<?
						$k=0;
						foreach($arParams["ITEMS"][52]["VALUES"] as $key => $v)
						{
							if($v["SHOW"] == "Y")
							{
								?>
								<li class="clearfix<?=$k>4?' hidden':''?>" id="lim<?=$k?>">
									<div class="b-sidenav_value left"><?=$v['VALUE']?></div>
									<div class="checkwrapper type2 right">
										<label for="<?=$v["CONTROL_ID"]?>" class="black"><?=$v['EXISTS_COUNT']?></label><input<?=($v['CHECKED'])?' checked="checked"':''?> value="<?=$v["HTML_VALUE"]?>" class="checkbox" type="checkbox" name="<?=$v["CONTROL_NAME"]?>" id="<?=$v["CONTROL_ID"]?>"/>
									</div>
								</li>
								<?
								$k++;
							}
						}
						if(count($arParams["ITEMS"][52]["VALUES"])>5)
						{
							?>
							<li><a href="#" class="b_sidenav_contmore js_moreopen">+ <?=Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_RIGHT_MORE');?></a></li>
							<?
						}
					?>
				</ul>
				<?
				}
			?>
		</div>
	</form>
	<?
	}
?>
</div>