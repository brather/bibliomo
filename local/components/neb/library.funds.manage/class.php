<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

CModule::IncludeModule("evk.books");
use Bitrix\Main;
use Bitrix\Main\Localization\Loc as Loc;
use Evk\Books\SearchQuery;
//use Nota\Exalead\BooksAdd;

class LibraryFundsManageComponent extends \CBitrixComponent
{
    protected $user;
    protected $userLibrary;

    public function onIncludeComponentLang()
    {
        $this -> includeComponentLang(basename(__FILE__));
        Loc::loadMessages(__FILE__);
    }

    public function onPrepareComponentParams($params)
    {
        $result = array(
            'LIBRARY_ID' => intval($params['LIBRARY_ID']),
        );
        return $result;
    }

    /**
     * Проверка на корректность обязательных параметров компонента
     * @throws Main\ArgumentNullException
     */
    protected function checkParams()
    {
        if ($this -> arParams['LIBRARY_ID'] <= 0)
            throw new Main\ArgumentNullException('LIBRARY_ID');
    }

    /**
     * Проверка прав доступа
     */
    protected function checkAccess()
    {
        $this->user = new nebUser();
        $this->userLibrary = $this->user->getLibrary();

        global $APPLICATION;
        if (!is_array($this->userLibrary) || $this->userLibrary['ID'] != $this->arParams['LIBRARY_ID'])
            $APPLICATION->AuthForm("У вас нет права доступа к данному файлу.");
    }

    /**
     * Обработка действий
     */
    protected function checkActions()
    {
        $action = (isset($_REQUEST['action']) ? trim($_REQUEST['action']) : '');

        if (!empty($action) && check_bitrix_sessid())
        {
            switch($action)
            {
                case 'addBook':
                    $this->addBook($_REQUEST);
                    break;
				case 'addBookByHand':
                    $this->addBookByHand($_REQUEST);
                    break;
                case 'editBook':
                    $this->editBook($_REQUEST);
                    break;
                case 'deleteBook':
                    $this->deleteBook($_REQUEST['bookId']);
                    break;
                case 'editBookForm':
                    $this->editBookForm($_REQUEST);
                    break;
            }
        }
    }

    /**
     * Добавление
     * @param $request
     */
    protected function addBook($request)
    {
        if(isset($request['BOOK_NUM_ID']))
        {
            global $APPLICATION;
            print_r($request);
            $bookID = $request['BOOK_ID_'.$request['BOOK_NUM_ID']];
            $bookTitle = $request['BOOK_TITLE_'.$request['BOOK_NUM_ID']];
            $bookAuthor = $request['BOOK_AUTHOR_'.$request['BOOK_NUM_ID']];
            $bookYear = $request['BOOK_YEAR_'.$request['BOOK_NUM_ID']];

			$properties = array(
				"AUTHOR" => $bookAuthor,
				"PUBLISH_YEAR" => $bookYear,
				"LIBRARIES" => array($this->arParams['LIBRARY_ID']),
			);
			$arLoadProductArray = Array(
				"IBLOCK_ID"			=> IBLOCK_ID_BOOKS,
				"PROPERTY_VALUES"	=> $properties,
				"NAME"				=> $bookTitle,
				"ACTIVE"			=> "Y",
				"XML_ID"			=> $bookID,
			);
/*
			$el = new CIBlockElement;
			if($res = $el->Add($arLoadProductArray))
				ShowMessage(Array("TYPE"=>"OK", "MESSAGE"=>"Книга была успешно добавлена"));
			else
				ShowError("Не удалось добавить книгу. ".$el->LAST_ERROR);
*/
        } else {
            ShowError(Loc::getMessage('LIBRARY_FUNDS_POPUP_CHOOSE_BOOK'));
        }
    }
	
	protected function addBookByHand($request)
    {
		if (check_bitrix_sessid())
		{
			// pre($request,1);
			global $APPLICATION;

			$properties = array (
				"AUTHOR" => trim($request["Author"]),
				"PART_NO" => trim($request["PartNumber"]),
				"PART_TITLE" => trim($request["SubName"]),
				"PUBLISH_PLACE" => trim($request["PublishPlace"]),
				"PUBLISH" => trim($request["Publisher"]),
				"PUBLISH_YEAR" => trim($request["PublishYear"]),
				"BBK" => trim($request["BBK"]),
				"ISBN" => trim($request["ISBN"]),
				"FILE" => CFile::MakeFileArray($request["FILE_PDF"]),
				"LIBRARIES" => array($this->arParams['LIBRARY_ID']),
				"COVER_NUMBER_PAGE" => trim($request["CoverNumberPage"]),
                "SERIA" => array($this->arParams['SERIA']),
                "VOLUME" => array($this->arParams['VOLUME']),
				// "83" => isset($request["Closed"]) ? "26" : "0",
			);

			$arLoadProductArray = Array(
				"IBLOCK_ID"			=> IBLOCK_ID_BOOKS,
				"PROPERTY_VALUES"	=> $properties,
				"NAME"				=> trim($request["Name"]),
				"ACTIVE"			=> "Y",
			);

			if (!empty($request["XML_ID"]))
				$arLoadProductArray["XML_ID"] = $request["XML_ID"];

			$el = new CIBlockElement;

			if($res = $el->Add($arLoadProductArray))
				ShowMessage(Array("TYPE"=>"OK", "MESSAGE"=>"Книга была успешно добавлена"));
			else
				ShowError("Не удалось добавить книгу. ".$el->LAST_ERROR);
		}
	}

    /**
     * Добавление / редактирование книги
     * @param $request
     */
    protected function editBook($request)
    {
        global $APPLICATION;
		$bookID = intval($request["bookId"]);
        if ($bookID > 0)
		{
			// pre($request,1);
			$properties = array(
				"AUTHOR" => trim($request["Author"]),
				"PART_NO" => trim($request["PartNumber"]),
				"PART_TITLE" => trim($request["SubName"]),
				"PUBLISH_PLACE" => trim($request["PublishPlace"]),
				"PUBLISH" => trim($request["Publisher"]),
				"PUBLISH_YEAR" => trim($request["PublishYear"]),
				"BBK" => trim($request["BBK"]),
				"ISBN" => trim($request["ISBN"]),
				"LIBRARIES" => array($this->arParams['LIBRARY_ID']),
                "SERIA" => array($this->arParams['SERIA']),
                "VOLUME" => array($this->arParams['VOLUME']),
				// "83" => isset($request["Closed"]) ? "26" : "0",
			);
			if (strpos($request["FILE_PDF"],"tmp_books_pdf") !== false)				//Если изменили файл (добавили другой), то заменяем. Иначе ничего не трогаем
			{
				$properties["FILE"] = CFile::MakeFileArray($request["FILE_PDF"]);
			}
			$arLoadProductArray = Array(
				"IBLOCK_ID"			=> IBLOCK_ID_BOOKS,
				"NAME"				=> trim($request["Name"]),
				"ACTIVE"			=> "Y",
			);

			CIBlockElement::SetPropertyValuesEx($bookID, IBLOCK_ID_BOOKS, $properties);

			$el = new CIBlockElement;
			$res = $el->Update($bookID, $arLoadProductArray);
			if ($res)
				ShowMessage(Array("TYPE"=>"OK", "MESSAGE"=>"Книга была успешно изменена"));
			else
				ShowError("Не удалось сохранить изменения в книге. ".$el->LAST_ERROR);
		}
    }

    /**
     * Форма редактирования книги
     * @param $request
     */
    protected function editBookForm($request)
    {
        $bookId = intval($request['bookId']);
        if ($bookId > 0)
        {
			$obSearch = SearchQuery::Create();
			$arFilter = array(
				"ACTIVE" => "Y",
				"ID" => $bookId,
			);
			$arSelect = array(
				"NAME",
				"DATE_CREATE",
				"PROPERTY_AUTHOR",
				"PROPERTY_LIBRARIES",
				"PROPERTY_PUBLISH_YEAR",
				"PROPERTY_PUBLISH_PLACE",
				"PROPERTY_PUBLISH",
				"PROPERTY_ISBN",
				"PROPERTY_BBK",
				"PROPERTY_PART_TITLE",
				"PROPERTY_FILE",
                "PROPERTY_SERIA",
                "PROPERTY_VOLUME",
			);
			$books = $obSearch->FindBooks(array(), $arFilter, false, false, $arSelect);
			$this->arResult = $books["ITEMS"][0];
        }
        global $APPLICATION;
        $APPLICATION->RestartBuffer();
        $this->IncludeComponentTemplate('editBookForm');
        die;
    }

    /**
     * Удаление книги
     * @param $id
     */
    protected function deleteBook($id)
    {
		if (CIBlockElement::Delete($id))
		{
			ShowMessage(Array("TYPE"=>"OK", "MESSAGE"=>"Книга была успешно удалена"));
		}
		else
		{
			ShowError("Не удалось удалить книгу");
		}
        
    }

    protected function getResult()
    {
		global $APPLICATION;
        $this->arResult['AJAX_FUNDS_MANAGE'] = (isset($_REQUEST["AJAX_FUNDS_MANAGE"]) && $_REQUEST["AJAX_FUNDS_MANAGE"] == "Y");

        $obSearch = SearchQuery::Create();
        $searchText = trim(htmlspecialcharsbx($_REQUEST["qbook"]));
        if(is_numeric($this->arParams["LIBRARY_ID"]) && $this->arParams["LIBRARY_ID"] > 0)
            $arFilter = array("PROPERTY_LIBRARIES" => array(intval($this->arParams["LIBRARY_ID"])));
        else
            $arFilter = array();
        if(!empty($searchText))
        {
            $arFilter = array_merge($arFilter, array(
                array(
                    "LOGIC" => "OR",
                    array("%NAME" => $searchText),
                    array("%PROPERTY_AUTHOR" => $searchText),
                ),
            ));
        }
		$arFilter["ACTIVE"] = "Y";
        if(is_numeric($this->arParams["COUNTONPAGE"]) && $this->arParams["COUNTONPAGE"] > 0)
            $nPageSize = intval($this->arParams["COUNTONPAGE"]);
        else
            $nPageSize = 15;
        $by = isset($_GET["by"]) ? htmlspecialcharsbx($_GET["by"]) : "TIMESTAMP_X";
        $order = isset($_GET["order"]) ? htmlspecialcharsbx($_GET["order"]) : "desc";
        // $this->arResult['BOOKS'] = $obSearch->FindBooksSimple(array($by=>$order.",nulls"), $arFilter, false, array(
        $arResSearch = $obSearch->FindBooks(array($by=>$order.",nulls"), $arFilter, false, array(
            "nPageSize" => $nPageSize,
        ),array(
            "PROPERTY_AUTHOR",
            "PROPERTY_LIBRARIES",
            "PROPERTY_PUBLISH_YEAR",
            "DATE_CREATE",
            "NAME",
        ));

		$this->arResult['BOOKS'] = $arResSearch["ITEMS"];
		$this->arResult['NAV_STRING'] = $arResSearch["NAV_STRING"];
        $this->arResult['NEXT_PAGE'] = \Evk\Books\PageNavigation::GetNextPage($obSearch->obNavigate, "AJAX_FUNDS_MANAGE=Y", array('AJAX_FUNDS_MANAGE'));
        //echo $obSearch->obNavigate->NavNum." ";
        // 1 134 2
        //\Evk\Books\Books::p($obSearch->obNavigate);
        $APPLICATION->SetTitle("Ведение фондов");
    }

    public function executeEpilog()
    {

    }

    public function executeComponent()
    {
        global $APPLICATION;
        try
        {
            $this->checkParams();
            $this->checkAccess();
            $this->checkActions();

            $this->getResult();
            if($this->arResult['AJAX_FUNDS_MANAGE'])
                $APPLICATION->RestartBuffer();

            $this->IncludeComponentTemplate();

            if($this->arResult['AJAX_FUNDS_MANAGE'])
                exit;

            return $this -> executeEpilog();
        }
        catch (Exception $e)
        {
            ShowError($e -> getMessage());
        }
    }
	/*
	protected function toDel($request){
		$arFields = array(
			'toDel' 			=> htmlspecialcharsEx(1)
		);
		$bookId = (int)$request['bookId'];
		if (intval($bookId) <= 0 || empty($arFields)) return;

		global $DB;
		foreach($arFields as &$Field)
		{
			$Field = '"'.$DB->ForSql($Field).'"';
		}		
		$DB->Update('tbl_common_biblio_card', $arFields, "WHERE ID='".$bookId."'", $err_mess.__LINE__);	
		print_r($err_mess);
		//LocalRedirect($APPLICATION->GetCurPage());
	}
	
	protected function undotoDel($request){
		$arFields = array(
			'toDel' 			=> htmlspecialcharsEx(0)
		);	
		$bookId = (int)$request['bookId'];
		if (intval($bookId) <= 0 || empty($arFields)) return;

		global $DB;
		foreach($arFields as &$Field)
		{
			$Field = '"'.$DB->ForSql($Field).'"';
		}		
		$DB->Update('tbl_common_biblio_card', $arFields, "WHERE ID='".$bookId."'", $err_mess.__LINE__);						
		LocalRedirect($APPLICATION->GetCurPage());
	}*/
}
