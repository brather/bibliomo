<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
global $APPLICATION;
?>
<div class="b-addsearchmode clearfix">
	<div style="float: left;  width: 40%;">
		<a class="button_blue btadd left closein add_books_to_fond" data-loadtxt="загрузка..." href="<?=$this->__folder . '/addBookForm.php?'.bitrix_sessid_get()?>"  data-width="635" >Добавить книгу</a>
		<a class="button_blue btadd left closein add_books_to_fond" data-loadtxt="загрузка..." href="<?=$this->__folder . '/importBooks.php?'.bitrix_sessid_get()?>"  data-width="635" >Импорт из XML-файла</a>
	</div>
    <div class="b-search_field right">
		<form method="get">
	        <input type="text" name="qbook" class="b-search_fieldtb b-text" id="asearch" value="<?=htmlspecialcharsbx($_REQUEST['qbook'])?>" autocomplete="off">
	        <input type="submit" class="b-search_bth bbox" value="Найти">
		</form>

    </div><!-- /.b-search_field-->
</div>
<div class="b-fondtable">

    <table class="b-usertable b-fondtb b-fondved">
        <tr>
            <th class="autor_cell"><a <?=SortingExalead("property_author")?>>Автор</a></th>
            <th class="title_cell"><a <?=SortingExalead("property_name")?>>Название</a></th>
            <th class="year_cell"><a <?=SortingExalead("property_publish_year")?>>Год <br> Издания</a></th>
            <th class="date_cell"><a <?=SortingExalead("date_create")?>>Дата <br> добавления</a></th>
            <th class="recycle_cell"><span class="nowrap">Действие</span></th>
        </tr>
        <?
		 foreach ($arResult['BOOKS'] as $book) {?>
            <tr>
                <td class="pl15"><?=$book['PROPERTY_AUTHOR_VALUE']?></td>
                <td class="pl15"><?=$book['NAME']?></td>
                <td class="pl15"><?=$book["PROPERTY_PUBLISH_YEAR_VALUE"]?></td>
				<td class="pl15"><?=$book['DATE_CREATE']?></td>
				<td>
					<a class="popup_opener ajax_opener coverlay" data-width="955" href="/catalog/<?=$book["ID"]?>/">Просмотр</a>
					<!--a class="button_blue btadd left closein add_books_to_fond" data-loadtxt="загрузка..." href="<?=$this->__folder . '/addBookForm.php?'.bitrix_sessid_get()?>"  data-width="635" >Добавить книгу</a-->
					<a class="btadd left popup_opener ajax_opener closein" href="<?=$APPLICATION->GetCurPageParam('action=editBookForm&bookId=' .$book["ID"]. '&' . bitrix_sessid_get(), array('action'))?>" data-popupsize="high" data-overlaybg="dark" data-width="632" data-height="920">Редактирование</a>
				</td>
            </tr>
        <?}?>
    </table>
</div><!-- /.b-stats-->
<?=$arResult["NAV_STRING"]?>
<script>
$(".plus_ico").click(function (e) {
	e.preventDefault()
	$(this).hide().after("<div style='margin-left:-30px;'>Отправлено</div>");;
	$(".b-hint").hide();
})
</script>
<style>
	th.year_cell{
		width: 90px;
	}
	.b-formsubmit .b-successlink.errortext {
		color: #F64F54;
	}
</style>