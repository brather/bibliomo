<?	require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
// pre($arResult,1);?>
<div class="b-addbook b-addfondbook">
	<h2>Редактирование книги</h2>
	<form method="post" action="" class="b-form b-form_common b-addbookform">
		<?=bitrix_sessid_post()?>
		<input type="hidden" name="action" value="editBook"/>
		<input type="hidden" name="bookId" value="<?=$arResult['ID']?>"/>
		<div class="fieldrow nowrap">
			<div class="fieldcell iblock">
				<label for="settings02">Полное заглавие произведения</label>
				<div class="field validate">
					<input type="text" data-required="required" value="<?=$arResult['NAME']?>" id="settings02" data-minlength="2" name="Name" class="input" >
					<em class="error required">Поле обязательно для заполнения</em>
				</div>
			</div>
		</div>
		<div class="fieldrow nowrap">
			<div class="fieldcell iblock w50c">
				<label for="settings03">Автор (сведения об интеллектуальной ответственности)</label>
				<div class="field validate">
					<input type="text"  value="<?=$arResult['PROPERTY_AUTHOR_VALUE']?>" id="settings03" data-minlength="2" name="Author" class="input w50p" >
					<em class="error validate">Поле заполнено неверно</em>
				</div>
			</div>
		</div>
		<div class="fieldrow nowrap">
			<div class="fieldcell iblock">
				<label for="settings22">Сведения, относящиеся к заглавию</label>
				<div class="field validate">
					<input type="text" value="<?=$arResult['PROPERTY_PART_TITLE_VALUE']?>" id="settings22" data-minlength="2" name="SubName" class="input" >
				</div>
			</div>
		</div>
		<div class="fieldrow nowrap">
			<div class="fieldcell iblock">
				<label for="settings22">Номер части</label>
				<div class="field validate">
					<input type="text" value="<?=$arResult['PROPERTY_PART_NO_VALUE']?>" id="settings22" data-minlength="2" name="PartNumber" class="input" >
				</div>
			</div>
		</div>
		<div class="fieldrow nowrap">
			<div class="fieldcell iblock mt0 w20pc">
				<label for="settings25">Год издания</label>
				<div class="field validate">
					<input type="text"  data-validate="number" value="<?=$arResult['PROPERTY_PUBLISH_YEAR_VALUE']?>" id="settings25" data-minlength="2" name="PublishYear" class="input b-yeartb" >
					<em class="error validate">Поле заполнено неверно</em>
				</div>
			</div>
		</div>
		<div class="fieldrow nowrap">
			<div class="fieldcell iblock">
				<label for="settings20">Издательство или издающая организация (точное название)</label>
				<div class="field validate">
					<input type="text" value="<?=$arResult['PROPERTY_PUBLISH_VALUE']?>" id="settings20" data-minlength="2" name="Publisher" class="input" >
					<em class="error validate">Поле заполнено неверно</em>
				</div>
			</div>
		</div>
		<div class="fieldrow nowrap">
			<div class="fieldcell iblock w50c">
				<label for="settings19">Место издания</label>
				<div class="field validate">
					<input type="text" value="<?=$arResult['PROPERTY_PUBLISH_PLACE_VALUE']?>" id="settings19" data-minlength="2" name="PublishPlace" class="input w50p" >
					<em class="error validate">Поле заполнено неверно</em>
				</div>
			</div>
		</div>
		<div class="fieldrow nowrap">
			<div class="fieldcell iblock mt10 w50c">
				<label for="settings18">Международные стандартные книжные номера (ISBN)</label>
				<div class="field validate">
					<input type="text" data-validate="number"  value="<?=$arResult['PROPERTY_ISBN_VALUE']?>" id="settings18" data-minlength="2" name="ISBN" class="input w50p" >
					<em class="error validate">Поле заполнено неверно</em>
				</div>
			</div>
		</div>
		<div class="fieldrow nowrap">
			<div class="fieldcell iblock w50">
				<label for="settings07">Код ББК</label>
				<div class="field validate">
					<input type="text" data-validate="number"  value="<?=$arResult['PROPERTY_BBK_VALUE']?>" id="settings07" data-minlength="2" name="BBK" class="input " >
					<em class="error validate">Поле заполнено неверно</em>
				</div>
			</div>
		</div>
		<!--div class="checkwrapper">
			<input class="checkbox addrindently" type="checkbox" name="Closed" id="cb_closed"><label for="cb_closed" class="black">Закрытое издание</label>
			<?
			if(!empty($arResult['PROPERTY_PROTECTED_VALUE']))
			{
				?>
				<script type="text/javascript">
					$( document ).ready(function() {
						$('input[name="Closed"]').click();
					});
				</script>
			<?
			}
			?>
				<script type="text/javascript">
					$( document ).ready(function() {
						$('#cb_closed').change(function(){
							$(this).attr('value', +$(this)[0].checked);
						});		
					});
				</script>									
		</div-->
		<div class="fieldrow nowrap">
			<div class="fieldcell iblock b-pdfadd">
			
				<?$APPLICATION->IncludeComponent(
					"notaext:plupload",
					"fond_add_books",
					array(
						"MAX_FILE_SIZE" => "512",
						"FILE_TYPES" => "pdf",
						"DIR" => "tmp_books_pdf",
						"FILES_FIELD_NAME" => "books_pdf",
						"MULTI_SELECTION" => "N",
						"CLEANUP_DIR" => "Y",
						"UPLOAD_AUTO_START" => "Y",
						"RESIZE_IMAGES" => "N",
						"UNIQUE_NAMES" => "Y",
						// "ALREADY_UPLOADED_FILE" => CFile::GetPath($arResult['PROPERTY_FILE_VALUE']), //TODO: Заменить прямую ссылку на pdf!!!!
						"ALREADY_UPLOADED_FILE" => \Evk\Books\Books::GetDownloadPDFLinkByFileID($arResult['PROPERTY_FILE_VALUE']), //TODO: Заменить прямую ссылку на pdf!!!!
					),
					false
				);?>

				<!--div class="right">
					<p>Дата добавления </p>
					<div class="b-fieldeditable">
						<div class="b-fondtitle"><span class="b-fieldtext"><?=$arResult['CreationDateTime']?></span><a class="b-fieldeedit" href="#"></a></div>
						<div class="b-fieldeditform"><span class="txt_fld iblock"><input type="text" name="CreationDateTime" class="txt txt_size2"></span><a class="b-editablsubmit iblock" href="#"></a></div>
					</div>
				</div-->
			</div>
		</div>
		<div class="fieldrow nowrap fieldrowaction">
			<div class="fieldcell ">
				<div class="field clearfix">
					<a href="#" class="formbutton gray right" style="width:176px" data-url="<?=$APPLICATION->GetCurPageParam('bookId='.$arResult['ID'].'&action=deleteBook', array("bookId", "action"));?>">Удалить книгу</a>
					<!--div class="meta minus">
						<div class="b-hint rel"><span>Удалить</span> книгу</div>
						<a href="#" class="b-bookremove fav"></a>
						<a href="#" data-url="<?=$APPLICATION->GetCurPageParam('bookId='.$arResult['ID'].'&action=deleteBook', array("bookId", "action"));?>" class="b-bookadd fav b-bookdelete" data-remove="removeonly"></a>
					</div-->
					<button class="formbutton left" value="1" type="submit">Сохранить изменения</button>
				</div>
			</div>
		</div>
	</form>
</div>
<script>
DOCUMENT.on( "click", ".fieldrowaction .formbutton.gray.right",function(e) {
	e.preventDefault();
	if (confirm('Вы действительно хотите удалить книгу?')) {
		window.location.href = $(this).data('url');
	}	
});
/*
	DOCUMENT.on( "click", ".b-bookdelete",function(e) {
		e.preventDefault();
		var cont = $(this), topPos = cont.offset(), par = cont.closest('.meta'),
		hint = $('.b-hint', par), block = $('.selectionBlock'),
		clone = block.clone(true) , 
		// form = $('.b-selectionadd', clone), 	
		// myfavslink = $('.b-openermenu', clone),
		metalabel = par.siblings('.metalabel');

		clone.addClass('selectionClone');

		if (cont.hasClass('fav')) {
			$('body').append('<div class="b-removepopup"></div>');
			var htmltxt = $('body').find('.b-removepopup.hidden').html();

			var removepopup = $('body > .b-removepopup:not(".hidden")');
				removepopup.html(htmltxt);
			var bt_remove = $('.btremove', removepopup), 
			bt_cancel = $('.gray', removepopup);


			removepopup.css('top', topPos.top - 400);	
			removepopup.show();
			// if (!par.hasClass('minus') ) { metalabel.html(cont.data('plus')); metalabel.removeClass('minuslb'); }			

			bt_remove.click(function(e){
				e.preventDefault();
				document.location.href = cont.data('url');
			});
		}
	});*/
</script>