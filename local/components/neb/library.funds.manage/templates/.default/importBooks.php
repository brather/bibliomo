<?require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");
IncludeModuleLangFile(__FILE__);
require_once($_SERVER["DOCUMENT_ROOT"]."/local/php_interface/include/helpers/nebImportXML.php");
// use Bitrix\NotaExt\WriteLog;
// $logF=new WriteLog($_SERVER['DOCUMENT_ROOT'].'/importDebugLog.txt');

// Получение роли пользователя. Парсер доступен только администратору сайта и администратору библиотеки
if ($USER->IsAdmin())
	$role = 'admin';
else
{
	$uobj = new nebUser();
	$role = $uobj->getRole();
}
if (!($role == 'library_admin' || $role == 'admin'))
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

// Количество элементов записываемых или обновляемых) в БД за один проход
if(!isset($INTERVAL))
	$INTERVAL = 1;
else
	$INTERVAL = intval($INTERVAL);

@set_time_limit(0);

$arErrors = array();

if($_SERVER["REQUEST_METHOD"] == "POST" && $_REQUEST["Import"]=="Y")
{
	// pre($_REQUEST,1);
	//Initialize NS variable which will save step data
	if(array_key_exists("NS", $_POST) && is_array($_POST["NS"]))
	{
		$NS = $_POST["NS"];
		if(array_key_exists("charset", $NS) && $NS["charset"] === "false") $NS["charset"] = false;
		if(array_key_exists("PREVIEW", $NS) && $NS["PREVIEW"] === "false") $NS["PREVIEW"] = false;
		if(array_key_exists("bOffer", $NS) && $NS["bOffer"] === "false") $NS["bOffer"] = false;
		$logF = new ImportLog($_POST["NS"]["LOG_FILE"]); 												// Записываем в старый
	}
	else
	{
		$logfile = "importLog_".date("d.m.Y_H.i.s").".txt";
		$NS = array(
			"STEP" => 0,
			"URL_DATA_FILE" => $_REQUEST["URL_DATA_FILE"],
			"ACTION" => $_REQUEST["outFileAction"],
			"LIBRARY_ID" => $_REQUEST["id_library"],
			"LOG_FILE" => $logfile,
			"SelectProfileInput" => $_REQUEST["SelectProfileInput"],
		);
		$logF = new ImportLog($logfile);																// Создаем новый		
	}
	$ABS_FILE_NAME = false;
	$WORK_DIR_NAME = false;
	if(isset($NS["URL_DATA_FILE"]) && (strlen($NS["URL_DATA_FILE"])>0))
	{
		$filename = trim(str_replace("\\", "/", trim($NS["URL_DATA_FILE"])), "/");
		$FILE_NAME = rel2abs($_SERVER["DOCUMENT_ROOT"], "/".$filename);
		if((strlen($FILE_NAME) > 1) && ($FILE_NAME === "/".$filename) && ($APPLICATION->GetFileAccessPermission($FILE_NAME) >= "W"))
		{
			$ABS_FILE_NAME = $_SERVER["DOCUMENT_ROOT"].$FILE_NAME;
			$WORK_DIR_NAME = substr($ABS_FILE_NAME, 0, strrpos($ABS_FILE_NAME, "/")+1);
		}
	}

	$obImportXML = new nebImportXML($NS["LIBRARY_ID"]);

	if(!check_bitrix_sessid())
	{
		$arErrors[] = GetMessage("IBLOCK_CML2_ACCESS_DENIED");
	}
	elseif($ABS_FILE_NAME)
	{
		echo "<pre>" . print_r($ABS_FILE_NAME, 1) . "</pre>";
		if($NS["STEP"] < 1)
		{
			if($obImportXML->DropTemporaryTable())
			{
				if($obImportXML->CreateTemporaryTable())
					$NS["STEP"]++;
				else
					$arErrors[] = GetMessage("IBLOCK_CML2_TABLE_CREATE_ERROR");
			}
		}
		elseif($NS["STEP"] < 2)
		{
			if(file_exists($ABS_FILE_NAME) && is_file($ABS_FILE_NAME))
			{
				if ($result = $obImportXML->ReadXML($ABS_FILE_NAME, $_REQUEST["SelectProfileInput"]) == true)
					$NS["STEP"]++;
				else
					$arErrors[] = $result;
			}
		}
		elseif($NS["STEP"] < 3)
		{
			if (!empty($NS["current_position"]))
				$k = $NS["current_position"];
			else
				$k = 0;
			$count_items = $obImportXML->GetCountItemsFromDB();
			$obItem = $obImportXML->GetItemFromDB($k,$INTERVAL);
			while ($arFields = $obItem->Fetch())
			{
				$el = new CIBlockElement;
				
				$PROP = $obImportXML->GetPropsFromFields($arFields);		// получить массив свойств. Соответствия поле - свойство прописывается в этой функции
				$PROP[63] = array($NS["LIBRARY_ID"]);						// id библиотеки
				$arLoadProductArray = Array(
					"MODIFIED_BY"    => $USER->GetID(), 					// элемент изменен текущим пользователем
					"IBLOCK_ID"      => IBLOCK_ID_BOOKS,
					"PROPERTY_VALUES"=> $PROP,
					"NAME"           => $arFields["NAME"],
					"ACTIVE"         => "Y",
				);
				$pdf_dir = $_SERVER["DOCUMENT_ROOT"]."/upload/books_pdf_import/";
				$arPdf = array();
				$filename = "";
				if (!empty($arFields["XML_ID"]))
				{
					$arLoadProductArray["XML_ID"] = $arFields["XML_ID"];
					$filename = $pdf_dir.$arFields["XML_ID"].".pdf";
					if (file_exists($filename)) {
						$arPdf = CFile::MakeFileArray($filename);
					}
				}
					
				$arFilter = array(
					"IBLOCK_ID"				=> IBLOCK_ID_BOOKS,
					"PROPERTY_LIBRARIES"	=> $NS["LIBRARY_ID"],
				);
				
				if (!empty($PROP[57]))
				{
					$res = CIBlockElement::GetList(Array(), array_merge($arFilter, array("PROPERTY_ISBN" => $PROP[57])), false, false, array("IBLOCK_ID", "ID"));
				}
				elseif (!empty($arFields["XML_ID"]))
				{
					$res = CIBlockElement::GetList(Array(), array_merge($arFilter, Array("XML_ID" => $arFields["XML_ID"])), false, false, array("IBLOCK_ID", "ID"));
				}
				elseif (!empty($PROP[95]))
				{
					$res = CIBlockElement::GetList(Array(), array_merge($arFilter, array("PROPERTY_AUTHOR" => $PROP[48], "PROPERTY_TITLE" => $PROP[95])), false, false, array("IBLOCK_ID", "ID"));
				}
				else
					$res = new CIBlockResult();
				
				$NS["DONE"]["ALL"]++;
				if($ob = $res->GetNextElement())
				{
					$arUpdateFields = $ob->GetFields();	
					if($NS["ACTION"] == "N")
					{
						//Update											
						if ($el->Update($arUpdateFields["ID"], $arLoadProductArray))
						{
							$logF->addLog(sprintf("%5d", $NS["DONE"]["ALL"]).") Обновлена книга с ID = ".$arUpdateFields["ID"].". Название: ".$arLoadProductArray["NAME"]);
							if (!empty($arPdf))
								if (CIBlockElement::SetPropertyValueCode($arUpdateFields["ID"], "FILE", Array("VALUE"=>$arPdf)))
								{
									unlink($filename);
									$logF->addLog(sprintf("%7s", "")."Добавлен файл книги ".$arLoadProductArray["XML_ID"].".pdf. Копия книги удалена с сервера");
								}
							$NS["DONE"]["UPD"]++;
						}
						else
						{
							$logF->addLog(sprintf("%5d", $NS["DONE"]["ALL"]).") Ошибка обновления книги с ID = ".$arUpdateFields["ID"].". Номер книги в xml-файле №".$arFields["ID"].". Название: ".$arLoadProductArray["NAME"].". Причина: ".$el->LAST_ERROR);
							$NS["DONE"]["ERR"]++;
							// $NS["DONE"]["ERR_LIST"][$arFields["ID"]] = "Ошибка обновления книги с ID = ".$arUpdateFields["ID"].": ".$el->LAST_ERROR;
						}
						
					}
					else
						$logF->addLog(sprintf("%5d", $NS["DONE"]["ALL"]).") Книга с ID = ".$arUpdateFields["ID"]." не изменена. Название: ".$arLoadProductArray["NAME"]);
				}
				else
				{
					//Add
					if($id = $el->Add($arLoadProductArray))
					{
						$logF->addLog(sprintf("%5d", $NS["DONE"]["ALL"]).") Добавлена новая книга с ID = ".$id.". Название: ".$arLoadProductArray["NAME"]);
						if (!empty($arPdf))
							if (CIBlockElement::SetPropertyValueCode($id, "FILE", Array("VALUE"=>$arPdf)))
							{
								unlink($filename);
								$logF->addLog(sprintf("%7s", "")."Добавлен файл книги ".$arLoadProductArray["XML_ID"].".pdf. Копия книги удалена с сервера");
							}
						$NS["DONE"]["ADD"]++;
					}
					else
					{
						$logF->addLog(sprintf("%5d", $NS["DONE"]["ALL"]).") Ошибка добавления книги. Номер книги в xml-файле №".$arFields["ID"].". Название: ".$arLoadProductArray["NAME"].". Причина: ".$el->LAST_ERROR);
						$NS["DONE"]["ERR"]++;
						// $NS["DONE"]["ERR_LIST"][$arFields["ID"]] = "Ошибка добавления книги №".$arFields["ID"].". Причина: ".$el->LAST_ERROR;
					}
				}
			}
			$k += $INTERVAL;
			if ($k < $count_items)
			{
				$NS["current_position"] = $k;
			}
			else
				$NS["STEP"]++;
		}
		elseif($NS["STEP"] < 4)
		{
			$NS["STEP"]++;
		}
	}
	else
	{
		$arErrors[] = GetMessage("IBLOCK_CML2_FILE_ERROR");
	}

	foreach($arErrors as $strError)
		ShowError($strError);

	if(count($arErrors) == 0)
	{
		if($NS["STEP"] < 4)
		{
			// sleep(1);
			$progressItems = array(
				GetMessage("IBLOCK_CML2_TABLES_DROPPED"),
				GetMessage("IBLOCK_CML2_TABLES_CREATED"),
			);
			$progressTotal = 0;
			$progressValue = 0;
/*
			if($NS["STEP"] < 1)
				$progressItems[] = GetMessage("IBLOCK_CML2_TABLES_CREATION");
			elseif($NS["STEP"] < 2)
				$progressItems[] = "<b>".GetMessage("IBLOCK_CML2_TABLES_CREATION")."</b>";
			else
				$progressItems[] = GetMessage("IBLOCK_CML2_TABLES_CREATED");*/

			if($NS["STEP"] < 1)
				$progressItems[] = GetMessage("IBLOCK_CML2_FILE_READING");
			elseif($NS["STEP"] < 2)
				$progressItems[] = "<b>".GetMessage("IBLOCK_CML2_FILE_READING")."</b>";
			else
				$progressItems[] = GetMessage("IBLOCK_CML2_FILE_READ");
			
			if($NS["STEP"] < 2)
			{
				$progressItems[] = GetMessage("IBLOCK_CML2_ELEMENTS");				
			}
			elseif($NS["STEP"] < 3)
			{
				$progressItems[] = "<b>".GetMessage("IBLOCK_CML2_ELEMENTS")."</b>";
				$progressTotal = $count_items;
				$progressValue = $NS["current_position"];
				if ($progressTotal > 0 && $progressValue > 0)
					$progressItems[] =  "<b>Завершено ".$progressValue." из ".$progressTotal."</b>";
			}
			else
				$progressItems[] = GetMessage("IBLOCK_CML2_ELEMENTS_DONE");			
			
			CPublicMessage::ShowMessage(array(
				"MESSAGE" => "Прогресс",				
				"DETAILS" => "<p>".implode("</p><p>", $progressItems)."</p>",
			));			

			if($NS["STEP"] > 0)
				echo '<script>DoNext('.CUtil::PhpToJSObject(array("NS"=>$NS)).');</script>';
		}
		else
		{
			$progressItems = array(
				GetMessage("IBLOCK_CML2_PROCESSED", array("#COUNT#"=>intval($NS["DONE"]["ALL"]))),
				GetMessage("IBLOCK_CML2_ADDED", array("#COUNT#"=>intval($NS["DONE"]["ADD"]))),
				GetMessage("IBLOCK_CML2_UPDATED", array("#COUNT#"=>intval($NS["DONE"]["UPD"]))),
				GetMessage("IBLOCK_CML2_WITH_ERRORS", array("#COUNT#"=>intval($NS["DONE"]["ERR"]))),
			);
			$progressErrItems = array();
			foreach ($NS["DONE"]["ERR_LIST"] as $err)
			{
				$progressErrItems[] = $err;
			}
			CPublicMessage::ShowMessage(array(
				"MESSAGE" => GetMessage("IBLOCK_CML2_DONE"),
				"DETAILS" => "<p>".implode("</p><p>", $progressItems)."</p>",				
			));
			
			$logF->addLog("
			Результаты:
			Всего бработано: ". intval($NS["DONE"]["ALL"])."
			Обновлено книг: ". intval($NS["DONE"]["UPD"])."
			Добавлено книг: ". intval($NS["DONE"]["ADD"])."
			Книг с ошибками: ". intval($NS["DONE"]["ERR"]), true);
			
			echo '<a class="history_import" href="'.$logF->getDownloadLink().'" target="_blank">Скачать историю импорта</a>';
			
			if (!empty($progressErrItems))
			{
				CPublicMessage::ShowMessage(array(
					"TYPE"    => "ERROR",
					"MESSAGE" => "Ошибки при импорте",
					"DETAILS" => "<p>".implode("</p><p>", $progressErrItems)."</p>",
				));
			}

			echo '<script>EndImport();</script>';
		}
	}
	else
	{
		echo '<script>EndImport();</script>';
	}

	require($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/include/epilog_admin_js.php");
}

$APPLICATION->SetTitle(GetMessage("IBLOCK_CML2_TITLE"));
?>
<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="ru"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="ru"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="ru"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="ru"> <!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<script src="<?=MARKUP?>js/libs/modernizr.min.js"></script>
		<meta name="viewport" content="width=device-width"/>

		<title><?$APPLICATION->ShowTitle()?></title>
		<?$APPLICATION->ShowHead();?>
		<link rel="icon" href="<?=MARKUP?>favicon.ico" type="image/x-icon" />
		<?$APPLICATION->SetAdditionalCSS(MARKUP.'css/style.css');?>
		<?$APPLICATION->AddHeadScript(MARKUP.'js/libs/jquery.min.js');?>
		<?$APPLICATION->AddHeadScript('/local/templates/.default/js/script.js');?>
		<?$APPLICATION->AddHeadScript(MARKUP.'js/slick.min.js');?>
		<?$APPLICATION->AddHeadScript(MARKUP.'js/plugins.js');?>
		<?$APPLICATION->AddHeadScript(MARKUP.'js/jquery.knob.js');?>
		<?$APPLICATION->AddHeadScript(MARKUP.'js/script.js');?>
		<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
		<?if($APPLICATION->GetCurPage() == '/'){?>
			<script>
				$(function() {
					$('html').addClass('mainhtml'); // это для морды
				}); // DOM loaded
			</script>
			<?}?>
		<script type="text/javascript" src="//vk.com/js/api/openapi.js?115"></script>
		<script type="text/javascript">VK.init({apiId: 4525156, onlyWidgets: true});</script>
		<script>
			$(document).ready(function(){
				$(function() {
					var $items = $('.addBookTabs li');
					$items.click(function() {
						$items.removeClass('act');
						$(this).addClass('act');
						var index = $items.index($(this));
						$('.addBookContent>div').hide().eq(index).show();							
						return false;
					});
					if(location.href.indexOf('?updateSettings=yes') + 1)
					{
						$('#settings').trigger('click');
					}
				});
			});
		</script>
		<style>
			.addBookTabs{
				height: 34px;
				padding-bottom: 24px;
				margin-bottom: 10px;
				border-bottom: 1px solid #c3cccf;
			}
			.addBookTabs ul{
				list-style: none;
			}
			.addBookTabs ul li{
				float: left;
				width: 30%;
				margin: 0 5% 0 0;
			}			
			.addBookTabs ul li a{
				display: block;
				color: #ffffff;
				text-align: center;
				font: 14px/14px Arial, Helvetica, sans-serif;
				font-weight: normal;
				padding: 10px 0;
				text-decoration: none;
				background-image: linear-gradient(#f87579, #F79FA2);
			}
			.addBookTabs ul li.act a{
				  background-image: linear-gradient(#f5454b, #f87579);
				  font-weight: bold;
			}
			.b_bookfond_popup_content{
				// display: none;
			}
			.js_scroll {
				height: 680px;
			}
			.b_bookfond_popup_content .b-addbookform, .b_bookfond_popup_content .b-pdfadd{
				padding-top: 0;
				border-top: 0;
			}
			.history_import{
				margin-top: 20px;
			}
			
		</style>
	</head>

	<body>
		<base target="_self">
		<div class="addBookTabs">
			<ul>
				<li id="import" class="act"><a href="#">Импорт</a></li>
				<li id="settings"><a href="#">Настройки импорта</a></li>
			</ul>
		</div>
		<div class="addBookContent">
			<div class="b_bookfond_popup_content js_scroll">
				<div id="tbl_iblock_import_result_div"></div>
				<script language="JavaScript" type="text/javascript">
					var running = false;
					var sel = false;
					var oldNS = '';
					function DoNext(NS)
					{
						var interval = parseInt(document.getElementById('INTERVAL').value);
						var queryString = 'Import=Y'
							+ '&lang=<?echo LANG?>'
							+ '&<?echo bitrix_sessid_get()?>'
							+ '&INTERVAL=' + interval;
						;

						if(!NS)
						{
							queryString+='&SelectProfileInput='+document.getElementById('SelectProfileInput').value;
							queryString+='&URL_DATA_FILE='+document.getElementById('URL_DATA_FILE').value;
							if(document.getElementById('outFileAction_N').checked)
								queryString+='&outFileAction='+document.getElementById('outFileAction_N').value;
							if(document.getElementById('outFileAction_A').checked)
								queryString+='&outFileAction='+document.getElementById('outFileAction_A').value;
							queryString+='&id_library='+document.getElementById('id_library').value;
						}

						if(running)
						{
							// ShowWaitWindow();
							BX.ajax.post(
								'importBooks.php?'+queryString,
								NS,
								function(result){
									console.log(result);
									document.getElementById('tbl_iblock_import_result_div').innerHTML = result;
								}
							);
						}
					}
					function StartImport()
					{					
						if ($('#uploaded_file > a').attr('href') != "")	// Если выбран файл
						{
							sel = document.getElementById('SelectProfileInput').value;
							running = document.getElementById('start_button').disabled = true;
							DoNext();
						}
					}
					function EndImport()
					{
						running = false;
                        document.getElementById('start_button').disabled = running;
					}
					$(function() {
						$('#outFileAction_N, #outFileAction_A').change(function(){
							var checkbox;

							if (this.id == 'outFileAction_N'){
								checkbox = 'outFileAction_A';
							}else{
								checkbox = 'outFileAction_N';
							}
							
							
							if ($(this).prop("checked") == false){
								$("#"+checkbox).attr("checked","checked");
								$("#"+checkbox).parent('span').parent('.checkbox').addClass('checked');
							}else{
								$("#"+checkbox).removeAttr("checked");
								$("#"+checkbox).parent('span').parent('.checkbox').removeClass('checked');
							}						
						});
					});
				</script>
				<form method="POST" action="<?echo $APPLICATION->GetCurPage()?>?lang=<?echo htmlspecialcharsbx(LANG)?>" name="form1" id="form1" class="b-form b-form_common b-addbookform">
				<?/*
					$testString = "k1/vv1,vv2,vv3;k2/v2;k3/v3";
					$testString2 = "k4/v4";
					$arParts = explode(';', $testString);
					foreach ($arParts as $part)
					{
						$arRule = explode('/', $part);
						$arSubRule = explode(',', $arRule[1]);
						foreach ($arSubRule as $subRule)
						{
							$arVals[] = $arRule[0]." - ".$subRule;
						}
						$arFields[] = implode(",", $arVals);
						if (!empty($record[$arRule[0]][$arRule[1]]))
						{
							$arFields[$code] = $record[$arRule[0]][$arRule[1]];
							break;														// Если нашли элемент, заканчиваем перебор, иначе ищем какое-либо значение в следующем условии
						}
					}
					pre($arFields,1);*/
				?>

					<input type="hidden" id="INTERVAL" name="INTERVAL" class="input b-yeartb" value="<?echo intval($INTERVAL)?>" />
					<div class="fieldrow nowrap">
						<div class="fieldcell iblock">
							<label for="id_library">Библиотека:</label>
							<div class="field validate">
								<?echo nebImportXMLHelp::ShowSelectBoxLibrary('id_library')?>
							</div>
						</div>
					</div>
					<div class="fieldrow nowrap">
						<div class="fieldcell iblock">
							<? if (!($profile = nebImportXMLHelp::ShowActiveSettings()))
								$profile = "НЕ ДОБАВЛЕН! Будет использоваться стандартный!";?>
							<label for="NAME">Профиль настроек: <?=$profile?></label>
						</div>
					</div>
					<?/*<div class="fieldrow nowrap">
						<div class="fieldcell iblock">
							<label for="INTERVAL"><?echo GetMessage("IBLOCK_CML2_INTERVAL")?>:</label>
							<div class="field validate">
								<input type="text" id="INTERVAL" name="INTERVAL" class="input b-yeartb" value="<?echo intval($INTERVAL)?>" />
							</div>
						</div>
					</div>*/?>
					<div class="b_search_set clearfix">
						<div class="checkwrapper b-search_lib">
							<input class="checkbox" type="checkbox" name="outFileAction" checked="checked" value="N" id="outFileAction_N"><label for="outFileAction_N" class="black">Новые добавить, существующие обновить</label>
						</div>
						<div class="checkwrapper b-search_lib">
							<input class="checkbox" type="checkbox" name="outFileAction" value="A" id="outFileAction_A"><label for="outFileAction_A" class="black">Новые добавить, существующие не изменять</label>
						</div>
					</div> <!-- /.b_search_set -->
					<div class="fieldrow nowrap">
						<div class="fieldcell iblock b-pdfadd">
							<?$APPLICATION->IncludeComponent(
								"notaext:plupload",
								"books_import",
								array(
									"MAX_FILE_SIZE" => "1024",
									"FILE_TYPES" => "xml",
									"DIR" => "tmp_books_xml",
									"FILES_FIELD_NAME" => "books_xml",
									"MULTI_SELECTION" => "N",
									"CLEANUP_DIR" => "Y",
									"UPLOAD_AUTO_START" => "Y",
									"RESIZE_IMAGES" => "N",
									"UNIQUE_NAMES" => "Y",
									// "ALREADY_UPLOADED_FILE" => $arResult['FORM']['pdfLink']
								),
								false
							);?>
						</div>
					</div>
					<div class="fieldrow nowrap fieldrowaction">
						<div class="fieldcell">
							<div class="field clearfix">
								<input type="button" id="stop_button" value="<?echo GetMessage("IBLOCK_CML2_STOP_IMPORT")?>" OnClick="EndImport();" class="formbutton gray right" />
								<input type="button" id="start_button" value="<?echo GetMessage("IBLOCK_CML2_START_IMPORT")?>" OnClick="StartImport();" class="formbutton left" type="submit">
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="b_bookfond_popup_content js_scroll">
				<?$APPLICATION->IncludeComponent(
					"neb:import.settings.form",
					"",
					array(),
					false
				);?>
			</div>
		</div>
	</body>
</html>