<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Main\Loader;
Loader::includeModule("iblock");
$arFilter = array(
	"IBLOCK_ID" => 15,
);

$arResult["ajax_catalog_bbk"] = (isset($_REQUEST["ajax_catalog_bbk"]) && $_REQUEST["ajax_catalog_bbk"] == "Y");
$arResult["ajax_catalog_section_bbk"] = (isset($_REQUEST["ajax_catalog_section_bbk"]))?intval($_REQUEST["ajax_catalog_section_bbk"]):0;
$arResult["section_bbk_depth"] = (isset($_REQUEST["bbk_d"]))?intval($_REQUEST["bbk_d"]):0;
$arResult["find_catalog_bbk"] = (isset($_REQUEST["find_catalog_bbk"])) ? htmlspecialchars(trim($_REQUEST["find_catalog_bbk"])) : "";
$arSectionCount = array();
$arNavParams = array();

if($arResult["ajax_catalog_section_bbk"] <= 0)
{
	if(!$arResult["ajax_catalog_bbk"])
		$arNavParams["iNumPage"] = 1;
	$arNavParams["nPageSize"] = 6;
	$bIntCount = false;
	if(empty($arResult["find_catalog_bbk"]))
		$arFilter["SECTION_ID"] = false;
}
else
{
	$arFilter["SECTION_ID"] = $arResult["ajax_catalog_section_bbk"];
}

$arResult["SECTIONS"] = array();

if(!empty($arResult["find_catalog_bbk"]) && $arResult["ajax_catalog_section_bbk"] <= 0)
{
	$obSearch = new \Evk\Books\CSearch();
	$arSearchFilter = array (
		"QUERY" => $arResult["find_catalog_bbk"],
		"SITE_ID" => SITE_ID,
		"MODULE_ID" => "iblock",
		"PARAM1" => "collections",
		"PARAM2" => 15,
		"CHECK_DATES" => "N",
	);
	$obSearch->Search($arSearchFilter);
	if ($obSearch->errorno==0)
	{
		while($arItem = $obSearch->GetNext())
		{
			$arItem["~ITEM_ID"] = preg_replace("@\D@", "", $arItem["ITEM_ID"]);
			$arFilter["=ID"][] = $arItem["~ITEM_ID"];
			$arSearchResult[$arItem["~ITEM_ID"]] = $arItem;
		}
		$findSphinxError = count($arSearchResult) ? 0 : 1;
	}
	else
	{
		$findSphinxError = $obSearch->errorno;
	}
}
if($rsData = CIBlockSection::GetList(array("UF_LAST"=>"ASC"), $arFilter, false, array("UF_*"), false))
{
	while($arData = $rsData->Fetch())
	{
		if($arData["UF_LAST"])
		{
			$arData["AJAX_URL"] = str_replace(
				array("#SITE_DIR#", "#SECTION_ID#"),
				array("", $arData["ID"]),
				$arData["SECTION_PAGE_URL"]
			);
		}
		else
		{
			$arData["AJAX_URL"] = $APPLICATION->GetCurPageParam(
				sprintf('ajax_catalog_section_bbk=%d&bbk_d=%d',
					$arData["ID"],
					$arData["DEPTH_LEVEL"]),
				array("ajax_catalog_section_bbk", "bbk_d")
			);
		}
		$arResult["SECTIONS"][] = $arData;
	}
}


//\Evk\Books\Books::p($arResult["INDEXES"]);
//\Evk\Books\Books::p($arResult["SECTIONS"]);

//$obSection->GetBooksCount($arResult["SECTIONS"]);

$arResult["SHOW_NEXT_URL"] = ($rsData->NavPageNomer < $rsData->NavPageCount) ? $APPLICATION->GetCurPageParam(sprintf('PAGEN_1=%d&ajax_catalog_bbk=Y', $rsData->NavPageNomer+1), array("PAGEN_1", "catalog_bbk")) : false;

if($arResult["ajax_catalog_bbk"] || $arResult["ajax_catalog_section_bbk"])
	$APPLICATION->RestartBuffer();

$this->IncludeComponentTemplate();

if($arResult["ajax_catalog_bbk"] || $arResult["ajax_catalog_section_bbk"])
	exit;
?>