<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?if(!$arResult["ajax_catalog_section_bbk"]):?>
	<?if(!$arResult["ajax_catalog_bbk"]):?>
		<div class="wrapper">
			<section class="b-inner clearfix">
				<h1>Каталог ББК</h1>
				<form class="b-collection_search clearfix">
					<input name="find_catalog_bbk" class="b-collection_tb iblock" type="text" placeholder="Поиск по названию индекса" value="<?=$arResult["find_catalog_bbk"]?>" kl_virtual_keyboard_secure_input="on" />
					<input type="submit" value="Искать" class="b-collection_bt bt_typelt iblock" />
				</form>
	<?endif;?>
	<div class="b-catalog_bbk">
<?endif;?>
<ul>
<?foreach($arResult["SECTIONS"] as $arSection):?>
	<li<?=sprintf(' id="section%d"', $arSection["ID"])?>>
		<a class="b-section-href<?=($arSection["UF_LAST"])?' b-section-last':''?>" href="<?=$arSection["AJAX_URL"]?>"><?=$arSection["NAME"]?>
			<span><?=$arSection["XML_ID"]?></span>
			<?if($arSection["DEPTH_LEVEL"] > 2 || $arSection["UF_LAST"]):?>
				<sup><?=(!empty($arSection["UF_COUNT"])) ? $arSection["UF_COUNT"] : 0;?></sup>
			<?endif;?>
		</a>
	</li>
<?endforeach;?>
</ul>
<?if(!$arResult["ajax_catalog_section_bbk"]):?>
	</div>
	<?if($arResult["SHOW_NEXT_URL"]):?>
		<div class="b-wrap_more" style="margin-top: 40px;">
			<a class="ajax_add b-morebuttton" href="<?=$arResult["SHOW_NEXT_URL"]?>">Показать ещё</a>
		</div>
	<?endif;?>
	<?if(!$arResult["ajax_catalog_bbk"]):?>
			</section>
		</div>
	<?endif;?>
<?endif;?>
<script type="text/javascript">
	$(function(){
		$('a.b-section-href')
			.unbind('click')
			.click(function() {
				var link = $(this);
				if(link.hasClass('loaded'))
				{
					link.nextAll('ul').remove();
					link.removeClass('loaded');
				}
				else
				{
					BX.showWait(link.closest('li').attr('id'));
					$.post(link.attr('href'), function(data){
						link.after(data);
						link.addClass('loaded');
						BX.closeWait();
					});
				}
				return false;
			}
		);
		$('a.b-section-last').unbind('click');
	});
</script>