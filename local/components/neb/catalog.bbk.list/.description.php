<?
	if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

	$arComponentDescription = array(
		"NAME" => "Каталог ББК список",
		"DESCRIPTION" => "",
		"ICON" => "/images/icon.gif",
		"SORT" => 10,
		"CACHE_PATH" => "Y",
		"PATH" => array(
			"ID" => "catalog_bbk",
			"CHILD" => array(
				"ID" => "catalog_bbk_list",
				"NAME" => "Каталог ББК список",
				"SORT" => 30,
			)
		),
	);

?>