<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
	use \Bitrix\Main\Localization\Loc as Loc;
	Loc::loadMessages(__FILE__);
?>
<section class="innersection innerwrapper searchempty clearfix">
	<div class="b-mainblock left">
		<?$APPLICATION->ShowViewContent('lib_menu')?>
		<?
			$APPLICATION->IncludeComponent(
				"neb:search.page.viewer.left",
				"",
				Array(
					"PARAMS" => array_merge($arParams, array('TITLE_MODE_BLOCK' => 'Отобразить книги плиткой', 'TITLE_MODE_LIST' => 'Отобразить книги списком')),
					"RESULT" => $arResult,
				),
				$component
			);
		?>

	</div>
	<div class="b-side right b-sidesmall">
		<?
			$APPLICATION->IncludeComponent(
				"neb:library.right_counter",
				"",
				Array(
					"IBLOCK_ID" => $arParams['IBLOCK_ID'],
					"LIBRARY_ID" => $arResult['ITEM']["ID"],
					"CACHE_TIME" => $arParams["CACHE_TIME"],
					"COLLECTION_URL" => $arParams['COLLECTION_URL'],
				),
				$component
			);
		?>
	</div>
</section>