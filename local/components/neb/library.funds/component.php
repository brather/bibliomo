<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/**
 * global $APPLICATION;
 */
if(empty($arParams['CODE']))
	return false;

if(empty($arParams['IBLOCK_ID']))
	return false;

$arParams["AJAX_GET_FUNDS"] = (isset($_REQUEST["AJAX_GET_FUNDS"]) && $_REQUEST["AJAX_GET_FUNDS"] == "Y");

CPageOption::SetOptionString("main", "nav_page_in_session", "N");

$arParams["DISPLAY_TOP_PAGER"] = $arParams["DISPLAY_TOP_PAGER"]=="Y";
$arParams["DISPLAY_BOTTOM_PAGER"] = $arParams["DISPLAY_BOTTOM_PAGER"]!="N";

if (!CModule::IncludeModule('evk.books')) return false;
use Evk\Books\IBlock\Element;

$arResult = Element::getList(
	array('IBLOCK_ID' => $arParams['IBLOCK_ID'], 'CODE' => $arParams['CODE']),
	1,
	array(
		'*',
	)
);

if(empty($arResult['ITEM']))
{
	$this->AbortResultCache();
	@define("ERROR_404", "Y");
	CHTTP::SetStatus("404 Not Found");
	return;
}

use Evk\Books\SearchQuery;

$arParams['ITEM_COUNT'] = 15;
$arParams['MODE'] = empty($_REQUEST['mode']) ? 'list' : trim($_REQUEST['mode']);

$by = trim(htmlspecialcharsEx($_REQUEST['by']));
$order = trim(htmlspecialcharsEx($_REQUEST['order']));

if(in_array($order, array("asc", "desc")) && in_array($by, array("name", "property_author")))
	$arSort = array($by => $order);
else
	$arSort = array();

$arParams["PAGE_NUM"] = (!isset($_REQUEST["PAGEN_1"])) ? 1 : intval($_REQUEST["PAGEN_1"]);

	$arNavParams = array(
		"nPageSize" => $arParams['ITEM_COUNT'],
		"bDescPageNumbering" => $arParams["PAGER_DESC_NUMBERING"],
		"bShowAll" => $arParams["PAGER_SHOW_ALL"],
		'iPageNum' => $arParams["PAGE_NUM"],
	);
	$arNavigation = CDBResult::GetNavParams($arNavParams);
	if($arNavigation["PAGEN"]==0 && $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"]>0)
		$arParams["CACHE_TIME"] = $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"];

$obSearch = new SearchQuery($arResult, $arParams);

$obSearch->setPageLimit($arParams['ITEM_COUNT']);
$arFilter = array(
	"PROPERTY_LIBRARIES" => array($arResult["ITEM"]['ID']),
);
$arSelect = Array(
	"ID",
	"NAME",
	"DATE_ACTIVE_FROM",
	"PROPERTY_FILE",
	"DETAIL_PAGE_URL",
	"PROPERTY_STATUS",
	"PROPERTY_LIBRARIES",
	"DATE_CREATE"
);

$obSearch->GetDataForSmartFilter();

$arReturn = $obSearch->FindBooks($arSort, $arFilter, false, $arNavParams, $arSelect);

$arResult['NEXT_PAGE'] = \Evk\Books\PageNavigation::GetNextPage($obSearch->obNavigate, "AJAX_GET_FUNDS=Y", array('AJAX_GET_FUNDS'));

$arResult = array_merge_recursive($arResult, $arReturn);

if($arParams["AJAX_GET_FUNDS"])
	$APPLICATION->RestartBuffer();

$this->IncludeComponentTemplate();

if($arParams["AJAX_GET_FUNDS"])
	exit;

if(!empty($arResult['ITEM']['NAME']))
	$APPLICATION->SetTitle($arResult['ITEM']['NAME']);

?>