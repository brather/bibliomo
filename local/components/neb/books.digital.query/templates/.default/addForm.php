<?	require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");?>

<div class="b-addcomment">
	<form method="post" action="<?=$APPLICATION->GetCurPage();?>" class="b-form b-form_common b-formcomment" >
		<?=bitrix_sessid_post()?>
		<input type="hidden" name="action" value="addDigitizationQuery" />
		<h3>Добавить комментарий</h3>
		<p>Обязательно опишите почему вам нужна данная книга</p>
		<div class="fieldrow nowrap">
			<div class="fieldcell iblock ">

				<div class="field validate">
					<textarea   data-required="required" class="textarea" name="comment" id="settings10" data-minlength="2" data-maxlength="800"></textarea>
					<em class="error required">Поле обязательно для заполнения</em>
				</div>

			</div>
		</div>

		<p>Ваш комментарий может ускорить процесс оцифровки</p>
		<div class="clearfix">
			<button type="submit" value="1" class="formbutton small">Добавить</button>
			<a class="formbutton gray btrefuse small" href="#">Отмена</a>
		</div>
	</form>
</div>