<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<div class="b-libfilter right_menu_collections" <?/*?>data-type="<?=$arParams['TYPE']?>"<?*/?>>
	<h4><?=GetMessage("USER_RIGHT_TITLE");?></h4>
	<?/*
	#клиент попросил убрать
	?>
	<div class="b-libfilter_item<?=(empty($arParams['COLLECTION_ID']) and $arParams['TYPE'] == 'books' and empty($arParams['CURRENT_MENU']))? ' current' : ''?>"><span class="b-libfilter_num" id="all_books"><?=$arResult['ALL_BOOKS_CNT']?></span><a href="<?=$arParams['ALL_BOOKS_URL']?>" class="b-libfilter_name">Все книги</a></div>
	<div class="b-libfilter_item<?=(!empty($arParams['CURRENT_MENU']) and $arParams['CURRENT_MENU'] == 'reading') ? ' current': ''?>"><span class="b-libfilter_num"><?=$arResult['READING_BOOKS_CNT']?></span><a href="<?=$arParams['READING_BOOKS_URL']?>" class="b-libfilter_name" id="reading_books">Сейчас читаю</a></div>
	<div class="b-libfilter_item<?=(!empty($arParams['CURRENT_MENU']) and $arParams['CURRENT_MENU'] == 'read') ? ' current': ''?>"><span class="b-libfilter_num"><?=$arResult['READ_BOOKS_CNT']?></span><a href="<?=$arParams['READ_BOOKS_URL']?>" class="b-libfilter_name" id="read_books">Прочитал</a></div>
	<?*/?>
		<form action="<?=$this->__folder?>/remove.php">
			<div class="js_sortable_del list-collections-menu">
				<? if ( !empty($arResult['COLLECTIONS']) ) {
					if($_REQUEST['reloadRightCollection'] == 'Y' and $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')
						$APPLICATION->RestartBuffer();

					foreach($arResult['COLLECTIONS'] as $arItem)
					{
						?>
						<div class="b-libfilter_item b-libfilter_useritem<?=(!empty($arParams['COLLECTION_ID']) and $arParams['COLLECTION_ID'] == $arItem['ID']) ? ' current':''?>">
							<span class="b-libfilter_num"><?=(int)$arItem['cnt']?></span>
							<div class="checkwrapper">
								<a href="<?=$arItem['URL']?>" class="black b-libfilter_name"><?=$arItem['UF_NAME']?></a><input class="checkbox" type="checkbox" name="right_menu_collections[]" value="<?=$arItem['ID']?>">
							</div>
						</div>
						<?
					}

					if($_REQUEST['reloadRightCollection'] == 'Y' and $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')
						exit();
				}
				elseif ( $_REQUEST['reloadRightCollection'] == 'Y' and $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' )
				{
					$APPLICATION->RestartBuffer();
					exit();
				}?>
			</div>
		</form>

	<div class="b-libfilter_action clearfix">
		<a href="#" class="b-libfilter_remove"></a>
		<form action="<?=ADD_COLLECTION_URL?>add.php" method="post" class="b-selectionadd collection" data-callback="reloadRightMenuBlock()">
			<input type="submit" value="+" data-collection="ajax_favs.html" class="b-selectionaddsign">
			<span class="b-selectionaddtxt "><?=GetMessage("USER_RIGHT_CREATE_COLLECTION");?></span>
			<input type="text" class="input hidden" name="name">
		</form>
	</div>
	<div class="b-removepopup"><p><?=GetMessage("USER_RIGHT_REMOVE_COLLECTION");?></p><a class="formbutton btremove" href="#"><?=GetMessage("USER_RIGHT_REMOVE");?></a><a class="formbutton gray" href="#"><?=GetMessage("USER_RIGHT_RETAIN");?></a></div>
</div><!-- /.b-side_libfilter -->
