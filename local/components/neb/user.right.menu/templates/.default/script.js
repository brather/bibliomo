$(function() {
	$('.right_menu_collections .btremove').click(function() {
		var form = $(this).closest('.right_menu_collections').find('form');

		$.ajax({
			url: form.attr('action'),
			method: 'POST',
			data: form.serialize(),
			success: function(html){
				reloadRightMenuBlock();
			}
		}); 
	});
});

function reloadRightMenuBlock(){
	$('.b-libfilter_remove:visible').hide();
	$.ajax({
		//url: '/local/components/neb/user.right.menu/templates/.default/reload.php',
		url: window.location.href,
		method: 'GET',
		data: {
		reloadRightCollection : 'Y'
		//, type: $('.right_menu_collections').data('type')
		},
		success: function(html){
			$('.right_menu_collections .list-collections-menu').html(html);
			$('input.checkbox:not(".custom")').replaceCheckBox();
			$('.b-libfilter').libFilter();
		}
	}); 
}