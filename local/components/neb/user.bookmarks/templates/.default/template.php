<?
	if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
?>
<div class="b-filter">
	<div class="b-filter_wrapper">
		<a href="#" class="sort sort_opener"><?=GetMessage("BOOKMARKS_SORT");?></a>
		<span class="sort_wrap">
			<a <?=SortingExalead("UF_BOOK_AUTHOR")?>><?=GetMessage("BOOKMARKS_SORT_AUTHOR");?></a>
			<a <?=SortingExalead("UF_BOOK_NAME")?>><?=GetMessage("BOOKMARKS_SORT_NAME");?></a>
			<a <?=SortingExalead("UF_DATE_ADD")?>><?=GetMessage("BOOKMARKS_SORT_DATE");?></a>
		</span>
		<span class="b-filter_act">
			<span class="b-filter_show"><?=GetMessage("BOOKMARKS_SHOW");?></span>
			<a href="<?=$APPLICATION->GetCurPageParam("pagen=10", array("pagen", "dop_filter"));?>" class="b-filter_num b-filter_num_paging<?=($arParams['ITEM_COUNT'] == 10 and !$arParams['LONG_PAGE']) ? ' current' : ''?>">10</a>			
			<a href="<?=$APPLICATION->GetCurPageParam("pagen=25", array("pagen", "dop_filter"));?>" class="b-filter_num b-filter_num_paging<?=($arParams['ITEM_COUNT'] == 25 and !$arParams['LONG_PAGE']) ? ' current' : ''?>">25</a>			
			<a href="<?=$APPLICATION->GetCurPageParam("pagen=30", array("pagen", "dop_filter"));?>" class="b-filter_num b-filter_num_paging<?=($arParams['ITEM_COUNT'] == 30 and !$arParams['LONG_PAGE']) ? ' current' : ''?>">30</a>		
		</span>
	</div>
</div><!-- /.b-filter -->
<a href="#" class="set_opener iblock right"><?=GetMessage("BOOKMARKS_SETTINGS");?></a>
<div class="b-bookmark_list">
	<?
		if(!empty($arResult['ITEMS']))
		{
			foreach($arResult['ITEMS'] as $arItem)
			{
				$book = $arResult['EVK_BOOKS'][$arItem['UF_BOOK_ID']];
				if(empty($book))
					continue;
			?>
				<div class="b-note_item removeitem b-result-docitem" id="<?=$arItem['ID']?>">
					<div class="meta minus">
						<div class="b-hint rel"><span><?=GetMessage("BOOKMARKS_REMOVE");?></span> <?=GetMessage("BOOKMARKS_FROM_BOOKMARKS");?></div>
						<a class="b-bookadd fav" data-remove="removeonly" data-callback="reloadRightMenuBlock()" data-url="<?=$APPLICATION->GetCurPageParam('id='.$arItem['ID'].'&action=remove', array("id", "action"));?>" href="#"></a>
					</div> <!-- meta -->

					<div class="b-note">
						<a href="<?=$book['DETAIL_PAGE_URL']?>" class="b-bookboardphoto iblock popup_opener ajax_opener coverlay" data-width="955"><img class="loadingimg" src="<?=$book['IMAGE_URL_PREVIEW']?>" alt=""></a>
						<div class="iblock b-notes_txt">
							<p><?=$arItem['UF_PREVIEW']?></p>
							<a target="_blank" href="<?=$book['VIEWER_URL']?>?page=<?=$arItem['UF_NUM_PAGE']?>" class="button_mode b-bookmarklink"><?=GetMessage("BOOKMARKS_ON_PAGE");?> <?=$arItem['UF_NUM_PAGE']?></a>
							<div class="clearfix rel b-notes_act">
								<ul class="b-resultbook-info left">
									<li><span><?=GetMessage("BOOKMARKS_AUTHOR");?>:</span> <a href="<?=$book['SEARCH_AUTHOR_URL']?>"><?=$book['PROPERTY_AUTHOR_VALUE']?></a></li>
									<li><span><?=GetMessage("BOOKMARKS_BOOK");?>:</span> <a href="<?=$book['DETAIL_PAGE_URL']?>" class="popup_opener ajax_opener coverlay" data-width="955"><?=$book['NAME']?></a></li>
								</ul>
								<div class="iblock rel b-myselection_list">
									<a href="#" class="b-openermenu js_openmfavs" data-callback="reloadRightMenuBlock()" data-favs="<?=ADD_COLLECTION_URL?>list.php?t=bookmarks&id=<?=$arItem['ID']?>"><?=GetMessage("BOOKMARKS_MY_COLLECTIONS");?></a>
									<div class="b-favs">
										<form class="b-selectionadd collection" action="<?=ADD_COLLECTION_URL?>add.php" method="post">
											<input type="hidden" name="t" value="bookmarks"/>
											<input type="hidden" name="id" value="<?=$arItem['ID']?>"/>

											<input type="submit" class="b-selectionaddsign" value="+">
											<span class="b-selectionaddtxt"><?=GetMessage("BOOKMARKS_CREATE_COLLECTION");?></span>
											<input type="text" name="name" class="input hidden">
										</form>
									</div><!-- /.b-favs  -->
								</div> <!-- /.b-myselection_list-->

							</div>

						</div>

					</div><!-- /.b-quote -->

				</div><!-- /.b-note_item -->
			<?
			}
		}
	?>
</div><!-- /.b-quote_list -->
<?=$arResult['NAV_STRING']?>