<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentParameters = array(
	'GROUPS' => array(
	),
	'PARAMETERS' => array(
		'CACHE_TIME' => array(
			'DEFAULT' => 3600
		),
		"PLACEHOLDER" => array(
			"PARENT" => "BASE",
			"NAME" => 'Подсказка в поле поиска',
			"TYPE" => "STRING",
			"COLS" => 60,
			"DEFAULT" => "Поиск книг по всем разделам портала",
		),
	)
);
?>