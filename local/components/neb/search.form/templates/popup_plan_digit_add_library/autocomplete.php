<?
	define("STOP_STATISTICS", true);
	define("NOT_CHECK_PERMISSIONS", true);
	require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

	use \Bitrix\Main\Localization\Loc as Loc;
	Loc::loadMessages(__FILE__);

	$q = trim(htmlspecialcharsEx($_REQUEST['term'])); 
	if(empty($q))
		exit();

	CModule::IncludeModule('nota.exalead');

	use Nota\Exalead\SearchQuery;
	use Nota\Exalead\SearchClient;
	use Bitrix\NotaExt\Utils;

	if(check_bitrix_sessid()){
		header('Content-type: application/json');

		$query = new SearchQuery();
		$arResult = array();

		$query->getSuggestTitle($q);	
		$client = new SearchClient();
		$result = $client->getResult($query);
		if(!empty($result))
		{
			foreach($result as $v)
			{
				$_v = Utils::trimming_line($v, 60, '');
				$arResult[] = array('label' => $_v, 'original' => $v, 'category' => Loc::getMessage('LIB_SEARCH_FORM_AUTOCOMPLETE_BOOK'));
			}
		}

		$query->getSuggestAuthor($q);
		$client = new SearchClient();
		$result = $client->getResult($query);
		if(!empty($result))
		{
			foreach($result as $v)
			{
				$_v = Utils::trimming_line($v, 60, '');
				$arResult[] = array('label' => $_v, 'original' =>$v, 'category' => Loc::getMessage('LIB_SEARCH_FORM_AUTOCOMPLETE_AUTHOR'));
			}
		}

		if(!empty($arResult))
			echo json_encode($arResult);

	}
?>