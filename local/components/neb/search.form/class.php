<?
namespace Neb\Search;
use Bitrix\Main\Loader;
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
Loader::includeModule("evk.books");
class LibSearchFormComponent extends \CBitrixComponent
{
	/**
	 * шаблоны путей по умолчанию
	 * @var array
	 */
	protected $defaultUrlTemplates404 = array();
	/**
	 * страница шаблона
	 * @var string
	 */
	protected $page = '';

	/**
	 * переменные шаблонов путей
	 * @var array
	 */
	protected $componentVariables = array();

	/**
     * подготавливает входные параметры
     * @param array $arParams
     * @return array
     */

    public function onPrepareComponentParams($params)
    {
    	$parentResult = parent::onPrepareComponentParams($params);
		$result = array(
			'PAGE' => trim($params['PAGE']),
		);
	    $result["EXTENDED_FILTER"] = (isset($_REQUEST["extended_filter"]) && $_REQUEST["extended_filter"]=="Y")?"Y":"N";
		$result = array_merge($parentResult, $result);
		return $result;
	}



	/**
	 * определяет переменные шаблонов и шаблоны путей
	 */
	protected function setSefDefaultParams()
	{
		$this -> defaultUrlTemplates404 = array(
		    'index' => 'index.php',
		    'detail' => 'detail/#ELEMENT_ID#/'
		);
		$this -> componentVariables = array('ELEMENT_ID');
	}

	/**
	 * получение результатов
	 */
	protected function getResult()
	{
		$urlTemplates = array();

		if ($this->arParams['SEF_MODE'] == 'Y') {
			$variables = array();
			$urlTemplates = \CComponentEngine::MakeComponentUrlTemplates(
				$this->defaultUrlTemplates404,
				$this->arParams['SEF_URL_TEMPLATES']
			);
			$variableAliases = \CComponentEngine::MakeComponentVariableAliases(
				$this->defaultUrlTemplates404,
				$this->arParams['VARIABLE_ALIASES']
			);
			$this->page = \CComponentEngine::ParseComponentPath(
				$this->arParams['SEF_FOLDER'],
				$urlTemplates,
				$variables
			);

			if (strlen($this->page) <= 0)
				$this->page = 'template';

			\CComponentEngine::InitComponentVariables(
				$this->page,
				$this->componentVariables, $variableAliases,
				$variables
			);
		} else {
			$this->page = 'template';
		}

		$is_full_search = (isset($_REQUEST["is_full_search"]) && $_REQUEST["is_full_search"] == "Y");
		$s_strict = (isset($_REQUEST["s_strict"]) && $_REQUEST["s_strict"] == "Y");
		$s_neb = (isset($_REQUEST["s_neb"]) && $_REQUEST["s_neb"] == "Y");
		$obLogicContainer = \Evk\Books\QueryLogic::Create()->AddQueryArray($_REQUEST);
		$arLogic = array();
		/**
		 * @var $obLogic \Evk\Books\QueryLogic
		 */

		foreach($obLogicContainer as $obLogic)
		{

			$arLogic[] = array(
				"theme" => array(
					"fieldValue" => $obLogic->GetThemeOrigin(),
					"fieldName" => 'theme[]',
				),
				"text" => array(
					"fieldValue" => $obLogic->GetText(),
					"fieldName" => 'text[]',
				),
				"logic" => array(
					"fieldValue" => $obLogic->GetLogic(),
					"fieldName" => 'logic[]',
				),
			);
		}
		if(empty($arLogic))
		{
			$arLogic[] = array(
				"theme" => array(
					"fieldValue" => "",
					"fieldName" => 'theme[]',
				),
				"text" => array(
					"fieldValue" => "",
					"fieldName" => 'text[]',
				),
				"logic" => array(
					"fieldValue" => "",
					"fieldName" => 'logic[]',
				),
			);
		}
		$this -> arResult = array(
		   'FOLDER' => $this -> arParams['SEF_FOLDER'],
		   'URL_TEMPLATES' => $urlTemplates,
		   'VARIABLES' => $variables,
		   'ALIASES' => $variableAliases,
			'is_full_search' => $is_full_search,
			's_strict' => $s_strict,
			's_neb' => $s_neb,
			"arLogic" => $arLogic,
		);
	}

	/**
	 * выполняет логику работы компонента
	 */
	public function executeComponent()
	{

		if(isset($_REQUEST["publishyear"]) && is_numeric($_REQUEST["publishyear"]))
		{

			$selectYear = intval($_REQUEST["publishyear"]);
			if($selectYear >= 1000 && $selectYear <= date("Y"))
			{
				$_REQUEST["publishyear_prev"] = $_REQUEST["publishyear_next"] = $selectYear;
			}
		}
		try
		{
			$this -> setSefDefaultParams();
			$this -> getResult();
			$this -> includeComponentTemplate($this -> page);
		}
		catch (Exception $e)
		{
			ShowError($e -> getMessage());
		}
	}
}
?>
