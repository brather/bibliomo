$(document).ready(function(){
	CKEDITOR.replace( 'iDETAIL_TEXT', {
		'language': 'en'
	} );
});

$(document).on('click', '#preview_show_button', function(e){
	e.preventDefault();

	var form 		= $(this).closest('form');
	var actionTmp 	= form.attr('action');
	putEditorData();

	// Форме нужно указать, что она сабмитится на _blank и другую страницу.
	form.attr('target', '_blank');
	form.attr('action', $(this).data('preview'));
	form.submit();
	form.attr('action', actionTmp);
	form.removeAttr('target');

	return false;
});

function putEditorData()
{
	$('#iDETAIL_TEXT').val(CKEDITOR.instances['iDETAIL_TEXT'].getData());
}