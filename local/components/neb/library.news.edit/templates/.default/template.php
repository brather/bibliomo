<?
	if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
	$edit = isset($arResult['ITEM']);
	$APPLICATION->AddHeadScript('/local/tools/ckeditor/ckeditor.js');
?>
<div class="b-newsformpage">
	<span class="right regfield_label">* – обязательные поля</span>
	<h2 class="mode"><?=$edit ? 'редактирование новости библиотеки' : 'добавление новости библиотеки'?></h2>
	<form action="<?=$APPLICATION->GetCurPage();?>" class="b-form b-form_common b-newsform" method="post" name="form_edit_news">
		<input type="hidden" name="ID" value="<?=$edit ? $arResult['ITEM']['ID'] : ''?>"/>
		<input type="hidden" name="CODE" value="<?=$arResult['ITEM']['CODE']?>" />
		<input type="hidden" name="LIBRARY_ID" value="<?=$arResult['LIBRARY']['ID']?>">
		<?=bitrix_sessid_post()?>

		<div class="fieldrow nowrap">
			<div class="fieldcell iblock">
				<em class="hint">*</em>
				<label for="inputNAME">Заголовок</label>
				<div class="field validate">
					<em style="display: none;" class="error required">Заполните</em>
					<input type="text" data-required="required" value="<?=$arResult['ITEM']['NAME']?>" id="inputNAME" data-maxlength="255" data-minlength="2" name="NAME" class="input" >
				</div>
			</div>
		</div>
		<div class="fieldrow nowrap">
			<div class="fieldcell iblock mt10">
				<label for="iPREVIEW_TEXT">Краткое описание</label>
				<div class="field validate">
					<textarea name="PREVIEW_TEXT" id="iPREVIEW_TEXT" data-maxlength="1500" data-minlength="2" class="input tasmall"><?=strip_tags($arResult['ITEM']['~PREVIEW_TEXT'])?></textarea>
				</div>
			</div>
		</div>
		<?$APPLICATION->IncludeComponent(
			"notaext:plupload",
			"library_news_detail",
			array(
				"MAX_FILE_SIZE" => "24",
				"FILE_TYPES" => "jpg,jpeg,png",
				"DIR" => "libnews_detail",
				"FILES_FIELD_NAME" => "DETAIL_PICTURE",
				"MULTI_SELECTION" => "N",
				"CLEANUP_DIR" => "N",
				"UPLOAD_AUTO_START" => "Y",
				"RESIZE_IMAGES" => "Y",
				"RESIZE_WIDTH" => "300",
				"RESIZE_HEIGHT" => "300",
				"RESIZE_CROP" => "N",
				"RESIZE_QUALITY" => "98",
				"UNIQUE_NAMES" => "Y",
				'DETAIL_PICTURE' => $arResult['ITEM']['DETAIL_PICTURE']['SRC']
			),
			false
		);?>

		<div class="fieldrow nowrap">
			<div class="fieldcell iblock mt28">
				<em class="hint">*</em>
				<label for="iDETAIL_TEXT">Текст новости</label>
				<div class="field validate">
					<em style="display: none;" class="error required">Заполните</em>
					<em style="display: none;" class="error maxlength">Не более 50000 символов</em>
					<em style="display: none;" class="error minlength">Не менее 2 символов</em>
					<textarea data-required="required" data-maxlength="50000" data-minlength="2" name="DETAIL_TEXT" class="input" id="iDETAIL_TEXT"><?=$arResult['ITEM']['~DETAIL_TEXT']?></textarea>
				</div>
			</div>
		</div>
		<div class="fieldrow nowrap">
			<div class="fieldcell iblock mt10">
				<em class="hint">*</em>
				<label for="iDETAIL_TEXT">Дата новости</label>
				<div class="field validate">
					<em style="display: none;" class="error required">Заполните</em>
					<?=CalendarDate("DATE_ACTIVE_FROM", $arResult['ITEM']['DATE_ACTIVE_FROM'], "form_edit_news", "15", 'data-required="required"')?>
				</div>
			</div>
		</div>
		<?$APPLICATION->IncludeComponent(
			"notaext:plupload",
			"library_news_gallery",
			array(
				"MAX_FILE_SIZE" => "24",
				"FILE_TYPES" => "jpg,jpeg,png",
				"DIR" => "libnews_gallery",
				"FILES_FIELD_NAME" => "GALLERY",
				"MULTI_SELECTION" => "Y",
				"CLEANUP_DIR" => "N",
				"UPLOAD_AUTO_START" => "Y",
				"RESIZE_IMAGES" => "Y",
				"RESIZE_WIDTH" => "600",
				"RESIZE_HEIGHT" => "340",
				"RESIZE_CROP" => "Y",
				"RESIZE_QUALITY" => "98",
				"UNIQUE_NAMES" => "Y",
				'GALLERY' => $arResult['ITEM']['GALLERY']
			),
			false
		);?>
		<?/*<div class="fieldcell nowrap photorow">
			<label>Прикрепить фотогалерею</label>
			<div class="field validate">
				<div class="setscan iblock">
					<input type="file" class="photofile" name="" id="" multiple >
					<a href="#">Загрузить изображение (одно или несколько)</a>
					<div class="setphoto_lb">или перетащите на это поле</div>
				</div>
			</div>
			<div class="b_photobl">
				<div class="b_photo iblock">
					<img src="./pic/pic_43.jpg" alt="photo">
				</div>
				<div class="descrphoto iblock">
					<label for="settings13">Описание фотографии</label>
					<div class="field validate">
						<textarea class="input" data-minlength="2" data-maxlength="1500" value="" id="settings13" name=""></textarea>
					</div>
				</div>
			</div>
			<div class="b_photobl">
				<div class="b_photo iblock">
					<img src="./pic/pic_44.jpg" alt="photo">
					<div class="progressbar"><div class="progress"><span style="width:50px;"></span></div><div class="text">Загружается <span class="num">33</span>%</div></div>
				</div>
				<div class="descrphoto iblock">
					<label for="settings12">Описание фотографии</label>
					<div class="field validate">
						<textarea class="input" rows="1" cols="1" data-minlength="2" data-maxlength="1500" value="" id="settings12" name=""></textarea>
					</div>
				</div>
			</div>
		</div>*/?>

		<div class="fieldrow nowrap">
			<div class="fieldcell iblock w700">
				<label for="iPROPERTY_YOUTUBE_VALUE">Прикрепить видео (вставьте ссылку на www.youtube.com)</label>
				<div class="field validate">
					<input type="text" name="YOUTUBE" class="input" id="iPROPERTY_YOUTUBE_VALUE"  value="<?=$arResult['ITEM']['PROPERTY_YOUTUBE_VALUE']?>" >
				</div>
			</div>
		</div>

		<?$APPLICATION->IncludeComponent(
			"notaext:plupload",
			"library_news_file",
			array(
				"MAX_FILE_SIZE" => "24",
				"FILE_TYPES" => "jpg,jpeg,png,doc,docx,ppt,xls,xlsx,pdf",
				"DIR" => "libnews_file",
				"FILES_FIELD_NAME" => "FILE",
				"MULTI_SELECTION" => "N",
				"CLEANUP_DIR" => "N",
				"UPLOAD_AUTO_START" => "Y",
				"RESIZE_IMAGES" => "Y",
				"RESIZE_WIDTH" => "800",
				"RESIZE_HEIGHT" => "600",
				"RESIZE_CROP" => "Y",
				"RESIZE_QUALITY" => "98",
				"UNIQUE_NAMES" => "N",
				'FILE' => $arResult['ITEM']['FILE']
			),
			false
		);?>
		<hr>
		<div class="b-newspublication">
			<a href="" class="right" id="preview_show_button" data-preview="/library/<?=$arResult['LIBRARY']['CODE']?>/news-preview-preview/">Посмотреть превью новости</a>
			<div class="checkwrapper ">
				<input class="checkbox" type="checkbox" name="ACTIVE" value="Y" <?=$arResult['ITEM']['ACTIVE'] == 'Y' ? 'checked="checked"' : ''?> id="iACTIVE"><label for="iACTIVE" class="lite">Опубликовать на сайте</label>
			</div>
		</div>

		<div class="fieldrow nowrap fieldrowaction">
			<div class="fieldcell iblock w700">
				<div class="field clearfix">
					<button class="formbutton" value="1" type="submit" onclick="putEditorData(); return true;">сохранить</button>
					<span class="b-listlink right">Перейти к <a href="<?=$arParams['URL_LIST']?>">списку новостей</a></span>
				</div>
			</div>
		</div>
	</form>
</div><!-- /.b-newsform-->