<?
    $MESS['LIB_SEARCH_PAGE_VIEWER_LEFT_AUTH'] = 'Authorization required';
    $MESS['LIB_SEARCH_PAGE_VIEWER_LEFT_ADD_TO_PLAN'] = 'Добавить в План оцифровки';
    $MESS['LIB_SEARCH_PAGE_VIEWER_LEFT_REMOVE_FROM_PLAN'] = 'Удалить из Плана оцифровки';
    $MESS['LIB_SEARCH_PAGE_VIEWER_LEFT_REMOVE_FROM_LIB'] = '<span>Удалить</span> из Моей библиотеки';
    $MESS['LIB_SEARCH_PAGE_VIEWER_LEFT_ADD_TO_LIB'] = '<span>Добавить</span> в Мою библиотеку';
    $MESS['LIB_SEARCH_PAGE_VIEWER_LEFT_AUTHOR'] = 'Author';
    $MESS['LIB_SEARCH_PAGE_VIEWER_LEFT_YEAR'] = 'Year of publication';
    $MESS['LIB_SEARCH_PAGE_VIEWER_LEFT_PAGES'] = 'Pages';
    $MESS['LIB_SEARCH_PAGE_VIEWER_LEFT_PUBLISH'] = 'Publisher';
    $MESS['LIB_SEARCH_PAGE_VIEWER_LEFT_SOURCE'] = 'Source';

    $MESS['LIB_SEARCH_PAGE_VIEWER_LEFT_SORT_BY_AUTHOR'] = 'Author';
    $MESS['LIB_SEARCH_PAGE_VIEWER_LEFT_SORT_BY_NAME'] = 'Main title';
    $MESS['LIB_SEARCH_PAGE_VIEWER_LEFT_SORT_BY_DATE'] = 'Date';
    $MESS['LIB_SEARCH_PAGE_VIEWER_LEFT_SORT_BY_SHOW'] = 'Show';

    $MESS['LIB_SEARCH_PAGE_VIEWER_LEFT_SORT'] = 'Sort';
	
	$MESS['LIB_SEARCH_PAGE_VIEWER_LEFT_READ'] = 'read';
	$MESS['LIB_SEARCH_PAGE_VIEWER_LEFT_SHOW_ALL'] = 'Show all search results on page';
?>