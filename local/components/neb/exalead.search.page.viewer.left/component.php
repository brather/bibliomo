<? 
	if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

	$arResult = $arParams['RESULT'];
	$arParams = $arParams['PARAMS'];

	CModule::IncludeModule("nota.userdata"); 
	use Nota\UserData\Books;
	
	\CModule::IncludeModule("highloadblock");
	use Bitrix\Highloadblock as HL;
	use Bitrix\Main\Entity;
	use Bitrix\NotaExt\Iblock\Element;
	use Bitrix\NotaExt\Iblock\IblockTools;

	if ($USER->IsAuthorized())
	{
		$arResult['USER_BOOKS'] = Books::getListCurrentUser();
	}

	$arParams['TITLE_MODE_BLOCK'] = empty($arParams['TITLE_MODE_BLOCK']) ? GetMessage('TITLE_MODE_BLOCK') : $arParams['TITLE_MODE_BLOCK']; 
	$arParams['TITLE_MODE_LIST'] = empty($arParams['TITLE_MODE_LIST']) ? GetMessage('TITLE_MODE_LIST') : $arParams['TITLE_MODE_LIST']; 

	if(!empty($arResult['ITEMS']))
	{
	
		foreach($arResult['ITEMS'] as &$arItem)
		{
			$arItem['~authorbook'] = substr( str_replace(array('"','\''), '', strip_tags($arItem['authorbook'])), 0, 35);
			$arItem['~title'] = substr( str_replace(array('"','\''), '', strip_tags($arItem['title'])), 0, 35);
            // $tmpLibrary = Bitrix\NotaExt\Iblock\Element::getList(Array('IBLOCK_ID' => IBLOCK_ID_LIBRARY, 'PROPERTY_LIBRARY_LINK' => $arItem['idlibrary']), 1, Array('DETAIL_PAGE_URL'));
			
					$libraryHLBlock = HL\HighloadBlockTable::getById(HIBLOCK_LIBRARY)->fetch();			
					$libraryEntity = HL\HighloadBlockTable::compileEntity($libraryHLBlock);
					$libraryEntity = $libraryEntity->getDataClass();
					$libraryData = $libraryEntity::getList(array(
						"select" => array("ID", "UF_NAME", "UF_ADRESS", "UF_POS", "UF_ID"),
						"order" => array("ID"),
						// "filter" => $arFilter,
					));
					while($l = $libraryData->Fetch())
					{
						$libraries[] = array(
							"NAME" => $l["UF_NAME"],
							"ID" => $l["UF_ID"],
						);
					}
					if (empty($_SESSION["LIBRARY_BOOK"][$arItem['id']]))
					{
						$rand = rand(0,9);
						$_SESSION["LIBRARY_BOOK"][$arItem['id']] = $libraries[$rand];
					}
					$arItem['library'] = $_SESSION["LIBRARY_BOOK"][$arItem['id']]["NAME"];
					$arItem['idlibrary'] = $_SESSION["LIBRARY_BOOK"][$arItem['id']]["ID"];
					/*
					Получаем ссылку на страницу библиотеки к которой привязана книга
					*/
					if((int)$arItem['idlibrary'] > 0){
						$arLib = Element::getList(array('IBLOCK_ID' => IblockTools::getIBlockId(IBLOCK_CODE_LIBRARY), 'PROPERTY_LIBRARY_LINK' => $arItem['idlibrary']), 1, array('DETAIL_PAGE_URL' , 'skip_other' => true));
						if(!empty($arLib['ITEM']))
						{
							$arItem['library_url'] = $arLib['ITEM']['DETAIL_PAGE_URL'];
							$_SESSION["LIBRARY_BOOK"][$arItem['id']]["LINK"] = $arLib['ITEM']['DETAIL_PAGE_URL'];
						}
					}
		}
	}
	echo 'adsf';
	$this->IncludeComponentTemplate();
?>