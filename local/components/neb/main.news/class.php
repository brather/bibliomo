<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}
CModule::IncludeModule("evk.books");
class NebMainNewsComponent extends CBitrixComponent
{
	public function onPrepareComponentParams($arParams)
	{
		if(isset($_REQUEST['tag']) && '' !== ($tag = trim($_REQUEST['tag']))) {
			$arParams['TAGS'] = $tag;
		}
		$arParams['by'] = isset($_REQUEST['by']) ? $_REQUEST['by'] : 'created';
		$arParams['order'] = isset($_REQUEST['order']) ? $_REQUEST['order'] : 'desc';
		$arParams['sorts'] = ['active_from', 'created'];
		$arParams['ORDER'] = [];
		if (in_array($arParams['by'], $arParams['sorts'])) {
			$arParams['ORDER'][$_REQUEST['by']] = ('desc' === $arParams['order'])?'desc':'asc';
		} else {
			$arParams['ORDER']['created'] = 'desc';
		}

		return $arParams;
	}
	
	public function getResult()
	{
		global $APPLICATION;
		CPageOption::SetOptionString("main", "nav_page_in_session", "N");
		$arAllNews = array();
		$obCache = new \CPHPCache();
		$cache_time = 36000000;
		$cache_dir = sprintf("/%s/%s/", SITE_ID, "main_news_block");
		$cache_tag_id = md5($cache_dir);
		\Evk\Books\Cache::ClearByTag($cache_tag_id);
		$arNavigation = CDBResult::GetNavParams($this->arParams["COUNT"]);
		$arNavigation['nPageSize'] = $this->arParams["COUNT"];
		if(!empty($this->arParams["PAGE"]))
		{
			$cache_dir = sprintf("/%s/%s%s/", SITE_ID, "main_news_block", $this->arParams["PAGE"]);
			$cache_id = md5($cache_dir.serialize($arNavigation).serialize($this->arParams['ORDER']));
		}
		if($obCache->InitCache($cache_time, $cache_id, $cache_dir))
		{
			extract($obCache->GetVars());
		}
		else
		{
			if($obCache->StartDataCache($cache_time, $cache_id, $cache_dir))
			{
				$arSelect = array(
					"IBLOCK_ID",
					"ID",
				);
				$arFilter = array(
					"IBLOCK_ID" => 3,
					"ACTIVE" => "Y",
					"PROPERTY_PUBLISH_MAIN" => 27,
					"PROPERTY_PUBLISHING" => 28,
				);
				if($this->arParams["PAGE"]=="news-archive")
					$arFilter["!ACTIVE_DATE"] = "Y";
				else
					$arFilter["ACTIVE_DATE"] = "Y";
				$arIDs = array();
				$arLibs = array();
				$rsLibNews = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
				while($arItem = $rsLibNews->Fetch())
				{
					$arIDs[] = $arItem["ID"];
				}

				if($this->arParams["PAGE"]=="news-archive")
					$arFilter2 = array(
						"IBLOCK_ID" => 9,
						array(
							"LOGIC" => "OR",
							array("ACTIVE" => "N",),
							array("!ACTIVE_DATE" => "Y",),
						),
					);
				else
					$arFilter2 = array(
						"IBLOCK_ID" => 9,
						"ACTIVE" => "Y",
						"ACTIVE_DATE" => "Y",
					);
				$arFilter = array(
					array(
						"LOGIC" => "OR",
						$arFilter2,
					),
				);
				if(!empty($arIDs))
				{
					$arFilter[0][] = array(
						"IBLOCK_ID" => 3,
						"ID" => $arIDs,
					);
				}
				$arSelect = array(
					"ID",
					"IBLOCK_ID",
					"DATE_CREATE",
					"NAME",
					"DATE_ACTIVE_FROM",
					"PREVIEW_PICTURE",
					"PREVIEW_TEXT",
					"DETAIL_PAGE_URL",
					"DATE_CREATE_UNIX",
					"TAGS",
				);
				if(isset($this->arParams['TAGS'])) {
					$arFilter['?TAGS'] = $this->arParams['TAGS'];
				}
				$this->arParams['ORDER'] = ['HAS_PREVIEW_PICTURE' => 'desc'] + $this->arParams['ORDER'];
				$rsNews = CIBlockElement::GetList($this->arParams['ORDER'], $arFilter, false, $arNavigation, $arSelect);
				$this->arResult["NAV_RESULT"] = $rsNews;
				while($arItem = $rsNews->Fetch())
				{
					if(!empty($arItem["PREVIEW_PICTURE"]))
					{
						$arItem["PREVIEW_PICTURE_ID"] = $arItem["PREVIEW_PICTURE"];
						$arFile = CFile::MakeFileArray($arItem["PREVIEW_PICTURE"]);
						$arItem["PREVIEW_PICTURE"] = substr($arFile["tmp_name"], strlen($_SERVER["DOCUMENT_ROOT"]));
					}
					if($arItem["IBLOCK_ID"]==3)
					{
						$arItem["DETAIL_PAGE_URL"] = sprintf('#SITE_DIR#/news/detail.php?ID=#ELEMENT_ID#');
					}
					$arItem["DETAIL_PAGE_URL"] = str_replace(array(
						"#SITE_DIR#",
						"#ELEMENT_ID#",
					), array(
						"",
						$arItem["ID"],
					),
						$arItem["DETAIL_PAGE_URL"]
					);

					$arItem["DATE"] = date('d.m.Y', $arItem["DATE_CREATE_UNIX"]);

					$arItem['TAGS'] = explode(',', $arItem['TAGS']);
					$arItem['TAGS'] = array_map('trim', $arItem['TAGS']);
					$arItem['TAGS'] = array_filter($arItem['TAGS']);

					$arAllNews[] = $arItem;
				}
				if(count($arAllNews))
				{
					/**
					 * @var \CCacheManager $CACHE_MANAGER
					 */
					global $CACHE_MANAGER;
					$CACHE_MANAGER->StartTagCache($cache_dir);
					$CACHE_MANAGER->RegisterTag($cache_tag_id);
					$CACHE_MANAGER->EndTagCache();
					$obCache->EndDataCache(compact("arAllNews"));
				}
				else
					$obCache->AbortDataCache();
			}
		}
		return $arAllNews;
	}
	public function executeComponent()
	{
		$this->arResult["ITEMS"] = $this->getResult();
		$this->includeComponentTemplate();
	}
}