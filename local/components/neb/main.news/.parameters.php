<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

use \Bitrix\Main;
use \Bitrix\Main\Localization\Loc as Loc;

Loc::loadMessages(__FILE__);

try {
	if (!Main\Loader::includeModule('iblock'))
		throw new Main\LoaderException(Loc::getMessage('NEW_LIST_PARAMETERS_IBLOCK_MODULE_NOT_INSTALLED'));

	$arComponentParameters = array(
		'GROUPS' => array(),
		'PARAMETERS' => array(
			
			"COUNT" => array(
				"PARENT" => "BASE",
				"NAME" => Loc::getMessage('NEW_LIST_PARAMETRS_COUNT'),
				"TYPE" => "STRING",
				"DEFAULT" => '',
			),
			"PAGE" => array(
				"PARENT" => "BASE",
				"NAME" => Loc::getMessage('NEW_LIST_PARAMETRS_COUNT'),
				"TYPE" => "STRING",
				"DEFAULT" => '',
			),
			'CACHE_TIME' => array(
				'DEFAULT' => 3600
			)
		)
	);
} catch (Main\LoaderException $e) {
	ShowError($e->getMessage());
}
?>