<?php
\Bitrix\Main\Loader::includeModule('evk.books');
use Evk\Books\PageNavigation;
if($arResult["NAV_RESULT"] instanceof CDBResult)
{
	$arResult["NEWS_AJAX_PAGE"] = (isset($_REQUEST["NEWS_AJAX_PAGE"]) && $_REQUEST["NEWS_AJAX_PAGE"] == "Y");
	$arResult["NEXT_NEWS_PAGE"] = PageNavigation::GetNextPage($arResult["NAV_RESULT"], 'NEWS_AJAX_PAGE');
}

