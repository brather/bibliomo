<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/**
 * @global $arResult Array
 */
?>
<?if(count($arResult["ITEMS"]) <= 0) return;?>
<div class="b-newswrapper">
	<h3>новости</h3>
	<ul class="b-main-newslist">
		<?php
		$itemID = 0;
		foreach($arResult["ITEMS"] as $arItem):?>
			<li>
				<?if($itemID < 2 && !empty($arItem["PREVIEW_PICTURE"])):
					$image_resize = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE_ID"], array("width" => 240, "height" => 189), BX_RESIZE_IMAGE_PROPORTIONAL, true); ?>
					<a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
						<img src="<?=$image_resize["src"]?>" alt="<?=htmlspecialchars($arItem["NAME"])?>">
					</a>
				<?endif;?>
				<div class="date"><?=$arItem["DATE"]?></div>
				<h4><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></h4>
				<?php if($itemID < 2):?>
				<p><?
					if(strlen($arItem["PREVIEW_TEXT"]) > 300){
						echo substr($arItem["PREVIEW_TEXT"], 0, strpos($arItem["PREVIEW_TEXT"], ' ', 300)).'...';
					}
					else{
						echo $arItem["PREVIEW_TEXT"];
					}
					?></p>
				<?php endif;?>
			</li>
		<?php
			$itemID++;
		endforeach;?>
	</ul>
	<a href="/news/" class="b-newslist-more">Смотреть все</a>
</div><!-- /.b-newswrapper -->