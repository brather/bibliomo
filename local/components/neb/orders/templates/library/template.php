<?
	if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
	$APPLICATION->SetTitle('Заказы литературы в библиотеке');
?>
<h2>Заказ литературы</h2>
<form method="get" class="clearfix">
	<input type="text" class="b-search_fieldtb b-text right" id="asearch-orders" value="<?=$_REQUEST['q']?>" name="q" placeholder="Поиск" autocomplete="off">
	<? if($arResult['SINGLE_USER'] == true) :?>
		<input type="hidden" name="uid" value="<?=$_REQUEST['uid']?>">
	<? endif; ?>
	<input type="submit" class="b-search_bth bbox" value="Найти">
</form>
<div class="message-result"><?=$arResult['MESSAGE']?></div>
<? if($arResult['SINGLE_USER']) :?>
	<a class="button_blue left" href="<?=$APPLICATION->GetCurPage()?>">Вернуться к списку заказов</a> 
	<table class="b-usertable b-libuser b-tbscan tsize">
		<tr>
			<th class="col-author">Автор</th>
			<th>Название</th>
			<th class="col-center">Статус получения</th>
			<th class="col-center">Удалить из заказа</th>
		</tr>
	<?
			if(!empty($arResult['ITEMS']))
			{
				foreach($arResult['ITEMS'] as $arItem)
				{
					if(!isset($arItem['BOOK_AUTHOR']) && !isset($arItem['BOOK_TITLE']))
						continue;
				?>
				<tr>
					<td><?=$arItem['BOOK_AUTHOR']?></td>
					<td><a class="popup_opener ajax_opener coverlay" data-width="955" href="/catalog/<?=$arItem['UF_BOOK_ID']?>/"><?=$arItem['BOOK_TITLE']?></a></td>
					<td class="col-center">
						<input type="checkbox" data-href="<?=$APPLICATION->GetCurPage()?>?uid=<?=$arItem['UF_USER_ID']?>&id=<?=$arItem['ID']?>&action=res" class="checkbox book-res" <?=($arItem['UF_RECEIVED']) ? 'checked' : ''?> />
					</td>
					<td class="col-center">
						<div class="rel plusico_wrap minus">
							<a href="<?=$APPLICATION->GetCurPage()?>?uid=<?=$arItem['UF_USER_ID']?>&id=<?=$arItem['ID']?>&action=del" class="plus_ico"></a>
						</div>
					</td>
				</tr>
				<?
				}
			}
		?>
	</table>
<? else: ?>
	<table class="b-usertable b-tbscan tsize">
		<tr>
			<th>Фамилия, имя, отчество</th>
			<th>Номер ЕЭЧБ</th>
			<th>Кол-во книг</th>
			<th>Просмотреть заказ</th>
		</tr>
	<?
			if(!empty($arResult['ITEMS']))
			{
				foreach($arResult['ITEMS'] as $arItem)
				{
					if(!isset($arItem['USER_FIO']))
						continue;
				?>
				<tr>
					<td><?=$arItem['USER_FIO']?></td>
					<td><?=$arItem['USER_ECHB']?></td>
					<td><?=$arItem['COUNT']?></td>
					<td><a href="<?=$APPLICATION->GetCurPage()?>?uid=<?=$arItem['UF_USER_ID']?>">Просмотреть</a></td>
				</tr>
				<?
				}
			}
		?>
	</table>
<? endif; ?>
