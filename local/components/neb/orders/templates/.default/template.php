<?
	if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
	$APPLICATION->SetTitle('Заказ литературы в библиотеке');

?>
<h2>Заказ литературы</h2>
<form method="get">
	<input type="text" class="b-search_fieldtb b-text right" id="asearch-orders" value="<?=$_REQUEST['q']?>" name="q" placeholder="Поиск" autocomplete="off">
	<input type="submit" class="b-search_bth bbox" value="Найти">
</form>
<div class="message-result"><?=$arResult['MESSAGE']?></div>
<table class="b-usertable b-tbscan tsize">
	<tr>
		<th class="col-author">Автор</th>
		<th>Название</th>
		<th class="col-center">Статус получения</th>
		<th class="col-center">Удалить из заказа</th>
	</tr>
<?
		if(!empty($arResult['ITEMS']))
		{
			foreach($arResult['ITEMS'] as $arItem)
			{
				if(!isset($arItem['BOOK_AUTHOR']) && !isset($arItem['BOOK_TITLE']))
					continue;
			?>
			<tr>
				<td class="col-author"><?=$arItem['BOOK_AUTHOR']?></td>
				<td><a class="popup_opener ajax_opener coverlay" data-width="955" href="/catalog/<?=$arItem['UF_BOOK_ID']?>/"><?=$arItem['BOOK_TITLE']?></a></td>
				<td class="col-center">
					<?=($arItem['UF_RECEIVED']) ? 'Выдано' : 'Не выдано'?>
				</td>
				<td class="col-center">
					<div class="rel plusico_wrap minus">
						<a href="<?=$APPLICATION->GetCurPage()?>?id=<?=$arItem['ID']?>&action=del" class="plus_ico"></a>
					</div>
				</td>
			</tr>
			<?
			}
		}
	?>
</table>
