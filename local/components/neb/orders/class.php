<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

class COrders extends CBitrixComponent
{

	public function executeComponent()
	{
		CModule::IncludeModule('iblock');
		$this->parseAction();
		$this->includeComponentTemplate();

		return $this->arResult;
	}

	public function parseAction()
	{
		$action = htmlspecialcharsEx($_REQUEST['action']);
		$orderID = htmlspecialcharsEx($_REQUEST['id']);
		$uID = htmlspecialcharsEx($_REQUEST['uid']);

		if ($action == "res")
			OrderTable::changeStatus($orderID);
		else if ($action == "addBook") 
			$this->addBook();

		$this->setResult($uID);
	}

	public function addBook()
	{
		$bookNum = htmlspecialcharsEx($_REQUEST['BOOK_NUM_ID']);
		if (!empty($bookNum))
		{
			$bookID = htmlspecialcharsEx($_REQUEST['BOOK_ID_'.$bookNum]);
			OrderTable::addUserBook($bookID);
		}
	}

	public function setResult($uID)
	{
		$nebUser = new nebUser();

		if (!$nebUser->isLibrary() || !empty($uID))
		{
			//echo "(".$uID.")";
			$this->getUserOrders($uID);
			$this->arResult['USER_FIO'] = "";
			if(!empty($uID))
			{
				$arUser = CUser::GetByID($uID)->Fetch();
				$this->arResult['USER_FIO'] = sprintf('%s %s %s', $arUser["LAST_NAME"], $arUser["NAME"], $arUser["SECOND_NAME"]);
			}
			$this->arResult['SINGLE_USER'] = (!empty($uID));
		}
		else
		{
			$this->getLibOrders();
		}
	}

	public function getUserOrders($userID = 0)
	{
		$this->arResult['ITEMS'] = OrderTable::getUserOrders($userID);
		$this->getBooksDescription();
	}

	public function getLibOrders()
	{
		$this->arResult['ITEMS'] = OrderTable::getLibOrdersFilterBooks();
		$this->getUsersDescription();
	}

	public function getBooksDescription()
	{
		$q = htmlspecialchars($_REQUEST['q_book']);
		$bookIds = array();
		foreach ($this->arResult['ITEMS'] as $arItem)
		{
			$bookIds[] = $arItem['UF_BOOK_ID'];
		}

		$arFilter = array(
			"IBLOCK_TYPE" => $this->arParams["IBLOCK_TYPE"],
			"IBLOCK_ID" => $this->arParams["IBLOCK_ID"],
			"=ID" => $bookIds,
		);

        if(!empty($q))
		{
			$arFilter[] = array(
				'LOGIC' => 'OR',
				'%NAME' => $q,
				'%PROPERTY_AUTHOR' => $q
			);
        }
		$rsBook = CIBlockElement::GetList(array(), $arFilter, false, false, array('ID', 'IBLOCK_ID', 'NAME', 'PROPERTY_AUTHOR'));
		while($fields = $rsBook->GetNext())
		{
			foreach ($this->arResult['ITEMS'] as $arKey => $arItem)
			{
				if($arItem['UF_BOOK_ID'] == $fields['ID'])
				{
					$this->arResult['ITEMS'][$arKey]['BOOK_TITLE'] = $fields['NAME'];
					$this->arResult['ITEMS'][$arKey]['BOOK_AUTHOR'] = $fields['PROPERTY_AUTHOR_VALUE'];
				}
			}
		}
	}

	public function getUsersDescription()
	{
		$q = htmlspecialchars($_REQUEST['q_user']);
		foreach ($this->arResult['ITEMS'] as $arKey => $arItem)
		{
			if(isset($userIds))
				$userIds .= '|' . $arItem['UF_USER_ID'];
			else
				$userIds = $arItem['UF_USER_ID'];
		}
		$arFilter = array('ID' => $userIds);
		if(!empty($q))
		{
			$arFilter['NAME'] = '%'.$q.'%';
		}
		$rsUser = nebUser::GetList($by = "timestamp_x", $order = "desc", $arFilter, array('SELECT' => array('UF_NUM_ECHB'), 'FIELDS' => array('ID', 'NAME', 'SECOND_NAME', 'LAST_NAME')));
		while($fields = $rsUser->GetNext())
		{
			foreach ($this->arResult['ITEMS'] as $arKey => $arItem)
			{
				if($arItem['UF_USER_ID'] == $fields['ID'])
				{
					$this->arResult['ITEMS'][$arKey]['USER_FIO'] = $fields['LAST_NAME'] . " " . $fields['NAME'] . " " . $fields['SECOND_NAME'];
					$this->arResult['ITEMS'][$arKey]['USER_ECHB'] = $fields['UF_NUM_ECHB'];
				}
			}
		}
	}
}       
