<?
	if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

	$arComponentParameters = array(
		"GROUPS" => array(
		),
		"PARAMETERS" => array(

			"HOURS_LIMIT" => Array(
				"PARENT" => "BASE",
				"NAME" => "Период автивности токена (час.)",
				"TYPE" => "STRING",
				"DEFAULT" => "24",
			),
			"VARIABLE_ALIASES" => array(
			),
			"SEF_MODE" => array(
				"list" => array(
					"NAME" => "LIST PAGE",
					"DEFAULT" => "/",
				),
				"auth" => array(
					"NAME" => "AUTH PAGE",
					"DEFAULT" => "auth/",
				),
				"check_token" => array(
					"NAME" => "AUTH PAGE",
					"DEFAULT" => "check_token/",
				),
			),
		),
	);
?>