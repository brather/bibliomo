<?
	if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
	$strError = '';
	$arJResult = array();
	
	$arMessage = array(
		1 => 'token not found',	 
		2 => 'token has expired'	 
	);
	
	if($_SERVER['REQUEST_METHOD'] != 'POST')
		$strError = 'Error method';
	
	if(empty($strError))
	{
		$token = trim($_REQUEST['token']);
		//22 == свойсто статус пользователя
        $rsStatus = CUserFieldEnum::GetList(array('SORT' => 'ASC'), Array('USER_FIELD_ID' => '22'));
        while($status = $rsStatus->GetNext())
            $ufStatus[$status['ID']] = $status['XML_ID'];
		
		$arFilter = array(
			'UF_TOKEN' => $token,
			'ACTIVE' => 'Y'
		); 
		
		$rsUsers = CUser::GetList(($by="id"), ($order="desc"), $arFilter, array('FIELDS'=> array('ID', 'NAME', 'LAST_NAME', 'EMAIL', 'DATE_REGISTER', 'SECOND_NAME'), 'SELECT' => array('UF_TOKEN_ADD_DATE', 'UF_TOKEN', 'UF_STATUS')));
		if($arUser = $rsUsers->Fetch())
		{
			$arUser['UF_STATUS'] = $ufStatus[$arUser['UF_STATUS']];
			$date = $arUser['UF_TOKEN_ADD_DATE'];
			$stmp = MakeTimeStamp($date);
			$stmp = AddToTimeStamp(array('HH' => $arParams['HOURS_LIMIT']), $stmp);

			if(time() < $stmp){
				$arUser['TOKEN'] = $arUser['UF_TOKEN'];
				
				unset($arUser['UF_TOKEN']);
				unset($arUser['UF_TOKEN_ADD_DATE']);
				
				$arJResult = $arUser;
			}else{
				$strError = $arMessage[2];
			}
		}
		else
		{
			$strError = $arMessage[1];
		}
	}	

	echo json_encode(array('result'=>$arJResult, 'error'=> $strError));
?>