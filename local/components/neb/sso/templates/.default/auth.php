<?
	if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
	$strError = '';
	$arJResult = array();

	$arMessage = array(
		1 => 'Емайл или пароль введен не верно',
		2 => 'Не заполнен емайл или пароль'
	);
	if($_SERVER['REQUEST_METHOD'] != 'POST')
		$strError = 'error method';

	if(empty($strError)){
		$email = trim($_REQUEST['email']);
		$password = trim($_REQUEST['password']);
		if(empty($email) or empty($password))
			$strError = $arMessage[2];
	}

	if(empty($strError)){
	
		//22 == свойсто статус пользователя
        $rsStatus = CUserFieldEnum::GetList(array('SORT' => 'ASC'), Array('USER_FIELD_ID' => '22'));
        while($status = $rsStatus->GetNext())
            $ufStatus[$status['ID']] = $status['XML_ID'];
			
		$arFilter = array(
			'EMAIL' => $email,
			'ACTIVE' => 'Y'
		); 
		$rsUsers = CUser::GetList(($by="id"), ($order="desc"), $arFilter, array('FIELDS'=> array('ID', 'PASSWORD', 'NAME', 'LAST_NAME', 'EMAIL', 'DATE_REGISTER', 'SECOND_NAME'), 'SELECT'=>array('UF_STATUS')));
		if($arUser = $rsUsers->Fetch())
		{	$arUser['UF_STATUS'] = $ufStatus[$arUser['UF_STATUS']];
			
			/*
			Проверяем валидность пароля
			*/
			if(strlen($arUser["PASSWORD"]) > 32)
			{
				$salt = substr($arUser["PASSWORD"], 0, strlen($arUser["PASSWORD"]) - 32);
				$db_password = substr($arUser["PASSWORD"], -32);
			}
			else
			{
				$salt = "";
				$db_password = $arUser["PASSWORD"];
			}

			$user_password =  md5($salt.$password);

			if($db_password === $user_password)
			{
				unset($arUser['PASSWORD']);
				$arJResult = $arUser;
				$arJResult['TOKEN'] = md5(serialize($arUser).randString(10));
				
				$user = new CUser;
				$fields = Array( 
					"UF_TOKEN" => $arJResult['TOKEN'], 
					"UF_TOKEN_ADD_DATE" => ConvertTimeStamp(time(), 'FULL'), 
				); 
				$user->Update($arUser['ID'], $fields);
			}
			else{
				$strError = $arMessage[1];
			}
		}
		else{
			$strError = $arMessage[1];
		}

	}
	
	echo json_encode(array('result'=>$arJResult, 'error'=> $strError));
?>