<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="ru"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="ru"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="ru"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="ru"> <!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>add plan</title>
		<meta name="viewport" content="width=device-width"/>
		<script src="<?=MARKUP?>js/libs/modernizr.min.js"></script>
		<link rel="stylesheet" href="<?=MARKUP?>css/style.css">
		<script src="<?=MARKUP?>js/libs/jquery.min.js"></script>
		<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>	
		<script>window.jQuery.ui || document.write('<script src="<?=MARKUP?>js/libs/jquery.ui.min.js">\x3C/script>')</script>
		<script src="<?=MARKUP?>js/plugins.js"></script>
		<script src="<?=MARKUP?>js/slick.min.js"></script>
		<script src="<?=MARKUP?>js/jquery.cookie.js"></script>
		<script src="<?=MARKUP?>js/blind.js"></script>
		<script src="<?=MARKUP?>js/script.js"></script>

		<script type="text/javascript">
			$(function() {
				$( document ).on( "click", ".plan-digitalization .plus_ico", function() {
					var book_id = $(this).closest('.search-result').attr('id');
					console.log(book_id);
					if($(this).closest('.plusico_wrap').hasClass('minus'))
					{
						console.log('+');
						$.post("/local/tools/plan_digitization/add.php", { sessid: '<?=bitrix_sessid()?>', BOOK_ID: book_id } );
					}
					else
					{
						console.log('-');
						$.post("/local/tools/plan_digitization/remove.php", { sessid: '<?=bitrix_sessid()?>', BOOK_ID: book_id } );
					}
				});

				$( document ).on( "click", ".b-digitizing_mass_action a", function() {
					var aLink = this;
					if($(aLink).hasClass('minus'))
					{
						$(aLink).removeClass('minus');
						$(aLink).find('.wAdd').text('Добавить');
						$(aLink).find('.wAdd2').text('в');

						$('.search-result .plusico_wrap.plan-digitalization.minus').each(function() {
							$(this).find('.plus_ico').click();
						});

						console.log('1');
					}
					else
					{
						$(aLink).addClass('minus');
						$(aLink).find('.wAdd').text('Удалить');
						$(aLink).find('.wAdd2').text('из');
						$('.search-result .plusico_wrap.plan-digitalization').each(function() {
							if(!$(this).hasClass('minus'))
							{	
								$(this).find('.plus_ico').click();
							}
						});
						console.log('2');	
					}

					return false;
				});
				
				var iframe = $('#ifr_popup', parent.document.body);
				var exHeight = 288;
				$( document ).on( "click", "a.b-search_exlink", function() {
				console.log(iframe.height());
					if ($(this).hasClass('open'))
						iframe.height(iframe.height()+exHeight);
					else
						iframe.height(iframe.height()-exHeight);
				});
			});

		</script>

	</head>
	<body class="">
		<div class="b_digitizing_popup_content">
			<?$APPLICATION->IncludeComponent(
				"exalead:search.form",
				"popup_plan_digit_add_library",
				Array(
					"PAGE" => (''),
					"POPUP_VIEW" => 'Y',
					"ACTION_URL" => $APPLICATION->GetCurPage()
				)
			);
			$APPLICATION->IncludeComponent(
				"exalead:search.page",
				"popup_plan_digit_add_library",
				Array(
					'ID_LIBRARY' => $arParams['LIBRARY_ID'],
					'NAV_PARENT' => '_self',
				)
			);?>
		</div>
	</body>
</html>