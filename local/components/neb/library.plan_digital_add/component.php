<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

	$APPLICATION->RestartBuffer();
	
	$nebUser = new nebUser();
	$lib = $nebUser->getLibrary();
	
	CModule::IncludeModule("highloadblock"); 
	use Bitrix\Highloadblock as HL; 
	use Bitrix\Main\Entity; 

	if(!empty($lib['PROPERTY_LIBRARY_LINK_VALUE']))
	{
		$hlblock = HL\HighloadBlockTable::getById(HIBLOCK_LIBRARY)->fetch();
		$entity = HL\HighloadBlockTable::compileEntity($hlblock); 
		$entity_data_class = $entity->getDataClass(); 

		$rsData = $entity_data_class::getById($lib['PROPERTY_LIBRARY_LINK_VALUE']);
		$arData = $rsData->Fetch();
		$arParams['LIBRARY_ID'] = $arData['UF_ID'];
	}

	$this->IncludeComponentTemplate();

	exit();
?>