<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(!CModule::IncludeModule("evk.books")) return false;

if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 3600;

if(empty($arParams['CODE']))
	return false;

if(empty($arParams['IBLOCK_ID']))
	return false;

use Evk\Books\IBlock\IblockTools;
use Evk\Books\IBlock\Element;

$viewTemplate 	= '';
$viewTheme 		= '';

$arResult = Element::getList(
	array('IBLOCK_ID' => $arParams['IBLOCK_ID'], 'CODE' => $arParams['CODE']),
	1,
	array(
		'PREVIEW_TEXT',
		'PROPERTY_MAP',
		'PROPERTY_STATUS',
		'PROPERTY_ADDRESS',
		'PROPERTY_SCHEDULE',
		'PROPERTY_PHONE',
		'PROPERTY_EMAIL',
		'PROPERTY_SKYPE',
		'PROPERTY_URL',
		'PROPERTY_COLLECTIONS',
		'PROPERTY_USERS',
		'PROPERTY_PUBLICATIONS',
		'PROPERTY_VIEWS',
		'PROPERTY_CONTACTS',
		'PROPERTY_VIEW_TEMPLATE',
		'PROPERTY_VIEW_THEME',
		'NAME',
		'PREVIEW_PICTURE',
		'DETAIL_PICTURE',
	)
);

if(empty($arResult['ITEM']))
{
	@define("ERROR_404", "Y");
	CHTTP::SetStatus("404 Not Found");
	return;
}

$arResult['ITEM']["PROPERTY_VIEWS_VALUE"] = intval(trim($arResult['ITEM']["PROPERTY_VIEWS_VALUE"]));
if($arResult['ITEM']["PROPERTY_VIEWS_VALUE"] <= 0)
	$arResult['ITEM']["PROPERTY_VIEWS_VALUE"] = \Evk\Books\Stat\BookStat::CountBooksViewsForLibrary($arResult['ITEM']['ID']);

$arResult['ITEM']["PROPERTY_USERS_VALUE"] = intval(trim($arResult['ITEM']["PROPERTY_USERS_VALUE"]));
if($arResult['ITEM']["PROPERTY_USERS_VALUE"] <= 0)
	$arResult['ITEM']["PROPERTY_USERS_VALUE"] = \Evk\Books\Stat\BookStat::CountUsersForLibrary($arResult['ITEM']['ID']);

$arResult['ITEM']["PROPERTY_PUBLICATIONS_VALUE"] = intval(trim($arResult['ITEM']["PROPERTY_PUBLICATIONS_VALUE"]));
if($arResult['ITEM']["PROPERTY_PUBLICATIONS_VALUE"] <= 0)
	$arResult['ITEM']["PROPERTY_PUBLICATIONS_VALUE"] = \Evk\Books\Stat\BookStat::CountBooksForLibrary($arResult['ITEM']['ID']);

$arResult['ITEM']["PROPERTY_COLLECTIONS_VALUE"] = intval(trim($arResult['ITEM']["PROPERTY_COLLECTIONS_VALUE"]));
if($arResult['ITEM']["PROPERTY_COLLECTIONS_VALUE"] <= 0)
{
	$arCounts = \Evk\Books\Stat\BookStat::CountCollectionsForLibrary($arResult['ITEM']['ID']);
	$arResult['ITEM']["PROPERTY_COLLECTIONS_VALUE"] = $arCounts["count"];
}

if(!empty($arResult['ITEM']['PREVIEW_PICTURE']))
{
	$arResult['ITEM']['PREVIEW_PICTURE'] = CFile::ResizeImageGet($arResult['ITEM']['PREVIEW_PICTURE'], array('width'=>150, 'height'=>150), BX_RESIZE_IMAGE_PROPORTIONAL, true);
}
$arResult['ITEM']["COLLECTIONS_LINK"] = sprintf('/collections/?library=%d', $arResult['ITEM']['ID']);

if(!empty($arResult['ITEM']['PROPERTY_URL_VALUE']))
	$arResult['ITEM']['PROPERTY_URL_VALUE'] = str_replace('http://', '', $arResult['ITEM']['PROPERTY_URL_VALUE']);

if(isset($arResult['ITEM']['PROPERTY_MAP_VALUE']))
	$arResult['ITEM']['MAP'] = explode(',', $arResult['ITEM']['PROPERTY_MAP_VALUE']);

if(is_array($arResult['ITEM']['PROPERTY_PHONE_VALUE']))
	$arResult['ITEM']['PHONE_FORMATTED'] = implode('<br />', $arResult['ITEM']['PROPERTY_PHONE_VALUE']);
else
	$arResult['ITEM']['PHONE_FORMATTED'] =  $arResult['ITEM']['PROPERTY_PHONE_VALUE'];

if ( isset($arResult['ITEM']['PROPERTY_VIEW_TEMPLATE_ENUM_ID']) )
{
	$viewTemplate = IblockTools::GetPropertyEnumXmlId('biblioteki_4', 'VIEW_TEMPLATE', $arResult['ITEM']['PROPERTY_VIEW_TEMPLATE_ENUM_ID']);
}

if ( isset($arResult['ITEM']['PROPERTY_VIEW_THEME_ENUM_ID']) )
{
	$viewTheme = IblockTools::GetPropertyEnumXmlId($arParams['IBLOCK_ID'], 'VIEW_THEME', $arResult['ITEM']['PROPERTY_VIEW_THEME_ENUM_ID']);
}


//$viewTemplate = '';
$this->IncludeComponentTemplate($viewTemplate);

if(!empty($arResult['ITEM']['NAME']))
	$APPLICATION->SetTitle($arResult['ITEM']['NAME']);

if ( strlen($viewTheme) > 0 )
	$APPLICATION->SetPageProperty('view_theme', $viewTheme);



/* crouches in order to success */
if ( strlen($viewTemplate) > 0 )
	$APPLICATION->SetPageProperty('view_template_index', $viewTemplate);
if ( strlen($viewTheme) > 0 )
	$APPLICATION->SetPageProperty('view_theme_index', $viewTheme);
/* crouches in order to success */

if(CModule::IncludeModule("iblock"))
	CIBlockElement::CounterInc($arResult['ITEM']['ID']);

if (CModule::IncludeModule('evk.books')) {
    \Evk\Books\Tables\MoStatsEventTable::trackEvent('library_page');
}
