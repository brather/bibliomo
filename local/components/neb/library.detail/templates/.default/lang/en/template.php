<?php
$MESS['LIBRARY_DETAIL_ADDRESS'] = 'Post Address';
$MESS['LIBRARY_DETAIL_ADDRESS_MAP'] = 'Address';
$MESS['LIBRARY_DETAIL_WORK'] = 'Working schedule';
$MESS['LIBRARY_DETAIL_PHONE'] = 'Phone number';
$MESS['LIBRARY_DETAIL_EMAIL'] = 'Email';
$MESS['LIBRARY_DETAIL_CONTACT'] = 'Related person';
$MESS['LIBRARY_DETAIL_USER'] = 'Участник';