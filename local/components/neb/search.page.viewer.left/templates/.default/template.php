<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/**
 * @var CBitrixComponentSearchPageViewerLeft $component
 * @var Array $arParams
 * @var Array $arResult
 */
use \Bitrix\Main\Localization\Loc as Loc;
Loc::loadMessages(__FILE__);
?>
<a name="nav_start"></a>
<div class="b-filter">
	<div class="b-filter_wrapper">
		<a href="#" class="sort sort_opener"><?=Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_LEFT_SORT');?></a>
		<span class="sort_wrap" align="center">
			<a <?=SortingExalead("property_author")?>><?=Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_LEFT_SORT_BY_AUTHOR');?></a>
			<a <?=SortingExalead("name")?>><?=Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_LEFT_SORT_BY_NAME');?></a>
		</span>
		<span class="b-filter_act" align="center">
			<span class="b-filter_show"><?=Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_LEFT_SORT_BY_SHOW');?></span>
			<a href="<?=$APPLICATION->GetCurPageParam("pagen=15", array("pagen", "dop_filter", 'longpage'));?>" class="b-filter_num b-filter_num_paging<?=($arParams['ITEM_COUNT'] == 15 and !$arParams['LONG_PAGE']) ? ' current' : ''?>">15</a>
			<a href="<?=$APPLICATION->GetCurPageParam("pagen=30", array("pagen", "dop_filter", 'longpage'));?>" class="b-filter_num b-filter_num_paging<?=$arParams['ITEM_COUNT'] == 30 ? ' current' : ''?>">30</a>
			<a href="<?=$APPLICATION->GetCurPageParam("pagen=45", array("pagen", "dop_filter", 'longpage'));?>" class="b-filter_num b-filter_num_paging<?=$arParams['ITEM_COUNT'] == 45 ? ' current' : ''?>">45</a>
		</span>
	</div>
</div><!-- /.b-filter -->
<?
if ($arParams["DISPLAY_TOP_PAGER"])
{
	?><? echo $arResult["NAV_STRING"]; ?><?
}
?>
<div class="b-filter_list_wrapper">
	<div class="b-result-doc" id="list">
		<?
		if(!empty($arResult['ITEMS']))
		{
			foreach($arResult['ITEMS'] as $index=>$arItem)
			{
				?>
				<div class="b-result-docitem search-result" id="<?=$arItem['~ID']?>">
					<div class="iblock b-result-docinfo">
						<?
						if($arParams["IS_COLLECTION_ADD"])
						{
							if(!empty($arResult['USER_BOOKS'][$arItem['ID']]))
							{
								?>
								<div class="meta minus">
									<div class="b-hint rel"><?=Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_LEFT_REMOVE_FROM_LIB');?></div>
									<a href="#" data-normitem="Y" data-url="<?=ADD_COLLECTION_URL?>removeBook.php?id=<?=$arResult['USER_BOOKS'][$arItem['ID']]?>" data-collection="<?=ADD_COLLECTION_URL?>list.php?t=books&id=<?=$arItem['ID']?>" class="b-bookadd fav"></a>
								</div>
							<?
							}
							else
							{
								?>
								<div class="meta">
									<div class="b-hint rel" data-plus="<?=Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_LEFT_ADD_TO_LIB');?>" data-minus="<?=Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_LEFT_REMOVE_FROM_LIB');?>"><?=Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_LEFT_ADD_TO_LIB');?></div>
									<a href="#" data-normitem="Y" data-collection="<?=ADD_COLLECTION_URL?>list.php?t=books&id=<?=$arItem['ID']?>" data-url="<?=ADD_COLLECTION_URL?>removeBook.php?id=<?=$arResult['USER_BOOKS'][$arItem['ID']]?>"  class="b-bookadd"></a>
								</div>
							<?
							}
						}
						?>
						<?if(!empty($arItem['DETAIL_PAGE_URL'])):?>
							<h2><a class="popup_opener ajax_opener coverlay" data-width="955" href="<?=$arItem['DETAIL_PAGE_URL']?>"><?=$arItem['title']?></a></h2>
						<?else:?>
							<h2><?=$arItem['NAME']?></h2>
						<?endif;?>
						<ul class="b-resultbook-info">
							<?
							if(!empty($arItem['PROPERTY_AUTHOR_VALUE']))
							{
								?>
								<li id="<?=$arItem["ID"]."_AUTHOR"?>"><span><?=Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_LEFT_AUTHOR');?>:</span> <a href="<?=$component->GetLink("AUTHOR", $arItem['PROPERTY_AUTHOR_VALUE']);?>"><?=$arItem['PROPERTY_AUTHOR_VALUE']?></a></li>
								<?
							}
							if(!empty($arItem['PROPERTY_PUBLISH_YEAR_VALUE']))
							{
								?>
								<li id="<?=$arItem["ID"]."_PUBLISH_YEAR"?>"><span><?=Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_LEFT_YEAR');?>:</span> <a href="<?=$component->GetLink("PUBLISH_YEAR", $arItem['PROPERTY_PUBLISH_YEAR_VALUE']);?>"><?=$arItem['PROPERTY_PUBLISH_YEAR_VALUE']?></a></li>
								<?
							}
							if(intval($arItem['PROPERTY_VOLUME_VALUE']) > 0)
							{
								?>
								<li><span><?=Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_LEFT_PAGES');?>:</span> <?=$arItem['PROPERTY_VOLUME_VALUE']?></li>
								<?
							}
							if(!empty($arItem['PROPERTY_PUBLISH_VALUE']))
							{
								?>
								<li id="<?=$arItem["ID"]."_PUBLISH"?>"><span><?=Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_LEFT_PUBLISH');?>:</span> <a href="<?=$component->GetLink("PUBLISH", $arItem['PROPERTY_PUBLISH_VALUE']);?>"><?=$arItem['PROPERTY_PUBLISH_VALUE']?></a></li>
								<?
							}
							if(!empty($arItem['PROPERTY_PUBLISH_PLACE_VALUE']))
							{
								?>
								<li id="<?=$arItem["ID"]."_PUBLISH_PLACE"?>"><span><?=Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_LEFT_PUBLISHPLACE');?>:</span> <a href="<?=$component->GetLink("PUBLISH_PLACE", $arItem['PROPERTY_PUBLISH_PLACE_VALUE']);?>"><?=$arItem['PROPERTY_PUBLISH_PLACE_VALUE']?></a></li>
								<?
							}
							?>
						</ul>
						<p><?=$arItem["TEXT"]?></p>
						<div class="b-result_sorce clearfix">
							<div class="b-result_sorce_info left"><em><?=Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_LEFT_SOURCE');?>:</em>
								<?
								$i = 1;
								$c = count($arItem["LIBS"]);
								foreach($arItem["LIBS"] as $LIB):?>
									<?if($LIB['DETAIL_PAGE_URL']):?>
										<a href="<?=$LIB['DETAIL_PAGE_URL']?>" class="b-sorcelibrary"><?=$LIB['NAME']?></a>
									<?else:?>
										<span><?=$LIB['NAME']?><?if($i<$c)
											{
												echo ", ";
											}
											$i++;
										?></span>
									<?endif;?>
								<?endforeach;?>
							</div>
							<div class="b-result-type right">
								<?
								if(!empty($arItem['FILE']) && $arItem['PROPERTY_STATUS_VALUE'] == 2)
								{
									?>
									<span class="b-result-type_txt" data-ext="pdf"><a target="_blank" href="<?=$arItem['FILE']['FILE_PATH']?>"><?=Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_LEFT_READ');?></a></span>
									<?
								}
								?>
							</div>
						</div>
						<span class="num"><?=$arItem['NUM_ITEM']?>.</span>
					</div>
				</div>
			<?
			}
		}
		?>
	</div><!-- /.b-result-doc -->
	<?
	if ($arParams["DISPLAY_BOTTOM_PAGER"])
	{
		?><? echo $arResult["NAV_STRING"]; ?><?
	}
	?>
</div><!-- /.b-filter_list_wrapper -->