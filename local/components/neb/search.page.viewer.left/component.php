<?
/**
 * @var CBitrixComponentSearchPageViewerLeft $this
 */
	if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
	CModule::IncludeModule("evk.books");
	use Evk\Books\Collections;
	use Evk\Books\Books;

	$arResult = $arParams['RESULT'];
	$arParams = $arParams['PARAMS'];



	if ($USER->IsAuthorized())
	{
		$arResult['USER_BOOKS'] = Books::getListCurrentUser();
	}

	$arResult["NAV_STRING"] = htmlspecialchars_decode($arResult["NAV_STRING"]);

	$arParams['TITLE_MODE_BLOCK'] = empty($arParams['TITLE_MODE_BLOCK']) ? GetMessage('TITLE_MODE_BLOCK') : $arParams['TITLE_MODE_BLOCK']; 
	$arParams['TITLE_MODE_LIST'] = empty($arParams['TITLE_MODE_LIST']) ? GetMessage('TITLE_MODE_LIST') : $arParams['TITLE_MODE_LIST']; 

	$arParams["IS_COLLECTION_ADD"] = Collections::isAdd();
	if(!empty($arResult['ITEMS']))
	{
		foreach ($arResult['ITEMS'] as $key=>&$arItem)
		{
			if(!empty($arItem["SEARCH_ITEM"]["TITLE"]))
				$arItem["TITLE"] = $arItem["SEARCH_ITEM"]["TITLE"];
			if(!empty($arItem["SEARCH_ITEM"]["BODY"]))
				$arItem["TEXT"] = $arItem["SEARCH_ITEM"]["BODY"];
		}
	}
	$this->IncludeComponentTemplate();
?>