<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/**
 * Class CBitrixComponentSearchPageViewerLeft
 */
class CBitrixComponentSearchPageViewerLeft extends CBitrixComponent
{
    protected $arFields = array(
        "PUBLISH_PLACE" => 52,
        "PUBLISH" => 53,
        "AUTHOR" => 48,
        "PUBLISH_YEAR" => array(
            'publishyear_prev',
            'publishyear_next',
        ),
    );
    public function p($array, $opt = false)
    {
        $str = sprintf("<pre>%s</pre>", print_r($array, 1));
        if($opt)
            return $str;
        echo $str;
    }
    protected function getFilterName($link)
    {
        return $this->arFields[$link];
    }
    protected function getControlName($PID, $text)
    {
        if(array_key_exists($text, $this->arResult["PROPS_ITEMS"][$PID]["VALUES"]))
        {
            return $this->arResult["PROPS_ITEMS"][$PID]["VALUES"][$text]["CONTROL_NAME"];
        }
        return false;
    }
    public function GetLink($link, $text)
    {
        $text = trim($text);
        $url = "/search/";
        $arFilter = array();
        if(is_string($text) && strlen($text) > 0)
        {
            $PID = $this->getFilterName($link);
            if(is_numeric($PID))
            {
                if(($name = $this->getControlName($PID, $text))!==false)
                {
                    $arFilter[$name] = "Y";
                }
            }
            elseif(is_string($PID))
            {
                $arFilter[$PID] = $text;
            }
            elseif(is_array($PID))
            {
                foreach($PID as $PID_value)
                {
                    $arFilter[$PID_value] = $text;
                }
            }
        }
        if(is_array($arFilter) && count($arFilter) > 0)
        {
            $url .= "?".http_build_query($arFilter);
        }
        return $url;
    }
}