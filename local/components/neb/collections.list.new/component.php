<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/**
* @var NebCollectionsListNewCBitrixComponent $this
*/
if(!CModule::IncludeModule('evk.books'))
	return false;
use Evk\Books\Collection;
use Evk\Books\IBlock\Section;
use Evk\Books\IBlock\Element;

CPageOption::SetOptionString("main", "nav_page_in_session", "N");
$arParams["IBLOCK_ID"] = 6;
if($arParams["IBLOCK_ID"] < 1) {
	ShowError("IBLOCK_ID IS NOT DEFINED");
	return false;
}

$arParams['ITEM_COUNT'] = $arParams['SHOW_SKBR']?3:4;

$arFilter = array('IBLOCK_ID' => $arParams['IBLOCK_ID']);

$arSearchResult = array();

$findSphinxError = 0;


if(!empty($arParams["LIBRARY_ID"]))
	$arFilter['UF_LIBRARY'] = $arParams["LIBRARY_ID"];
$arFilter["=ID"] = $this->GetCachedCollectionIDs($arFilter, $arParams['SHOW_EMPTY_COLLECTION']);

$by = trim(htmlspecialcharsEx($_REQUEST['by']));
$order = trim(htmlspecialcharsEx($_REQUEST['order']));

$arNavParams = array(
	"nPageSize" => $arParams['ITEM_COUNT'],
);

$arNavigation = CDBResult::GetNavParams($arNavParams);
$arParams['PAGEN'] = $arNavigation['PAGEN'];
$arParams['SIZEN'] = $arNavigation['SIZEN'];

$arNavParams = array_merge($arNavigation, $arNavParams);

/*
Формируем разделы коллекций с навигацией
*/

if(empty($by) and empty($order))
	$arSort = array('SORT' => 'ASC', 'created' => 'ASC');
else
	$arSort = array($by => $order);
$arSections = array();



if($findSphinxError == 0)
{
	$arSections = Section::getList(
		$arFilter,
		array('UF_LIBRARY', 'SECTION_PAGE_URL', 'UF_ADDED_FAVORITES', 'UF_VIEWS', "CNT_ACTIVE", "DESCRIPTION"),
		$arSort,
		true,
		$arNavParams
	);
}
$obParser = new CTextParser();
$libIDs = array();
foreach($arSections["ITEMS"] as $arSection)
{
	$arSection['TRUNCATE_DESCRIPTION'] = $obParser->html_cut($arSection["DESCRIPTION"], 50);
	$arResult['SECTIONS'][] = $arSection;
	if(!in_array($arSection["UF_LIBRARY"], $libIDs))
		$libIDs[] = $arSection["UF_LIBRARY"];
}
$arLibraries = array();
if(count($libIDs) > 0)
{
	$arLibraries = Section::getList(
		array(
			"IBLOCK_ID" => 4,
			"=ID" => $libIDs
		)
	);
}
$arResult['SECTIONS_COUNT'] = count($arResult['SECTIONS']);
$this->IncludeComponentTemplate();

?>