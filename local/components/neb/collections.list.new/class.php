<?php
if(!CModule::IncludeModule('evk.books'))
	return false;
use Evk\Books\IBlock\Section;
class NebCollectionsListNewCBitrixComponent extends CBitrixComponent
{
	public function GetCachedCollectionIDs($arFilter, $showEmpty=false)
	{
		$obCache = new CPHPCache();
		$cache_id = "neb_collections_list_search_page".$showEmpty.serialize($arFilter);
		$cache_id = md5($cache_id);
		$cache_dir = "/".SITE_ID."/".$this->__name;
		$cache_time = 3600000000;
		$arCollections = array();
		if($obCache->InitCache($cache_time, $cache_id, $cache_dir))
		{
			$vars = $obCache->GetVars();
			extract($vars);
		}
		else
		{
			if ($obCache->StartDataCache($cache_time, $cache_id, $cache_dir))
			{
				$arSects = Section::getList(
					$arFilter,
					array('ID'),
					array(),
					true
				);
				foreach($arSects as $arSect)
				{
					if(!$showEmpty && $arSect["ELEMENT_CNT"] <= 0)
						continue;
					$arCollections[] = $arSect["ID"];
				}
				if(count($arCollections))
				{
					$GLOBALS["CACHE_MANAGER"]->StartTagCache($cache_dir);
					$GLOBALS["CACHE_MANAGER"]->RegisterTag($cache_id);
					$GLOBALS["CACHE_MANAGER"]->EndTagCache();
					$obCache->EndDataCache(compact("arCollections"));
				}
				else
					$obCache->AbortDataCache();
			}
		}
		return $arCollections;
	}
}