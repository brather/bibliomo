<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use \Bitrix\Main\Localization\Loc as Loc;
Loc::loadMessages(__FILE__);
?>
	<div class="b-newcollections">
		<div class="b-publishing_tit">
			<h3>новые коллекции <sup><?=$arResult['SECTIONS_COUNT']?></sup></h3>
		</div>
		<ul class="b-newcollectionlist">
			<?foreach($arResult["SECTIONS"] as $arSection):?>
				<li>
					<a href="<?=$arSection["SECTION_PAGE_URL"]?>"><h4><?=$arSection["NAME"]?> <sup><?=$arSection["ELEMENT_CNT"]?></sup></h4></a>
					<span class="b-collectlib">Российская государственная библиотека</span>
				</li>
			<?endforeach;?>
		</ul>
        <? if ($arParams['SHOW_SKBR']) { ?>
        <!-- BEGIN skbr2 search box -->
        <div id="wdg_elar">
            <form name="wdg_elar_form" id="wdg_elar_form" method="POST"
                  accept-charset="UTF-8" action="//skbr2.ru/skbr/find.php"
                  target="_blank"
                  style="position:relative;margin:0px;padding:0px;"><input
                    type="hidden" name="from" value="www.elar.ru"/><input
                    type="hidden" name="iddb" value="1"/><input
                    placeholder="Искать в СКБР..." type="text" name="FT" id="FT"
                    style="border:solid 1px #ddd;background:#fff;font: italic normal 10pt/10pt Arial, sans-serif;height:28px;width:230px;position:relative;margin:0px;padding:0px;color:#999;text-align:center"
                    maxLength="2000"/>
            </form>
        </div>
        <!-- END skbr2 search box -->
        <br>
        <!-- BEGIN skbr2 search box -->
        <div id="wdg_elar">
            <form name="wdg_elar_form" id="wdg_elar_form" method="POST"
                  accept-charset="UTF-8" action="//skbr2.ru/skbr/find.php"
                  target="_blank"
                  style="position:relative;margin:0px;padding:0px;"><input
                    type="hidden" name="from" value="www.elar.ru"/><input
                    type="hidden" name="iddb" value="2"/><input
                    placeholder="Искать в СКЭР..." type="text" name="FT" id="FT"
                    style="border:solid 1px #ddd;background:#fff;font: italic normal 10pt/10pt Arial, sans-serif;height:28px;width:230px;position:relative;margin:0px;padding:0px;color:#999;text-align:center"
                    maxLength="2000"/>
            </form>
        </div>
        <br>
        <!-- END skbr2 search box -->
        <?}?>
	</div><!-- /.b-newcollections -->
