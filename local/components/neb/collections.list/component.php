<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/**
* @var NebCollectionsListCBitrixComponent $this
*/
if(!CModule::IncludeModule('evk.books'))
	return false;
use Evk\Books\Collection;

CPageOption::SetOptionString("main", "nav_page_in_session", "N");

if($arParams["IBLOCK_ID"] < 1) {
	ShowError("IBLOCK_ID IS NOT DEFINED");
	return false;
}

$this->arResult['AJAX'] = (isset($_REQUEST["ajax_collections_list"]) && $_REQUEST["ajax_collections_list"] == "Y");

$arParams['SHOW_EMPTY_COLLECTION'] = (isset($arParams['SHOW_EMPTY_COLLECTION']) && (bool)$arParams['SHOW_EMPTY_COLLECTION']) ? (bool)$arParams['SHOW_EMPTY_COLLECTION']: false;

$arResult["find_collections"] = (isset($_REQUEST["find_collections"])) ? htmlspecialcharsbx(trim($_REQUEST["find_collections"])) : "";

if($_REQUEST['action'] == 'addBook' && isset($_REQUEST['COLLECTION_ID']) && $_REQUEST['BOOK_NUM_ID'] > 0 && check_bitrix_sessid()){
	Collection::addBook($_REQUEST['COLLECTION_ID'], $_REQUEST['BOOK_ID_' . $_REQUEST['BOOK_NUM_ID']]);
	LocalRedirect($APPLICATION->GetCurPage());
}

if($_REQUEST['action'] == 'remove' && isset($_REQUEST['COLLECTION_ID']) > 0 && check_bitrix_sessid()){
	Collection::remove($_REQUEST['COLLECTION_ID']);
	LocalRedirect($APPLICATION->GetCurPage());
}

$arParams["LIBRARY_ID"] = intval($arParams["LIBRARY_ID"]);
if(empty($arParams["LIBRARY_ID"]) and !empty($_REQUEST['library']))
	$arParams["LIBRARY_ID"] = intval($_REQUEST['library']);

use Evk\Books\IBlock\Section;
use Evk\Books\IBlock\Element;

$arParams['ITEM_COUNT'] = intval($_REQUEST['pagen']) > 0 ? intval($_REQUEST['pagen']) : 16;//16;
if($arParams['ITEM_COUNT'] > 60) $arParams['ITEM_COUNT'] = 60;

$arFilter = array('IBLOCK_ID' => $arParams['IBLOCK_ID']);

$arSearchResult = array();

$findSphinxError = 0;

if(!empty($arResult["find_collections"]))
{
	$obSearch = new \Evk\Books\CSearch();
	$arSearchFilter = array (
		"QUERY" => $arResult["find_collections"],
		"TITLE" => 1,
		"SITE_ID" => SITE_ID,
		"MODULE_ID" => "iblock",
		"PARAM1" => "collections",
		"PARAM2" => 6,
		"CHECK_DATES" => "Y",
	);
	$obSearch->Search($arSearchFilter);
	if ($obSearch->errorno==0)
	{
		while($arItem = $obSearch->GetNext())
		{
			$arItem["~ITEM_ID"] = preg_replace("@\D@", "", $arItem["ITEM_ID"]);
			$arFilter["=ID"][] = $arItem["~ITEM_ID"];
			$arSearchResult[$arItem["~ITEM_ID"]] = $arItem;
		}
		$findSphinxError = count($arSearchResult) ? 0 : 1;
	}
	else
	{
		$findSphinxError = $obSearch->errorno;
	}
	$arFilter["CNT_ACTIVE"] = "Y";
}
else
{
	$arFilter["=ID"] = $this->GetCachedCollectionIDs($arFilter, $arParams['SHOW_EMPTY_COLLECTION']);
}
if(!empty($arParams["LIBRARY_ID"]))
	$arFilter['UF_LIBRARY'] = $arParams["LIBRARY_ID"];

$by = trim(htmlspecialcharsEx($_REQUEST['by']));
$order = trim(htmlspecialcharsEx($_REQUEST['order']));

$arNavParams = array(
	"nPageSize" => $arParams['ITEM_COUNT'],
);

$arNavigation = CDBResult::GetNavParams($arNavParams);
$arParams['PAGEN'] = $arNavigation['PAGEN'];
$arParams['SIZEN'] = $arNavigation['SIZEN'];

$arNavParams = array_merge($arNavigation, $arNavParams);

//\Evk\Books\Books::p($arParams);

/*
Формируем разделы коллекций с навигацией
*/

if(empty($by) and empty($order))
	$arSort = array('SORT' => 'ASC', 'created' => 'ASC');
else
	$arSort = array($by => $order);
$arSections = array();
if($findSphinxError == 0)
{
	$arSections = Section::getList(
		$arFilter,
		array(
			'UF_LIBRARY',
			'SECTION_PAGE_URL',
			'UF_ADDED_FAVORITES',
			'UF_VIEWS',
			'PICTURE',
			'DETAIL_PICTURE',
		),
		$arSort,
		true,
		$arNavParams
	);
}

$arResult['NEXT_COLLECTIONS'] = $this->GetLastLink($arSections['NAV_STRING']);

/*
$arResult['NAV_STRING'] = $arSections['NAV_STRING'];

$arResult['NAV_STRING'] = preg_replace("@</?div[^>]*>@", "", $arResult['NAV_STRING']);
*/
//$arResult['NAV_STRING']
//$arNavigation["PAGEN"];
/*
$arResult['NAV_NEXT'] = $arSections;
$arResult['NAV'] = $arNavigation;
*/
$arLibraryId = array();
$arSectionsId = array();
$arCount = array();
$obFile = new CFile();
foreach($arSections['ITEMS'] as &$arItem)
{
	if((int)$arItem['ELEMENT_CNT'] <= 0 && $this->getTemplateName() != "library_profile")
		continue;
	if(!empty($arItem['UF_LIBRARY']) and !in_array($arItem['UF_LIBRARY'], $arLibraryId))
	{
		$arLibraryId[] = $arItem['UF_LIBRARY'];
	}
	if(!isset($arCount[$arItem["UF_LIBRARY"]]))
		$arCount[$arItem["UF_LIBRARY"]] = 1;
	else
		$arCount[$arItem["UF_LIBRARY"]]++;
	$arSectionsId[] = $arItem['ID'];
	if(count($arSearchResult) && array_key_exists($arItem["ID"], $arSearchResult))
	{
		$arItem["SEARCH"] = $arSearchResult[$arItem["ID"]];
		$arItem["NAME"] = $arItem["SEARCH"]["TITLE_FORMATED"];
	}
	if(isset($arItem["DETAIL_PICTURE"]) && intval($arItem["DETAIL_PICTURE"]) > 0)
	{
		$arItem["DETAIL_PICTURE"] = intval($arItem["DETAIL_PICTURE"]);
		$arItem["DETAIL_PICTURE"] = $obFile->GetPath($arItem["DETAIL_PICTURE"]);
	}
	if(isset($arItem["PICTURE"]) && intval($arItem["PICTURE"]) > 0)
	{
		$arItem["PICTURE"] = intval($arItem["PICTURE"]);
		$arItem["PICTURE"] = $obFile->GetPath($arItem["PICTURE"]);
	}
	$arResult['SECTIONS'][] = $arItem;
}

/*
получаем список библиотек используемых в коллекции
*/

$arLibrary = array();

if(!empty($arLibraryId))
{
	$resLibrary = Element::getList(
		array('IBLOCK_ID' => \Evk\Books\IBlock\IblockTools::getIBlockId('biblioteki_4'), 'ID' => $arLibraryId),
		false,
		array('ID', 'DETAIL_PAGE_URL', 'skip_other')
	);

	if(!empty($resLibrary['ITEMS']))
		foreach($resLibrary['ITEMS'] as $arItem)
			$arLibrary[$arItem['ID']] = array('NAME' => $arItem['NAME'], 'DETAIL_PAGE_URL' => $arItem['DETAIL_PAGE_URL'], 'CNT_COLLECTIONS' => $arCount[$arItem['ID']]);
}
$arResult['LIBRARY'] = $arLibrary;


if($this->arResult['AJAX'])
	$APPLICATION->RestartBuffer();
$this->IncludeComponentTemplate();
if($this->arResult['AJAX'])
	exit;
?>