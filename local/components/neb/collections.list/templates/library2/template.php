<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use \Bitrix\Main\Localization\Loc as Loc;
Loc::loadMessages(__FILE__);
//print_r($arResult);
?>
<?
	if(empty($arResult['SECTIONS']))
		return false;

?>
<div class="b-mainblock left">
	<a name="nav_start"></a>
	<?
		if($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')
			$APPLICATION->RestartBuffer();
	?>
	<div class="b-filter b-filternums">
		<div class="b-filter_wrapper">		
			<a <?=SortingExalead("element_cnt")?>><?=Loc::getMessage('COLLECTION_LIST_SORT_BY_COUNT'); ?></a>
			<a <?=SortingExalead("name")?>><?=Loc::getMessage('COLLECTION_LIST_SORT_BY_TITLE'); ?></a>
			<a <?=SortingExalead("created")?>><?=Loc::getMessage('COLLECTION_LIST_SORT_BY_DATE'); ?></a>
			<span class="b-filter_act">					
				<span class="b-filter_show"><?=Loc::getMessage('COLLECTION_LIST_SHOW'); ?></span>
				<a href="<?=$APPLICATION->GetCurPageParam("pagen=10", array("pagen", "dop_filter", "PAGEN_1"));?>" class="b-filter_num b-filter_num_paging<?=$arParams['ITEM_COUNT'] == 10 ? ' current' : ''?>">10</a>
				<a href="<?=$APPLICATION->GetCurPageParam("pagen=25", array("pagen", "dop_filter", "PAGEN_1"));?>" class="b-filter_num b-filter_num_paging<?=$arParams['ITEM_COUNT'] == 25 ? ' current' : ''?>">25</a>
				<a href="<?=$APPLICATION->GetCurPageParam("pagen=30", array("pagen", "dop_filter", "PAGEN_1"));?>" class="b-filter_num b-filter_num_paging<?=$arParams['ITEM_COUNT'] == 30 ? ' current' : ''?>">30</a>
			</span>
		</div>
	</div><!-- /.b-filter -->
	<div class="b-collection_list mode">
		<?
			if($arParams['PAGEN'] > 1)
				$n = ($arParams['PAGEN'] - 1) * $arParams['SIZEN'];
			else
				$n = 0;

			foreach($arResult['SECTIONS'] as $arSection)
			{
				if(empty($arResult['BOOKS_IN_SECTIONS'][$arSection['ID']]))
					continue;
				$n++;
			?>
			<div class="b-collection-item rel renumber">
				<?
					if(collectionUser::isAdd()){
					?>
					<div class="b-addcollecion rel right">
						<span class="metalabel"><?=Loc::getMessage('COLLECTION_LIST_ADD_TO_MY_COL'); ?></span>
						<div class="meta"><a class="b-bookadd" data-collection="<?=ADD_COLLECTION_URL?>list.php?t=books_collection&id=<?=$arSection['ID']?>"  data-plus="<?=Loc::getMessage('COLLECTION_LIST_ADD_TO_MY_COL'); ?>" data-minus="<?=Loc::getMessage('COLLECTION_LIST_REMOVE_FROM_MY_COL'); ?>" href="#" data-normitem="Y" data-url="<?=ADD_COLLECTION_URL?>removeBook.php?t=books_collection&id=<?=$arSection['ID']?>"></a></div>
					</div>
					<?
					}
				?>

				<h2><span class="num" style="position:relative; padding-right:5px; top:auto"><?=$n?>.</span><a href="<?=$arSection['SECTION_PAGE_URL']?>"><?=$arSection['NAME']?></a></h2>

				<?
					if(!empty($arResult['LIBRARY'][$arSection['UF_LIBRARY']]))
					{
					?>
					<div class="b-result_sorce_info"><em><?=Loc::getMessage('COLLECTION_LIST_AUTHOR'); ?>:</em> <a href="<?=$arResult['LIBRARY'][$arSection['UF_LIBRARY']]['DETAIL_PAGE_URL']?>" class="b-sorcelibrary"><?=$arResult['LIBRARY'][$arSection['UF_LIBRARY']]['NAME']?></a></div>
					<?
					}
				?>
				<div class="clearfix">
					<?
						$coverBookID = $arResult['SECTIONS_BOOKS']['COVER'][$arSection['ID']];

						if(!empty($arResult['BOOKS'][$coverBookID]))
						{

							$arItem = $arResult['BOOKS'][$coverBookID];
						?>
						<div class="b-collcover iblock" style="margin-bottom:10px; border:none">
							<a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="b_bookpopular_photolg popup_opener ajax_opener coverlay" data-width="955" ><img src="<?=$arItem['IMAGE_URL']?>&width=131&height=194" class="b-bookboard_img loadingimg" alt="<?=$arItem['title']?>"></a>
							<h3><?=trimming_line($arItem['title'], 45)?></h3>
							<?
								if(!empty($arItem['authorbook']))
								{
								?>
								<a href="/search/?f_field[authorbook]=<?=urlencode($arItem['r_authorbook'])?>" class="b-autor"><?=$arItem['authorbook']?></a>
								<?
								}
							?>
						</div>
						<?
						}

					?>
						

					<?
                        if(!empty($arResult['SECTIONS_BOOKS']['ITEMS'][$arSection['ID']])){
                            $i = 0;
                            foreach($arResult['SECTIONS_BOOKS']['ITEMS'][$arSection['ID']] as $bookID)
                            {
                                $arItem = $arResult['BOOKS'][$bookID];
                                if(empty($arItem))
                                    continue;
                                $i++;
                            ?>
							<div class="b-collcover iblock" style="margin-bottom:20px; border:none">
							<a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="b_bookpopular_photolg popup_opener ajax_opener coverlay" data-width="955" ><img src="<?=$arItem['IMAGE_URL']?>&width=131&height=194" class="b-bookboard_img loadingimg" alt="<?=$arItem['title']?>"></a>
							<h3><?=trimming_line($arItem['title'], 45)?></h3>
							<?
								if(!empty($arItem['authorbook']))
								{
								?>
								<a href="/search/?f_field[authorbook]=<?=urlencode($arItem['r_authorbook'])?>" class="b-autor"><?=$arItem['authorbook']?></a>
								<?
								}
							?>
						</div><?
							}
						}
					?>


					
				</div><!-- /.clearfix -->
				
			</div><!-- /.b-collection-item -->
			<?
			}
		?>
	</div><!-- /.b-collection_list -->
	<?=$arResult['NAV_STRING']?>
	<?
		if($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')
			exit();
	?>
</div><!-- /.b-mainblock -->


