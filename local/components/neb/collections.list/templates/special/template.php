<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use \Bitrix\Main\Localization\Loc as Loc;
Loc::loadMessages(__FILE__);
?>
<?
	if(empty($arResult['SECTIONS']))
		return false;
?>


<section class="innersection innerwrapper clearfix searchempty">
    <!--<nav class="b-commonnav noborder">
        <a href="#" title="новые" class="current">новые</a>
        <a href="#" title="популярные" >популярные</a>
        <a href="#" title="рекомендованные" >рекомендованные</a>
    </nav>-->
    <div class="b-collectionpage rel">
        <h1>коллекции</h1>
        <div class="b-collection_list mode">
            <?
			if($arParams['PAGEN'] > 1)
				$n = ($arParams['PAGEN'] - 1) * $arParams['SIZEN'];
			else
				$n = 0;
			foreach($arResult['SECTIONS'] as $k => $collection):
				$n++;?>
                <div class="b-collection-item rel renumber">
                    <span class="num"><?=$n?>.</span>
                    <h2><a href="/special<?=$collection['SECTION_PAGE_URL']?>" title="<?=$collection['NAME']?>"><?=$collection['NAME']?></a></h2>
                    <div><span class="button_mode b-bookincoll"><?=$collection['ELEMENT_CNT']?> <?=GetEnding($collection['ELEMENT_CNT'], 'книг', 'книга', 'книги')?> в подборке</span></div>
                    <div class="b-result_sorce_info"><em>Автор:</em> <?=$arResult['LIBRARY'][$collection['UF_LIBRARY']]['NAME']?></div>
                </div><!-- /.b-collection-item -->
            <?endforeach;?>
        </div><!-- /.b-collection_list -->
</section>

<?=$arResult['NAV_STRING']?>

