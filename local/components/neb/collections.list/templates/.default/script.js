$(function(){
    function toggle_find_collection_form_buttons()
    {
        if($(this).val() == "")
            $(this).parent().find('input').not(":text").hide();
        else
            $(this).parent().find('input').not(":text").show();
    }
    function toggle_find_collection_bind()
    {
        if($('.b-form_find_wrapper input:text').val().length > 0)
            $('.b-form_find_wrapper input:text').addClass("not_empty");
        $('.b-form_find_wrapper input:text').focusin(function(){
            placeholder_fond = $(this).attr('placeholder');
            $(this).attr('placeholder', '');
        }).focusout(function(){
            if($(this).val().length > 0)
                $(this).addClass("not_empty");
            else
                $(this).removeClass("not_empty");
            $(this).attr('placeholder', placeholder_fond);
        }).bind("keyup keydown keypress", toggle_find_collection_form_buttons);
        $('.b-form_find_wrapper input:reset').click(function(){
            $('.b-form_find_wrapper input:text').removeClass("not_empty").val("");
            $(this).closest("form").submit();
            toggle_find_collection_form_buttons.call($('.b-form_find_wrapper input:text'));
            return false;
        });
        toggle_find_collection_form_buttons.call($('.b-form_find_wrapper input:text'));
    }
    var placeholder_fond = '';
    toggle_find_collection_bind();
    $.ajaxSuccess(function(){
        toggle_find_collection_bind();
    });

});