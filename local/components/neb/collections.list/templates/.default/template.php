<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use \Bitrix\Main\Localization\Loc as Loc;
Loc::loadMessages(__FILE__);
?>
<div class="b-mainblock left">
	<a name="nav_start"></a>
	<?
		if($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')
			$APPLICATION->RestartBuffer();
	?>
	<div class="b-filter b-filternums">
		<?
		if(!empty($arResult['SECTIONS'])):
		?>
		<div class="b-filter_wrapper">
			<a href="#" class="sort sort_opener"><?=Loc::getMessage('COLLECTION_LIST_SORT'); ?></a>
			<span class="sort_wrap">
				<a <?=SortingExalead("element_cnt")?>><?=Loc::getMessage('COLLECTION_LIST_SORT_BY_COUNT'); ?></a>
				<a <?=SortingExalead("name")?>><?=Loc::getMessage('COLLECTION_LIST_SORT_BY_TITLE'); ?></a>
				<a <?=SortingExalead("created")?>><?=Loc::getMessage('COLLECTION_LIST_SORT_BY_DATE'); ?></a>
			</span>
			<span class="b-filter_act">
				<span class="b-filter_show"><?=Loc::getMessage('COLLECTION_LIST_SHOW'); ?></span>
				<a href="<?=$APPLICATION->GetCurPageParam("pagen=10", array("pagen", "dop_filter", "PAGEN_1"));?>" class="b-filter_num b-filter_num_paging<?=$arParams['ITEM_COUNT'] == 10 ? ' current' : ''?>">10</a>
				<a href="<?=$APPLICATION->GetCurPageParam("pagen=25", array("pagen", "dop_filter", "PAGEN_1"));?>" class="b-filter_num b-filter_num_paging<?=$arParams['ITEM_COUNT'] == 25 ? ' current' : ''?>">25</a>
				<a href="<?=$APPLICATION->GetCurPageParam("pagen=30", array("pagen", "dop_filter", "PAGEN_1"));?>" class="b-filter_num b-filter_num_paging<?=$arParams['ITEM_COUNT'] == 30 ? ' current' : ''?>">30</a>
			</span>
		</div>
		<?php endif;?>
		<div class="b-filter_wrapper">
			<div class="b-form_find_wrapper">
				<form action="<?=POST_FORM_ACTION_URI?>" method="get">
					<input placeholder="<?=Loc::getMessage("COLLECTION_LIST_FIND_PLACEHOLDER")?>" type="text" name="find_collections" value="<?=$arResult["find_collections"]?>"/>
					<input type="submit" value="<?=Loc::getMessage("COLLECTION_LIST_FIND_BUTTON")?>";> <input  name="find_submit" type="reset" value="<?=Loc::getMessage("COLLECTION_LIST_FIND_BUTTON_CLEAR")?>";>
				</form>
			</div>
		</div>
	</div><!-- /.b-filter -->
	<div class="b-collection_list mode">
		<?
		if(empty($arResult['SECTIONS'])):
		?>
			<?=Loc::getMessage("COLLECTION_LIST_NOT_FOUND")?>
		<?php else:?>
		<?
			if($arParams['PAGEN'] > 1)
				$n = ($arParams['PAGEN'] - 1) * $arParams['SIZEN'];
			else
				$n = 0;

			foreach($arResult['SECTIONS'] as $arSection)
			{
				/*if(empty($arResult['BOOKS_IN_SECTIONS'][$arSection['ID']]))
					continue;*/
				$n++;
			?>
			<div class="b-collection-item rel renumber">
				<span class="num"><?=$n?>.</span>
				<?
					if(collectionUser::isAdd()){
					?>
					<div class="b-addcollecion rel right">
						<span class="metalabel"><?=Loc::getMessage('COLLECTION_LIST_ADD_TO_MY_COL'); ?></span>
						<div class="meta"><a class="b-bookadd" data-collection="<?=ADD_COLLECTION_URL?>list.php?t=books_collection&id=<?=$arSection['ID']?>"  data-plus="<?=Loc::getMessage('COLLECTION_LIST_ADD_TO_MY_COL'); ?>" data-minus="<?=Loc::getMessage('COLLECTION_LIST_REMOVE_FROM_MY_COL'); ?>" href="#" data-normitem="Y" data-url="<?=ADD_COLLECTION_URL?>removeBook.php?t=books_collection&id=<?=$arSection['ID']?>"></a></div>
					</div>
					<?
					}
				?>
				<h2><a href="<?=$arSection['SECTION_PAGE_URL']?>"><?=$arSection['NAME']?></a></h2>
				<a href="<?=$arSection['SECTION_PAGE_URL']?>" class="button_mode b-bookincoll"><?=$arSection['ELEMENT_CNT']?> <?=GetEnding($arSection['ELEMENT_CNT'], Loc::getMessage('COLLECTION_LIST_BOOK_5'), Loc::getMessage('COLLECTION_LIST_BOOK_1'), Loc::getMessage('COLLECTION_LIST_BOOK_2'))?> <?=Loc::getMessage('COLLECTION_LIST_IN_COL'); ?></a>
				<?
					if(!empty($arResult['LIBRARY'][$arSection['UF_LIBRARY']]))
					{
					?>
					<div class="b-result_sorce_info"><em><?=Loc::getMessage('COLLECTION_LIST_AUTHOR'); ?>:</em> <a href="<?=$arResult['LIBRARY'][$arSection['UF_LIBRARY']]['DETAIL_PAGE_URL']?>" class="b-sorcelibrary"><?=$arResult['LIBRARY'][$arSection['UF_LIBRARY']]['NAME']?></a></div>
					<?
					}
				?>
				<div class="clearfix">
					<?
						$coverBookID = $arResult['SECTIONS_BOOKS']['COVER'][$arSection['ID']];

						if(!empty($arResult['BOOKS'][$coverBookID]))
						{
							$arItem = $arResult['BOOKS'][$coverBookID];
						?>
						<div class="b-collcover iblock">
							<a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="b_bookpopular_photolg popup_opener ajax_opener coverlay" data-width="955" >
								<img src="<?=$arItem['IMAGE_URL']?>" class="b-bookboard_img loadingimg" alt="<?=$arItem['title']?>" title="<?=$arItem['title']?>">
							</a>
							<h3><?=trimming_line($arItem['title'], 45)?></h3>
							<?
								if(!empty($arItem['authorbook']))
								{
								?>
								<a href="<?=$arItem['r_authorbook']?>" class="b-autor"><?=$arItem['authorbook']?></a>
								<?
								}
							?>
						</div>
						<?
						}
					?>
					<div class="b-boardslider_min iblock">
						<div class="b-boardslider js_slider_single_nodots  iblock">
							<div>
								<div class="wrapper bbox">
									<div class="b-bookboard_cl iblock">
										<ul class="b-bookboard_list">
											<?
												if(!empty($arResult['SECTIONS_BOOKS']['ITEMS'][$arSection['ID']]))
												{
													$i = 0;
													foreach($arResult['SECTIONS_BOOKS']['ITEMS'][$arSection['ID']] as $bookID)
													{
														$arItem = $arResult['BOOKS'][$bookID];
														if(empty($arItem))
															continue;
														$i++;
													?>
													<li>
														<a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="b_bookpopular_photo popup_opener ajax_opener coverlay" data-width="955" >
															<img src="<?=$arItem['IMAGE_URL']?>" alt="<?=$arItem['title']?>" title="<?=$arItem['title']?>" class="loadingimg">
														</a>
													</li>
														<?
														if(($i%4) == 0 and $i < count($arResult['SECTIONS_BOOKS']['ITEMS'][$arSection['ID']]))
														{
														?>
													</ul>
												</div>
											</div>
										</div>
										<div>
											<div class="wrapper bbox">
												<div class="b-bookboard_cl iblock">
													<ul class="b-bookboard_list">
														<?
														}
													}
												}
											?>
										</ul>
									</div>
								</div>
							</div>
						</div> <!-- /.boardslider -->
					</div>
				</div><!-- /.clearfix -->
			</div><!-- /.b-collection-item -->
			<?
			}
		?>
		<?endif;?>
	</div><!-- /.b-collection_list -->

	<?=$arResult['NAV_STRING']?>
	<?
		if($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')
			exit();
	?>
</div><!-- /.b-mainblock -->


<?
	if(!empty($arResult['LIBRARY'])){
	?>
	<script type="text/javascript">
		$(function() {
			$(".b-sidenav input.checkbox").change(function()
            {
				var libraryId = 0;
				if($(this).attr('checked') == 'checked')
					var libraryId = $(this).val();

				$(".b-sidenav input.checkbox").not(this).attr('checked', false).closest('span.field').removeClass('checked');

				BX.showWait($(this).closest('li').attr('id'));

				$.get( "<?=$APPLICATION->GetCurPage();?>", {library: libraryId}, function( data ) {
					BX.closeWait();
					$('.b-mainblock.left>*').remove();
					$('.b-mainblock.left').prepend(data);

					$('.js_slider_single_nodots').slick({
						dots: false,
						cssEase: 'ease',
						speed: 600
					});

					$('.iblock').cleanWS();
					// $('.popup_opener').uipopup();
					// $('.b-bookadd').favAdd();
				});
			});

			<?

				if(!empty($arParams["LIBRARY_ID"]))
				{
				?>
				//var checkedDf = $(".b-sidenav input.checkbox[value='<?=$arParams["LIBRARY_ID"]?>']").closest('ul').show().prev().addClass('open');
				var checkedDf = $(".b-sidenav input.checkbox[value='<?=$arParams["LIBRARY_ID"]?>']").closest('div.b-sidenav').find('.b-sidenav_title').addClass('open');
				var checkedDf = $(".b-sidenav input.checkbox[value='<?=$arParams["LIBRARY_ID"]?>']").closest('div.b-sidenav_cont').show();
				<?
				}
			?>
		});
	</script>
	<div class="b-side right mtm10">
		<div class="b-sidenav">
			<a href="#" class="b-sidenav_title"><?=Loc::getMessage('COLLECTION_LIST_BY_LIB'); ?></a>
			<div class="b-sidenav_cont">
				<ul class="b-sidenav_cont_list">
					<?
						$i = 0;
						foreach($arResult['LIBRARY'] as $id => $arLibrary)
						{
							$i++;
						?>
                        <li class="clearfix<?=$i>5?' hidden':''?>" id='lib<?=$id?>'>
							<div class="b-sidenav_value left"><?=$arLibrary['NAME']?></div>
							<div class="checkwrapper type2 right">
								<label for="lib<?=$id?>" class="black"><?=$arLibrary['CNT_COLLECTIONS']?></label><input <?=$arParams["LIBRARY_ID"] == $id? 'checked="checked"':''?> class="checkbox" type="checkbox" name="library" value="<?=$id?>" id="lib<?=$id?>">
							</div>
						</li>
						<?
                            if($i>5){
							?>
							<li><a href="#" class="b_sidenav_contmore js_moreopen">+ <?=Loc::getMessage('COLLECTION_LIST_MORE'); ?></a></li>
							<?
							}
						}
					?>
				</ul>
			</div>
		</div> <!-- /.b-sidenav -->

	</div><!-- /.b-side -->
	<?
	}
?>

