<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use Bitrix\Main\Loader;
use Evk\Books\SearchQuery;
use Evk\Books\Searches;
if(!Loader::includeModule("evk.books") || !Loader::includeModule("iblock"))
	return false;

$arParams['sorts'] = array("name", "property_author", "property_publish_year");
$arParams["AJAX"] = $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest';

/** @var CBitrixComponentNebCatalogSection $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
/** @global CUser $USER */
/** @global CMain $APPLICATION */

$arParams["CACHE_TIME"] = (intval($arParams["CACHE_TIME"]) < 0) ? 3600000000 : intval($arParams["CACHE_TIME"]);

$arParams["q"] = trim(htmlspecialcharsEx($_REQUEST['q']));

$arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM", "PROPERTY_FILE", "DETAIL_PAGE_URL", "PROPERTY_STATUS");
if(empty($arParams["FILTER_NAME"]) || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["FILTER_NAME"]))
{
	$arrFilter = array();
}
else
{
	global ${$arParams["FILTER_NAME"]};
	$arrFilter = ${$arParams["FILTER_NAME"]};
	if(!is_array($arrFilter))
		$arrFilter = array();
}

if(is_array($arParams["PROPERTY_CODE"]))
{
	foreach($arParams["PROPERTY_CODE"] as $v)
	{
		if(strlen(trim($v)) > 0)
		{
			$arSelect[] = "PROPERTY_".$v;
		}
	}
}

$by = strtolower(htmlspecialchars($_REQUEST["by"]));
$order = strtolower(htmlspecialchars($_REQUEST["order"]));

if(in_array($order, array("asc", "desc")) && in_array($by, $arParams['sorts']))
{
	$arSort = array($by => $order);
}
else
{
	$arSort = array();
}
$arParams['IS_SAVE_QUERY'] = Searches::isSave();

$arFilter = Array("IBLOCK_ID" => IBLOCK_ID_BOOKS, "ACTIVE"=>"Y");

$arParams["DISPLAY_TOP_PAGER"] = $arParams["DISPLAY_TOP_PAGER"]=="Y";
$arParams["DISPLAY_BOTTOM_PAGER"] = true;
$arParams["PAGER_TITLE"] = trim($arParams["PAGER_TITLE"]);
$arParams["PAGER_SHOW_ALWAYS"] = $arParams["PAGER_SHOW_ALWAYS"]=="Y";
$arParams["PAGER_TEMPLATE"] = trim($arParams["PAGER_TEMPLATE"]);
$arParams["PAGER_DESC_NUMBERING"] = $arParams["PAGER_DESC_NUMBERING"]=="Y";
$arParams["PAGER_DESC_NUMBERING_CACHE_TIME"] = intval($arParams["PAGER_DESC_NUMBERING_CACHE_TIME"]);
$arParams["PAGER_SHOW_ALL"] = $arParams["PAGER_SHOW_ALL"]=="Y";

$arParams["SEARCH_IN_NEB"] = (isset($_REQUEST["s_neb"]) && $_REQUEST["s_neb"] == "Y");

$arParams["PAGE_ELEMENT_COUNT"] = $this->GetPageCount(15, array(15,30,45));

$arParams["S_STRICT"] = (isset($_REQUEST["s_strict"]) && $_REQUEST["s_strict"] == "Y");
$arParams["IS_FULL_SEARCH"] = (isset($_REQUEST{"is_full_search"}) && $_REQUEST{"is_full_search"} == "Y");


if ($arParams['DISPLAY_TOP_PAGER'] || $arParams['DISPLAY_BOTTOM_PAGER'])
{
	$arNavParams = array(
		"nPageSize" => $arParams["PAGE_ELEMENT_COUNT"],
		"bDescPageNumbering" => $arParams["PAGER_DESC_NUMBERING"],
		"bShowAll" => $arParams["PAGER_SHOW_ALL"],
	);

	global $NavNum;
	if(!isset($_SESSION["neb_catalog_section_search_nuv_num"]))
	{
		$_SESSION["neb_catalog_section_search_nuv_num"] = $NavNum = 0;
	}
	else
	{
		$NavNum = $_SESSION["neb_catalog_section_search_nuv_num"];
	}
	$arNavigation = CDBResult::GetNavParams($arNavParams);
	if($arNavigation["PAGEN"]==0 && $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"]>0)
		$arParams["CACHE_TIME"] = $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"];
}
else
{
	$arNavParams = array(
		"nTopCount" => $arParams["PAGE_ELEMENT_COUNT"],
		"bDescPageNumbering" => $arParams["PAGER_DESC_NUMBERING"],
	);
	$arNavigation = false;
}

$arNavParams['iNumPage'] = (isset($_REQUEST["PAGEN_1"]) && intval($_REQUEST["PAGEN_1"]) > 0) ? intval($_REQUEST["PAGEN_1"]) : 1;

if($arParams["AJAX"])
	$APPLICATION->RestartBuffer();
$obSearch = new SearchQuery($arResult, $arParams);

if(!$arParams["SEARCH_IN_NEB"])
{
	$obSearch->GetDataForSmartFilter();
	$obSearch->FillFilterArray($_REQUEST, $arrFilter);

	if(count($arrFilter) > 0 || $obSearch->CheckExtendedFilterExists())
	{
		$arFilter = array_merge($arFilter, $arrFilter);
	}
}
$arParams["IS_EMPTY_QUERY"] = ($obSearch->CountFilter() <=0 && empty($arParams['q']));

$arResult["EVK_SEARCH_PARAMS"] = htmlspecialcharsbx(serialize($arFilter));
$arResult["EVK_SEARCH_PARAMS_HASH"] = md5($arResult["EVK_SEARCH_PARAMS"].$USER->GetID()."A386");

$ins = $arFilter["=ID"];
unset($arFilter["=ID"]);

if(!$arParams["IS_EMPTY_QUERY"])
{
	$arSubFilter = array(
		"LOGIC" => "OR",
		array(
			"NAME" => sprintf('%s', $obSearch->QUERY),
		),
		array(
			"PROPERTY_AUTHOR" => sprintf('%s', $obSearch->QUERY),
		),
		array(
			"PROPERTY_PUBLISH_PLACE" => sprintf('%s', $obSearch->QUERY),
		),
		array(
			"PROPERTY_PUBLISH" => sprintf('%s', $obSearch->QUERY),
		),
		array(
			"PROPERTY_LIBRARIES.NAME" => sprintf('%s', $obSearch->QUERY),
		),
	);
	if(!$arParams["S_STRICT"])
	{
		foreach($arSubFilter as &$subFilter)
		{
			if(is_array($subFilter))
			{
				foreach($subFilter as $subKey=>$subValue)
				{
					unset($subFilter[$subKey]);
					$subFilter[sprintf('%%%s', $subKey)] = $subValue;
				}
			}
		}
	}
	if(strlen($obSearch->QUERY) > 0)
	{
		if(!$obSearch->CountFilter())
		{
			if(!empty($ins))
			{
				$arSubFilter[] = array(
					"=ID" => $ins,
				);
				$arFilter[] = $arSubFilter;
			}
			else
			{
				$arFilter[] = $arSubFilter;
			}
		}
		else {
			if(!empty($ins))
			{
				$arSubFilter[] = array(
					"=ID" => $ins,
				);
				$arFilter[] = $arSubFilter;
			}
			else
			{
				$arFilter[] = $arSubFilter;
			}
		}
	}
	else
	{
		if(!empty($ins))
			$arFilter["=ID"] = $ins;
	}
}

$cache_array_id = array(
	$arSort,
	$arFilter,
	$arParams,
	$arNavParams,
);

if($arParams["IS_FULL_SEARCH"] && !array_key_exists('!PROPERTY_FILE', $arFilter))
	$arFilter['!PROPERTY_FILE'] = false;
if(!$arParams["SEARCH_IN_NEB"])
{
	$obSearch->GetAllSearchElementsForSmartFilter($arFilter, $arrFilter);
}


if(isset($_GET["debug"]))
	\Evk\Books\Books::p($arFilter);

if($this->StartResultCache($arParams["CACHE_TIME"], $cache_array_id))
{
	if(!$arParams["SEARCH_IN_NEB"])
	{
		//if($res = $obSearch->CheckExtendedFilterExists())
		//{
			global $CACHE_MANAGER;
			$CACHE_MANAGER->StartTagCache($this->getCachePath());
			$CACHE_MANAGER->RegisterTag("iblock_id_library_catalog_section");
			$CACHE_MANAGER->EndTagCache();
			$arReturn = $obSearch->FindBooks($arSort, $arFilter, false, $arNavParams, $arSelect);


			if($obSearch->obNavigate->NavPageNomer < $obSearch->obNavigate->NavPageCount)
				$arResult["NEXT_PAGE"] = $APPLICATION->GetCurPageParam(sprintf("PAGEN_1=%d", ($arNavParams['iNumPage']+1)), array('PAGEN_1'));
			else
				$arResult["NEXT_PAGE"] = false;

			$arResult = array_merge_recursive($arResult, $arReturn);

			if(count($arReturn["ITEMS"]) <= 0)
			{
				$this->abortResultCache();
			}
			else
			{
				if(!$obSearch->CountFilter())
				{
					$LIBS = array();
					foreach($arReturn["ITEMS"][0]["LIBS"] as $libValue)
					{
						if(!empty($libValue["NAME"]))
						{
							$LIBS[] = $libValue["NAME"];
						}
					}

					$boolCache = false;
					$boolCache = (
						$arReturn["ITEMS"][0]["PROPERTY_AUTHOR_VALUE"] == $obSearch->QUERY
						|| $arReturn["ITEMS"][0]["PROPERTY_PUBLISH_VALUE"] == $obSearch->QUERY
						|| $arReturn["ITEMS"][0]["PROPERTY_PUBLISH_PLACE_VALUE"] == $obSearch->QUERY
						|| in_array($obSearch->QUERY, $LIBS)
					);
					if(!$boolCache)
					{
						$this->abortResultCache();
					}
				}
			}
		//}
		//else
		//{
		//	$this->abortResultCache();
		//}
	}
	else
	{
		$this->abortResultCache();
	}
	$this->IncludeComponentTemplate();
}

if($arParams["AJAX"])
	exit();