<?
$MESS["IBLOCK_MODULE_NOT_INSTALLED"] = "Модуль Информационных блоков не установлен";
$MESS["CATALOG_SECTION_NOT_FOUND"] = "Раздел не найден.";
$MESS["CATALOG_ERROR2BASKET"] = "Ошибка добавления товара в корзину";
$MESS["CATALOG_PRODUCT_NOT_FOUND"] = "Товар не найден";
$MESS["CATALOG_SUCCESSFUL_ADD_TO_BASKET"] = "Товар успешно добавлен в корзину";
$MESS["CATALOG_PARTIAL_BASKET_PROPERTIES_ERROR"] = "Не все свойства товара, добавляемые в корзину, заполнены";
$MESS["CATALOG_EMPTY_BASKET_PROPERTIES_ERROR"] = "Не заполнены свойства товара, добавляемые в корзину";

$MESS['SEARCH_PARAM_AUTHORBOOK'] = 'автор: #VALUE#';
$MESS['SEARCH_PARAM_YEARRANGE'] = 'дата с #VALUE# год';
$MESS['SEARCH_PARAM_PUBLISHER'] = 'издательство: #VALUE#';
$MESS['SEARCH_PARAM_LIBRARY'] = 'библиотека: #VALUE#';
$MESS['SEARCH_PARAM_PUBLISHPLACE'] = 'место издания: #VALUE#';
$MESS['SEARCH_PARAM_LOGIC_AND'] = 'и';
$MESS['SEARCH_PARAM_LOGIC_OR'] = 'или';
$MESS['SEARCH_PARAM_title'] = 'название: #VALUE#';
$MESS['SEARCH_PARAM_ALL'] = 'везде: #VALUE#';
$MESS['SEARCH_PARAM_authorbook'] = 'автор: #VALUE#';
$MESS['SEARCH_PARAM_publisher'] = 'издательство: #VALUE#';
$MESS['SEARCH_PARAM_publishplace'] = 'Место издания: #VALUE#';
$MESS['SEARCH_PARAM_library'] = 'библиотека: #VALUE#';
$MESS['SEARCH_PARAM_foraccess'] = 'доступ: #VALUE#';
$MESS['SEARCH_PARAM_ACCESS_0'] = 'открытый';
$MESS['SEARCH_PARAM_ACCESS_1'] = 'закрытый';
$MESS['SEARCH_PARAM_PUBLISHYEAR'] = 'Дата публикации: с #VALUE_1# по #VALUE_2# год';
?>