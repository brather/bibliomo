<?
	define("STOP_STATISTICS", true);
	define("NOT_CHECK_PERMISSIONS", true);
	require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
	if (!$USER->IsAuthorized())
		exit();
	CModule::IncludeModule("evk.books");
	use Evk\Books\Searches;
	$saveId = intval($_REQUEST['saveId']);
	if($_SERVER['REQUEST_METHOD'] == 'POST' and $saveId > 0)
	{
		Searches::UpdateQueryName($saveId);
	}
	if($_SERVER['REQUEST_METHOD'] == 'POST' and check_bitrix_sessid() and !empty($_REQUEST['url']))
	{
		Searches::SaveQuery();
	}
?>