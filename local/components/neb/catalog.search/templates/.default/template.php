<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use \Bitrix\Main\Localization\Loc as Loc;
Loc::loadMessages(__FILE__);
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponentNebCatalogSection $component */
?>
<section class="innersection innerwrapper<?=empty($arResult['ITEMS'])? ' searchempty' : ''?> clearfix" id="search_page_block">
	<script type="text/javascript">
		$(function(){
			$('#search_page_block .b-mainblock a').not('a.b-sorcelibrary, #neb-search, a.popup_opener, .b-result-type_txt a, a.b-bookadd').click(function(e){
				if(!$(this).attr("onclick") || $(this).attr("onclick").indexOf("return false;")==-1)
				{
					BX.showWait($(this).closest('li').attr('id'));
					$.get($(this).attr("href"), function(data){
						$('#search_page_block').remove();
						$('section.mainsection.innerpage').after(data);
						BX.closeWait();
					});
					return false;
				}
			});
		});
	</script>
<?
$APPLICATION->IncludeComponent(
	"neb:exalead.search.page",
	"",
	Array(
		"COMPONENT_TEMPLATE" => ".default",
		"SMART_FILTER" => $arResult["SMART_FILTER"],
	)
);
if(empty($arResult['ITEMS']))
{
	?>
	<div class="b-mainblock left">
		<?
		if(!empty($arParams['q']))
		{
			?>
			<div class="b-searchresult noborder">
				<span class="b-searchresult_label iblock"><?=Loc::getMessage('LIB_SEARCH_PAGE_TEMPLATE_FOUND_ZERO');?></span>
			</div><!-- /.b-searchresult-->
		<?
		}
		?>
		<div class="b-search_empty">
			<h2><?
				if(empty($arParams['q']))
					echo Loc::getMessage('LIB_SEARCH_PAGE_TEMPLATE_SEARCH_EMPTY');
				else
					echo Loc::getMessage('LIB_SEARCH_PAGE_TEMPLATE_NOTHING_FOUND');
				?></h2>
		</div>
	</div><!-- /.b-mainblock -->
<?
}
else
{
	?>
	<div class="clearfix">
		<div class="b-mainblock left">
			<div class="b-searchresult">
				<span class="b-searchresult_label iblock">
					<?=GetEnding($arResult["NAV_RESULT"]->nSelectedCount, Loc::getMessage("LIB_SEARCH_PAGE_TEMPLATE_FOUND_5"), Loc::getMessage("LIB_SEARCH_PAGE_TEMPLATE_FOUND_1"), Loc::getMessage("LIB_SEARCH_PAGE_TEMPLATE_FOUND_2"))?>&nbsp;<?=$arResult["NAV_RESULT"]->nSelectedCount?>&nbsp;<?=GetEnding($arResult["NAV_RESULT"]->nSelectedCount, Loc::getMessage("LIB_SEARCH_PAGE_TEMPLATE_DOCUMENT_5"), Loc::getMessage("LIB_SEARCH_PAGE_TEMPLATE_DOCUMENT_2"), Loc::getMessage("LIB_SEARCH_PAGE_TEMPLATE_DOCUMENT_1"))?>
				</span>
			</div><!-- /.b-searchresult-->
			<div class="b-searchresult_info rel">
				<?
				/*
				if ($arParams['IS_SAVE_QUERY'] === true)
				{
					?>
					<div class="b-search_save">
						<form action="<?=$this->__folder.'/save_query.php'?>" class="searchsave_form" method="POST">
							<?=bitrix_sessid_post()?>
							<input type="hidden" name="url" value="<?=$APPLICATION->GetCurPageParam("", array("clear_cache", "debug")); ?>">
							<input type="hidden" name="q" value="<?=htmlspecialcharsbx($arParams['q'])?>">
							<input type="hidden" name="more_options" value="<?=!empty($arResult['RESULT_PARAMS']) ? htmlspecialcharsbx(serialize($arResult['RESULT_PARAMS'])) : ''?>">
							<input type="hidden" name="found" value="<?=$arResult["NAV_RESULT"]->nSelectedCount?>">
							<input type="hidden" name="evk_search_params" value="<?=$arResult["EVK_SEARCH_PARAMS"]?>">
							<input type="hidden" name="evk_search_params_hash" value="<?=$arResult["EVK_SEARCH_PARAMS_HASH"]?>">
									<span class="b-searchresult_save rel">
										<input type="submit" class="b-searchresult_bt" value="<?=Loc::getMessage('LIB_SEARCH_PAGE_TEMPLATE_SAVE_QUERY');?>">
									</span>
						</form>
						<div class="b-searchsave_popup">
							<p><?=Loc::getMessage('LIB_SEARCH_PAGE_TEMPLATE_QUERY_SAVED');?></p>
							<div class="b-search_tb">
								<input placeholder="<?=Loc::getMessage('LIB_SEARCH_PAGE_TEMPLATE_ADD_TITLE');?>" type="text" name="query_name" class="input" id="query_name">
							</div>
						</div>
					</div>
				<?
				}
				*/
				if(!empty($arParams['q']))
				{
					?>
					<div class="b-searchresult_line iblock"><?=$arParams['q']?></div>
					<?
				}
				if(!empty($arResult['RESULT_PARAMS']))
				{
					?>
					<div class="b-searchresult_portal">
						<?=Loc::getMessage('LIB_SEARCH_PAGE_TEMPLATE_SEARCH_ON_WEBSITE');?>:
						<?
						$i = 0;
						foreach($arResult['RESULT_PARAMS'] as $param)
						{
							$i++;
							?>
							<?=($i > 1) ? $param['logic'].' ' : ''?> <span class="b-searchresult_tag"><?=$param['text']?><?/*<a href="<?=$param['url']?>" class="del"></a>*/?></span>
							<?
						}
						?>
					</div>
				<?
				}
				?>
			</div><!-- /.b-searchresult_info -->
		</div>
	</div>
	<div class="b-mainblock left" id=111>
		<?
		$APPLICATION->IncludeComponent(
			"neb:search.page.viewer.left",
			"",
			Array(
				"PARAMS" => $arParams,
				"RESULT" => $arResult,
			),
			$component
		);
		?>
	</div>
	<?$APPLICATION->IncludeComponent(
		"neb:catalog.smart.filter",
		"",
		Array(
			"COMPONENT_TEMPLATE" => ".default",
			"IBLOCK_TYPE" => "library",
			"IBLOCK_ID" => "10",
			"SECTION_ID" => "",
			"SECTION_CODE" => "",
			"FILTER_NAME" => $arParams["FILTER_NAME"],
			"HIDE_NOT_AVAILABLE" => "N",
			"TEMPLATE_THEME" => "blue",
			"FILTER_VIEW_MODE" => "vertical",
			"ITEMS" => $arResult["PROPS_ITEMS"],
			"ALL_ITEM_IDS" => $arResult["ALL_ITEM_IDS"],
			"CACHE_TYPE" => "A",
			"CACHE_TIME" => $arParams["CACHE_TIME"],
			"CACHE_GROUPS" => "Y",
			"SAVE_IN_SESSION" => "N",
			"INSTANT_RELOAD" => "N",
			"PRICE_CODE" => array(),
			"XML_EXPORT" => "N",
			"SECTION_TITLE" => "-",
			"SECTION_DESCRIPTION" => "-",
			"POPUP_POSITION" => "left",
			"AJAX" => $arParams["AJAX"],
		),
		$component
	);
	?>
	<?
}
?>
</section>