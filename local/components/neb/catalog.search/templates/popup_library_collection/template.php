<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
	use \Bitrix\Main\Localization\Loc as Loc;
	Loc::loadMessages(__FILE__);
?>
<form action="<?=$arParams['FORM_ACTION']?>" target="_top" method="post" class="b-form b-form_common b-addbookform">
	<input type="hidden" name="action" value="addBook">
	<input type="hidden" name="COLLECTION_ID" value="<?=$arParams['ID_COLLECTION']?>">
	<?=bitrix_sessid_post()?>
	<div class="fieldrow">
		<table class="b-result-doc">
			<?foreach($arResult["ITEMS"] as $item):?>
			<tr class="b-result-docitem">
				<td>
					<a class="b_bookpopular_photo" href="<?=$item['DETAIL_PAGE_URL']?>" target="_blank">
						<img class="loadingimg" data-title="<?=$item['NAME']?>" data-autor="<?=$item['PROPERTY_AUTHOR_VALUE']?>" alt="" src="<?=$item['IMAGE_URL']?>">
					</a>
				</td>
				<td>
					<h2><span class="num"><?=$item['NUM_ITEM']?>.</span><a href="<?=$item['DETAIL_PAGE_URL']?>" target="_blank"><?=$item['NAME']?></a></h2>
					<ul class="b-resultbook-info">
						<?if(!empty($item['PROPERTY_AUTHOR_VALUE'])):?><li><span>Автор:</span> <?=$item['PROPERTY_AUTHOR_VALUE']?></li><?endif;?>
						<?if(!empty($item['PROPERTY_PUBLISH_YEAR_VALUE'])):?><li><span>Год публикации:</span> <?=$item['PROPERTY_PUBLISH_YEAR_VALUE']?></li><?endif;?>
						<?if(!empty($item['PROPERTY_VOLUME_VALUE'])):?><li><span>Количество страниц:</span> <?=$item['PROPERTY_VOLUME_VALUE']?></li><?endif;?>
						<?if(!empty($item['PROPERTY_PUBLISH_VALUE'])):?><li><span>Издательство:</span> <?=$item['PROPERTY_PUBLISH_VALUE']?></li><?endif;?>
						<?
						$i = 1;
						$c = count($arItem["LIBS"]);
						foreach($arItem["LIBS"] as $LIB):?>
							<li><span><em>Источник:</em> <?=$LIB['NAME']?></li>
						<?endforeach;?>
					</ul>
				</td>
				<td>
					<div class="b-radio">
						<input type="radio" name="BOOK_NUM_ID" value="<?=$item['NUM_ITEM']?>" class="radio" id="libraryCollectionBooks">
						<input type="hidden" name="BOOK_ID_<?=$item['NUM_ITEM']?>" value="<?=$item['ID']?>">
						<input type="hidden" name="BOOK_TITLE_<?=$item['NUM_ITEM']?>" value="<?=$item['NAME']?>">
						<input type="hidden" name="BOOK_AUTHOR_<?=$item['NUM_ITEM']?>" value="<?=$item['PROPERTY_AUTHOR_VALUE']?>">
						<input type="hidden" name="BOOK_YEAR_<?=$item['NUM_ITEM']?>" value="<?=$item['PROPERTY_PUBLISH_YEAR_VALUE']?>">
					</div>
				</td>
			</tr>
			<?endforeach;?>
		</table><!-- /.b-result-doc -->
	</div>
	<?=$arResult['STR_NAV']?>
	<div class="fieldrow nowrap fieldrowaction">
		<div class="fieldcell ">
			<div class="field clearfix">
				<a href="#" class="formbutton gray right btrefuse">Отказаться</a>
				<button class="formbutton left" value="1" type="submit">Добавить произведение</button>
			</div>
		</div>
	</div>
</form>