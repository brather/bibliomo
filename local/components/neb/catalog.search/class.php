<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/**
 * Class CBitrixComponentNebCatalogSection
 */
class CBitrixComponentNebCatalogSection extends CBitrixComponent
{
	public function GetLastLink($NAV_STRING)
	{
		$address = "";
		if(preg_match_all('@<a\s+([^>]*)>@ims', $NAV_STRING, $match))
		{
			$last = $match[1][count($match[1])-1];
			if(preg_match_all('@(\w+)=([\'"])(.*?)\\2@i', $last, $match))
			{
				foreach($match[1] as $id=>$attr)
				{
					if(strtolower($attr)=="href")
					{
						$address = $match[3][$id];
					}
				}
			}
		}
		return $address;
	}
	public function GetPageCount($defaultCount = 15, $pageCounts = array(15, 30, 45))
	{
		$pageCount = intval($_REQUEST['pagen']);
		if ($GLOBALS["USER"]->IsAuthorized()) {
			$rsPagen = CUser::GetByID($GLOBALS["USER"]->GetID())->Fetch();
			if (intval($rsPagen['UF_SEARCH_PAGE_COUNT']) > 0) {
				$personPageCount = intval($rsPagen['UF_SEARCH_PAGE_COUNT']);
				$pageCount = (in_array($pageCount, $pageCounts) && $personPageCount != $pageCount) ? $pageCount : $personPageCount;
			}
		}
		return (in_array($pageCount, $pageCounts)) ? $pageCount : $defaultCount;
	}
}
?>