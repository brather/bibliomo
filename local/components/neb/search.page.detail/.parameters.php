<?
	if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
	if(!CModule::IncludeModule("iblock"))
		return;
	$arProperty_LNS = array();
	$rsProp = CIBlockProperty::GetList(array("sort"=>"asc", "name"=>"asc"), array("ACTIVE"=>"Y", "IBLOCK_ID" => IBLOCK_ID_BOOKS));
	while ($arr=$rsProp->Fetch())
	{
		$arProperty[$arr["CODE"]] = "[".$arr["CODE"]."] ".$arr["NAME"];
		if (in_array($arr["PROPERTY_TYPE"], array("L", "N", "S")))
		{
			$arProperty_LNS[$arr["CODE"]] = "[".$arr["CODE"]."] ".$arr["NAME"];
		}
	}

	$arComponentParameters = array(
		"GROUPS" => array(
		),
		"PARAMETERS" => array(
			"VARIABLE_ALIASES" => array(
				"ELEMENT_ID" => array(
					"NAME" => "ELEMENT_ID",
					"DEFAULT" => "ELEMENT_ID",
				),
			),
			"SEF_MODE" => array(
				"list" => array(
					"NAME" => "LIST PAGE",
					"DEFAULT" => "",
					"VARIABLES" => array()
				),
				"detail" => array(
					"NAME" => "DETAIL PAGE",
					"DEFAULT" => "#ELEMENT_ID#/",
					"VARIABLES" => array("ELEMENT_ID")
				),
				"viewer" => array(
					"NAME" => "VIEWER PAGE",
					"DEFAULT" => "#ELEMENT_ID#/viewer/",
					"VARIABLES" => array("ELEMENT_ID")
				),
                "video" => array(
                    "NAME" => "VIDEO PAGE",
                    "DEFAULT" => "#ELEMENT_ID#/video/",
                    "VARIABLES" => array()
                )
			),
			"PROPERTY_CODE" => array(
				"PARENT" => "DATA_SOURCE",
				"NAME" => "Дополнительные поля",
				"TYPE" => "LIST",
				"MULTIPLE" => "Y",
				"VALUES" => $arProperty_LNS,
				"ADDITIONAL_VALUES" => "Y",
			),
			"IBLOCK_ID" => array(
				"PARENT" => "DATA_SOURCE",
				"NAME" => "Идентификатор инфоблока",
				"TYPE" => "STRING",
				"DEFAULT" => "10",
			),
			"IBLOCK_TYPE" => array(
				"PARENT" => "DATA_SOURCE",
				"NAME" => "Тип инфоблока",
				"TYPE" => "STRING",
				"DEFAULT" => "library",
			),
		),
	);
?>