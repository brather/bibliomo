<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/**
 * @var CBitrixComponentSearchPageDetail $this
 */
use Bitrix\Main\Loader;
use Evk\Books\SearchQuery;
use Evk\Books\Quotes;
use Evk\Books\Notes;
use Evk\Books\Bookmarks;
use Evk\Books\Books;
use Evk\Books\Stat\BookView;
use Evk\Books\Stat\BookRead;
global $USER;
if(!Loader::includeModule("iblock") || !Loader::includeModule("evk.books"))
	return false;

$arDefaultUrlTemplates404 = array(
	"list" => "/",
	"detail" => "#ELEMENT_ID#/"
);

$arDefaultVariableAliases404 = array();

$arDefaultVariableAliases = array();

$arComponentVariables = array("ELEMENT_ID");

$SEF_FOLDER = "";
$arUrlTemplates = array();

if ($arParams["SEF_MODE"] == "Y")
{
	$arVariables = array();

	$arUrlTemplates = CComponentEngine::MakeComponentUrlTemplates($arDefaultUrlTemplates404, $arParams["SEF_URL_TEMPLATES"]);
	$arVariableAliases = CComponentEngine::MakeComponentVariableAliases($arDefaultVariableAliases404, $arParams["VARIABLE_ALIASES"]);

	$componentPage = CComponentEngine::ParseComponentPath($arParams["SEF_FOLDER"], $arUrlTemplates, $arVariables);

	if (StrLen($componentPage) <= 0) $componentPage = "list";

	CComponentEngine::InitComponentVariables($componentPage, $arComponentVariables, $arVariableAliases, $arVariables);

	$SEF_FOLDER = $arParams["SEF_FOLDER"];

}
else
{

	$arVariables = array();

	$arVariableAliases = CComponentEngine::MakeComponentVariableAliases($arDefaultVariableAliases, $arParams["VARIABLE_ALIASES"]);
	CComponentEngine::InitComponentVariables(false, $arComponentVariables, $arVariableAliases, $arVariables);

	$componentPage = "";
	if (!empty($arVariables["ELEMENT_ID"]) > 0)
	{
		$componentPage = "detail";
	}
	else
	{
		$componentPage = "list";
	}
}

$arResult = array(
	"FOLDER" => $SEF_FOLDER,
	"URL_TEMPLATES" => $arUrlTemplates,
	"VARIABLES" => $arVariables,
	"ALIASES" => $arVariableAliases,
);
$arResult["IS_USER_AUTH"] = $USER->IsAuthorized();
$RestartBuffer = false;

if($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' or $componentPage == 'viewer')
	$arParams['RestartBuffer'] = true;

if($componentPage == "detail" || $componentPage == "viewer")
{
	TrimArr($arParams["PROPERTY_CODE"]);
	$BOOK_ID = intval($arResult['VARIABLES']['BOOK_ID']);
	if($BOOK_ID > 0)
	{
		$obSearch = new SearchQuery();
		$arBook = $obSearch->FindBooksSimple(
			array(),
			array("=ID" => $BOOK_ID),
			false,
			false,
			array(
				"PROPERTY_AUTHOR",
				"PROPERTY_LIBRARIES",
				"PROPERTY_STATUS",
				"PROPERTY_FILE",
				"PROPERTY_VOLUME",
				"PROPERTY_PUBLISH_YEAR",
				"PROPERTY_PUBLISH",
				"PROPERTY_PUBLISH_PLACE",
				"PROPERTY_COUNT_PAGES",
				"PROPERTY_PART_NO",
				"PROPERTY_SERIA",
				"PROPERTY_BBK",
				"PROPERTY_ISBN",
				"PROPERTY_PART_TITLE",
				"NAME",
			),
			true
		);
		if(is_array($arBook) && !empty($arBook))
		{
			if(!isset($_SESSION["BOOKS_EXTEND"]))
			{
				$_SESSION["BOOKS_EXTEND"] = array();
			}
			if(!isset($_SESSION["BOOKS_EXTEND"][$BOOK_ID]))
			{
				$_SESSION["BOOKS_EXTEND"][$BOOK_ID] = array();
			}
			if(!isset($_SESSION["BOOKS_EXTEND"][$BOOK_ID]["NOTE"]) || empty($_SESSION["BOOKS_EXTEND"][$BOOK_ID]["NOTE"]))
			{
				$_SESSION["BOOKS_EXTEND"][$BOOK_ID]["NOTE"] = Notes::getNotesList($BOOK_ID);
			}
			if(!isset($_SESSION["BOOKS_EXTEND"][$BOOK_ID]["MARK"]) || empty($_SESSION["BOOKS_EXTEND"][$BOOK_ID]["MARK"]))
			{
				$_SESSION["BOOKS_EXTEND"][$BOOK_ID]["MARK"] = Bookmarks::getBookmarkList($BOOK_ID);
			}
			if(!isset($_SESSION["BOOKS_EXTEND"][$BOOK_ID]["QUOTES"]) || empty($_SESSION["BOOKS_EXTEND"][$BOOK_ID]["QUOTES"]))
			{
				$_SESSION["BOOKS_EXTEND"][$BOOK_ID]["QUOTES"] = Quotes::getListBook($BOOK_ID);
			}
			//Получение заметок
			$arBook["PROPERTIES"]["NOTE"]["VALUE"] = $_SESSION["BOOKS_EXTEND"][$BOOK_ID]["NOTE"];
			//Получение закладок
			$arBook["PROPERTIES"]["MARK"]["VALUE"] = $_SESSION["BOOKS_EXTEND"][$BOOK_ID]["MARK"];
			//Получение цитат
			$arBook["PROPERTIES"]["QUOTES"]["VALUE"] = $_SESSION["BOOKS_EXTEND"][$BOOK_ID]["QUOTES"];

			$nebUser = new nebUser();
			$libraryUser = $nebUser->GetLibrary();
			$arResult["ROLE"] = $nebUser->getRole();

			$arResult["CAN_ORDER_BOOKS"] = (!empty($libraryUser) && array_key_exists($libraryUser['ID'], $arBook["LIBS"]));

			if ($USER->IsAuthorized())
				$arResult['USER_BOOKS'] = Books::getListCurrentUser($BOOK_ID);

			$arResult['USER_BOOKS'] = $arResult['USER_BOOKS']["ID"];
			$arResult["BOOK"] = $arBook;
			$APPLICATION->SetTitle("Книга - ".$arBook["NAME"]);
		}
		else
		{
			$BOOK_ID = 0;
		}
	}
	if($BOOK_ID <= 0)
	{
		@define("ERROR_404", "Y");
		CHTTP::SetStatus("404 Not Found");
		return;
	}
}
	if($arParams['RestartBuffer'] === true)
		$APPLICATION->RestartBuffer();

	$this->IncludeComponentTemplate($componentPage);

	if($arParams['RestartBuffer'] === true)
		exit();
?>