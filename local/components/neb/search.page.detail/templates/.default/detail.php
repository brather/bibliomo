<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/**
 * @var CBitrixComponentSearchPageDetail $component
 */
if (empty($arResult['BOOK']))
	return false;

use \Bitrix\Main\Localization\Loc as Loc;

Loc::loadMessages(__FILE__);

if ($arParams['RestartBuffer'] === true)
{
?>
<script>
function addBook() {
	bid = $(".addorder").attr('data-id');
	$.ajax({
		type: "GET",
		url: "/local/tools/add_book_order.php?BOOK_ID="+bid,
		success: function(response){
			if (response == "OK")
			{
				$(".addorder").addClass("disabled");
				$(".addorder").text("Заказана");
			}
		}
	})
}
</script>

<div class="b-bookpopup popup">
	<a href="#" class="closepopup"><?= Loc::getMessage('LIB_SEARCH_PAGE_CLOSE_WINDOW') ?></a>
	<?
	}
	else
	{
	?>
	<div class="b-book_bg">
		<section class="innersection innerwrapper clearfix">
			<div class="b-bookpopup b-bookpage">
				<?
				}
				?>
				<div class="b-bookpopup_in bbox">

					<? if ($arParams['RestartBuffer'] !== true): ?>
						<div class="rel clearfix"><a href="javascript:;" onclick="window.close();"
						                             class="bookclose right"><?= Loc::getMessage('LIB_SEARCH_PAGE_CLOSE_WINDOW') ?></a>
						</div>
					<? endif; ?>

					<div class="b-onebookinfo iblock">
						<div class="b-bookhover">
							<?
							if ($component->CheckExistsField(($string_key = "AUTHOR"))) {
								?>
								<span class="b-autor"><a href="<?= $component->GetFieldUrl($string_key) ?>"
								                         class="lite"><?= $component->GetField($string_key) ?></a></span>
							<?
							}
							?>
							<div class="b-bookhover_tit black"><?= $arResult['BOOK']['NAME'] ?></div>
						</div>
						<div class="clearfix">
							<?
							if(!empty($arResult['BOOK']['VIEWER_URL']))
							{
								?>
								<a href="<?=$arResult['BOOK']['VIEWER_URL']?>" target="_blank"><button type="button" value="1" class="formbutton left"><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_VIEW')?></button></a>
								<?
							}
							?>
						</div>
						<div class="rel">
							<ul class="b-resultbook-info">
								<?
								if ($component->CheckExistsField(($string_key = "VOLUME"))) {
									?>
									<li><span><?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_PAGES') ?>
											: </span> <?= $component->GetField($string_key) ?> </li>
								<?
								}
								?>
								<?
								if ($component->CheckExistsField(($string_key = "PUBLISH_YEAR"))) {
									?>
									<li><span><?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_YEAR') ?>:</span> <a
											target="_parent"
											href="<?= $component->GetFieldUrl($string_key) ?>"><?= $component->GetField($string_key) ?></a>
									</li>
								<?
								}
								?>
								<?
								if ($component->CheckExistsField(($string_key = "PUBLISH"))) {
									?>
									<li><span><?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_PUBLISH') ?>:</span> <a
											target="_parent"
											href="<?= $component->GetFieldUrl($string_key) ?>"><?= $component->GetField($string_key) ?></a>
									</li>
								<?
								}
								?>
							</ul>
							<?
							?>
							<div class="b-infobox b-descrinfo" data-link="descr">
								<?
								if ($component->CheckExistsField(($string_key = "AUTHOR"))) {
									?>
									<div class="b-infoboxitem">
										<span class="tit iblock"><?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_AUTHOR') ?>
											: </span>
										<span class="iblock val"><?= $component->GetField("AUTHOR") ?></span>
									</div>
								<?
								}
								?>
								<div class="b-infoboxitem">
									<span class="tit iblock"><?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_TITLE') ?>
										: </span>
									<span class="iblock val"><?= $component->GetField("PART_TITLE") ?></span>
								</div>
								<?
								if ($component->CheckExistsField($string_key = "SERIA")) {
									?>
									<div class="b-infoboxitem">
										<span class="tit iblock"><?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_SERIE') ?>
											: </span>
										<span class="iblock val"><?= $component->GetField($string_key) ?></span>
									</div>
								<?
								}
								?>
								<?
								if ($component->CheckExistsField($string_key = "BBK")) {
									?>
									<div class="b-infoboxitem">
										<span class="tit iblock"><?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_BBK') ?>
											: </span>
										<span class="iblock val"><?= $component->GetField($string_key) ?></span>
									</div>
								<?
								}
								?>
								<?
								if ($component->CheckExistsField($string_key = "PUBLISH_PLACE")) {
									?>
									<div class="b-infoboxitem">
										<span
											class="tit iblock"><?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_PUBLISH_PLACE') ?>
											: </span>
										<span class="iblock val"><?= $component->GetField($string_key) ?></span>
									</div>
								<?
								}
								?>
								<?
								if ($component->CheckExistsField($string_key = "ISBN")) {
									?>
									<div class="b-infoboxitem">
										<span class="tit iblock">ISBN: </span>
										<span class="iblock val"><?= $component->GetField($string_key) ?></span>
									</div>
								<?
								}
								?>
								<?
								if ($component->CheckExistsField($string_key = "LIBRARIES")) {
									$nebUser = new nebUser;
									$libraryUser = $nebUser->GetLibrary();
									$foundLib = false;
									?>
									<div class="b-infoboxitem">
										<span class="tit iblock"><?= Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_LIB') ?>
											: </span>
									<span class="iblock val">
										<?
										$LIBRARIES = $component->GetField($string_key);
										foreach ($LIBRARIES as $key => $LIBRARY) {
											if (!empty($libraryUser) && ($libraryUser['ID'] == $LIBRARY['ID']))
												$foundLib = true;
											if (!empty($LIBRARY['LIBRARY_DETAIL_PAGE_URL'])) {
												?>
												<a target="_blank"
												   href="<?= $LIBRARY['LIBRARY_DETAIL_PAGE_URL'] ?>"><?= $LIBRARY["NAME"] ?></a>
											<?
											} else {
												?>
												<?= $LIBRARY["NAME"] ?>
											<?
											}
											if ($key < count($LIBRARIES) - 1) {
												echo ", ";
											}
										}
										?>
									</span>
									</div>
									<? if($foundLib) :?>
										<? if(!OrderTable::bookExists($arResult['BOOK']['ID'])) :?>
											<div class="clearfix">
												<a class="formbutton left addorder" data-id="<?=$arResult['BOOK']['ID']?>" onclick="addBook()">Заказать</a>
											</div>
										<? else :?>
											<div class="clearfix">
												<a class="formbutton left addorder disabled" >Заказана</a>
											</div>
										<? endif; ?>
									<? endif; ?>
								<?
								}
								?>
							</div>
							<!-- /b-infobox -->
						</div>
					</div>
				</div>
				<?
				if ($arParams['RestartBuffer'] === true)
				{
				?>
			</div>
			<!-- /.bookpopup -->
			<?
			}
			else
			{
			?>
	</div>
	<!-- /.bookpopup -->
	</section>
</div>
<?
}
?>
