<?use \Bitrix\Main\Localization\Loc as Loc;
/**
 * @var CBitrixComponentSearchPageDetail $component
 * @var CBitrixComponentTemplate $this
 */
Loc::loadMessages(__FILE__);
$arResult['BOOK']['URL'] = urldecode($arResult['VARIABLES']['BOOK_ID']);
global $USER;
?>
<!DOCTYPE html>
<html>
	<head>
		<title><?=$component->GetField(($string_key = "NAME"))?></title>
		<meta charset="utf-8">
		<link href="/local/templates/.default/markup/css/style.css" type="text/css"  data-template-style="true"  rel="stylesheet" />
		<link rel="stylesheet" href="<?=$templateFolder?>/styles.css" type="text/css" media="screen">
		<link rel="stylesheet" href="<?=$templateFolder?>/js/jquery.arcticmodal-0.3.css">
		<link rel="stylesheet" href="<?=$templateFolder?>/js/themes/simple.css">
		<script type="text/javascript" src="/local/templates/.default/markup/js/libs/jquery.min.js"></script>
		<script src="<?=$templateFolder?>/js/jquery.arcticmodal-0.3.min.js"></script>
		<script type="text/javascript" src="/bitrix/js/jqRotate.js"></script>
		<script type="text/javascript" src="/bitrix/js/jquery.Jcrop.js"></script>
		<script type="text/javascript" src="<?=$templateFolder?>/js/jquery.mousewheel.min.js"></script>
		<script type="text/javascript" src="<?=$templateFolder?>/script.js?version=2"></script>

	</head>
	<body>
	<div class="page-book">
	<div class=""></div>
	<span class="turn-page">Свернуть экран</span>
		<!-- left -->
		<div class="left">
			<a href="/" class="logo"></a>

			<div class="left-tools">
				<ul>
					<li class="sub">
						<span class="scale" title="Масштаб страницы">Масштаб</span>
						<ul>
							<li><span class="increase" title="Увеличить"></span></li>
							<li><span class="reduce" title="Уменьшить"></span></li>
							<li><span class="scale-status" title="Сбросить масштаб">1:1</span></li>
						</ul>
					</li>
					<li><span class="bookmark-tools" title="Добавить страницу в закладки">В закладки</span></li>
					<li><span class="scale-page" title="Смотреть страницу во весь экран">Во весь экран</span></li>
					<li><span class="save" title="Сохранить страницу">Сохранить страницу</span></li>
					<li><span class="question" title="Руководство пользователя"><a href='/upload/docs/Работа с открытыми изданиями.pdf'>Руководство пользователя</a></span></li>
				</ul>
			</div>
		</div>
		<!-- end left -->

		<!-- right -->
		<div class="right">
			<div class="top-right-tools">
				<ul>
						<li><span class="properties-book">Свойства книги</span></li>
						<?/*/?>
						<li><span class="search-book">Поиск по книге</span></li>
						<?/*/?>
						<li><span class="favorite-book<?if(!empty($arResult['USER_BOOKS'])):?> active<?endif;?>" title="<?if(!empty($arResult['USER_BOOKS'])):?>В личном кабинете<?else:?>Добавить в личный кабинет<?endif;?>" data-collection="/local/tools/collections/list.php?t=books&id=<?=$component->CheckExistsField(($string_key = "URL"))?>" data-url="/local/tools/collections/removeBook.php?id=<?=$component->CheckExistsField(($string_key = "URL"))?>"><?if(!empty($arResult['USER_BOOKS'])):?>Удалить из избранного<?else:?>Добавить в избранное<?endif;?></span></li>
						<?/*/?>
						<li><a href='/local/tools/exalead/getFiles.php?book_id=<?=$component->CheckExistsField(($string_key = "URL"))?>&name=<?=$component->CheckExistsField(($string_key = "title"))?>'><span class="save-book">Сохранить книгу</span></a>
		<div class="loading-book"><div>Загрузка началась</div></div>
						</li>
						<?/*/?>
				</ul>
			</div>
			<div class="right-tools">
					<div class="properties-book-content">
						<h2>Свойства книги</h2>
						<?if($component->CheckExistsField(($string_key = "AUTHOR"))):?><div><h3><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_AUTHOR')?>:</h3> <span><?=$component->GetField($string_key)?></span></div><?endif;?>
						<?if($component->CheckExistsField(($string_key = "NAME"))):?><div><h3><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_TITLE')?>:</h3> <span><?=$component->GetField($string_key)?></span></div><?endif;?>
						<?if($component->CheckExistsField(($string_key = "PUBLISH_YEAR"))):?><div><h3><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_YEAR')?>:</h3> <span><?=$component->GetField($string_key)?></span></div><?endif;?>
						<?if($component->CheckExistsField(($string_key = "PUBLISH"))):?><div><h3><span><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_PUBLISH')?>:</h3> <span><?=$component->GetField($string_key)?></span></div><?endif;?>
						<?if($component->CheckExistsField(($string_key = "BBK"))):?><div><h3>BBK:</h3> <span><?=$component->GetField($string_key)?></span></div><?endif;?>
						<?if($component->CheckExistsField(($string_key = "VOLUME"))):?><div><h3><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_PAGES')?>:</h3> <span><?=$component->GetField($string_key)?></span></div><?endif;?>
						<?if($component->CheckExistsField(($string_key = "SERIA"))):?><div><h3><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_SERIE')?>:</h3> <span><?=$component->GetField($string_key)?></span></div><?endif;?>
						<?if($component->CheckExistsField(($string_key = "PUBLISH_PLACE"))):?><div><h3><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_PUBLISH_PLACE')?>:</h3> <span><?=$component->GetField($string_key)?></span></div><?endif;?>
						<?if($component->CheckExistsField(($string_key = "ISBN"))):?><div><h3>ISBN:</h3> <span><?=$component->GetField($string_key)?></span></div><?endif;?>
						<?if($component->CheckExistsField(($string_key = "LIBRARIES"))):?>
							<div>
								<h3><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_LIB')?>:</h3>
								<?
								$LIBRARIES = $component->GetField($string_key);
								foreach($LIBRARIES as $key=>$LIBRARY):?>
									<span><?=$LIBRARY["NAME"]?></span>
									<?if($key < count($LIBRARIES)-1) echo ", "?>
								<?endforeach;?>
							</div>
						<?endif;?>
						<?/*/?>
						<?if($component->CheckExistsField(($string_key = "contentnotes"))):?><div><h3>Примечание содержания:</h3> <span><?=$component->GetField($string_key)?></span></div><?endif;?>
						<?if($component->CheckExistsField(($string_key = "edition"))):?><div><h3><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_EDITION')?>:</h3> <span><?=$component->GetField($string_key)?></span></div><?endif;?>
						<?if($component->CheckExistsField(($string_key = "issn"))):?><div><h3>ISSN:</h3> <span><?=$component->GetField($string_key)?></span></div><?endif;?>
						<?if($component->CheckExistsField(($string_key = "notes"))):?><div><h3><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_BOOK_NOTE')?>:</h3> <span><?=$component->GetField($string_key)?></span></div><?endif;?>
						<?if($component->CheckExistsField(($string_key = "udk"))):?><div><h3>УДК:</h3> <span><?=$component->GetField($string_key)?></span></div><?endif;?>
						<?if($component->CheckExistsField(($string_key = "placepublish"))):?><div><h3><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_PUBLISH_PLACE')?>:</h3> <span><?=$component->GetField($string_key)?></span></div><?endif;?>
						<?if($component->CheckExistsField(($string_key = "annotation"))):?><div><h3><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_ANNOTATION')?>:</h3> <span><?=$component->GetField($string_key)?></div><?endif;?>
						<?/*/?>
					</div>


					<div>
					<span class="note">Заметки</span>
					<div class="note-content">
						<?foreach($component->GetField(($string_key = "NOTE")) as $note):?>
						<div>
							<a p="<?=$note["NUM_PAGE"]?>"><span><?=$note["NUM_PAGE"]?> страница</span>
							<span><?=$note["TEXT"]?></span>
							</a>
							<span class='delete' id='<?=$note["ID"]?>'></span>
						</div>
						<?endforeach;?>
					</div>
					</div>

					<div>
					<span class="bookmark">Закладки</span>
					<div class="bookmark-content">
						<?foreach ($component->GetField(($string_key = "MARK")) as $mark):?>
						<div>
							<a p="<?=$mark['NUM_PAGE']?>"><span><?=$mark['NUM_PAGE']?></span> страница</a>
							<span class='delete' id='<?=$mark['ID']?>'></span>
						</div>
						<?endforeach;?>
					</div>
					</div>

					<div>
					<span class="quote">Цитаты</span>
					<div class="quote-content">
						<?foreach($component->GetField(($string_key = "QUOTES")) as $quotes):?>
						<div>
							<a p="<?=$quotes["UF_PAGE"]?>"><span><?=$quotes["UF_PAGE"]?> страница</span>
							<img src='<?=$quotes["UF_IMG_DATA"]?>'>
							</a>
							<span class='delete' id='<?=$quotes["ID"]?>'></span>
						</div>
						<?endforeach;?>
					</div>
					</div>


					<?/*/?>
					<div>
					<span class="search">Поиск по тексту</span>
					<div class='search-book-content'>
						<div class='search_'>
							<input name='search_text' id='search_text' type="text">
							<input id='search_submit' type='submit' value='Искать'>
							<div class='search_result empty'>
							</div>
						</div>
					</div>
					</div>
					<?/*/?>
				</div>
			</div>
			<!-- end right -->

			<!-- main -->
			<div class="main">
				<div class="prev-page"></div>
				<div class="view-content">
					<div class="book-title"><?=$component->GetField("NAME")?></div>
			<div class="view-page">
				<span class="add-bookmark" title="Добавить страницу в закладки"></span>
			<div class="viewer">
				<div class='viewer_img'>
					<img id='image' class='zoomer-image'>
				</div>

			</div>
			<div class="preloader"></div>
				<div class="quote-book">
					<div class="quote-icon"></div>
					<div class="quote-book-content"></div>
				</div>
				<div class="note-book">
					<div class="note-icon"></div>
					<div class="note-book-content"></div>
				</div>
			<div id='selectedrect' style='display:none'>
				<div id='selected_rect_menu'><a href='' id='b-textLayer_quotes'>Добавить в цитаты</a>
				<a href='' id='b-textLayer_notes'>Добавить в заметки</a></div>
				<div class='layer_notes' style='display:none;'><textarea id='text_note'></textarea><input type='button' value='Добавить' id='add_note'></div>
				</div>


					</div>
					<input type='hidden' id="page-number" value='<?=(intval($_REQUEST['page'])>0?intval($_REQUEST['page']):1)?>'>
					<input type='hidden' id="book_id" value='<?=$component->GetField("ID")?>'>
					<input type='hidden' id="count_page" value='<?=$component->GetField("COUNT_PAGES")?>'>
				</div>
				<div class="navigation">
					<ul>
						<li><span class="first" p=1></span></li>
						<li><span class="prev"></span></li>
						<li><span class='page_1'></span></li>
						<li><span class='page_2'></span></li>
						<li><span class="page_3"></span></li>
						<li>...</li>
						<li><span p=<?=$component->GetField("COUNT_PAGES")?>><?=$component->GetField("COUNT_PAGES")?></span></li>
						<li><span class="next"></span></li>
						<li><span class="last" p=<?=$component->GetField("COUNT_PAGES")?>></span></li>
					</ul>
					<span class="toggle-page-navigation"></span>
					<div class="page-navigation">
						<label>Переход к странице</label>
						<input type="text" class='page'>
						<span class="go"></span>
					</div>
				</div>

				<div class="next-page"></div>
			</div>
			<div style="display: none;">
				<div class="box-modal" id="exampleModal">
					<div class="box-modal_close arcticmodal-close">закрыть</div>
					<p></p>
				</div>
			</div>
			<!-- end main -->

			<!-- footer -->
			<footer>
				<div class="wrapper">
					<div class="b-footer_row clearfix">
						<?$APPLICATION->IncludeFile("/local/include_areas/logo_bottom.php", Array(), Array(
											"MODE"      => "text",
							));?>
						<div class="footer-container">
						<?$APPLICATION->IncludeComponent("bitrix:menu", "bottom", array(
							"ROOT_MENU_TYPE" => "bottom1_" . LANGUAGE_ID,
							"MENU_CACHE_TYPE" => "N",
							"MENU_CACHE_TIME" => "3600",
							"MENU_CACHE_USE_GROUPS" => "Y",
							"MENU_CACHE_GET_VARS" => array(
							),
							"MAX_LEVEL" => "1",
							"CHILD_MENU_TYPE" => "left",
							"USE_EXT" => "N",
							"DELAY" => "N",
							"ALLOW_MULTI_SELECT" => "N"
							),
							false
						);?>
						<?$APPLICATION->IncludeComponent("bitrix:menu", "bottom", array(
							"ROOT_MENU_TYPE" => "bottom2_" . LANGUAGE_ID,
							"MENU_CACHE_TYPE" => "N",
							"MENU_CACHE_TIME" => "3600",
							"MENU_CACHE_USE_GROUPS" => "Y",
							"MENU_CACHE_GET_VARS" => array(
							),
							"MAX_LEVEL" => "1",
							"CHILD_MENU_TYPE" => "left",
							"USE_EXT" => "N",
							"DELAY" => "N",
							"ALLOW_MULTI_SELECT" => "N"
							),
							false
						);?>
						<?$APPLICATION->IncludeComponent("bitrix:menu", "bottom", array(
							"ROOT_MENU_TYPE" => "bottom3_" . LANGUAGE_ID,
							"MENU_CACHE_TYPE" => "N",
							"MENU_CACHE_TIME" => "3600",
							"MENU_CACHE_USE_GROUPS" => "Y",
							"MENU_CACHE_GET_VARS" => array(
							),
							"MAX_LEVEL" => "1",
							"CHILD_MENU_TYPE" => "left",
							"USE_EXT" => "N",
							"DELAY" => "N",
							"ALLOW_MULTI_SELECT" => "N"
							),
							false
						);?>
						<?$APPLICATION->IncludeFile("/local/include_areas/ministr.php", Array(), Array(
							"MODE"      => "text",
						));?>
						</div>
					</div>

				</div><!-- /.wrapper -->
			</footer>
			<!-- end footer -->
			<img class='big_image' style='display:none;' >
		</div>
		<?/*/?>
		<?
		$APPLICATION->IncludeComponent("bitrix:system.auth.form", "for_viewer",
			Array(
				"FORGOT_PASSWORD_URL" => "/auth/",
				"SHOW_ERRORS" => "N",
				"PROFILE_URL" => "/catalog2/"
			),
			false
		);
		?>
		<?/*/?>
	</body>
</html>
