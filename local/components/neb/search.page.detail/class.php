<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
class CBitrixComponentSearchPageDetail extends CBitrixComponent
{
	public function p()
	{
		$args = func_get_args();
		$str = "";
		foreach($args as $arg)
		{
			if(is_bool($arg))
			{
				$str .= "<pre>" . (($arg)?"true":"false") . "</pre>";
			}
			elseif(is_null($arg))
			{
				$str .= "<pre>null</pre>";
			}
			elseif(is_string($arg))
			{
				$str .= "<pre>". ((strlen($arg) <= 0) ? "<div style='color:red;'>empty string</div>":$arg) ."</pre>";
			}
			else
			{
				$str .= "<pre>" . print_r($arg, 1) . "</pre>";
			}
		}
		echo $str;
	}
	public function CheckExistsField($name)
	{
		if(
			(in_array($name, $this->arParams["PROPERTY_CODE"]) &&
			array_key_exists($name, $this->arResult["BOOK"]["PROPERTIES"])) ||
			array_key_exists($name, $this->arResult["BOOK"])
		)
		{
			if((is_string($this->arResult["BOOK"]["PROPERTIES"][$name]["VALUE"]) && strlen($this->arResult["BOOK"]["PROPERTIES"][$name]["VALUE"]) > 0) ||
				(is_array($this->arResult["BOOK"]["PROPERTIES"][$name]["VALUE"]) && count($this->arResult["BOOK"]["PROPERTIES"][$name]["VALUE"]) > 0) ||
				(is_string($this->arResult["BOOK"][$name]) && strlen($this->arResult["BOOK"][$name]) > 0) ||
				(is_array($this->arResult["BOOK"][$name]) && count($this->arResult["BOOK"][$name]) > 0)
			)
			{
				return true;
			}
		}
		return false;
	}
	public function GetField($name, &$type="", &$pid=0)
	{
		if($this->CheckExistsField($name))
		{
			if(array_key_exists($name, $this->arResult["BOOK"]))
			{
				if(is_string($this->arResult["BOOK"][$name]) && strlen($this->arResult["BOOK"][$name]) > 0)
				{
					$pid = 0;
					$type = "E";
					return $this->arResult["BOOK"][$name];
				}
			}
			elseif(array_key_exists($name, $this->arResult["BOOK"]["PROPERTIES"]))
			{
				if(is_string($this->arResult["BOOK"]["PROPERTIES"][$name]["VALUE"]) && strlen($this->arResult["BOOK"]["PROPERTIES"][$name]["VALUE"]) > 0)
				{
					$pid = $this->arResult["BOOK"]["PROPERTIES"][$name]["ID"];
					$type = "P";
					return $this->arResult["BOOK"]["PROPERTIES"][$name]["VALUE"];
				}
				else
				{
					$pid = $this->arResult["BOOK"]["PROPERTIES"][$name]["ID"];
					$type = "A";
					if($name == "LIBRARIES")
					{
						$this->getDetailPageUrlForLibraries($this->arResult["BOOK"]["PROPERTIES"][$name]["VALUE"]);
					}
					return $this->arResult["BOOK"]["PROPERTIES"][$name]["VALUE"];
				}
			}
		}
		return false;
	}
	protected function getDetailPageUrlForLibraries(&$arLibraries)
	{
		foreach($arLibraries as &$arLibrary)
		{
			$arLibrary["LIBRARY_DETAIL_PAGE_URL"] = str_replace(array("#SITE_DIR#", "#CODE#"), array(((SITE_DIR=="/")?"":SITE_DIR),$arLibrary["CODE"]), $arLibrary["DETAIL_PAGE_URL"]);
		}
	}
	public function GetFieldUrl($key)
	{
		if(($value = $this->GetField($key, $type, $pid))!==false)
		{
			if($key == "PUBLISH_YEAR")
			{
				return "/search/?".sprintf("publishyear=%d", $value);
			}
			else
			{
				$htmlKey = htmlspecialcharsbx($value);
				$keyCrc = abs(crc32($htmlKey));
				$value = htmlspecialcharsex($value);
				$filterPropertyID = sprintf("searchFilter_%d", $pid);
				$filterPropertyIDKey = sprintf("%s_%d", $filterPropertyID, $keyCrc);
				return "/search/?".sprintf("%s=Y", $filterPropertyIDKey);
			}
		}
		return 'javascript:void(null)';
	}
}