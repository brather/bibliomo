<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/**
 * @var CBitrixComponent $this
 */
if(!CModule::IncludeModule("evk.books") || !CModule::IncludeModule("iblock")) return false;
use Evk\Books\SearchQuery;

$arParams["AJAX"] = (isset($_REQUEST["ajax_tab1"]) && $_REQUEST["ajax_tab1"] == "Y");
if($arParams["AJAX"])
{
	$arParams['ITEM_COUNT'] = 3;
}

if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 3600;
$arParams['ITEM_COUNT'] = intval($arParams['ITEM_COUNT']);
if(empty($arParams['ITEM_COUNT'])) $arParams['ITEM_COUNT'] = 12;

if($this->StartResultCache(false))
{
	$arNavParams = array(
		"nPageSize" => $arParams['ITEM_COUNT'],
	);
	$arFilter = array();
	if(!empty($arParams['LIBRARY_ID']))
	{
		$arFilter["PROPERTY_LIBRARIES"] = array($arParams['LIBRARY_ID']);
	}
	$obSearch = new SearchQuery($arResult, $arParams);
	$arResult['POPULAR_BOOKS'] = $obSearch->FindBooksSimple(array("CREATED" => "DESC"), $arFilter, false, $arNavParams, array(
		"PROPERTY_FILE",
		"PROPERTY_AUTHOR",
		"PROPERTY_PUBLISH_YEAR",
		"PROPERTY_COUNT_PAGES",
		"NAME",
	));
	$arResult['POPULAR_BOOKS_OBJ'] = $obSearch->obNavigate;
	$arResult['MAX_PAGE_COUNT'] = $obSearch->obNavigate->NavPageCount;
	$arResult['NAV_PAGE_NUMBER'] = $obSearch->obNavigate->NavPageNomer;
	if($arResult['NAV_PAGE_NUMBER'] == 1) $num = 2;
	else $num = 1;
	$arResult['NEXT_PAGE_NUMBER'] = $obSearch->obNavigate->NavPageNomer+$num;
	$arResult["NEXT_PAGE_LINK"] = $APPLICATION->GetCurPage()."?ajax_tab1=Y&PAGEN_1=".$arResult['NEXT_PAGE_NUMBER'];
	$this->EndResultCache();
}
if($arParams["AJAX"])
	$APPLICATION->RestartBuffer();
$this->IncludeComponentTemplate();
if($arParams["AJAX"])
	exit;
?>