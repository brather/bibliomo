<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
	use \Bitrix\Main\Localization\Loc as Loc;
	Loc::loadMessages(__FILE__);
	if(empty($arResult['POPULAR_BOOKS']))
		return false;
?>
<?
	if(!empty($arResult['POPULAR_BOOKS']))
	{
	?>
	<div class="b-profile_lk">
		<h2 class="mode"><?=Loc::getMessage('NEB_BOOKS_POPULAR_TITLE_HEAD')?></h2>
		<table class="b-usertable tsize">
			<tr>
				<th><span><?=Loc::getMessage('NEB_BOOKS_POPULAR_AUTHOR')?></span></th>
				<th><span><?=Loc::getMessage('NEB_BOOKS_POPULAR_NAME')?></span></th>
				<th><span><?=Loc::getMessage('NEB_BOOKS_POPULAR_DATE')?></span></th>
				<!-- th><span><?//=Loc::getMessage('NEB_BOOKS_POPULAR_LIB')?></span></th -->
				<th><span><?=Loc::getMessage('NEB_BOOKS_POPULAR_READ')?></span></th>
			</tr>
			<?
				foreach($arResult['POPULAR_BOOKS'] as $arItem)
				{
				?>
				<tr>
					<td class="pl15"><?=$arItem['PROPERTY_AUTHOR_VALUE']?></td>
					<td class="pl15"><a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="popup_opener ajax_opener coverlay" data-width="955" ><?=$arItem['NAME']?></a></td>
					<td class="pl15"><?=$arItem['PROPERTY_PUBLISH_YEAR_VALUE']?></td>
					<!-- td><?//=$arItem['library']?></td -->
					<td><?if(!empty($arItem['VIEWER_URL']))
						{
							?><a href="<?=$arItem['VIEWER_URL']?>" target="_blank"><button type="submit" value="1" class="formbutton">Читать</button></a><?
						}?></td>
				</tr>
				<?
				}
			?>
		</table>
	</div><!-- /.b-popularbooks -->
	<?
	}
?>