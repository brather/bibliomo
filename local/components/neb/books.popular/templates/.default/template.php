<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
	use \Bitrix\Main\Localization\Loc as Loc;
	Loc::loadMessages(__DIR__.'/template.php');
	if(empty($arResult['POPULAR_BOOKS']))
		return false;
?>
<?
	if(!empty($arResult['POPULAR_BOOKS']))
	{
	?>
	<div class="b-popularbooks">
		<h2><?=Loc::getMessage('NEB_BOOKS_POPULAR_TITLE');?></h2>

		<table class="b-usertable tsize">
			<tr>
				<th><span>Автор</span></th>
				<th><span>Название</span></th>
				<th><span>Дата публикации</span></th>
				<!-- th><span>Библиотека</span></th -->
				<th><span>Читать</span></th>
			</tr>
			<?
				foreach($arResult['POPULAR_BOOKS'] as $arItem)
				{
				?>
				<tr>
					<td class="pl15"><?=$arItem['authorbook']?></td>
					<td class="pl15"><a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="popup_opener ajax_opener coverlay" data-width="955" ><?=$arItem['title']?></a></td>
					<td class="pl15"><?=$arItem['publishyear']?></td>
					<!-- td><?//=$arItem['library']?></td -->
					<td><?if(!empty($arItem['VIEWER_URL']) and $arItem['IS_READ'] == 'Y'){?><a href="<?=$arItem['VIEWER_URL']?>" target="_blank"><button type="submit" value="1" class="formbutton">Читать</button></a><?}?></td>
				</tr>
				<?
				}
			?>
		</table>
	</div><!-- /.b-popularbooks -->
	<?
	}
?>