<?
	if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
?>
<div class="b-filter">
	<div class="b-filter_wrapper">
		<a href="#" class="sort sort_opener"><?=GetMessage("QUOTES_SORT");?></a>
		<span class="sort_wrap">
			<a <?=SortingExalead("UF_BOOK_AUTHOR")?>><?=GetMessage("QUOTES_SORT_AUTHOR");?></a>
			<a <?=SortingExalead("UF_BOOK_NAME")?>><?=GetMessage("QUOTES_SORT_NAME");?></a>
			<a <?=SortingExalead("UF_DATE_ADD")?>><?=GetMessage("QUOTES_SORT_DATE");?></a>
		</span>
		<span class="b-filter_act">
			<span class="b-filter_show"><?=GetMessage("QUOTES_SHOW");?></span>
			<a href="<?=$APPLICATION->GetCurPageParam("pagen=10", array("pagen", "dop_filter"));?>" class="b-filter_num b-filter_num_paging<?=($arParams['ITEM_COUNT'] == 10 and !$arParams['LONG_PAGE']) ? ' current' : ''?>">10</a>			
			<a href="<?=$APPLICATION->GetCurPageParam("pagen=25", array("pagen", "dop_filter"));?>" class="b-filter_num b-filter_num_paging<?=($arParams['ITEM_COUNT'] == 25 and !$arParams['LONG_PAGE']) ? ' current' : ''?>">25</a>			
			<a href="<?=$APPLICATION->GetCurPageParam("pagen=30", array("pagen", "dop_filter"));?>" class="b-filter_num b-filter_num_paging<?=($arParams['ITEM_COUNT'] == 30 and !$arParams['LONG_PAGE']) ? ' current' : ''?>">30</a>		
		</span>
	</div>
</div><!-- /.b-filter -->
<a href="#" class="set_opener iblock right"><?=GetMessage("QUOTES_SETTINGS");?></a>
<div class="b-quote_list" data-action="<?=$APPLICATION->GetCurPage();?>?action=save">
	<?
		if(!empty($arResult['ITEMS']))
		{
			foreach($arResult['ITEMS'] as $arItem)
			{
				$Book = $arResult['EVK_BOOKS'][$arItem['UF_BOOK_ID']];
				if(empty($Book))
					continue;
			?>
			<div class="b-quote_item rel removeitem b-result-docitem" id="<?=$arItem['ID']?>">
				<div class="meta minus">
					<div class="b-hint rel"><span><?=GetMessage("QUOTES_REMOVE");?></span> <?=GetMessage("QUOTES_FROM_QUOTES");?></div>
					<a class="b-bookadd fav" data-remove="removeonly" data-callback="reloadRightMenuBlock()" data-url="<?=$APPLICATION->GetCurPageParam('id='.$arItem['ID'].'&action=remove', array("id", "action"));?>" href="#"></a>
				</div> <!-- meta -->
				<div class="b-quote">
					<a href="<?=$Book['DETAIL_PAGE_URL']?>" class="b-bookboardphoto iblock popup_opener ajax_opener coverlay" data-width="955"><img class="loadingimg" src="<?=$Book['IMAGE_URL_PREVIEW']?>" alt=""></a>
					<div class="iblock b-quote_txt">
						<div class="b-quotein">
							<img class="b-quoteimg" src="<?=$arItem['UF_IMG_DATA']?>" alt="">
							<p class="hidden"><?=$arItem['UF_TEXT']?></p>
							<textarea name="quote_text" id="<?=$arItem['ID']?>" class="hidden input"><?=$arItem['UF_TEXT']?></textarea>
						</div>
						<ul class="b-resultbook-info">
							<li><span><?=GetMessage("QUOTES_AUTHOR");?>:</span> <a href="<?=$Book['SEARCH_AUTHOR_URL']?>"><?=$arItem['UF_BOOK_AUTHOR']?></a></li>
							<li><span><?=GetMessage("QUOTES_BOOK");?>:</span> <a href="<?=$Book['DETAIL_PAGE_URL']?>" class="popup_opener ajax_opener coverlay" data-width="955"><?=$arItem['UF_BOOK_NAME']?></a></li>
						</ul>
					</div>

				</div><!-- /.b-quote -->
				<div class="clearfix rel b-quote_act">
					<a class="iblock b-quoteformat bttxt" data-text="текст" data-pdf="pdf" href="#"><?=GetMessage("QUOTES_TEXT");?></a>
					<div class="iblock rel b-myselection_list">
						<a href="#" class="b-openermenu js_openmfavs" data-callback="reloadRightMenuBlock()" data-favs="<?=ADD_COLLECTION_URL?>list.php?t=quotes&id=<?=$arItem['ID']?>"><?=GetMessage("QUOTES_MY_COLLECTIONS");?></a>
						<div class="b-favs">										
							<form class="b-selectionadd collection" action="<?=ADD_COLLECTION_URL?>add.php" method="post">
								<input type="hidden" name="t" value="quotes"/>
								<input type="hidden" name="id" value="<?=$arItem['ID']?>"/>

								<input type="submit" class="b-selectionaddsign" value="+">
								<span class="b-selectionaddtxt"><?=GetMessage("QUOTES_CREATE_COLLECTION");?></span>
								<input type="text" name="name" class="input hidden">
							</form>
						</div><!-- /.b-favs  -->
					</div> <!-- /.b-myselection_list-->
				</div>
			</div><!-- /.b-quote_item rel -->
			<?
			}
		}
	?>
</div><!-- /.b-quote_list -->
<?=$arResult['NAV_STRING']?>