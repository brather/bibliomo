<?
$MESS['QUOTES_SORT'] = 'Sort';
$MESS['QUOTES_SORT_AUTHOR'] = 'Author';
$MESS['QUOTES_SORT_NAME'] = 'Name';
$MESS['QUOTES_SORT_DATE'] = 'Date';
$MESS['QUOTES_SHOW'] = 'Show';
$MESS['QUOTES_SETTINGS'] = 'Settings';
$MESS['QUOTES_REMOVE'] = 'Remove';
$MESS['QUOTES_FROM_QUOTES'] = 'from my quotes';
$MESS['QUOTES_AUTHOR'] = 'Author';
$MESS['QUOTES_BOOK'] = 'Book';
$MESS['QUOTES_TEXT'] = 'text';
$MESS['QUOTES_MY_COLLECTIONS'] = 'my collections';
$MESS['QUOTES_CREATE_COLLECTION'] = 'create a collection';
?>