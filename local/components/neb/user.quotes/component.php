<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

	use \Bitrix\Main\Localization\Loc as Loc;
	Loc::loadMessages(__FILE__);

	CPageOption::SetOptionString("main", "nav_page_in_session", "N");
	if(!CModule::IncludeModule("evk.books"))
	{
		return false;
	}
	use Evk\Books\Books;
	CModule::IncludeModule("highloadblock"); 
	use Bitrix\Highloadblock as HL; 
	use Bitrix\Main\Entity;
	use Evk\Books\Quotes;
	if(isset($_REQUEST['id']))
	{
		$_REQUEST['id'] = intval($_REQUEST['id']);
	}

	if($_REQUEST['action'] == 'remove' and !empty($_REQUEST['id']) and $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')
	{
		$APPLICATION->RestartBuffer();
		Quotes::deleteWithTest($_REQUEST['id']);
		exit();
	}
	elseif($_REQUEST['action'] == 'save' and !empty($_REQUEST['id']) and !empty($_REQUEST['quotetext']) and $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')
	{
		$APPLICATION->RestartBuffer();
		$status = Quotes::update($_REQUEST['id'], $_REQUEST['quotetext']);
		echo json_encode(array('status' => $status));
		exit();
	}

	$arParams['ITEM_COUNT'] = empty($_REQUEST['pagen']) ? 10 : (int)$_REQUEST['pagen'];
	// pagination
	$limit = array(
		'nPageSize' => $arParams['ITEM_COUNT'],
		'iNumPage' => is_set($_GET['PAGEN_1']) ? $_GET['PAGEN_1'] : 1,
		'bShowAll' => false
	);		

	$arNavigation = CDBResult::GetNavParams($limit);		

	$hlblock = HL\HighloadBlockTable::getById(HIBLOCK_QUO_DATA_USERS)->fetch();
	$entity = HL\HighloadBlockTable::compileEntity($hlblock); 
	$entity_data_class = $entity->getDataClass();

	global $by, $order;
	$by = trim(htmlspecialcharsEx($by));
	$order = trim(htmlspecialcharsEx($order));

	$by = empty($by) ? 'UF_DATE_ADD': $by;
	$order = empty($order) ? 'desc': $order;	

	$arOrder = array($by => $order);

	$arParamsGetList = array(
		"select" => array('ID', 'UF_TEXT', 'UF_IMG_DATA', 'UF_BOOK_ID', 'UF_BOOK_NAME', 'UF_BOOK_AUTHOR'),
		"filter" => array('UF_UID' => $USER->GetID()),
		"order" => $arOrder,
		"limit"		=> $limit['nPageSize'],
		"offset"	=> (($limit['iNumPage']-1) * $limit['nPageSize']),
	);

	if((int)$arParams['COLLECTION_ID'] > 0)
	{
		$hlblockLinks = HL\HighloadBlockTable::getById(HIBLOCK_COLLECTIONS_LINKS_USERS)->fetch();
		$entitykLinks = HL\HighloadBlockTable::compileEntity($hlblockLinks);
		$entity_data_classkLinks = $entitykLinks->getDataClass();

		$arParamsGetList['runtime'] = array(
			'link' => array(
				"data_type" => $entity_data_classkLinks,
				'reference' => array('=this.ID' => 'ref.UF_OBJECT_ID'),
			),
		);
		$arParamsGetList['filter']['=link.UF_COLLECTION_ID'] = (int)$arParams['COLLECTION_ID'];
		$arParamsGetList['filter']['=link.UF_TYPE'] = 'quotes';
	}

	$rsData = $entity_data_class::getList($arParamsGetList);

	//подсчет для постранички, костылим
	$arParamsGetListCnt = $arParamsGetList;
	unset($arParamsGetListCnt['limit'], $arParamsGetListCnt['offset'], $arParamsGetListCnt['order']);
	$arParamsGetListCnt['runtime']['CNT'] = array('expression' => array('COUNT(*)'), 'data_type'=>'integer'); 
	$arParamsGetListCnt['select'] = array('CNT');
	$rsDataCnt = $entity_data_class::getList($arParamsGetListCnt); #количество всех элементов	
	$arDataCnt = $rsDataCnt->Fetch();
	$resultCnt = $arDataCnt['CNT']; 

	$rsData = new CDBResult($rsData);
	$rsData->NavStart($arNavigation['SIZEN'], $limit['bShowAll']);
	$rsData->NavRecordCount = $resultCnt;
	$rsData->NavPageSize = $limit['nPageSize'];
	$rsData->bShowAll = $limit['bShowAll'];
	$rsData->NavPageCount = ceil($rsData->NavRecordCount/$rsData->NavPageSize);
	$rsData->NavPageNomer = $limit['iNumPage'];

	$arResult = array();
	$arResult["NAV_STRING"] = $rsData->GetPageNavStringEx($navComponentObject, '', '', $limit['bShowAll']);	

	$arBooksID = array();
	
	while($arData = $rsData->Fetch()){

		if(!in_array($arData['UF_BOOK_ID'], $arBooksID))
			$arBooksID[] = $arData['UF_BOOK_ID'];

		$arResult['ITEMS'][] = $arData;
	}
/*
	if (!CModule::IncludeModule('nota.exalead')) return false;

	use Nota\Exalead\SearchQuery;
	use Nota\Exalead\SearchClient;
*/
	if(!empty($arBooksID))
	{
		$arResult['EVK_BOOKS'] = Books::GetListAccess(array("=ID" => $arBooksID));
	}

	$this->IncludeComponentTemplate();
	$APPLICATION->SetTitle(Loc::getMessage('PAGE_TITLE'));
	$APPLICATION->SetPageProperty("ALERT_REMOVE_MESSAGE", "Удалить цитату из Моих цитат?");
?>