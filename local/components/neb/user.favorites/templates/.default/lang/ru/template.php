<?
$MESS['FAVORITES_SORT'] = 'Сортировать';
$MESS['FAVORITES_SORT_AUTHOR'] = 'По автору';
$MESS['FAVORITES_SORT_MAIN_TITLE'] = 'По названию';
$MESS['FAVORITES_SORT_DATE'] = 'По дате';
$MESS['FAVORITES_SHOW'] = 'Показать';
$MESS['FAVORITES_SETTINGS'] = 'Настройки';
$MESS['FAVORITES_REMOVE'] = 'Удалить';
$MESS['FAVORITES_FROM_MY_LIBRARY'] = 'из Избранного';
$MESS['FAVORITES_AUTHOR'] = 'Автор';
$MESS['FAVORITES_YEAR_PUBLICATION'] = 'Год публикации';
$MESS['FAVORITES_NUMBER_OF_PAGES'] = 'Количество страниц';
$MESS['FAVORITES_PUBLISHER'] = 'Издательство';
$MESS['FAVORITES_MY_COLLECTIONS'] = 'мои подборки';
$MESS['FAVORITES_CREATE_COLLECTION'] = 'cоздать подборку';
?>