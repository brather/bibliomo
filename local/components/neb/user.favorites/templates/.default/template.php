<?
	if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/**
 * @var CBitrixComponentNebUserFavorites $component
 */
?>
<div class="b-filter">
	<div class="b-filter_wrapper">
		<a href="#" class="sort sort_opener"><?=GetMessage("FAVORITES_SORT");?></a>
		<span class="sort_wrap">
			<a <?=SortingExalead("property_author")?>><?=GetMessage("FAVORITES_SORT_AUTHOR");?></a>
			<a <?=SortingExalead("name")?>><?=GetMessage("FAVORITES_SORT_MAIN_TITLE");?></a>
			<a <?=SortingExalead("property_publish_year")?>><?=GetMessage("FAVORITES_SORT_DATE");?></a>
		</span>
		<span class="b-filter_act">
			<span class="b-filter_show"><?=GetMessage("FAVORITES_SHOW");?></span>
			<a href="<?=$APPLICATION->GetCurPageParam("pagen=15", array("pagen", "dop_filter"));?>" class="b-filter_num b-filter_num_paging<?=($arParams['ITEM_COUNT'] == 15 and !$arParams['LONG_PAGE']) ? ' current' : ''?>">15</a>			
			<a href="<?=$APPLICATION->GetCurPageParam("pagen=30", array("pagen", "dop_filter"));?>" class="b-filter_num b-filter_num_paging<?=($arParams['ITEM_COUNT'] == 30 and !$arParams['LONG_PAGE']) ? ' current' : ''?>">30</a>			
			<a href="<?=$APPLICATION->GetCurPageParam("pagen=45", array("pagen", "dop_filter"));?>" class="b-filter_num b-filter_num_paging<?=($arParams['ITEM_COUNT'] == 45 and !$arParams['LONG_PAGE']) ? ' current' : ''?>">45</a>
		</span>
	</div>
</div><!-- /.b-filter -->
<div class="b-filter_list_wrapper">
	<div class="b-result-doc b-result-docfavorite">
		<?
			if(!empty($arResult['ITEMS']))
			{
				$i = 0;
				foreach($arResult['ITEMS'] as $arItem)
				{
					if(!empty($arResult['RESBOOKS'][$arItem['ID']]))
						$arResBooks = $arResult['RESBOOKS'][$arItem['ID']];
					else
						continue;
					$i++;
					$arItem['NUM_ITEM'] = $arResult['START_ITEM'] + $i;
				?>
				<div class="b-result-docitem removeitem search-result" id="<?=$arItem['ID']?>">
					<div class="iblock b-result-docphoto">
							<?if(isset($arItem['IMAGE_URL'])):?>
								<a class="b_bookpopular_photo iblock popup_opener ajax_opener coverlay" data-width="955"  href="<?=$arItem['DETAIL_PAGE_URL']?>">
									<img class="loadingimg" alt="" src="<?=$arItem['IMAGE_URL']?>&width=80&height=125">
								</a>
							<?else:?>
								<a class="b_bookpopular_photo iblock popup_opener ajax_opener coverlay" data-width="955"  href="<?=$arItem['DETAIL_PAGE_URL']?>">
									<img class="loadingimg" alt="" src="/local/images/book_empty.jpg">
								</a>
							<?endif;?>
						<?
							if(!empty($arResBooks['UF_READING_PERCENT']))
							{
								?>
								<div class="b-loadprogress">
									<div class="b-loadlabel"></div>
									<input type="text" class="knob" data-width="37" data-height="37" data-displayInput="false" data-inputColor="#8b9597" data-min="0"  data-min="100"  data-fgColor="#f06735" data-thickness=".1" value="<?=$arResBooks['UF_READING_PERCENT']?>">
								</div>
								<?
							}
						?>
					</div>
					<div class="iblock b-result-docinfo">
						<span class="num"><?=$arItem['NUM_ITEM']?>.</span>
						<div class="meta minus">
							<div class="b-hint rel"><span><?=GetMessage("FAVORITES_REMOVE");?></span> <?=GetMessage("FAVORITES_FROM_MY_LIBRARY");?></div>
							<a class="b-bookadd fav" data-remove="removeonly" data-callback="reloadRightMenuBlock(); location.reload(false);" data-url="<?=$APPLICATION->GetCurPageParam('id=' . intval($arItem['ID']).'&action=remove', array("id", "action"));?>" href="#"></a>
						</div> <!-- meta -->
						<h2><a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="popup_opener ajax_opener coverlay" data-width="955" ><?=$arItem['NAME']?></a></h2>
						<ul class="b-resultbook-info">
							<?
								if(!empty($arItem['PROPERTY_AUTHOR_VALUE']))
								{
									?>
									<li><span><?=GetMessage("FAVORITES_AUTHOR");?>:</span> <a href="<?=$component->GetLink("AUTHOR", $arItem['PROPERTY_AUTHOR_VALUE'])?>"><?=$arItem['PROPERTY_AUTHOR_VALUE']?></a></li>
									<?
								}
								if(!empty($arItem['PROPERTY_PUBLISH_YEAR_VALUE']))
								{
								?>
								<li><span><?=GetMessage("FAVORITES_YEAR_PUBLICATION");?>:</span> <a href="<?=$component->GetLink("PUBLISH_YEAR", $arItem['PROPERTY_PUBLISH_YEAR_VALUE'])?>"><?=$arItem['PROPERTY_PUBLISH_YEAR_VALUE']?></a></li>
								<?
								}
								if(!empty($arItem['PROPERTY_VOLUME_VALUE']))
								{
								?>
								<li><span><?=GetMessage("FAVORITES_NUMBER_OF_PAGES");?>:</span> <?=$arItem['PROPERTY_VOLUME_VALUE']?></li>
								<?
								}
								if(!empty($arItem['PROPERTY_PUBLISH_VALUE']))
								{
								?>
								<li><span><?=GetMessage("FAVORITES_PUBLISHER");?>:</span> <a href="<?=$component->GetLink("PUBLISH", $arItem['PROPERTY_PUBLISH_VALUE'])?>"><?=$arItem['PROPERTY_PUBLISH_VALUE']?></a></li>
								<?
								}
							?>
						</ul>
						<p><?=$arItem['TEXT']?></p>
						<div class="b-result_sorce clearfix">
							<div class="b-result_selection left">
								<div class="iblock rel">
									<a href="#" class="b-openermenu js_openmfavs" data-callback="reloadRightMenuBlock()" data-favs="<?=ADD_COLLECTION_URL?>list.php?t=books&id=<?=$arItem['ID']?>"><?=GetMessage("FAVORITES_MY_COLLECTIONS");?></a>
									<div class="b-favs">
										<form class="b-selectionadd collection" action="<?=ADD_COLLECTION_URL?>add.php" method="post">
											<input type="hidden" name="t" value="books"/>
											<input type="hidden" name="id" value="<?=$arItem['ID']?>"/>
											<input type="submit" class="b-selectionaddsign" value="+">
											<span class="b-selectionaddtxt"><?=GetMessage("FAVORITES_CREATE_COLLECTION");?></span>
											<input type="text" name="name" class="input hidden">
										</form>
									</div><!-- /.b-favs  -->
								</div>
								<div class="iblock b-fav_info">
									<span class="b-fav_info_quote">х <?=$arItem['CNT_QUOTES']?></span>
									<span class="b-fav_info_bmark">х <?=$arItem['CNT_MARKS']?></span>
									<span class="b-fav_info_note">х <?=$arItem['CNT_NOTES']?></span>
								</div>
							</div><!-- /.b-result_selection  -->
							<div class="b-result-type right">
								<?
									if(!empty($arItem['FILE']["FILE_PATH"]))
									{
										?>
										<span class="b-result-type_txt"><a target="_blank" href="<?=$arItem['FILE']["FILE_PATH"]?>">Читать</a></span>
										<?
									}
								?>
							</div>	
						</div><!-- /.b-result_sorce -->
					</div>
				</div><!-- /.b-result-docitem -->
				<?
				}
			}
		?>
	</div><!-- /.b-result-doc -->
</div>
<?=$arResult['STR_NAV']?>