<?php
CBitrixComponent::includeComponentClass("neb:search.page.viewer.left");

class CBitrixComponentNebUserFavorites extends CBitrixComponentSearchPageViewerLeft
{
	public function GetDatesFromSmartFilter($arProps, $propName)
	{
		global $APPLICATION;
		$fieldName = sprintf('smart_%s', strtolower($propName));
		$fieldValue = (isset($_REQUEST[$fieldName]))?trim($_REQUEST[$fieldName]):"";
		$arSmartFilter = array();
		foreach($arProps as $arProp)
		{
			if($propName == "LIBS")
			{
				foreach($arProp[$propName] as $arProp2)
				{
					$arProp2["NAME"] = trim($arProp2["NAME"]);
					if(!empty($arProp2["NAME"]))
					{
						if(!array_key_exists($arProp2["NAME"], $arSmartFilter))
						{
							$checked = (!empty($fieldValue) && ($fieldValue == $arProp2["ID"]));
							$arSmartFilter[$arProp2["NAME"]] = array(
								"COUNT" => 1,
								"VALUE" => $arProp2["ID"],
								"NAME" => $arProp2["NAME"],
								"CHECKED" => $checked,
								"LINK" => $APPLICATION->GetCurPageParam((($checked) ? "" : (sprintf('%s=%s', $fieldName, urlencode(htmlspecialchars($arProp2["ID"]))))), array($fieldName)),
							);
						}
						else
						{
							$arSmartFilter[$arProp2["NAME"]]["COUNT"]++;
						}
					}
				}
			}
			elseif($propName == "COLLECTIONS")
			{
				foreach($arProps as $arProp)
				{
					if(!array_key_exists($arProp["UF_NAME"], $arSmartFilter))
					{
						$checked = (!empty($fieldValue) && ($fieldValue == $arProp["ID"]));
						$arSmartFilter[$arProp["UF_NAME"]] = array(
							"COUNT" => count($arProp["BOOKS"]),
							"VALUE" => $arProp["ID"],
							"NAME" => $arProp["UF_NAME"],
							"CHECKED" => $checked,
							"LINK" => $APPLICATION->GetCurPageParam((($checked) ? "" : (sprintf('%s=%s', $fieldName, urlencode(htmlspecialchars($arProp["ID"]))))), array($fieldName)),
						);
					}
				}
			}
			else
			{
				$arProp[$propName] = trim($arProp[$propName]);
				if(!empty($arProp[$propName]))
				{
					if(!array_key_exists($arProp[$propName], $arSmartFilter))
					{
						$checked = (!empty($fieldValue) && ($fieldValue == $arProp[$propName]));
						$arSmartFilter[$arProp[$propName]] = array(
							"COUNT" => 1,
							"VALUE" => $arProp[$propName],
							"NAME" => $arProp[$propName],
							"CHECKED" => $checked,
							"LINK" => $APPLICATION->GetCurPageParam((($checked) ? "" : (sprintf('%s=%s', $fieldName, urlencode(htmlspecialchars($arProp[$propName]))))),
								array($fieldName)),
						);
					}
					else
					{
						$arSmartFilter[$arProp[$propName]]["COUNT"]++;
					}
				}
			}
		}
		uasort($arSmartFilter, function($a, $b){
			return ($a["COUNT"]<$b["COUNT"])?1:(($a["COUNT"]>$b["COUNT"])?-1:0);
		});
		return $arSmartFilter;
	}
}