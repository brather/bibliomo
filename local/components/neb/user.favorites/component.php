<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/**
 * @var CBitrixComponentNebUserFavorites $this
 */
use \Bitrix\Main\Localization\Loc as Loc;
Loc::loadMessages(__FILE__);

CPageOption::SetOptionString("main", "nav_page_in_session", "N");

if (!CModule::IncludeModule('evk.books')) return false;

CModule::IncludeModule("highloadblock");

use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;
use Evk\Books\SearchQuery;
use Evk\Books\Books;

$arParams["ajax_favorites"] = (isset($_REQUEST["ajax_favorites"]) && $_REQUEST["ajax_favorites"] == "Y");

$hlblock = HL\HighloadBlockTable::getById(HIBLOCK_BOOKS_DATA_USERS)->fetch();
$entity = HL\HighloadBlockTable::compileEntity($hlblock);
$entity_data_class = $entity->getDataClass();

if($_REQUEST['action'] == 'remove' and !empty($_REQUEST['id']) and $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')
{
	$APPLICATION->RestartBuffer();
	Books::deleteWithTest(intval($_REQUEST['id']));
	exit();
}

$arParamsGetList = array(
	"select" => array("*"),
	"filter" => array('UF_UID' => $USER->GetID()),
	"order"	 => array('UF_DATE_ADD' => 'DESC')
);

if(!empty($arParams['UF_READING']))
	$arParamsGetList['filter']['UF_READING'] = $arParams['UF_READING'];

if(isset($_REQUEST["smart_collections"]) && !empty($_REQUEST["smart_collections"]))
	$arParams["COLLECTION_ID"] = intval($_REQUEST["smart_collections"]);

if((int)$arParams['COLLECTION_ID'] > 0)
{
	$hlblockLinks = HL\HighloadBlockTable::getById(HIBLOCK_COLLECTIONS_LINKS_USERS)->fetch();
	$entitykLinks = HL\HighloadBlockTable::compileEntity($hlblockLinks);
	$entity_data_classkLinks = $entitykLinks->getDataClass();

	$arParamsGetList['runtime'] = array(
		'link' => array(
			"data_type" => $entity_data_classkLinks,
			'reference' => array('=this.ID' => 'ref.UF_OBJECT_ID'),
		),
	);

	$arParamsGetList['filter']['=link.UF_COLLECTION_ID'] = (int)$arParams['COLLECTION_ID'];
	$arParamsGetList['filter']['=link.UF_TYPE'] = 'books';
}

$rsData = $entity_data_class::getList($arParamsGetList);

$arBooksID = array();
while($arData = $rsData->Fetch()){
	$arData['UF_DATE_ADD'] = $arData['UF_DATE_ADD']->toString();
	$arResult['RESBOOKS'][$arData['UF_BOOK_ID']] = $arData;
	$arBooksID[] = $arData['UF_BOOK_ID'];
}
if(empty($arBooksID))
	return false;

$arParams['ITEM_COUNT'] = 9;

$by = trim(htmlspecialcharsEx($_REQUEST['by']));
$order = trim(htmlspecialcharsEx($_REQUEST['order']));
if(empty($by))
	$by = "property_author";
if(empty($order))
	$order = "asc";

$arResult["sort_by"] = $by;
$arResult["find_favorites_books"] = (strlen(trim($_REQUEST["find_favorites_books"]))>0) ? htmlspecialchars(trim($_REQUEST["find_favorites_books"])): "";
$arSearchResult = array();

if(!empty($arResult["find_favorites_books"]))
{
	$obSearch = new \Evk\Books\CSearch();
	$arSearchFilter = array (
		"QUERY" => $arResult["find_favorites_books"],
		"SITE_ID" => SITE_ID,
		"MODULE_ID" => "iblock",
		"PARAM1" => "library",
		"PARAM2" => IBLOCK_ID_BOOKS,
	);
	$obSearch->Search($arSearchFilter);
	if ($obSearch->errorno==0)
	{
		$arBooksID2 = array();
		while($arItem = $obSearch->GetNext())
		{
			if(in_array($arItem["ITEM_ID"], $arBooksID))
			{
				$arBooksID2[] = $arItem["ITEM_ID"];
				$arSearchResult[$arItem["ITEM_ID"]] = $arItem;
			}
		}
		$arBooksID = $arBooksID2;
		$findSphinxError = count($arSearchResult) ? 0 : 1;
	}
	else
	{
		$findSphinxError = $obSearch->errorno;
	}
}

if(!isset($findSphinxError) || $findSphinxError == 0)
{
	$arSelectFields = array(
		"*",
		"PROPERTY_FILE",
		"PROPERTY_LIBRARIES",
		"PROPERTY_PUBLISH_YEAR",
		"PROPERTY_AUTHOR",
		"PROPERTY_PUBLISH",
		"PROPERTY_PUBLISH_PLACE",
		"CNT_NOTES",
		"CNT_MARKS",
		"CNT_QUOTES",
	);
	$obSearch = new SearchQuery($arResult, $arParams);

	$arFilter = array(
		"=ID"=>$arBooksID
	);

	if(isset($_REQUEST["smart_property_author_value"]) && !empty($_REQUEST["smart_property_author_value"]))
		$arFilter["PROPERTY_AUTHOR"] = htmlspecialchars($_REQUEST["smart_property_author_value"]);

	if(isset($_REQUEST["smart_libs"]) && !empty($_REQUEST["smart_libs"]))
		$arFilter["PROPERTY_LIBRARIES"] = array(intval($_REQUEST["smart_libs"]));

	if(isset($_REQUEST["smart_property_publish_place_value"]) && !empty($_REQUEST["smart_property_publish_place_value"]))
		$arFilter["PROPERTY_PUBLISH_PLACE"] = htmlspecialchars($_REQUEST["smart_property_publish_place_value"]);

	if(isset($_REQUEST["smart_property_publish_value"]) && !empty($_REQUEST["smart_property_publish_value"]))
		$arFilter["PROPERTY_PUBLISH"] = htmlspecialchars($_REQUEST["smart_property_publish_value"]);

	$result["ITEMS"] = $obSearch->FindBooksSimple(array($by=>$order), $arFilter, false, false, $arSelectFields);
	$arCollections = \Evk\Books\Collections::getListCurrentUser(array_keys($result["ITEMS"]), "books");
	$fieldValue = (isset($_REQUEST['smart_collections'])) ? trim($_REQUEST['smart_collections']) : "";
	foreach ($arCollections as $key => $collection) {
		$collection['LINK'] = $APPLICATION->GetCurPageParam(
			(($checked)
				? ""
				: (
				sprintf(
					'%s=%s',
					'smart_collections',
					urlencode(htmlspecialchars($collection["ID"]))
				)
				)),
			array('smart_collections')
		);
		$collection['CHECKED'] = (!empty($fieldValue) && ($fieldValue == $collection["ID"]));
		$collection['COUNT'] = count($collection['BOOKS']);
		$collection['VALUE'] = $collection['ID'];
		$arCollections[$key] = $collection;
	}
	$arResult["COLLECTIONS"] = ($arCollections !== false) ? $arCollections : array();
	$arResult["SMART"] = array();
	$arResult["SMART"]["COLLECTIONS"]{"NAME"} = "Моя библиотека";
	$arResult["SMART"]["COLLECTIONS"]{"SORT"} = 100;
	$arResult["SMART"]["COLLECTIONS"]{"ITEMS"} = $arCollections;
	$arResult["SMART"]["COLLECTIONS"]{"FIELD_ID"} = "collections";
	$arResult["SMART"]["AUTHORS"]{"NAME"} = "Авторы";
	$arResult["SMART"]["AUTHORS"]{"SORT"} = 200;
	$arResult["SMART"]["AUTHORS"]{"ITEMS"} = $this->GetDatesFromSmartFilter($result["ITEMS"], "PROPERTY_AUTHOR_VALUE");
	$arResult["SMART"]["AUTHORS"]{"FIELD_ID"} = "authors";
	$arResult["SMART"]["PUBLISH"]{"NAME"} = "Издательство";
	$arResult["SMART"]["PUBLISH"]{"SORT"} = 300;
	$arResult["SMART"]["PUBLISH"]{"ITEMS"} = $this->GetDatesFromSmartFilter($result["ITEMS"], "PROPERTY_PUBLISH_VALUE");
	$arResult["SMART"]["PUBLISH"]{"FIELD_ID"} = "publish";
	$arResult["SMART"]["LIBRARIES"]{"NAME"} = "Библиотеки";
	$arResult["SMART"]["LIBRARIES"]{"SORT"} = 400;
	$arResult["SMART"]["LIBRARIES"]{"ITEMS"} = $this->GetDatesFromSmartFilter($result["ITEMS"], "LIBS");
	$arResult["SMART"]["LIBRARIES"]{"FIELD_ID"} = "libraries";
	$arResult["SMART"]["PUBLISH_PLACE"]{"NAME"} = "Место издания";
	$arResult["SMART"]["PUBLISH_PLACE"]{"SORT"} = 500;
	$arResult["SMART"]["PUBLISH_PLACE"]{"ITEMS"} = $this->GetDatesFromSmartFilter($result["ITEMS"], "PROPERTY_PUBLISH_PLACE_VALUE");
	$arResult["SMART"]["PUBLISH_PLACE"]{"FIELD_ID"} = "publish_place";

	$arResult = array_merge($arResult, $result);

	if(is_array($arResult['ITEMS']) || !empty($arResult['ITEMS']))
	{
		$rs = new CDBResult;
		$rs->InitFromArray($arResult['ITEMS']);
		$rs->NavStart($arParams['ITEM_COUNT']);
		$arResult["STR_NAV"] = $rs->GetPageNavStringEx($navComponentObject, "", "");
		$arResult['NAV_COUNT'] = (int)$rs->NavRecordCount;
		unset($arResult['ITEMS']);
		while($arRes = $rs->Fetch())
		{
			if(!empty($arSearchResult))
			{
				$arRes["title"] = $arRes["NAME"] = $arSearchResult[$arRes["ID"]]["TITLE_FORMATED"];
			}
			$arResult['ITEMS'][] = $arRes;
		}
	}
}

$numberPage = ($rs->NavPageNomer < $rs->NavPageCount) ? $rs->NavPageNomer+1 : 0;
$arParams["ajax_link"] = ($numberPage) ? $APPLICATION->GetCurPageParam(
	sprintf("PAGEN_1=%d&ajax_favorites=Y", $numberPage), array("PAGEN_1", "ajax_favorites")
) : false;

if($arParams["ajax_favorites"])
	$APPLICATION->RestartBuffer();

$this->IncludeComponentTemplate();

if($arParams["ajax_favorites"])
	exit;

$APPLICATION->SetTitle(Loc::getMessage('PAGE_TITLE'));
return array('arParams' => $arParams, 'arResult' => $arResult);
?>