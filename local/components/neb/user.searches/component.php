<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

	use \Bitrix\Main;
	use \Bitrix\Main\Localization\Loc as Loc;
	Loc::loadMessages(__FILE__);

	CPageOption::SetOptionString("main", "nav_page_in_session", "N");

	CModule::IncludeModule("highloadblock"); 
	use Bitrix\Highloadblock as HL; 
	use Bitrix\Main\Entity; 
	use Bitrix\Main\Type\DateTime;

	CModule::IncludeModule("nota.userdata"); 
	use Nota\UserData\Searches;	

	$hlblock = HL\HighloadBlockTable::getById(HIBLOCK_SEARCHSERS_USERS)->fetch();
	$entity = HL\HighloadBlockTable::compileEntity($hlblock); 
	$entity_data_class = $entity->getDataClass(); 

	if($_REQUEST['action'] == 'remove' and (int)$_REQUEST['id'] > 0 and $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')
	{
		$APPLICATION->RestartBuffer();
		Searches::delete((int)$_REQUEST['id']);
		exit();
	}

	$arParams['ITEM_COUNT'] = empty($_REQUEST['pagen']) ? 15 : (int)$_REQUEST['pagen'];

	if(!empty($_REQUEST['by']) and !empty($_REQUEST['order']))
		$arOrder = array(strtoupper($_REQUEST['by']) => $_REQUEST['order']);
	else
		$arOrder = array("UF_DATE_ADD" => "DESC");

	// pagination
	$limit = array(
		'nPageSize' => $arParams['ITEM_COUNT'],
		'iNumPage' => is_set($_GET['PAGEN_1']) ? $_GET['PAGEN_1'] : 1,
		'bShowAll' => false
	);		

	$arNavigation = CDBResult::GetNavParams($limit);

	$arParamsGetList = array(
		"select" 	=> array("*"),
		"filter" 	=> array('UF_UID' => $USER->GetID()),
		"order"		=> $arOrder,
		"limit"		=> $limit['nPageSize'],
		"offset"	=> (($limit['iNumPage']-1) * $limit['nPageSize']),
	);

	if((int)$arParams['COLLECTION_ID'] > 0){

		$hlblockLinks = HL\HighloadBlockTable::getById(HIBLOCK_COLLECTIONS_LINKS_USERS)->fetch();
		$entitykLinks = HL\HighloadBlockTable::compileEntity($hlblockLinks); 
		$entity_data_classkLinks = $entitykLinks->getDataClass();

		$arParamsGetList['runtime'] = array(
			'link' => array(
				"data_type" => $entity_data_classkLinks,
				'reference' => array('=this.ID' => 'ref.UF_OBJECT_ID'),
			),
		);
		$arParamsGetList['filter']['=link.UF_COLLECTION_ID'] = (int)$arParams['COLLECTION_ID'];
		$arParamsGetList['filter']['=link.UF_TYPE'] = 'searches';
	}

	$rsData = $entity_data_class::getList($arParamsGetList);

	//подсчет для постранички, костылим
	$arParamsGetListCnt = $arParamsGetList;
	unset($arParamsGetListCnt['limit'], $arParamsGetListCnt['offset'], $arParamsGetListCnt['order']);
	$arParamsGetListCnt['runtime']['CNT'] = array('expression' => array('COUNT(*)'), 'data_type'=>'integer'); 
	$arParamsGetListCnt['select'] = array('CNT');
	$rsDataCnt = $entity_data_class::getList($arParamsGetListCnt); #количество всех элементов	
	$arDataCnt = $rsDataCnt->Fetch();	
	$resultCnt = $arDataCnt['CNT']; 

	$rsData = new CDBResult($rsData);
	$rsData->NavStart($arNavigation['SIZEN'], $limit['bShowAll']);
	$rsData->NavRecordCount = $resultCnt;
	$rsData->NavPageSize = $limit['nPageSize'];
	$rsData->bShowAll = $limit['bShowAll'];
	$rsData->NavPageCount = ceil($rsData->NavRecordCount/$rsData->NavPageSize);
	$rsData->NavPageNomer = $limit['iNumPage'];

	$arResult["NAV_STRING"] = $rsData->GetPageNavStringEx($navComponentObject, '', '', $limit['bShowAll']);

	while($arData = $rsData->Fetch())
	{
		$arData['UF_DATE_ADD'] = $arData['UF_DATE_ADD']->toString();
		$arData['UF_DATE_ADD'] = FormatDate('d.m.Y H:i', MakeTimeStamp($arData["UF_DATE_ADD"]), time());

		if(!empty($arData['UF_MORE_OPTIONS']))
			$arData['UF_MORE_OPTIONS'] = unserialize(htmlspecialcharsBack($arData['UF_MORE_OPTIONS']));

		if(!empty($arData['UF_EXALEAD_PARAM']))
			$arData['UF_EXALEAD_PARAM'] = unserialize(htmlspecialcharsBack($arData['UF_EXALEAD_PARAM']));

		$arResult['ITEMS'][] = $arData; 
	}

	$this->IncludeComponentTemplate();
	$APPLICATION->SetTitle(Loc::getMessage('PAGE_TITLE'));
	
	$APPLICATION->SetPageProperty("ALERT_REMOVE_MESSAGE", "Удалить запрос из Моих поисковых запросов?");
?>