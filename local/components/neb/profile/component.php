<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/*
    CModule::IncludeModule('nota.userdata');
    use Nota\UserData\Rgb;
*/

if (!$USER->IsAuthorized())
    LocalRedirect('/');

$arDefaultUrlTemplates404 = array(
    "main" => "/",
    "profile_edit" => "edit/"
);

$arDefaultVariableAliases404 = array();

$arDefaultVariableAliases = array();

$arComponentVariables = array();

$SEF_FOLDER = "";
$arUrlTemplates = array();

if ($arParams["SEF_MODE"] == "Y") {
    $arVariables = array();

    $arUrlTemplates = CComponentEngine::MakeComponentUrlTemplates($arDefaultUrlTemplates404, $arParams["SEF_URL_TEMPLATES"]);
    $arVariableAliases = CComponentEngine::MakeComponentVariableAliases($arDefaultVariableAliases404, $arParams["VARIABLE_ALIASES"]);

    $componentPage = CComponentEngine::ParseComponentPath($arParams["SEF_FOLDER"], $arUrlTemplates, $arVariables);

    $obNUser = new nebUser();
    $obNUser->getRole();

    if ($obNUser->role == UGROUP_LIB_CODE_ADMIN or $obNUser->role == UGROUP_LIB_CODE_EDITOR or $obNUser->role == UGROUP_LIB_CODE_CONTORLLER) {
        $arLibrary = $obNUser->getLibrary();
        if ($arLibrary === false) {
            ?>
            <center>
                <?
                ShowError("Ошибка! Отсутствует привязка к библиотеке в профиле текущего пользователя.");
                ?>
            </center>
            <?
            return false;
        }
    }

    $arUser = $obNUser->getUser();


    if (empty($componentPage))
        $componentPage = "main";

    if ($obNUser->role == 'user')
        $componentPage .= "_user";
    elseif ($obNUser->role == UGROUP_LIB_CODE_ADMIN or $obNUser->role == UGROUP_LIB_CODE_EDITOR or $obNUser->role == UGROUP_LIB_CODE_CONTORLLER)
        $componentPage .= "_library";
    if ($componentPage == 'profile_update_select_user' && $_POST['action'] == 'update_profile') {
        LocalRedirect('/profile/' . htmlspecialchars($_POST['type']) . '/');
    }

    CComponentEngine::InitComponentVariables($componentPage, $arComponentVariables, $arVariableAliases, $arVariables);
    $SEF_FOLDER = $arParams["SEF_FOLDER"];
}

$arResult = array(
    "FOLDER" => $SEF_FOLDER,
    "URL_TEMPLATES" => $arUrlTemplates,
    "VARIABLES" => $arVariables,
    "ALIASES" => $arVariableAliases,
    "LIBRARY" => $arLibrary,
    "USER" => $arUser,
    "CHECK_STATUS" => $arResult['CHECK_STATUS'],
    "ERROR_MESSAGE" => $arResult['ERROR_MESSAGE'],
);
$arResult['USER_ROLE'] = $obNUser->role;
#pre($componentPage,1);

if (($componentPage == "profile_edit_library") || ($componentPage == "collections_user"))    //Если у пользователя нет доступа до библиотеки, то не даем зайти на страницу коллекций библиотеки
{
    LocalRedirect('/profile/');
}


//echo $componentPage;
$this->IncludeComponentTemplate($componentPage);

?>