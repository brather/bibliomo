<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?
	$this->SetViewTarget('menu_top')
?>
	<?$APPLICATION->IncludeComponent("bitrix:menu", "lk_left", Array(
			"ROOT_MENU_TYPE" => "left",	// Тип меню для первого уровня
			"MENU_CACHE_TYPE" => "N",	// Тип кеширования
			"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
			"MENU_CACHE_USE_GROUPS" => "N",	// Учитывать права доступа
			"MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
				0 => "",
			),
			"MAX_LEVEL" => "2",	// Уровень вложенности меню
			"CHILD_MENU_TYPE" => "",	// Тип меню для остальных уровней
			"USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
			"DELAY" => "N",	// Откладывать выполнение шаблона меню
			"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
			),
			false
		);?>
<?
	$this->EndViewTarget();
?>
<style type="text/css">
	.b-profile_set.b-profile_brd{
		border:none!important;
	}
</style>

<?
	$APPLICATION->IncludeComponent(
		"bitrix:main.profile",
		"",
		Array(
			"USER_PROPERTY_NAME" => "",
			"SET_TITLE" => "Y",
			"AJAX_MODE" => "N",
			"USER_PROPERTY" => array(),
			"SEND_INFO" => "N",
			"LK_USER" => "Y",
			"CHECK_RIGHTS" => "N",
			"AJAX_OPTION_JUMP" => "N",
			"AJAX_OPTION_STYLE" => "Y",
			"AJAX_OPTION_HISTORY" => "N"
		),
		$component
	);
?>