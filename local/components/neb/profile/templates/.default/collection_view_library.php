<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use \Bitrix\Main\Localization\Loc as Loc;
Loc::loadMessages(__DIR__.'/main_library.php'); // Для того, что б не копировать один и тот же текст
Loc::loadMessages(__FILE__);
?>
<section class="innersection innerwrapper clearfix ">
	<div class="b-mainblock left">
		<div class="b-searchresult">
			<?$APPLICATION->IncludeComponent("bitrix:menu", "lk_left", Array(
					"ROOT_MENU_TYPE" => "left",	// Тип меню для первого уровня
					"MENU_CACHE_TYPE" => "N",	// Тип кеширования
					"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
					"MENU_CACHE_USE_GROUPS" => "N",	// Учитывать права доступа
					"MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
						0 => "",
					),
					"MAX_LEVEL" => "2",	// Уровень вложенности меню
					"CHILD_MENU_TYPE" => "",	// Тип меню для остальных уровней
					"USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
					"DELAY" => "N",	// Откладывать выполнение шаблона меню
					"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
				),
				false
			);?>
		</div><!-- /.b-searchresult-->

        <?
        $APPLICATION->IncludeComponent(
            "neb:collections.detail",
            "library_profile",
            array(
                "PAGE_URL" => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['section'],
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "3600",
                "IBLOCK_ID" => IBLOCK_ID_COLLECTION,
                "SECTION_ID" => $arResult["VARIABLES"]["ID"],
                "LIBRARY_ID" => $arResult['LIBRARY']['ID']
            ),
            $component
        );
        ?>
    </div>
</section>