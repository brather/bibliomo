<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?
$APPLICATION->SetTitle("Дополнение персональных данных");
?>

<?
$APPLICATION->IncludeComponent("bitrix:main.profile", "update", Array(
        "AJAX_MODE" => "N",    // Включить режим AJAX
        "AJAX_OPTION_JUMP" => "N",    // Включить прокрутку к началу компонента
        "AJAX_OPTION_STYLE" => "Y",    // Включить подгрузку стилей
        "AJAX_OPTION_HISTORY" => "N",    // Включить эмуляцию навигации браузера
        "SET_TITLE" => "N",    // Устанавливать заголовок страницы
        "USER_PROPERTY" => array(    // Показывать доп. свойства
            0 => "UF_PASSPORT_NUMBER",
            1 => "UF_ISSUED",
            2 => "UF_CORPUS",
            3 => "UF_STRUCTURE",
            4 => "UF_FLAT",
            5 => "UF_HOUSE2",
            6 => "UF_STRUCTURE2",
            7 => "UF_FLAT2",
            8 => "UF_PLACE_REGISTR",
            9 => "UF_SCAN_PASSPORT1",
            10 =>"UF_SCAN_PASSPORT2",
            11 => "UF_PASSPORT_SERIES",
            12 => "UF_BRANCH_KNOWLEDGE",
            13 => "UF_EDUCATION",
            14 => "UF_PLACE_REGISTR",
            15 => "UF_CITIZENSHIP"
        ),
        "SEND_INFO" => "N",    // Генерировать почтовое событие
        "CHECK_RIGHTS" => "N",    // Проверять права доступа
    ),
    false
);
?>