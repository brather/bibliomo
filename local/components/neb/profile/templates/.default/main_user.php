<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
	use \Bitrix\Main\Localization\Loc as Loc;
	Loc::loadMessages(__FILE__);
?>
<section class="innersection innerwrapper clearfix ">
	<div class="b-searchresult" style="border: medium none;">
		<?$APPLICATION->IncludeComponent("bitrix:menu", "lk_left", Array(
				"ROOT_MENU_TYPE" => "left_".LANGUAGE_ID,	// Тип меню для первого уровня
				"MENU_CACHE_TYPE" => "N",	// Тип кеширования
				"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
				"MENU_CACHE_USE_GROUPS" => "N",	// Учитывать права доступа
				"MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
					0 => "",
				),
				"MAX_LEVEL" => "2",	// Уровень вложенности меню
				"CHILD_MENU_TYPE" => "",	// Тип меню для остальных уровней
				"USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
				"DELAY" => "N",	// Откладывать выполнение шаблона меню
				"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
				),
				false
			);?>
	</div>

	<div class="b-mainblock left mt10">
		<?
			$APPLICATION->IncludeComponent(
				"neb:books.popular",
				"lk_library",
				Array(
					"LIBRARY_ID" => $arResult['LIBRARY']["ID"],
					"ITEM_COUNT" => 12,
					"CACHE_TIME" => $arParams["CACHE_TIME"],
				),
				$component
			);
		?>
	</div><!-- /.b-mainblock -->

	<div class="b-side right mt">
		<div class="b-profile_side">
			<div class="b-profile_photo">
				<img alt="user photo" width="110" src="<?=(intval($arResult['USER']['PERSONAL_PHOTO']) > 0)? CFile::GetPath($arResult['USER']['PERSONAL_PHOTO']) :  MARKUP.'i/user_photo.png'?>">
			</div>
			<div class="b-profile_name"><?=$arResult['USER']['NAME'].' '.$arResult['USER']['LAST_NAME']?></div>
			<?if (!empty($arResult['USER']['UF_RGB_CARD_NUMBER']))
			{?>
				<div class="b-profile_email"><?=$arResult['USER']['UF_RGB_CARD_NUMBER']?></div>
			<?}
			if (strpos($arResult['USER']['EMAIL'], $arResult['USER']['UF_RGB_CARD_NUMBER']) === false)
			{?>
				<div class="b-profile_email"><?=$arResult['USER']['EMAIL']?></div>
			<?}?>
			<div class="b-profile_reg"><?=Loc::getMessage('PROFILE_MU_REGISTER'); ?>: <?=ConvertTimeStamp(MakeTimeStamp($arResult['USER']['DATE_REGISTER']))?> <?if(empty($arResult['USER']['UF_STATUS'])){?><?=Loc::getMessage('PROFILE_MU_ACCESS_R'); ?><?}else{?><?=Loc::getMessage('PROFILE_MU_ACCESS_F'); ?><?}?></div>
			<ul class="b-profile_info">
				<? if ($arResult['USER']['UF_NUM_ECHB']):?>
					<?=Loc::getMessage('PROFILE_MU_NUMBER_LC');?>: <?=$arResult['USER']['UF_NUM_ECHB']?><br><br>
					<? endif;?>
				<li><a href="<?=$arResult['FOLDER'].$arResult['URL_TEMPLATES']['profile_edit']?>"><?=Loc::getMessage('PROFILE_MU_PROFILE'); ?></a></li>
			</ul>
            <?
            //упрощенная регистрация?
            if(($arResult['USER']['UF_REGISTER_TYPE'] == 37) || empty($arResult['USER']['UF_REGISTER_TYPE'])){
                ?>
                <a href="<?=$arResult['FOLDER'].$arResult['URL_TEMPLATES']['profile_update_select']?>" class="button_mode"><?=Loc::getMessage('PROFILE_MU_ACCESS_GET'); ?></a>
            <?
            }
            ?>
			<a href="<?=str_replace("#USER_ID#", $arResult["USER"]["ID"], $arParams["FORUM_USER_ALL_MESSAGES_URL"])?>" class="button_mode"><?=Loc::getMessage('PROFILE_MU_VIEW_ALL_MESSAGES'); ?></a>
        </div>
	</div>


</section>

<?if($_GET['FULL_REG_REQ'] == 'Y'):?>
<script type="text/javascript">


    $(function() {
        $('.b-message_ask').doAutoreg();
    });

    (function() { //create closure
        $.fn.doAutoreg = function() { // попап регистрации

            this.each(function() {
                var popup = $(this).clone(true);
                popup.removeClass('hidden');
                popup.dialog({
                    closeOnEscape: true,
                    modal: true,
                    draggable: false,
                    resizable: false,
                    width: ($(window).width() > 550)? 550: 300,
                    dialogClass: 'autoreg_popup',
                    position: "center",
                    open: function(){
                        $('.ui-widget-overlay').addClass('black');
                        popup.find('.closepopup, .pclose').click(function() {
                            //Close the dialog
                            popup.dialog("close").remove();
                            $('.ui-widget-overlay').remove();
                            return false;
                        });
                    },
                    close: function() {
                        popup.dialog("close").remove();
                    }
                });

                $('.ui-widget-overlay').click(function() {
                    //Close the dialog
                    popup.dialog("close");

                });

            });
        }
        //end of closure
    })(jQuery);

</script>
<!--предупреждение о необходимости доп программы.-->
<div class="b-message_ask hidden">
    <a href="#" class="closepopup"><?=Loc::getMessage('PROFILE_MU_CLOSE_WINDOW');?></a>
    <p><?=Loc::getMessage('PROFILE_MU_CLOSE_WINDOW');?></p>
    <p class="tcenter mt10">
        <a href="/profile/update_select/" class="formbutton"><?=Loc::getMessage('PROFILE_MU_YES');?></a>
        <a  href="#" class="formbutton pclose"><?=Loc::getMessage('PROFILE_MU_NO');?></a>
    </p>
</div>
<!--/предупреждение о необходимости доп программы.-->

<?endif;?>