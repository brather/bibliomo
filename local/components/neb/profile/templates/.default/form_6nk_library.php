<?php
/**
 * Created by PhpStorm.
 * User: D-Efremov
 * Date: 20.12.2016
 * Time: 10:48
 */

use \Bitrix\Main\Localization\Loc as Loc;

Loc::loadMessages(__FILE__);

global $APPLICATION, $USER;

$APPLICATION->SetTitle("Отчеты по форме 6-НК");
?>

<?
$APPLICATION->IncludeComponent(
    "elr:reporting.6nk",
    ".default",
    Array(
        "SEF_MODE" => "Y",
        "SEF_FOLDER" => $arParams['SEF_FOLDER'] . str_replace('(.*)', '/', $arParams['SEF_URL_TEMPLATES']['form_6nk']),
        "SEF_URL_TEMPLATES" => array(
            "template" => "",
            "detail" => "#ID#/",
        )
    ),
    $component
);
?>