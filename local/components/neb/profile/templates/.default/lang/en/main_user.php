<?php
    $MESS['PROFILE_MU_REGISTER'] = 'Registered';
    $MESS['PROFILE_MU_ACCESS_R'] = 'limited access';
    $MESS['PROFILE_MU_ACCESS_F'] = 'full access';
    $MESS['PROFILE_MU_PROFILE'] = 'profile settings';
    $MESS['PROFILE_MU_ACCESS_GET'] = 'get full access';
    $MESS['PROFILE_MU_CLOSE_WINDOW'] = 'Close window';
	$MESS['PROFILE_MU_ACCESS_QUESTION'] = 'Access to the private publication is available only for users NEB receiving full registration. Go to registration?';	
	$MESS['PROFILE_MU_YES'] = 'Yes';
	$MESS['PROFILE_MU_NO'] = 'No';
	$MESS['PROFILE_MU_VIEW_ALL_MESSAGES'] = 'My messages';
	$MESS['PROFILE_MU_NUMBER_LC'] = 'Number of SELC';
?>