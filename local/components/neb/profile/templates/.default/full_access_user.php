<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?
	$APPLICATION->SetTitle("Получение единого электронного читательского билета. Шаг 1 из 3");
?>
<section class="innersection innerwrapper clearfix">
	<div class="b-registration rel">

		<h2 class="mode">получение единого электронного читательского билета. Шаг 1 из 3</h2>
		<?
			$APPLICATION->IncludeComponent("bitrix:main.profile", "full_access_1", Array(
				"AJAX_MODE" => "N",	// Включить режим AJAX
				"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
				"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
				"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
				"SET_TITLE" => "N",	// Устанавливать заголовок страницы
				"USER_PROPERTY" => array(	// Показывать доп. свойства
					0 => "UF_BRANCH_KNOWLEDGE",
					1 => "UF_EDUCATION",
				),
				"SEND_INFO" => "N",	// Генерировать почтовое событие
                "CHECK_RIGHTS" => "N",    // Проверять права доступа
				"STEP_2_PAGE" => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['full_access_2'],	// Проверять права доступа
				),
				false
			);
		?>
	</div><!-- /.b-registration-->
</section>