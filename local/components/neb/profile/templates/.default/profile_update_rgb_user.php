<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?
use \Bitrix\Main\Localization\Loc as Loc;
Loc::loadMessages(__FILE__);
$APPLICATION->SetTitle(Loc::getMessage('UPDATE_RGB_TITLE_BROWSER'));
?>

<section class="innersection innerwrapper clearfix">
    <div class="b-registration rel">
        <h2 class="mode"><?=Loc::getMessage('UPDATE_RGB_TITLE');?></h2>
        <form action="" class="b-form b-form_common b-regform" method="post" id="regform">
            <?=bitrix_sessid_post()?>
            <input type="hidden" name="ID" value="<?=$arResult['USER']['ID']?>"/>
            <hr/>
			<?
            if($arResult['CHECK_STATUS'] === false)
            {
                ?>
                <div class="nookresult">
                    <p class="error"><?if(!empty($arResult['ERROR_MESSAGE'])){echo $arResult['ERROR_MESSAGE'];}else{?><?=Loc::getMessage('UPDATE_RGB_DATA_NOT_FOUND');?><?}?></p>
                </div>
            <?
            }
            ?>
            <div class="fieldrow nowrap">
                <div class="fieldcell iblock">
                    <label for="settings01"><?=Loc::getMessage('UPDATE_RGB_LAST_NAME');?></label>
                    <div class="field validate">
                        <input type="text" data-validate="fio" data-required="required" value="<?=$arResult['USER']['LAST_NAME']?>" id="settings01" data-maxlength="30" data-minlength="2" name="LAST_NAME" class="input">
                        <em class="error required"><?=Loc::getMessage('UPDATE_RGB_FILL_IN');?></em>
                        <em class="error validate"><?=Loc::getMessage('UPDATE_RGB_INVALID_FORMAT');?></em>
                        <em class="error maxlength"><?=Loc::getMessage('UPDATE_RGB_MORE_THAN_30_CHAR');?></em>
                    </div>
                </div>
            </div>
            <div class="fieldrow nowrap">
                <div class="fieldcell iblock">
                    <label for="settings02"><?=Loc::getMessage('UPDATE_RGB_NAME');?></label>
                    <div class="field validate">
                        <input type="text" data-required="required" data-validate="fio" value="<?=$arResult['USER']['NAME']?>" id="settings02" data-maxlength="30" data-minlength="2" name="NAME" class="input" >
                        <em class="error required"><?=Loc::getMessage('UPDATE_RGB_FILL_IN');?></em>
                        <em class="error validate"><?=Loc::getMessage('UPDATE_RGB_INVALID_FORMAT');?></em>
                        <em class="error maxlength"><?=Loc::getMessage('UPDATE_RGB_MORE_THAN_30_CHAR');?></em>
                    </div>
                </div>
            </div>
            <div class="fieldrow nowrap">
                <div class="fieldcell iblock">
                    <label for="settings22"><?=Loc::getMessage('UPDATE_RGB_SECOND_NAME');?></label>
                    <div class="field validate">
                        <input type="text" data-validate="fio" value="<?=$arResult['USER']['SECOND_NAME']?>" id="settings22" data-maxlength="30" data-minlength="2" name="SECOND_NAME" class="input" >
                        <em class="error required"><?=Loc::getMessage('UPDATE_RGB_FILL_IN');?></em>
                        <em class="error validate"><?=Loc::getMessage('UPDATE_RGB_INVALID_FORMAT');?></em>
                        <em class="error maxlength"><?=Loc::getMessage('UPDATE_RGB_MORE_THAN_30_CHAR');?></em>
                    </div>
                </div>
            </div>

            <div class="fieldrow nowrap">
                <div class="fieldcell iblock">
                    <label for="settings21" class="disable"><?=Loc::getMessage('UPDATE_RGB_CARD_NUMBER');?></label>
                    <div class="field validate">
                        <input type="text" data-validate="number" value="<?=$arResult['USER']['UF_RGB_CARD_NUMBER']?>" id="settings21" data-required="required" data-maxlength="30" data-minlength="2" name="RGB_CARD_NUMBER" class="input">
                        <em class="error required"><?=Loc::getMessage('UPDATE_RGB_FILL_IN');?></em>
                        <em class="error validate"><?=Loc::getMessage('UPDATE_RGB_INVALID_FORMAT');?></em>
                    </div>
                </div>
            </div>
            <div class="fieldrow nowrap">
                <div class="fieldcell iblock">
                    <label for="settings05"><?=Loc::getMessage('UPDATE_RGB_PASSWORD_LK_RGB');?></label>
                    <div class="field validate">
                        <input type="password" data-validate="password" data-required="required" value="" id="settings05" data-maxlength="30" name="PASSWORD" class="pass_status input">
                        <em class="error required"><?=Loc::getMessage('UPDATE_RGB_FILL_IN');?></em>
                        <em class="error validate"><?=Loc::getMessage('UPDATE_RGB_AT_LEAST_6_CHAR');?></em>
                        <em class="error maxlength"><?=Loc::getMessage('UPDATE_RGB_PASSWORD_MORE_THAN_30');?></em>
                    </div>
                </div>
            </div>
            <div class="fieldrow nowrap">
                <div class="fieldcell iblock">
                    <label for="settings55"><?=Loc::getMessage('UPDATE_RGB_CONFIRM_PASSWORD');?></label>
                    <div class="field validate">
                        <input data-identity="#settings05" type="password" data-required="required" value="" id="settings55" data-maxlength="30" name="CONFIRM_PASSWORD" class="input" data-validate="password">
                        <em class="error identity "><?=Loc::getMessage('UPDATE_RGB_PASSWORD_MISMATCH');?></em>
                        <em class="error required"><?=Loc::getMessage('UPDATE_RGB_FILL_IN');?></em>
                        <em class="error validate"><?=Loc::getMessage('UPDATE_RGB_AT_LEAST_6_CHAR');?></em>
                        <em class="error maxlength"><?=Loc::getMessage('UPDATE_RGB_PASSWORD_MORE_THAN_30');?></em>
                    </div>
                </div>
            </div>
            <hr>
			
			<div class="checkwrapper">
				<label><?=Loc::getMessage('UPDATE_RGB_AGREE_FOR_POPUP');?> </label><a href="/local/components/neb/registration/templates/.default/ajax_agreement.php" target="_blank" class="popup_opener ajax_opener closein" data-width="955" data-height="auto"><?=Loc::getMessage('UPDATE_RGB_AGREE_FOR_POPUP_TERMS');?></a>
			</div>
            <div class="fieldrow nowrap fieldrowaction">
                <div class="fieldcell ">
                    <div class="field clearfix divdisable">
                        <button class="formbutton btdisable" value="1" type="submit" disabled="disabled"><?=Loc::getMessage('UPDATE_RGB_UPDATE');?></button>
						<div class="b-hint"><?=Loc::getMessage('UPDATE_RGB_READ_TERMS');?></div>
                    </div>
                </div>
            </div>            
        </form>
    </div><!-- /.b-registration-->
</section>