<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?
	$APPLICATION->SetTitle("Получение единого электронного читательского билета. Шаг 2 из 3");
?>
<section class="innersection innerwrapper clearfix">
	<div class="b-registration rel">
		<h2 class="mode">получение единого электронного читательского билета. Шаг 2 из 3</h2>

		<?
			$APPLICATION->IncludeComponent("bitrix:main.profile", "full_access_2", Array(
				"AJAX_MODE" => "N",    // Включить режим AJAX
				"AJAX_OPTION_JUMP" => "N",    // Включить прокрутку к началу компонента
				"AJAX_OPTION_STYLE" => "Y",    // Включить подгрузку стилей
				"AJAX_OPTION_HISTORY" => "N",    // Включить эмуляцию навигации браузера
				"SET_TITLE" => "N",    // Устанавливать заголовок страницы
				"USER_PROPERTY" => array(    // Показывать доп. свойства
					0 => "UF_PASSPORT_NUMBER",
					1 => "UF_ISSUED",
					2 => "UF_CORPUS",
					3 => "UF_STRUCTURE",
					4 => "UF_FLAT",
					5 => "UF_HOUSE2",
					6 => "UF_STRUCTURE2",
					7 => "UF_FLAT2",
					8 => "UF_PLACE_REGISTR",
					9 => "UF_SCAN_PASSPORT1",
					10 =>"UF_SCAN_PASSPORT2",
				),
				"SEND_INFO" => "N",    // Генерировать почтовое событие
				"CHECK_RIGHTS" => "N",    // Проверять права доступа
				"STEP_1_PAGE" => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['full_access'],    // Проверять права доступа
				"STEP_3_PAGE" => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['full_access_3'],    // Проверять права доступа
				),
				false
			);
		?>




	</div><!-- /.b-registration-->



</section>