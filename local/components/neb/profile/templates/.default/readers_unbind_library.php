<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?

$APPLICATION->RestartBuffer();

$jsonResult = array();

if(check_bitrix_sessid() && $GLOBALS["USER"]->IsAuthorized())
{
	$USER_ID = intval($arResult["VARIABLES"]["USER_ID"]);
	CModule::IncludeModule('nota.userdata');
	$obEUser = new nebUser($USER_ID);
	$aELibrary = $obEUser->getLibrary();
	if($arResult['LIBRARY']["ID"] == $aELibrary["ID"])
	{
		$user = new nebUser();
		if($user->Update($USER_ID, Array("UF_LIBRARY" => "",)))
		{
			$jsonResult = array(
				"error" => 0,
				"message" => "Вы успешно удалили читателя из библиотеки",
			);
		}
		else
		{
			$jsonResult = array(
				"error" => 2,
				"message" => "Не удалось удалить читателя из библиотеки. Обратитесь, пожалуйста, за консультацией к администратору сайта.",
			);
		}
	}
	else
	{
		$jsonResult = array(
			"error" => 3,
			"message" => "Невозможно удалить указанного читателя, так как он не приписен к вашей библиотеке.",
		);
	}
	echo json_encode($jsonResult);
	die();
}
$jsonResult = array(
	"error" => 1,
	"message" => "Неверные параметры запроса.",
);
echo json_encode($jsonResult);
die();
?>