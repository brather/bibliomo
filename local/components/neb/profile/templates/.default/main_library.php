<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use \Bitrix\Main\Localization\Loc as Loc;
Loc::loadMessages(__FILE__);
?>
<section class="innersection innerwrapper clearfix ">
	<div class="b-mainblock left">
		<div class="b-searchresult">
			<?$APPLICATION->IncludeComponent("bitrix:menu", "lk_left", Array(
					"ROOT_MENU_TYPE" => "left",	// Тип меню для первого уровня
					"MENU_CACHE_TYPE" => "N",	// Тип кеширования
					"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
					"MENU_CACHE_USE_GROUPS" => "N",	// Учитывать права доступа
					"MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
						0 => "",
					),
					"MAX_LEVEL" => "2",	// Уровень вложенности меню
					"CHILD_MENU_TYPE" => "",	// Тип меню для остальных уровней
					"USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
					"DELAY" => "N",	// Откладывать выполнение шаблона меню
					"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
					),
					false
				);?>
		</div><!-- /.b-searchresult-->
		<div class="b-profile_lkact">
			<?
			if($arResult['USER_ROLE'] != UGROUP_LIB_CODE_CONTORLLER)
			{
			?>
			<div class="b-profile_lkbt iblock">
				<a href="/profile/collection/add/" class="gray button_mode">+ <?=Loc::getMessage('PROFILE_ML_COLLECTION'); ?></a>
				<a href="/profile/news/add/" class="gray button_mode">+ <?=Loc::getMessage('PROFILE_ML_NEWS'); ?></a>
				<a href="/profile/readers/new/" class="gray button_mode">+ <?=Loc::getMessage('PROFILE_ML_READER'); ?></a>
			</div>
			<?
			}
			?>
			<div class="iblock b-lib_fulldescr">
				<?
					if(!empty($arResult['LIBRARY']['PROPERTY_STATUS_VALUE']))
					{
					?>
					<div class="b-libstatus"><?=$arResult['LIBRARY']['PROPERTY_STATUS_VALUE']?></div>
					<?
					}
				?>
				<h2><?=$arResult['LIBRARY']['NAME']?></h2>
				<!--	<div class="b-libsitelink">
				<a href="#" >изменить информацию о библиотеке</a>
				<a href="#" class="lk_msg">личные сообщения</a>
				<div class="b-profile_messages"><span>3</span></div>
				</div>-->
			</div><!-- /.b-lib_fulldescr -->
		</div>

		<?
			$APPLICATION->IncludeComponent(
				"neb:books.popular",
				"lk_library",
				Array(
					"LIBRARY_ID" => $arResult['LIBRARY']["ID"],
					"ITEM_COUNT" => 12,
					"CACHE_TIME" => $arParams["CACHE_TIME"],
				),
				$component
			);
		?>
	</div><!-- /.b-mainblock -->
	<div class="b-side right b-sidesmall">
		<a href="<?=$arResult['LIBRARY']['DETAIL_PAGE_URL']?>" class="b-btlibpage"><?=Loc::getMessage('PROFILE_ML_PAGE'); ?></a>
		<?
			$APPLICATION->IncludeComponent(
				"neb:library.right_counter",
				"",
				Array(
					"IBLOCK_ID" => Bitrix\NotaExt\Iblock\IblockTools::getIBlockId(IBLOCK_CODE_LIBRARY),
					"LIBRARY_ID" => $arResult['LIBRARY']["ID"],
					"CACHE_TIME" => $arParams["CACHE_TIME"],
				),
				$component
			);
		?>
	</div><!-- /.b-side -->
</section>