<?
	if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
		die();
	use \Bitrix\Main\Localization\Loc as Loc;
	Loc::loadMessages(__FILE__);
	if(empty($arResult['ITEMS']))
		return false;
?>

<?if(!$arResult['AJAX']){?>
	<div class="b-bookinfo_new iblock">
		<div class="b-bookinfo_tit">
			<h2><?=Loc::getMessage('NEW_LIST_TEMPLATE_NEW'); ?></h2> <a href="/search/?date1=<?=urlencode($arResult['DATE_1'])?>" class="b-searchpage_lnk"><?=$arResult['MONTH']?>: <?=Loc::getMessage('NEW_LIST_TEMPLATE_ADDED'); ?> <strong><?=$arResult['COUNT']?></strong></a>
		</div>
		<?}?>
	<ul class="b-bookboard_list">
		<?
			foreach($arResult['ITEMS'] as $arElement)
			{
			?>

			<li id="<?=urlencode($arElement['ID'])?>">
				<div class="b_popular_descr">
					<h4 class="iblock"><a href="<?=$arElement['LINK']?>" class="popup_opener ajax_opener coverlay" data-width="955" ><?=$arElement['TITLE']?></a></h4>
					<?
						if(!empty($arElement['AUTHOR']))
						{
						?>
						<span class="b-autor iblock"><?=Loc::getMessage('NEW_LIST_TEMPLATE_AUTHOR'); ?>: <a  href="<?=$arElement['AUTHOR_LINK']?>" class="lite"><?=$arElement['AUTHOR']?></a></span>
						<?
						}
					?>
					<?
						if(!empty($arElement['LIBRARY']))
						{
						?>
						<div class="b-result_sorce_info iblock"><em><?=Loc::getMessage('NEW_LIST_TEMPLATE_SOURCE'); ?>:</em> <a href="<?=$arElement['LIB_LINK']?>" class="b-sorcelibrary"><?=$arElement['LIBRARY']?></a></div>
						<?
						}
					?>
				</div>
			</li>
<?
			}
		?>
	</ul>
	<a class="button_mode js_ajax_morebook" id="js_ajax_morebook" href="<?=$this->__folder?>/ajax.php?cnt=<?=$arParams['COUNT']?>&library_id=<?=$arParams['LIBRARY_ID']?>&date=<?=$arParams['DATE']?>&next_book=<?=$arParams['NEXT_BOOK']?>&page_url=<?=urlencode($arParams['PAGE_URL'])?>"><?=Loc::getMessage('NEW_LIST_TEMPLATE_NEXT'); ?></a>
	<?
		if(!$arResult['AJAX'])
		{
		?>
	</div> <!-- /.b-bookinfo_new -->
	<?
	}
?>
