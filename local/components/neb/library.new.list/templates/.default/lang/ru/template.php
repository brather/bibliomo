<?
	$MESS['NEW_LIST_TEMPLATE_NEW'] = 'новые поступления';
	$MESS['NEW_LIST_TEMPLATE_ADDED'] = 'добавлено';
	$MESS['NEW_LIST_TEMPLATE_BOOK'] = 'книги';
	$MESS['NEW_LIST_TEMPLATE_NEXT'] = 'следующие';
	$MESS['ADD_MY_LIB'] = 'Добавить в Мою библиотеку';
	$MESS['ADD_MY_LIB'] = '<span>Добавить</span> в Мою библиотеку';
	$MESS['REMOVE_MY_LIB'] = '<span>Удалить</span> из Моей библиотеки';

	$MESS['NEW_LIST_TEMPLATE_AUTHOR'] = 'Автор';
	$MESS['NEW_LIST_TEMPLATE_SOURCE'] = 'Источник';
