<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}
use Bitrix\Main\Localization\Loc;
CModule::IncludeModule("evk.books");
/*
CModule::IncludeModule('nota.exalead');
CModule::IncludeModule('nota.library');
use Nota\Exalead\SearchQuery;
use Nota\Exalead\SearchClient;
*/
Loc::loadMessages(__FILE__);

class NebLibraryNewListCBitrixComponent extends CBitrixComponent
{
	public function onPrepareComponentParams($arParams)
	{
		$this->arParams['DATE'] = intval($arParams['DATE']);
		$this->arParams['COUNT'] = intval($arParams['COUNT']);
		$this->arParams['PAGE_URL'] = trim($arParams['PAGE_URL']);
		$this->arParams['NEXT_BOOK'] = intval($arParams['NEXT_BOOK']);
		$this->arParams['LIBRARY_ID'] = intval($arParams['LIBRARY_ID']);
		return $arParams;
	}
	
	public function getResult()
	{
		global $APPLICATION;
		$this->arResult["ajax_new_books"] = (isset($_REQUEST{"ajax_new_books"}) && $_REQUEST{"ajax_new_books"} == "Y");

		if(!empty($this->arParams['LIBRARY_ID']))
		{
			$arLib = \Evk\Books\IBlock\Element::getByID($this->arParams['LIBRARY_ID'], array('PROPERTY_LIBRARY_LINK', 'skip_other'));
		}
		$arFilter = array(
			"PROPERTY_LIBRARIES" => array($this->arParams['LIBRARY_ID']),
		);
		$time_stamp = AddToTimeStamp(array('DD' => - $this->arParams['DATE']));
		$arFilter[">=DATE_CREATE"] = ConvertTimeStamp($time_stamp);
		$obSearch = new \Evk\Books\SearchQuery();
		$arNavParams = array();
		$arNavParams["nPageSize"] = 3;
		if(!$this->arResult["ajax_new_books"])
		{
			$arNavParams["iNumPage"] = 1;
		}
		$this->arResult["ITEMS"] = $obSearch->FindBooksSimple(array("CREATED"=>"DESC"), $arFilter, false, $arNavParams,
			array (
			"PROPERTY_AUTHOR",
			"PROPERTY_LIBRARIES",
			"PROPERTY_STATUS",
			"PROPERTY_FILE",
			"PROPERTY_VOLUME",
			"PROPERTY_PUBLISH_YEAR",
			"PROPERTY_PUBLISH",
			"PROPERTY_PUBLISH_PLACE",
			"PROPERTY_COUNT_PAGES",
			"NAME",
		));
		foreach($this->arResult["ITEMS"] as &$arBoor)
		{
			$arBoor["PROPERTY_PUBLISH_YEAR_VALUE"] = (intval($arBoor["PROPERTY_PUBLISH_YEAR_VALUE"]) >=900 && intval($arBoor["PROPERTY_PUBLISH_YEAR_VALUE"]) <= date("Y")) ? intval($arBoor["PROPERTY_PUBLISH_YEAR_VALUE"]): null;
		}
		$paramString = sprintf('ajax_new_books=Y');
		$this->arResult["NavNextPageLink"] = \Evk\Books\PageNavigation::GetNextPage($obSearch->obNavigate, $paramString, array('ajax_new_books'));
	}

	public function executeComponent()
	{
		global $APPLICATION;
		$this->getResult();
		if($this->arResult["ajax_new_books"])
			$APPLICATION->RestartBuffer();
		$this->includeComponentTemplate();
		if($this->arResult["ajax_new_books"])
			exit;
	}
}

?>