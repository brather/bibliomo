<?
	if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

	$arComponentParameters = array(
		"GROUPS" => array(
		),
		"PARAMETERS" => array(

			"VARIABLE_ALIASES" => array(
			),
			"SEF_MODE" => array(
				"main" => array(
					"NAME" => "main PAGE",
					"DEFAULT" => "/",
					"VARIABLES" => array()
				),
				"simple" => array(
					"NAME" => "simple PAGE",
					"DEFAULT" => "simple/",
					"VARIABLES" => array()
				),
				"full" => array(
					"NAME" => "full PAGE",
					"DEFAULT" => "full/",
					"VARIABLES" => array()
				),
				"rgb" => array(
					"NAME" => "rgb PAGE",
					"DEFAULT" => "rgb/",
					"VARIABLES" => array()
				),
				"ecia" => array(
					"NAME" => "ecia PAGE",
					"DEFAULT" => "ecia/",
					"VARIABLES" => array()
				),
			),
		),
	);
?>