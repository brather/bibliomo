<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

	CModule::IncludeModule('nota.userdata');
	use Nota\UserData\Rgb; 

	if ($USER->IsAuthorized()) LocalRedirect('/');


	$arDefaultUrlTemplates404 = array(
		"main" => "/",
		"full" => "full/",
		"rgb" => "rgb/",
		"ecia" => "ecia/",
	);

	$arDefaultVariableAliases404 = array();

	$arDefaultVariableAliases = array();

	$arComponentVariables = array();

	$SEF_FOLDER = "";
	$arUrlTemplates = array();

	if ($arParams["SEF_MODE"] == "Y")
	{
		$arVariables = array();

		$arUrlTemplates = CComponentEngine::MakeComponentUrlTemplates($arDefaultUrlTemplates404, $arParams["SEF_URL_TEMPLATES"]);
		$arVariableAliases = CComponentEngine::MakeComponentVariableAliases($arDefaultVariableAliases404, $arParams["VARIABLE_ALIASES"]);

		$componentPage = CComponentEngine::ParseComponentPath($arParams["SEF_FOLDER"], $arUrlTemplates, $arVariables);

		if (StrLen($componentPage) <= 0) $componentPage = "main";

		CComponentEngine::InitComponentVariables($componentPage, $arComponentVariables, $arVariableAliases, $arVariables);

		$SEF_FOLDER = $arParams["SEF_FOLDER"];

	} 

	$arResult = array(
		"FOLDER" => $SEF_FOLDER,
		"URL_TEMPLATES" => $arUrlTemplates,
		"VARIABLES" => $arVariables,
		"ALIASES" => $arVariableAliases
	);

	$type = htmlspecialcharsbx($_REQUEST['type']);
	if(empty($type) and !empty($_REQUEST['types']))
		$type = htmlspecialcharsbx($_REQUEST['types']);

	if($componentPage == 'main' and !empty($type))
	{
		if(!empty($arResult['URL_TEMPLATES'][$type]))
		{
			$USER->SetParam("REGISTER_TYPE", $type);
			LocalRedirect($arResult['FOLDER'].$arResult['URL_TEMPLATES'][$type]);
		}
	}
	elseif($componentPage == 'rgb')
	{
		if($_SERVER['REQUEST_METHOD'] == 'POST' and check_bitrix_sessid())
		{
			$RGB_CARD_NUMBER 	= htmlspecialcharsbx($_REQUEST['RGB_CARD_NUMBER']);
			$PASSWORD 			= htmlspecialcharsbx($_REQUEST['PASSWORD']);
			$LAST_NAME 			= htmlspecialcharsbx($_REQUEST['LAST_NAME']);
			$NAME 				= htmlspecialcharsbx($_REQUEST['NAME']);
			$SECOND_NAME 		= htmlspecialcharsbx($_REQUEST['SECOND_NAME']);

			$res = Rgb::getUserByCredential($RGB_CARD_NUMBER, $PASSWORD, 'reader_number');
			if((int)$res['uid'] > 0)
			{
				$arResult['CHECK_STATUS'] = true;

				$nebUser = new nebUser();

				$rsUser = $nebUser->GetByLogin($RGB_CARD_NUMBER);
				$arUser = $rsUser->Fetch();
				if(!empty($arUser))
				{
					# пользователь с таким логином уже существует
					$arResult['CHECK_STATUS'] = false;
					$arResult['ERROR_MESSAGE'] = 'Пользователь с данным номером читательского билета уже зарегистрирован';
				}
				else
				{
					$arFieldsUser = array(
						'LOGIN' 			=> $RGB_CARD_NUMBER,
						'EMAIL' 			=> $RGB_CARD_NUMBER.'@elar.ru',
						'LAST_NAME' 		=> $LAST_NAME,
						'NAME' 				=> $NAME,
						'SECOND_NAME'		=> $SECOND_NAME,
						'PASSWORD'			=> $PASSWORD,
						'CONFIRM_PASSWORD'	=> $PASSWORD,
						'ACTIVE'			=> 'Y',
						'UF_RGB_CARD_NUMBER'=> $RGB_CARD_NUMBER,
						'UF_RGB_USER_ID'	=> $res['uid'],
						'UF_LIBRARY'		=> RGB_LIB_ID,
					);

					$UID_BX = $nebUser->Add($arFieldsUser);
					if($UID_BX > 0)
					{
						$nebUser->setEICHB();
						$arResult['RGB_CARD_NUMBER'] = $RGB_CARD_NUMBER;
						$arResult['EICHB'] = $nebUser->EICHB;

						$USER->Authorize($UID_BX);
						CUser::SendUserInfo($UID_BX, 's1', "Приветствуем Вас как нового пользователя!");

						$componentPage = 'rgb_ok';
					}
					else
						$arResult['CHECK_STATUS'] = false;
				}
			}
			else
				$arResult['CHECK_STATUS'] = false;
		}
	}

	$this->IncludeComponentTemplate($componentPage);

?>