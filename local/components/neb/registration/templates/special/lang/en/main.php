<?php
	$MESS['SPECIAL_REG_TITLE'] = 'Registration on the Portal';
	$MESS['SPECIAL_REG_SELECT'] = 'Select the method of registration';
	$MESS['SPECIAL_REG_SIMPLE'] = 'Simplified registration';
	$MESS['SPECIAL_REG_FULL'] = 'Full Registration';
	$MESS['SPECIAL_REG_RGB'] = 'Registration for the readers RGB';
	$MESS['SPECIAL_REG_ESIA'] = 'Register with ESIA';
    $MESS['SPECIAL_REG_STANDART'] = 'Standart Registration';
	$MESS['SPECIAL_REG_NEXT'] = 'Next';
?>