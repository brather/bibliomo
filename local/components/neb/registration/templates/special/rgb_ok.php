<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<section class="innersection innerwrapper clearfix">
	<div class="b-registration rel">
		<h2 class="mode">Поздравляем! Вы успешно зарегистрированы</h2>
		<div class="succsestxt rgbok">
			<div class="iblock">
				<p><span>Ваш логин</span></p>
				<p class="nechb"><?=$arResult['RGB_CARD_NUMBER']?></p>
			</div>
			<div class="iblock">
				<p><span>Ваш читательский билет</span></p>
				<p class="nechb"><?=$arResult['EICHB']?></p>
			</div>
			<p>
				<a href="/profile/" class="formbutton">личный кабинет</a>
				<a href="/search/" class="black">Перейти к поиску </a>
				<a href="/upload/documents/user_agreement_neb.docx" target="_blank" class="black l-print">Распечатать соглашение</a>
			</p>
		</div>
	</div><!-- /.b-registration-->
</section>