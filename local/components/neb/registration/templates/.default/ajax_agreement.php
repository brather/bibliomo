<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
define("STOP_STATISTICS", true);
define("NOT_CHECK_PERMISSIONS", true);
use \Bitrix\Main\Localization\Loc as Loc;
Loc::loadMessages(__FILE__);
?>
<div class="b-bookpopup popup" >
	<a href="#" id="closebtn" class="closepopup close_in"><?=Loc::getMessage('AJAX_AGREEMENT_CLOSE_POPUP');?></a>
	<div class="b-bookpopup_in bbox">
		<div id="div_iframe">
			<!--iframe id="iframe" src="/local/components/neb/main.register/templates/lib_new_user/agreement.php" height="500px" width="100%"></iframe-->
			<iframe id="iframe" src="/user-agreement/agreement-text.php" height="500px" width="100%"></iframe>
		</div>			
		<div class="checkwrapper">
			<input class="checkbox agreebox" type="checkbox" name="agreebox" id="cb1"><label for="cb1" class="black"><?=Loc::getMessage('AJAX_AGREEMENT_AGREE_WITH_PROCESSING_DATA');?></label>
			<div class="field clearfix divdisable">
				<a href="javascript:void(0)" onclick="sign_up();" class="formbutton btdisable close_popup_ok"><?=Loc::getMessage('AJAX_AGREEMENT_AGREE_WITH_TERMS');?></a>
				<div class="b-hint"><?=Loc::getMessage('AJAX_AGREEMENT_READ_TERMS');?></div>
			</div>
		</div>
	</div>
</div>
<script>
var scroll_bool = false;
var check_bool = false;
$('#iframe').load(function(){
	if (!($('button.formbutton').hasClass('btdisable')))
	{
		scroll_bool = true;
        $('.checkbox').trigger("click");
	}
	addDisable($('#regform button.formbutton'));
});
var fr = document.getElementById('iframe');
var fw = (fr.contentWindow || fr.contentDocument);
if ( !!!fw.document) fw = fw.defaultView;
$(fw).scroll(function()
{
	if  ($(fw).scrollTop() == $(fw).height() - $(fr).height())
	{
		scroll_bool = true;
		check_status();
	}
});
$('.agreebox').on('change',function(){
	if ($('.agreebox').attr("checked"))
		check_bool = true;
	else
		check_bool = false;
	check_status();
});

function check_status(){
	if ((scroll_bool == true) && (check_bool == true))
	{
		removeDisable($('a.formbutton'));
	}
	else
	{
		addDisable($('a.formbutton'));
	}
}

function sign_up(){
	if (!($('a.formbutton').hasClass("btdisable")))
	{
		$('.uipopupcontainer').dialog("close");
		removeDisable($('#regform button.formbutton'));
	}
	return false;
}

function removeDisable(elem){
	elem.removeClass('btdisable');
	elem.prop('disabled', false);
	elem.parent().removeClass('divdisable');
}
function addDisable(elem){
	elem.addClass('btdisable');
	elem.prop('disabled', true);
	elem.parent().addClass('divdisable');
}
</script>