<?php
$MESS['REG_RGB_OK_MESSAGE'] = 'Specified in the questionnaire data is successfully verified.<br/>
Within a few hours of your profile will be fully tested by the operator group recording readers NEL.<br/>
The result of verification will be sent to your email. In case of inconsistencies your access may be blocked.';
$MESS['REG_RGB_OK_LOGIN'] = 'Your Login';
$MESS['REG_RGB_OK_CARD_NUMBER'] = 'Your Card Number';
$MESS['REG_RGB_OK_LINK_LK'] = 'Private office';
$MESS['REG_RGB_OK_LINK_SEARCH'] = 'Skip to search';
$MESS['REG_RGB_OK_PRINT_AGREE'] = 'Print an agreement with the NEL';
?>