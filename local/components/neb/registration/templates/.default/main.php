<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
	use \Bitrix\Main\Localization\Loc as Loc;
	Loc::loadMessages(__FILE__);
?>
<section class="innersection innerwrapper clearfix">
	<div class="b-registration rel">
		<h2 class="mode"><?=Loc::getMessage('REG_TITLE');?></h2>
		<form action="<?=$APPLICATION->GetCurPage();?>" class="b-form b-form_common b-regform">
			<hr/>
			<div class="fieldrow nowrap radiolist">
				<div class="fieldcell iblock mt10">
					<label for="settings12"><?=Loc::getMessage('REG_SELECT');?>:</label>
					<div class="field validate">
						<div class="b-radio">
							<input type="radio" name="types" value="simple" class="radio" id="rb4" checked="checked">
							<label for="rb4"><?=Loc::getMessage('REG_SIMPLE');?></label>
						</div>	
						<div class="b-radio">
							<input type="radio" name="types" value="full" class="radio" id="rb1">
							<label for="rb1"><?=Loc::getMessage('REG_FULL');?></label>
						</div>	
					</div>
					<div class="field validate sub_radio">
						<div class="b-radio">
							<input type="radio" name="type" value="full" class="radio" id="rb5" disabled="">
							<label for="rb5"><?=Loc::getMessage('REG_STANDART');?></label>
						</div>

						<div class="b-radio">
							<input type="radio" name="type" value="rgb" class="radio" id="rb2" disabled="">
							<label for="rb2"><?=Loc::getMessage('REG_RGB');?></label>
						</div>
						<div class="b-radio">
							<input type="radio" name="type" value="ecia" class="radio" id="rb3" disabled="">
							<label class="disable" for="rb3"><?=Loc::getMessage('REG_ESIA');?></label>
						</div>	
					</div>
				</div>
			</div>
			<hr/>
			<div class="fieldrow nowrap fieldrowaction">
				<div class="fieldcell">
					<div class="field clearfix">
						<button class="formbutton" value="1" type="submit"><?=Loc::getMessage('REG_NEXT');?></button>
					</div>
				</div>
			</div>
		</form>
	</div><!-- /.b-registration-->
</section>
<?if($GLOBALS['USER']->GetParam('recomended_registration_mode') === 'full') : ?>
<?$GLOBALS['USER']->SetParam('recomended_registration_mode', null)?>
<script>
    $(document).ready(function(){
        $('#rb1').click();
    });
</script>
<? endif; ?>
