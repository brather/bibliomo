<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

use \Bitrix\Main\Localization\Loc as Loc;
Loc::loadMessages(__FILE__);
?>

<div class="b-quote_list">
	<?foreach($arResult['BOOK'] as $arItem){?>
		<div class="b-quote_item removeitem">
			<a class="b-result-remove" href="#"></a>
				<div class="b-quote">
					<?if($arItem['PREVIEW_PICTURE']){?>
						<a href="#" class="b-bookboardphoto iblock"><img src="<?=$arItem['PREVIEW_PICTURE']?>" alt=""></a>
					<?}?>
					<div class="iblock b-quote_txt">
						<p><?=$arItem['PREVIEW_TEXT']?></p>
						<ul class="b-resultbook-info">
							<li><span>Автор:</span> <a href="#"><?=$arItem['AUTHOR']?></a></li>
							<li><span>Книга:</span> <a href="#"><?=$arItem['NAME']?></a></li>
		
						</ul>
					</div>
					
				</div><!-- /.b-quote -->
				<div class="clearfix rel b-quote_act">
					<div class="b-quote_copy iblock">
						<a href="#" class="b-quote_copy_label">Скопировать</a>
						<div class="iblock ">
							<a href="#" class="b-openermenu js_openmenu">подпись ГОСТ</a>
							<ul class="b-gostlist">
								<li><span>Исаев И.А. История государства и права России : учеб. пособие для студ. вузов / И.А. Исаев. - М. : Проспект, 2009. - 336 с.</span></li>
								<li><span>Писахов С.Г. Сказки Сени Малины : сказки / С. Г. Писахов. - Архангельск : ИПП "Правда Севера", 2009. - 272 с. ил.</span></li>
								<li class="current"><span>Холодная война в Арктике / сост. М. Н. Супрун. - Архангельск : [б. и.], 2009. - 379 с.</span></li>		
							</ul>
						</div>
					</div> <!-- /.b-quote_copy -->
					<div class="iblock rel b-myselection_list">
						<a href="#" class="b-openermenu js_openmenu">мои подборки</a>
						<ul class="b-selectionlist">
							<li class="checkwrapper">									
								<label for="cb8" class="black">Любимые авторы</label>
								<input class="checkbox" type="checkbox" name="" id="cb8">								
							</li>
							<li class="checkwrapper">									
								<label for="cb9" class="black">Научно-популярная фантастика</label>
								<input class="checkbox" type="checkbox" name="" id="cb9">								
							</li>
							<li class="checkwrapper">									
								<label for="cb10" class="black">Ракеты и люди</label>
								<input class="checkbox" type="checkbox" name="" id="cb10">								
							</li>	
							<li class="checkwrapper">									
								<label for="cb14" class="lite">Отметить как прочитанное</label>
								<input class="checkbox" type="checkbox" name="" id="cb14">								
							</li>
							<li><a href="#" class="b-selection_add">+  создать подборку</a></li>
						</ul>
					</div> <!-- /.b-myselection_list-->
				</div>
			</div><!-- /.b-quote_item -->
	<?}?>
	
</div><!-- /.b-quote_list -->

<div class="b-paging">
	<div class="b-paging_cnt">
		<?=$arResult['PAGINATION'];?>
		<?/*<a href="" class="b-paging_prev iblock"></a>
		<a href="" class="b-paging_num current iblock">1</a>
		<a href="" class="b-paging_num iblock">2</a>
		<a href="" class="b-paging_num iblock">3</a>
		<a href="" class="b-paging_num iblock">4</a>
		<a href="" class="b-paging_next iblock"></a>*/?>
	</div>
</div><!-- /.b-paging -->