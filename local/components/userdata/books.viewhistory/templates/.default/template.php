<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
 ?>

<?
use \Bitrix\Main\Localization\Loc as Loc;
Loc::loadMessages(__FILE__);
?>
<div class="b-viewhistory">
	<h2><?=Loc::getMessage('BOOK_VIEW_HISTORY_TEMPLATE_HISTORY'); ?></h2>

	<div class="b-boookslider js_multipleslider">
		<div>
			<div class="rel">
				<a href="#" class="b-bookboardphoto"><img src="<?=MARKUP?>pic/pic_12.jpg" alt=""></a>
				<div class="b-bhistiry_date">12 июня</div>
			</div>					
		</div>
		<div>
			<div class="rel">
				<a href="#" class="b-bookboardphoto"><img src="<?=MARKUP?>pic/pic_13.jpg" alt=""></a>
				<div class="b-bhistiry_date">11 июня</div>
			</div>					
		</div>
		<div>
			<div class="rel">
				<a href="#" class="b-bookboardphoto"><img src="<?=MARKUP?>pic/pic_5.jpg" alt=""></a>
				<div class="b-bhistiry_date">15 мая</div>
			</div>					
		</div>
		<div>
			<div class="rel">
				<a href="#" class="b-bookboardphoto"><img src="<?=MARKUP?>pic/pic_6.jpg" alt=""></a>
				<div class="b-bhistiry_date">2 марта</div>
			</div>					
		</div>
		<div>
			<div class="rel">
				<a href="#" class="b-bookboardphoto"><img src="<?=MARKUP?>pic/pic_12.jpg" alt=""></a>
				<div class="b-bhistiry_date">12 июня</div>
			</div>					
		</div>
		<div>
			<div class="rel">
				<a href="#" class="b-bookboardphoto"><img src="<?=MARKUP?>pic/pic_13.jpg" alt=""></a>
				<div class="b-bhistiry_date">11 июня</div>
			</div>					
		</div>
		<div>
			<div class="rel">
				<a href="#" class="b-bookboardphoto"><img src="<?=MARKUP?>pic/pic_5.jpg" alt=""></a>
				<div class="b-bhistiry_date">15 мая</div>
			</div>					
		</div>
		<div>
			<div class="rel">
				<a href="#" class="b-bookboardphoto"><img src="<?=MARKUP?>pic/pic_6.jpg" alt=""></a>
				<div class="b-bhistiry_date">2 марта</div>
			</div>					
		</div>
	</div> <!-- /.boardslider -->
	<a href="#" class="button_mode"><?=Loc::getMessage('BOOK_VIEW_HISTORY_TEMPLATE_GOTO'); ?></a>
</div>