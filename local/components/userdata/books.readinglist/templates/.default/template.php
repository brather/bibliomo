<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
 ?>

<?
use \Bitrix\Main\Localization\Loc as Loc;
Loc::loadMessages(__FILE__);
?>
<h2 class="bold"><?=Loc::getMessage('BOOKS_READING_LIST_TEMPLATE_TITLE'); ?></h2>
<ul class="b-bookboard_list b-readnow">
	<li>
		<a href="#" class="b-bookboardphoto iblock"><img src="<?=MARKUP?>pic/pic_1.jpg" alt=""></a>
		<div class="b-readprogress"><div class="b-loadprogress">
			<div class="b-loadlabel"></div>
			<input type="text" class="knob" data-width="37" data-height="37" data-displayInput="false" data-inputColor="#8b9597" data-min="0"  data-min="100"  data-fgColor="#f06735" data-thickness=".1" value="38">
		</div></div>
	</li>
	<li>
		<a href="#" class="b-bookboardphoto iblock"><img src="<?=MARKUP?>pic/pic_2.jpg" alt=""></a>
		<div class="b-readprogress"><div class="b-loadprogress"><div class="b-loadlabel"></div><input type="text"  data-height="37" class="knob" data-displayInput="false" data-width="37" data-inputColor="#8b9597" data-min="0"  data-min="100"  data-fgColor="#f06735" data-thickness=".1" value="68"></div></div>
	</li>
	<li>
		<a href="#" class="b-bookboardphoto iblock"><img src="<?=MARKUP?>pic/pic_3.jpg" alt=""></a>
		<div class="b-readprogress"><div class="b-loadprogress"><div class="b-loadlabel"></div><input type="text"  data-height="37" class="knob" data-displayInput="false" data-width="37" data-inputColor="#8b9597" data-min="0"  data-min="100"  data-fgColor="#f06735" data-thickness=".1" value="75"></div></div>
	</li>
	<li>
		<a href="#" class="b-bookboardphoto iblock"><img src="<?=MARKUP?>pic/pic_11.jpg" alt=""></a>
		<div class="b-readprogress"><div class="b-loadprogress"><div class="b-loadlabel"></div><input type="text"  data-height="37" class="knob" data-displayInput="false" data-width="37" data-inputColor="#8b9597" data-min="0"  data-min="100"  data-fgColor="#f06735" data-thickness=".1" value="95"></div></div>
	</li>	
</ul> <!-- /.b-bookboard_list-->	