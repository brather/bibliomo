<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
 ?>

<?
use \Bitrix\Main\Localization\Loc as Loc;
Loc::loadMessages(__FILE__);
?>
<div class="b-profile_side">
	<div class="b-profile_photo">
		<img src="<?=MARKUP?>i/user_photo.png" alt="user photo">
	</div>
	<ul class="b-profile_info">
		<li><a href="#"><?=Loc::getMessage('PROFILE_TEMPLATE_CHANGE'); ?></a></li>
		<li><a href="#"><?=Loc::getMessage('PROFILE_TEMPLATE_SETTINGS'); ?></a></li>
		<li><a href="#"><?=Loc::getMessage('PROFILE_TEMPLATE_SETTINGS'); ?></a></li>
	</ul>
	<div class="b-profile_messages"><span>3</span></div>			
</div>