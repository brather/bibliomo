<?php
use Nota\Bookcatalog\AuthorBookTable;
use Nota\Bookcatalog\BookTable;
use Nota\Bookcatalog\SectionTable;

use Nota\Bookcatalog\AuthorIdTable;
use Nota\Bookcatalog\SectionIdTable;

use Nota\UserData\UserBookTable;

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Type\DateTime;

CModule::IncludeModule('nota.userdata');
CModule::IncludeModule('nota.bookcatalog');
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

Loc::loadMessages(__FILE__);

class GetDataLibrary extends CBitrixComponent
{
    public function onPrepareComponentParams($arParams)
    {
        $arParams['CACHE_TIME'] = $arParams['CACHE_TIME'];
        return $arParams;
    }
	
	/*
	 * Получаем список id книг
	 * */
	 
	 public function getBooks()
    {
    	global $USER;
			
        $rsBook = UserBookTable::getList(array(
            'select' => array_keys(UserBookTable::getMap()),
            'filter' => array('USER_ID' => $USER->GetID()),
            'order' => array()
        ));

        while ($arBook = $rsBook->Fetch()) {
        	if($arBook['STATUS'] == 'READ')
			{
				$name = 'READ';
			}
			else 
			{
				$name = 'OLD';
			}
			
	        $this->arResult['BOOK'][$name]['VALUES'][] = $arBook['BOOK_ID'];
			
			
			//$arResult['AUTHOR_ID'][] = $arBook['AUTHOR_ID'];
			//$arResult['EXALEAD_ID'][] = $arBook['EXALEAD_ID'];
        }
		$this->arResult['BOOK']['READ']['COUNT'] = count($this->arResult['BOOK']['READ']['VALUES']);
		$this->arResult['BOOK']['OLD']['COUNT'] = count($this->arResult['BOOK']['OLD']['VALUES']);
		$this->arResult['BOOK']['COUNT'] = count($this->arResult['BOOK']['READ']['VALUES']) + count($this->arResult['BOOK']['OLD']['VALUES']);
		
		//return $arResult;
	}
		/*
	 * Получаем массив секций
	 * */
	public function getTmemeBooks()
    {
		global $USER;
        
        $rsBook = SectionIdTable::getList(array(
            'select' => array_keys(SectionIdTable::getMap()),
            'filter' => array('USER_ID' => $USER->GetID()),
            'order' => array()
        ));

        while ($arBook = $rsBook->Fetch()) 
        {
	        $arSectionId[] = $arBook['SECTION_ID'];
        }
		
        $rsBook = SectionTable::getList(array(
            'select' => array_keys(SectionTable::getMap()),
            'filter' => array('EXALEAD_ID' => $arSectionId),
            'order' => array()
        ));
		
        while ($arBook = $rsBook->Fetch()) 
        {
			$arId = array();
			$rsCount = BookTable::getList(array(
	            'select' => array_keys(BookTable::getMap()),
	            'filter' => array('SECTION_ID' => $arBook['EXALEAD_ID']),
	            'order' => array()
	        ));
			
			$count = 0;
			
	        while ($arCount = $rsCount->Fetch()) 
	        {
	        	$arId[] = $arCount['EXALEAD_ID'];			
		        $count++;
	        }
			
	        $this->arResult['THEME'][$arBook['EXALEAD_ID']]['VALUES'] = array(
				'ID' => $arBook['ID'],
				'EXALEAD_ID' => $arBook['EXALEAD_ID'],
				'NAME' => $arBook['NAME'],	
			);
			 $this->arResult['THEME'][$arBook['EXALEAD_ID']]['COUNT'] = $count;
			 $this->arResult['THEME'][$arBook['EXALEAD_ID']]['ID'] = $arId;	 
			 
        }
	}
	
	/**
	 * Получаем массив авторов
	 */
	 
	public function getAuthorBooks()
    {			
    	global $USER;
			
        $rsAuthor = AuthorIdTable::getList(array(
            'select' => array_keys(AuthorIdTable::getMap()),
            'filter' => array('USER_ID' => $USER->GetID()),
            'order' => array()
        ));

        while ($arAuthor = $rsAuthor->Fetch()) {
	        $arAuthorId[] = $arAuthor['AUTHOR_ID'];
        }
		
        $rsBook = AuthorBookTable::getList(array(
            'select' => array_keys(AuthorBookTable::getMap()),
            'filter' => array('EXALEAD_ID' => $arAuthorId),
            'order' => array()
        ));
		
        while ($arBook = $rsBook->Fetch()) 
        {
        	$arId = array();
			$rsCount = BookTable::getList(array(
	            'select' => array_keys(BookTable::getMap()),
	            'filter' => array('AUTHOR_ID' => $arBook['EXALEAD_ID']),
	            'order' => array()
	        ));
			
			$count = 0;
			
	        while ($arCount = $rsCount->Fetch()) 
	        {
	        	$arId[] = $arCount['EXALEAD_ID'];
		        $count++;
	        }
			
	        $this->arResult['AUTHOR'][$arBook['EXALEAD_ID']]['VALUES'] = array(
				'ID' => $arBook['ID'],
				'EXALEAD_ID' => $arBook['EXALEAD_ID'],
				'NAME' => $arBook['NAME'],
				'SECOND_NAME' => $arBook['SECOND_NAME'],
				'LAST_NAME' => $arBook['LAST_NAME'],	
			);
			 $this->arResult['AUTHOR'][$arBook['EXALEAD_ID']]['COUNT'] = $count;	
			 $this->arResult['AUTHOR'][$arBook['EXALEAD_ID']]['ID'] = $arId;	 
        }
	 }
	
	/*
	 * Формируем даты
	 * */
	public function getDateBooks($arDate)
    {
    	
		if($this -> arResult['BOOK']['OLD']['VALUES'] and $this -> arResult['BOOK']['READ']['VALUES'])
		{
			$arAllBooks = array_merge($this -> arResult['BOOK']['OLD']['VALUES'], $this -> arResult['BOOK']['READ']['VALUES']);
		}
		elseif(!$this -> arResult['BOOK']['READ']['VALUES'])
		{
			$arAllBooks = $this -> arResult['BOOK']['OLD']['VALUES'];
		}
		elseif(!$this -> arResult['BOOK']['OLD']['VALUES'])
		{
			$arAllBooks = $this -> arResult['READ']['OLD']['VALUES'];
		}	
		
		$i = 0;
		foreach($arDate as $arDates)
		{
			$k = 0;		
			$rsBook = BookTable::getList(array(
		            'select' => array_keys(BookTable::getMap()),
		            'filter' => array(
		            '>DATE' => DateTime::createFromUserTime($arDates['FIRST']),
					'<DATE' => DateTime::createFromUserTime($arDates['END']),
					'ID' => $arAllBooks
				),
	            'order' => array()
	        ));
			
			$arId = array();
			while ($arBook = $rsBook->Fetch()) 
        	{
        		$k ++;	
				
				$arId[] = $arBook['EXALEAD_ID'];
				$format = "DD.MM.YYYY";
				$new_format = "YYYY";
				$first = CDatabase::FormatDate($arDates['FIRST'], $format, $new_format);
				$end = CDatabase::FormatDate($arDates['END'], $format, $new_format);
				
        		$this->arResult['DATES'][$i]['DATE'] = array(
					'FIRST' => $first,
					'END' => $end
				);
				
				$this->arResult['DATES'][$i]['COUNT'] = $k;
				$this->arResult['DATES'][$arBook['EXALEAD_ID']]['ID'] = $arId;	
			}
			$i ++;		
		}
	 }
	
	/**
	 * 
	 * Массив дат
	 */
	public function getArrayDates()
    {
		$result = array(
			0 => array(
				'FIRST' => '01.01.1900',
				'END' => '01.01.1950',
			),
			1 => array(
				'FIRST' => '01.01.1950',
				'END' => '01.01.2000',
			),
			2 => array(
				'FIRST' => '01.01.2000',
				'END' => '01.01.2050',
			),
		);
		return $result;
	 }
	
	/**
	 * 
	 *Список id секции, текущего пользователя 
	 */
	 
	public function getSectionId($arId)
    {	
    	global $USER;
        $rsBook = SectionUserTable::getList(array(
            'select' => array_keys(SectionUserTable::getMap()),
           	'filter' => array('USER_ID' => $USER->GetID()),
            'order' => array()
        ));
		
        while ($arBook = $rsBook->Fetch()) 
        {
	        $this->arResult['THEME_ID']['VALUES'][] = $arBook['SECTION_ID'];
			$this->arResult['THEME_ID']['COUNT'] = count($this->arResult['THEME_ID']['VALUES']);
        }
	}

	public function executeComponent()
    {
		
       	if ($this->StartResultCache(false, array())) {
       		
            if (!CModule::includeModule('nota.userdata')) {
                $this->AbortResultCache();
                throw new Exception(Loc::getMessage('QUOTES_MODULE_NOT_INSTALLED'));
            }
			
			$this->getBooks();
			$this->getTmemeBooks();
			$this->getAuthorBooks();
			$arDate = $this->getArrayDates();
			$this->getDateBooks($arDate);
			return $this->arResult;
        }
    }
}

?>