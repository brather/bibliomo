<?
	if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
		die();

	use \Bitrix\Main\Localization\Loc as Loc;
	Loc::loadMessages(__FILE__);
?>

<?if(count($arResult['SECTIONS']) > 0):?>
    <h2 class="b-mainslider_tit"><a href="/special/collections/" class="b-collectlink" title="коллекции">коллекции</a></h2>
    <div class="b-mainslider">
        <?foreach($arResult['SECTIONS'] as $collection):?>
        <div>
            <div class="sliderinner">
                <div class="b-mainslider_photo iblock"><img class="loadingimg" src="<?=$collection['BACKGROUND']?>" alt=""></div>
                <div class="b-mainslider_descr iblock">
                    <h3><?=$collection['NAME']?></h3>
                </div>
            </div>
        </div>
        <?endforeach;?>
    </div> <!-- /.b-mainslider -->
<?endif;?>