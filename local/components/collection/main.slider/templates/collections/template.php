<?
	if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
		die();

	use \Bitrix\Main\Localization\Loc as Loc;
	Loc::loadMessages(__FILE__);

	if(!empty($arResult['BOOKS'])){

	?>
	<div class="b-collectionpage rel">
		<div class="b-boardslider narrow_panel js_slider_single_nodots">
			<?
				foreach($arResult['SECTIONS'] as $arSection)
				{
					if(empty($arResult['BOOKS']['IDS'][$arSection['ID']]))
						continue;
				?>
				<div>
					<div class="js_flexbackground">
						<img src="<?=$arSection['BACKGROUND']?>" alt="" data-bgposition="50% 0" class="js_flex_bgimage" />
						<div class="wrapper bbox">
							<?
								if($arSection['SHOW_NAME']){
								?>
								<h3><?=$arSection['NAME']?></h3>						
								<?
								}
								$coverID = $arResult['BOOKS']['IDS'][$arSection['ID']][0];
								$arItem = $arResult['BOOKS']['ITEMS'][$coverID];
								unset($arResult['BOOKS']['IDS'][$arSection['ID']][0]);
							?>
							<div class="b-bookboardmain iblock">
								<a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="popup_opener ajax_opener coverlay" data-width="955"><img src="<?=$arItem['IMAGE_URL']?>&width=160&height=247" class="b-bookboard_img loadingimg" data-autor="<?=$arItem['authorbook']?>" data-title="<?=$arItem['title']?>" title="<?=$arItem['title']?>" ></a>
								<a href="<?=$arSection['SECTION_PAGE_URL']?>" class="button_mode button_revers"><?=$arSection['CNT']?> <?=GetEnding($arSection['CNT'], Loc::getMessage('COLLECTION_SLIDER_BOOK_5'), Loc::getMessage('COLLECTION_SLIDER_BOOK_1'), Loc::getMessage('COLLECTION_SLIDER_BOOK_2'))?> <?=Loc::getMessage('COLLECTION_SLIDER_IN_COL')?></a>
							</div>

							<div class="b-bookboard_cl iblock">
								<ul class="b-bookboard_list">
									<?
										foreach($arResult['BOOKS']['IDS'][$arSection['ID']] as $book_id)
										{
											$arItem = $arResult['BOOKS']['ITEMS'][$book_id];
											if(empty($arItem))
												continue;
										?>
										<li class="b-result-docitem" id="<?=urlencode($arItem['id'])?>">
											<a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="popup_opener ajax_opener coverlay" data-width="955"><img src="<?=$arItem['IMAGE_URL']?>&width=90&height=130" data-autor="<?=$arItem['authorbook']?>" data-title="<?=$arItem['title']?>" title="<?=$arItem['title']?>"  class="loadingimg"></a>
<?/*?>
											<div class="b-bookhover bbox">
												<?
													if(collectionUser::isAdd())
													{
														if(!empty($arResult['USER_BOOKS'][urlencode($arItem['id'])]))
														{
														?>
														<div class="meta minus">
															<div class="b-hint rel"><?=Loc::getMessage('REMOVE_MY_LIB')?></div>
															<a href="#" data-normitem="Y" data-url="<?=ADD_COLLECTION_URL?>removeBook.php?id=<?=urlencode($arItem['id'])?>" data-collection="<?=ADD_COLLECTION_URL?>list.php?t=books&id=<?=urlencode($arItem['id'])?>" class="b-bookadd fav"></a>
														</div>
														<?
														}
														else
														{
														?>
														<div class="meta">
															<div class="b-hint rel"><?=Loc::getMessage('ADD_MY_LIB')?></div>
															<a href="#" data-collection="<?=ADD_COLLECTION_URL?>list.php?t=books&id=<?=urlencode($arItem['id'])?>" class="b-bookadd"></a>
														</div>
														<?
														}
													}
												?>
												<div class="b-bookhover_tit black"><a class="lite popup_opener ajax_opener coverlay"  data-width="955" href="<?=$arItem['DETAIL_PAGE_URL']?>"><?=trimming_line($arItem['title'], 20)?></a></div>
												<span class="b-autor"><a class="lite popup_opener ajax_opener coverlay"  data-width="955" href="<?=$arItem['DETAIL_PAGE_URL']?>"><?=$arItem['authorbook']?></a></span>
											</div>
											<?*/?>
										</li>
										<?
										}
									?>
								</ul>
							</div>
						</div>
					</div><!-- /.wrapper -->
				</div>
				<?
				}
			?>
		</div> <!-- /.boardslider -->
	</div>
	<?
	}
?>