<?
	if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
		die();

	use \Bitrix\Main\Localization\Loc as Loc;
	Loc::loadMessages(__FILE__);

    include_once($_SERVER['DOCUMENT_ROOT'].'/local/include_areas/books_closed.php');
?>
<?if(!empty($arResult['BOOKS'])){?>
	<div class="b-bookboard_main clearfix rel">
		<div class="wrapper b-bookboard_main_tit"><a href="/collections/" class="b-collectlink"><?=Loc::getMessage('CATALOG_PROMO_TEMPLATE_COLLECTIONS')?></a></div>
		<div class="b-boardslider js_slider_single_nodots narrow_panel">
			<?
				foreach($arResult['SECTIONS'] as $arSection)
				{
					if(empty($arResult['BOOKS']['IDS'][$arSection['ID']]))
						continue;
				?>
				<div>
					<div class="js_flexbackground">
						<img src="<?=$arSection['BACKGROUND']?>" alt="" data-bgposition="50% 0" class="js_flex_bgimage" />
						<div class="wrapperboard bbox">
							<?
								if($arSection['SHOW_NAME']){
								?>
								<h3><?=$arSection['NAME']?></h3>
								<?
								}
								$coverID = $arResult['BOOKS']['IDS'][$arSection['ID']][0];
								$arItem = $arResult['BOOKS']['ITEMS'][$coverID];
								unset($arResult['BOOKS']['IDS'][$arSection['ID']][0]);
								if(!empty($arItem)){
								?>
								<div class="b-bookboardmain iblock">
									<a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="popup_opener ajax_opener coverlay" data-width="955"><img src="<?=$arItem['IMAGE_URL']?>&width=262&height=408" class="b-bookboard_img" data-autor="<?=$arItem['authorbook']?>" data-title="<?=$arItem['title']?>" ></a>
									<!--
									<div class="b-bookhover bbox">
										<?
											if(collectionUser::isAdd())
											{
												if(!empty($arResult['USER_BOOKS'][urlencode($arItem['id'])]))
												{
												?>
												<div class="meta minus">
													<div class="b-hint rel" data-minus="<?=Loc::getMessage('CATALOG_PROMO_TEMPLATE_REMOVE_MY_LIB')?>" data-plus="<?=Loc::getMessage('CATALOG_PROMO_TEMPLATE_ADD_MY_LIB')?>"><?=Loc::getMessage('CATALOG_PROMO_TEMPLATE_REMOVE_MY_LIB')?></div>
													<a href="#" data-normitem="Y" data-url="<?=ADD_COLLECTION_URL?>removeBook.php?id=<?=urlencode($arItem['id'])?>" data-collection="<?=ADD_COLLECTION_URL?>list.php?t=books&id=<?=urlencode($arItem['id'])?>" class="b-bookadd fav"></a>
												</div>
												<?
												}
												else
												{
												?>
												<div class="meta">
													<div class="b-hint rel" data-minus="<?=Loc::getMessage('CATALOG_PROMO_TEMPLATE_REMOVE_MY_LIB')?>" data-plus="<?=Loc::getMessage('CATALOG_PROMO_TEMPLATE_ADD_MY_LIB')?>"><?=Loc::getMessage('CATALOG_PROMO_TEMPLATE_ADD_MY_LIB')?></div>
													<a href="#" data-collection="<?=ADD_COLLECTION_URL?>list.php?t=books&id=<?=urlencode($arItem['id'])?>" class="b-bookadd"></a>
												</div>
												<?
												}
											}
										?>

										<div class="b-bookhover_tit black"><a class="lite popup_opener ajax_opener coverlay"  data-width="955" href="<?=$arItem['DETAIL_PAGE_URL']?>"><?=trimming_line($arItem['title'], 20)?></a></div>
										<?
											if(!empty($arItem['authorbook'])){
											?>
											<span class="b-autor"><a class="lite" href="/search/?f_field[authorbook]=<?=urlencode($arItem['r_authorbook'])?>"><?=$arItem['authorbook']?></a></span>
											<?
											}
										?>
									</div>
									-->
								</div>
								<?
								}
							?>
							<div class="b-bookboard_cl iblock">
								<ul class="b-bookboard_list">
									<?
										foreach($arResult['BOOKS']['IDS'][$arSection['ID']] as $book_id)
										{
											$arItem = $arResult['BOOKS']['ITEMS'][$book_id];
											if(empty($arItem))
												continue;
										?>
										<li class="b-result-docitem" id="<?=urlencode($arItem['id'])?>">
											<a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="popup_opener ajax_opener coverlay" data-width="955"><img src="<?=$arItem['IMAGE_URL']?>&width=90&height=130" data-autor="<?=$arItem['authorbook']?>" data-title="<?=$arItem['title']?>"  ></a>
											<!--<div class="b-bookhover bbox">
												<?
													if(collectionUser::isAdd())
													{
														if(!empty($arResult['USER_BOOKS'][urlencode($arItem['id'])]))
														{
														?>
														<div class="meta minus">
															<div class="b-hint rel" data-minus="<?=Loc::getMessage('CATALOG_PROMO_TEMPLATE_REMOVE_MY_LIB')?>" data-plus="<?=Loc::getMessage('CATALOG_PROMO_TEMPLATE_ADD_MY_LIB')?>"><?=Loc::getMessage('CATALOG_PROMO_TEMPLATE_REMOVE_MY_LIB')?></div>
															<a href="#" data-normitem="Y" data-url="<?=ADD_COLLECTION_URL?>removeBook.php?id=<?=urlencode($arItem['id'])?>" data-collection="<?=ADD_COLLECTION_URL?>list.php?t=books&id=<?=urlencode($arItem['id'])?>" class="b-bookadd fav"></a>
														</div>
														<?
														}
														else
														{
														?>
														<div class="meta">
															<div class="b-hint rel" data-minus="<?=Loc::getMessage('CATALOG_PROMO_TEMPLATE_REMOVE_MY_LIB')?>" data-plus="<?=Loc::getMessage('CATALOG_PROMO_TEMPLATE_ADD_MY_LIB')?>"><?=Loc::getMessage('CATALOG_PROMO_TEMPLATE_ADD_MY_LIB')?></div>
															<a href="#" data-collection="<?=ADD_COLLECTION_URL?>list.php?t=books&id=<?=urlencode($arItem['id'])?>" class="b-bookadd"></a>
														</div>
														<?
														}
													}
												?>
												<div class="b-bookhover_tit black"><a class="lite popup_opener ajax_opener coverlay"  data-width="955" href="<?=$arItem['DETAIL_PAGE_URL']?>"><?=trimming_line($arItem['title'], 20)?></a></div>
												<?
													if(!empty($arItem['authorbook'])){
													?>
													<span class="b-autor"><a class="lite" href="/search/?f_field[authorbook]=<?=urlencode($arItem['r_authorbook'])?>"><?=$arItem['authorbook']?></a></span>
													<?
													}
												?>
											</div>
											-->
										</li>
										<?
										}
									?>
								</ul>
								<a href="<?=$arSection['SECTION_PAGE_URL']?>" class="b-bookboard_more button_mode"><?=Loc::getMessage('CATALOG_PROMO_TEMPLATE_MORE')?></a>
							</div>
							<?

							?>
						</div>
					</div><!-- /.wrapper -->
				</div>
				<?
				}
			?>
		</div> <!-- /.boardslider -->
	</div><!-- /.b-bookboard_main -->	
	<?
	}
?>

<?//echo '<pre>'; print_r($arResult); echo '</pre>'?>