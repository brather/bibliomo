<?php
	if (!CModule::IncludeModule('nota.exalead')) return false;

	use Nota\Exalead\SearchQuery;
	use Nota\Exalead\SearchClient;

	use Bitrix\NotaExt\Iblock\Section;
	use Bitrix\NotaExt\Iblock\Element;

	class MainSlider extends CBitrixComponent
	{
		public function onPrepareComponentParams($arParams)
		{
			$arParams['CACHE_TIME'] = intval($arParams['CACHE_TIME']);
			$arParams['LIBRARY_ID'] = intval($arParams['LIBRARY_ID']);
			$arParams['IBLOCK_ID'] = intval($arParams['IBLOCK_ID']);
			return $arParams;
		}

		public function getSection()
		{
			$arFilter = array('IBLOCK_ID' => $this->arParams['IBLOCK_ID'], '!UF_SLIDER' => false);

			if($this->arParams['LIBRARY_ID'] > 0)
				$arFilter['UF_LIBRARY'] = $this->arParams['LIBRARY_ID'];

			$arSections = Section::getList(
				$arFilter,
				array('DETAIL_PICTURE', 'SECTION_PAGE_URL', 'UF_SHOW_NAME'),
				array('SORT' => 'ASC', 'NAME' => 'ASC'),
				true
			);

			if(!empty($arSections))
			{
				$i = 0;
				foreach($arSections as $arSection)
				{
					if($arSection['DETAIL_PICTURE'] > 0)
						$file = CFile::ResizeImageGet($arSection['DETAIL_PICTURE'], array('width'=>1500, 'height'=>1500), BX_RESIZE_IMAGE_PROPORTIONAL, true);
					else
					{
						$i++;
						if($i > 5) $i = 0;
						$file = array('src' => '/local/images/background/'.$i.'.jpg');
					} 

					$arResult[] = array(
						'ID' 				=> $arSection['ID'],
						'NAME' 				=> $arSection['NAME'],
						'SHOW_NAME' 		=> empty($arSection['UF_SHOW_NAME']) ? false : true,
						'CNT' 				=> $arSection['ELEMENT_CNT'],
						'SECTION_PAGE_URL' 	=> $arSection['SECTION_PAGE_URL'],
						'BACKGROUND' 		=> file_exists($_SERVER['DOCUMENT_ROOT'].$file['src']) ? $file['src'] : false,	
					);
				}
			}
			return $arResult;
		}

		public function getBooks()
		{
			$arResult = array();
			if(empty($this->arResult['SECTIONS']))
				return false;

			foreach($this->arResult['SECTIONS'] as $arSection)
			{
				$arBooks = Element::getList(	
					array('IBLOCK_ID' => $this->arParams['IBLOCK_ID'], 'SECTION_ID' => $arSection['ID'], '!PROPERTY_TYPE_IN_SLIDER' => false, '!PROPERTY_BOOK_ID_VALUE' => false), 
					12, 
					array('PROPERTY_BOOK_ID'),
					array('PROPERTY_TYPE_IN_SLIDER' => 'ASC')
				);

				if(empty($arBooks['ITEMS']))
					continue;

				foreach($arBooks['ITEMS'] as $arItem)
				{
					$arItem['PROPERTY_BOOK_ID_VALUE'] = trim($arItem['PROPERTY_BOOK_ID_VALUE']);
					if(!empty($arItem['PROPERTY_BOOK_ID_VALUE']))
						$arResult['IDS'][$arSection['ID']][] = $arItem['PROPERTY_BOOK_ID_VALUE'];
				}

				if(empty($arResult['IDS']))
					continue;

				$query = new SearchQuery();
				$query->getByArIds($arResult['IDS'][$arSection['ID']]);
				$client = new SearchClient();
				$result = $client->getResult($query);

				if(!empty($result['ITEMS']))
				{	
				
					$arResult['IDS'][$arSection['ID']] = array();
					$i = 0;
					foreach($result['ITEMS'] as $arElement)
					{
						if($i >= 7)
							continue;
							
						$arResult['ITEMS'][$arElement['id']] = $arElement;
						$arResult['IDS'][$arSection['ID']][] = $arElement['id'];
						$i++; 
					}
				}
				else
				{
					unset($arResult['IDS'][$arSection['ID']]);
				}
				unset($result);
			}

			return $arResult;
		}

		public function executeComponent()
		{
			global $CACHE_MANAGER;
			if ($this->StartResultCache(false, array())) {

				$CACHE_MANAGER->RegisterTag('IBLOCK_ID_'.$this->arParams['IBLOCK_ID']);
				#$CACHE_MANAGER->RegisterTag('collection_main_slider');

				$this->arResult['SECTIONS'] = $this->getSection();
				$this->arResult['BOOKS'] = $this->getBooks();

				$this->EndResultCache();
			}

			$this->includeComponentTemplate();
		}
	}

?>