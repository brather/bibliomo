<? 
	if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

	if($arParams["IBLOCK_ID"] < 1) {	
		ShowError("IBLOCK_ID IS NOT DEFINED");
		return false;
	}
	if(empty($arParams["BOOK_ID"])) 
		return false;

	if (!CModule::IncludeModule('nota.exalead')) return false;

	use Nota\Exalead\SearchQuery;
	use Nota\Exalead\SearchClient;

	use Bitrix\NotaExt\Iblock\Section;
	use Bitrix\NotaExt\Iblock\Element;


	$arFilter = array(
		'IBLOCK_ID' => $arParams["IBLOCK_ID"],
		'PROPERTY' => array('BOOK_ID' => $arParams["BOOK_ID"])
	);

	$arSections = Section::getList(
		$arFilter,
		array('DETAIL_PICTURE', 'SECTION_PAGE_URL'),
		array('SORT' => 'ASC', 'NAME' => 'ASC')
	);

	if(empty($arSections))
		return false;

	$arBookIds = array();

	$i = 0;
	foreach($arSections as &$arSection)
	{
		if($arSection['DETAIL_PICTURE'] > 0)
			$file = CFile::ResizeImageGet($arSection['DETAIL_PICTURE'], array('width'=>230, 'height'=>170), BX_RESIZE_IMAGE_PROPORTIONAL, true);
		else
		{
			$i++;
			if($i > 5) $i = 0;
			$file = array('src' => '/local/images/background/'.$i.'.jpg');
		} 

		$arSection['BACKGROUND'] = file_exists($_SERVER['DOCUMENT_ROOT'].$file['src']) ? $file['src'] : false;

		$arBooks = Element::getList(	
			array('IBLOCK_ID' => $arParams['IBLOCK_ID'], 'SECTION_ID' => $arSection['ID'], '!PROPERTY_TYPE_IN_SLIDER' => false, '!PROPERTY_BOOK_ID_VALUE' => false), 
			1, 
			array('PROPERTY_BOOK_ID'),
			array('PROPERTY_TYPE_IN_SLIDER' => 'ASC'),
			array('nPageSize'	=> '')
		);
		
		if(!empty($arBooks['ITEMS']))
		{
			$arBookIds[] = $arBooks['ITEMS'][0]['PROPERTY_BOOK_ID_VALUE'];
			$arSection['BOOK_ID'] = $arBooks['ITEMS'][0]['PROPERTY_BOOK_ID_VALUE']; 
			$arSection['CNT'] = $arBooks['NAV_RESULT']->nSelectedCount; 
		}
	}

	$arResult['SECTIONS'] = $arSections;

	if(empty($arBookIds))
		return false;

	$query = new SearchQuery();
	$query->getByArIds($arBookIds);
	$client = new SearchClient();
	$result = $client->getResult($query);

	if(empty($result['ITEMS']))
		return false;
			
	foreach($result['ITEMS'] as $arBook)
		$arResult['BOOKS_EXALEAD'][$arBook['id']] = $arBook;

	#pre($arResult,1);

	$this->IncludeComponentTemplate();
?>