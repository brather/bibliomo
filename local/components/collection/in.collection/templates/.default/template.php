<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use \Bitrix\Main\Localization\Loc as Loc;
Loc::loadMessages(__FILE__);
?>
<div class="b-addbook_popuptit clearfix">
	<h2><?=Loc::getMessage('IN_COLLECTION_IS_IN_COLLECTION');?></h2>
</div>
<div class="js_threeslide selected_slider">
	<?
		foreach($arResult['SECTIONS'] as $arSection)
		{
			if(empty($arResult['BOOKS_EXALEAD'][$arSection['BOOK_ID']]))
				continue;		
		?>
		<div class="slide">
			<div class="b-favside_img js_flexbackground">
				<img src="<?=$arSection['BACKGROUND']?>"  data-bgposition="50% 0" class="js_flex_bgimage" alt="">
				<img src="<?=$arResult['BOOKS_EXALEAD'][$arSection['BOOK_ID']]['IMAGE_URL']?>&width=131&height=145" class="real loadingimg" alt="">
			</div>
			<h2><?=$arSection['NAME']?></h2>
			<a href="<?=$arSection['SECTION_PAGE_URL']?>"><?=$arSection['CNT']?> <?=GetEnding($arSection['CNT'], Loc::getMessage('IN_COLLECTION_BOOKS_5'), Loc::getMessage('IN_COLLECTION_BOOKS_1'), Loc::getMessage('IN_COLLECTION_BOOKS_2'))?> <?=Loc::getMessage('IN_COLLECTION_IN_COLLECTION');?> </a>
		</div>
		<?
		}
	?>
</div>					