<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

	if(!CModule::IncludeModule('nota.exalead'))
		return false;

	require_once ($_SERVER["DOCUMENT_ROOT"]. '/local/tools/suggest/ReflectionTypeHint.php');
	require_once ($_SERVER["DOCUMENT_ROOT"]. '/local/tools/suggest/Text/LangCorrect.php');
	require_once ($_SERVER["DOCUMENT_ROOT"]. '/local/tools/suggest/UTF8.php');


	use \Bitrix\Main\Localization\Loc as Loc;
	Loc::loadMessages(__FILE__);

	use Nota\Exalead\SearchQuery;
	use Nota\Exalead\SearchClient;

	CPageOption::SetOptionString("main", "nav_page_in_session", "N");

	$intDefaultPagen = 15;
	if($USER->IsAuthorized()){
		if (\Bitrix\Main\Loader::includeModule("intervolga.remoteuserclient"))
		{
			$arUser = \Intervolga\RemoteUserClient\User::getById($USER->GetID());
			if(intval($arUser['UF_SEARCH_PAGE_COUNT']) > 0)
				$intDefaultPagen = $arUser['UF_SEARCH_PAGE_COUNT'];
		}
		else
		{
			$rsPagen = CUser::GetByID($USER->GetID())->Fetch();
			if(intval($rsPagen['UF_SEARCH_PAGE_COUNT']) > 0)
				$intDefaultPagen = $rsPagen['UF_SEARCH_PAGE_COUNT'];
		}
	}

	$arParams['IS_ADD_PLAN_DIGIT'] = PlanDigitalization::isAdd();
	if($arParams['IS_ADD_PLAN_DIGIT'])
		$arParams['BOOK_IN_PLAM'] = PlanDigitalization::getBookInPlan();

	$arParams['IS_SAVE_QUERY'] = saveQuery::isSave();
	$arParams['IS_SHOW_PROTECTED'] = nebUser::isShowProtectedContent();

	$arParams['ITEM_COUNT'] = intval($_REQUEST['pagen']) > 0 ? intval($_REQUEST['pagen']) : $intDefaultPagen;	if($arParams['ITEM_COUNT'] > 60) $arParams['ITEM_COUNT'] = 60;
	$arParams['SHOW_LONG_PAGE'] = true;
	$arParams['LONG_PAGE'] = !empty($_REQUEST['longpage']);
	$arParams['NEXT_ITEM'] = intval($_REQUEST['next']);
	$arParams['MODE'] = empty($_REQUEST['mode']) ? 'list' : trim($_REQUEST['mode']);

	$arResult = array();
	$arResult['REFINEMENT'] = '';

	if(!empty($_REQUEST['date1']))
		$arParams['DATE1'] = urldecode($_REQUEST['date1']);

	if(!empty($_REQUEST['q'])):
		/* проверка на раскладку клавииатуры*/
		if(isset($_REQUEST['notransform']) && $_REQUEST['notransform'] ):
			$_REQUEST['q'] = $q = trim(htmlspecialcharsEx($_REQUEST['q']));
			$_REQUEST["QUERY"] = $q;

		else:
			$corrector = new Text_LangCorrect();
			$_REQUEST['q'] = trim(htmlspecialcharsEx($_REQUEST['q']));

			$q = $corrector->parse($_REQUEST['q'], $corrector::KEYBOARD_LAYOUT);

			$arResult["REQUEST"]['QUERY'] = $q;
			if ($_REQUEST['q'] !== $q):
				$arResult["REQUEST"]['ORIGINAL_QUERY'] = $_REQUEST["q"];
			endif;
		endif;
	else:
		$q = false;
		$arParams['q'] = false;
	endif;


	$arParams['q'] = $arResult["REQUEST"]["~QUERY"] = $q;




	$arParams['~q2'] = $arParams['q'];

	/*
	Искать в найденном
	*/
	$arParams['s_in_results'] = !empty($_REQUEST['s_in_results']);

	$by = trim(htmlspecialcharsEx($_REQUEST['by']));
	$order = trim(htmlspecialcharsEx($_REQUEST['order']));


	if(empty($arParams['q']))
		$arParams['q'] = '#all';

	if(!empty($arParams['q']))
	{
		$arParams['~q'] = $arParams['q'];

		# по какому полю искать
		if(!empty($_REQUEST['f_type']) and $_REQUEST['f_type'] != 'ALL')
			$arParams['q'] = trim(htmlspecialcharsEx($_REQUEST['f_type'])).':'.$arParams['q'];

		if($arParams['s_in_results'] === true) #искать в найденном
			$arParams['q'] = $APPLICATION->get_cookie("SEARCH_QUERY").' AND '.$arParams['q'];
		else
			$APPLICATION->set_cookie("SEARCH_QUERY", $arParams['q']);

		/*
		Обрабатываем логику
		*/

		if(!empty($_REQUEST['text']) and is_array($_REQUEST['text']))
		{
			foreach($_REQUEST['text'] as $k => $v)
			{
				if($_REQUEST['theme'][$k] == 'foraccess')
				{
					$tmp_theme = 'isprotected:'.(int)$_REQUEST['isprotected'][$k];
				}
				else
				{
					$v = trim(htmlspecialcharsEx($v));
					if(empty($v))
						continue;

					$_tmp_theme = trim(htmlspecialcharsEx($_REQUEST['theme'][$k]));
					$tmp_theme = $_tmp_theme.':';
					if($tmp_theme == 'ALL:')
						$tmp_theme = '';
				}

				$logic = trim(htmlspecialcharsEx($_REQUEST['logic'][$k]));

				if($_tmp_theme == 'authorbook' or $_tmp_theme == 'title' or $_tmp_theme == 'publisher' or $_tmp_theme == 'publishplace')
					$v = '('.$v.')';

				$arParams['q']	= $arParams['q'].' '.$logic.' '.$tmp_theme.''.$v;
			}
		}

		$f_publishyear = intval($_REQUEST['f_publishyear']);
		if($f_publishyear > 0)
			$arParams['q']	= $arParams['q'].' AND publishyear:'.$f_publishyear;

		$publishyear_prev = intval($_REQUEST['publishyear_prev']);
		$publishyear_next = intval($_REQUEST['publishyear_next']);
		if(intval($publishyear_prev) > SEARCH_BEGIN_YEAR or (!empty($publishyear_next) and $publishyear_next < date('Y'))){
			$arParams['q']	= $arParams['q'].' AND publishyearint>='.$publishyear_prev;

			if(!empty($publishyear_next) > 0){
				$arParams['q']	= $arParams['q'].' AND publishyearint<='.$publishyear_next;
			}
		}

		# Если пустая поисковая строка и нет дополнительных условий поиска
		if($arParams['q'] == '#all' and empty($_REQUEST['f_field']) and empty($arParams['DATE1']))
		{
			$arParams['q'] = '';
		}
		else
		{


			#Теперь поле is_skbr никак не должно участвовать
			/*if((!empty($_REQUEST['s_tl']) and $_REQUEST['s_tl'] == 'Y') and ((!empty($_REQUEST['s_tp']) and $_REQUEST['s_tp'] == 'Y'))){
			# если отмечены оба чекбокса, то ничего не делаем
			}elseif(!empty($_REQUEST['s_tp']) and $_REQUEST['s_tp'] == 'Y')
			$arParams['q']	= $arParams['q'].' AND is_skbr:0';
			elseif(!empty($_REQUEST['s_tl']) and $_REQUEST['s_tl'] == 'Y')
			$arParams['q']	= $arParams['q'].' AND is_skbr:1';*/
			$sQuery = $arParams['q'];

			$arParams['q'] = 'simple:('.$sQuery.')';

			//пусто == simple:(ЗАПРОС)
			//s_tp == simple:"ЗАПРОС" AND  filesize<100000000 AND filesize>0
			//s_strict == simple:"ЗАПРОС"
			//is_full_search == ЗАПРОС

			if(!empty($_REQUEST['is_full_search']) and $_REQUEST['is_full_search'] == 'Y')
				$arParams['q'] = $sQuery;

			if(!empty($_REQUEST['s_strict']) and $_REQUEST['s_strict'] == 'Y')
				$arParams['q'] = '"'.$sQuery.'"';

			if($_REQUEST['s_tc']!= 'Y')
				$arParams['q']	= $arParams['q'].' AND filesize>0';

			if(!empty($arParams['DATE1'])) # ставим условие как в new.list
				$arParams['q'] = 'dateindex>='.$arParams['DATE1'].' AND filesize>0';

			if(strpos($arParams['q'], '#all OR ') !== false)
				$arParams['q'] = str_replace('#all OR ', '', $arParams['q']);

			$query = new SearchQuery($arParams['q']);
			$query->setPageLimit($arParams['ITEM_COUNT']);
			$query->setNavParams();

			if(!empty($_REQUEST['slparam']) and $_REQUEST['slparam'] == 'sl_nofuzzy')
				$query->setParam('sl', 'sl_nofuzzy');

			if((int)$arParams['ID_LIBRARY'] > 0 && !$arParams['ID_COLLECTION'])
			{
				$query->setParam('q', $query->getParameter('q').' AND idlibrary:'.$arParams['ID_LIBRARY']);
			}

			/*
			Уточнение
			*/
			if(!empty($_REQUEST['f_field']))
			{
				$f_field = $_REQUEST['f_field'];
				if(!is_array($f_field))
					$f_field = array($f_field);

				foreach ($f_field as $k => $v)
				{
					$v = urldecode($v);
					$v = trim($v);
					$query->setParam('r', '+'.$v, true);
				}
			}

			if(empty($_REQUEST['isreferats']))
				$query->setParam('q', $query->getParameter('q').' AND collection: -autoref AND collection: -disser AND collection: -Авторефераты');

			/*
			Фильтрация по типу контента
			*/
			if(!empty($_REQUEST['f_ft']))
			{
				$client_ft = new SearchClient();
				$result_ft = $client_ft->getResult($query);
				unset($result_ft['ITEMS'], $result_ft['GROUPS_KEY_VAL']);
				foreach($result_ft['GROUPS'] as $k => $v)
				{
					if($k != 'TYPEBOOK')
						unset($result_ft['GROUPS'][$k]);
				}
				$query->setParam('r', '+'.trim(urldecode($_REQUEST['f_ft'])), true);
			}


			if(!empty($by) and !empty($order)){
				$query->setParam('s', $order.'('.$by.')');
			}

			$arResult['EXALEAD_PARAMS'] = $query->getParameters();
			if(!empty($_REQUEST['debug']))
				pre($arResult['EXALEAD_PARAMS'],1);

			$client = new SearchClient();
			$result = $client->getResult($query);
			#pre($result,1); exit();
			#pre($query->getParameters(),1);

			#$result['~COUNT'] = $result['COUNT'];
			#$result['COUNT'] = $result['NHITS'];

			if(!empty($result_ft['GROUPS']['TYPEBOOK']))
			{
				$result['GROUPS']['TYPEBOOK'] = $result_ft['GROUPS']['TYPEBOOK'];
				$result['COUNT'] = $result_ft['COUNT'];
			}

			if(intval($result['COUNT']) > 0)
			{

				$client->getDBResult($result);
				if(!$arParams['LONG_PAGE'])
					$arResult['STR_NAV'] = $client->strNav;

				$arResult += $result;
				if($arParams['LONG_PAGE'])
				{
					$nextPage = $client->arNavParam['NavPageNomer'] + 1;
					if($nextPage <= $client->arNavParam['NavPageCount'])
					{
						$arResult['NEXT_URL_PARAMS'] = parse_url($APPLICATION->GetCurPageParam('PAGEN_'.$client->arNavParam['NavNum'].'='.$nextPage,array('PAGEN_'.$client->arNavParam['NavNum'], 'dop_filter')), PHP_URL_QUERY);

					}
				}

			}
			else
			{
				$query = new SearchQuery();
				$query->getSpellcheck($arParams['~q']);

				$client = new SearchClient();
				$result = $client->getResult($query);

				$arResult['REFINEMENT'] = $result['REFINEMENT'];
			}
		}
	}

	/* Вы искали на портале */
	#pre($arResult['GROUPS_KEY_VAL'],1);
	#pre($arResult['GROUPS'],1);
	#pre($_REQUEST,1);

	$arSearchResultParams = array();

	if(!empty($_REQUEST['theme']) and is_array($_REQUEST['theme']))
	{
		foreach($_REQUEST['theme'] as $k => $v)
		{
			$arTParams = array();
			$arTParams['logic'] = $_REQUEST['logic'];
			$arTParams['theme'] = $_REQUEST['theme'];
			$arTParams['text'] = $_REQUEST['text'];
			$arTParams['isprotected'] = $_REQUEST['isprotected'];

			unset($arTParams['logic'][$k]);
			unset($arTParams['theme'][$k]);
			unset($arTParams['text'][$k]);
			unset($arTParams['isprotected'][$k]);

			$logic = htmlspecialcharsEx($_REQUEST['logic'][$k]);

			if($_REQUEST['theme'][$k] == 'foraccess')
			{
				$arSearchResultParams[] = array(
					'text' => Loc::getMessage('SEARCH_PARAM_'.$v, array('#VALUE#' => Loc::getMessage('SEARCH_PARAM_ACCESS_'.(int)$_REQUEST['isprotected'][$k]))),
					'logic' => Loc::getMessage('SEARCH_PARAM_LOGIC_'.$logic),
					'url' =>  $APPLICATION->GetCurPageParam(http_build_query($arTParams), array('logic', 'theme', 'text', 'isprotected'))
				);
			}
			else
			{
				if($v != 'foraccess' and empty($_REQUEST['text'][$k]))
					continue;

				$val = htmlspecialcharsEx($_REQUEST['text'][$k]);

				$arSearchResultParams[] = array(
					'text' => Loc::getMessage('SEARCH_PARAM_'.$v, array('#VALUE#' => $val)),
					'logic' => Loc::getMessage('SEARCH_PARAM_LOGIC_'.$logic),
					'url' =>  $APPLICATION->GetCurPageParam(http_build_query($arTParams), array('logic', 'theme', 'text', 'isprotected'))
				);
			}
		}

	}

	if(intval($publishyear_prev) > SEARCH_BEGIN_YEAR or (!empty($publishyear_next) and $publishyear_next < date('Y')))
	{
		$arSearchResultParams[] = array(
			'text' => Loc::getMessage('SEARCH_PARAM_PUBLISHYEAR', array('#VALUE_1#' => $publishyear_prev, '#VALUE_2#' => $publishyear_next)),
			'logic' => Loc::getMessage('SEARCH_PARAM_LOGIC_AND'),
			'url' =>  $APPLICATION->GetCurPageParam('', array('publishyear_prev', 'publishyear_next'))
		);
	}

	if(!empty($_REQUEST['f_field']))
	{
		foreach($_REQUEST['f_field'] as $k => $v)
		{
			$v = urldecode($v);
			if(isset($arResult['GROUPS_KEY_VAL'][$v])){
				$arFFParams = $_REQUEST['f_field'];
				unset($arFFParams[$k]);
				$k = strtoupper($k);
				$arSearchResultParams[] = array(
					'text' => Loc::getMessage('SEARCH_PARAM_'.$k, array('#VALUE#' => $arResult['GROUPS'][$k][$arResult['GROUPS_KEY_VAL'][$v]]['title'])),
					'logic' => Loc::getMessage('SEARCH_PARAM_LOGIC_AND'),
					'url' =>  $APPLICATION->GetCurPageParam(http_build_query(array('f_field' => $arFFParams)), array("f_field"))
				);
			}
		}
	}

	$arResult['RESULT_PARAMS'] = $arSearchResultParams;
	#pre($arSearchResultParams,1);
	/* Вы искали на портале End*/

	#pre($arResult['NEXT_URL'],1);
	#pre($arResult,1);

	if(!empty($_REQUEST['dop_filter']) and $_REQUEST['dop_filter'] == 'Y' and $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')
		$APPLICATION->RestartBuffer();


	if($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' and empty($_REQUEST['dop_filter']))
		$arParams['ajax'] = true;




	$this->IncludeComponentTemplate();

	if($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')
		exit();


?>