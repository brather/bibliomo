<?
	if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
		die();

	use \Bitrix\Main\Localization\Loc as Loc;
	Loc::loadMessages(__FILE__);

?>
<form action="<?=!empty($arParams['ACTION_URL'])? $arParams['ACTION_URL'] : '/search/'?>" class="searchform">
	<?
		if(!empty($_REQUEST['pagen'])){?><input type="hidden" name="pagen" value="<?=(int)$_REQUEST['pagen']?>"/><?}
		if(!empty($_REQUEST['longpage'])){?><input type="hidden" name="longpage" value="y"/><?}
		if(!empty($_REQUEST['debug'])){?><input type="hidden" name="debug" value="1"/><?}?>

		<?foreach($arParams['REQUEST_PARAMS'] as $k => $v):?>
			<input type="hidden" name="<?=$k?>" value="<?=$v?>"/>
		<?endforeach;?>


		<?if($arParams['PAGE'] == 'MAIN')
		{
		?>
		<div class="b-search wrapper">
			<div class="b-search_field">
				<div class="clearfix">
					<input type="text" name="q" class="b-search_fieldtb b-text" id="asearch" placeholder="<?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_EXAMPLES'); ?>" autocomplete="off" data-src="<?=$this->__folder.'/autocomplete.php?'.bitrix_sessid_get()?>">
					<a href="#" title="Очистить поиск" class="clean-link js_cleaninp">Очистить поиск</a>
					<input type="submit" class="b-search_bth bbox" value="<?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_FIND'); ?>">
				</div>

				<div class="b_search_set clearfix">
					<a href="#" class="b-search_exlink js_extraform" style="text-decoration: none;"><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_SEARCH'); ?></a>
					<div class="checkwrapper">
                        <input class="checkbox js_btnsub" type="checkbox" name="s_tc" value="Y" id="cb1"><label for="cb1" class="black"><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_PORTAL'); ?></label>
					</div>
					<div class="checkwrapper">
                        <input class="checkbox js_btnsub" type="checkbox" name="is_full_search" checked="checked" value="Y" id="cb38"><label for="cb38" class="black"><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_FOR_FULL_TEXT'); ?></label>
					</div>
				</div> <!-- /.b_search_set -->
			</div>
		</div> <!-- /.b-search -->
		<?
		}
		else
		{
		?>
		<div class="clearfix wrapper">
			<?
				if($arParams['POPUP_VIEW'] != 'Y')
				{
				?>
				<div class="leftblock iblock">					
					<a href="/" class="b_logo iblock">
						<img src="<?=MARKUP?>i/logo_1.png" alt="<?=Loc::getMessage('INCLUDE_LOGO_NEB')?>">
					</a>		
				</div>
				<?
				}
			?>
			<div class="rightblock iblock">
				<div class="b-search_field">
					<div class="clearfix">
						<input type="text" name="q" class="b-search_fieldtb b-text" id="asearch" value="<?=htmlspecialcharsEx($_REQUEST['q'])?>" placeholder="<?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_EXAMPLES'); ?>" autocomplete="off" data-src="<?=$this->__folder.'/autocomplete.php?'.bitrix_sessid_get()?>">
						<input type="submit" class="b-search_bth bbox bt_typelt" value="Искать">
						<a href="#" class="b-search_exlink js_extraform" style="text-decoration: none;"><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_SEARCH'); ?></a>
					</div>
				</div>
			</div>
		</div><!-- /.b-portalinfo -->
		<div class="b-search wrapper">
		</div> <!-- /.b-search -->

		<!--<input type="hidden" name="s_tc" id="s_tc" value="<?=$_REQUEST['s_tc'] == 'Y'? 'Y':''?>">

		<input type="hidden" name="is_full_search" id="is_full_search" value="<?=($_REQUEST['q']== '' || $_REQUEST['is_full_search'] == 'Y')? 'Y':''?>">-->

		<input type="hidden" name="s_tl" id="s_tl" value="<?=$_REQUEST['s_tl'] == 'Y'? 'Y':''?>">
		<?
		}

		$exOpen = false;
		if(!empty($_REQUEST['theme']) and is_array($_REQUEST['theme']))
		{
			foreach($_REQUEST['theme'] as $v)
			{
				if($v == 'foraccess' and !$exOpen)
					$exOpen = true;
			}
			if(!$exOpen)
			{
				foreach($_REQUEST['text'] as $v)
				{
					if(!empty($v) and !$exOpen) 
						$exOpen = true;
				}
			}	
		}
	?>
	<div class="b-search_ex<?=$exOpen? ' open' : ''?>">
		<div class="wrapper rel bbox">
					<div class="b_search_set clearfix">
						<?
							if($arParams['POPUP_VIEW'] != 'Y')
							{
							?>
							<div class="checkwrapper b-search_lib">
								<input class="checkbox" type="checkbox" name="s_in_results"<?=!empty($_REQUEST['s_in_results'])? ' checked="checked"': '';?> value="Y" id="cb2"><label for="cb2" class="black"><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_FIND_IN'); ?></label>
							</div>
							<?
							}
						?>
						<?
							//if($arParams['PAGE'] != '/search/' and $arParams['POPUP_VIEW'] != 'Y')
							//{
							?>
							<div class="checkwrapper b-search_lib">
								<input class="checkbox js_btnsub_rm" type="checkbox" name="s_tc" value="Y" id="cb1" <?= $_REQUEST['s_tc']=='Y'? 'checked="checked"' : ''?>><label for="cb1" class="black fz_mid"><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_PORTAL'); ?></label>
							</div>
							
							<div class="checkwrapper b-search_lib">
								<?//var_dump($_REQUEST)?>
								<input class="checkbox js_btnsub_rm" type="checkbox" name="is_full_search" <?=$_REQUEST['is_full_search']=='Y' ? 'checked="checked"' : ''?><?=!isset($_REQUEST['q']) ? 'checked="checked"' : ''?> value="Y" id="cb38"><label for="cb38" class="black"><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_FOR_FULL_TEXT'); ?></label>
							</div>
							
							<div class="checkwrapper b-search_lib">
								<span class="nowrap"><input class="checkbox js_btnsub" type="checkbox" name="s_strict"<?=!empty($_REQUEST['s_strict'])? ' checked="checked"': '';?> value="Y" id="cb4"><label for="cb4" class="black"><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_STRICT_SEARCH'); ?></label></span>
							</div>
							<?
							//}
						?>

					</div> <!-- /.b_search_set -->
			<div class="b_search_row js_search_row hidden" >
				<select name="logic" disabled="disabled" id="" class="ddl_logic">
					<option value="OR"><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_OR'); ?></option>
					<option selected="selected" value="AND"><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_AND'); ?></option>
					<option value="NOT"><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_NO'); ?></option>
				</select>
				<select name="theme" disabled="disabled" id="" class="ddl_theme theme">
					<option value="ALL"><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_ALL'); ?></option>
					<option value="authorbook" data-asrc="<?=$this->__folder.'/authors.php?'.bitrix_sessid_get()?>"><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_AUTHORBOOK'); ?></option>
					<!--<option value="foraccess"><?/*=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_FORACCESS'); */?></option>-->
					<option value="title" data-asrc="<?=$this->__folder.'/title.php?'.bitrix_sessid_get()?>"><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_TITLE'); ?></option>
					<option value="publisher"><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_PUBLISHER'); ?></option>
					<option value="publishplace"><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_PUBLISHPLACE'); ?></option>
				</select>
				<input type="text" disabled="disabled" name="text" class="b-text b_search_txt" id="">
				<select name="isprotected" disabled="disabled" id="" class="b-access hidden">
					<option value="0"><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_OPEN_ACCESS'); ?></option>
					<option value="1"><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_CLOSE_ACCESS'); ?></option>
				</select>
				<a href="#" class="b-searchdelrow b-searchaddrow"><span class="b-searchaddrow_minus">-</span><span class="b-searchaddrow_lb js_delsearchrow"><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_DEL'); ?></span></a>
			</div>
			<?
				if(!empty($_REQUEST['theme']) and is_array($_REQUEST['theme']))
				{
					$i = 0;
					foreach($_REQUEST['theme'] as $k => $v)
					{
						if(count($_REQUEST['theme']) > 1)
						{
							if($v != 'foraccess' and empty($_REQUEST['text'][$k]))
								continue;
						}

					?>
					<div class="b_search_row visiblerow">
						<select name="logic[<?=$i?>]" id="" class="js_select ddl_logic ">
							<option value="OR" <?=$_REQUEST['logic'][$k] == 'OR' ? 'selected="selected"' : ''?>><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_OR'); ?></option>
							<option value="AND" <?=$_REQUEST['logic'][$k] == 'AND' ? 'selected="selected"' : ''?>><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_AND'); ?></option>
							<option value="NOT" <?=$_REQUEST['logic'][$k] == 'NOT' ? 'selected="selected"' : ''?>><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_NO'); ?></option>
						</select>
						<select name="theme[<?=$i?>]" id="" class="js_select ddl_theme theme">
							<option value="ALL" <?=$v == 'ALL' ? 'selected="selected"' : ''?>><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_ALL'); ?></option>
							<option value="authorbook" data-asrc="<?=$this->__folder.'/authors.php?'.bitrix_sessid_get()?>" <?=$v == 'authorbook' ? 'selected="selected"' : ''?>><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_AUTHORBOOK'); ?></option>
							<!--<option value="foraccess" <?/*=$v == 'foraccess' ? 'selected="selected"' : ''*/?>><?/*=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_FORACCESS'); */?></option>-->
							<option value="title" data-asrc="<?=$this->__folder.'/title.php?'.bitrix_sessid_get()?>" <?=$v == 'title' ? 'selected="selected"' : ''?>><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_TITLE'); ?></option>
							<option value="publisher" <?=$v == 'publisher' ? 'selected="selected"' : ''?>><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_PUBLISHER'); ?></option>
							<option value="publishplace" <?=$v == 'publishplace' ? 'selected="selected"' : ''?>><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_PUBLISHPLACE'); ?></option>
						</select>
						<input type="text" name="text[<?=$i?>]" value="<?=htmlspecialcharsbx($_REQUEST['text'][$k])?>" class="b-text b_search_txt<?=$v == 'foraccess' ? ' hidden':''?>" id="">
						<select name="isprotected[<?=$i?>]" id="" class="js_select b-access<?=$v != 'foraccess' ? ' hidden':''?>">
							<option value="0" <?=$_REQUEST['isprotected'][$k] == '0' ? 'selected="selected"' : ''?>><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_OPEN_ACCESS'); ?></option>
							<option value="1" <?=$_REQUEST['isprotected'][$k] == '1' ? 'selected="selected"' : ''?>><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_CLOSE_ACCESS'); ?></option>
						</select>	
						<?
							if(($i + 1) == count($_REQUEST['theme'])){
							?>			
							<a href="#" class="b-searchaddrow"><span class="b-searchaddrow_plus">+</span><span class="b-searchaddrow_lb js_addsearchrow"><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_ADD'); ?></span></a>
							<?
							}else
							{
							?>
							<a href="#" class="b-searchdelrow b-searchaddrow"><span class="b-searchaddrow_minus">-</span><span class="b-searchaddrow_lb js_delsearchrow"><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_DEL'); ?></span></a>
							<?
							}
						?>
					</div>	
					<?
						$i++;
					}
				}
				else
				{
				?>
				<div class="b_search_row visiblerow">
					<select name="logic[0]" id="" class="js_select ddl_logic ">
						<option value="OR"><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_OR'); ?></option>
						<option value="AND" selected="selected"><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_AND'); ?></option>
						<option value="NOT"><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_NO'); ?></option>
					</select>
					<select name="theme[0]" id="" class="js_select ddl_theme theme">
						<option value="ALL"><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_ALL'); ?></option>
						<option value="authorbook" data-asrc="<?=$this->__folder.'/authors.php?'.bitrix_sessid_get()?>"><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_AUTHORBOOK'); ?></option>
						<!--<option value="foraccess"><?/*=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_FORACCESS'); */?></option>-->
						<option value="title" data-asrc="<?=$this->__folder.'/title.php?'.bitrix_sessid_get()?>"><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_TITLE'); ?></option>
						<option value="publisher"><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_PUBLISHER'); ?></option>
						<option value="publishplace"><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_PUBLISHPLACE'); ?></option>
					</select>
					<input type="text" name="text[0]"  class="b-text b_search_txt" id="">
					<select name="isprotected[0]" id="" class="js_select b-access hidden">
						<option value="0"><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_OPEN_ACCESS'); ?></option>
						<option value="1"><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_CLOSE_ACCESS'); ?></option>
					</select>	
					<a href="#" class="b-searchaddrow"><span class="b-searchaddrow_plus">+</span><span class="b-searchaddrow_lb js_addsearchrow"><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_ADD'); ?></span></a>

				</div>		
				<?
				}
			?>
			<div class="b_search_date">
				<div class="b_search_datelb iblock"><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_DATE_PUBLIC'); ?></div>
                <input class="b-text" type="text" id="js_searchdate_prev" name="publishyear_prev" value="<?=intval($_REQUEST['publishyear_prev']) > 0 ? (int)$_REQUEST['publishyear_prev']:SEARCH_BEGIN_YEAR;?>"  />
				<div class="b_searchslider iblock js_searchslider"></div>
				<input class="b-text"  type="text" id="js_searchdate_next" name="publishyear_next" value="<?=intval($_REQUEST['publishyear_next']) > 0 ? (int)$_REQUEST['publishyear_next']:date('Y');?>" />
				<div class="b-search_sliderdate"></div>
			</div>

		</div>
	</div><!-- /.b-search_ex -->
</form>		
