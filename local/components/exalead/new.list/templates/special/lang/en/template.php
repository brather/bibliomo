<?
$MESS['NEW_LIST_TEMPLATE_NEW'] = 'New';
$MESS['NEW_LIST_TEMPLATE_ADDED'] = 'added';
$MESS['NEW_LIST_TEMPLATE_BOOK'] = 'books';
$MESS['NEW_LIST_TEMPLATE_NEXT'] = 'next 3';

$MESS['ADD_MY_LIB'] = '<span>Add</span> to my Library';
$MESS['REMOVE_MY_LIB'] = '<span>Remove</span> from my Library';

$MESS['NEW_LIST_TEMPLATE_AUTHOR'] = 'Author';
$MESS['NEW_LIST_TEMPLATE_SOURCE'] = 'Source';
