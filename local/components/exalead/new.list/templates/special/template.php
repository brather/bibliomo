<?
	if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
		die();
	use \Bitrix\Main\Localization\Loc as Loc;
	Loc::loadMessages(__FILE__);
	if(empty($arResult['ITEMS']))
		return false;
?>

<div class="b-bookinfo_new iblock">
    <div class="b-bookinfo_tit">
        <!--<h2><a href="/special/new/" title="новые поступления">новые поступления</a></h2> <span class="b-searchpage_lnk">Май: добавлено <strong>1405 книги</strong> </span>-->
        <h2>новые поступления</h2> <span class="b-searchpage_lnk"><?=$arResult['MONTH']?>: добавлено <strong><?=$arResult['COUNT']?></strong> </span>
    </div>
    <ul class="b-bookboard_list">
        <?foreach($arResult['ITEMS'] as $book):?>
            <li>
                <div class="b_popular_descr">
                    <h4><?=$book['TITLE']?></h4>
                    <span class="b-autor">Автор: <?=$book['AUTHOR']?></span>
                    <div class="b-result_sorce_info"><em>Источник:</em> <?=$book['LIBRARY']?></div>

                </div>
            </li>
        <?endforeach;?>
    </ul>
</div>
