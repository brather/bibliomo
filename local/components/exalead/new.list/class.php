<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}

use Bitrix\Main\Localization\Loc;
CModule::IncludeModule('nota.exalead');
use Nota\Exalead\SearchQuery;
use Nota\Exalead\SearchClient;
Loc::loadMessages(__FILE__);

class ExaleadNewList extends CBitrixComponent
{
	public function onPrepareComponentParams($arParams)
	{
		$this->arParams['DATE'] = intval($arParams['DATE']);
		$this->arParams['COUNT'] = intval($arParams['COUNT']);
		$this->arParams['PAGE_URL'] = trim($arParams['PAGE_URL']);
		$this->arParams['NEXT_BOOK'] = intval($arParams['NEXT_BOOK']);
		$this->arParams['LIMIT'] = intval($arParams['LIMIT']);
		return $arParams;
	}
	
	public function getResult()

	{
		if($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')

		{
			 $this->arResult['AJAX'] = true;
		}
				
		
		$time_stamp = AddToTimeStamp(array('DD' => - $this->arParams['DATE']));
		$date = date('Y/m/d', $time_stamp);
		
		$this->arResult['DATE_1'] = $date;
		$this->arResult['MONTH'] = FormatDate("f", $time_stamp);
		
		$query = new SearchQuery('dateindex>='.$date.' AND filesize<100000000 AND filesize>0 AND -idlibrary:847');
		$query->setParam('s', 'desc(document_dateindexsort)');
		$query->setPageLimit($this->arParams['COUNT']);
		$query->setNextPagePosition($this->arParams['NEXT_BOOK']);
		
		$this->arParams['NEXT_BOOK'] = $this->arParams['NEXT_BOOK'] + $this->arParams['COUNT'];
		
		$client = new SearchClient();
		$result = $client->getResult($query);

		$this->arResult['COUNT'] = $result['COUNT'];
		if(isset($this->arParams['LIMIT']) && ($this->arParams['LIMIT'] > 0) && ($this->arParams['LIMIT'] < $result['COUNT']))
			$this->arResult['COUNT'] = $this->arParams['LIMIT'];

		if(count($result['ITEMS']))
		{	
			foreach($result['ITEMS'] as $arElement)
			{
				$this->arResult['ITEMS'][] = array(
					'ID' => $arElement['id'],
					'URL' => $arElement['URL'],
					'SOURCE' => $arElement['source'],
					'AUTHOR' => $arElement['authorbook'],
					'AUTHOR_LINK' => '/search/?f_field[authorbook]='.urlencode($arElement['r_authorbook']),
					'TITLE' => TruncateText($arElement['title'], 45),
					'LINK' => $arElement['DETAIL_PAGE_URL'],
					'IMAGE_URL' => $arElement['IMAGE_URL'],
					'VIEWER_URL' => $arElement['VIEWER_URL'],
					// 'LIBRARY' => $arElement['library'],
					'LIBRARY' => $_SESSION["LIBRARY_BOOK"][$arElement["id"]]["NAME"],
					// 'LIB_LINK' => '/search/?f_field[library]=' . $arElement['r_library'],
					'LIB_LINK' => $_SESSION["LIBRARY_BOOK"][$arElement["id"]]["LINK"],
				);	
			}
		}
	}

	public function executeComponent()
	{
		$this->getResult();
		$this->includeComponentTemplate();
	}
}

?>
