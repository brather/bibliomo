<?	if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
	use \Bitrix\Main\Localization\Loc as Loc;
	Loc::loadMessages(__FILE__);
	if(empty($arResult['ITEMS']))
		return false;

?>
<div class="b-bookinfo_popular iblock">
    <!--<div class="b-bookinfo_tit"><h2><a href="/special/popular/" title="популярные издания">популярные издания</a></h2></div>-->
    <div class="b-bookinfo_tit"><h2>популярные издания</h2></div>
    <ul class="b_bookpopular">
        <?foreach($arResult['ITEMS'] as $book):?>
            <li>
                <div class="b_popular_descr">
                    <h4><?=$book['TITLE']?></h4>
                    <?if($book['AUTHOR']):?><span class="b-autor">Автор: <?=$book['AUTHOR']?></span><?endif;?>
                    <?if($book['LIBRARY']):?><div class="b-result_sorce_info"><em>Источник:</em> <?=$book['LIBRARY']?></div><?endif;?>
                </div>
            </li>
        <?endforeach;?>
    </ul> <!-- /.b_bookpopular -->
</div> <!-- /.b-bookinfo_popular -->

