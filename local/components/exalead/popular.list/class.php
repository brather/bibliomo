<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}

if (!CModule::IncludeModule('nota.exalead')) return false;
if (!CModule::IncludeModule('iblock')) return false;
use Bitrix\Main\Localization\Loc;

use Nota\Exalead\SearchQuery;
use Nota\Exalead\SearchClient;
Loc::loadMessages(__FILE__);

class ExaleadPopularList extends CBitrixComponent
{
	public function onPrepareComponentParams($arParams)
	{
		$this->arParams['COUNT'] = intval($arParams['COUNT']);
		$this->arParams['PAGE_URL'] = trim($arParams['PAGE_URL']);
		return $arParams;
	}
	
	public function getResult()
	{	
		$arFilter = array(
			'IBLOCK_CODE' => 'popular.books',
			'!PROPERTY_EXALEAD_ID' => false
		);
		
		$arSelect = array(
			'ID',
			'IBLOCK_ID',
			'PROPERTY_EXALEAD_ID'
		);
		
		$arOrder = array(
			'SORT' => 'DESC'
		);
		
		$rsItems = CIBlockElement::GetList($arOrder, $arFilter, false, array('nTopCount' => $this->arParams['COUNT']), $arSelect);
		
		while($arItems = $rsItems -> Fetch())
		{
			$arCode[] = $arItems['PROPERTY_EXALEAD_ID_VALUE']; 
		}
		
		$this->arResult['PAGE_SIZE'] = $rsItems->SIZEN;
		
		$query = new SearchQuery();
		$query->getByArIds($arCode);
		$query->setPageLimit($this->arParams['COUNT']);
	
		$client = new SearchClient();
		$result = $client->getResult($query);
		
		if(count($result['ITEMS']))
		{	
			foreach($result['ITEMS'] as $arElement)
			{	
				$this->arResult['ITEMS'][] = array(
					'ID' => $arElement['id'],
					'TITLE' => TruncateText($arElement['title'], 45),
					'URL' => $arElement['URL'],
					'SOURCE' => $arElement['source'],
					'AUTHOR' => $arElement['authorbook'],
					// 'LIBRARY' => $arElement['library'],
					'LIBRARY' => $_SESSION["LIBRARY_BOOK"][$arElement["id"]]["NAME"],
					'LINK' => str_replace('#BOOK_ID#', $arElement['id'], $this->arParams['PAGE_URL']),
					// 'LIB_LINK' => '/search/?f_field[library]=' . $arElement['r_library'],
					'LIB_LINK' => $_SESSION["LIBRARY_BOOK"][$arElement["id"]]["LINK"],
					'AUTH_LINK' => '/search/?f_field[authorbook]=' . $arElement['r_authorbook'],
				);	
			}
		}
	}

	public function executeComponent()
	{
		
		if ($this->StartResultCache(false, array())) {
			
			if (!CModule::includeModule('nota.exalead')) {
				
				$this->AbortResultCache();
				throw new Exception(Loc::getMessage('BOOK_CATALOG_CATALOG_PROMO_CLASS_ERROR'));
			}
			$this->getResult();
		$this->includeComponentTemplate();
		}
	}
}

?>