<?use \Bitrix\Main\Localization\Loc as Loc;
Loc::loadMessages(__FILE__);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
//var_dump($GLOBALS['USER']->IsAuthorized());?>
<!DOCTYPE html>
<html dir="ltr" mozdisallowselectionprint moznomarginboxes>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width">
		<meta name="viewport" content="width=980">		
		<meta name="google" content="notranslate">
		<title><?=$arResult['BOOK']['title']?></title>		
		<link rel="stylesheet" href="/local/components/exalead/search.page.detail2/templates/.default/styles.css" type="text/css" media="screen">
		<script type="text/javascript" src="/local/templates/.default/markup/js/libs/jquery.min.js"></script>
		<script type="text/javascript" src="/bitrix/js/jqRotate.js"></script>
	</head>

	<body>
		<div class="page-book">
			<div class="sidebar">
				<div class="sidebar-header">
					<div class="sidebar-tools">
						<span class="document-options" title="Свойства документа"></span>
						<span class="hand" title="Инструмент перемещения"></span>
						<a href="/local/tools/exalead/getFiles.php?book_id=<?=$arResult['BOOK']['URL']?>"><span class="save" title="Сохранить"></span></a>
						<span class="search" title="Поиск по тексту"></span>
					</div>
					<div class='document-options-view'>
						<?if($arResult['BOOK']['authorbook']):?><div><span><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_AUTHOR')?>:</span><span><?=$arResult['BOOK']['authorbook']?></span></div><?endif;?>
						<?if($arResult['BOOK']['title']):?><div><span><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_TITLE')?>:</span> <span><?=$arResult['BOOK']['title']?></span></div><?endif;?>
						<?if($arResult['BOOK']['year']):?><div><span><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_YEAR')?>:</span> <span><?=$arResult['BOOK']['year']?></span></div><?endif;?>
						<?if($arResult['BOOK']['publisher']):?><div><span><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_PUBLISH')?>:</span> <span><?=$arResult['BOOK']['publisher']?></span></div><?endif;?>
						<?if($arResult['BOOK']['bbk']):?><div><span>BBK:</span> <span><?=$arResult['BOOK']['bbk']?></span></div><?endif;?>
						<?if($arResult['BOOK']['countpages']):?><div><span><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_PAGES')?>:</span> <span><?=$arResult['BOOK']['countpages']?></span></div><?endif;?>
						<?if ($arResult['BOOK']['series']):?><div><span><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_SERIE')?>:</span> <span><?=$arResult['BOOK']['series']?></span></div><?endif;?>
						<?if($arResult['BOOK']['contentnotes']):?><div><span>Примечание содержания:</span> <span><?=$arResult['BOOK']['contentnotes']?></span></div><?endif;?>
						<?if($arResult['BOOK']['edition']):?><div><span><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_EDITION')?>:</span> <span><?=$arResult['BOOK']['edition']?></span></div><?endif;?>
						<?if($arResult['BOOK']['publishplace']):?><div><span><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_PUBLISH_PLACE')?>:</span> <span><?=$arResult['BOOK']['publishplace']?></span></div><?endif;?>
						<?if($arResult['BOOK']['isbn']):?><div><span>ISBN:</span> <span><?=$arResult['BOOK']['isbn']?></span></div><?endif;?>
						<?if($arResult['BOOK']['issn']):?><div><span>ISSN:</span> <span><?=$arResult['BOOK']['issn']?></span></div><?endif;?>
						<?if($arResult['BOOK']['notes']):?><div><span><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_BOOK_NOTE')?>:</span> <span><?=$arResult['BOOK']['notes']?></span></div><?endif;?>
						<?if($arResult['BOOK']['udk']):?><div><span>УДК:</span> <span><?=$arResult['BOOK']['udk']?></span></div><?endif;?>
						<?if($arResult['BOOK']['placepublish']):?><div><span><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_PUBLISH_PLACE')?>:</span> <span><?=$arResult['BOOK']['placepublish']?></span></div><?endif;?>
						<?if($arResult['BOOK']['annotation']):?><div><span><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_ANNOTATION')?>:</span> <span><?=$arResult['BOOK']['annotation']?></div><?endif;?>
						<?if($arResult['BOOK']['library']):?><div><span ><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_LIB')?>:</span> <span><?=$arResult['BOOK']['library']?></span></div><?endif;?>
					</div>
				</div>
				<div class="sidebar-content">
					<div class="section  contents">
						<h2>Содержание</h2>
						<div class='thumb'></div>
					</div>
					<div class="section">
						<h2>Закладки</h2>
						<div class='mark'>
							<?foreach ($arResult['BOOK']['MARK'] as $mark):?>
							<div>
								<a p="<?=$mark['page']?>"><span><?=$mark['page']?></span> страница</a>
								<span class='delete' id='<?=$mark['id']?>'>Удалить</span>
							</div>
							<?endforeach;?>
						</div>
					</div>
					<div class="section">
						<h2>Цитаты</h2>
						<div class='quotes'>
							<?foreach($arResult['BOOK']['QUOTES'] as $quotes):?>
								<div>
								<a p="<?=$quotes["UF_PAGE"]?>"><span><?=$quotes["UF_PAGE"]?> страница</span>
								<img src='<?=$quotes["UF_IMG_DATA"]?>'>
								</a>
								<span class='delete' id='<?=$quotes["ID"]?>'>Удалить</span>
								</div>
							<?endforeach;?>
						</div>
					</div>
					<div class="section">
						<h2>Заметки</h2>
						<div class='notes'>
							<?foreach($arResult['BOOK']['NOTE'] as $note):?>
							<div>
								<a p="<?=$note["NUM_PAGE"]?>"><span><?=$note["NUM_PAGE"]?> страница</span>
								<span><?=$note["TEXT"]?></span>
								</a>
								<span class='delete' id='<?=$note["ID"]?>'>Удалить</span>
							</div>
							<?endforeach;?>
						</div>
					</div>
					<div class="section search_block">
						<h2>Поиск по тексту</h2>
						<div class='search_'>
							<input name='search_text' id = 'search_text'>
							<input id = 'search_submit' type='submit' value='Искать'>
							<div class='search_result'>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="main">
				<div class="header">
					<a href="/" class="logo"></a>
					<div class="book-info">
						<span class="book-title"><?=$arResult['BOOK']['title']?></span>
					</div>
					<div class="add_to_lk<?if(!empty($arResult['USER_BOOKS'])):?> active<?endif;?>" title="<?if(!empty($arResult['USER_BOOKS'])):?>В личном кабинете<?else:?>Добавить в личный кабинет<?endif;?>" data-collection="/local/tools/collections/list.php?t=books&id=<?=$arResult['BOOK']['URL']?>" data-url="/local/tools/collections/removeBook.php?id=<?=$arResult['BOOK']['URL']?>"></div>
					</div>
				<div class="content">
					<div class="navigation">
						<div p=1>В начало</div>
						<span class="prev active" p=1>1</span>
						<span p=2>2</span>
						<span class='next' p=3>3</span>
						<div  p="<?=$arResult['BOOK']['pageCount']?>">В конец</div>
					</div>
					<div class="page-view">
						<span class="page-number" style='display:none;'>1</span>
						<span class="book_id" style='display:none;'><?=$arResult['BOOK']['URL']?></span>
						<div class="tools">
							<span class="turn-page" title="Повернуть страницу"></span>
							<span class="bookmark" title='Закладка'></span>
							<span class="save-page" title="Сохранить"></span>
						</div>
						<div class="view" id="view">
							<div class='view-page'>
								<img id='image' width='500px'>
								<div id='selectedrect'>
									<div id='selected_rect_menu'><a href='' id='b-textLayer_quotes'>Добавить в цитаты</a>
									<a href='' id='b-textLayer_notes'>Добавить в заметки</a></div>
									<div class='layer_notes' style='display:none;'><textarea id='text_note'></textarea><input type='button' value='Добавить' id='add_note'></div>
									</div>
								</div>
							</div>
							<div id="infoTop"></div>
							<div id="infoLeft"></div>
							<div id="infoWidth"></div>
							<div id="infoHeight"></div>
							<div id="infoBottom"></div>
							<div id='page-preloader'>
								<div class="spinner"></div>
							</div>
					
						<div class="prev-page" p=0></div>
						<div class="next-page" p=2></div>
					</div>
				</div>
			</div>
		</div>
<script>
	var angle=0;
 
 
		$(document).ready(function(){
				
			$("body").keyup(function(e){
				if(e.which == 39) $('.next-page').click();
				if(e.which == 37) $('.prev-page').click();
				// поиск по Enter
				if(e.keyCode==13) {
					if ($.trim($('#search_text').val()) != ''){
						$('#search_submit').click();
					}
				}
			});
			
			
			// смена страницы
			$('.navigation span, .navigation div, .next-page, .prev-page').click(function(){
				var p = parseInt($(this).attr('p'));
				// получение страницы
				getImage(p)
			});

			// перевернуть страницу
			$('.turn-page').click(function(){
				angle+=90;
				if (angle == 360) angle=0;
				$('.view-page').rotate(angle);

			});
			
			// подгрузка страниц в содержание
			$('.contents').click(function(){
				if ($('.thumb').children('img').length == 0){
				<?for ($i=1;$i<=6;$i++):?>
					$('.thumb').append('<img src="/local/tools/exalead/thumbnails.php?book_id=<?=$arResult['BOOK']['URL']?>&p=<?=$i?>" p="<?=$i?>">');
				<?endfor;?>
				}
			});

			//добавление в закладки
			$('.bookmark').click(function(){
					if($(this).hasClass('active') != true){
					var book_id = "<?=$arResult['BOOK']['URL']?>";
					var page = parseInt($('.page-number').html());
					$.ajax({
						type: "GET",
						url: "/local/tools/viewer/bookmark_list.php",
						data: "book_id="+book_id+"&page="+page,
						success: function(msg){

						if (parseInt(msg) >0 ){
						$('.bookmark').addClass('active');
						$('.mark').append('<div><a p="'+page+'"><span>'+page+'</span> страница</a><span id='+msg+' class="delete">Удалить</span></div>');
					}
					}
				})
				}
			});
								
			// смена страницы из выпадающего списка
			$('#pages').change(function(){
				var p = parseInt($(this).val());
				// получение страницы
				getImage(p);
			});
			
			// смена страницы
			$('.thumb,div.search_,.notes, .quotes , .mark').on('click','img,a',function(){
				var p = parseInt($(this).attr('p'));
				// получение страницы
				getImage(p);
			});
			
			// поиск
			$('#search_submit').click(function(){
				var book_id = "<?=$arResult['BOOK']['URL']?>";
				var search_text = encodeURI($('#search_text').val());
				$.ajax({
					type: "GET",
					url: "/local/tools/exalead/search.php?search_text="+search_text+"&book_id="+book_id,
					success: function(data){

						$('.search_result div').remove();
						$('.search_result').append(data).css('display','block');
					}
				})
			});

			$('.search').click(function(){
				$(this).toggleClass("active");
				$('.search_block h2').click();
			})
			
			// добавление/удаление в личный кабинет
			$('.add_to_lk').click(function(){
				var url = '';
				if ($(this).hasClass("active") == true){
					url = $(this).attr('data-url');
					$(this).attr('title','Добавить в личный кабинет');
				}else{
					url = $(this).attr('data-collection');
					$(this).attr('title','В личном кабинете');
				}
				
				$.ajax({
						type: "GET",
						url: url,
						success: function(data){
							$('.add_to_lk').toggleClass("active");
						}
					})
				
			
			});
			
			// свойства документа
			$('.document-options').click(function(){
				$('.document-options-view').slideToggle();
				$('.document-options').toggleClass("active");

			})

			// добавление в цитаты
			$('#b-textLayer_quotes').click(function(event){
					$('.layer_notes').css({'display':'none'});
					var url = $(this).attr('href');
					$.ajax({
								type: "GET",
								url: url,
								success: function(data){
									$('.quotes').append(data);
									alert('Цитата добавлена');
									$('#selectedrect').css({'display':'none'});
								}
							})
					event.preventDefault();
				
			});
			// добавление в заметки
			$('#b-textLayer_notes').click(function(event){

				$('.layer_notes').css('display', 'block');
				event.preventDefault();
				

				
			});
			
			$('#add_note').click(function(event){
					var url = $('#b-textLayer_notes').attr('href');
					var note = $('#text_note').val();
					$.ajax({
								type: "GET",
								url: url+'&note='+note,
								success: function(data){
									$('.notes').append(data);
									alert('Заметка добавлена');
									$('#selectedrect').css({'display':'none'});
								}
							})
					event.preventDefault();
				
			});
			
			// сохранение страницы
			$('.save-page').click(function(){
				var book_id = $('.book_id').html();
				var page = parseInt($('.page-number').html());
				var url = '/local/tools/exalead/savePage.php?book_id='+book_id+'&p='+page;
			
				  window.location.href = url;
			
			
			})
			
			// удалить закладку
			$('.mark').on('click', 'span.delete',function(){
				var mark_id = this.id;
				var url = '/local/tools/viewer/bookmark_list.php';
				var page = parseInt($('.page-number').html());
				$.ajax({
								type: "GET",
								url: url+'?mark_id='+mark_id,
								success: function(data){
									$('#'+mark_id).parent('div').remove();
									isMark(page);
								}
							})
			})
			
			// удалить цитату
			$('.quotes').on('click', 'span.delete',function(){
				var quo_id = this.id;
				var url = '/local/tools/exalead/saveImg.php';
				$.ajax({
								type: "GET",
								url: url+'?quo_id='+quo_id,
								success: function(data){
									$('#'+quo_id).parent('div').remove();
								}
							})
			})
			// удалить закладку
			$('.notes').on('click', 'span.delete',function(){
				var note_id = this.id;
				var url = '/local/tools/exalead/saveImg.php';
				$.ajax({
								type: "GET",
								url: url+'?note_id='+note_id,
								success: function(data){
									$('#'+note_id).parent('div').remove();
								}
							})
			})
			
		
			// подгрузка страниц в содержимое
			$(function() {
			var container = $('.thumb');
			//обработчик события scroll
				container.scroll(function() {
					//это условие выполняется когда ползунок находится в крайнем нижнем положении
					if  (container[0].scrollTop == container[0].scrollHeight - container[0].clientHeight){
						var page = parseInt($(".thumb img").last().attr('p'))+1;
						for (var i=page;i<=page+1;i++){
							if (i<= <?=$arResult['BOOK']['pageCount']?>){
								$('.thumb').append('<img src="/local/tools/exalead/thumbnails.php?book_id=<?=$arResult['BOOK']['URL']?>&p='+i+'" p="'+i+'">');
							}
						}

					}
				});
			});

		});
function Croppable(options) {
  var self = this;

  var elem = options.elem;

  var cropArea;
  var cropStartX, cropStartY;

  elem.on('selectstart dragstart', false)
   .on('mousedown', onImgMouseDown);

  function initCropArea(pageX, pageY) {
    cropArea = $('<div class="crop-area"/>')
      .appendTo('.page-book');

    cropStartX = pageX;
    cropStartY = pageY;
  }

  function onDocumentMouseMove(e) {
    drawCropArea(e.pageX, e.pageY);
  }

  function onDocumentMouseUp(e) {
    endCrop(e.pageX, e.pageY);

    cropArea.remove();
    $(document).off('.croppable');
  }

  function onImgMouseDown(e) {
    initCropArea(e.pageX, e.pageY);

    $(document).on({
      'mousemove.croppable': onDocumentMouseMove,
      'mouseup.croppable': onDocumentMouseUp
    });
   
    return false;
  };

  function drawCropArea(pageX, pageY) {
    var dims = getCropDimensions(pageX, pageY);

    cropArea.css(dims);

    // вычитаем 2, т.к. ширина будет дополнена рамкой
    // если не вычесть, то рамка может вылезти за изображение
    cropArea.css({
      height: Math.max(dims.height-2, 0),
      width: Math.max(dims.width-2,0)
    });

    // здесь мы просто рисуем полупрозрачный квадрат
    // альтернативный подход - накладывать на каждую часть изображения div'ы с opacity и черным цветом, чтобы только кроп был ярким
  }

  function endCrop(pageX, pageY) {
    var dims = getCropDimensions(pageX, pageY);

    var coords = elem.offset();
    // получить координаты относительно изображения
    dims.left -= coords.left;
    dims.top -= coords.top;

    $(self).triggerHandler($.extend({ type: "crop" }, dims));
  }

  function getCropDimensions(pageX, pageY) {
    // 1. Делаем квадрат из координат нажатия и текущих
    // 2. Ограничиваем размерами img, если мышь за его пределами

    var left = Math.min(cropStartX, pageX);
    var right = Math.max(cropStartX, pageX);
    var top = Math.min(cropStartY, pageY);
    var bottom = Math.max(cropStartY, pageY);

    var coords = elem.offset();

    left = Math.max(left, coords.left);
    top = Math.max(top, coords.top);
    right = Math.min(right, coords.left + elem.outerWidth());
    bottom = Math.min(bottom, coords.top + elem.outerHeight());

    return { left: left, top: top, width: right-left, height: bottom-top };
  }

}

var croppable = new Croppable({
  elem: $('#image')
});

$(croppable).on("crop", function(event) {
  // вывести координаты и размеры crop-квадрата относительно изображения
$('#selectedrect').css({'display':'none'});
		$('.layer_notes').css({'display':'none'});
		$('#text_note').val('');
  var offsetWidth = <?=$arResult['BOOK']['width']?>;
	var strWidth = "";
  $(['width']).each(function() {
    strWidth += event[this];
  });

	// var p = offsetWidth/parseInt(strWidth);
 var p = 1;
  
  var strTop = "";
  $(['top']).each(function() {
    strTop += event[this];
  });
  var top = parseInt(strTop)*p;

  $('#infoTop').html("Top: " + strTop);
  
   var strLeft = "";
  $(['left']).each(function() {
    strLeft += event[this];
  });
  var left = parseInt(strLeft)*p;

  $('#infoLeft').html("Left: " + strLeft);
  
  

  $('#infoWidth').html("Width: " + strWidth);
  
  
   var strHeight = "";
  $(['height']).each(function() {
    strHeight += event[this];
  });
   var h = parseInt(strHeight)*p;
$('#selectedrect').css('height',strHeight);
  $('#infoHeight').html("Height: " + strHeight);

  var strBottom = +strTop + +strHeight;
  $('#infoBottom').html('Bottom: ' + strBottom);
	if (parseInt(strWidth) > 0){


		var page = parseInt($('.page-number').html());

		// определить повернута ли книга 
		
		if (angle == 0){
		// высота и ширина картинки
			$('#selectedrect').css({'display':'block','width': strWidth+"px", 'height':strHeight+"px", 'top':strTop+"px", 'left':strLeft+"px"}); 
			$('#b-textLayer_quotes').attr('href', '/local/tools/exalead/saveImg.php?book_id=<?=$arResult['BOOK']['URL']?>&p='+page+'&top='+top+'&left='+left+'&w='+strWidth+'&h='+h);
			$('#b-textLayer_notes').attr('href', '/local/tools/exalead/saveImg.php?book_id=<?=$arResult['BOOK']['URL']?>&p='+page+'&top='+top+'&left='+left+'&w='+strWidth+'&h='+h+'&type=note');
		}

	}
 });


	$('.section>h2').click(function() {
		if ( !$(this).parent().hasClass('active') ) {
			$('.sidebar .section').removeClass('active');
			$('.sidebar .section div').slideUp(900);
			$(this).siblings('div').slideDown(900);
			$(this).parent().addClass('active');
		} else {
			$(this).siblings('div').slideUp(900);
			$(this).parent().removeClass('active');
		}
	});

function getImage(p){
$('#selectedrect').css({'display':'none'});
$('.layer_notes').css({'display':'none'});
$('#text_note').val('');


						if (p > 0){
							var $preloader = $('#page-preloader'),
							$spinner   = $preloader.find('.spinner');
							$preloader.fadeIn();
							var search = '';

							// если был осуществлен поиск, то добавить
							if ($('.search_ div').length > 0){

								search = $('#search_text').val();
							} 
							
							$('#image').attr("src", "/local/tools/exalead/getImages.php?book_id=<?=$arResult['BOOK']['URL']?>&p="+p+"&search="+search+"");
							
							//проверка в закладках ли страница
							isMark(p);
							
							$('.page-number').html(p);
							var prev = 1;
							var center = 2;
							var next = 3

							$('.navigation span.active').removeClass('active');
							$('.prev-page').removeClass('noactive');
							$('.next-page').removeClass('noactive');
							
							switch(p){
								// первая страница
								case 1:
									$('.navigation span.prev').addClass('active');
									$('.prev-page').addClass('noactive');
									$('.prev-page').attr('p',1);
									$('.next-page').attr('p',2);
									break;
									
								// последняя страница
								case <?=$arResult['BOOK']['pageCount']?>:
									prev = p-2;
									center = p-1;
									next = p;
									$('.navigation span.next').addClass('active');
									$('.next-page').addClass('noactive');
									$('.next-page').attr('p',next);
									$('.prev-page').attr('p',center);
									break;
									
								default:
									$('.navigation span.prev').next('span').addClass('active');
									prev = p-1;
									center = p;
									next = p+1;
									
									$('.next-page').attr('p',next);
									$('.prev-page').attr('p',prev);
									
							}
							$('.navigation span.prev').html(prev);
							$('.navigation span.prev').attr('p', prev);
							$('.navigation span.prev').next('span').html(center);
							$('.navigation span.prev').next('span').attr('p', center);
							$('.navigation span.next').html(next);
							$('.navigation span.next').attr('p', next);
							$('#image').load(function() {$preloader.delay(350).fadeOut('slow');});
						}
						
					}

						function isMark(page){

							$.ajax({
								type: "GET",
								url: "/local/tools/viewer/bookmark_exist.php",
								data: "book_id=<?=$arResult['BOOK']['URL']?>&page="+page,
							  	success: function(data){
								if (parseInt(data) >0 ){
									$('.bookmark').addClass('active');
							}else{
								$('.bookmark').removeClass('active');
								   }
								}
							})


						}
					var p = <?=(intval($_REQUEST['page'])>0?intval($_REQUEST['page']):1)?>;
					getImage(p);

	$(window).load(function () {
    var $preloader = $('#page-preloader'),
        $spinner   = $preloader.find('.spinner');

    $preloader.fadeOut('slow');

	});
</script>
<?php require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>