var angle=0;
var book_id, count_page, min_width_img;

$(document).ready(function(){

	book_id = $('#book_id').val();
	count_page = parseInt($('#count_page').val());

	var p = parseInt($('#page-number').val());

	var max_width_img = 800;
	var max_height_img = 1000;
	min_width_img = parseInt($('#image').css('max-width'));
	var min_height_img = parseInt($('#image').css('max-height'));

	getImage(p);	
	
	/* $(".viewer").zoomer({
		controls: {
			zoomIn: ".increase",
			zoomOut: ".reduce"
		}
	});  */

	/* toggle left tools */
	$('.toggle-left-tools').click(function() {
		$(this).toggleClass('close');
		$('.prev-page').toggleClass('left');

		if ( $('.left-tools').is(':visible') ) {
			$('.left-tools').slideUp(200);
		}
		else {
			$('.left-tools').slideDown(200);
		}
	});
	/* end toggle left tools */
	
	/* show submenu */
	$('.left-tools li.sub').hover(
	function() {
		$(this).children('ul').animate({
			left: "80px",
			}, 100, function() {
		});
	}, function() {
		$(this).children('ul').animate({
			left: "-80px",
			}, 100, function() {
		});
	}
	);
	/* end show submenu */
	
	/* toggle right tools */
	$('.toggle-right-tools').click(function() {
		$(this).toggleClass('close');
		$('.top-right-tools').toggleClass('close');
		if ( $('.top-right-tools').hasClass('close') ) {
			$('.properties-book-content, .miniatures-content, .bookmark-content, .quote-content, .note-content, .search-book-content').hide();
			$( ".top-right-tools" ).animate({
				width: "42px",
				}, 300, function() {
			});
		}
		else {
			$( ".top-right-tools" ).animate({
				width: "210px",
				}, 300, function() {
			});
		}
		if ( $('.right-tools').is(':visible') ) {
			$('.right-tools').slideUp(200);
		}
		else {
			$('.right-tools').slideDown(200);
		}
	});
	/* end toggle right tools */
	
	/* show right tools */

	$('.properties-book').click(function() {
			$('.miniatures-content, .bookmark-content, .quote-content, .note-content, .search-book-content').slideUp();
			$('.miniatures-content, .bookmark-content, .quote-content, .note-content, .search-book-content').siblings('span').removeClass('active');
			$(this).toggleClass('active'); 
			$('.search-book').removeClass('active');
			$('.properties-book-content').slideToggle(200);
	});
	
	$('.miniatures').click(function() {
			$('.properties-book-content, .bookmark-content, .quote-content, .note-content, .search-book-content').slideUp();
			$('.properties-book-content, .bookmark-content, .quote-content, .note-content, .search-book-content').siblings('span').removeClass('active');
			$(this).toggleClass('active'); 
			$('.miniatures-content').slideToggle(200);
			$('.search-book').removeClass('active');
			$('.properties-book').removeClass('active');
	});
	
	$('.bookmark').click(function() {
			$('.properties-book-content, .miniatures-content, .quote-content, .note-content, .search-book-content').slideUp();
			$('.properties-book-content, .miniatures-content, .quote-content, .note-content, .search-book-content').siblings('span').removeClass('active');
			$(this).toggleClass('active'); 
			$('.bookmark-content').slideToggle(200);
			$('.search-book').removeClass('active');
			$('.properties-book').removeClass('active');
	});

	$('.quote').click(function() {
			$('.properties-book-content, .miniatures-content, .bookmark-content, .note-content, .search-book-content').slideUp();
			$('.properties-book-content, .miniatures-content, .bookmark-content, .note-content, .search-book-content').siblings('span').removeClass('active');
			$(this).toggleClass('active'); 
			$('.quote-content').slideToggle(200);
			$('.search-book').removeClass('active');
			$('.properties-book').removeClass('active');
	});
	
	$('.note').click(function() {
		$('.properties-book-content, .miniatures-content, .bookmark-content, .quote-content, .search-book-content').slideUp();
		$('.properties-book-content, .miniatures-content, .bookmark-content, .quote-content, .search-book-content').siblings('span').removeClass('active');
		$(this).toggleClass('active'); 
		$('.note-content').slideToggle(200);
		$('.search-book').removeClass('active');
		$('.properties-book').removeClass('active');
	});
	
	$('.search-book').click(function() {
		$('.properties-book-content, .miniatures-content, .bookmark-content, .quote-content, .note-content').slideUp();
		$('.properties-book-content, .miniatures-content, .bookmark-content, .quote-content, .note-content').siblings('span').removeClass('active');
		$(this).toggleClass('active'); 
		$('.properties-book').removeClass('active');
		if ( $(this).hasClass('active') ) {
			$('.search').addClass('active');
			$('.search-book-content').slideDown(200);
		}
		else {
			$('.search-book-content').slideUp(200);
			$('.search').removeClass('active');
		}
	});
	$('.search').click(function() {
		$('.properties-book-content, .miniatures-content, .bookmark-content, .quote-content, .note-content').slideUp();
		$('.properties-book-content, .miniatures-content, .bookmark-content, .quote-content, .note-content').siblings('span').removeClass('active');
		$(this).toggleClass('active');
		$('.properties-book').removeClass('active'); 
		if ( $(this).hasClass('active') ) {
			$('.search-book').addClass('active');
			$('.search-book-content').slideDown(200);
		}
		else {
			$('.search-book-content').slideUp(200);
			$('.search-book').removeClass('active');
		}
	});

	

	/* end show right tools */
	
	/* footer */
	$('.toggle-page-navigation').click(function() {
		$(this).toggleClass('active');
		$('.page-navigation').toggleClass('active');
	});
	/* end footer */



	$("body").keyup(function(e){
		if(e.which == 39) $('.next-page').click();
		if(e.which == 37) $('.prev-page').click();
		// поиск по Enter
		if(e.keyCode==13 && $(".page-navigation input.page:focus")) {
			$('.go').click();
		}
		if(e.keyCode==13 && $("#search_text:focus")) {
			if ($.trim($('#search_text').val()) != ''){
			$('#search_submit').click();
			}
		}

		if (e.keyCode == 27){
			$(".b-headernav_login").fadeOut("slow");
			$('#selectedrect').css({'display':'none'});
			$('.layer_notes').css({'display':'none'});
			$('#text_note').val('');
		}
	});

	// смена страницы
	$('.navigation span, .navigation div, .next-page, .prev-page').on('click',function(){
				
		var p = parseInt($(this).attr('p'));
		var page = parseInt($('#page-number').val());
		if (p != page) getImage(p);
	});
	
	$('.page-navigation span.go').click(function(){
		var p = parseInt($('.page-navigation input.page').val())
		// получение страницы
		var page = parseInt($('#page-number').val());
		if (p != page) getImage(p);

	});

	// перевернуть страницу по часовой стрелки
			$('.rotate-right').click(function(){
				angle+=90;
				if (angle == 360) angle=0;
				$('.viewer').rotate(angle);
				$('#selectedrect').css({'display':'none'});
				$('.layer_notes').css({'display':'none'});
				$('#text_note').val('');
				if (angle == 0){
				$("div.quote-book, div.note-book").css('display', 'block');
				}else{
				$("div.quote-book, div.note-book").css('display', 'none');
				}
			});

			// перевернуть страницу против часовой стрелки
			$('.rotate-left').click(function(){
				angle-=90;
				if (angle == -360) angle=0;
				$('.viewer').rotate(angle);
				$('#selectedrect').css({'display':'none'});
				$('.layer_notes').css({'display':'none'});
				$('#text_note').val('');
				if (angle == 0){
				$("div.quote-book, div.note-book").css('display', 'block');
				}else{
				$("div.quote-book, div.note-book").css('display', 'none');
				}
			});	
			
		// сохранение страницы
			$('.left-tools span.save').click(function(){
				var page = $('#page-number').val();
				var url = '/local/tools/exalead/savePage.php?book_id='+book_id+'&p='+page;
			
				  window.location.href = url;
			
			
			})
			
	// подгрузка страниц в содержание
			$('.miniatures').click(function(){
				var page = $('#page-number').val();
				var count = 4;
				if (count_page < count ) count = count_page;
				if ($('.miniatures-content div.miniatures-images').children('span').length == 0){
					var i;
					for (i=1;i<=count;i++){
						$('.miniatures-content div.miniatures-images').append('<span  p='+i+' ><img src="/local/tools/exalead/thumbnails.php?book_id='+book_id+'&p='+i+'"></span>');
					}

				$(".miniatures-content div.miniatures-images span[p = "+page+"]").addClass('active');

					$('.miniatures-content div.miniatures-images span img').load(function() {
				$('.miniatures-content div.miniatures-images span[p=1] img, .miniatures-content div.miniatures-images span[p=2] img, .miniatures-content div.miniatures-images span[p=3] img, .miniatures-content div.miniatures-images span[p=4] img').css('display', 'block')});


				}
			});


	// содержание - следующий элемент
		$('.miniatures-content div.miniatures-top div,.miniatures-content div.miniatures-bottom div').on('click', function(){
			var p = parseInt($(this).attr('p'));

			var page = parseInt($('#page-number').val());

			if (p>0 && p <= count_page){
				$(".miniatures-content div.miniatures-images span").removeClass('active');
				//следующий элемент
				if ($(this).hasClass('next') == true){
					$('.miniatures-content div.miniatures-images span:first').remove();
					$('.miniatures-content div.miniatures-images').append('<span p='+p+'><img  src="/local/tools/exalead/thumbnails.php?book_id='+book_id+'&p='+p+'"></span>');
					$('.miniatures-content div.miniatures-bottom div.next').attr('p', p+1);
					$('.miniatures-content div.miniatures-top div.prev').attr('p', p-4);

				}else{
					$('.miniatures-content div.miniatures-images span:last-child').remove();
					$('.miniatures-content div.miniatures-images').prepend('<span p='+p+'><img  src="/local/tools/exalead/thumbnails.php?book_id='+book_id+'&p='+p+'" p='+p+'></span>');
					$('.miniatures-content div.miniatures-bottom div.next').attr('p', p+4);
					$('.miniatures-content div.miniatures-top div.prev').attr('p', p-1);
				}
				$(".miniatures-content div.miniatures-images span[p = "+page+"]").addClass('active');
				$('.miniatures-content div.miniatures-images span[p='+p+'] img').load(function() {$('.miniatures-content div.miniatures-images span[p='+p+'] img').css('display', 'block')});
			}
		
		
		})
		
	// смена страницы
	$('div.miniatures-images, div.bookmark-content, div.quote-content, div.note-content, div.search_result').on('click','span,a',function(){	
		var p = parseInt($(this).attr('p'));
		// получение страницы
		var page = parseInt($('#page-number').val());
		if (p != page) getImage(p);
	});
	
	// добавление/удаление в личный кабинет
		$('.favorite-book').click(function(){
			if ($('form.b-formlogin')[0]){
				$('.b-headernav_login').slideDown();

			}else{
				var url = '';
				if ($(this).hasClass("active") == true){
					url = $(this).attr('data-url');
					$(this).attr('title','Добавить в личный кабинет');
					$(this).html('Добавить в избранное');
				}else{
					url = $(this).attr('data-collection');
					$(this).attr('title','В личном кабинете');
					$(this).html('Удалить из избранного');
				}

				$.ajax({
						type: "GET",
						url: url,
						success: function(data){
							
							$('.favorite-book').toggleClass("active");
						}
					})
					
			}
			});

	/*$('.main *').click(function(){
		if ($(this).attr("class") != 'view-page'){
		console.log($(this).attr("class"));
      $(".b-headernav_login").fadeOut("slow");
		}
		

	})
	*/
	
	//добавление в закладки
	$('.bookmark-tools, .add-bookmark').click(function(event){
		if ($('form.b-formlogin')[0]){

			$('.b-headernav_login').slideDown();
			event.preventDefault();
			
		}else{
			if ($('.favorite-book').hasClass('active') == false){
				$('.favorite-book').click();
			}
			var page = parseInt($('#page-number').val());

			if($(this).hasClass('active') != true){

				$.ajax({
					type: "GET",
					url: "/local/tools/viewer/bookmark_list.php",
					data: "book_id="+book_id+"&page="+page,
					success: function(msg){

						if (parseInt(msg) >0 ){
							$('#exampleModal p').html('Закладка добавлена');
							$('#exampleModal').arcticmodal();
							$('.bookmark-content').append('<div><a p="'+page+'"><span>'+page+'</span> страница</a><span id='+msg+' class="delete"></span></div>');
							isMark(page);
						}
					}
				})
			}else{
				$('.bookmark-content a[p='+page+']').next('span.delete').click();

			}


		}
	});
	
	// удалить закладку
	$('.bookmark-content').on('click', 'span.delete',function(){
		var mark_id = this.id;
		var url = '/local/tools/viewer/bookmark_list.php';
		var page = parseInt($('#page-number').val());

		$.ajax({
			type: "GET",
			url: url+'?mark_id='+mark_id,
			success: function(data){
			$('#'+mark_id).parent('div').remove();

			isMark(page);
			}
		})
	})
	
	// добавление в цитаты
		$('#b-textLayer_quotes').click(function(event){
			if ($('form.b-formlogin')[0]){

				$('.b-headernav_login').slideDown();
				event.preventDefault();
			
			}else{
				if ($('.favorite-book').hasClass('active') == false){
						$('.favorite-book').click();
				}
					$('.layer_notes').css({'display':'none'});
					var url = $(this).attr('href');
					$.ajax({
								type: "GET",
								url: url,
								success: function(data){
									$('.quote-content').append(data);
									var p = parseInt($('#page-number').val());
									getImage(p);
									$('#exampleModal p').html('Цитата добавлена');
									$('#exampleModal').arcticmodal();
									$('#selectedrect').css({'display':'none'});
								}
							})

					event.preventDefault();
			}
			});
			// добавление в заметки
			$('#b-textLayer_notes').click(function(event){
				if ($('form.b-formlogin')[0]){

					$('.b-headernav_login').slideDown();
					event.preventDefault();
			
				}else{
					$('.layer_notes').css('display', 'block');
					event.preventDefault();
				
				}
				
			});

			$('#add_note').click(function(event){
				if ($('.favorite-book').hasClass('active') == false){
					$('.favorite-book').click();
				}
					var url = $('#b-textLayer_notes').attr('href');
					var note = $('#text_note').val();
					$.ajax({
								type: "GET",
								url: url+'&note='+note,
								success: function(data){
									$('#exampleModal p').html('Заметка добавлена');
									$('#exampleModal').arcticmodal();
									var p = parseInt($('#page-number').val());
									getImage(p);
									alert('Заметка добавлена');
									$('#selectedrect').css({'display':'none'});
								}
							})
					event.preventDefault();
				
			});
			// удалить цитату
			$('.quote-content').on('click', 'span.delete',function(){
				var quo_id = this.id;
				var url = '/local/tools/exalead/saveImg.php';
				$.ajax({
								type: "GET",
								url: url+'?quo_id='+quo_id,
								success: function(data){
									$('#'+quo_id).parent('div').remove();
									var p = parseInt($('#page-number').val());
									getImage(p);
								}
							})
			})
			// удалить закладку
			$('.note-content').on('click', 'span.delete',function(){
				var note_id = this.id;
				var url = '/local/tools/exalead/saveImg.php';
				$.ajax({
								type: "GET",
								url: url+'?note_id='+note_id,
								success: function(data){
									$('#'+note_id).parent('div').remove();
									var p = parseInt($('#page-number').val());
									getImage(p);
								}
							})
			})
			
	
	
	$('.move').click(function(){
		$(this).toggleClass("active");
		if ($(this).hasClass("active") == true){
			$('.zoomer-image').css('cursor', 'move');
		}else{
			$('.zoomer-image').css('cursor', 'default');

		}

	})
	
	
	// поиск
			$('#search_submit').click(function(){
				var search_text = encodeURI($('#search_text').val());
				$.ajax({
					type: "GET",
					url: "/local/tools/exalead/search.php?search_text="+search_text+"&book_id="+book_id,
					success: function(data){

						$('.search_result div').remove();
						$('.search_result').append(data).css('display','block');
					}
				})
			});
			
			
	$('.scale-page').click(function(){
		$('.turn-page ').addClass('active');
		$('.left-tools').hide();
		$('.footer').addClass('close');
		$('.footer-small').addClass('open');
		$('.main').addClass('padding-bottom');
		$('.page-book .right').hide();
		$('.navigation').addClass('open');
		$('#selectedrect').css({'display':'none'});
		$('.layer_notes').css({'display':'none'});
		$('#text_note').val('');
		$("div.quote-book, div.note-book").css('display', 'none');
		$("#image").animate({"max-width": max_width_img+"px","max-height": max_height_img+"px" }, "normal");

	})	

	$('.turn-page').click(function(){
		$(this).removeClass('active');
		$('.left-tools').show();
		$('.footer').removeClass('close');
		$('.footer-small').removeClass('open');
		$('.main').removeClass('padding-bottom');
		$('.page-book .right').show();
		$('.navigation').removeClass('open');
		$("div.quote-book, div.note-book").css('display', 'block');
		$("#image").animate({"max-width": min_width_img+"px","max-height": min_height_img+"px" }, "normal");

	})	
	//масштабирование
	$('.increase').click(function(){

		if ($("#image").width() < max_width_img && $("#image").height() < max_height_img){
		 //$(".viewer").animate({"width": "+=20px"}, "slow");
		$('#selectedrect').css({'display':'none'});
$('.layer_notes').css({'display':'none'});
$('#text_note').val('');
$("div.quote-book, div.note-book").css('display', 'none');
		 $("#image").animate({"max-width": "+=20px","max-height": "+=20px"}, "normal");
		}

	});

	$('.reduce').click(function(){

		if ($("#image").width() > min_width_img){
		 //$(".viewer").animate({"width": "-=20px"}, "slow");
			$("#image").animate({"max-width": "-=20px", "max-height": "-=20px"}, "normal");
		}else{
			$("div.quote-book, div.note-book").css('display', 'block');
		}

	});

	$('.scale-status').click(function(){
		$("div.quote-book, div.note-book").css('display', 'block');
		 $("#image").animate({"max-width": min_width_img+"px","max-height": min_height_img+"px" }, "normal");

	});



    var jcrop_api;

    $('.viewer').Jcrop({
      onChange:   showCoords_,
      onSelect:   showCoords,
      onRelease:  clearCoords
    },function(){

      jcrop_api = this;
    });

    $('#coords').on('change','input',function(e){

      jcrop_api.setSelect([x1,y1,x2,y2]);

    });

  });

  // Simple event handler, called from onChange and onSelect
  // event handlers, as per the Jcrop invocation above
	function showCoords_(c)
  {
	$('#selectedrect').css({'display':'none'});
	$('.layer_notes').css({'display':'none'});
	$('#text_note').val('');
	  if (angle == 0 && min_width_img == parseInt($('#image').css('max-width'))){
 		$('.jcrop-keymgr').next().css('display', 'block');
	  }
  }

	function showCoords(c)
	{

	var page = parseInt($('#page-number').val());
	var width_img = parseInt($('#image').width());
	if (angle == 0 && min_width_img == parseInt($('#image').css('max-width'))){
		var max_width_img = 800;
		var left = (max_width_img-width_img)/2;
		left = c.x-left;

		$('#selectedrect').css({'display':'block','width': c.w+"px", 'height':c.h+"px", 'top':c.y+"px", 'left': c.x+"px"}); 
		$('#b-textLayer_quotes').attr('href', '/local/tools/exalead/saveImg.php?book_id='+book_id+'&p='+page+'&top='+c.y+'&left='+left+'&w='+c.w+'&h='+c.h+'&w_img='+width_img);
		$('#b-textLayer_notes').attr('href', '/local/tools/exalead/saveImg.php?book_id='+book_id+'&p='+page+'&top='+c.y+'&left='+left+'&w='+c.w+'&h='+c.h+'&type=note&w_img='+width_img);
	}

	$('.jcrop-keymgr').next().css('display', 'none');
	};

	function clearCoords()
	{
		$('#coords input').val('');
	};




function getImage(p){

	

	if (p > 0 && p <= count_page){
	
		$('#selectedrect').css({'display':'none'});
		$('.layer_notes').css({'display':'none'});
		$('#text_note').val('');
		$("div.quote-book, div.note-book").remove();
		$('.navigation span').removeClass('end');
		var $preloader = $('.preloader');
		$preloader.fadeIn();					
		var search = '';
		// если был осуществлен поиск, то добавить
		if ($('.search_ div').length > 0){

			search = $('#search_text').val();
		} 

		//проверка в закладках ли страница
		isMark(p);

		$('.zoomer-image').attr("src", "/local/tools/exalead/getImages.php?book_id="+book_id+"&p="+p+"&search="+search+"");
							

							
		$('#page-number').val(p);
		var page_1 = 1;
		var page_2 = 2;
		var page_3 = 3;

		$('.navigation span.active').removeClass('active');
									
		switch(p){
			// первая страница
			case 1:
				var page_1 = 1;
				var page_2 = 2;
				var page_3 = 3;
				$('.page_1').addClass('active');
				$('.prev').addClass('end');
				$('.first').addClass('end');
				break;
								
			// вторая страница
			case 2:
				var page_1 = p-1;
				var page_2 = p;
				var page_3 = p+1;
				$('.page_2').addClass('active');
				break;	

			case count_page:
				var page_1 = p-3;
				var page_2 = p-2;
				var page_3 = p-1;
				$('.next').parent('li').prev('li').children('span').addClass('active');
				$('.next').addClass('end');
				$('.last').addClass('end');
				if (count_page != 3) break;	
								
			default:
				var page_1 = p-2;
				var page_2 = p-1;
				var page_3 = p;
				$('.page_3').addClass('active');
											
		}
		$('.page_1').attr('p',page_1).html(page_1);
		$('.page_3').attr('p',page_3).html(page_3);
		if (page_2 <= count_page) {
			$('.page_2').attr('p',page_2).html(page_2);
		} else{
			$('.page_3').hide();
		}
		if (page_3 >= count_page) {
			$('.next').addClass('end');
			$('.last').addClass('end');
			$('.page_3').parent('li').next('li').next('li').hide();
			$('.page_3').parent('li').next('li').hide();
		}					
		if (p == count_page ){
			$('.navigation span.next').attr('p',p);
			$('.next-page').attr('p',p);
		}else{
			$('.navigation span.next').attr('p',p+1);
			$('.next-page').attr('p',p+1);
		}
		
		$('.navigation span.prev').attr('p',p-1);
		$('.prev-page').attr('p',p-1);
		$(".miniatures-content div.miniatures-images span").removeClass('active');
		$(".miniatures-content div.miniatures-images span[p = "+p+"]").addClass('active');
	
		$('.zoomer-image').load(function() {$preloader.fadeOut('slow');});
		isQuo(p);
	}

}				
// проверка на наличие цитат, заметок
function isQuo(page){
	$.ajax({
		type: "GET",
		url: "/local/tools/viewer/quo_list.php",
		data: "book_id="+book_id,
		success: function(data){
		var page = parseInt($('#page-number').val());
			//цитаты
			for(var i in data['cites']) {
				if (data['cites'][i].citepage == page){
			$('.viewer_img').append('<div id="quo_'+data['cites'][i].id+'" class="quote-book"><div class="quote-icon"></div><div class="quote-book-content"></div></div>');
			$('#quo_'+data['cites'][i].id).css({'width': data['cites'][i].citewidth+'px', 'height': data['cites'][i].citeheight+'px', 'top': data['cites'][i].citetop+'px', 'left': data['cites'][i].citeleft+'px'});
				}
			}

			// заметки
			for(var i in data['notes']) {

				if (data['notes'][i].notepage == page){
					console.log(data['notes'][i]);
					//$('.viewer_img').append('<div id="note_'+data['notes'][i].id+'" class="note-book"><div class="note-icon"></div><textarea class="note-book-content" disabled>'+data['notes'][i].note+'</textarea><span class="note-edit"></span></div>');
					$('.viewer_img').append('<div id="note_'+data['notes'][i].id+'" class="note-book"><div class="note-icon"></div><textarea class="note-book-content" disabled>'+data['notes'][i].note+'</textarea><span class="note-edit" data-url="/local/tools/viewer/note_add.php?noteid='+data['notes'][i].id+'&notepage='+data['notes'][i].notepage+'&notetop='+data['notes'][i].notetop+'&noteleft='+data['notes'][i].noteleft+'&notewidth='+data['notes'][i].notewidth+'&noteheight='+data['notes'][i].noteheight+'"></span></div>');

					$('#note_'+data['notes'][i].id).css({'width': data['notes'][i].notewidth+'px', 'height': data['notes'][i].noteheight+'px', 'top': data['notes'][i].notetop+'px', 'left': data['notes'][i].noteleft+'px'});

				}
			}
			setTimeout(function() {

				$('.note-icon').click(function() {
					$(this).parent('.note-book').toggleClass('hide');
					$(this).siblings('.note-book-content').slideToggle(300);
					$(this).siblings('.note-edit').toggleClass('visible');
				});
				$('.quote-icon').click(function() {
					$(this).parent('.quote-book').toggleClass('hide');
					$(this).siblings('.quote-book-content').slideToggle(300);
				});

				$('.note-edit').click(function() {
					if ( $(this).hasClass('save') ) {
						$(this).siblings('.note-book-content').attr('disabled',true);
						$(this).parent('.note-book').removeClass('edit');
						$(this).removeClass('save');
						var text = $(this).prev().val();
						var url = $(this).attr('data-url')+'&note='+text+'&book_id='+book_id;

						console.log(text);
						console.log(url);
							$.ajax({
											type: "GET",
											url: url,
											success: function(data){
												console.log(url);

											}
										})
					} else {
						$(this).siblings('.note-book-content').removeAttr('disabled');
						$(this).parent('.note-book').addClass('edit');
						$(this).addClass('save');
					}
				});

				$('.note-book').addClass('hide');
				$('.quote-book').addClass('hide');
		}, 1000);
		}
	})
}

// проверка страницы на закладку
function isMark(page){

	$.ajax({
		type: "GET",
		url: "/local/tools/viewer/bookmark_exist.php",
		data: "book_id="+book_id+"&page="+page,
		success: function(data){
			if (parseInt(data) >0 ){
				$('.bookmark-tools, .add-bookmark').addClass('active');
			}else{
				$('.bookmark-tools, .add-bookmark').removeClass('active');
			}
		}
	})

}

$(window).load(function () {
    var $preloader = $('.preloader');
    $preloader.fadeOut('slow');
});



$(document).ready(function(){
	var rightHeight = $('.main').height();
	var rightPanelHeight = parseInt(rightHeight) - 134;
	var searchPanelHeight = parseInt(rightHeight) - 203;
	$('.properties-book-content').css('max-height', rightPanelHeight);
	$('.miniatures-content').css('max-height', rightPanelHeight);
	$('.note-content').css('max-height', rightPanelHeight);
	$('.bookmark-content').css('max-height', rightPanelHeight);
	$('.quote-content').css('max-height', rightPanelHeight);
	$('.search_result').css('max-height', searchPanelHeight);

	$('#search_submit').click(function() {
		$(".search_result").removeClass('empty');
	});
});
