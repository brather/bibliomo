<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
global $APPLICATION;
$APPLICATION->ShowHead();
?>

<style>
    /* скрытие scrollbars */
    body{ overflow-x:hidden;overflow-y:hidden; }
</style>

<?if (!empty($arResult['publicurl'])) {?>

    <?$APPLICATION->IncludeComponent(
        "bitrix:player",
        ".default",
        Array(
            "PLAYER_TYPE" => "auto",
            "USE_PLAYLIST" => "N",
            "PATH" => $arResult['publicurl'],
            "PROVIDER" => "http",
            "WIDTH" => "640",
            "HEIGHT" => "480",
            "SKIN_PATH" => "/bitrix/components/bitrix/player/mediaplayer/skins",
            "CONTROLBAR" => "bottom",
            "WMODE" => "opaque",
            "LOGO_POSITION" => "none",
            "PLUGINS" => array(0=>"",1=>"",),
            "WMODE_WMV" => "window",
            "SHOW_CONTROLS" => "Y",
            "SHOW_DIGITS" => "Y",
            "CONTROLS_BGCOLOR" => "FFFFFF",
            "CONTROLS_COLOR" => "000000",
            "CONTROLS_OVER_COLOR" => "000000",
            "SCREEN_COLOR" => "000000",
            "AUTOSTART" => "N",
            "REPEAT" => "none",
            "VOLUME" => "90",
            "MUTE" => "N",
            "ADVANCED_MODE_SETTINGS" => "Y",
            "BUFFER_LENGTH" => "10",
            "DOWNLOAD_LINK_TARGET" => "_self",
            "ALLOW_SWF" => "N",
            "STREAMER" => "",
            "PREVIEW" => "",
            "FILE_TITLE" => "",
            "FILE_DURATION" => "",
            "FILE_AUTHOR" => "",
            "FILE_DATE" => "",
            "FILE_DESCRIPTION" => "",
            "PLAYER_ID" => "",
            "DOWNLOAD_LINK" => "",
            "ADDITIONAL_WMVVARS" => "",
            "SKIN" => "glow.zip",
            "LOGO" => "",
            "LOGO_LINK" => "",
            "ADDITIONAL_FLASHVARS" => ""
        )
    );?>

<?}?>