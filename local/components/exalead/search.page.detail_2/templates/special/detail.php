<?
	if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

	if(empty($arResult['BOOK']))
		return false;
	use \Bitrix\Main\Localization\Loc as Loc;
	Loc::loadMessages(__FILE__);		

	if((bool)$arParams['IS_SHOW_PROTECTED'] === false)
		include_once($_SERVER['DOCUMENT_ROOT'].'/local/include_areas/books_closed.php');
?>
<?$book = $arResult['BOOK']?>
<section class="innersection innerwrapper clearfix">
    <div class="b-bookpopup b-bookpage" >
        <div class="b-bookpopup_in bbox">
            <div class="b-bookframe iblock">
                <img alt="" class="b-bookframe_img" src="<?=$book['IMAGE_URL']?>">

            </div>
            <div class="b-onebookinfo iblock">
                <div class="b-bookhover">
                    <span class="b-autor"><?=$book['authorbook']?></span>
                    <div class="b-bookhover_tit black"><?=$book['title']?></div>
                    <p><?=$book['text']?></p>
                </div>
                <?if(!empty($arResult['VIEWER_URL']) && $arResult['PDF_AVAILABLE']):?>
                <div class="clearfix">
                    <a href="<?=$arResult['VIEWER_URL']?>" class="formbutton left">Читать</a>
                </div>
                <?endif;?>
                <div class="rel">
                    <ul class="b-resultbook-info">
                        <?if($book['countpages']):?><li><span>Количество страниц: </span><?=$book['countpages']?></li><?endif;?>
                        <?if($book['publishyear']):?><li><span>Год публикации:</span><?=$book['publishyear']?></li><?endif;?>
                        <?if($book['publisher']):?><li><span>Издательство:</span><?=$book['publisher']?></li><?endif;?>
                    </ul>
                    <div class="b-infobox b-descrinfo">
                        <?if($book['authorbook']):?>
                        <div class="b-infoboxitem">
                            <span class="tit iblock">Автор: </span>
                            <span class="iblock val"><?=$book['authorbook']?></span>
                        </div>
                        <?endif;?>
                        <?if($book['publishplace']):?>
                        <div class="b-infoboxitem">
                            <span class="tit iblock">Место издания: </span>
                            <span class="iblock val"><?=$book['publishplace']?></span>
                        </div>
                        <?endif;?>
                        <?if($book['isbn']):?>
                        <div class="b-infoboxitem">
                            <span class="tit iblock">ISBN:</span>
                            <span class="iblock val"><?=$book['isbn']?></span>
                        </div>
                        <?endif;?>
                        <?if($book['series']):?>
                        <div class="b-infoboxitem">
                            <span class="tit iblock">Серия: </span>
                            <span class="iblock val"><?=$book['series']?></span>
                        </div>
                        <?endif;?>
                        <?if($book['title']):?>
                        <div class="b-infoboxitem">
                            <span class="tit iblock">Заглавие: </span>
                            <span class="iblock val"><?=$book['title']?></span>
                        </div>
                        <?endif;?>
                    </div><!-- /b-infobox -->
                </div>
            </div>



        </div> 	<!-- /.bookpopup -->



</section>
