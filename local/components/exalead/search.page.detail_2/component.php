<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

	if(!CModule::IncludeModule('nota.exalead'))
		return false;
	use \Nota\Exalead\SearchQuery;
	use \Nota\Exalead\SearchClient;
	use Nota\Exalead\RslViewer;

	use Bitrix\NotaExt\Iblock\Element;
	use Bitrix\NotaExt\Iblock\IblockTools;

	CModule::IncludeModule("nota.userdata"); 
	use Nota\UserData\Books;
	use Nota\UserData\Bookmarks;
	use Nota\UserData\Quotes;
	use Nota\UserData\Notes;


	$arDefaultUrlTemplates404 = array(
		"list" => "/",
		"detail" => "#ELEMENT_ID#/"
	);
	
	$arDefaultVariableAliases404 = array();

	$arDefaultVariableAliases = array();

	$arComponentVariables = array("ELEMENT_ID");

	$SEF_FOLDER = "";
	$arUrlTemplates = array();
	
	if ($arParams["SEF_MODE"] == "Y")
	{
		$arVariables = array();

		$arUrlTemplates = CComponentEngine::MakeComponentUrlTemplates($arDefaultUrlTemplates404, $arParams["SEF_URL_TEMPLATES"]);
		$arVariableAliases = CComponentEngine::MakeComponentVariableAliases($arDefaultVariableAliases404, $arParams["VARIABLE_ALIASES"]);

		$componentPage = CComponentEngine::ParseComponentPath($arParams["SEF_FOLDER"], $arUrlTemplates, $arVariables);

		if (StrLen($componentPage) <= 0) $componentPage = "list";

		CComponentEngine::InitComponentVariables($componentPage, $arComponentVariables, $arVariableAliases, $arVariables);

		$SEF_FOLDER = $arParams["SEF_FOLDER"];

	} else {

		$arVariables = array();

		$arVariableAliases = CComponentEngine::MakeComponentVariableAliases($arDefaultVariableAliases, $arParams["VARIABLE_ALIASES"]);
		CComponentEngine::InitComponentVariables(false, $arComponentVariables, $arVariableAliases, $arVariables);

		$componentPage = "";
		if (!empty($arVariables["ELEMENT_ID"]) > 0) {
			$componentPage = "detail";
		} else {
			$componentPage = "list";
		}
	}
	
	
	$arResult = array(
		"FOLDER" => $SEF_FOLDER,
		"URL_TEMPLATES" => $arUrlTemplates,
		"VARIABLES" => $arVariables,
		"ALIASES" => $arVariableAliases
	);
	
	$RestartBuffer = false;

	if($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' or $componentPage == 'viewer')
		$arParams['RestartBuffer'] = true;

	
	if($componentPage == 'detail' or $componentPage == 'viewer')
	{
	
		$BOOK_ID = urldecode(trim($arResult['VARIABLES']['BOOK_ID']));
		$BOOK_ID = str_replace('!|', '/', $BOOK_ID);
		
		//$BOOK_ID = '005664_000048_RuPRLIB15062001';
		//$arResult['VARIABLES']['BOOK_ID'] = '005664_000048_RuPRLIB15062001';
		if(empty($BOOK_ID))
			return false;

		$query = new SearchQuery();
		$query->getById($BOOK_ID);

		$client = new SearchClient();
		$arResult['BOOK'] = $client->getResult($query);
		
		if(empty($arResult['BOOK']))
		{
			@define("ERROR_404", "Y");
			CHTTP::SetStatus("404 Not Found");
			return;
		}
		
		/*			
		Проверяем доступна ли книга для просмотра в пдф
		*/
		if($arResult['BOOK']['isprotected'] <=0 or nebUser::isShowProtectedContent())
		{
			#$isAvailable = \Nota\Exalead\RslViewer::isAvailable($BOOK_ID);
			$isAvailable = $arResult['BOOK']['IS_READ'] == 'Y';

			#if($isAvailable === true)
			#	$arResult['VIEWER_URL'] = str_replace('#BOOK_ID#', urlencode($BOOK_ID), $arResult['FOLDER'].$arResult['URL_TEMPLATES']['viewer']);
			$arResult['VIEWER_URL'] = $arResult['BOOK']['VIEWER_URL'];
			$arResult['PDF_AVAILABLE'] = $isAvailable;
		}
		
		$arParams['IS_SHOW_PROTECTED'] = nebUser::isShowProtectedContent();
		
		if(!empty($arResult['BOOK']['title']))
			$APPLICATION->SetTitle($arResult['BOOK']['title']);

		
		if($componentPage == 'detail' and !empty($arResult['BOOK']))
		{
			/*			
			Получаем ссылку на страницу библиотеки к которой привязана книга
			*/
			if((int)$arResult['BOOK']['idlibrary'] > 0){
				$arLib = Element::getList(array('IBLOCK_ID' => IblockTools::getIBlockId(IBLOCK_CODE_LIBRARY), 'PROPERTY_LIBRARY_LINK' => $arResult['BOOK']['idlibrary']), 1, array('DETAIL_PAGE_URL' , 'skip_other' => true));
				if(!empty($arLib['ITEM']))
					$arResult['BOOK']['LIBRARY_DETAIL_PAGE_URL'] = $arLib['ITEM']['DETAIL_PAGE_URL'];
			}

			\Nota\Exalead\Stat\BookView::set($BOOK_ID, $arResult['BOOK']['idlibrary']);
			if ($USER->IsAuthorized())
				$arResult['USER_BOOKS'] = Books::getListCurrentUser($BOOK_ID);
		}

		if($componentPage == 'viewer' and !empty($arResult['BOOK']))
		{
			// количество страниц
			$Viewer = new RslViewer($BOOK_ID);
			$arResult['BOOK']['pageCount'] = $Viewer->getPagesCount();
			list($arResult['BOOK']['width']) = explode("x",$Viewer->getGeometry(1));
			// закладки в книге
			$arResult['BOOK']['MARK'] = Bookmarks::getBookmarksListJSON($BOOK_ID);


			// цитаты 
			$arResult['BOOK']['QUOTES'] = Quotes::getListBook($BOOK_ID);
			// заметки
			$arResult['BOOK']['NOTE'] = Notes::getNotesList($BOOK_ID);

			if ($USER->IsAuthorized())
				$arResult['USER_BOOKS'] = Books::getListCurrentUser($BOOK_ID);
			\Nota\Exalead\Stat\BookRead::set($BOOK_ID);
		}
	}
	elseif ($componentPage == 'video')
	{
		$query = new SearchQuery('id:"' . $arResult['VARIABLES']['BOOK_ID'] . '"');
		$query->setParam('r', 'f/Source/videodata');

		$client = new SearchClient();
		$result = $client->getResult($query);

		if (is_array($result['ITEMS']) and !empty($result['ITEMS'][0]))
			$arResult['publicurl'] = $result['ITEMS'][0]['publicurl'];

		$arParams['RestartBuffer'] = true;
	}
	
	$arParams['RestartBuffer'] = true;

	if (!$arResult['PDF_AVAILABLE'] ){
			
				
				$componentPage = 'not_viewer';
				$arParams['RestartBuffer'] = false;
			}
	if($arParams['RestartBuffer'] === true)
		$APPLICATION->RestartBuffer();

	$this->IncludeComponentTemplate($componentPage);

	if($arParams['RestartBuffer'] === true)
		exit();
?>