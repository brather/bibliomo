<?
$MESS['NEW_LIST_TEMPLATE_NEW'] = 'New';
$MESS['NEW_LIST_TEMPLATE_ADDED'] = 'added';
$MESS['NEW_LIST_TEMPLATE_BOOK'] = 'books';
$MESS['NEW_LIST_TEMPLATE_NEXT'] = 'next 3';

$MESS['ADD_MY_LIB'] = '<span>Add</span> to my Library';
$MESS['REMOVE_MY_LIB'] = '<span>Remove</span> from my Library';

$MESS['NEW_LIST_TEMPLATE_AUTHOR'] = 'Author';
$MESS['NEW_LIST_TEMPLATE_SOURCE'] = 'Source';

$MESS['SAME_LIST_SIMILAR'] = 'Similar editions';

$MESS['SAME_LIST_AUTHOR'] = 'Author';
$MESS['SAME_LIST_NAME'] = 'Title';
$MESS['SAME_LIST_DATE'] = 'Date';
$MESS['SAME_LIST_LIB'] = 'Library';
$MESS['SAME_LIST_READ'] = 'Read';
