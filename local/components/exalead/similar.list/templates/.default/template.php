<?
	if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
		die();
	use \Bitrix\Main\Localization\Loc as Loc;
	Loc::loadMessages(__FILE__);
	
	if(empty($arResult['ITEMS']))
		return false;
		
#pre($arResult,1);

	if($_REQUEST['Ajax_instances'] != 'Y'){
	?>
	<div class="b-addbook_popuptit similar clearfix">
		<h2><?=Loc::getMessage('SAME_LIST_SIMILAR')?></h2>
	</div>
	<span class="content-instances similar">
		<?
		}
		else
			$APPLICATION->RestartBuffer();
	?>
	<table class="b-usertable tsize">
		<tr>
			<th><span><?=Loc::getMessage('SAME_LIST_AUTHOR')?></span></th>
			<th><span><?=Loc::getMessage('SAME_LIST_NAME')?></span></th>
			<th><span><?=Loc::getMessage('SAME_LIST_DATE')?></span></th>
			<th><span><?=Loc::getMessage('SAME_LIST_LIB')?></span></th>
			<th><span><?=Loc::getMessage('SAME_LIST_READ')?></span></th>
		</tr>
		<?
			foreach($arResult['ITEMS'] as $arElement)
			{
			?>
			<tr>
				<td class="pl15"><?=$arElement['AUTHOR']?></td>
				<td class="pl15"><a class="popup_opener ajax_opener coverlay" data-width="955" href="<?=$arElement['LINK']?>"><?=$arElement['TITLE']?></a></td>
				<td class="pl15"><?=$arElement['PUBLISHYEAR']?></td>
				<td><?=$arElement['LIBRARY']?></td>
				<td><?if(!empty($arElement['VIEWER_URL']) and $arElement['IS_READ'] == 'Y'){?><a href="<?=$arElement['VIEWER_URL']?>" target="_blank"><button type="submit" value="1" class="formbutton"><?=Loc::getMessage('SAME_LIST_READ')?></button></a><?}?></td>
			</tr>
			<?
			}
		?>
	</table>
	<span class="nav-instances similar">
		<?=$arResult['STR_NAV']?>
	</span>
	<?
		if($_REQUEST['Ajax_instances'] == 'Y')
			exit();
	?>

</span>
<script type="text/javascript">
	$(function() {
		DOCUMENT.on('click','.nav-instances.similar a', function(e){
			var href = $(this).attr('href');
			if(href == '#')
				return false;

			BX.showWait(this);

			$.get(href, { Ajax_instances: "Y" }, function( data ) {
				$('.content-instances.similar').html(data);
				BX.closeWait();
				$('.iblock').cleanWS();

				var destination = $('.b-addbook_popuptit.similar').offset().top;
				if ($.browser.safari) {
					$('body').animate({ scrollTop: destination }, 1100);
				} else {
					$('html').animate({ scrollTop: destination }, 1100);
				}
			});
			return false;
		});
	});
</script>				 