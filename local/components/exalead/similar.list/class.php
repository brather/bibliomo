<?php
	if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
		die();
	}

	use Bitrix\Main\Localization\Loc;
	CModule::IncludeModule('nota.exalead');
	use Nota\Exalead\SearchQuery;
	use Nota\Exalead\SearchClient;
	Loc::loadMessages(__FILE__);

	class ExaleadSimilarList extends CBitrixComponent
	{
		public function onPrepareComponentParams($arParams)
		{
			$arParams['ITEM_COUNT'] = 10;
			$arParams['PARENT_ID'] = trim($arParams['PARENT_ID']);
			return $arParams;
		}

		public function getResult()
		{
			if($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')
				$this->arResult['AJAX'] = true;

			$query = new SearchQuery('idparent:"'.$this->arParams['PARENT_ID'].'"');
			$query->setPageLimit(150);

			#$query->setPageLimit($this->arParams['ITEM_COUNT']);
			#$query->setNavParams();

			$client = new SearchClient();
			$result = $client->getResult($query);

			#$client->getDBResult($result);
			#$this->arResult['STR_NAV'] = $client->strNav; 
			#$this->arResult['COUNT'] = $result['COUNT'];

			if(!empty($result['ITEMS']))
			{	

				/*	  Сортируем массив по полю filesize		*/
				$result['ITEMS'] = nebSort::SortFieldTypeNum($result['ITEMS'], 'filesize');

				/* Формируем постраничку */
				$rs = new CDBResult;
				$rs->InitFromArray($result['ITEMS']);
				$rs->NavStart($this->arParams['ITEM_COUNT']);
				$this->arResult["STR_NAV"] = $rs->GetPageNavStringEx($navComponentObject, "", "");
				$this->arResult['NAV_COUNT'] = (int)$rs->NavRecordCount; 

				unset($result['ITEMS']);
				while($arRes = $rs->Fetch())
					$result['ITEMS'][] = $arRes;

				foreach($result['ITEMS'] as $arElement)
				{
					$this->arResult['ITEMS'][] = array(
						'ID' => $arElement['id'],
						'URL' => $arElement['URL'],
						'SOURCE' => $arElement['source'],
						'AUTHOR' => $arElement['authorbook'],
						'AUTHOR_LINK' => '/search/?f_field[authorbook]='.urlencode($arElement['r_authorbook']),
						'~TITLE' => TruncateText($arElement['title'], 90),
						'TITLE' => $arElement['title'],
						'LINK' => $arElement['DETAIL_PAGE_URL'],
						'IMAGE_URL' => $arElement['IMAGE_URL'],
						'VIEWER_URL' => $arElement['VIEWER_URL'],
						'LIBRARY' => $arElement['library'],
						'PUBLISHYEAR' => $arElement['publishyear'],
						'filesize' => $arElement['filesize'],
						'IS_READ' => $arElement['IS_READ'],
					);
				}
			}
		}

		public function executeComponent()
		{
			if(empty($this->arParams['PARENT_ID']))
				return false;

			CPageOption::SetOptionString("main", "nav_page_in_session", "N");
			$this->getResult();
			$this->includeComponentTemplate();
		}
	}

?>