<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use \Bitrix\Main\Localization\Loc as Loc;
Loc::loadMessages(__FILE__);
?>
<?
	if(!empty($arResult['GROUPS']))
	{
	?>
	<script type="text/javascript">
		$(function() {
			$(".b-sidenav input.checkbox").change(function() {
				var val = $(this).val();
				var strDopParam = $( "form#dop_filter" ).serialize();

				BX.showWait($(this).closest('li').attr('id'));

				$.get( "<?=$APPLICATION->GetCurPageParam('', array('f_field', 'dop_filter', 'PAGEN_1'));?>", strDopParam, function( data ) {
					$('#search_page_block').remove();
					$('section.mainsection.innerpage').after(data);
					BX.closeWait();
				});
			});

			<?
				if(!empty($_REQUEST['f_field']) and is_array($_REQUEST['f_field'])){
					foreach($_REQUEST['f_field'] as $v){
					?>
					var checkedDf = $(".b-sidenav input.checkbox[value='<?=$v?>']").closest('ul').show().prev().addClass('open');
					var checkedDf = $(".b-sidenav input.checkbox[value='<?=urlencode($v)?>']").closest('ul').show().prev().addClass('open');
					//checkedDf.closest('ul').show().prev().addClass('open');
					<?
					}
				}

				if($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')
				{
				?>
				$('input.checkbox:not(".custom")').replaceCheckBox();
				$('.b-libfilter').libFilter();
				
				$('#search_page_block input.checkbox').replaceCheckBox();
				$('.js_select').selectReplace();
				
				$('.b-sidenav').sideAccord(); // аккордеон менюшки сайдбара
				//$('.b-bookadd').favAdd();
				$('.iblock').cleanWS();
				 
				$('.js_filter').filterInit(); 
				$('.js_moreopen').opened();

				//$('.popup_opener').uipopup();
				
				checkIsAvailablePdf();
				
				$('.js_flex_bgimage').flexbgimage(); // https://yadi.sk/i/AUQFqp8ucFNRA
				<?
				}
			?>
		});
	</script>
	<form method="post" id="dop_filter">
		<input type="hidden" name="dop_filter" value="Y"/>
		<div class="b-sidenav <?=$arParams['sidenav_class']?>">
			<?
				if(!empty($arResult['GROUPS']['AUTHORBOOK'])){
				?>
				<a href="#" class="b-sidenav_title"><?=Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_RIGHT_AUTHORS');?></a>
				<ul class="b-sidenav_cont">
					<?
						foreach($arResult['GROUPS']['AUTHORBOOK'] as $k => $v)
						{
						?>
						<li class="clearfix<?=$k>4?' hidden':''?>" id="lia<?=$k?>">
							<div class="b-sidenav_value left"><?=$v['title']?></div>
							<div class="checkwrapper type2 right">
								<label for="AUTHORBOOK_<?=$k?>" class="black"><?=$v['count']?></label><input value="<?=urlencode($v['id'])?>" class="checkbox" type="checkbox" name="f_field[authorbook]" id="AUTHORBOOK_<?=$k?>"<?=urldecode($_REQUEST['f_field']['authorbook']) == $v['id']? ' checked="checked"' : ''?>/>
							</div>
						</li>
						<?
						}
						if(count($arResult['GROUPS']['AUTHORBOOK'])>5){
						?>
						<li><a href="#" class="b_sidenav_contmore js_moreopen">+ <?=Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_RIGHT_MORE');?></a></li>
						<?
						}
					?>
				</ul>
				<?
				}

				if(!empty($arResult['GROUPS']['YEARRANGE'])){
				?>
				<a href="#" class="b-sidenav_title"><?=Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_RIGHT_DATE');?></a>
				<ul class="b-sidenav_cont">
					<?
						function cmp($a, $b)
						{
							if(strpos($a, '-') !== false)
							{
								$arA = explode("-", $a);
								$a = $arA[0]; 
							}

							if(strpos($b, '-') !== false)
							{
								$arB = explode("-", $b);
								$b = $arB[0]; 
							}

							if ($a == $b) {
								return 0;
							}
							return ($a < $b) ? -1 : 1;
						}

						usort($arResult['GROUPS']['YEARRANGE'], "cmp");
						foreach($arResult['GROUPS']['YEARRANGE'] as $k => $v)
						{
						?>
						<li class="clearfix<?=$k>4?' hidden':''?>" id="liyear<?=$k?>">
							<div class="b-sidenav_value left"><?=$v['title']?></div>
							<div class="checkwrapper type2 right">
								<label for="YEARRANGE<?=$k?>" class="black"><?=$v['count']?></label><input value="<?=urlencode($v['id'])?>" class="checkbox" type="checkbox" name="f_field[yearrange]" id="YEARRANGE<?=$k?>"<?=urldecode($_REQUEST['f_field']['yearrange']) == $v['id']? ' checked="checked"' : ''?>/>
							</div>
						</li>
						<?
						}
						if(count($arResult['GROUPS']['YEARRANGE'])>5)
						{
						?>
						<li><a href="#" class="b_sidenav_contmore js_moreopen">+ <?=Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_RIGHT_MORE');?></a></li>
						<?
						}
					?>
				</ul>
				<?
				}

				if(!empty($arResult['GROUPS']['PUBLISHER']))
				{
				?>
				<a href="#" class="b-sidenav_title"><?=Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_RIGHT_PUBLISH');?></a>
				<ul class="b-sidenav_cont">
					<?
						foreach($arResult['GROUPS']['PUBLISHER'] as $k => $v)
						{
						?>
						<li class="clearfix<?=$k>4?' hidden':''?>" id="lip<?=$k?>">
							<div class="b-sidenav_value left"><?=$v['title']?></div>
							<div class="checkwrapper type2 right">
								<label for="PUBLISHER<?=$k?>" class="black"><?=$v['count']?></label><input value="<?=urlencode($v['id'])?>" class="checkbox" type="checkbox" name="f_field[publisher]" id="PUBLISHER<?=$k?>"<?=urldecode($_REQUEST['f_field']['publisher']) == $v['id']? ' checked="checked"' : ''?>/>
							</div>
						</li>
						<?
						}
						if(count($arResult['GROUPS']['PUBLISHER'])>5){
						?>
						<li><a href="#" class="b_sidenav_contmore js_moreopen">+ <?=Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_RIGHT_MORE');?></a></li>
						<?
						}
					?>
				</ul>
				<?
				}

				if(!empty($arResult['GROUPS']['LIBRARY']))
				{
				?>
				<a href="#" class="b-sidenav_title"><?=Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_RIGHT_LIBS');?></a>
				<ul class="b-sidenav_cont">
					<?
						foreach($arResult['GROUPS']['LIBRARY'] as $k => $v)
						{
						?>
						<li class="clearfix<?=$k>4?' hidden':''?>" id="lib<?=$k?>">
							<div class="b-sidenav_value left"><?=$v['title']?></div>
							<div class="checkwrapper type2 right">
								<label for="LIBRARY<?=$k?>" class="black"><?=$v['count']?></label><input value="<?=urlencode($v['id'])?>" class="checkbox" type="checkbox" name="f_field[library]" id="LIBRARY<?=$k?>"<?=urldecode($_REQUEST['f_field']['library']) == $v['id']? ' checked="checked"' : ''?>/>
							</div>
						</li>
						<?
						}
						if(count($arResult['GROUPS']['LIBRARY'])>5){
						?>
						<li><a href="#" class="b_sidenav_contmore js_moreopen">+ <?=Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_RIGHT_MORE');?></a></li>
						<?
						}
					?>
				</ul>
				<?
				}

				if(!empty($arResult['GROUPS']['PUBLISHPLACE']))
				{
				?>
				<a href="#" class="b-sidenav_title"><?=Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_RIGHT_PLACE');?></a>
				<ul class="b-sidenav_cont">
					<?
						foreach($arResult['GROUPS']['PUBLISHPLACE'] as $k => $v)
						{
						?>
						<li class="clearfix<?=$k>4?' hidden':''?>" id="lim<?=$k?>">
							<div class="b-sidenav_value left"><?=$v['title']?></div>
							<div class="checkwrapper type2 right">
								<label for="PUBLISHPLACE<?=$k?>" class="black"><?=$v['count']?></label><input value="<?=urlencode($v['id'])?>" class="checkbox" type="checkbox" name="f_field[publishplace]" id="PUBLISHPLACE<?=$k?>"<?=urldecode($_REQUEST['f_field']['publishplace']) == $v['id']? ' checked="checked"' : ''?>/>
							</div>
						</li>
						<?
						}
						if(count($arResult['GROUPS']['PUBLISHPLACE'])>5){
						?>
						<li><a href="#" class="b_sidenav_contmore js_moreopen">+ <?=Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_RIGHT_MORE');?></a></li>
						<?
						}
					?>
				</ul>
				<?
				}
			?>
		</div>
	</form>
	<?
	}
?>