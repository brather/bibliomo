<? require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php'); ?>

<?$APPLICATION->IncludeComponent(
	"exalead:library.new.list",
	".default",
	Array(
		'LIBRARY_ID' => intval($_REQUEST['library_id']),
		'DATE' => intval($_REQUEST['date']),
		'COUNT' => intval($_REQUEST['cnt']),
		'NEXT_BOOK' => intval($_REQUEST['next_book']),
		'PAGE_URL' => urldecode($_REQUEST['page_url']),
	)
);?>
			