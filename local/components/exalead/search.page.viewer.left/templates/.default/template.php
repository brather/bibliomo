<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use \Bitrix\Main\Localization\Loc as Loc;
Loc::loadMessages(__FILE__);
?>
<a name="nav_start"></a>
<div class="b-filter">
	<div class="b-filter_wrapper">
		<a href="#" class="sort sort_opener"><?=Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_LEFT_SORT');?></a>
		<span class="sort_wrap" align="center">
			<a <?=SortingExalead("document_authorsort")?>><?=Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_LEFT_SORT_BY_AUTHOR');?></a>
			<a <?=SortingExalead("document_titlesort")?>><?=Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_LEFT_SORT_BY_NAME');?></a>
			<!--a <?=SortingExalead("document_publishyearsort")?>><?=Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_LEFT_SORT_BY_DATE');?></a-->
		</span>
		<span class="b-filter_act" align="center">
			<!--a title="<?=$arParams['TITLE_MODE_BLOCK']?>" href="<?=$APPLICATION->GetCurPageParam("mode=block", array("mode", "dop_filter"));?>" class="b-filter_items<?=$arParams['MODE'] == 'block' ? ' current' : ''?>"></a-->
			<!--a title="<?=$arParams['TITLE_MODE_LIST']?>" href="<?=$APPLICATION->GetCurPageParam("mode=list", array("mode", "dop_filter"));?>" class="b-filter_list<?=$arParams['MODE'] == 'list' ? ' current' : ''?>"></a-->
			<span class="b-filter_show"><?=Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_LEFT_SORT_BY_SHOW');?></span>
			<a href="<?=$APPLICATION->GetCurPageParam("pagen=15", array("pagen", "dop_filter", 'longpage'));?>" class="b-filter_num b-filter_num_paging<?=($arParams['ITEM_COUNT'] == 15 and !$arParams['LONG_PAGE']) ? ' current' : ''?>">15</a>
			<a href="<?=$APPLICATION->GetCurPageParam("pagen=30", array("pagen", "dop_filter", 'longpage'));?>" class="b-filter_num b-filter_num_paging<?=$arParams['ITEM_COUNT'] == 30 ? ' current' : ''?>">30</a>
			<a href="<?=$APPLICATION->GetCurPageParam("pagen=45", array("pagen", "dop_filter", 'longpage'));?>" class="b-filter_num b-filter_num_paging<?=$arParams['ITEM_COUNT'] == 45 ? ' current' : ''?>">45</a>
			<?
				if(false && !empty($arParams['SHOW_LONG_PAGE'])){
				?>
				<a title="<?=Loc::getMessage('LIB_SEARCH_PAGE_VIEWER_LEFT_SHOW_ALL');?>" href="<?=$APPLICATION->GetCurPageParam("pagen=15&longpage=y", array("pagen", "dop_filter"));?>" class="b-filter_num <?=$arParams['LONG_PAGE'] ? 'current' : ''?>">&hellip;</a>
				<?
				}
			?>
		</span>
	</div>
</div><!-- /.b-filter -->
<?
	/*		
	представление в виде блоков 
	*/
	if($arParams['MODE'] == 'block')
	{
		include_once('view_block.php');
	}
	/*
	представление в виде списка
	*/
	else
	{
		//include_once('view_list.php');
		include_once('view_list_nocovers.php');
	}
?>