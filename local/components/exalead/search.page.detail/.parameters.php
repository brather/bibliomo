<?
	if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

	$arComponentParameters = array(
		"GROUPS" => array(
		),
		"PARAMETERS" => array(
			"VARIABLE_ALIASES" => array(
				"ELEMENT_ID" => array(
					"NAME" => "ELEMENT_ID",
					"DEFAULT" => "ELEMENT_ID",
				),
			),
			"SEF_MODE" => array(
				"list" => array(
					"NAME" => "LIST PAGE",
					"DEFAULT" => "",
					"VARIABLES" => array()
				),
				"detail" => array(
					"NAME" => "DETAIL PAGE",
					"DEFAULT" => "#ELEMENT_ID#/",
					"VARIABLES" => array("ELEMENT_ID")
				),
				"viewer" => array(
					"NAME" => "VIEWER PAGE",
					"DEFAULT" => "#ELEMENT_ID#/viewer/",
					"VARIABLES" => array("ELEMENT_ID")
				),
                "video" => array(
                    "NAME" => "VIDEO PAGE",
                    "DEFAULT" => "#ELEMENT_ID#/video/",
                    "VARIABLES" => array()
                )
			),
		),
	);
?>