<?
    $MESS['LIB_SEARCH_PAGE_DETAIL_ADD_TO_LIB'] = '<span>Добавить</span> в Мою библиотеку';
    $MESS['LIB_SEARCH_PAGE_DETAIL_REMOVE_FROM_LIB'] = '<span>Удалить</span> из Моей библиотеки';

    $MESS['LIB_SEARCH_PAGE_DETAIL_PAGES'] = 'Pages';
    $MESS['LIB_SEARCH_PAGE_DETAIL_YEAR'] = 'Year of publication';
    $MESS['LIB_SEARCH_PAGE_DETAIL_PUBLISH'] = 'Publisher';
    $MESS['LIB_SEARCH_PAGE_DETAIL_BOOK_INFO'] = 'complete book information';
    $MESS['LIB_SEARCH_PAGE_DETAIL_AUTHOR'] = 'Author';
    $MESS['LIB_SEARCH_PAGE_DETAIL_TITLE'] = 'Title';
    $MESS['LIB_SEARCH_PAGE_DETAIL_LIB'] = 'Library';
    $MESS['LIB_SEARCH_PAGE_DETAIL_SHARE'] = 'Share with friends';
    $MESS['LIB_SEARCH_PAGE_DETAIL_FIND_BOOKS'] = 'find similar books on the topic';
    $MESS['LIB_SEARCH_PAGE_DETAIL_SIMILAR_BOOKS'] = 'SIMILAR PUBLICATIONS';
    $MESS['LIB_SEARCH_PAGE_DETAIL_SAME_BOOKS'] = 'дублирующиеся издания';

    $MESS['LIB_SEARCH_PAGE_DETAIL_SERIE'] = 'Chapter';
    $MESS['LIB_SEARCH_PAGE_DETAIL_EDITION'] = 'Edition';
    $MESS['LIB_SEARCH_PAGE_DETAIL_PUBLISH_PLACE'] = 'Publish place';
    $MESS['LIB_SEARCH_PAGE_DETAIL_ANNOTATION'] = 'Annotation';
    $MESS['LIB_SEARCH_PAGE_DETAIL_VIEW'] = 'View';
	$MESS['LIB_SEARCH_PAGE_DETAIL_BBK'] = 'BBK';
	$MESS['LIB_SEARCH_PAGE_DETAIL_UDC'] = 'UDC';
	$MESS['LIB_SEARCH_PAGE_DETAIL_NOTE_CONTENT'] = 'Note content';

    $MESS['LIB_SEARCH_PAGE_DETAIL_BOOK_NOTE'] = 'Note';

    $MESS['LIB_SEARCH_PAGE_CLOSE_WINDOW'] = 'Close window';
?>