<?
	$MESS['LIB_SEARCH_PAGE_DETAIL_ADD_TO_LIB'] = '<span>Добавить</span> в Мою библиотеку';
	$MESS['LIB_SEARCH_PAGE_DETAIL_REMOVE_FROM_LIB'] = '<span>Удалить</span> из Моей библиотеки';

	$MESS['LIB_SEARCH_PAGE_DETAIL_PAGES'] = 'Количество страниц';
	$MESS['LIB_SEARCH_PAGE_DETAIL_YEAR'] = 'Год издания';
	$MESS['LIB_SEARCH_PAGE_DETAIL_PUBLISH'] = 'Издательство';
	$MESS['LIB_SEARCH_PAGE_DETAIL_BOOK_INFO'] = 'полная информация о книге';
	$MESS['LIB_SEARCH_PAGE_DETAIL_AUTHOR'] = 'Автор';
	$MESS['LIB_SEARCH_PAGE_DETAIL_TITLE'] = 'Заглавие';
	$MESS['LIB_SEARCH_PAGE_DETAIL_LIB'] = 'Библиотека';
	$MESS['LIB_SEARCH_PAGE_DETAIL_SHARE'] = 'Поделиться с друзьями';
	$MESS['LIB_SEARCH_PAGE_DETAIL_FIND_BOOKS'] = 'найти похожие книги по тематике';
	$MESS['LIB_SEARCH_PAGE_DETAIL_SIMILAR_BOOKS'] = 'похожие издания';
	$MESS['LIB_SEARCH_PAGE_DETAIL_SAME_BOOKS'] = 'дублирующиеся издания';

	$MESS['LIB_SEARCH_PAGE_DETAIL_SERIE'] = 'Серия';
	$MESS['LIB_SEARCH_PAGE_DETAIL_EDITION'] = 'Редакция';
	$MESS['LIB_SEARCH_PAGE_DETAIL_PUBLISH_PLACE'] = 'Место издания';
	$MESS['LIB_SEARCH_PAGE_DETAIL_ANNOTATION'] = 'Аннотация';
	$MESS['LIB_SEARCH_PAGE_DETAIL_VIEW'] = 'Читать';
	$MESS['LIB_SEARCH_PAGE_DETAIL_BBK'] = 'ББК';
	$MESS['LIB_SEARCH_PAGE_DETAIL_UDC'] = 'УДК';
	$MESS['LIB_SEARCH_PAGE_DETAIL_NOTE_CONTENT'] = 'Примечание содержания';

	$MESS['LIB_SEARCH_PAGE_DETAIL_BOOK_NOTE'] = 'Общее примечание';

	$MESS['LIB_SEARCH_PAGE_CLOSE_WINDOW'] = 'Закрыть окно';
?>