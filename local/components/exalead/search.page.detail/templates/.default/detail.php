<?
	if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

	if(empty($arResult['BOOK']))
		return false;
	use \Bitrix\Main\Localization\Loc as Loc;
	Loc::loadMessages(__FILE__);		

	if($arParams['RestartBuffer'] === true)
	{
	?>
	<div class="b-bookpopup popup" >
		<a href="#" class="closepopup"><?=Loc::getMessage('LIB_SEARCH_PAGE_CLOSE_WINDOW')?></a>
		<?
		}
		else
		{
		
			if((bool)$arParams['IS_SHOW_PROTECTED'] === false)
				include_once($_SERVER['DOCUMENT_ROOT'].'/local/include_areas/books_closed.php');

		?>
		<div class="b-book_bg">
			<section class="innersection innerwrapper clearfix">
			<div class="b-bookpopup b-bookpage" >
				<?
				}
			?>
			<?
				/* Start Тело карточки книги*/
			?>
			<div class="b-bookpopup_in bbox">
				<?/*?><div class="b-bookframe iblock">
					<img alt="" class="b-bookframe_img" src="<?=$arResult['BOOK']['IMAGE_URL']?>&width=262&height=408">
				</div>
				<?*/?>
                <?if($arParams['RestartBuffer'] !== true):?>
                    <div class="rel clearfix"><a href="javascript:;" onclick="window.close();"class="bookclose right"><?=Loc::getMessage('LIB_SEARCH_PAGE_CLOSE_WINDOW')?></a></div>
                <?endif;?>
				<div class="b-onebookinfo iblock">
					<div class="b-bookhover">
						<?
							if(collectionUser::isAdd())
							{
								if(!empty($arResult['USER_BOOKS']))
								{
								?>
								<div class="meta minus">
									<div class="b-hint rel"><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_REMOVE_FROM_LIB')?></div>
									<a href="#" data-normitem="Y" data-url="<?=ADD_COLLECTION_URL?>removeBook.php?id=<?=urlencode($arResult['BOOK']['id'])?>" data-collection="<?=ADD_COLLECTION_URL?>list.php?t=books&id=<?=urlencode($arResult['BOOK']['id'])?>" class="b-bookadd fav"></a>
								</div>
								<?
								}
								else
								{
								?>
								<div class="meta">
									<div class="b-hint rel"><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_ADD_TO_LIB')?></div>
									<a href="#" data-normitem="Y" data-collection="<?=ADD_COLLECTION_URL?>list.php?t=books&id=<?=urlencode($arResult['BOOK']['id'])?>" data-url="<?=ADD_COLLECTION_URL?>removeBook.php?id=<?=urlencode($arResult['BOOK']['id'])?>" class="b-bookadd"></a>
								</div>
								<?
								}
							}

							if(!empty($arResult['BOOK']['authorbook'])){

							?>
							<span class="b-autor"><a href="/search/?f_field[authorbook]=<?=urlencode($arResult['BOOK']['r_authorbook'])?>" class="lite"><?=$arResult['BOOK']['authorbook']?></a></span>
							<?
							}
						?>
						<div class="b-bookhover_tit black"><?=$arResult['BOOK']['title']?></div>
						<?/*<p><?=$arResult['BOOK']['text']?></p>*/?>
					</div>

					<div class="clearfix">
						<?
							if($arResult['BOOK']['isprotected'] > 0 and !empty($arResult['BOOK']['PROTECTED_URL']))
							{?>
								<div class="private">
									<span class="b-result-type_txt closed_book">Закрытое издание, доступно только в библиотеке</span>
								</div>
								<?/*
								if((bool)$arParams['IS_SHOW_PROTECTED'] === true)
								{
								?>
								<a href="<?//=$arResult['BOOK']['PROTECTED_URL']?>" target="_blank"><button type="button" value="1" class="formbutton left"><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_VIEW')?></button></a>
								<?
								}
								else
								{
								?>
								<a data-link="<?=$arResult['BOOK']['PROTECTED_URL']?>" href="#" target="_blank" class="aformbutton closed_book"><button type="button" value="1" class="formbutton left"><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_VIEW')?></button></a>
								<?
								}*/
							}elseif(!empty($arResult['VIEWER_URL']) && $arResult['PDF_AVAILABLE'])
							{
							?>
							<a href="<?=$arResult['VIEWER_URL']?>" target="_blank"><button type="button" value="1" class="formbutton left"><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_VIEW')?></button></a>
							<?
							}
						?>	
						<?
						  /*	
							if(!empty($arResult['BOOK']['fileExt']) && $arResult['PDF_AVAILABLE'])
							{
							?>						
							<div class="b-result-type right">
								<?
									foreach($arResult['BOOK']['fileExt'] as $ext)
									{
									?>
									<span class="b-result-type_txt"><?=$ext?></span>
									<?
									}
								?>
							</div>
							<?
							}
							*/
						?>
					</div>
					<?/*if (!$arResult['PDF_AVAILABLE']) {
						$APPLICATION->IncludeComponent("neb:books.digital.query", "",
						Array(
						'EXALEAD_BOOK_ID' => $arResult['BOOK']['id'],
						'PDF_AVAILABLE' => $arResult['PDF_AVAILABLE']
						),
						false
						);
					}*/?>

					<div class="rel">	
						<ul class="b-resultbook-info">
							<?
								if(!empty($arResult['BOOK']['countpages']))
								{
								?>
								<li><span><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_PAGES')?>: </span> <?=$arResult['BOOK']['countpages']?> </li>
								<?
								}
							?>
							<?
								if(!empty($arResult['BOOK']['publishyear']))
								{
								?>
								<li><span><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_YEAR')?>:</span> <a target="_parent" href="/search/?f_publishyear=<?=$arResult['BOOK']['publishyear']?>"><?=$arResult['BOOK']['publishyear']?></a></li>
								<?
								}
							?>
							<?
								if(!empty($arResult['BOOK']['publisher'])){
								?>
								<li><span><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_PUBLISH')?>:</span> <a target="_parent" href="/search/?f_field[publisher]=<?=$arResult['BOOK']['r_publisher']?>"><?=$arResult['BOOK']['publisher']?></a></li>
								<?
								}
							?>
						</ul>
						<?

						?>
						<div class="b-infobox b-descrinfo"  data-link="descr">
							<?
								if(!empty($arResult['BOOK']['authorbook'])){
								?>						
								<div class="b-infoboxitem">
									<span class="tit iblock"><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_AUTHOR')?>: </span>
									<span class="iblock val"><?=$arResult['BOOK']['authorbook']?></span>
								</div>
								<?
								}
							?>
							<div class="b-infoboxitem">
								<span class="tit iblock"><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_TITLE')?>: </span>
								<span class="iblock val"><?=$arResult['BOOK']['title']?></span>
							</div>
							<?
								if(!empty($arResult['BOOK']['series'])){
								?>	
								<div class="b-infoboxitem">
									<span class="tit iblock"><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_SERIE')?>: </span>
									<span class="iblock val"><?=$arResult['BOOK']['series']?></span>
								</div>
								<?
								}
							?>
							<?
								if(!empty($arResult['BOOK']['bbk'])){
								?>	
								<div class="b-infoboxitem">
									<span class="tit iblock"><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_BBK')?>: </span>
									<span class="iblock val"><?=$arResult['BOOK']['bbk']?></span>
								</div>
								<?
								}
							?>
							<?
								if(!empty($arResult['BOOK']['contentnotes'])){
								?>	
								<div class="b-infoboxitem">
									<span class="tit iblock"><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_NOTE_CONTENT')?>: </span>
									<span class="iblock val"><?=$arResult['BOOK']['contentnotes']?></span>
								</div>
								<?
								}
							?>
							<?
								if(!empty($arResult['BOOK']['edition'])){
								?>	
								<div class="b-infoboxitem">
									<span class="tit iblock"><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_EDITION')?>: </span>
									<span class="iblock val"><?=$arResult['BOOK']['edition']?></span>
								</div>
								<?
								}
							?>
							<?
								if(!empty($arResult['BOOK']['publishplace'])){
								?>	
								<div class="b-infoboxitem">
									<span class="tit iblock"><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_PUBLISH_PLACE')?>: </span>
									<span class="iblock val"><?=$arResult['BOOK']['publishplace']?></span>
								</div>
								<?
								}
							?>
							<?
								if(!empty($arResult['BOOK']['isbn'])){
								?>	
								<div class="b-infoboxitem">
									<span class="tit iblock">ISBN: </span>
									<span class="iblock val"><?=$arResult['BOOK']['isbn']?></span>
								</div>
								<?
								}
							?>
							<?
								if(!empty($arResult['BOOK']['issn'])){
								?>	
								<div class="b-infoboxitem">
									<span class="tit iblock">ISSN: </span>
									<span class="iblock val"><?=$arResult['BOOK']['issn']?></span>
								</div>
								<?
								}
							?>
							<?
								if(!empty($arResult['BOOK']['notes'])){
								?>	
								<div class="b-infoboxitem">
									<span class="tit iblock"><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_BOOK_NOTE')?>: </span>
									<span class="iblock val"><?=$arResult['BOOK']['notes']?></span>
								</div>
								<?
								}
							?>
							<?
								if(!empty($arResult['BOOK']['udk'])){
								?>	
								<div class="b-infoboxitem">
									<span class="tit iblock"><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_UDC')?>: </span>
									<span class="iblock val"><?=$arResult['BOOK']['udk']?></span>
								</div>
								<?
								}
							?>
							<?
								if(!empty($arResult['BOOK']['placepublish'])){
								?>	
								<div class="b-infoboxitem">
									<span class="tit iblock"><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_PUBLISH_PLACE')?>: </span>
									<span class="iblock val"><?=$arResult['BOOK']['placepublish']?></span>
								</div>
								<?
								}
							?>
							<?
								if(!empty($arResult['BOOK']['annotation'])){
								?>	
								<div class="b-infoboxitem">
									<span class="tit iblock"><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_ANNOTATION')?>: </span>
									<span class="iblock val"><?=$arResult['BOOK']['annotation']?></span>
								</div>
								<?
								}
							?>
							<?
								if(!empty($arResult['BOOK']['library'])){
								?>
								<div class="b-infoboxitem">
									<span class="tit iblock"><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_LIB')?>: </span>
									<span class="iblock val">
										<?
											if(!empty($arResult['BOOK']['LIBRARY_DETAIL_PAGE_URL']))
											{
											?>
											<a target="_blank" href="<?=$arResult['BOOK']['LIBRARY_DETAIL_PAGE_URL']?>"><?=$arResult['BOOK']['library']?></a>
											<?
											}
											else
											{
											?>
											<?=$arResult['BOOK']['library']?>
											<?
											}
										?>
									</span>
								</div>
								<?
								}
							?>

						</div><!-- /b-infobox -->
					</div>

					<div class="b-line_social">
						<h5><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_SHARE')?></h5>
						<?$APPLICATION->IncludeFile("/local/include_areas/social_button.php", Array(), Array(
								"MODE"      => "text",                                           
								"NAME"      => "Социальные кнопки",
							));?>
					</div>
					<?
						#if ($arResult['PDF_AVAILABLE']) {
						$APPLICATION->IncludeComponent("neb:books.digital.query", "",
							Array(
								'EXALEAD_BOOK_ID' => $arResult['BOOK']['id'],
								#'PDF_AVAILABLE' => $arResult['PDF_AVAILABLE']
								'PDF_AVAILABLE' => true
							),
							false
						);
						#}
					?>
				</div>
				<?/*
					$APPLICATION->IncludeComponent(
						"collection:in.collection",
						"",
						Array(
							"IBLOCK_TYPE" => "collections",
							"IBLOCK_ID" => "6",
							"BOOK_ID" => $arResult['BOOK']['id']
						)
					);*/
				?>

				<?/*if($arResult['BOOK']['idparent']):?>
					<?
						$APPLICATION->IncludeComponent(
							"exalead:similar.list",
							"",
							Array(
								"PARENT_ID" => $arResult['BOOK']['idparent']
							)
						);
					?>
					<?endif;*/?>

				<?/*if($arResult['BOOK']['idparent2']):?>
					<?
						$APPLICATION->IncludeComponent(
							"exalead:same.list",
							"",
							Array(
								"PARENT_ID" => $arResult['BOOK']['idparent2']
							)
						);
					?>
					<?endif;*/?>
			</div>

			<?
				/* End Тело карточки книги*/
			?>

			<?
				if($arParams['RestartBuffer'] === true)
				{
				?>		
			</div> 	<!-- /.bookpopup -->
			<?
			}
			else
			{
			?>
		</div> 	<!-- /.bookpopup -->
		</section>
	</div>
	<?
	}
?>
