<?
	if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
	
	$book_id = urldecode($arResult['VARIABLES']['BOOK_ID']);
	#$book_id = '01003741195';
	if(empty($book_id))
		return false;

	#$isAvailable = \Nota\Exalead\RslViewer::isAvailable($book_id);
	if($arResult['PDF_AVAILABLE'] === false){
		ShowError("Данный материал недоступен для просмотра.");
		return false;
	}

?><!DOCTYPE html>
<html dir="ltr" mozdisallowselectionprint moznomarginboxes>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width">
		<meta name="viewport" content="width=980">		
		<meta name="google" content="notranslate">
		<title><?=$arResult['BOOK']['title']?></title>
		<link rel="stylesheet" href="/bitrix/templates/template_footer/styles.css" type="text/css" media="screen">
		<script type="text/javascript" src="/local/templates/.default/markup/js/libs/jquery.min.js"></script>
		
		<script type="text/javascript">
		$(document).ready(function(){
			$('.navigation span, .navigation div, .next-page, .prev-page, .thumb img').click(function(){
			
				var p = parseInt($(this).attr('p'));
				
				if( p > 0){
					// получение страницы
					getImage(p);
					var prev = 1;
					var center = 2;
					var next = 3
					
					$('.navigation span.active').removeClass('active');
					$('.prev-page').removeClass('noactive');
					$('.next-page').removeClass('noactive');
					switch(p){
						// первая страница
						case 1:
							$('.navigation span.prev').addClass('active');
							$('.prev-page').addClass('noactive');
							$('.prev-page').attr('p',1);
							$('.next-page').attr('p',2);
							break;
							
						// последняя страница
						case <?=$arResult['BOOK']['pageCount']?>:
							prev = p-2;
							center = p-1;
							next = p;
							$('.navigation span.next').addClass('active');
							$('.next-page').addClass('noactive');
							$('.next-page').attr('p',next);
							$('.prev-page').attr('p',center);
							break;
							
						default:
							$('.navigation span.prev').next('span').addClass('active');
							prev = p-1;
							center = p;
							next = p+1;
							$('.next-page').attr('p',next);
							$('.prev-page').attr('p',prev);
							
							
					}
					$('.navigation span.prev').html(prev);
					$('.navigation span.prev').attr('p', prev);
					$('.navigation span.prev').next('span').html(center);
					$('.navigation span.prev').next('span').attr('p', center);
					$('.navigation span.next').html(next);
					$('.navigation span.next').attr('p', next);
				}
			
			});
		
		
		});
		
		
		</script>
	</head>

	<body >
<div class="page-book">
	<div class="sidebar">
		<div class="sidebar-header">
			<div class="sidebar-tools">
				<span class="document-options" title="Свойства документа"></span>
				<span class="turn-page" title="Повернуть страницу"></span>
				<span class="hand" title="Инструмент перемещения"></span>
				<span class="scale" title="Масштаб"></span>
				<span class="search" title="Поиск по тексту"></span>
			</div>
		</div>
		<div class="sidebar-content">
			<div class="section">
				<h2>Содержание</h2>
				<div class='thumb'>
					<?for ($i=1;$i<=$arResult['BOOK']['pageCount'];$i++):?>
					<img src='/local/tools/exalead/thumbnails.php?book_id=<?=$arResult['BOOK']['URL']?>&p=<?=$i?>' p='<?=$i?>'>
					<?endfor;?>
				</div>
			</div>
			<div class="section">
				<h2>Закладки</h2>
				<div>
					Закладки
				</div>
			</div>
			<div class="section">
				<h2>Цитаты</h2>
				<div>
					Цитаты
				</div>
			</div>
			<div class="section">
				<h2>Заметки</h2>
				<div>
					Заметки
				</div>
			</div>
			<div class="section">
				<h2>Поиск по тексту</h2>
				<div>
					Поиск по тексту
				</div>
			</div>
		</div>
	</div>
	<div class="main">
		<div class="header">
			<a href="/" class="logo"></a>
			<div class="book-info">
				<span class="book-title"><?=$arResult['BOOK']['title']?></span>
			</div>
			<div class="add_to_lk" title="Добавить в личный кабинет"></div>
		</div>
		<div class="content">
			<div class="navigation">
				<div p=1>В начало</div>
				<span class="prev active" p=1>1</span>
				<span p=2>2</span>
				<span class='next' p=3>3</span>
				<div  p=<?=$arResult['BOOK']['pageCount']?>>В конец</div>
			</div>
			<div class="page-view">
				<span class="page-number" style='display:none;'>1</span>
				<div class="tools">
					<span class="scale"></span>
					<span class="bookmark"></span>
				</div>
				<div class="view" id="view">
					<img id='image'>
					<script type="text/javascript">
					function getImage(page){
						$('#image').attr("src", "/local/tools/exalead/getImages.php?book_id=<?=$arResult['BOOK']['URL']?>&p="+page+"");
					}
					

					getImage(1);
				</script>
					<div id="infoTop" style='display:none;'></div>
					<div id="infoLeft" style='display:none;'></div>
					<div id="infoWidth" style='display:none;'></div>
					<div id="infoHeight" style='display:none;'></div>
					<div id="infoBottom" style='display:none;'></div>
<script>
function Croppable(options) {
  var self = this;

  var elem = options.elem;

  var cropArea;
  var cropStartX, cropStartY;

  elem.on('selectstart dragstart', false)
   .on('mousedown', onImgMouseDown);

  function initCropArea(pageX, pageY) {
    cropArea = $('<div class="crop-area"/>')
      .appendTo('.page-book');

    cropStartX = pageX;
    cropStartY = pageY;
  }

  function onDocumentMouseMove(e) {
    drawCropArea(e.pageX, e.pageY);
  }

  function onDocumentMouseUp(e) {
    endCrop(e.pageX, e.pageY);

    cropArea.remove();
    $(document).off('.croppable');
  }

  function onImgMouseDown(e) {
    initCropArea(e.pageX, e.pageY);

    $(document).on({
      'mousemove.croppable': onDocumentMouseMove,
      'mouseup.croppable': onDocumentMouseUp
    });
   
    return false;
  };

  function drawCropArea(pageX, pageY) {
    var dims = getCropDimensions(pageX, pageY);

    cropArea.css(dims);

    // вычитаем 2, т.к. ширина будет дополнена рамкой
    // если не вычесть, то рамка может вылезти за изображение
    cropArea.css({
      height: Math.max(dims.height-2, 0),
      width: Math.max(dims.width-2,0)
    });

    // здесь мы просто рисуем полупрозрачный квадрат
    // альтернативный подход - накладывать на каждую часть изображения div'ы с opacity и черным цветом, чтобы только кроп был ярким
  }

  function endCrop(pageX, pageY) {
    var dims = getCropDimensions(pageX, pageY);

    var coords = elem.offset();
    // получить координаты относительно изображения
    dims.left -= coords.left;
    dims.top -= coords.top;

    $(self).triggerHandler($.extend({ type: "crop" }, dims));
  }

  function getCropDimensions(pageX, pageY) {
    // 1. Делаем квадрат из координат нажатия и текущих
    // 2. Ограничиваем размерами img, если мышь за его пределами

    var left = Math.min(cropStartX, pageX);
    var right = Math.max(cropStartX, pageX);
    var top = Math.min(cropStartY, pageY);
    var bottom = Math.max(cropStartY, pageY);

    var coords = elem.offset();

    left = Math.max(left, coords.left);
    top = Math.max(top, coords.top);
    right = Math.min(right, coords.left + elem.outerWidth());
    bottom = Math.min(bottom, coords.top + elem.outerHeight());

    return { left: left, top: top, width: right-left, height: bottom-top };
  }

}

var croppable = new Croppable({
  elem: $('#view')
});

$(croppable).on("crop", function(event) {
  // вывести координаты и размеры crop-квадрата относительно изображения
 
  
  var strTop = "";
  $(['top']).each(function() {
    strTop += event[this];
  });

  $('#infoTop').html("Top: " + strTop);
  
   var strLeft = "";
  $(['left']).each(function() {
    strLeft += event[this];
  });

  $('#infoLeft').html("Left: " + strLeft);
  
   var strWidth = "";
  $(['width']).each(function() {
    strWidth += event[this];
  });

  $('#infoWidth').html("Width: " + strWidth);
  
   var strHeight = "";
  $(['height']).each(function() {
    strHeight += event[this];
  });

  $('#infoHeight').html("Height: " + strHeight);
  
  var strBottom = +strTop + +strHeight;
  $('#infoBottom').html('Bottom: ' + strBottom);
  
});

</script>
<div id='page-preloader'>
	<div class="spinner">
	</div>
</div>
				</div>
				<div class="prev-page" p=0></div>
				<div class="next-page" p=2></div>
			</div>
		<!--</div>-->
		<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
	</div>
</div>
<script>
	$('.section>h2').click(function() {
		if ( !$(this).parent().hasClass('active') ) {
			$('.sidebar .section').removeClass('active');
			$('.sidebar .section div').slideUp(900);
			$(this).siblings('div').slideDown(900);
			$(this).parent().addClass('active');
		} else {
			$(this).siblings('div').slideUp(900);
			$(this).parent().removeClass('active');
		}
	});
	
	$(window).load(function () {
    var $preloader = $('#page-preloader'),
        $spinner   = $preloader.find('.spinner');
    $spinner.fadeOut();
    $preloader.delay(350).fadeOut('slow');
});
</script>

</body>
</html>