<?
	$MESS['NEW_LIST_TEMPLATE_NEW'] = 'новые поступления';
	$MESS['NEW_LIST_TEMPLATE_ADDED'] = 'добавлено';
	$MESS['NEW_LIST_TEMPLATE_BOOK'] = 'книги';
	$MESS['NEW_LIST_TEMPLATE_NEXT'] = 'следующие 3';
	$MESS['ADD_MY_LIB'] = 'Добавить в Мою библиотеку';
	$MESS['ADD_MY_LIB'] = '<span>Добавить</span> в Мою библиотеку';
	$MESS['REMOVE_MY_LIB'] = '<span>Удалить</span> из Моей библиотеки';

	$MESS['NEW_LIST_TEMPLATE_AUTHOR'] = 'Автор';
	$MESS['NEW_LIST_TEMPLATE_SOURCE'] = 'Источник';

    $MESS['SAME_LIST_COPIES'] = 'Экземпляры';
    $MESS['SAME_LIST_AUTHOR'] = 'Автор';
    $MESS['SAME_LIST_NAME'] = 'Название';
    $MESS['SAME_LIST_DATE'] = 'Дата публикации';
    $MESS['SAME_LIST_LIB'] = 'Библиотека';
    $MESS['SAME_LIST_READ'] = 'Читать';