<?php
	if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
		die();
	}

	use Bitrix\Main\Localization\Loc;
	CModule::IncludeModule('nota.exalead');
	use Nota\Exalead\SearchQuery;
	use Nota\Exalead\SearchClient;
	Loc::loadMessages(__FILE__);

	class ExaleadSameList extends CBitrixComponent
	{
		public function onPrepareComponentParams($arParams)
		{

			$arParams['ITEM_COUNT'] = 10;
			$arParams['PARENT_ID'] = trim($arParams['PARENT_ID']);
			#$arParams['PARENT_ID'] = trim('002072_000044_ARONB-RU_Архангельская ОНБ_DOLIB_31.16-08я22_Я 996-460828 002072_000044_ARONB-RU_Архангельская ОНБ_DOLIB_31.16-08я22_Я 996-460828');
			return $arParams;
		}

		public function getResult()
		{
			if($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')
				$this->arResult['AJAX'] = true;

			/*				
			Нужно вывести сначала книги у которых filesize >0 а потом остальные
			*/

			$query = new SearchQuery('idparent2:('.$this->arParams['PARENT_ID'].') '); 
			$query->setPageLimit(150);
			$query->setParam('sl', 'sl_nofuzzy');
			$client = new SearchClient();
			$result = $client->getResult($query);

			#$query->setPageLimit($this->arParams['ITEM_COUNT']);
			#$query->setNavParams();

			#$query->setParam('s', 'desc(document_file_size)');
			#pre($query->getParameters(),1);

			#$client->getDBResult($result);
			#$this->arResult['STR_NAV'] = $client->strNav; 

			if(!empty($result['ITEMS']))
			{	
				/*	  Сортируем массив по полю filesize		*/
				$result['ITEMS'] = nebSort::SortFieldTypeNum($result['ITEMS'], 'filesize');
				
				/* Формируем постраничку */
				$rs = new CDBResult;
				$rs->InitFromArray($result['ITEMS']);
				$rs->NavStart($this->arParams['ITEM_COUNT']);
				$this->arResult["STR_NAV"] = $rs->GetPageNavStringEx($navComponentObject, "", "");
				$this->arResult['NAV_COUNT'] = (int)$rs->NavRecordCount; 

				unset($result['ITEMS']);
				while($arRes = $rs->Fetch())
					$result['ITEMS'][] = $arRes;

				foreach($result['ITEMS'] as $arElement)
				{
					$this->arResult['ITEMS'][] = array(
						'ID' => $arElement['id'],
						'URL' => $arElement['URL'],
						'SOURCE' => $arElement['source'],
						'AUTHOR' => $arElement['authorbook'],
						'AUTHOR_LINK' => '/search/?f_field[authorbook]='.urlencode($arElement['r_authorbook']),
						'~TITLE' => TruncateText($arElement['title'], 90),
						'TITLE' => $arElement['title'],
						'LINK' => $arElement['DETAIL_PAGE_URL'],
						'IMAGE_URL' => $arElement['IMAGE_URL'],
						'VIEWER_URL' => $arElement['VIEWER_URL'],
						'LIBRARY' => $arElement['library'],
						'PUBLISHYEAR' => $arElement['publishyear'],
						'filesize' => $arElement['filesize'],
						'IS_READ' => $arElement['IS_READ'],
					);
				}
			}
		}

		public function executeComponent()
		{
			CPageOption::SetOptionString("main", "nav_page_in_session", "N");	
			$this->getResult();
			$this->includeComponentTemplate();
		}
	}

?>