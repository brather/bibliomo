<?php

	if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)	die();

	use Bitrix\Main\Loader;
	Loader::includeModule('notaext');

	class Plupload extends CBitrixComponent
	{
		public function onPrepareComponentParams($arParams)
		{
			if (!$dir = htmlspecialcharsEx($arParams['DIR']))
				$dir = '/upload/tmp_plupload';
			elseif(strpos($dir, '/upload/') === false)
				$dir = '/upload/'.$dir;

			$dir = $dir.DIRECTORY_SEPARATOR;

			$arParams['DIR'] = $dir;

			if (!$arParams['FILES_FIELD_NAME'])
				$arParams['FILES_FIELD_NAME'] = 'plupload_files';

			if(empty($arParams['RAND_STR']))
				$arParams['RAND_STR'] = randString(7);

			$arParams['MAX_FILE_SIZE'] = intval($arParams['MAX_FILE_SIZE']);

			if($arParams['MAX_FILE_SIZE'] <= 0)
				$arParams['MAX_FILE_SIZE'] = 512;

			$arParams['THUMBNAIL_HEIGHT'] = intval($arParams['THUMBNAIL_HEIGHT']);
			$arParams['THUMBNAIL_WIDTH'] = intval($arParams['THUMBNAIL_WIDTH']);
			$arParams['THUMBNAIL_EXACT'] = (empty($arParams['THUMBNAIL_EXACT']) or (bool)$arParams['CLEANUP_DIR'] === false) ? false : true;
			 
			$arParams['CLEANUP_DIR'] = (empty($arParams['CLEANUP_DIR']) or (bool)$arParams['CLEANUP_DIR'] === false) ? false : true; 

			$arParams['FILE_TYPES']	= str_replace(' ', '', $arParams['FILE_TYPES']);
			$arParams['DIR_PATH'] = $_SERVER['DOCUMENT_ROOT'].$dir;
			$arParams['MAX_FILE_AGE'] = empty($arParams['MAX_FILE_AGE'])? 3 * 3600 : $arParams['MAX_FILE_AGE'];
			$arParams['FILES_FIELD_NAME'] = htmlspecialchars($arParams['FILES_FIELD_NAME']);

			return $arParams;
		}

		public function executeComponent()
		{
			if ($_REQUEST['plupload_ajax'] === 'Y')
			{
				static::uploadFiles();
			}
			else
			{
				$this->includeComponentTemplate();
				return $this->arResult;
			}
		}

		protected function uploadFiles()
		{
			header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
			header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
			header('Cache-Control: no-store, no-cache, must-revalidate');
			header('Cache-Control: post-check=0, pre-check=0', false);
			header('Pragma: no-cache');
			header('Content-type: text/json');
			header('Content-type: application/json');
			@set_time_limit(5 * 60);


			if (isset($_REQUEST["name"])) {
				$fileName = $_REQUEST["name"];
			} elseif (!empty($_FILES)) {
				$fileName = $_FILES["file"]["name"];
			} else {
				$fileName = uniqid("file_");
			}

			$folderForFile = md5(bitrix_sessid_get().$this->arParams['RAND_STR'].$fileName);

			$fileName = $folderForFile.'/'.$fileName;
			$fileFullPath = $this->arParams['DIR_PATH'].$fileName;
			/*			
			Защищаемся от взлома
			*/
			// Если пытаются модифицировать путь загрузки
			if(strpos($fileFullPath, '..') !== false)
				exit();	
			// если загружают файл не того типа				
			if(!empty($this->arParams['FILE_TYPES']))
			{
				if(strpos(strtoupper($this->arParams['FILE_TYPES']), strtoupper(pathinfo($fileFullPath, PATHINFO_EXTENSION))) === false )	
					exit();
			}		
			// php не пройдет
			if(strtoupper(pathinfo(trim($fileFullPath), PATHINFO_EXTENSION)) == 'PHP')
				exit();	
			// если грузят не в папку upload
			if(strpos($fileFullPath, 'upload/') === false)
				exit();		
			// в iblock грузить не надо
			if(strpos($fileFullPath, '/iblock/') !== false)
				exit();			

			CheckDirPath($this->arParams['DIR_PATH'].$folderForFile.'/');			

			// Chunking might be enabled
			$chunk = isset($_REQUEST['chunk']) ? intval($_REQUEST['chunk']) : 0;
			$chunks = isset($_REQUEST['chunks']) ? intval($_REQUEST['chunks']) : 0;

			// Remove old temp dirs
			if ($this->arParams['CLEANUP_DIR'])
			{
				if (!is_dir($this->arParams['DIR_PATH']) || !$dir = opendir($this->arParams['DIR_PATH']))
				{
					die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory."}, "id" : "id"}');
				}

				$timeRight = time() - $this->arParams['MAX_FILE_AGE'];

				while (($curDir = readdir($dir)) !== false)
				{
					if ($curDir != '.' && $curDir != '..')
					{
						if (filemtime($this->arParams['DIR_PATH'].$curDir) < $timeRight)
						{
							DeleteDirFilesEx($this->arParams['DIR'].$curDir.'/');
						}
					}
				}

				closedir($dir);
			}

			// Open temp file
			if (!$out = @fopen("{$fileFullPath}.part", $chunks ? "ab" : "wb"))
			{
				die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
			}

			if (!empty($_FILES))
			{
				if ($_FILES["file"]["error"] || !is_uploaded_file($_FILES["file"]["tmp_name"]))
				{
					die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
				}

				// Read binary input stream and append it to temp file
				if (!$in = @fopen($_FILES["file"]["tmp_name"], "rb"))
				{
					die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
				}
			}
			else
			{
				if (!$in = @fopen("php://input", "rb"))
				{
					die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
				}
			}

			while ($buff = fread($in, 4096))
			{
				fwrite($out, $buff);
			}

			@fclose($out);
			@fclose($in);

			// Check if file has been uploaded
			if (!$chunks || $chunk == $chunks - 1)
			{
				// Strip the temp .part suffix off
				rename("{$fileFullPath}.part", $fileFullPath);

				# делаем превью для картинки, если заданы параметры
				if($this->arParams['THUMBNAIL_HEIGHT'] > 0 and $this->arParams['THUMBNAIL_WIDTH'] > 0)
				{
					$path_parts = pathinfo($fileFullPath);

					$arResize = Bitrix\NotaExt\ImgConvert::Resize($fileFullPath, $this->arParams['THUMBNAIL_WIDTH'], $this->arParams['THUMBNAIL_HEIGHT'], $arParams['THUMBNAIL_EXACT'], 95, $path_parts['dirname'].'/');
					
					if($arResize !== false)
						$thumbnail = ', "thumbnail" : "'.$arResize['src'].'"';
				}	
			}

			// Return Success JSON-RPC response

			die('{"jsonrpc" : "2.0", "result" : null, "id" : "id", "file" : "'.str_replace('\\', "/", $this->arParams['DIR']).$fileName.'"'.$thumbnail.'}');
		}
}