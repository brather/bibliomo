<?php
CBitrixComponent::includeComponentClass("rest:rest.server");
CModule::IncludeModule('nota.userdata');

\CModule::IncludeModule("highloadblock");
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;
use Bitrix\Main\Type\Date;

/**
 * Компонент организует API для получения данных о библиотеках
 *
 * Class RestLibrary
 */
class RestLibrary extends RestServer
{
	protected $token, $from, $to;

	public function processRequest($token = null, $from = null, $to = null)
	{
        $this->token = !empty($token) ? trim($token) : false;
        $this->from = !empty($from) ? trim($from) : false;
        $this->to = !empty($to) ? trim($to) : false;

        if(!$this->token || strlen($this->token) <= 0)
            return $this->restError(500, GetMessage('NO_REQUIRED', array('#PARAM#' => 'token')));

        $tokenUser = CUser::GetList(
            ($by = "id"),
            ($order = "desc"),
            array(
                'ACTIVE' => 'Y',
                'UF_TOKEN' => $this->token,
            ),
            array('FIELDS' => array('ID'))
        )->Fetch();

        if(!$tokenUser)
            return $this->restError(404, GetMessage('USER_NOT_FOUND'));

        return $this->GETLibrary();
	}

	protected function GETLibrary()
	{
        $hlblock = HL\HighloadBlockTable::getById(HIBLOCK_LIBRARY)->fetch();
        $entity = HL\HighloadBlockTable::compileEntity($hlblock);
        $libs = $entity->getDataClass();

        //получим общее число библиотек с установленной датой подключения
        $libsCount = $libs::getList(array(
            'select' => Array('CNT'),
            'filter' => Array('!=UF_DATE_JOIN' => null),
            'runtime' => Array(
                new Entity\ExpressionField('CNT', 'COUNT(*)')
            )
        ));

        $result['TOTAL_COUNT'] = $libsCount->Fetch();
        $result['TOTAL_COUNT'] = $result['TOTAL_COUNT']['CNT'];

        //7.1. Список библиотек - участников проекта за дату.
        $joinedLibsDB = $libs::getList(array(
            'select' => Array('ID', 'NAME' => 'UF_NAME', 'DATE_JOIN' => 'UF_DATE_JOIN'),
            'filter' => Array('>=UF_DATE_JOIN' => new Date($this->from), '<=UF_DATE_JOIN' => new Date($this->to)),
        ));

        while($res = $joinedLibsDB->Fetch()){
            $result['TOTAL_JOINED_COUNT']++;
            $res['DATE_JOIN'] = $res['DATE_JOIN']->format('d.m.Y');
            $result['JOINED_LIBS'][] = $res;
        }
        $result['TOTAL_JOINED_PERCENT_COUNT'] = round(($result['TOTAL_JOINED_COUNT']/$result['TOTAL_COUNT']) * 100);

        //7.2. 7.3 группировка по месяцу
        $from_sql = date('Y-m-d', strtotime($this->from));
        $to_sql = date('Y-m-d', strtotime($this->to));

        global $DB;
        $strSql = "SELECT MONTH(UF_DATE_JOIN) as MONTH, COUNT(ID) as COUNT FROM neb_libs WHERE (UF_DATE_JOIN BETWEEN '".$from_sql."' AND '".$to_sql."')  GROUP BY MONTH(UF_DATE_JOIN)";
        $resultSql = $DB->Query($strSql, false, $err_mess.__LINE__);
        while($res = $resultSql->Fetch()){
            $res['PERCENT_COUNT'] = round(($res['COUNT']/$result['TOTAL_COUNT']) * 100);
            $result['JOINED_LIBS_BY_MONTH'][] = $res;
        }

        $this->arResult = Array($result);
        return 200;
	}
}