<?php
CBitrixComponent::includeComponentClass("rest:rest.server");
/**
 *
 * Class RestReader
 */
class RestReader extends RestServer
{
	protected $token, $action, $q;

	public function processRequest($token = null, $action = null, $q = null)
	{
        $this->token = !empty($token) ? trim($token) : false;
        $this->action = !empty($action) ? trim($action) : false;
        $this->q = !empty($q) ? trim($q) : false;

        if(!$this->token || strlen($this->token) <= 0)
            return $this->restError(500, GetMessage('NO_REQUIRED', array('#PARAM#' => 'token')));

        $tokenUser = CUser::GetList(
            ($by = "id"),
            ($order = "desc"),
            array(
                'ACTIVE' => 'Y',
                'UF_TOKEN' => $this->token,
            ),
            array('FIELDS' => array('ID'))
        )->Fetch();

        if(!$tokenUser)
            return $this->restError(404, GetMessage('USER_NOT_FOUND'));

        if($this->action == 'getUserStatus'){
            return $this->GETUserStatus();
        }elseif($this->action == 'getIPStatus'){
            return $this->GETIPStatus();
        } else {
            return $this->restError(404, GetMessage('USER_NOT_FOUND'));
        }
	}

    protected function GETUserStatus(){
        $u = CUser::GetByLogin($this->q);
        $u = $u->Fetch();

        //22 == свойсто статус пользователя
        $rsStatus = CUserFieldEnum::GetList(array('SORT' => 'ASC'), Array('USER_FIELD_ID' => '22'));
        while($status = $rsStatus->GetNext())
            $ufStatus[$status['ID']] = $status['XML_ID'];

        $this->arResult = $ufStatus[$u['UF_STATUS']];

        return 200;
    }

    protected function GETIPStatus(){
        $this->arResult = NebWorkplaces::checkAccessEx($this->q);
        return 200;
    }
}