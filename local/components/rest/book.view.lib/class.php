<?php
CBitrixComponent::includeComponentClass("rest:rest.server");
CModule::IncludeModule('nota.userdata');
CModule::IncludeModule('nota.exalead');

\CModule::IncludeModule("highloadblock");
use Bitrix\Main\Type\Date;
use Bitrix\Main\Entity\ExpressionField;

use Nota\Exalead\Stat\StatBookReadTable;

/**
 * Компонент организует API для получения данных о библиотеках и количестве просмотров изданий
 * https://docs.google.com/document/d/1Nxj__eio7bvaMpDRfaEAzeJ1o-9JI4mu5VnzTESRVBo/edit#
 * Пункт 8.4
 *
 * Class RestBookViewLib
 */
class RestBookViewLib extends RestServer
{
	protected $token, $from, $to, $id;

	public function processRequest($token = null, $from = null, $to = null, $id = Array())
	{
        $this->token = !empty($token) ? trim($token) : false;
        $this->from = !empty($from) ? trim($from) : false;
        $this->to = !empty($to) ? trim($to) : false;
        $this->id = !empty($id) ? $id : false;

        if(!$this->token || strlen($this->token) <= 0)
            return $this->restError(500, GetMessage('NO_REQUIRED', array('#PARAM#' => 'token')));

        $tokenUser = CUser::GetList(
            ($by = "id"),
            ($order = "desc"),
            array(
                'ACTIVE' => 'Y',
                'UF_TOKEN' => $this->token,
            ),
            array('FIELDS' => array('ID'))
        )->Fetch();

        if(!$tokenUser)
            return $this->restError(404, GetMessage('USER_NOT_FOUND'));

        return $this->GETLibView();
	}

	protected function GETLibView()
	{
        $result = StatBookReadTable::getList(Array(
            'select' => Array('LIB_NAME' => 'BOOK.LIB.Name', 'COUNT'),
            'filter' => Array(
                'BOOK.ALIS' => $this->id,
                '>=X_TIMESTAMP' => new Date($this->from),
                '<=X_TIMESTAMP' => new Date($this->to),
            ),
            'runtime' => Array(
                new ExpressionField('COUNT', 'COUNT(DISTINCT %s)', Array('ID')),
            )
        ));

        while($res = $result->Fetch()){
            $return[] = $res;
        }

        $this->arResult = $return;
        return 200;
	}
}