<?php
CBitrixComponent::includeComponentClass("rest:rest.server");
CModule::IncludeModule('nota.userdata');
use Nota\UserData\Bookmarks as UserBookmarks;
/**
 * Компонент организует API по приёму, передаче, удалению закладок
 *
 * Class RestBookmarks
 */
class RestBookmarks extends RestServer
{
	protected $token, $bookID, $numPage, $bookmarkID, $uid;

	/**
	* Т.к. через ЧПУ мы не обработаем GET, PUT, DELETE - обрабатываем их тут.
	* Но помним уже, что в параметрах у нас стоит ограничение, и другие REQUEST_METHOD не пройдут.
	*/
	public function processRequest($token = null, $bookID = null, $numPage = null, $ID = null)
	{     
		if ( isset($token) && !empty($token) )
			$this->token 		= trim($token);
			
		if ( isset($bookID) && !empty($bookID) )
			$this->bookID 		= trim($bookID);
			
		if ( isset($numPage) && !empty($numPage) )
			$this->numPage 		= trim($numPage);
			
		if ( isset($ID) && !empty($ID) )
			$this->bookmarkID 	= $ID;
	
		// Токен нам нужен везде - так что проверить его можно и тут.
		if ( strlen($this->token) <= 0 )
			return $this->restError(500, GetMessage('NO_REQUIRED', array('#PARAM#' => 'token')));
			
		// Теперь сразу найдём uid пользователя
		$tokenUser = CUser::GetList(
			($by="id"),
			($order="desc"),
			array(
				'ACTIVE' 	=> 'Y',
				'UF_TOKEN' 	=> $this->token,
			), 
			array('FIELDS'=> array('ID'))
		)->Fetch();
		if ( !$tokenUser || (int)$tokenUser['ID'] <= 0 )
			return $this->restError(404, GetMessage('USER_NOT_FOUND'));
		else
			$this->uid = $tokenUser['ID'];


		switch ( $_SERVER['REQUEST_METHOD'] )
		{
			case 'PUT':
				return $this->PUTBookmarks();
				break;
			case 'GET':
				return $this->GETBookmarks();
				break;
			case 'DELETE':
				return $this->DELETEBookmarks();
				break;
		}
		
	}
	
	/**
	* Находит UID по token
	* Добавляет в избранное указанную книгу
	*/
	protected function PUTBookmarks()
	{
		if ( !$this->bookID || strlen($this->bookID) <= 0 )
			return $this->restError(500, GetMessage('NO_REQUIRED', array('#PARAM#' => 'book_id')));
	
		if ( !$this->numPage || strlen($this->numPage) <= 0 )
			return $this->restError(500, GetMessage('NO_REQUIRED', array('#PARAM#' => 'num_page')));
		if ( !is_numeric($this->numPage) )
			return $this->restError(500, GetMessage('NUM_PAGE_INT'));
	
		$resultID = UserBookmarks::add($this->bookID, $this->numPage, $this->uid);
		$this->arResult = array(
			'resultID' => (int)$resultID
		);
		
		return 200;
	}
	
	/**
	* Отдаёт список всех книг из избранного
	*/
	protected function GETBookmarks()
	{
		$result = UserBookmarks::getBookmarkList($this->bookID, $this->numPage, $this->uid);
		if($result === false)
			$result = (object) array();
		$this->arResult = $result;
		
		return 200;
	}
	
	/**
	* Удаляет из избранного по token
	*/
	protected function DELETEBookmarks()
	{
		
		if ( !$this->bookmarkID || strlen($this->bookmarkID) <= 0 )
			return $this->restError(500, GetMessage('NO_REQUIRED', array('#PARAM#' => 'ID')));
		
		if (is_array($this->bookmarkID)){
		
			foreach ($this->bookmarkID as $id){
				$result = UserBookmarks::deleteWithTest($id, $this->uid);
			}
		}else{
			// Удаляем с предварительным сопоставлением пользователю
			$result = UserBookmarks::deleteWithTest($this->bookmarkID, $this->uid);
		}
		
		if ( !$result )
		{
			$this->restError(404, GetMessage('NOT_FOUND'));
		}
			
		$this->arResult = true;
		return 200;
	}
	
	
}