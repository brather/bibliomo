<?php
CBitrixComponent::includeComponentClass("rest:rest.server");
CModule::IncludeModule('nota.userdata');
CModule::IncludeModule('nota.exalead');

use Nota\Exalead\SearchQuery;
use Nota\Exalead\SearchClient;

/**
 * Компонент организует API для получения числа изданий в библиотеке из Экзалиды
 * https://docs.google.com/document/d/1Nxj__eio7bvaMpDRfaEAzeJ1o-9JI4mu5VnzTESRVBo/edit#
 * Пункт 8.1
 *
 * Class RestExaleadBook
 */
class RestExaleadBook extends RestServer
{
	protected $token, $library;

	public function processRequest($token = null, $library = null)
	{
        $this->token = !empty($token) ? trim($token) : false;
        $this->library = !empty($library) ? trim($library) : false;

        if(!$this->token || strlen($this->token) <= 0)
            return $this->restError(500, GetMessage('NO_REQUIRED', array('#PARAM#' => 'token')));

        $tokenUser = CUser::GetList(
            ($by = "id"),
            ($order = "desc"),
            array(
                'ACTIVE' => 'Y',
                'UF_TOKEN' => $this->token,
            ),
            array('FIELDS' => array('ID'))
        )->Fetch();

        if(!$tokenUser)
            return $this->restError(404, GetMessage('USER_NOT_FOUND'));

        return $this->GETData();
	}

	protected function GETData()
	{
        $query = new SearchQuery('all#');
        $query->setQueryType('raw');

        $client = new SearchClient();
        $result = $client->getResult($query);

        $xml = simplexml_load_string($result);
        $attr = $xml->attributes();
        preg_match("/nbdocs=(\\d+),/", $attr['ellql'], $book_count);
        $book_count = (int)$book_count[1];

        $this->arResult = $book_count;
        return 200;
	}
}