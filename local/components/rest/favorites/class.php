<?php
CBitrixComponent::includeComponentClass("rest:rest.server");
CModule::IncludeModule('nota.userdata');
use Nota\UserData\Books as UserFavorites;
/**
 * Компонент организует API по приёму, передаче, удалению избранного
 *
 * Class RestFavorites
 */
class RestFavorites extends RestServer
{
	protected $token, $bookID, $favoriteID, $uid;

	/**
	* Т.к. через ЧПУ мы не обработаем GET, PUT, DELETE - обрабатываем их тут.
	* Но помним уже, что в параметрах у нас стоит ограничение, и другие REQUEST_METHOD не пройдут.
	*/
	public function processRequest($token = null, $bookID = null, $ID = null)
	{
		if ( isset($token) && !empty($token) )
			$this->token 		= trim($token);
			
		if ( isset($bookID) && !empty($bookID) )
			$this->bookID 		= trim($bookID);
			
		if ( isset($ID) && !empty($ID) )
			$this->favoriteID 	= $ID;
	
		// Токен нам нужен везде - так что проверить его можно и тут.
		if ( strlen($this->token) <= 0 )
			return $this->restError(500, GetMessage('NO_REQUIRED', array('#PARAM#' => 'token')));
			
		// Теперь сразу найдём uid пользователя
		$tokenUser = CUser::GetList(
			($by="id"),
			($order="desc"),
			array(
				'ACTIVE' 	=> 'Y',
				'UF_TOKEN' 	=> $this->token,
			), 
			array('FIELDS'=> array('ID'))
		)->Fetch();
		if ( !$tokenUser || (int)$tokenUser['ID'] <= 0 )
			return $this->restError(404, GetMessage('USER_NOT_FOUND'));
		else
			$this->uid = $tokenUser['ID'];

		switch ( $_SERVER['REQUEST_METHOD'] )
		{
			case 'PUT':
				return $this->PUTFavorites();
				break;
			case 'GET':
				return $this->GETFavorites();
				break;
			case 'DELETE':
				return $this->DELETEFavorites();
				break;
		}
		
	}
	
	/**
	* Находит UID по token
	* Добавляет в избранное указанную книгу
	*/
	protected function PUTFavorites()
	{
		if ( !$this->bookID || strlen($this->bookID) <= 0 )
			return $this->restError(500, GetMessage('NO_REQUIRED', array('#PARAM#' => 'book_id')));
	
		$resultID = UserFavorites::add($this->bookID, $this->uid);
		$this->arResult = array(
			'resultID' => (int)$resultID
		);
		
		return 200;
	}
	
	/**
	* Отдаёт список всех книг из избранного
	*/
	protected function GETFavorites()
	{
		$result = UserFavorites::getListCurrentUser(false, $this->uid);
		if($result === false)
			$result = (object) array();
		$this->arResult = $result;
		
		return 200;
	}
	
	/**
	* Удаляет из избранного по token
	*/
	protected function DELETEFavorites()
	{
		if ( !$this->favoriteID || strlen($this->favoriteID) <= 0 )
			return $this->restError(500, GetMessage('NO_REQUIRED', array('#PARAM#' => 'ID')));
		
		
		if (is_array($this->favoriteID)){
			foreach ($this->favoriteID as $id){
				$result = UserFavorites::deleteWithTest($id, $this->uid);
			}
		}else{
			// Сперва пробуем получить эту запись
			$result = UserFavorites::deleteWithTest($this->favoriteID, $this->uid);
		}
		if ( !$result )
		{
			$this->restError(404, GetMessage('NOT_FOUND'));
		}
			
		$this->arResult = true;
		return 200;
	}
}