<?php
$MESS['NO_SETTINGS'] = 'Компонент на сервере настроен неверно';
$MESS['FUNCTION_DOES_NOT_EXISTS'] = 'Запрошенная функция не реализована';
$MESS['INVALID_REQUEST_METHOD'] = 'Неверный метод запроса: #CURRENT# (Поддерживается: #NEEDED#)';
$MESS['DOCUMENTATION'] = 'Документация';

$MESS['DOC_NO_PARAMETERS'] = 'Без параметров';
$MESS['DOC_URL_PARTS'] = 'Параметры в URL';
$MESS['DOC_REQUIRED'] = 'Обязательные параметры';
$MESS['DOC_OPTIONAL'] = 'Не обязательные параметры';
$MESS['DOC_PARAMETER_TITLE'] = 'Параметр';
$MESS['DOC_PARAMETER_TYPE'] = 'Тип';
$MESS['DOC_PARAMETER_VARIANTS'] = 'Варианты значения';
$MESS['DOC_PARAMETER_DESCRIPTION'] = 'Описание';

$MESS['BAD_IP_ERROR'] = 'C Вашего ip \'#IP#\' доступ в сервис запрещен';
$MESS['DOWNLOAD_PDF'] = 'Скачать PDF';

$MESS['NO_TOKEN'] = 'Не указан token';
$MESS['USER_NOT_FOUND'] = 'Указан несуществующий token';

$MESS['NO_BOOK_ID'] = 'Обязательный параметр book_id не указан';
$MESS['NOT_FOUND'] = 'Запись с таким ID не найдена';
$MESS['NO_NUM_PAGE'] = 'Обязательный параметр num_page не указан';
$MESS['NUM_PAGE_INT'] = 'Параметр num_page должен быть типа int';

$MESS['NO_REQUIRED'] = 'Обязательный параметр #PARAM# не указан.';
$MESS[''] = '';