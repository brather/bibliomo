<? if (!isset($arResult['PDF_MODE']) OR $arResult['PDF_MODE'] != true): ?>
    <div id="download_pdf">
        <a href="?_doc=1&_pdf=1"><img src="<?= $templateFolder ?>/pdf.png" width="32" height="32"><span><?= GetMessage("DOWNLOAD_PDF"); ?></span></a>
    </div>
<? endif; ?>

<div id="groups">
    <? foreach ($arResult['GROUPS'] as $groupName => $groupData): ?>
        <h3 class="doc_group__name"><?= $arResult['GROUPS_CAPTIONS'][$groupName] ?></h3>
        <div class="doc_group">

            <div class="doc_group__functions">
                <? foreach ($groupData as $functionName => $functionData): ?>
                    <div class="doc_function">

                        <div class="doc_function__header">
                            <div class="doc_function__request_type doc_function__request_<?= strtolower($functionData['TYPE']) ?>"><?= strtoupper($functionData['TYPE']) ?></div>
                            <div class="doc_function__request_url">
                                <a href="<?= 'http://' . $functionData['SERVICE_DOC_URL'] ?>">
                                    <?= $functionData['SERVICE_URL'] ?>
                                </a>
                            </div>
                            <div class="doc_function__name"><?= $functionData['NAME'] ?></div>
                        </div>

                        <div class="doc_function__detail">

                            <? if (!empty($functionData['DESCRIPTION'])): ?>
                                <div class="doc_function__description"><?= $functionData['DESCRIPTION'] ?></div>
                            <? endif; ?>

                            <div class="doc_function__parameters">
                                <? if (!isset($functionData['PARAMETERS']) OR empty($functionData['PARAMETERS'])): ?>
                                    <?= GetMessage('DOC_NO_PARAMETERS'); ?>
                                <? else: ?>

                                    <? foreach ($functionData['PARAMETERS'] as $groupName => $groupParameters): ?>
                                        <? if (empty($groupParameters)) continue; ?>

                                        <div class="doc_function__parameter_group">
                                            <span><?= GetMessage('DOC_' . $groupName); ?>:</span>

                                            <table border="0" cellspacing="0" cellpadding="0">
                                                <thead>
                                                <tr>
                                                    <td><?= GetMessage('DOC_PARAMETER_TITLE'); ?></td>
                                                    <td><?= GetMessage('DOC_PARAMETER_TYPE'); ?></td>

                                                    <?
                                                    $hasVariants = false;
                                                    foreach ($groupParameters as $parameterName => $parameterData) {
                                                        if (!empty($parameterData['VARIANTS'])) {
                                                            $hasVariants = true;
                                                        }
                                                    }?>
                                                    <? if ($hasVariants): ?>
                                                        <td><?= GetMessage('DOC_PARAMETER_VARIANTS'); ?></td>
                                                    <? endif; ?>
                                                    <td><?= GetMessage('DOC_PARAMETER_DESCRIPTION'); ?></td>

                                                </thead>

                                                <tbody>

                                                <? foreach ($groupParameters as $parameterName => $parameterData): ?>
                                                    <tr>
                                                        <td><?= $parameterName ?></td>
                                                        <td><?= $parameterData['TYPE'] ?></td>
                                                        <? if ($hasVariants): ?>
                                                            <td class="variants">
                                                                <? if (count($parameterData['VARIANTS'])): ?>
                                                                    <table>
                                                                        <? foreach ($parameterData['VARIANTS'] as $variantValue => $variantName): ?>
                                                                            <tr>
                                                                                <td class="value"><?= $variantValue ?></td>
                                                                                <td><?= $variantName ?></td>
                                                                            </tr>
                                                                        <? endforeach; ?>
                                                                    </table>
                                                                <? endif; ?>
                                                            </td>
                                                        <? endif; ?>
                                                        <td><?= $parameterData['DESCRIPTION'] ?></td>
                                                    </tr>

                                                <? endforeach; ?>

                                                </tbody>
                                            </table>

                                        </div>

                                    <? endforeach; ?>

                                <? endif; ?>
                            </div>
                        </div>

                    </div>
                <? endforeach; ?>
            </div>

        </div>
    <? endforeach; ?>
</div>