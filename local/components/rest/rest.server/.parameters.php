<?php
$arComponentParameters = array(
    "PARAMETERS" => array(
        "CACHE_TYPE" => "N",
        "CACHE_TIME" => Array("DEFAULT" => 3600),
        "SEF_FOLDER" => "/rest_api/",
    ),
);