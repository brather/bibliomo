<?php
include_once 'dompdf/dompdf_config.inc.php';

class RestServer extends CBitrixComponent
{
	private $reflection;
	private $componentDir;
	private $componentParameters;
	private $docMode = false;
	protected $moduleName = false;

	public function executeComponent()
	{
		$this->prepareREQUEST();
	
		if ((isset($this->arParams['DOCUMENTATION']) AND $this->arParams['DOCUMENTATION'] == 'Y')
			AND isset($_GET['_doc'])
		) {
			$this->docMode = (bool)$_GET['_doc'];
		}

		$this->reflection = new ReflectionClass($this);
		$this->componentDir = dirname($this->reflection->getFileName()) . '/';

		__IncludeLang($this->componentDir . 'lang/' . LANGUAGE_ID . '/.parameters.php');
		include $this->componentDir . '.parameters.php';
		$this->componentParameters = $arComponentParameters;

		$this->includeParentLang();

		if ($this->moduleName) {
			$access = $this->checkIP();
			if (!$access)
				return $this->restError(403, GetMessage('BAD_IP_ERROR', array('#IP#' => $this->getRemoteIP())));
		}

		$settings = $this->getSEF_Settings();
		if (!$settings) return $this->restError(500, GetMessage("NO_SETTINGS"));

		list($variables, $function, $type) = $settings;
		if ( !is_array($type) )
			$type = array($type);

		$code = 200;
		if ($this->docMode) {

			$this->prepareDocumentation($function);

			$this->__parent = new RestServer();
			$this->__parent->initComponent('rest:rest.server');
			$this->__parent->arResult = $this->arResult;
			$this->__parent->arParams = $this->arParams;

			if (isset($_GET['_pdf']) AND $_GET['_pdf'] == '1') {

				$this->__parent->arResult['PDF_MODE'] = true;
				ob_start();
				$this->__parent->includeComponentTemplate();
				$html = ob_get_clean();

				$this->__parent->arResult = array(
					'css' => $this->getTempalteCSS(),
					'html' => $html
				);
				ob_start();
				$this->__parent->includeComponentTemplate('pdf');
				$html = ob_get_clean();


				global $APPLICATION;
				$APPLICATION->RestartBuffer();

				$dompdf = new DOMPDF();
				$dompdf->load_html($html);
				$dompdf->set_paper('A4', 'portrait');
				$dompdf->render();
				$dompdf->stream($this->arResult['SERVICE_NAME'] . ".pdf");
				exit(0);


			} else {
				CUtil::InitJSCore();
				CJSCore::Init(array("jquery"));
				$this->__parent->includeComponentTemplate();
			}

		} else {
			try {
				$this->reflection->getMethod($function);
			} catch (Exception $e) {
				return $this->restError(500, GetMessage("FUNCTION_DOES_NOT_EXISTS"));
			}

			if ( !in_array($_SERVER['REQUEST_METHOD'], $type) )
				return $this->restError(405, GetMessage('INVALID_REQUEST_METHOD', array('#CURRENT#' => $_SERVER['REQUEST_METHOD'], '#NEEDED#' => implode(', ', $type))));
					   
			$code = call_user_func_array(array($this, $function), $variables);
			if ($code !== false) {
				$this->printResponse($code);
			}
		}


		return $code;
	}

	private function getTempalteCSS()
	{
		$css = '';
		$cssFiles = array(
			$_SERVER['DOCUMENT_ROOT'] . '/bitrix/js/main/core/css/core.css',
		);
		foreach ($cssFiles as $file) {
			$css .= file_get_contents($file);
			$css .= "\n\n";
		}

		/** @var $class ReflectionClass */
		$class = $this->reflection;
		while ($class = $class->getParentClass()) {
			if ($class->getName() == 'CBitrixComponent') break;

			$file = dirname($class->getFileName()) . '/templates/.default/pdf.css';
			$css .= file_get_contents($file);
			$css .= "\n\n";
		}
		return $css;
	}

	private function includeParentLang()
	{
		/** @var $class ReflectionClass */
		$class = $this->reflection;
		while ($class = $class->getParentClass()) {
			if ($class->getName() == 'CBitrixComponent') break;

			$langDir = dirname($class->getFileName()) . '/lang/' . LANGUAGE_ID . '/component.php';
			__IncludeLang($langDir);
		}
	}

	protected function restError($code, $message = "")
	{
		$this->arResult = array(
			'errors' => array(
				'message' => $message,
				'code' => $code
			)
		);
		$this->printResponse($code);
		return false;
	}

	private function printResponse($code = 200)
	{
		global $APPLICATION;
		global $USER;
		if (!$USER->IsAdmin()) {
			$APPLICATION->RestartBuffer();
			header('Content-type: application/json');
		}

		//http_response_code($code); // php >= 5.4
		CHTTP::SetStatus($code);
		
		print json_encode($this->arResult);

		if (!$USER->IsAdmin()) {
			die();
		}
	}

	private function getSEF_Settings()
	{
		if ($this->arParams['SEF_MODE'] != 'Y')
			return false;

		$arComponentVariables = array();
		$arVariables = array();

		$arUrlTemplates = CComponentEngine::MakeComponentUrlTemplates(array(), $this->arParams["SEF_URL_TEMPLATES"]);
		$arVariableAliases = CComponentEngine::MakeComponentVariableAliases(array(), $this->arParams["VARIABLE_ALIASES"]);
		$componentPage = CComponentEngine::ParseComponentPath($this->arParams["SEF_FOLDER"], $arUrlTemplates, $arVariables);
		CComponentEngine::InitComponentVariables($componentPage, $arComponentVariables, $arVariableAliases, $arVariables);

		if (strlen($componentPage) == 0 OR (bool)$componentPage == false) {
			foreach ($this->componentParameters['PARAMETERS']['SEF_MODE'] as $function => $options) {
				if ($options['DEFAULT'] == '/' OR strlen($options['DEFAULT']) == 0) {
					$componentPage = $function;
					break;
				}
			}
		}

		if (isset($this->componentParameters['PARAMETERS']['SEF_MODE'][$componentPage]['VARIABLES'])) {
			$parametersVars = $this->componentParameters['PARAMETERS']['SEF_MODE'][$componentPage]['VARIABLES'];
			foreach ($parametersVars as $varName) {
				if (!isset($arVariables[$varName]) AND isset($_REQUEST[$varName]))
					$arVariables[$varName] = $_REQUEST[$varName];
				else
					$arVariables[$varName] = '';
			}
		}

		$type = isset($this->componentParameters['PARAMETERS']['SEF_MODE'][$componentPage]['TYPE']) ? $this->componentParameters['PARAMETERS']['SEF_MODE'][$componentPage]['TYPE'] : 'GET';
		return array($arVariables, $componentPage, $type);
	}

	private function prepareDocumentation($function)
	{
		global $APPLICATION;
		$isRoot = $APPLICATION->GetCurPage() == $this->arParams['SEF_FOLDER'];
		$this->arResult = array();

		$groupsList = isset($this->componentParameters['PARAMETERS']['DOCUMENTATION']['GROUPS']) ?
			$this->componentParameters['PARAMETERS']['DOCUMENTATION']['GROUPS'] : array();
		$this->arResult['GROUPS_CAPTIONS'] = $groupsList;

		$this->arResult['SERVICE_NAME'] = empty($this->arParams['SERVICE_NAME']) ?
			(isset($this->componentParameters['PARAMETERS']['SERVICE_NAME']['DEFAULT']) ? $this->componentParameters['PARAMETERS']['SERVICE_NAME']['DEFAULT'] : "")
			: $this->arParams['SERVICE_NAME'];

		if ($isRoot) {
			foreach ($this->componentParameters['PARAMETERS']['SEF_MODE'] as $function => $data) {
				$functionData = $this->processFunction($function, $data);

				$group = (isset($data['DOCUMENTATION']['GROUP']) AND array_key_exists($data['DOCUMENTATION']['GROUP'], $groupsList)) ? $data['DOCUMENTATION']['GROUP'] : "NO_GROUP";

				$this->arResult['GROUPS'][$group][$function] = $functionData;
			}

		} else {
			$data = $this->componentParameters['PARAMETERS']['SEF_MODE'][$function];
			$functionData = $this->processFunction($function, $data);
			$this->arResult['GROUPS']["NO_GROUP"][$function] = $functionData;
		}

	}

	private function processFunction($function, $data)
	{
		$docData = isset($data['DOCUMENTATION']) ? $data['DOCUMENTATION'] : array();
		$urlTemplate = $this->arParams['SEF_URL_TEMPLATES'][$function];

		$docData['NAME'] = $data['NAME'];
		$docData['TYPE'] = $data['TYPE'];
		$docData['SERVICE_URL'] = $this->arParams['SEF_FOLDER'] . $urlTemplate;
		$docData['SERVICE_DOC_URL'] = $_SERVER['HTTP_HOST'] . $docData['SERVICE_URL'] . '?_doc=1';

		if (isset($data['VARIABLES']) AND is_array($data['VARIABLES'])) {
			foreach ($data['VARIABLES'] as $parameter) {

				//Если переменная не встречается в шаблоне URL, то её надо передавать через POST
				if (strpos($urlTemplate, '#' . $parameter . '#') !== false) {
					$docData['PARAMETERS'][$parameter]['URL_PARTS'] = true;
					$docData['SERVICE_DOC_URL'] = str_replace("#$parameter#", '{' . $parameter . '}', $docData['SERVICE_DOC_URL']);
					$docData['SERVICE_URL'] = str_replace("#$parameter#", '{' . $parameter . '}', $docData['SERVICE_URL']);
				} else {
					$docData['PARAMETERS'][$parameter]['URL_PARTS'] = false;
				}
			}
		}

		//Сортируем по группам
		foreach ($docData['PARAMETERS'] as $name => $parameter) {
			$group = 'OPTIONAL';
			if (isset($parameter['REQUIRED']) AND $parameter['REQUIRED'] == true) {
				$group = 'REQUIRED';
			}
			if (isset($parameter['URL_PARTS']) AND $parameter['URL_PARTS'] == true) {
				$group = 'URL_PARTS';
			}

			unset($parameter['URL_PARTS']);
			unset($parameter['REQUIRED']);

			if (!empty($parameter)) {
				$docData['PARAMETERS'][$group][$name] = $parameter;
			}
			unset($docData['PARAMETERS'][$name]);
		}

		return $docData;
	}

	private function getRemoteIP()
	{
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}

		return $ip;
	}


	private function checkIP()
	{
		$remoteIP = $this->getRemoteIP();

		$arIP = explode(',', COption::GetOptionString($this->moduleName, 'permitted_ip'));
		if (in_array($remoteIP, $arIP))
			return true;
		else
			return false;

	}
	
	/**
	* Подготавилавет $_REQUEST для SEF методов
	*/
	private function prepareREQUEST()
	{
		// У нас могут создать проблемы PUT и DELETE запросы
		// Т.к. у них свои сетоды передачи данных
		// В этом методе будем собирать эти данные и наполнять ими $_REQUEST
		if ( in_array($_SERVER['REQUEST_METHOD'], array('PUT', 'DELETE')) )
		{
			$putdata = file_get_contents('php://input'); 
			$exploded = explode('&', $putdata);  

			foreach($exploded as $pair)
			{
				$item = explode('=', $pair);
				if ( count($item) == 2 )
				{
					$_REQUEST[urldecode($item[0])] = urldecode($item[1]);
				}
			}  
		}
	}

}