<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 23.11.2016
 * Time: 11:29
 */
use Bitrix\Main\Localization\Loc,
    Elr\Install\FirstUploadUsers,
    Elr\Install\AbisAddSyncUser;

global $USER, $APPLICATION;

if(!$USER->IsAdmin()) return false;

IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/options.php");
IncludeModuleLangFile(__FILE__);

$arAbisOption = array(
    array(
        "elr_useraccess_abis_secret_token",
        Loc::getMessage("ELR_ABIS_SECRET_KEY"),
        false,
        array(
            "text",
            20
        )
    ),
    array(
        "elr_usersccess_abis_host",
        Loc::getMessage( "ELR_ABIS_HOST" ),
        false,
        array(
            "text",
            20
        )
    ),
    array(
        "elr_usersccess_abis_protocol",
        Loc::getMessage( "ELR_ABIS_PROTOCOL" ),
        false,
        array(
            "selectbox",
            array(
                "http" => "http",
                "https" => "https",
            )
        )
    )
);

$arIrbisOption = array(
        array(
                "note" => "Файл должен быть формата CSV, и не превышать 15Мб."
        ),
        array(
                "elr_irbis_csv_file",
                "CSV File",
                '<input type="file" id="file_csv"/>',
                array('statichtml')
        ),
        array(
                "elr_containet_csv_upload",
                "",
                '<div class="pl_button" style="display: none;"></div>',
                array('statichtml')
        ),
        array(
                "elr_plupload_console",
                "",
                '<pre class="plupload_console" style="display: none;"></pre>
                <span class="" id="uploaded_file"><a target="_blank" href=""></a></span>&nbsp;<span class="file-progress hidden" id="add_file_progress"></span>
                <div class="field validate addpdfield" id="add_file_error">
                    <input type="text" data-required="false" value="" id="URL_DATA_FILE" name="URL_DATA_FILE" class="input" style="display: none"/>
                    <!--em class="error required">Поле обязательно для заполнения</em-->	
                    <em class="error upload"></em>	
                </div>',
                array("statichtml")
        ),
        array(
                "elr_parsing_csv",
                "",
                "<span class='lable_parsing_csv' style='display: none;'>Выгрузка пользователей</span>",
                array("statichtml")
        ),
        array(
                "elr_csv_total_users",
                "<span class='label_total_users' style='display: none'>Найдено пользователей в файле:</span>",
                "<span class='text_total_users' style='display: none;'></span>",
                array("statichtml")
        ),
        array(
                "elr_csv_total_users",
                "<span class='label_upload_users' style='display: none;'>Загружено пользователей:</span>",
                "<span class='text_upload_users' style='display: none;'></span>",
                array("statichtml")
        )
);

$aTabs = array(
    array(
        "DIV" => "useraccess_api",
        "TAB" => Loc::getMessage( "ELR_ABIS_OPTION_API" ),
        "TITLE" => Loc::getMessage( "ELR_ABIS_OPTION_API" ),
        "OPTIONS" => array()
    ),
    array(
        "DIV" => "irbis",
        "TAB" => Loc::getMessage( "ELR_ABIS_IRBIS_API" ),
        "TITLE" => Loc::getMessage( "ELR_ABIS_IRBIS_API" ),
        "OPTIONS" => array()
    ),
);

$tabControl = new CAdminTabControl("tabControl", $aTabs);

/* События на Сохранение\Применение\Сброса */
if($_SERVER['REQUEST_METHOD'] == "POST" && strlen($Update.$Apply.$RestoreDefaults)>0 && check_bitrix_sessid())
{
    if(strlen($RestoreDefaults)>0)
    {
        COption::RemoveOption($mid);
    }
    else
    {
        __AdmSettingsSaveOptions($mid, $arAbisOption);
    }

    if(strlen($Update)>0 && strlen($_REQUEST["back_url_settings"])>0)
        LocalRedirect($_REQUEST["back_url_settings"]);
    else
        LocalRedirect($APPLICATION->GetCurPage()."?mid=".urlencode($mid)."&lang=".urlencode(LANGUAGE_ID)."&back_url_settings=".urlencode($_REQUEST["back_url_settings"])."&".$tabControl->ActiveTabParam());
}

/* Событие на синхронизацию данных */
if ( $_SERVER['REQUEST_METHOD'] == "POST" && isset( $_REQUEST['action'] ) && $_REQUEST['action'] == "sync_first" )
{
    CModule::IncludeModule("elr.useraccess");
    $fuu = FirstUploadUsers::getInstance();

    if (COption::GetOptionString("elr.useraccess", "sync_first_time", "Y") === "N")
    {

        $fuu->setPerIteration(100);
        $next = true;
        $addAbisUser = new AbisAddSyncUser();
        $result = [];

        while($next)
        {
            $res = $fuu->getRequestData();

            if ($res['status'] === 'ok' && count($res['readers']) > 0)
            {

                $result = array_merge($result, $addAbisUser->pushUser( $res['readers'] ) );

                if ($addAbisUser->error !== null){
                    echo CAdminMessage::ShowOldStyleError(implode("<br>", $addAbisUser->error));
                    $next = false;
                }
            } else {
                $next = false;
            }
        }

        COption::SetOptionString("elr.useraccess", "sync_first_time", "Y");

        if (count($result['err']))
            echo CAdminMessage::ShowOldStyleError(implode("<br>", $result['del']));

        if (count($result['del']) > 1 || count($result['add']) > 1)
        {
            $strNotice = "";

            if (count($result['del']))
                $strNotice = implode("<br>", $result['del']) . "<br>";
            if (count($result['add']))
                $strNotice = implode("<br>", $result['add']) . "<br>";

            echo CAdminMessage::ShowNote($strNotice);
        }

    } else {
        echo CAdminMessage::ShowOldStyleError("Выгрузка не возможна. Первичный запуск уже производился.");
    }
}

$tabControl->Begin();
$tabControl->BeginNextTab();?>
<form method="post" action="<?=$APPLICATION->GetCurPage()?>?mid=<?=urlencode($mid)?>&amp;lang=<?=LANGUAGE_ID?>">
    <?
    __AdmSettingsDrawList($mid, $arAbisOption);

    $tabControl->BeginNextTab();

    __AdmSettingsDrawList($mid, $arIrbisOption);

    $tabControl->Buttons();
    ?>
    <input type="submit" name="Update" value="<?=Loc::getMessage("MAIN_SAVE")?>" title="<?=Loc::getMessage("MAIN_OPT_SAVE_TITLE")?>" class="adm-btn-save">
    <input type="submit" name="Apply" value="<?=Loc::getMessage("MAIN_OPT_APPLY")?>" title="<?=Loc::getMessage("MAIN_OPT_APPLY_TITLE")?>">
    <?if(strlen($_REQUEST["back_url_settings"])>0):?>
        <input type="button" name="Cancel" value="<?=Loc::getMessage("MAIN_OPT_CANCEL")?>" title="<?=Loc::getMessage("MAIN_OPT_CANCEL_TITLE")?>" onclick="window.location='<?echo htmlspecialcharsbx(CUtil::addslashes($_REQUEST["back_url_settings"]))?>'">
        <input type="hidden" name="back_url_settings" value="<?=htmlspecialcharsbx($_REQUEST["back_url_settings"])?>">
    <?endif?>
    <input type="submit" name="RestoreDefaults" title="<?echo Loc::getMessage("MAIN_HINT_RESTORE_DEFAULTS")?>" OnClick="return confirm('<?echo addslashes(Loc::getMessage("MAIN_HINT_RESTORE_DEFAULTS_WARNING"))?>')" value="<?echo Loc::getMessage("MAIN_RESTORE_DEFAULTS")?>">
    <?=bitrix_sessid_post();?>
</form>
<?$tabControl->End();?>

<script src="/local/components/notaext/plupload/lib/plupload/js/plupload.full.min.js" type="text/javascript"></script>
<script src="/local/components/notaext/plupload/lib/plupload/js/i18n/<?=LANGUAGE_ID?>.js" type="text/javascript"></script>
<script src="/local/modules/elr.useraccess/script-csv.js" type="text/javascript"></script>

<form method="POST" action="<?=$APPLICATION->GetCurPage()?>?lang=<?=LANGUAGE_ID?>&amp;mid=<?=urlencode($mid)?>" >
    <? echo CAdminMessage::ShowNote('Процедура выполняется лишь один раз ! Повтороное использование может привести к дублирубщим записям.'); ?>
    <?=bitrix_sessid_post()?>
    <input type="hidden" name="action" value="sync_first" >
    <input type="submit" name="sync_first" value="Выгрузить" >
    <input type="hidden" name="sync_abis" value="Y">
</form>





