/**
 * Created by AChechel on 20.02.2017.
 */
(function(bx, $){
    $(function() {
        var randStr = Math.random().toString().replace('0.', '');

        $('div.pl_button').attr('id', 'container_' + randStr);
        $('pre.plupload_console').attr('id', 'plupload_console_' + randStr);

        window["uploader_" + randStr] = new plupload.Uploader(
            {
                runtimes : 'html5,flash,html4',
                browse_button : 'file_csv',
                container: document.getElementById('container_' + randStr),
        url : '/local/components/notaext/plupload/ajax.php',
            filters : {
                max_file_size : '100000mb',
                prevent_duplicates: true,
                mime_types: [{title : "Mine files", extensions : 'csv'}]
        },
        max_file_size : '100000mb',
            chunk_size: '1mb',
            flash_swf_url : '/local/components/notaext/plupload/lib/plupload/js/Moxie.swf',
            multipart_params: {
            plupload_ajax: 'Y',
                sessid: BX.bitrix_sessid(),
                aFILE_TYPES : 'csv',
                aDIR : "tmp",
                aMAX_FILE_SIZE : '100000',
                aMAX_FILE_AGE : '30',
                aFILES_FIELD_NAME : '',
                aMULTI_SELECTION : 'N',
                aCLEANUP_DIR : 'Y',
                aRAND_STR : randStr
        },
        multi_selection: false,
            init: {
                FilesAdded: function(up, files) {
                    window["uploader_" + randStr].start();

                    $('#add_file_progress').addClass('hidden');
                    $("#add_file_error").removeClass('error').find('em.error').hide();
                    $('#uploaded_file').addClass('hidden');

                },
                FileUploaded: function(up, file, response) {
                    $('#add_file_error').find('em.error').hide();
                    $('#add_file_progress').addClass('hidden');
                    var result = response.response;
                    if (result)
                    {
                        var obResponse = JSON.parse(result);
                        $('#URL_DATA_FILE').val(obResponse.file);
                        $('#uploaded_file').removeClass('hidden');
                        $('#uploaded_file > a')
                            .attr('href', obResponse.file)
                            .text('файл загружен (просмотр)');

                        $('span.text_total_users').text('0');
                        $('span.text_upload_users').text('0');

                        $('span.lable_parsing_csv,span.label_total_users,span.text_total_users,span.label_upload_users,span.text_upload_users').show();

                        setTimeout(parsingCsv(obResponse.file), 1000);
                    }
                },
                UploadProgress: function(up, file) {
                    $('#add_file_progress').text(file.percent+ '%')
                        .removeClass('hidden');
                },

                Error: function(up, err) {
                    $('#add_file_error').find('em.error')
                        .hide();

                    document.getElementById('plupload_console_' + randStr).innerHTML += err.message + '<br>';
                    if(err.code == -601)
                    {
                        $("#add_file_error").addClass('error');
                        $('#add_file_error').find('em.error')
                            .text('Файл должен быть в CSV или TXT формате')
                            .show();
                    }
                }
            }
        });

        window['uploader_' + randStr].init();
    });

    var parsingCsv = function (filePath, line) {
        var start = line || 1;

        $.ajax({
            url:        '/local/modules/elr.useraccess/ajax.php',
            method:     'GET',
            dataType:   'JSON',
            data: {
                start: start,
                filePath: filePath
            },
            success: function(res){
                if(res.lines !== undefined && start === 1){ // Step 1
                    $('span.text_total_users').text(res.lines - 2);

                    parsingCsv(filePath, 3);
                } else {
                    if (res.lines == false){
                        console.log('All done !');
                    } else {
                        $('span.text_upload_users').text((res.lines - 2) + " (" + Math.round(((res.lines - 2) / (1 * $('span.text_total_users').text())) * 100) + "%)");
                        parsingCsv(filePath, res.lines);
                    }
                }
            }
        });
    }

})(BX, jQuery);