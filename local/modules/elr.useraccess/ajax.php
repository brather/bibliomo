<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 20.02.2017
 * Time: 14:40
 */
define("STOP_STATISTICS", true);
define("NOT_CHECK_PERMISSIONS", true);

require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php';

use \Bitrix\Main\Application;
use Bitrix\Main\Data\Cache;
use Bitrix\Main\Loader;
use Bitrix\Seo\Engine\Bitrix;
use Elr\Abis\userEvent;

$rq = Application::getInstance()->getContext()->getRequest();

if (!$rq->isAjaxRequest()) die();

/* Step 1 */
/* первый запрос на подсчет колличества строк */
if ($rq->get('filePath') && (int)$rq->get("start") === 1)
{
    $m1 = microtime();
    $rs = @fopen($_SERVER['DOCUMENT_ROOT'] . $rq->get('filePath'), "r");

    $lines = 0;
    $arTmp = array();

    /* Добавить в кэш значения первой и второй строки */
    while (($buffer = fgets($rs)) !== false)
    {
        $lines++;

        /* Первый две строки подготовить к кэшированию */
        if ($lines < 3)
        {
            $arTmp["LINE_" . $lines] = explode(";", trim($buffer));
        }
    }

/* Start cache */
    $cache = Cache::createInstance();
    $cacheID = md5($rq->get('filePath'));

    if ($cache->initCache(3600, $cacheID))
    {
        $cache->EndDataCache($arCache);
    }
    else if ($cache->startDataCache(3600, $cacheID))
    {
        $arCache = $arTmp;
        $arCache['LINES'] = $lines;

        $cache->endDataCache($arCache);
    }
/* End Cache */

    fclose($rs);
    $m2 = microtime() - $m1;

    die(json_encode(array("lines" => $lines, "time" => $m2)));
}

/* Step 2 */
/* Добавление пользователей */
if ($rq->get('filePath') && (int)$rq->get("start") > 1)
{
    $firstLines = array();

    /* Start init cache */
    $cache = Cache::createInstance();
    $cacheID = md5($rq->get('filePath'));

    if ($cache->initCache(3600, $cacheID))
    {
        $firstLines = $cache->getVars();
    }
    else { /* Если в кэше не найдется такого массива */
        $cache->abortDataCache();

        die(json_encode(array("err" => "NO data first and second lines from file")));
    }
    /* End init cache */

    if ((int)$rq->get("start") === (int)$firstLines['LINES'])
        die(json_encode(array('lines' => false)));

    $m1 = microtime();
    $rs = @fopen($_SERVER['DOCUMENT_ROOT'] . $rq->get('filePath'), "r");

    $lines = 0;
    $perStep = 60;

    $firstLines['LINE_2'][array_search('dateinput', $firstLines['LINE_2'])] = 'dateInput';

    Loader::includeModule('elr.useraccess');

    while (($user = fgets($rs)) !== false)
    {
        $lines++;
        /* Продолжим перебор строк до строки указанной в переменой $_GET['start'] */
        if ($lines < (int)$rq->get('start')) continue;

        /* Прекратим добавлять пользователей и вернем строку с которой надо начать в след раз */
        if ($lines === ((int)$rq->get('start') + $perStep)) break;

        /* Процедура добавления пользователей */
        /* Массив одной строки значений пользователя */
        $userFromLine = explode(";", trim($user));

        /* Комбинированный массив с ключами и значениями пользвоателя */
        $userFromLine = array_combine($firstLines['LINE_2'], $userFromLine);

        $userFromLine['id'] = "IRBIS_" . $firstLines['LINE_1'][1] . "_" . $userFromLine['id'];

        /* Сформируем ФИО */
        $userFromLine['fio'] = $userFromLine['family_name'] . " " . $userFromLine['first_name'] . " " . $userFromLine['middle_name'];

        /* ISIL библиотеки */
        $userFromLine['library'] = $firstLines['LINE_1'][1];

        unset($userFromLine['family_name'], $userFromLine['first_name'], $userFromLine['middle_name']);

        /* Добавлеяем недостающие параметры */
        if ( !isset($userFromLine['active']) )
            $userFromLine['active'] = true;

        //if ( !isset( $userFromLine['']))


        $eUser = new userEvent($userFromLine);
        $eUser->setRegisterType("irbis_register");
        $eUser->setFields(array(
            "UF_ABIS_ID" => "id",
            "EXTERNAL_AUTH_ID" => "id",
            "UF_DATE_INPUT" => "dateInput",
            "EMAIL" => "email",
            "fio" => array("NAME", "SECOND_NAME", "LAST_NAME"),
            "LOGIN" => "login",
            "PASSWORD" => "pass",
            "UF_ISIL" => "library",
            //"UF_LIBRARY", // обновить если "UF_ISIL" => "library" меняется
            "UF_NUM_ECHB" => "barcode",
            "ACTIVE" => "active"
        ));

        $eUser->doUser();
    }

    if ($lines )

    fclose($rs);
    $m2 = microtime() - $m1;

    die(json_encode(array("lines" => $lines, "time" => $m2, "f_s_l" => $firstLines)));
}
