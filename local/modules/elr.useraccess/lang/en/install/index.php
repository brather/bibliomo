<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 21.11.2016
 * Time: 10:46
 */
$MESS["ELR_UA_MODULE_NAME"] = "Users Access";
$MESS["ELR_UA_PARTNER_NAME"] = "Elar";
$MESS["ELR_UA_PARTNER_URI"] = "http://www.elar.ru";
$MESS["ELR_UA_USER_FIELD_ID_EDIT_FROM_LABEL"] = "The unique identifier united account";
$MESS["ELR_UA_ABIS_ID_EDIT_FROM_LABEL"] = "ABIS unique identifier account";
$MESS["ELR_UA_USER_FIELD_LIST_COLUMN_LABEL"] = "User field";
$MESS["ELR_UA_USER_FIELD_LIST_FILTER_LABEL"] = "User field";
$MESS["ELR_UA_USER_FIELD_ERROR_MASSAGE"] = "An error in completing the user field";
$MESS["ELR_UA_UNINSTALL_MODULE"] = "Remove module ";
$MESS["ELR_UA_INSTALL_MODULE"] = "Install ";
$MESS["ELR_UA_EXCEPTION_USER_FIELDS"] = "User Fields not adding.";
$MESS["ELR_UA_ISIL_EDIT_FROM_LABEL"] = "ISIL id";
$MESS["ELR_DATE_INPUT_LABEL"] = "Date registration";
$MESS["ELR_DATE_OF_FINISH_LABEL"] = "Date of finish ChB";