<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 23.11.2016
 * Time: 11:29
 */
$MESS['ELR_ABIS_SECRET_KEY'] = "Secret key";
$MESS["ELR_ABIS_HOST"] = "Host API";
$MESS["ELR_ABIS_PROTOCOL"] = "Protocol http(s)";
$MESS["ELR_ABIS_OPTION_API"] = "OpacGlobal API";
$MESS["ELR_ABIS_IRBIS_API"] = "IRBIS";