<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 23.11.2016
 * Time: 11:29
 */
$MESS['ELR_ABIS_SECRET_KEY'] = "Секретный ключ";
$MESS["ELR_ABIS_HOST"] = "Хост API";
$MESS["ELR_ABIS_PROTOCOL"] = "Протокол http(s)";
$MESS["ELR_ABIS_OPTION_API"] = "OpacGlobal API";
$MESS["ELR_ABIS_IRBIS_API"] = "ИРБИС";