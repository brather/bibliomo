<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 21.11.2016
 * Time: 10:46
 */
$MESS["ELR_UA_MODULE_NAME"] = "Пользовательский доступ";
$MESS["ELR_UA_PARTNER_NAME"] = "Элар";
$MESS["ELR_UA_PARTNER_URI"] = "http://www.elar.ru";
$MESS["ELR_UA_USER_FIELD_ID_EDIT_FROM_LABEL"] = "Уникальный идентификатор объединённой учётной записи";
$MESS["ELR_UA_ABIS_ID_EDIT_FROM_LABEL"] = "АБИС идентификатор учётной записи";
$MESS["ELR_UA_USER_FIELD_LIST_COLUMN_LABEL"] = "Пользовательское свойство";
$MESS["ELR_UA_USER_FIELD_LIST_FILTER_LABEL"] = "Пользовательское свойство";
$MESS["ELR_UA_USER_FIELD_ERROR_MASSAGE"] = "Ошибка при заполнении пользовательского свойства";
$MESS["ELR_UA_UNINSTALL_MODULE"] = "Удаление модуля ";
$MESS["ELR_UA_INSTALL_MODULE"] = "Установка ";
$MESS["ELR_UA_EXCEPTION_USER_FIELDS"] = "Пользовательские поля не добавлены.";
$MESS["ELR_UA_ISIL_EDIT_FROM_LABEL"] = "ISIL идентифактор бибилиотек";
$MESS["ELR_DATE_INPUT_LABEL"] = "Дата регистрации";
$MESS["ELR_DATE_OF_FINISH_LABEL"] = "Окончание читательского билета";
