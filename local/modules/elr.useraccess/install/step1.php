<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 21.11.2016
 * Time: 10:36
 */
use \Bitrix\Main\Localization\Loc;

IncludeModuleLangFile(__FILE__);

global $APPLICATION;
?>
<form action="<?= $APPLICATION->GetCurPage()?>" name="elr_useraccess_install">
    <?= bitrix_sessid_post()?>
    <input type="hidden" name="lang" value="<?= LANG ?>">
    <input type="hidden" name="id" value="elr.useraccess" >
    <input type="hidden" name="install" value="Y" >
    <input type="hidden" name="step" value="2" >
    <table>
        <tr>
            <td><label for="l-abis-secret-token">Секретный ключ</label></td>
            <td>
                <input class="bx-input" id="l-abis-secret-token" type="text" name="ABIS_SECRET_TOKEN" value="" required="required"></td>
        </tr>
        <tr>
            <td><label for="l-abis-host">Хост</label></td>
            <td><input id="l-abis-host" class="bx-input" type="text" name="ABIS_HOST" value="" required="required"></td>
        </tr>
        <tr>
            <td><label for="l-abis-protocol">Протокол</label></td>
            <td>
                <select id="l-abis-protocol" name="ABIS_PROTOCOL" required="required">
                    <option value="http" selected="selected">http</option>
                    <option value="https">https</option>
                </select>
            </td>
        </tr>
    </table>
    <p>
        <input type="submit" name="inst" value="<?= Loc::getMessage("MOD_INSTALL")?>">
    </p>
</form>
