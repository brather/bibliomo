<?php
IncludeModuleLangFile( __FILE__ );

use \Bitrix\Main\Localization\Loc;

if ( class_exists( "elr_useraccess" ) )
{
    return;
}

class elr_useraccess extends CModule
{
    public $MODULE_ID = 'elr.useraccess';
    public $MODULE_VERSION = null;
    public $MODULE_VERSION_DATE = null;
    public $MODULE_NAME = null;
    public $MODULE_DESCRIPTION = null;
    public $MODULE_GROUP_RIGHTS = "N";
    public $PARTNER_NAME = null;
    public $PARTNER_URI = null;

    /* @var CUserTypeEntity */
    private $CUserTypeEntity;

    function __construct()
    {
        $this->MODULE_NAME = Loc::getMessage( "ELR_UA_MODULE_NAME" );
        $this->PARTNER_NAME = Loc::getMessage( "ELR_UA_PARTNER_NAME" );
        $this->PARTNER_URI = Loc::getMessage( "ELR_UA_PARTNER_URI" );
        $this->MODULE_DESCRIPTION = "";

        if ( !isset( $this->CUserTypeEntity )
            && !is_object(
                $this->CUserTypeEntity
            )
        )
        {
            $this->CUserTypeEntity = new CUserTypeEntity();
        }

        $arModuleVersion = array();

        $path = str_replace( "\\", "/", __FILE__ );
        $path = substr( $path, 0, strlen( $path ) - strlen( "/index.php" ) );
        include( $path . "/version.php" );

        if ( is_array( $arModuleVersion )
            && array_key_exists(
                "VERSION", $arModuleVersion
            )
        )
        {
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        }
    }

    public function DoInstall()
    {
        global $APPLICATION, $step, $DB;


        if ( (int)$step < 2 )
        {
            if (
                !$this->addUfID()
                || !$this->addUFAbisID()
                || !$this->addUfDateInput()
                || !$this->addUfDateOfFinish()
                || !$this->addUFIsil()
                || !$this->addUfChanged()
            )
            {
                $APPLICATION->ThrowException(
                    Loc::getMessage( "ELR_UA_EXCEPTION_USER_FIELDS" )
                );
            }

            $DB->Query("UPDATE `b_uts_user` set `UF_MERGED_ID` = 0 WHERE ISNULL(`UF_MERGED_ID`) = 1");
            $DB->Query("UPDATE `b_uts_user` set `UF_CHANGED` = 0 WHERE `UF_CHANGED` IS NULL");

            /* Заведем новый тип регистрации */
            global $USER_FIELD_MANAGER;

            $arFields = $USER_FIELD_MANAGER->GetUserFields("USER");
            if (array_key_exists("UF_REGISTER_TYPE", $arFields))
            {
                $FIELD_ID = $arFields["UF_REGISTER_TYPE"]["ID"];

                $obEnum = new CUserFieldEnum;
                $rsNum = $obEnum->GetList(array(), array("XML_ID" => "abis_register"));
                /* если поле типа Список, уже имеется, добавлять ничего не будем */
                if (!($rs = $rsNum->Fetch() ))
                {
                    $obEnum->SetEnumValues($FIELD_ID, array(
                        "n0" => array(
                            "VALUE" => "Регистрация через ОПАК",
                            "XML_ID" => "abis_register"
                        )
                    ));
                }

                $rsNum = $obEnum->GetList(array(), array("XML_ID" => "irbis_register"));
                if (!($rs = $rsNum->Fetch() ))
                {
                    $obEnum->SetEnumValues($FIELD_ID, array(
                        "n0" => array(
                            "VALUE" => "Регистрация через ИРБИС",
                            "XML_ID" => "irbis_register"
                        )
                    ));
                }

                unset($arFields, $FIELD_ID, $obEnum, $rsNum, $rs);
            }

            $APPLICATION->IncludeAdminFile(
                Loc::getMessage( "ELR_UA_INSTALL_MODULE" ),
                __DIR__ . '/step1.php'
            );
        }

        if ( (int)$step === 2 )
        {
            RegisterModule( $this->MODULE_ID );

            $evMng = \Bitrix\Main\EventManager::getInstance();

            $evMng->registerEventHandler(
                "main",
                "OnUserLoginExternal",
                $this->MODULE_ID,
                '\Elr\Handler\ELRUser',
                "OnUserLoginExternal"
            );

            COption::SetOptionString(
                $this->MODULE_ID,
                "elr_useraccess_abis_secret_token",
                $_REQUEST['ABIS_SECRET_TOKEN']
            );
            COption::SetOptionString(
                $this->MODULE_ID,
                "elr_usersccess_abis_host",
                $_REQUEST['ABIS_HOST']
            );
            COption::SetOptionString(
                $this->MODULE_ID,
                "elr_usersccess_abis_protocol",
                $_REQUEST['ABIS_PROTOCOL']
            );

            COption::SetOptionString(
                $this->MODULE_ID,
                "sync_first_time",
                "N"
            );

            CAgent::AddAgent(
                'Elr\Agent\Users::sync();',
                'elr.useraccess',
                'N',
                86400,
                "",
                "Y",
                ConvertTimeStamp(time()+CTimeZone::GetOffset()+30, "FULL")
            );
        }
    }

    public function DoUninstall()
    {
        global $APPLICATION, $step;

        if ( (int)$step < 2 )
        {
            $APPLICATION->IncludeAdminFile(
                Loc::getMessage( "ELR_UA_UNINSTALL_MODULE" ) .
                $this->MODULE_NAME, __DIR__ . '/unstep1.php'
            );
        }
        else if ( (int)$step === 2 )
        {
            $this->_UFDelete( $this->deleteUF( "UF_MERGED_ID" ) );
            $this->_UFDelete( $this->deleteUF( "UF_ABIS_ID" ) );
            $this->_UFDelete( $this->deleteUF( "UF_ISIL" ) );
            $this->_UFDelete( $this->deleteUF( "UF_DATE_INPUT" ) );
            $this->_UFDelete( $this->deleteUF( "UF_DATE_OF_FINISH" ) );
            $this->_UFDelete( $this->deleteUF( "UF_CHANGED" ) );

            CAgent::RemoveAgent('Elr\Agent\Users::sync();',
                'elr.useraccess');

            COption::RemoveOption(
                $this->MODULE_ID,
                "elr_useraccess_abis_secret_token"
            );
            COption::RemoveOption(
                $this->MODULE_ID,
                "elr_usersccess_abis_host"
            );
            COption::RemoveOption(
                $this->MODULE_ID,
                "elr_usersccess_abis_protocol"
            );
            COption::RemoveOption(
                $this->MODULE_ID,
                "sync_first_time"
            );

            $evMng = \Bitrix\Main\EventManager::getInstance();

            $evMng->unRegisterEventHandler(
                "main",
                "OnUserLoginExternal",
                $this->MODULE_ID,
                '\Elr\Handler\ELRUser',
                "OnUserLoginExternal"
            );

            UnRegisterModule( $this->MODULE_ID );

            $APPLICATION->IncludeAdminFile(
                Loc::getMessage( "ELR_UA_UNINSTALL_MODULE" )
                . $this->MODULE_NAME, __DIR__ . '/unstep2.php'
            );
        }

    }

    /* Служебное: Дата окончания активации */
    protected function addUfDateOfFinish()
    {
        $arFields = array(
            'ENTITY_ID'       => 'USER',
            'FIELD_NAME'      => 'UF_DATE_OF_FINISH',
            'USER_TYPE_ID'    => 'date',
            'XML_ID'          => 'XML_DATE_OF_FINISH',
            'MANDATORY'       => 'N',
            /*
            * Показывать в фильтре списка. Возможные значения:
            * не показывать = N, точное совпадение = I,
            * поиск по маске = E, поиск по подстроке = S
            */
            'SHOW_FILTER'     => 'N',
            'SHOW_IN_LIST'    => 'N',
            'IS_SEARCHABLE'   => 'Y',
            /* Подпись в форме редактирования */
            'EDIT_FORM_LABEL' => array(
                'ru' => Loc::getMessage( "ELR_DATE_OF_FINISH_LABEL" ),
                'en' => Loc::getMessage( "ELR_DATE_OF_FINISH_LABEL" ),
            ),
            'SETTINGS'        => array(
                /* Значение по умолчанию */
                'DEFAULT_VALUE' => array(
                    'TYPE'  => 'NONE',
                    'VALUE' => '',
                ),
            ),
        );

        return $this->_UFAdd( $arFields );
    }

    /* Служебное: Дата добавления в АБИС */
    protected function addUfDateInput()
    {
        $arFields = array(
            'ENTITY_ID'       => 'USER',
            'FIELD_NAME'      => 'UF_DATE_INPUT',
            'USER_TYPE_ID'    => 'date',
            'XML_ID'          => 'XML_DATE_INPUT',
            'MANDATORY'       => 'N',
            /*
            * Показывать в фильтре списка. Возможные значения:
            * не показывать = N, точное совпадение = I,
            * поиск по маске = E, поиск по подстроке = S
            */
            'SHOW_FILTER'     => 'N',
            'SHOW_IN_LIST'    => 'N',
            'IS_SEARCHABLE'   => 'N',
            /* Подпись в форме редактирования */
            'EDIT_FORM_LABEL' => array(
                'ru' => Loc::getMessage( "ELR_DATE_INPUT_LABEL" ),
                'em' => Loc::getMessage( "ELR_DATE_INPUT_LABEL" ),
            ),
            'SETTINGS'        => array(
                /* Значение по умолчанию */
                'DEFAULT_VALUE' => array(
                    'TYPE'  => 'NONE',
                    'VALUE' => '',
                ),
            ),
        );

        return $this->_UFAdd( $arFields );
    }

    /* ISIL Идентификатор библиотеки */
    protected function addUFIsil()
    {
        $arFields = array(
            'ENTITY_ID'       => 'USER',
            'FIELD_NAME'      => 'UF_ISIL',
            'USER_TYPE_ID'    => 'string',
            'XML_ID'          => 'XML_ID_ISIL',
            'MANDATORY'       => 'N',
            'SHOW_FILTER'     => 'S',
            'SHOW_IN_LIST'    => 'Y',
            'IS_SEARCHABLE'   => 'Y',
            'EDIT_FORM_LABEL' => array(
                'ru' => Loc::getMessage(
                    "ELR_UA_ISIL_EDIT_FROM_LABEL", null, 'ru'
                ),
                'en' => Loc::getMessage(
                    "ELR_UA_ISIL_EDIT_FROM_LABEL", null, 'en'
                ),
            ),
            'SETTINGS'        => array(
                'DEFAULT_VALUE' => null,
                'SIZE'          => '20',
                'ROWS'          => '1',
                'MIN_LENGTH'    => '0',
                'MAX_LENGTH'    => '0',
                'REGEXP'        => '',
            ),
        );

        return $this->_UFAdd( $arFields );

    }

    /* Создаем пользовательское поле ABIS_ID */
    protected function addUFAbisID()
    {
        $arUserFieldsUSER = array(
            'ENTITY_ID'       => 'USER',
            'FIELD_NAME'      => 'UF_ABIS_ID',
            'USER_TYPE_ID'    => 'string',
            'XML_ID'          => 'XML_ABIS_ID',
            'MANDATORY'       => 'N',
            /*
            * Показывать в фильтре списка. Возможные значения:
            * не показывать = N, точное совпадение = I,
            * поиск по маске = E, поиск по подстроке = S
            */
            'SHOW_FILTER'     => 'S',
            'SHOW_IN_LIST'    => 'Y',
            'IS_SEARCHABLE'   => 'Y',
            /* Подпись в форме редактирования */
            'EDIT_FORM_LABEL' => array(
                'ru' => Loc::getMessage(
                    "ELR_UA_ABIS_ID_EDIT_FROM_LABEL", null, 'ru'
                ),
                'en' => Loc::getMessage(
                    "ELR_UA_ABIS_ID_EDIT_FROM_LABEL", null, 'en'
                ),
            ),
            'SETTINGS'        => array(
                /* Значение по умолчанию */
                'DEFAULT_VALUE' => null,
                /* Размер поля ввода для отображения */
                'SIZE'          => '20',
                /* Количество строчек поля ввода */
                'ROWS'          => '1',
                /* Минимальная длина строки (0 - не проверять) */
                'MIN_LENGTH'    => '0',
                /* Максимальная длина строки (0 - не проверять) */
                'MAX_LENGTH'    => '0',
                /* Регулярное выражение для проверки */
                'REGEXP'        => '',
            ),
        );

        return $this->_UFAdd( $arUserFieldsUSER );
    }

    /* Создаем пользовательское поле ID обединенный пользователь */
    protected function addUfChanged()
    {
        $arUserFieldsUSER = array(
            'ENTITY_ID'       => 'USER',
            'FIELD_NAME'      => 'UF_CHANGED',
            'USER_TYPE_ID'    => 'integer',
            'XML_ID'          => 'XML_ID_CHANGED',
            'SHOW_FILTER'     => 'N',
            'SHOW_IN_LIST'    => 'N',
            'IS_SEARCHABLE'   => 'Y',
            'EDIT_FORM_LABEL' => array(
                'ru' => "Измененная запись",
                'en' => "Changed item",
            ),
            'SETTINGS'        => array(
                'DEFAULT_VALUE' => 0,
                'SIZE'          => '20',
            ),
        );

        return $this->_UFAdd( $arUserFieldsUSER );
    }

    /* Создаем пользовательское поле ID обединенный пользователь */
    protected function addUfID()
    {
        $arUserFieldsUSER = array(
            'ENTITY_ID'       => 'USER',
            'FIELD_NAME'      => 'UF_MERGED_ID',
            'USER_TYPE_ID'    => 'integer',
            'XML_ID'          => 'XML_ID_MERGED_ID',
            /*
            * Показывать в фильтре списка. Возможные значения:
            * не показывать = N, точное совпадение = I,
            * поиск по маске = E, поиск по подстроке = S
            */
            'SHOW_FILTER'     => 'S',
            'SHOW_IN_LIST'    => 'Y',
            'IS_SEARCHABLE'   => 'Y',
            /* Подпись в форме редактирования */
            'EDIT_FORM_LABEL' => array(
                'ru' => Loc::getMessage(
                    "ELR_UA_USER_FIELD_ID_EDIT_FROM_LABEL", null, 'ru'
                ),
                'en' => Loc::getMessage(
                    "ELR_UA_USER_FIELD_ID_EDIT_FROM_LABEL", null, 'en'
                ),
            ),
            'SETTINGS'        => array(
                /* Значение по умолчанию */
                'DEFAULT_VALUE' => 0,
                /* Размер поля ввода для отображения */
                'SIZE'          => '20',
                /* Количество строчек поля ввода */
                'ROWS'          => '1',
                /* Минимальная длина строки (0 - не проверять) */
                'MIN_LENGTH'    => '0',
                /* Максимальная длина строки (0 - не проверять) */
                'MAX_LENGTH'    => '0',
                /* Регулярное выражение для проверки */
                'REGEXP'        => '',
            ),
        );

        return $this->_UFAdd( $arUserFieldsUSER );
    }

    /* Возвращает ID пользовательского свойства по его CODE */
    protected function deleteUF( $UF_CODE )
    {
        $res = $this->CUserTypeEntity->GetList(
            array(), array( "FIELD_NAME" => $UF_CODE )
        )->Fetch();
        if ( $res['FIELD_NAME'] === $UF_CODE )
        {
            return $res['ID'];
        }
        else
        {
            return 0;
        }
    }


    /**
     * @param $aUserField
     *
     * @return int
     */
    private function _UFAdd( $aUserField )
    {
        /**
         * Добавление пользовательского свойства
         */
        if ( !isset( $this->CUserTypeEntity )
            && !is_object(
                $this->CUserTypeEntity
            )
        )
        {
            $this->CUserTypeEntity = new CUserTypeEntity();
        }


        $aUserFields = array(
            'SORT'              => 500,
            'MULTIPLE'          => 'N',
            /* Обязательное или нет свойство */
            'MANDATORY'         => 'N',
            /*
            * Не разрешать редактирование пользователем.
            * Если передать какое-либо значение, то будет считаться,
            * что флаг выставлен.
            */
            'EDIT_IN_LIST'      => '',
            /* Заголовок в списке */
            'LIST_COLUMN_LABEL' => array(
                'ru' => Loc::getMessage(
                    "ELR_UA_LIST_COLUMN_LABEL", null, 'ru'
                ),
                'en' => Loc::getMessage(
                    "ELR_UA_LIST_COLUMN_LABEL", null, 'en'
                ),
            ),
            /* Подпись фильтра в списке */
            'LIST_FILTER_LABEL' => array(
                'ru' => Loc::getMessage(
                    "ELR_UA_LIST_FILTER_LABEL", null, 'ru'
                ),
                'en' => Loc::getMessage(
                    "ELR_UA_LIST_FILTER_LABEL", null, 'en'
                ),
            ),
            /* Сообщение об ошибке (не обязательное) */
            'ERROR_MESSAGE'     => array(
                'ru' => Loc::getMessage(
                    "ELR_UA_USER_FIELD_ERROR_MASSAGE", null, 'ru'
                ),
                'en' => Loc::getMessage(
                    "ELR_UA_USER_FIELD_ERROR_MASSAGE", null, 'en'
                ),
            ),
            /* Помощь */
            'HELP_MESSAGE'      => array(
                'ru' => '',
                'en' => '',
            ),
        );

        $aUserFields = array_merge( $aUserFields, $aUserField );

        return $this->CUserTypeEntity->Add( $aUserFields ); // int
    }

    /* провоцируем удаление ПС по его ID */
    private function _UFDelete( $UF_ID )
    {
        if ( (int)$UF_ID > 0 )
        {
            $this->CUserTypeEntity->Delete( $UF_ID );
        }
    }
}