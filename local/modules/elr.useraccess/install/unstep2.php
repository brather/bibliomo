<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 21.11.2016
 * Time: 10:37
 */
use Bitrix\Main\Localization\Loc;
global $APPLICATION;

if (!check_bitrix_sessid())
    return;

if ($ex = $APPLICATION->GetException()) {
    echo CAdminMessage::ShowMessage(
        array(
            'TYPE'    => 'ERROR',
            'MESSAGE' => Loc::getMessage('MOD_UNINST_ERR'),
            'DETAILS' => $ex->GetString(),
            'HTML'    => true
        )
    );
} else {
    echo CAdminMessage::ShowNote( Loc::getMessage('MOD_UNINST_OK'));
}

?>
<form action="<?= $APPLICATION->GetCurPage() ?>">
    <input type="hidden" name="lang" value="<?= LANG ?>">
    <input type="submit" name="" value="Вернуться">
</form>