<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 23.11.2016
 * Time: 12:20
 */
CModule::AddAutoloadClasses(
    "elr.useraccess",
    array(
        'Elr\Install\IABIS' => 'lib/iabis.php',
        'Elr\Install\FirstUploadUsers' => 'lib/firstuploadusers.php',
        'Elr\Install\AbisAddSyncuser' => 'lib/abisaddsyncuser.php',
        'Elr\Handler\ELRUser' => 'lib/elruser.php',
        'Elr\Logger' => 'lib/logger.php',
        'Elr\Abis\Sync' => 'lib/abis/sync.php',
        'Elr\Abis\userEvent' => 'lib/abis/userevent.php',
        'Elr\CollectionLibraries' => 'lib/abis/collectionlibraries.php',
        'Elr\UserMerged' => 'lib/usermerged.php',
        'Elr\AddUser' => 'lib/adduser.php',
        'Elr\Iblock\IblockElementPropertyS4Table' => 'lib/iblockelementproperty.php',
        'Elr\Agent\Users' => 'lib/agent/users.php',
    )
);