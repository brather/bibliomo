<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 05.12.2016
 * Time: 12:15
 */

namespace Elr;

use Elr\Logger;

class UserMerged
{
    const IS_MERGE = -1;
    const LOGGER = 1;


    static function gluing ( $gluingIDsUsers = array(), $arFieldsNewUser = array() )
    {
        if ( count($gluingIDsUsers) < 1 || count($arFieldsNewUser) < 8 )
        {
            if (self::LOGGER)
                Logger::pull( '$gluingIDsUsers или $arFieldsNewUser не массив данных или пуст ' . "\n" . __FILE__ . ":" . __LINE__ );
            return false;
        }

        $arFields = array(
            "LOGIN", "EMAIL", "NAME", "LAST_NAME", "SECOND_NAME", "GROUP_ID", "PASSWORD", "CONFIRM_PASSWORD"
        );

        $arDiff = array_diff_key(array_flip($arFields), $arFieldsNewUser);

        unset($arFields);

        /* На случай если не все поля придут */
        if ( count($arDiff) > 0 )
        {
            if (self::LOGGER)
                Logger::pull( "ERROR: Не заполнены обязательные поля: " . implode(", ", array_flip($arDiff)) . "\n" . __FILE__ . ":" . __LINE__ );
            return array("ERROR" => "Не заполнены обязательные поля: " . implode(", ", array_flip($arDiff)));
        }

        $arFields = array(
            "LOGIN" => $arFieldsNewUser['LOGIN'],
            "EMAIL" => $arFieldsNewUser['EMAIL'],
            "ACTIVE" => "Y",
            "PASSWORD" => $arFieldsNewUser['PASSWORD'],
            "CONFIRM_PASSWORD" => $arFieldsNewUser['CONFIRM_PASSWORD'],
            "UF_MERGED_ID" => static::IS_MERGE,
            "GROUP_ID" => $arFieldsNewUser['GROUP_ID'],
            "NAME" => $arFieldsNewUser["NAME"],
            "LAST_NAME" => $arFieldsNewUser["LAST_NAME"],
            "SECOND_NAME" => $arFieldsNewUser["SECOND_NAME"],
            "LID" => "s1",
        );

        {/* Пользовательское поле "Тип регистрации" типас "Список" */
            $cuEnum = new \CUserFieldEnum();
            $db = $cuEnum->GetList(array(), array("XML_ID" => "abis_register"));

            if ($res = $db->Fetch())
                $arFields["UF_REGISTER_TYPE"] = array($res["ID"]);

            unset($res, $db);
        }

        $u = new \CUser();
        $err = array();
        $res = array();
        if ($ID = $u->Add($arFields))
        {
            $res[$ID] = array("IS_NEW" => true, "FULL_NAME" => $arFields['LAST_NAME']. " " . $arFields['NAME']. " " . $arFields['SECOND_NAME']. " ");
            foreach ($gluingIDsUsers["ID"] as $id)
            {
                $us = \CUser::GetByID($id)->Fetch();
                if ($us['UF_MERGED_ID'] > 0 || $us['UF_MERGED_ID'] == -1)
                {
                    if ( static::LOGGER )
                        Logger::pull("ERROR: Попытка склеить уже склеенные уч. записи, либо склеивание базовой записи. UF_MERGED_ID ({$us['UF_MERGED_ID']}) для пользователя с ID ($id)");
                    $err[] = "Попытка склеить уже склеенные уч. записи, либо склеивание базовой записи.";
                    continue;
                }

                if( !( $u->Update( $id, array("UF_MERGED_ID" => $ID) ) ) )
                {
                    if ( static::LOGGER )
                        Logger::pull("ERROR: Ошибка обновления поля UF_MERGED_ID ($ID) для пользователя с ID ($id) \n" . $u->LAST_ERROR );
                    $err[] = $u->LAST_ERROR;
                } else {
                    $res[$id] = array("FULL_NAME" => $us["LAST_NAME"]. " " . $us["NAME"] . " " . $us["SECOND_NAME"]);
                }
            }
        }

        return array("ERROR" => $err, "RESULT" => $res);
    }
}