<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 24.11.2016
 * Time: 12:03
 */

namespace Elr\Install;


class AbisAddSyncUser
{
    private $CUser = null;
    public $error = null;
    private $collLibrary = null;

    function __construct()
    {
        if ( is_null($this->CUser) )
            $this->CUser = new \CUser();

    }

    function checkUser ( $AbisID, $return_ID = false )
    {
        $dbRes = $this->CUser->GetList(
            ($by = "SORT"), ($order = "DESC"),
            array("EXTERNAL_AUTH_ID" => $AbisID),
            array(
                "SELECT"  => array("UF_ABIS_ID"),
                "FIELDS"  => array("ID", "EXTERNAL_AUTH_ID", "UF_ABIS_ID"),
            )
        );

        while ($user = $dbRes->Fetch())
        {
            /* Для удаления пользователя по ео ID */
            if ( $return_ID === true )
                return $user['ID'];

            return false;
        }

        unset($by, $order, $users, $user);

        return true;
    }

    function pushUser( $data )
    {
        /* Сформировать массив XML_ID => ID библиотек */
        if ( is_null( $this->collLibrary) )
        {
            $dbRes = \Bitrix\Iblock\ElementTable::getList(array(
                    "select" => array("ID", "XML_ID"),
                    "filter" => array("IBLOCK_ID" => 4, "ACTIVE" => "Y")
                ));
            while ($lib = $dbRes->fetch() )
            {
                $this->collLibrary[$lib["XML_ID"]] = $lib['ID'];
            }
        }

        $res = array(
                "del" => array("Удалено пользователей:"),
                "add" => array("Добавлено пользователей:")
            );
        foreach ( $data as $user )
        {
            if ( $user['deleted'] === true )
            {
                $USER_ID = $this->checkUser( $user['id'], true );

                if ( is_int($USER_ID) && $USER_ID > 0 )
                    $this->CUser->Delete($USER_ID);

                $d = (count($res['del']) == 1) ? 1 : $res['del'][1] + 1;
                $res['del'][1] = $d;

                continue;
            }

            if (!$this->checkUser($user['id'])) continue;

            if (empty($user['email']) || mb_strlen($user['email']) <= 3 )
                $email = str_replace('\\', '', $user['id']) . "@example.com";
            else
                $email = $user['email'];

            if (empty($user['login']) || mb_strlen($user['login']) < 3 )
                $login = $email;
            else
                $login = $user['login'];

            $user['dateInput']      = preg_replace("/([0-9]{4})([0-9]{2})([0-9]{2})/", "$3.$2.$1", substr($user['dateInput'], 0, 8));
            $user['dateOfFinish']   = preg_replace("/([0-9]{4})([0-9]{2})([0-9]{2})/", "$3.$2.$1", $user['dateOfFinish']);

            $fio = explode(" ", $user['fio']);
            $arFields = array(
                "ACTIVE"            => ($user['active'] === false)? "N" : "Y",
                "UF_ABIS_ID"        => $user['id'],
                "EMAIL"             => $email,
                "LOGIN"             => $login,
                "LAST_NAME"         => trim($fio[0]),
                "NAME"              => trim($fio[1]),
                "SECOND_NAME"       => trim($fio[2]),
                "UF_NUM_ECHB"       => $user['barcode'],
                "EXTERNAL_AUTH_ID"  => $user['id'],
                "GROUP_ID"          => array(3,4),
                "PASSWORD"          => $user['pass'],
                "CONFIRM_PASSWORD"  => $user['pass'],
                "UF_ISIL"           => $user['library'],
                "UF_DATE_INPUT"     => $user['dateInput'],
                "UF_DATE_OF_FINISH" => $user['dateOfFinish'],
                "UF_LIBRARY"        => $this->collLibrary[ $user['library'] ],
                "LID"               => "s1"
            );

            {/* Пользовательское поле "Тип регистрации" типа "Список" */
                $cuEnum = new \CUserFieldEnum();
                $db = $cuEnum->GetList(array(), array("XML_ID" => "abis_register"));

                if ($res = $db->Fetch())
                    $arFields["UF_REGISTER_TYPE"] = (int)$res["ID"];

                unset($res, $db);
            }

            $USER_ID = $this->CUser->Add( $arFields );

            if (!$USER_ID)
            {
                $this->error[] = $this->CUser->LAST_ERROR;
                $res['err'][] = $this->CUser->LAST_ERROR;
            }
            else
            {
                $a = (count($res['add']) == 1) ? 1 : $res['add'][1] + 1;
                $res['add'][1] = $a;
            }
        }

        return $res;
    }
}