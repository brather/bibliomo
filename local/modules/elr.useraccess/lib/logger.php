<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 02.12.2016
 * Time: 9:26
 */

namespace Elr;


class Logger
{
    private static $file = "logger";
    private static $ext = ".log";
    const SAVE          = true;

    static function pull($mess)
    {
        if (self::SAVE === true)
        {
            $filePath = $_SERVER["DOCUMENT_ROOT"] . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . self::$file . "." . date("Ymd") . self::$ext;
            $str = "[" . date(DATE_ATOM) . "]\t" . $mess. "\n";

            if (file_exists($filePath))
                file_put_contents($filePath, $str, FILE_APPEND | LOCK_EX);
            else
                file_put_contents($filePath, $str);
        }
    }
}