<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 06.12.2016
 * Time: 11:04
 */

namespace Elr;

use Elr\UserMerged;

class AddUser
{
    private $data;
    private $main;
    private $ids;

    function __construct($data, $ids, $main)
    {
        $this->data = $data;
        $this->main = $main;
        $this->ids = array("ID" => $ids);
    }

    function gluingUser()
    {
        if (!$this->data)
            return false;

        $pass = md5(microtime() - 365);
        $arFieldsNewUser = array(
            "NAME" => $this->data['NAME'],
            "LAST_NAME" => $this->data['LAST_NAME'],
            "SECOND_NAME" => $this->data['SECOND_NAME'],
            "LOGIN" => md5(time()),
            "EMAIL" => md5(time()) ."@example.com",
            "PASSWORD" => $pass,
            "CONFIRM_PASSWORD" => $pass,
            "GROUP_ID" => array(3,4)
        );

        $res = UserMerged::gluing($this->ids, $arFieldsNewUser);

        if (count($res['ERROR']) > 0 )
        {
            return $res['ERROR'];
        }

        return $res['RESULT'];
    }
}