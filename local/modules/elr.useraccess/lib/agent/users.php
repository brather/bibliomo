<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 08.12.2016
 * Time: 9:55
 */

namespace Elr\Agent;

use Elr\Abis\Sync;

class Users
{
    static private $limit = 1;
    static private $offset = 1;
    static private $method = 'reports/plugins/portal/readers/';

    static function sync($time = false)
    {
        $sync = Sync::getInstance();
        $sync->setLimit(self::$limit);
        $sync->setOffset(self::$offset);
        $sync->setMethod(self::$method);

        $t = date("Ymd") ."T". date('His');
        $sync->setTime2($t);

        if ($time !== false)
            $sync->setTime1($time);

        $sync->synchronize();

        return 'Elr\Agent\Users::sync("' . $t . '");';
    }
}