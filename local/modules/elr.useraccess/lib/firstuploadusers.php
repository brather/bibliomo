<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 23.11.2016
 * Time: 12:25
 *
 * Singleton
 */

namespace Elr\Install;

class FirstUploadUsers  extends IABIS
{
    private static $instance = null;
    private $perIteration = 1;
    private $numIteration = 1;
    const MODULE_ID = "elr.useraccess";
    private $connData = array();

    private function __construct()
    {
        if (count($this->connData) <= 0)
        {
            $this->connData["SECRET_TOKEN"] = $this->getOptionValue("elr_useraccess_abis_secret_token");
            $this->connData["HOST"]         = $this->getOptionValue("elr_usersccess_abis_host");
            $this->connData["PROTOCOL"]     = $this->getOptionValue("elr_usersccess_abis_protocol");
        }

    }

    public static function getInstance()
    {
        if ( is_null(self::$instance) )
            self::$instance = new self();

        return self::$instance;
    }

    /**
     * @return int
     */
    public function getPerIteration()
    {
        return $this->perIteration;
    }

    /**
     * @param int $perIteration
     */
    public function setPerIteration( $perIteration )
    {
        $this->perIteration = $perIteration;
    }

    /**
     * @return false|array (assoc)
     */
    public function getRequestData(  )
    {
        $offset =
            ( $this->numIteration > 1  ) ?
                '&offset=' . $this->numIteration : '';
        $req = $this->curlRequest(
            'reports/plugins/portal/readers',
            'limit='.$this->perIteration.$offset
        );

        $this->numIteration = $this->numIteration + $this->perIteration;

        return json_decode($req, true);
    }

    /**
     * @param $optionName
     *
     * @return bool|mixed|null|string
     */
    private function getOptionValue ( $optionName )
    {
        return \COption::GetOptionString(
            static::MODULE_ID,
            $optionName
        );
    }

}