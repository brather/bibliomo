<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 28.11.2016
 * Time: 16:21
 */

namespace Elr\Handler;


class ELRUser
{
    function OnUserLoginExternal( &$arParams )
    {
        /* Искать пользователя в стандатр*/
        if ( $arParams['PASSWORD_ORIGINAL'] == "Y" )
        {
            $dbRes = \CUser::GetByLogin( $arParams["LOGIN"] );

            if ( $user = $dbRes->Fetch() )
            {
                /* 8 символов соли */
                $salt = substr($user["PASSWORD"], 0, -32);

                /* Дополнительная проверка для пользователей выгруженных из АБИС */
                /* Проверить что пароль в md5 совпадает с закодированным паролем в системе */
                if ( $user['PASSWORD'] === $salt.md5($salt.md5($arParams["PASSWORD"])) )
                {
                    $arParams['PASSWORD'] = md5($arParams['PASSWORD']);
                    $arParams['PASSWORD_ORIGINAL'] = "N";
                }

                return $user['ID'];
            }
        }

        return false;
    }


}