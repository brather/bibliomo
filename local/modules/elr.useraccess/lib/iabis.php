<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 23.11.2016
 * Time: 13:03
 */

namespace Elr\Install;

use Elr\Logger;

abstract class IABIS
{
    private $connData = array();


    /**
     * @return void
     */
    final private function setConnData()
    {
        $this->connData["SECRET_TOKEN"] = \COption::GetOptionString(
            "elr.useraccess",
            "elr_useraccess_abis_secret_token");
        $this->connData["HOST"]         = \COption::GetOptionString(
            "elr.useraccess",
            "elr_usersccess_abis_host");
        $this->connData["PROTOCOL"]     = \COption::GetOptionString(
            "elr.useraccess","elr_usersccess_abis_protocol");
    }

    /**
     * @param        $requestUri
     * @param        $urlParams
     * @param string $method
     * @param array  $postData
     *
     * @return mixed
     */
    final protected function curlRequest( $requestUri, $urlParams, $method = 'GET', $postData = array() )
    {
        $this->setConnData();

        $url    = $this->connData['PROTOCOL'] . "://";
        $url   .= $this->connData['HOST'] . "/";
        $url   .= $requestUri . "?" . $urlParams;

        if ( !empty($urlParams) )
            $url   .= "&";

        $url   .= "secret=" . $this->connData['SECRET_TOKEN'];

        Logger::pull("Url API: " . $url);

        $ch = \curl_init($url);

        if ( $method == "POST" )
            \curl_setopt($ch, CURLOPT_POST, true);

        if ( $method == "POST" && count($postData) > 0 )
            \curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postData));

        \curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $data =  \curl_exec($ch);
        \curl_close($ch);

        return $data;
    }
}