<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 06.12.2016
 * Time: 17:28
 */
namespace Elr\Iblock;

use Bitrix\Main\Entity;

class IblockElementPropertyS4Table extends Entity\DataManager
{
    public static function getFilePath()
    {
        return __FILE__;
    }

    public static function getTableName()
    {
        return 'b_iblock_element_prop_s4';
    }

    public static function getMap()
    {
        global $DB;

        $varType = array(
            "int(11)" => "integer",
            "text" => "string",
            "longtext" => "string",
            "decimal(18,4)" => "float",
            "varchar(255)" => "string"
        );

        $db = $DB->Query("SHOW FIELDS FROM `" . self::getTableName() . "`");
        $fields = array();

        while ($f = $db->Fetch())
        {
            $fields[ $f['Field'] ] = array(
                'data_type' => $varType[ $f['Type'] ],
                'title' => $f['Field'],
                'primary' => ( $f['Key'] ) ? true : false,
                'required' => ( $f['Null'] == "NO" ) ? true : false
            );
        }

        return $fields;
    }
}