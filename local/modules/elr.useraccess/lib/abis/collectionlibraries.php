<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 05.12.2016
 * Time: 11:00
 */

namespace Elr;


class CollectionLibraries
{
    private static $inst;
    private $collectionLibraries;

    private function __construct($dataArray = false)
    {
        if (empty($this->collectionLibraries))
        {
            $dbRes = \Bitrix\Iblock\ElementTable::getList(array(
                "select" => array("ID", "XML_ID", "NAME"),
                "filter" => array("IBLOCK_ID" => 4, "ACTIVE" => "Y")
            ));
            $tmp = [];
            if ($dataArray === true)
            {
                while ($lib = $dbRes->fetch() )
                    $tmp[$lib["XML_ID"]] = $lib;
            }
            else
            {
                while ($lib = $dbRes->fetch() )
                    $tmp[$lib["XML_ID"]] = $lib['ID'];
            }


            $this->setCollectionLibraries($tmp);

            unset ($dbRes, $lib, $tmp);
        }
    }

    static function getInstance($dataArray = false)
    {
        if (!is_object(self::$inst))
            self::$inst = new static($dataArray);

        return self::$inst;
    }

    /**
     * @return array
     */
    public function getCollectionLibraries($key = null)
    {
        if (!is_null($key))
            return $this->collectionLibraries[ $key ];

        return $this->collectionLibraries;
    }

    /**
     * @param array $collectionLibraries
     */
    private function setCollectionLibraries( $collectionLibraries )
    {
        $this->collectionLibraries = $collectionLibraries;
    }


}