<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 01.12.2016
 * Time: 16:04
 */

namespace Elr\Abis;

use Bitrix\Main\DB\Exception;
use Bitrix\Main\Loader;
use Elr\Install\IABIS,
    Elr\Logger,
    Elr\Abis\userEvent;

class Sync extends IABIS
{
    private $limit;
    private $offset;
    private $step = 1;
    private $method;
    const LOGGER = 1;
    private static $inst;
    private $time1;
    private $time2;

    private function __construct($limit = 10, $offset = 1)
    {
        $this->limit = $limit;
        $this->offset = $offset;
    }

    public static function getInstance()
    {
        if (!is_object(self::$inst))
            self::$inst = new static();

        return self::$inst;
    }

    /**
     * @param int $limit
     */
    public function setLimit( $limit )
    {
        $this->limit = $limit;
    }

    /**
     * @param int $offset
     */
    public function setOffset( $offset )
    {
        $this->offset = $offset;
    }

    /**
     * @param mixed $method
     */
    public function setMethod( $method )
    {
        $this->method = $method;
    }

    public function getRequest()
    {
        if (!$this->method)
        {
            if (self::LOGGER == 1)
                Logger::pull("ERROR: Not set method for request! Call before init \$this->setMethod(\"\$uri\").");

            return false;
        }
        if ($this->step === 1 && self::LOGGER == 1)
        {
            Logger::pull( "Start sync users." );
            Logger::pull( "Method: " . $this->method );
        }

        $urlParam = "limit=" . $this->limit . "&offset=" . $this->offset;

        if ($this->time1)
            $urlParam .= "&time1=" . $this->time1;

        if ($this->time2)
            $urlParam .= "&time2=" . $this->time2;

        $ts = microtime();
        $data = $this->curlRequest($this->method, $urlParam);
        $te = microtime() - $ts;
        if (self::LOGGER == 1)
            Logger::pull("Step " . $this->step ."\t| Time execute: " . $te . "s");

        return $data;
    }

    /**
     * @throws Exception
     */
    public function synchronize()
    {
        if ( $json = $this->getRequest() )
        {
            $arData = json_decode($json, true);

            /* Вызовы на проверку и добавление пользователей */
            if ( $arData['meta']['returned'] !== 0 )
            {
                if ( $this->step === 1 )
                    if (self::LOGGER == 1)
                        Logger::pull("Found users - " . $arData['meta']['found']);

                if (self::LOGGER == 1)
                    Logger::pull("JSON: " . $json);

                foreach ( $arData['readers'] as $reader)
                {
                    $aUs = new userEvent($reader);
                    $aUs->doUser();
                }

                $this->step++;

                $this->offset = $this->offset + $this->limit;
                $this->synchronize();
            }
            else
            {
                if (self::LOGGER == 1)
                    Logger::pull("End synchronization users.");
            }
        }
        else
        {
            if (self::LOGGER == 1)
                Logger::pull("End synchronization with error!");
            throw new Exception("Not set method uri for request!");
        }
    }

    /**
     * @param mixed $time2
     */
    public function setTime2( $time2 )
    {
        $this->time2 = $time2;
    }

    /**
     * @param mixed $time1
     */
    public function setTime1( $time1 )
    {
        $this->time1 = $time1;
    }
}