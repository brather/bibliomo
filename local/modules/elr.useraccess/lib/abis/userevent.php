<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 02.12.2016
 * Time: 11:27
 */

namespace Elr\Abis;

use Elr\Logger,
    Elr\CollectionLibraries;

class userEvent
{
    public $CUser;
    public $users;
    public $abisUser;
    private $collLibrary = null;
    private $registerType = "abis_register";
    private $fields = array(
        "UF_ABIS_ID" => "id",
        "EXTERNAL_AUTH_ID" => "id",
        "UF_DATE_INPUT" => "dateInput",
        "EMAIL" => "email",
        "fio" => array("NAME", "SECOND_NAME", "LAST_NAME"),
        "LOGIN" => "login",
        "PASSWORD" => "pass",
        "UF_ISIL" => "library",
        //"UF_LIBRARY", // обновить если "UF_ISIL" => "library" меняется
        "UF_NUM_ECHB" => "barcode",
        "UF_DATE_OF_FINISH" => "dateOfFinish",
        "ACTIVE" => "active"
    );

    /**
     * userEvent constructor.
     */
    function __construct($abisUser)
    {
        $this->CUser = new \CUser();
        $this->abisUser = $abisUser;

        if ( is_null( $this->collLibrary) )
        {
            $this->collLibrary = CollectionLibraries::getInstance()->getCollectionLibraries();
        }
    }

    function doUser()
    {
        $hand = $this->checkUser();

        /* если придет false, это ситуация когда пришел маркер на удаление пользователя, а такого нет в системе */
        if ($hand !== false)
        {
            if (!empty($hand['FIELDS']['ID']))
                $this->{$hand["METHOD"]}($hand['FIELDS']['ID'], $hand['FIELDS'], $hand['sendNotice']);
            else
                $this->{$hand["METHOD"]}($hand['FIELDS'], $hand['sendNotice']);
        }
        else
            Logger::pull("Exception: Пользователь АБИС [" . $this->abisUser['id'] ."] удален, в системе такой не обнаружен");
    }

    function checkUser()
    {
        /** @var \CDBResult $db */
        $db = $this->userByAbis( $this->abisUser['id']);

        /* Подразумевается только один пользователь в системе с таким ABIS_ID */
        if ($user = $db->Fetch())
        {
            $u = \CUser::GetByID($user['VALUE_ID']);
            $sendNotice = false;

            if( $us = $u->Fetch() )
            {
                $arUpdateFields = array();

                foreach ( $this->fields as $key => $field )
                {
                    /* Обновляем если Знаячение поля пользователя не равно значению поля пришедшегго массива из АБИС */
                    /* Проверяются только поля совпадающие в массиве $this->fields */
                    if ( $us[$key] != $this->abisUser[$field] )
                    {
                        if ($key === "LOGIN")
                        {
                            if ($dU = $this->checkByLogin($this->abisUser[$field])->Fetch())
                            { /* Пользователь с таким логином уже существует, а пришел обновленный логин */
                                if (isset($this->abisUser["email"]) && !empty($this->abisUser["email"]))
                                { /* Подмена логина емайлом, если он есть */
                                    $this->abisUser[$field] = $this->abisUser["email"];
                                    $sendNotice = true;
                                }
                                else
                                { /* Емайл не пришел, фармируем логин из ABIS ID */
                                    $this->abisUser[$field] = preg_replace("/\\\/", "", $this->abisUser["id"]) ."@example.com";
                                }
                            }
                            else
                            { /* Уникальный логин для системы, сформируем емайл из ABIS ID */
                                if (!isset($this->abisUser["email"]) || empty($this->abisUser["email"]))
                                    $this->abisUser["email"] = $arUpdateFields["EMAIL"] = preg_replace("/\\\/", "", $this->abisUser["id"]) ."@example.com";
                            }
                        }

                        /* Абсурдная ситуация если на емаль захочет обновлятся в пустую строку */
                        if ($key === "EMAIL" && !empty($us[$key]) && empty($this->abisUser[$field]))
                        {
                            continue;
                        }

                        $arUpdateFields[$key] = $this->abisUser[$field];
                    }

                    if ($key === "LOGIN" && $this->abisUser[$field] === null)
                    {
                        unset($arUpdateFields[$key]);
                    }

                    if ($key === "ACTIVE")
                    {
                        $arUpdateFields[$key] = $this->abisUser[$field]? "Y" : "N";
                    }

                    /* Ситуация для fio */
                    if ($key == "fio")
                    {
                        $fio = explode(" ", $this->abisUser[$key]);
                        if ($fio[0] !== $us["LAST_NAME"])
                            $arUpdateFields["LAST_NAME"] = $fio[0];
                        if ($fio[1] !== $us["NAME"])
                            $arUpdateFields["NAME"] = $fio[1];
                        if ($fio[2] !== $us["SECOND_NAME"])
                            $arUpdateFields["SECOND_NAME"] = $fio[2];
                    }

                    if ( $key == "UF_DATE_INPUT" )
                        $arUpdateFields['UF_DATE_INPUT'] = preg_replace("/([0-9]{4})([0-9]{2})([0-9]{2})/", "$3.$2.$1", substr($this->abisUser[$field], 0, 8));

                    if ( $key == "UF_DATE_OF_FINISH")
                        $arUpdateFields['UF_DATE_OF_FINISH'] = preg_replace("/([0-9]{4})([0-9]{2})([0-9]{2})/", "$3.$2.$1", $this->abisUser[$field]);

                    if ( $key == "library")
                    {
                        $arUpdateFields['UF_LIBRARY'] = $this->collLibrary[ $this->abisUser[$field] ];
                        $arUpdateFields['UF_ISIL'] = $this->collLibrary[ $this->abisUser[$field] ];
                    }

                    /* проверка  пароля. не меняем пароль если он не пришел или равен null */
                    if ( $key == 'password')
                    {
                        /* это md5 пароль ? */
                        if ( mb_strlen($this->abisUser[$field]) == 32 )
                        {
                            $salt = substr($us["PASSWORD"], 0, mb_strlen($us["PASSWORD"]) - 32);

                            $passConvert =  $salt . md5($salt . $this->abisUser[$field]);

                            /* Нужно обновить если не совпадают */
                            if ($passConvert !== $u["PASSWORD"])
                            {
                                $arUpdateFields["PASSWORD"] = $this->abisUser[$field];
                                $arUpdateFields["CONFIRM_PASSWORD"] = $this->abisUser[$field];
                            }
                        }
                    }
                }

                /* Маркер на измененную учетную запись */
                $arUpdateFields['UF_CHANGED'] = 0;

                $arUpdateFields['ID'] = $us['ID'];

                return array("METHOD" => "userUpdate", "FIELDS" =>  $arUpdateFields, "sendNotice" => $sendNotice);
            }
        }

        /* Процедура проверки для добавления нового пользователя */

        $byLogin = $this->checkByLogin($this->abisUser['login']);

        $byEmail = $this->checkByEmail($this->abisUser['email']);

        $fio = explode(" ", $this->abisUser['fio']);

        $arUpdateFields = array(
            "ACTIVE"            => ($this->abisUser['active'] === false)? "N" : "Y",
            "UF_ABIS_ID"        => $this->abisUser['id'],
            "LAST_NAME"         => trim($fio[0]),
            "NAME"              => trim($fio[1]),
            "SECOND_NAME"       => trim($fio[2]),
            "UF_NUM_ECHB"       => $this->abisUser['barcode'],
            "EXTERNAL_AUTH_ID"  => $this->abisUser['id'],
            "GROUP_ID"          => array(3,4),
            "PASSWORD"          => $this->abisUser['pass'],
            "CONFIRM_PASSWORD"  => $this->abisUser['pass'],
            "UF_ISIL"           => $this->abisUser['library'],
            "UF_DATE_INPUT"     => preg_replace("/([0-9]{4})([0-9]{2})([0-9]{2})/", "$3.$2.$1", substr($this->abisUser['dateInput'], 0, 8)),
            "UF_DATE_OF_FINISH" => preg_replace("/([0-9]{4})([0-9]{2})([0-9]{2})/", "$3.$2.$1", $this->abisUser['dateOfFinish']),
            "UF_LIBRARY"        => $this->collLibrary[ $this->abisUser['library'] ],
            "UF_CHANGED"        => 0,
            "LID"               => "s1"
        );

        /* логин уже такой ЕСТЬ */
        if ( $byLogin->SelectedRowsCount() > 0 )
        {
            if ($this->abisUser['deleted'] === true)
                return array("METHOD" => "deleteUser", "FIELDS" =>  $byLogin->Fetch()["ID"], "sendNotice" => false);

            if ( $byEmail === false && empty($this->abisUser['email'] ) )
            {
                /* генерируем email из АБИС ID и делаем его же логином */
                $arUpdateFields['LOGIN'] = $arUpdateFields['EMAIL'] = preg_replace("/\\\/", "", $this->abisUser["id"]) ."@example.com";
            }
            else if ( $byEmail->SelectedRowsCount() == 0 )
            {
                /* email из АБИС становится логином битрикс */
                $arUpdateFields['LOGIN'] = $arUpdateFields['EMAIL'] = $this->abisUser['email'];
            }
            else
            {
                /* обновляем пользователя */
                $fetch = $byLogin->Fetch();
                $arUpdateFields["ID"] = $fetch['ID'];

                return array("METHOD" => "userUpdate", "FIELDS" =>  $arUpdateFields, "sendNotice" => true);
            }
        }/* Логина НЕТ */
        else if ( $byLogin->SelectedRowsCount() == 0 )
        {
            /* пришел пользователь не зарег. в системе и с инструкцией на уделние */
            if ($this->abisUser['deleted'] === true) return false;

            if ( $byEmail === false && empty($this->abisUser['email'] ) || $this->abisUser['email'] === null)
            {
                /*генерируем email из АБИС ID*/
                $arUpdateFields['EMAIL'] = preg_replace("/\\\/", "", $this->abisUser["id"]) ."@example.com";
            }
            else if ( $byEmail->SelectedRowsCount() == 0 )
            {
                /*добавляем пользователя в битрикс, авторизация по логину АБИС*/
                $arUpdateFields['LOGIN'] = $this->abisUser['email'];
                $arUpdateFields['EMAIL'] = $this->abisUser['email'];
            }
            else
            {
                /* обновляем пользователя */
                $fetch = $byEmail->Fetch();
                $arUpdateFields["ID"] = $fetch['ID'];

                return array("METHOD" => "userUpdate", "FIELDS" =>  $arUpdateFields, "sendNotice" => true);
            }
        }

        return array("METHOD" => "addNewUser", "FIELDS" => $arUpdateFields, "sendNotice" => false);
    }

    function userUpdate ( $ID, $arFields, $sendNotice = false )
    {
        unset($arFields['ID']);

        $cuEnum = new \CUserFieldEnum();
        $db = $cuEnum->GetList(array(), array("XML_ID" => $this->registerType));

        if (!$arFields['PASSWORD'])
            unset($arFields['PASSWORD']);

        if ($res = $db->Fetch())
            $arFields["UF_REGISTER_TYPE"] = (int)$res["ID"];

        if ( is_array($arFields) && count($arFields) > 2 && !( $uUp = $this->updateAllFields($ID, $arFields) ) )
        {
            Logger::pull($this->CUser->LAST_ERROR);
        }
        else
        { /* Update Successful */
            Logger::pull("User ID#" . $ID . " is successful update.");
            Logger::pull("Fields SERIALIZE: " . serialize($arFields) );
            /* Send notification from user on email */
            if ($sendNotice === true)
            {
                /* Здесь отправляем письмо пользователю с информацией о Использовании для авторизации данных с АБИС */
            }
        }
    }

    function addNewUser ( $arFields, $sendNotice = false )
    {
        $id = null;
        if (!$arFields['UF_MERGED_ID'])
        {
            $arFields['UF_MERGED_ID'] = 0;
        }

        if (!$arFields['PASSWORD'])
        {
            $arFields['PASSWORD'] = randString(10);
            $arFields['CONFIRM_PASSWORD'] = $arFields['PASSWORD'];
        }

        if (!$arFields['LOGIN'] && $arFields['EMAIL'])
        {
            $arFields['LOGIN'] = $arFields['EMAIL'];
        }

        $cuEnum = new \CUserFieldEnum();
        $db = $cuEnum->GetList(array(), array("XML_ID" => $this->registerType));

        if ($res = $db->Fetch())
            $arFields["UF_REGISTER_TYPE"] = (int)$res["ID"];

        if ( is_array($arFields) && count($arFields) > 2 )
        {
            $id = $this->CUser->Add( $arFields );
        }

        if ( !is_null( $id ) )
        {
            Logger::pull( "New user ID#" . $id . " is successful Add." );
            Logger::pull( "Fields SERIALIZE: " . serialize( $arFields ) );
            /* Send notification from user on email */
            if ( $sendNotice === true )
            {
                /* Здесь отправляем письмо пользователю с информацией о Использовании для авторизации данных с АБИС */
            }
        }
        else
        {
           Logger::pull($this->CUser->LAST_ERROR);
        }
    }

    function deleteUser ($ID, $sendNotice = false)
    {
        Logger::pull("Пользователь ID [$ID] удален по запросу из АБИС");
        return \CUser::Delete($ID);
    }

    function updateAllFields($userID, $fieldsData)
    {
        $cuEnum = new \CUserFieldEnum();
        $db = $cuEnum->GetList(array(), array("XML_ID" => $this->registerType));

        if ($res = $db->Fetch())
            $fieldsData["UF_REGISTER_TYPE"] =(int)$res["ID"];

        return $this->CUser->Update($userID, $fieldsData);
    }

    /**
     * Only One user return or false
     *
     * @param $login
     *
     * @return bool|\CDBResult
     */
    function checkByLogin($login)
    {
        return \CUser::GetByLogin($login);
    }


    /**
     * Maybe return one or more users, check!
     *
     * @param $UF_ABIS_ID
     *
     * @return bool|\CDBResult
     */
    function userByAbis($UF_ABIS_ID)
    {
        global $DB;

        return $DB->Query("SELECT VALUE_ID FROM b_uts_user WHERE UF_ABIS_ID = '" . $DB->ForSql($UF_ABIS_ID) ."'");
    }


    /**
     * Only One user return or false
     *
     * @param $EMAIL
     *
     * @return bool|\CDBResult
     */
    function checkByEmail($EMAIL)
    {
        if (empty($EMAIL) || $EMAIL === "" || is_null($EMAIL))
            return false;

        return $this->CUser->GetList(
            ( $by = "ID" ), ( $order = "DESC" ),
            array( "EMAIL" => $EMAIL ),
            array(
                "SELECT"  => array(
                    "UF_ABIS_ID", "UF_LIBRARY", "UF_MERGE", "UF_ISIL", "UF_DATE_OF_INPUT", "UF_DATE_OF_FINISH",
                    "UF_NUM_ECHB"
                ),
                "FIELDS"  => array(
                    "ID", "EXTERNAL_AUTH_ID","PASSWORD", "ACTIVE", "NAME", "LAST_NAME", "SECOND_NAME", "EMAIL",
                    "LOGIN"
                ),
            )
        );
    }

    /**
     * @param array $fields
     */
    public function setFields( $fields )
    {
        $this->fields = $fields;
    }

    /**
     * @param string $registerType
     */
    public function setRegisterType( $registerType )
    {
        $this->registerType = $registerType;
    }
}