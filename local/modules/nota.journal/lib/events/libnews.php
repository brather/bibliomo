<?php

namespace Nota\Journal\Events;

use Nota\Journal\J;
use Bitrix\Main\Type;

class Libnews
{
    function onAdd(&$arFields){
        if($arFields['IBLOCK_ID'] == IBLOCK_ID_LIBRARY_NEWS)
            J::add('libnews', 'add', Array('NEWS_ID' => $arFields['ID']));
    }

    function onEdit(&$arFields){
        if($arFields['IBLOCK_ID'] == IBLOCK_ID_LIBRARY_NEWS)
            J::add('libnews', 'edit', Array('NEWS_ID' => $arFields['ID']));
    }

    function onDelete($arFields){
        if($arFields['IBLOCK_ID'] == IBLOCK_ID_LIBRARY_NEWS)
            J::add('libnews', 'delete', Array('NEWS_ID' => $arFields['ID']));
    }
}