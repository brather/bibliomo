<?php
namespace Nota\Journal;

use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);


class CollectionTable extends Entity\DataManager
{
    public static function getTableName()
    {
        return 'neb_journal.collection';
    }

    public static function getConnectionName()
    {
        return 'journal';
    }

    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
                'title' => Loc::getMessage('COLLECTION_ENTITY_ID_FIELD'),
            ),
            new Entity\EnumField('EVENT', array(
                'values' => array('ADD', 'EDIT', 'DELETE', 'VIEW', 'PERM')
            )),
            'USER' => array(
                'data_type' => 'integer',
                'required' => true,
            ),
            'USER_DATA' => array(
                'data_type' => 'text',
                'required' => true,
            ),
            'TIMESTAMP' => array(
                'data_type' => 'datetime',
            ),
            'COLLECTION_ID' => array(
                'data_type' => 'integer',
                'required' => true,
            ),
        );
    }
}