<?php
namespace Nota\Journal;

use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);


class LibstatTable extends Entity\DataManager
{
    public static function getTableName()
    {
        return 'neb_journal.libstat';
    }

    public static function getConnectionName()
    {
        return 'journal';
    }

    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
            ),
            new Entity\EnumField('EVENT', array(
                'values' => array('VIEW')
            )),
            'USER' => array(
                'data_type' => 'integer',
            ),
            'USER_DATA' => array(
                'data_type' => 'text',
            ),
            'TIMESTAMP' => array(
                'data_type' => 'datetime',
            ),
            'REPORT_ID' => array(
                'data_type' => 'text',
            ),
            'REPORT_PARAMS' => array(
                'data_type' => 'text',
            ),
        );
    }
}