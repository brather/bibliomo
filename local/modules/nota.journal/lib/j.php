<?php

namespace Nota\Journal;

class J
{
    /**
     * @param $entity название сущности для записи. Полное описание доступно в гуглдоке: https://docs.google.com/spreadsheets/d/1jPJk_Vlo7cnDDGhO5YiOuC2h-2TToPJNg36H0FeRPjw/edit#gid=0
     * @param $event тип события для записи: ADD, EDIT, DELETE, VIEW
     * @param $params у каждой сущности свои параметры. Ожидается массив, в котором ключ это название параметра с указанными значениями. Пример: Array('COLLECTION_ID' => 2, 'BOOK_ID' => 12). Посмотреть список параметров можно используя метод getMap. Пример: J::getMap('collection'). Список параметров для сущности collection
     */
    public static function add($entity, $event, $params)
    {
        //сущность ORM битрикс
        $entityClass = 'Nota\\Journal\\' . ucfirst($entity) . 'Table';
        $event = strtoupper($event);

        //параметры пользователя
        /*IP адрес пользователя;
        URL страницы;
        Параметры HTTP-запроса;
        Группа пользователя;
        Роль пользователя;
        Юзерагент*/

        global $USER;
        $userID = $USER->isAuthorized() ? $USER->GetID() : 0;

        if(\CSite::InGroup(self::getGroupsArrayByCode(array(UGROUP_LIB_CODE_ADMIN)))){
            $userRole = 'Администратор библиотеки';
        } elseif(\CSite::InGroup(self::getGroupsArrayByCode(array(UGROUP_LIB_CODE_EDITOR)))){
            $userRole = 'Редактор библиотеки';
        } elseif(\CSite::InGroup(self::getGroupsArrayByCode(array(UGROUP_LIB_CODE_CONTORLLER)))){
            $userRole = 'Контроллер библиотеки';
        } elseif(\CSite::InGroup(self::getGroupsArrayByCode(array(UGROUP_LIB_CODE_ADMIN, UGROUP_LIB_CODE_CONTORLLER, UGROUP_LIB_CODE_EDITOR)))){
            $userRole = 'Администрация';
        } else {
            $userRole = 'Обычный пользователь';
        }

        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $userIP = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $userIP = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $userIP = $_SERVER['REMOTE_ADDR'];
        }

        global $APPLICATION;
        $userArray = Array(
            'IP' => $userIP,
            'URL' => $APPLICATION->GetCurPageParam(),
            'REQUEST' => array_map("htmlspecialchars", $_REQUEST),
            'USER_GROUP' => $USER->GetUserGroupArray(),
            'USER_ROLE' => $userRole,
            'BROWSER' => $_SERVER['HTTP_USER_AGENT'],
        );

        $addArray = Array(
            'EVENT' => $event,
            'USER' => $userID,
            'USER_DATA' => serialize($userArray),
        );

        try{
            $entityClass::Add(array_merge($addArray, $params));
        } catch(\Bitrix\Main\ArgumentException $e) {
            $APPLICATION->ThrowException($e);
        } catch(\Bitrix\Main\DB\SqlQueryException $e){
            $APPLICATION->ThrowException($e);
        }
    }

    public static function getMap($entity){
        $entityClass = 'Nota\Journal\\' . ucfirst($entity) . 'Table';
        return $entityClass::getMap();
    }

    private static function getGroupsArrayByCode($groups){
        $groups = implode("|", $groups);
        $groupsDB = \CGroup::GetList($by = "c_sort", $order = "asc", array("STRING_ID" => $groups));
        if(intval($groupsDB->SelectedRowsCount()) > 0)
        {
            while($g = $groupsDB->Fetch())
            {
                $result[] = $g["ID"];
            }
        }
        return $result;//IBLOCK_CODE_LIBRARY
    }
}