-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 03, 2014 at 04:43 PM
-- Server version: 5.5.35
-- PHP Version: 5.4.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `neb`
--
CREATE DATABASE IF NOT EXISTS `neb_journal` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `neb_journal`;

-- --------------------------------------------------------

--
-- Table structure for table `book`
--

CREATE TABLE IF NOT EXISTS `book` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `EVENT` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `USER` int(11) NOT NULL,
  `USER_DATA` text COLLATE utf8_unicode_ci NOT NULL,
  `TIMESTAMP` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `BOOK_ID` text COLLATE utf8_unicode_ci NOT NULL,
  `AUTHOR` text COLLATE utf8_unicode_ci NOT NULL,
  `NAME` text COLLATE utf8_unicode_ci NOT NULL,
  `YEAR` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `LIB` text COLLATE utf8_unicode_ci NOT NULL,
  `PLACE` text COLLATE utf8_unicode_ci NOT NULL,
  `PUBLISHER` text COLLATE utf8_unicode_ci NOT NULL,
  `LANG` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `BBK` text COLLATE utf8_unicode_ci NOT NULL,
  `COLLECTION` text COLLATE utf8_unicode_ci NOT NULL,
  `FUND` text COLLATE utf8_unicode_ci NOT NULL,
  `METHOD` text COLLATE utf8_unicode_ci NOT NULL,
  `VOLUME` text COLLATE utf8_unicode_ci NOT NULL,
  `SYSTEM` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `bookmark`
--

CREATE TABLE IF NOT EXISTS `bookmark` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `EVENT` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `USER` int(11) NOT NULL,
  `USER_DATA` text COLLATE utf8_unicode_ci NOT NULL,
  `TIMESTAMP` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `BOOK_ID` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `BOOK_LINK` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `DATE` datetime NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `collection`
--

CREATE TABLE IF NOT EXISTS `collection` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `EVENT` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `USER` int(11) NOT NULL,
  `USER_DATA` text COLLATE utf8_unicode_ci NOT NULL,
  `TIMESTAMP` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `COLLECTION_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `consultant`
--

CREATE TABLE IF NOT EXISTS `consultant` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `EVENT` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `USER` int(11) NOT NULL,
  `USER_DATA` text COLLATE utf8_unicode_ci NOT NULL,
  `TIMESTAMP` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ANSWER_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `eechb`
--

CREATE TABLE IF NOT EXISTS `eechb` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `EVENT` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `USER` int(11) NOT NULL,
  `USER_DATA` text COLLATE utf8_unicode_ci NOT NULL,
  `TIMESTAMP` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `FIO` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `NUMBER` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fav`
--

CREATE TABLE IF NOT EXISTS `fav` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `EVENT` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `USER` int(11) NOT NULL,
  `USER_DATA` text COLLATE utf8_unicode_ci NOT NULL,
  `TIMESTAMP` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `FAV_ID` int(11) NOT NULL,
  `NAME` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `DATE` datetime NOT NULL,
  `DELETED` int(11) NOT NULL,
  `DELETED_DATE` datetime NOT NULL,
  `BOOK_ID` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `forum`
--

CREATE TABLE IF NOT EXISTS `forum` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `EVENT` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `USER` int(11) NOT NULL,
  `USER_DATA` text COLLATE utf8_unicode_ci NOT NULL,
  `TIMESTAMP` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `TEXT` text COLLATE utf8_unicode_ci NOT NULL,
  `DATE` datetime NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fundstat`
--

CREATE TABLE IF NOT EXISTS `fundstat` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `EVENT` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `USER` int(11) NOT NULL,
  `USER_DATA` text COLLATE utf8_unicode_ci NOT NULL,
  `TIMESTAMP` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `REPORT_ID` text COLLATE utf8_unicode_ci NOT NULL,
  `REPORT_PARAMS` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `lib`
--

CREATE TABLE IF NOT EXISTS `lib` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `EVENT` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `USER` int(11) NOT NULL,
  `USER_DATA` text COLLATE utf8_unicode_ci NOT NULL,
  `TIMESTAMP` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `BOOK_ID` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `libcontent`
--

CREATE TABLE IF NOT EXISTS `libcontent` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `EVENT` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `USER` int(11) NOT NULL,
  `USER_DATA` text COLLATE utf8_unicode_ci NOT NULL,
  `TIMESTAMP` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `IBLOCK_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `libnews`
--

CREATE TABLE IF NOT EXISTS `libnews` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `EVENT` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `USER` int(11) NOT NULL,
  `USER_DATA` text COLLATE utf8_unicode_ci NOT NULL,
  `TIMESTAMP` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `NEWS_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `libperm`
--

CREATE TABLE IF NOT EXISTS `libperm` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `EVENT` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `USER` int(11) NOT NULL,
  `USER_DATA` text COLLATE utf8_unicode_ci NOT NULL,
  `TIMESTAMP` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `USER_ID` int(11) NOT NULL,
  `PERM` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `libstat`
--

CREATE TABLE IF NOT EXISTS `libstat` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `EVENT` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `USER` int(11) NOT NULL,
  `USER_DATA` text COLLATE utf8_unicode_ci NOT NULL,
  `TIMESTAMP` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `REPORT_ID` text COLLATE utf8_unicode_ci NOT NULL,
  `REPORT_PARAMS` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `libvote`
--

CREATE TABLE IF NOT EXISTS `libvote` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `EVENT` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `USER` int(11) NOT NULL,
  `USER_DATA` text COLLATE utf8_unicode_ci NOT NULL,
  `TIMESTAMP` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `VOTE_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `note`
--

CREATE TABLE IF NOT EXISTS `note` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `EVENT` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `USER` int(11) NOT NULL,
  `USER_DATA` text COLLATE utf8_unicode_ci NOT NULL,
  `TIMESTAMP` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `BOOK_ID` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `DATE` datetime NOT NULL,
  `TEXT` text COLLATE utf8_unicode_ci NOT NULL,
  `USER_COLLECTION` text COLLATE utf8_unicode_ci NOT NULL,
  `INCLUDED` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `pdf`
--

CREATE TABLE IF NOT EXISTS `pdf` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `EVENT` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `USER` int(11) NOT NULL,
  `USER_DATA` text COLLATE utf8_unicode_ci NOT NULL,
  `TIMESTAMP` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `BOOK_ID` text COLLATE utf8_unicode_ci NOT NULL,
  `COPY_ID` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `plandigital`
--

CREATE TABLE IF NOT EXISTS `plandigital` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `EVENT` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `USER` int(11) NOT NULL,
  `USER_DATA` text COLLATE utf8_unicode_ci NOT NULL,
  `TIMESTAMP` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `BOOK_ID` text COLLATE utf8_unicode_ci NOT NULL,
  `DATE` date NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `quote`
--

CREATE TABLE IF NOT EXISTS `quote` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `EVENT` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `USER` int(11) NOT NULL,
  `USER_DATA` text COLLATE utf8_unicode_ci NOT NULL,
  `TIMESTAMP` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `BOOK_ID` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `BOOK_LINK` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `TEXT` text COLLATE utf8_unicode_ci NOT NULL,
  `DATE` datetime NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `rightholder`
--

CREATE TABLE IF NOT EXISTS `rightholder` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `EVENT` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `USER` int(11) NOT NULL,
  `USER_DATA` text COLLATE utf8_unicode_ci NOT NULL,
  `TIMESTAMP` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `BOOK_ID` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
