<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 10.12.2016
 * Time: 16:35
 */
namespace Elr\Monitoring;

use \Bitrix\Main\Entity,
    \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

/**
 * Class MonitoringTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> TASK_ID string(50) mandatory
 * <li> LIBRARY string(25) optional
 * <li> DATE_CREATE datetime mandatory default 'CURRENT_TIMESTAMP'
 * <li> NUM_VISITS int optional
 * <li> NUM_ACTIVE_READERS int optional
 * <li> NUM_REGISTERED_READERS int optional
 * <li> PERIOD_START datetime mandatory default '0000-00-00 00:00:00'
 * <li> PERIOD_END datetime mandatory default '0000-00-00 00:00:00'
 * </ul>
 *
 * @package Bitrix\Monitoring
 **/

class MonitoringTable extends Entity\DataManager
{
    const IBLOCK_ID_LIB = 4;

    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'elr_monitoring';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
                'title' => Loc::getMessage('MONITORING_ENTITY_ID_FIELD'),
            ),
            'TASK_ID' => array(
                'data_type' => 'string',
                'required' => true,
                'validation' => array(__CLASS__, 'validateTaskId'),
                'title' => Loc::getMessage('MONITORING_ENTITY_TASK_ID_FIELD'),
            ),
            'LIBRARY' => array(
                'data_type' => 'string',
                'validation' => array(__CLASS__, 'validateLibrary'),
                'title' => Loc::getMessage('MONITORING_ENTITY_LIBRARY_FIELD'),
            ),
            'DATE_CREATE' => array(
                'data_type' => 'datetime',
                'required' => true,
                'title' => Loc::getMessage('MONITORING_ENTITY_DATE_CREATE_FIELD'),
            ),
            'NUM_VISITS' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('MONITORING_ENTITY_NUM_VISITS_FIELD'),
            ),
            'NUM_ACTIVE_READERS' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('MONITORING_ENTITY_NUM_ACTIVE_READERS_FIELD'),
            ),
            'NUM_REGISTERED_READERS' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('MONITORING_ENTITY_NUM_REGISTERED_READERS_FIELD'),
            ),
            'PERIOD_START' => array(
                'data_type' => 'datetime',
                'required' => true,
                'title' => Loc::getMessage('MONITORING_ENTITY_PERIOD_START_FIELD'),
            ),
            'PERIOD_END' => array(
                'data_type' => 'datetime',
                'required' => true,
                'title' => Loc::getMessage('MONITORING_ENTITY_PERIOD_END_FIELD'),
            ),
            new \Bitrix\Main\Entity\ReferenceField(
                'LIBRARY_NAME',
                '\Bitrix\Iblock\Element',
                array(
                    '=this.LIBRARY' => 'ref.XML_ID',
                    '=ref.IBLOCK_ID' => new \Bitrix\Main\DB\SqlExpression('?i', self::IBLOCK_ID_LIB),
                )
            ),
            new Entity\IntegerField(
                "NUM_CHECKOUTS",
                array(
                    'title' => Loc::getMessage( "ELR_MONITORING_NUM_CHECKOUTS" )
                )
            ),
            new Entity\IntegerField(
                "NUM_CHECKINS",
                array(
                    'title' => Loc::getMessage( "ELR_MONITORING_NUM_CHECKINS" )
                )
            )
        );


    }
    /**
     * Returns validators for TASK_ID field.
     *
     * @return array
     */
    public static function validateTaskId()
    {
        return array(
            new Entity\Validator\Length(null, 50),
        );
    }
    /**
     * Returns validators for LIBRARY field.
     *
     * @return array
     */
    public static function validateLibrary()
    {
        return array(
            new Entity\Validator\Length(null, 25),
        );
    }
}