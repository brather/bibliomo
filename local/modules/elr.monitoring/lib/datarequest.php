<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 12.12.2016
 * Time: 9:12
 */

namespace Elr\Monitoring;

\Bitrix\Main\Loader::includeModule("elr.useraccess");

use Bitrix\Main\Loader;
use Elr\Monitoring\MonitoringTable,
    Elr\Install\IABIS,
    Elr\Logger;

class DataRequest extends IABIS
{
    private $report;
    private $time1;
    private $time2;
    private $taskId;

    function __construct($report, $time1, $time2)
    {
        $this->report   = $report;
        $this->time1    = $time1;
        $this->time2    = $time2;

        return $this;
    }

    public function reportLaunch()
    {
        if ($this->_checkPreviousPeriod() === true)
            return false;

        $postData = array(
            "report"    => $this->report,
            "time2"     => $this->time2
        );

        if ($this->time1)
            $postData["time1"] = $this->time1;

        $rq = $this->curlRequest(
                "reports/plugins/portal/tasks",
                "",
                "POST",
                $postData
            );

        $data = json_decode($rq, true);
        if ($data['status'] == "ok")
            return $data['taskId'];
        else
            return array('error' => $data['error']);
    }

    public function obtainingStatusReport( $reportId )
    {
        $this->taskId = $reportId;

        if ($this->wasGeneratedReport())
        {
            $rq = $this->curlRequest(
                "reports/plugins/portal/tasks/" . $reportId ."/index.json",
                ""
            );

            $data = json_decode($rq, true);

            if ($data['status'] == "ok")
                return $data['report'];
            else
                return array('error' => $data['error']);
        }
        else
        {
            return array('error' => "Error: to detail view log file.");
        }
    }

    public function fetchReports( $reports )
    {
        global $APPLICATION;
        foreach ($reports as $report)
        {
            if ( $ID = $this->_recordReportData( $report ))
            {
                if (is_array($ID))
                    $APPLICATION->ThrowException(implode("<br>", $ID));

                Logger::pull("Add report ID " . implode("\n", $ID) . "\n" . serialize($report));
            }
            else
            {
                Logger::pull("Something wrong. Serialize data: " . serialize($report));
            }
        }
    }

    private function _recordReportData( $report )
    {
        $DATE_CREATE =  \Bitrix\Main\Type\DateTime::createFromTimestamp(time());;
        $PERIOD_START = \Bitrix\Main\Type\DateTime::createFromTimestamp(strtotime($this->time1));
        $PERIOD_END = \Bitrix\Main\Type\DateTime::createFromTimestamp(strtotime($this->time2));

        $addResult = MonitoringTable::add(
            array(
                'TASK_ID' => $this->taskId,
                'LIBRARY' => $report['library'],
                'DATE_CREATE'=> $DATE_CREATE,
                'NUM_VISITS' => (int)$report['numVisits'],
                'NUM_ACTIVE_READERS' => (int)$report['numActiveReaders'],
                'NUM_REGISTERED_READERS' => (int)$report['numRegisteredReaders'],
                'NUM_CHECKOUTS' => (int)$report['numCheckouts'],
                'NUM_CHECKINS' => (int)$report['numCheckins'],
                'PERIOD_START' => $PERIOD_START,
                'PERIOD_END' => $PERIOD_END
            )
        );

        if ($addResult->isSuccess())
            return $addResult->getId();
        else
            return $addResult->getErrorMessages();
    }

    /* Return true if Period is already record */
    private function _checkPreviousPeriod()
    {
        /* Время пердыдущих суток */
        $previous = \Bitrix\Main\Type\DateTime::createFromTimestamp(time() - 86400);

        $db = MonitoringTable::getList(array(
            "filter" => array(
                "<=PERIOD_START" => $previous,
                ">=PERIOD_END" =>$previous
            )
        ));

        if ($db->fetch())
            return true;
        else
            return false;
    }

    function wasGeneratedReport()
    {
        $more = true;
        $completed = false;

        while ($more)
        {
            $data = $this->_reportResult();

            switch ($data['task']['status'])
            {
                case "running":
                    Logger::pull("Info: Report generated is  is running.\n" . serialize($data));
                    break;
                case "stopping":
                    Logger::pull("Info: Report generated is stopping.\n" . serialize($data));
                    $more = false;
                    break;
                case "failed":
                    Logger::pull("FILED: Report generated is filed.\n" . serialize($data));
                    $more = false;
                    break;
                case "canceled":
                    Logger::pull("INFO: Report generated is canceled.\n" . serialize($data));
                    $more = false;
                    break;
                case "completed":
                    Logger::pull("SUCCESS: Report generated is completed.\n" . serialize($data));
                    $more = false;
                    $completed = true;
                    break;
            }
        }

        if ($completed === true)
            return $completed;

        if ($more === false)
            return $more;
    }

    private function _reportResult ()
    {
        sleep(5);
        $rq = $this->curlRequest(
            "reports/plugins/portal/tasks/" . $this->taskId,
            ""
        );
        $data = json_decode($rq, true);

        return $data;
    }
}