<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 10.12.2016
 * Time: 17:38
 */
namespace Elr\Monitoring;

use Bitrix\Main\Application,
    Elr\Monitoring\DataRequest,
    Elr\Logger,
    Bitrix\Main\Loader;

class Agent
{
    private function __construct(){}

    /* Запустим агента и вернем его же но уже с временем нынешнего запуска */
    static public function getOrder( )
    {
        $prevDay = date("Ymd", (time() - 86400));

        /* @var \Elr\Monitoring\DataRequest $dr*/
        $dr = new DataRequest(
                "visits", $prevDay."T000000", $prevDay."T235959"
            );
        $taskId = $dr->reportLaunch();
        if (isset($taskId['error']))
        {
            Logger::pull("ERROR: " . $taskId['error']);
        }
        elseif ($taskId === false)
        {
            Logger::pull("Info: A data up to update!");
        }
        else
        {
            $reports = $dr->obtainingStatusReport( $taskId );

            if (isset($reports['error']))
            {
                Logger::pull("ERROR: " . $reports['error']);
            }
            else
            {
                $dr->fetchReports($reports);
            }
        }

        return 'Elr\Monitoring\Agent::getOrder();';
    }

    static public function lastPeriodPerDay($lastDays, $step)
    {
        Application::getInstance()->getCache()->abortDataCache();
        Loader::includeModule("elr.useraccess");
        Loader::includeModule("elr.monitoring");
        $lastDays = $lastDays - $step;

        /* Вернем окончание цикла */
        if ($step !== 0 && $lastDays == 0)
            die (json_encode(array("status"=>"done")));

        $minDate  = date("Ymd", strtotime(date("Y-m-d")."- ".$lastDays." DAY"));
        $maxDate  = $minDate;
        $maxDate .= "T235959";
        $minDate .= "T000000";

        /* @var \Elr\Monitoring\DataRequest $dr*/
        $dr = new DataRequest(
            "visits", $minDate, $maxDate
        );
        $taskId = $dr->reportLaunch();
        if (isset($taskId['error']))
        {
            Logger::pull("ERROR: " . $taskId['error']);
        }
        elseif ($taskId === false)
        {
            Logger::pull("Info: A data up to update!");
        }
        else
        {
            $reports = $dr->obtainingStatusReport( $taskId );

            if (isset($reports['error']))
            {
                Logger::pull("ERROR: " . $reports['error']);
            }
            else
            {
                $dr->fetchReports($reports);
            }
        }

        /* Return result */
        die(json_encode(array("status"=>"ok", "step" => $step + 1, "date" => $minDate)));
    }
}