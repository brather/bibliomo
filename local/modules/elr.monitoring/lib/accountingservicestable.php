<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 16.12.2016
 * Time: 16:33
 */

namespace Elr;

use \Bitrix\Main\Entity\DataManager,
    \Bitrix\Main\Entity,
    \Bitrix\Highloadblock as HL;

class AccountingServicesTable extends DataManager
{
    const IBLOCK_ID_LIB = 4;

    public static function getTableName()
    {
        return 'elr_accounting_services';
    }

    public static function getMap()
    {
        \Bitrix\Main\Loader::includeModule('highloadblock');
        /* HardCode 17 is ID HighLoadBlock LibraryServices */
        $hl = HL\HighloadBlockTable::getById(17)->fetch();
        $entity = HL\HighloadBlockTable::compileEntity($hl);
        $libraryServicesClass = $entity->getDataClass();

        return array(
            new Entity\IntegerField("ID",
                array(
                    'primary' => true,
                    'autocomplete' => true,
                    'title' => "Уникальный идентификатор"
                )
            ),
            new Entity\DatetimeField("DATE_CREATE",
                array(
                    'require' => true,
                    'title' => "Дата создания"
                )
            ),
            new Entity\DatetimeField("DATE_PROVISION",
                array(
                    'require' => true,
                    'title' => "Дата оказания услуги"
                )
            ),
            new Entity\IntegerField("CODE_ID",
                array(
                    'require' => true,
                    'title' => "Идентификатор кода услуги"
                )
            ),
            new Entity\ReferenceField("SERVICE",
                $libraryServicesClass,
                array(
                    '=this.CODE_ID' => 'ref.ID'
                )
            ),
            new Entity\ExpressionField("SERVICE_CODE", "%s", "SERVICE.UF_SERVICE_CODE"),
            new Entity\ExpressionField("SERVICE_NAME", "%s", "SERVICE.UF_NAME"),
            new Entity\ExpressionField("SERVICE_TYPE", "%s", "SERVICE.UF_SERVICE_TYPE"),
            new Entity\IntegerField("ECHB_NUM",
                array(
                    'title' => "Идентифигатор читательского билета"
                )
            ),
            new Entity\IntegerField("LIBRARY_ID",
                array(
                    'require' => true,
                    'title' => "Идентификатор библиотеки"
                )
            ),
            new Entity\ReferenceField("LIBRARY",
                '\Bitrix\Iblock\Element',
                array(
                    '=this.LIBRARY_ID' => 'ref.ID',
                    '=ref.IBLOCK_ID' => new \Bitrix\Main\DB\SqlExpression('?i', self::IBLOCK_ID_LIB)
                )
            ),
            new Entity\ExpressionField("LIBRARY_NAME", "%s", "LIBRARY.NAME"),
            new Entity\FloatField("PRICE",
                array(
                    'title' => "Стоимость услуги"
                )
            ),
            new Entity\TextField("COMMENT",
                array(
                    'title' => "Произвольный комментарий"
                )
            ),
            new Entity\TextField("FULL_NAME",
                array(
                    'title' => "ФИО пользователя"
                )
            ),
            new Entity\IntegerField("USER_TOUCH_ID",
                array(
                    'require' => true,
                    'title' => "Пользователь внёсший изменения"
                )
            )
        );
    }
}