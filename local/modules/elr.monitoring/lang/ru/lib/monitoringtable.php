<?
$MESS["MONITORING_ENTITY_ID_FIELD"] = "Идентификатор";
$MESS["MONITORING_ENTITY_TASK_ID_FIELD"] = "Отчет";
$MESS["MONITORING_ENTITY_LIBRARY_FIELD"] = "ISIL уникальный номер";
$MESS["MONITORING_ENTITY_DATE_CREATE_FIELD"] = "Дата создания записи";
$MESS["MONITORING_ENTITY_NUM_VISITS_FIELD"] = "Число посиетителей";
$MESS["MONITORING_ENTITY_NUM_ACTIVE_READERS_FIELD"] = "Число активных посетителей";
$MESS["MONITORING_ENTITY_NUM_REGISTERED_READERS_FIELD"] = "Число зарегистрированных послетителей";
$MESS["MONITORING_ENTITY_PERIOD_START_FIELD"] = "Начало периода данных";
$MESS["MONITORING_ENTITY_PERIOD_END_FIELD"] = "Конец периода даных";
$MESS["ELR_MONITORING_NUM_CHECKOUTS"] = "Число выданых книг";
$MESS["ELR_MONITORING_NUM_CHECKINS"] = "Число возвратов книг";
?>