<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 10.12.2016
 * Time: 11:23
 */

use Bitrix\Main\Localization\Loc;

global $APPLICATION;

if ($ex = $APPLICATION->GetException()) { ?>
	<form action="<?=$APPLICATION->GetCurPage(); ?>">
		<? $message = new CAdminMessage(Loc::getMessage('MOD_UNINST_IMPOSSIBLE'), $ex); ?>
		<?=$message->show(); ?>
		<input type="hidden" name="lang" value="<?=LANG; ?>">
		<input type="submit" name="inst" value="Вернуться">
	<form>
<? } else { ?>
	<form action="<?=$APPLICATION->GetCurPage(); ?>">
		<?=bitrix_sessid_post(); ?>

		<input type="hidden" name="lang" value="<?=LANG; ?>">
		<input type="hidden" name="id" value="elr.monitoring">
		<input type="hidden" name="uninstall" value="Y">
		<input type="hidden" name="step" value="2">
		<?=CAdminMessage::ShowMessage(Loc::getMessage('MOD_UNINST_WARN')); ?>
		<p>
            Очистить и удалить данные из базы
            <input type="checkbox" name="flush_data" />
        </p>
		<input type="submit" name="inst" value="Удалить модуль">
	</form>
<? } ?>