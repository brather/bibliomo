<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 10.12.2016
 * Time: 11:23
 */
use \Bitrix\Main\Localization\Loc;

IncludeModuleLangFile(__FILE__);

global $APPLICATION;
?>
<form action="<?= $APPLICATION->GetCurPage()?>" name="elr_monitoring_install">
    <?= bitrix_sessid_post()?>
    <input type="hidden" name="lang" value="<?= LANG ?>">
    <input type="hidden" name="id" value="elr.monitoring" >
    <input type="hidden" name="install" value="Y" >
    <input type="hidden" name="step" value="2" >
    <p>
        <input type="submit" name="inst" value="<?= Loc::getMessage("MOD_INSTALL")?>">
    </p>
</form>
