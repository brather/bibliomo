<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 10.12.2016
 * Time: 11:23
 */
use Bitrix\Main\Localization\Loc;

IncludeModuleLangFile(__FILE__);

if(!check_bitrix_sessid()) die();

global $errors, $APPLICATION;

if(strlen($errors)<=0):
    echo CAdminMessage::ShowNote( Loc::getMessage("MOD_INST_OK") );
else:
    for($i=0; $i<count($errors); $i++)
        $alErrors .= $errors[$i]."<br>";
    echo CAdminMessage::ShowMessage(array(
            "TYPE"=>"ERROR",
            "MESSAGE" => Loc::getMessage("MOD_INST_ERR"),
            "DETAILS"=>$alErrors,
            "HTML"=>true
        )
    );
endif;
if ($ex = $APPLICATION->GetException())
{
    echo CAdminMessage::ShowMessage(array(
            "TYPE" => "ERROR",
            "MESSAGE" => Loc::getMessage("MOD_INST_ERR"),
            "HTML" => true,
            "DETAILS" => $ex->GetString()
        )
    );
}
?>

<form action="<? echo $APPLICATION->GetCurPage() ?>">
    <input type="hidden" name="lang" value="<? echo LANG ?>">
    <input type="submit" name="" value="<? echo GetMessage( "MOD_BACK" ) ?>">
</form>
