<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 10.12.2016
 * Time: 11:22
 */
IncludeModuleLangFile(__FILE__);

use \Bitrix\Main\Localization\Loc;

class elr_monitoring extends CModule
{
    public $MODULE_ID = 'elr.monitoring';
    public $MODULE_VERSION = null;
    public $MODULE_VERSION_DATE = null;
    public $MODULE_NAME = null;
    public $MODULE_DESCRIPTION = null;
    public $MODULE_GROUP_RIGHTS = "N";
    public $PARTNER_NAME = null;
    public $PARTNER_URI = null;

    function __construct()
    {
        $this->MODULE_NAME = "Мониторинг АБИС";
        $this->PARTNER_NAME = "Элар";
        $this->PARTNER_URI = "http://elar.ru";
        $this->MODULE_DESCRIPTION = "";

        $arModuleVersion = array();

        $path = str_replace( "\\", "/", __FILE__ );
        $path = substr( $path, 0, strlen( $path ) - strlen( "/index.php" ) );
        include( $path . "/version.php" );

        if ( is_array( $arModuleVersion )
            && array_key_exists(
                "VERSION", $arModuleVersion
            )
        )
        {
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        }
    }

    public function DoInstall()
    {
        global $APPLICATION, $step, $DB;

        if ((int)$step < 2)
        {
            if (!\Bitrix\Main\Loader::includeModule("elr.useraccess"))
            {
                $this->errors = array("Для работы модуля требуется установить модуль \"Пользовательский доступ\" (elr.useraccess)");
            }

            $GLOBALS['errors'] = $this->errors;

            $APPLICATION->IncludeAdminFile(
                Loc::getMessage( "ELR_UA_INSTALL_MODULE" ),
                __DIR__ . '/step1.php'
            );
        }

        if ((int)$step === 2)
        {
            if ($this->InstallDB())
                $APPLICATION->ThrowException($DB->GetErrorMessage());

            $GLOBALS['errors'] = $this->errors;

            RegisterModule( $this->MODULE_ID );

            /*CAgent::AddAgent(
                'Elr\Agent\Users::sync();',
                'elr.useraccess',
                'N',
                86400,
                "",
                "Y",
                ConvertTimeStamp(time()+CTimeZone::GetOffset()+30, "FULL")
            );*/
        }
    }

    public function DoUninstall()
    {
        global $APPLICATION, $step, $flush_data, $DB;

        if ( (int)$step < 2 )
        {
            $APPLICATION->IncludeAdminFile(
                Loc::getMessage( "ELR_UA_UNINSTALL_MODULE" ) .
                $this->MODULE_NAME, __DIR__ . '/unstep1.php'
            );
        }
        else if ( (int)$step === 2 )
        {
            if ($flush_data)
            {
                $this->UnInstallDB();
            }
            UnRegisterModule( $this->MODULE_ID );

            $APPLICATION->IncludeAdminFile(
                Loc::getMessage( "ELR_UA_UNINSTALL_MODULE" )
                . $this->MODULE_NAME, __DIR__ . '/unstep2.php'
            );
        }
    }

    function InstallDB()
    {
        global $DB, $APPLICATION;

        $errors = $DB->RunSQLBatch($_SERVER["DOCUMENT_ROOT"]."/local/modules/elr.monitoring/install/db/install.sql");
        if ($errors !== false)
        {
            $APPLICATION->ThrowException(implode("", $errors));
            return false;
        }

    }

    function UnInstallDB()
    {
        global $DB, $APPLICATION;

        $errors = $DB->RunSQLBatch($_SERVER["DOCUMENT_ROOT"]."/local/modules/elr.monitoring/install/db/uninstall.sql");

        if ($errors !== false)
        {
            $APPLICATION->ThrowException(implode("", $errors));
            return false;
        }
    }

}