<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 10.12.2016
 * Time: 11:22
 */
CModule::AddAutoloadClasses(
    "elr.monitoring",
    array(
        'Elr\Install\IABIS' => $_SERVER
['DOCUMENT_ROOT'] . '/local/modules/elr.useraccess/lib/iabis.php',
        'Elr\Monitoring\MonitoringTable' => 'lib/monitoringtable.php',
        'Elr\Monitoring\Agent' => 'lib/agent.php',
        'Elr\Monitoring\DataRequest' => 'lib/datarequest.php',
        'PHPExcel' => 'lib/excel/PHPExcel.php',
        'Elr\AccountingServicesTable' => 'lib/accountingservicestable.php'
    )
);