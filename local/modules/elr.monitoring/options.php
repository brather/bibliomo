<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 10.12.2016
 * Time: 11:22
 */
use Bitrix\Main\Localization\Loc;

IncludeModuleLangFile(__FILE__);

global $USER, $APPLICATION;

CJSCore::RegisterExt("bootstrap_ext", Array(
    "js"    =>  "/bitrix/js/elr.useraccess/bootstrap.min.js",
    "css"   =>  array("/bitrix/css/elr.useraccess/bootstrap.min.css", "/bitrix/css/elr.useraccess/bootstrap-theme.min.css"),
    "rel"   =>  array('jquery2'),
    "skip_core" => "Y"
));

CJSCore::Init(array("bootstrap_ext"));

if(!$USER->IsAdmin()) return false;

$arAbisOption = array(
    array(
        "elr_monitoring_days",
        Loc::getMessage( "MONITORING_LAST_DAYS_REPORT" ),
        false,
        array(
            "text",
            10
        )
    ),
);

$aTabs = array(
    array(
        "DIV" => "monitoring",
        "TAB" => Loc::getMessage( "MONITORING_GETTER_REPORTS" ),
        "TITLE" => Loc::getMessage( "MONITORING_REPORTS_FUNDS" ),
        "OPTIONS" => array()
    ),
);

$tabControl = new CAdminTabControl("tabControl", $aTabs);?>

<?$tabControl->Begin();
$tabControl->BeginNextTab();?>
        <div class="row">
            <div class="col-lg-6">
                <div style="margin-bottom:20px;padding: 15px;background: #fff; border: 1px solid #D9D9D9;">
                    <div class="progress progress-striped active" style="text-align: center; color: #fff;">
                        <div class="progress-bar"  role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0">
                            0%
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <div class="clearfix"></div>
    <form id="getLastReport" method="post" action="" onsubmit="return false;">
        <?
        __AdmSettingsDrawList($mid, $arAbisOption);

        $tabControl->Buttons();
        ?>
        <input type="submit" name="sync" value="Синхронизровать" title="Синхронизровать">
        <?=bitrix_sessid_post();?>
    </form>
<?$tabControl->End();?>
<script type="application/javascript">
    $(document).ready(function () {
        $(document).on("submit", "form#getLastReport", function () {
            var days = document.getElementById('getLastReport').elr_monitoring_days.value;
            var replayAjax = function (lastDay, s) {
                var step = (s === undefined) ? 0 : s,
                    progress = 0,
                    onePercent = (100 / lastDay) * (step + 1);

                $.ajax({
                    url: '/local/components/elr/info.monitoring/templates/.default/get_last_report.php',
                    method: 'POST',
                    dataType: 'json',
                    timeout: 30000,
                    data: {lastDay: lastDay, step: step},
                    success: function (res) {
                        if (res.status == "ok") {
                            replayAjax(lastDay, step + 1);
                            $('.progress .progress-bar').css({width: onePercent + '%'}).text(Math.ceil(onePercent) + '%');
                        }

                        if (res.status == "done") {
                            console.log("Finish");
                        }
                    },
                    error: function (st, msg) {
                        console.error("Status:", st, "Message:", msg);
                    }
                });
            };
            replayAjax(days);

            return false;
        });
    });
</script>
