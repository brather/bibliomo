<?php
namespace Evk\Books;
use \Bitrix\Main\Loader;
Loader::includeModule("iblock");
Loader::includeModule("search");
class UpdateSearch
{
	public static $filename = '/local/tools/cron/.update_search';
	public static $reIndexFilename = '/local/tools/cron/.update_search_reindex';

	public static function GetAbsoluteFilename()
	{
		return rtrim($_SERVER["DOCUMENT_ROOT"], "/") . self::$filename;
	}
	public static function GetAbsoluteReIndexFilename()
	{
		return rtrim($_SERVER["DOCUMENT_ROOT"], "/") . self::$filename;
	}
	public static function SetReIndexing($setup=true)
	{
		$filenameReIndex = self::GetAbsoluteReIndexFilename();
		if($setup)
			touch($filenameReIndex);
		elseif(file_exists($filenameReIndex))
			unlink($filenameReIndex);
	}
	public static function IsReIndexing()
	{
		$filenameReIndex = self::GetAbsoluteReIndexFilename();
		if(file_exists($filenameReIndex) && time()-filemtime($filenameReIndex) < 1200)
		{
			return true;
		}
		else
		{
			self::SetReIndexing(0);
			return false;
		}
	}
	public static function AddToFile($string)
	{
		$filename = self::GetAbsoluteFilename();
		if(is_array($string))
			$string = join("\n", $string);
		$f = fopen($filename, 'a+');
		fwrite($f, $string);
		fclose($f);
	}
	public static function WriteToFile($string="")
	{
		$filename = self::GetAbsoluteFilename();
		if(is_array($string))
		{
			if(count($string))
			{
				$newString = array();
				foreach($string as $str)
				{
					$str = trim($str);
					if(strlen($str))
						$newString[] = $str;
				}
				$string = join("\n", $newString);
			}
			else
			{
				$string = "";
			}
		}
		$f = fopen($filename, 'w');
		fwrite($f, $string);
		fclose($f);
	}
	public static function ReadFromFile($splitToRows=0)
	{
		$filename = self::GetAbsoluteFilename();
		$string = array();
		if(file_exists($filename))
		{
			$f = fopen($filename, 'r');
			while(($str=fgets($f))!==false)
			{
				$str = trim($str);
				if(strlen($str))
					$string[] = $str;
			}
			fclose($f);
			if(!$splitToRows)
				$string = join("\n", $string);
			return $string;
		}
		return null;
	}
	public static function AddIdInFile($id)
	{
		self::AddToFile($id."\n");
	}
	public static function GetIdsFromFile()
	{
		return self::ReadFromFile(1);
	}
	public static function SaveIdsToFile($ids)
	{
		if(count($ids))
			self::WriteToFile($ids);
		else
			self::WriteToFile();
		Books::p($ids);
	}
	public static function GetBookAndReIndex()
	{
		if(!self::IsReIndexing())
		{
			self::SetReIndexing();
			$id = self::GetId();
			self::ReIndexBook($id);
		}
	}
	public static function ReIndexBook($id)
	{
		$arBook = self::GetBookFieldsById($id);
		if(!empty($arBook))
		{
			$arFields = array(
				"DATE_CHANGE"=>$arBook["TIMESTAMP_X"],
				"TITLE"=>$arBook["NAME"],
				"SITE_ID"=>$arBook["LID"],
				//"PERMISSIONS"=>$arGroups,
				"PARAM1"=>$arBook["IBLOCK_TYPE_ID"],
				"PARAM2"=>$arBook["IBLOCK_ID"],
				"URL"=>$arBook["DETAIL_PAGE_URL"],
				"BODY"=>$arBook["NAME"],
				"TAGS"=>$arBook["TAGS"]
			);
			$obSearch = new \CSearch();
			$obSearch->Index('iblock', $arBook["ID"], $arFields, true);
			Cache::ClearCache();
		}
		self::SetReIndexing(0);
	}
	public static function GetBookFieldsById($id)
	{
		$id = intval($id);
		if($id > 0)
		{
			$arFilter = array(
				"IBLOCK_TYPE" => "library",
				"IBLOCK_ID" => IBLOCK_ID_BOOKS,
			);
			if($rsBook = \CIBlockElement::GetList(array(),$arFilter))
			{
				if($obBook = $rsBook->GetNextElement())
				{
					$arBook = $obBook->GetFields();
					$arBook["PROPS"] = $obBook->GetProperties();
					return $arBook;
				}
			}
		}
		return null;
	}
	public static function GetId()
	{
		$ids = self::GetIdsFromFile();
		$id = null;
		foreach($ids as $rowId=>$id)
		{
			$id = intval($id);
			if($id > 0)
			{
				unset($ids[$rowId]);
				break;
			}
			else
			{
				$id = null;
				unset($ids[$rowId]);
			}
		}
		self::SaveIdsToFile($ids);
		return $id;
	}
}