<?
namespace Evk\Books;
/**
 * Class PageNavigation
 * @package Evk\Books
 */
class PageNavigation
{
	/**
	 * @param \CDBResult $obNav
	 *
	 */
	public static function GetNextPage(\CDBResult $obNav, $paramString="", $paramDelete=array())
	{
		global $APPLICATION;
		if(!empty($paramString) && strpos($paramString, '=') === false)
		{
			$paramString = sprintf('%s=Y', $paramString);
			$paramDelete = array($paramString);
		}
		$navID = sprintf('PAGEN_%d', $obNav->NavNum);
		$pageCurrent = $obNav->NavPageNomer;
		$pagesCount = $obNav->NavPageCount;
		if($pageCurrent < $pagesCount)
		{
			if(empty($paramString))
				$paramString = sprintf('%s=%d', $navID, $pageCurrent+1);
			else
				$paramString = sprintf('%s&%s=%d', $paramString, $navID, $pageCurrent+1);
			$paramDelete = array_unique(array_merge(array($navID), $paramDelete));
			return $APPLICATION->GetCurPageParam($paramString, $paramDelete);
		}
	}
}
?>