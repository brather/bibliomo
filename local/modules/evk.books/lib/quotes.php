<?php
namespace Evk\Books;
\Bitrix\Main\Loader::includeModule("highloadblock");
\Bitrix\Main\Loader::includeModule("iblock");
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;
use Bitrix\Main\Type\DateTime;

class Quotes
{
	private function getEntity_data_class($HIBLOCK = HIBLOCK_QUO_DATA_USERS)
	{
		$hlBlock = HL\HighloadBlockTable::getById($HIBLOCK)->fetch();
		$entity = HL\HighloadBlockTable::compileEntity($hlBlock);
		$entity_data_class = $entity->getDataClass();
		return $entity_data_class;
	}

	/*
	@method add добавление цитаты
	@param $arFields
	@param $arFields['BOOK_ID'] - ID книги в Экзалиде
	@param $arFields['TEXT'] - текст цитаты
	@param $arFields['IMG_DATA'] - data image base64
	@param $arFields['PAGE'] - номер страницы
	@param $arFields['TOP'] - левый верхний угол: Y
	@param $arFields['LEFT'] - левый верхний угол: X
	@param $arFields['WIDTH'] - ширина блока выделения/картинки
	@param $arFields['HEIGHT'] - высота блока выделения/картинки
	*/
	public static function add(array $arFields,$uid = false)
	{
		global $USER;
		if ( !$uid )
			$uid = $USER->GetID();
		if(empty($arFields))
			return false;

		$arFilter = array(
			"IBLOCK_ID" => IBLOCK_ID_BOOKS,
			"IBLOCK_TYPE" => "library",
			"=ID" => $arFields['BOOK_ID'],
		);
		if($rsBook = \CIBlockElement::GetList(array(), $arFilter, false, false, array(
			"ID",
			"IBLOCK_ID",
			"NAME",
			"PROPERTY_AUTHOR",
			))
		)
		{
			if(($arBook = $rsBook->Fetch())==null)
			{
				return false;
			}
		}
		$entity_data_class = self::getEntity_data_class();
		$dt = new DateTime();
		$arFieldsAdd = array(
			'UF_DATE_ADD'		=> $dt,
			'UF_BOOK_ID'		=> $arFields['BOOK_ID'],
			'UF_UID'			=> $uid,
			'UF_TEXT'			=> $arFields['TEXT'],
			'UF_IMG_DATA'		=> $arFields['IMG_DATA'],
			'UF_PAGE'			=> $arFields['PAGE'],
			'UF_TOP'			=> $arFields['TOP'],
			'UF_LEFT'			=> $arFields['LEFT'],
			'UF_WIDTH'			=> $arFields['WIDTH'],
			'UF_HEIGHT'			=> $arFields['HEIGHT'],
			'UF_BOOK_NAME'		=> $arBook['NAME'],
			'UF_BOOK_AUTHOR'	=> $arBook['PROPERTY_AUTHOR_VALUE'],
		);
		// J::add('quote', 'add', Array('BOOK_ID' => $arFields['BOOK_ID'], 'DATE' => $dt, 'TEXT' => $arFields['TEXT'], 'BOOK_LINK' => '/catalog/'.$arFields['BOOK_ID'].'/'));
		$result = $entity_data_class::add($arFieldsAdd);
		$saveID = $result->getId();
		unset($_SESSION["BOOKS_EXTEND"][$arFields['BOOK_ID']]["QUOTES"]);
		return $saveID;
	}

	/*
	@method getListBook - получить список цитат для книги
	@param $BOOK_ID string
	*/

	public static function getListBook($bookID = false, $uid = false)
	{
		global $USER;
		if ( !$uid )
			$uid = $USER->GetID();

		if(empty($bookID))
			return false;

		$entity_data_class = self::getEntity_data_class();

		$rsData = $entity_data_class::getList(array(
			"select" => array('ID', 'UF_IMG_DATA', 'UF_PAGE', 'UF_TOP', 'UF_LEFT', 'UF_WIDTH', 'UF_HEIGHT', 'UF_DATE_ADD'),
			"filter" => array(
				'UF_UID' 		=> $uid,
				'UF_BOOK_ID' 	=> $bookID
			),
		));

		$arResult = array();

		while($arData = $rsData->Fetch())
		{
			$arResult[] = $arData;
		}

		return $arResult;
	}

	public static function getListForBooks($bookIDs = array(), $uid = null)
	{
		global $USER;
		if ( !is_numeric($uid) )
			$uid = $USER->GetID();

		if(!is_array($bookIDs))
			return false;

		$entity_data_class = self::getEntity_data_class();

		$rsData = $entity_data_class::getList(array(
			"select" => array('ID', "UF_BOOK_ID", 'UF_IMG_DATA', 'UF_PAGE', 'UF_TOP', 'UF_LEFT', 'UF_WIDTH', 'UF_HEIGHT', 'UF_DATE_ADD'),
			"filter" => array(
				'UF_UID' 		=> $uid,
				'=UF_BOOK_ID' 	=> $bookIDs,
			),
		));
		$arResult = array();
		while($arData = $rsData->Fetch())
		{
			$arResult[$arData["UF_BOOK_ID"]][] = $arData;
		}
		return $arResult;
	}

	public static function getListCountForBooks($bookIDs = array(), $uid = null)
	{
		global $USER;
		if ( !is_numeric($uid) )
			$uid = $USER->GetID();

		if(!is_array($bookIDs))
			return false;

		$entity_data_class = self::getEntity_data_class();

		$rsData = $entity_data_class::getList(array(
			"select" => array("UF_BOOK_ID", "CNT"),
			"filter" => array(
				'UF_UID' 		=> $uid,
				'=UF_BOOK_ID' 	=> $bookIDs,
			),
			"group" => array(
				"UF_BOOK_ID",
			),
			"runtime" => array(
				new Entity\ExpressionField('CNT', 'COUNT(*)'),
			),
		));
		if($rsData->getSelectedRowsCount() <= 0)
			return false;

		$arResult = array();
		while($arData = $rsData->Fetch())
		{
			$arResult[$arData["UF_BOOK_ID"]] = $arData["CNT"];
		}
		return $arResult;
	}

	public static function getById($ID)
	{
		if(empty($ID))
			return false;

		$entity_data_class = self::getEntity_data_class();

		$rsData = $entity_data_class::getList(array(
			"select" => array('*'),
			"filter" => array(
				#'UF_UID' 		=> $USER->GetID(),
				'ID'   			=> $ID
			),
		));
		return $rsData->Fetch();
	}

	/**
	 * Удаляет по паре UID + ID, заведомо проверив, реально ли эта запись принадлежит пользователю
	 */
	public static function deleteWithTest($id, $uid=false)
	{
		global $USER;
		if ( !$uid )
			$uid = $USER->GetID();

		$entity_data_class = self::getEntity_data_class();
		$toDelete = $entity_data_class::getList(array(
			"select" => array("ID", "UF_BOOK_ID"),
			"filter" => array(
				'ID' 		=> $id,
				'UF_UID' 	=> $uid,
			),
		))->Fetch();

		if ( !$toDelete )
			return false;

		self::delete($toDelete['ID']);
		unset($_SESSION["BOOKS_EXTEND"][$toDelete['UF_BOOK_ID']]["QUOTES"]);
		return true;
	}

	protected static function delete($id)
	{

		$id = intval($id);
		if($id <= 0)
			return false;

		$quote = self::getById($id);

		if(empty($quote))
			return false;

		// J::add('quote', 'delete', Array('BOOK_ID' => $quote['BOOK_ID'], 'DATE' => new DateTime(), 'TEXT' => $quote['TEXT'], 'BOOK_LINK' => '/catalog/'.$quote['BOOK_ID'].'/'));

		$entity_data_class = self::getEntity_data_class();
		$entity_data_class::Delete($quote['ID']);
		Collections::removeLinkObject($quote['ID'], false, 'quotes');

	}

	public static function update($id, $text)
	{
		$id = intval($id);
		$text = strip_tags(trim($text));

		if(empty($id) or empty($text))
			return false;

		$quote = self::getById($id);
		if(empty($quote))
			return false;

		// J::add('quote', 'edit', Array('BOOK_ID' => $quote['BOOK_ID'], 'DATE' => new DateTime(), 'TEXT' => $text, 'BOOK_LINK' => '/catalog/'.$quote['BOOK_ID'].'/'));

		$entity_data_class = self::getEntity_data_class();

		$arFields = array(
			'UF_TEXT' => $text
		);
		$entity_data_class::update($quote['ID'], $arFields);
		unset($_SESSION["BOOKS_EXTEND"][$quote['UF_BOOK_ID']]["QUOTES"]);
		return true;
	}
}