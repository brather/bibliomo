<?php
namespace Evk\Books;
\Bitrix\Main\Loader::includeModule("iblock");
\Bitrix\Main\Loader::includeModule("highloadblock");
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;
use Bitrix\Main\Type\DateTime;
class Bookmarks
{
	private function getEntity_data_class($HIBLOCK = HIBLOCK_BOOKMARKS_DATA_USERS){
		$hlblock = HL\HighloadBlockTable::getById($HIBLOCK)->fetch();
		$entity = HL\HighloadBlockTable::compileEntity($hlblock);
		$entity_data_class = $entity->getDataClass();
		return $entity_data_class;
	}

	public static function getListCountForBooks($bookIDs = array(), $uid = null)
	{
		global $USER;
		if ( !is_numeric($uid) )
			$uid = $USER->GetID();

		if( !is_array($bookIDs) )
			return false;

		$entity_data_class = self::getEntity_data_class();

		$rsData = $entity_data_class::getList(array(
			"select" => array('UF_BOOK_ID', "CNT"),
			"filter" => array(
				'UF_UID'        => $uid,
				'=UF_BOOK_ID'   => $bookIDs,
			),
			"group" => array('UF_BOOK_ID'),
			"runtime" => array(
				new Entity\ExpressionField("CNT", "COUNT(*)"),
			),
		));
		if($rsData->getSelectedRowsCount() <= 0)
			return false;
		$arResult = array();
		while($arData = $rsData->Fetch())
		{
			$arResult[$arData["UF_BOOK_ID"]] = $arData["CNT"];
		}
		return $arResult;
	}
	public static function getBookmarkList($bookID = false, $numPage = false, $uid = false)
	{
		global $USER;
		if ( !$uid )
			$uid = $USER->GetID();

		$entity_data_class = self::getEntity_data_class();
		$arFilter = array('UF_UID' => $uid);

		if ( (bool)$bookID )
			$arFilter['UF_BOOK_ID'] = $bookID;

		if ( (bool)$numPage )
			$arFilter['UF_NUM_PAGE'] = $numPage;

		$rsData = $entity_data_class::getList(array(
			"select" => array('ID', 'UF_BOOK_ID', 'UF_NUM_PAGE', 'UF_PREVIEW'),
			"filter" => $arFilter,
		));

		if($rsData->getSelectedRowsCount() <= 0)
			return false;

		if ( (bool)$bookID && (bool)$numPage )
		{
			$arData = $rsData->Fetch();
			$arResult = array(
				'ID' 		=> $arData['ID'],
				'BOOK_ID' 	=> $arData['UF_BOOK_ID'],
				'NUM_PAGE' 	=> $arData['UF_NUM_PAGE'],
				'PREVIEW' 	=> $arData['UF_PREVIEW']
			);
		}
		else
		{
			while($arData = $rsData->Fetch())
				$arResult[$arData['ID']] = array(
					'ID' 		=> $arData['ID'],
					'BOOK_ID' 	=> $arData['UF_BOOK_ID'],
					'NUM_PAGE' 	=> $arData['UF_NUM_PAGE'],
					'PREVIEW' 	=> $arData['UF_PREVIEW']
				);
		}

		return $arResult;
	}

	/**
	 * Получаем список заметок в книге для пользователя
	 *
	 * @param $bookID
	 * @param bool $uid
	 *
	 * @return array|bool
	 */
	public static function getBookmarksListJSON($bookID, $uid = false)
	{
		global $USER;

		if ( !$uid )
			$uid = $USER->GetID();

		if ( !(bool)$bookID || !(bool)$uid )
			return false;

		// Получаем список закладок
		$userBookmarksArray = self::getBookmarkList($bookID, false, $uid);
		if ( empty($userBookmarksArray) )
			return false;

		// Приводим ключики к нужному виду
		$jsonArray = array();
		foreach ( $userBookmarksArray as $userBookmark )
		{
			$jsonArray[] = array(
				'id' 		=> $userBookmark['ID'],
				'page' 		=> $userBookmark['NUM_PAGE'],
				'preview' 	=> $userBookmark['PREVIEW'],
			);
		}
		return $jsonArray;
	}

	/**
	 * @method add добавление закладки
	 * @param $bookId
	 * @param $numPage
	 * @param int|bool $uid
	 * @param $arDataFields - Массив дополнительных параметов
	 * @param $arDataFields['PREVIEW'] - выделенный текст
	 *
	 * @return int
	 */
	public function add($bookId, $numPage, $uid = false, $arDataFields)
	{
		global $USER;
		if ( !$uid )
			$uid = $USER->GetID();

		$bookmarkObject = self::getBookmarkList($bookId, $numPage, $uid);
		if($bookmarkObject !== false)
			return $bookmarkObject['ID'];

		$entity_data_class = self::getEntity_data_class();
		$dt = new DateTime();

		$arFields = array(
			'UF_UID'			=> $uid,
			'UF_BOOK_ID'		=> $bookId,
			'UF_NUM_PAGE'		=> $numPage,
			'UF_DATE_ADD'		=> $dt,
			'UF_PREVIEW' 		=> $arDataFields['PREVIEW'],
			'UF_BOOK_NAME'		=> $resExalead['title'],
			'UF_BOOK_AUTHOR'	=> $resExalead['authorbook'],
		);

		//J::add('bookmark', 'add', Array('BOOK_ID' => $bookId, 'BOOK_LINK' => '/catalog/'.$bookId.'/', 'DATE' => $dt));

		$result = $entity_data_class::add($arFields);
		$saveID = $result->getId();

		unset($_SESSION["BOOKS_EXTEND"][$bookId]["MARK"]);

		return $saveID;
	}

	/**
	 * Для api был добавлен метод add(), на тот момент список его параметров был актуален
	 * Для добавления из массива добавлен метод addFromArray, который уже использует add()
	 *
	 * @method addFromArray добавление закладки
	 * @param $arFields - Массив дополнительных параметов
	 * @param $arFields['BOOK_ID'] - ID книги
	 * @param $arFields['NUM_PAGE'] - Номер страницы
	 * @param $arFields['PREVIEW'] - превью
	 * @param $arFields['UID'] - ID пользователя
	 *
	 * @return int
	 */
	public static function addFromArray($arFields)
	{
		return self::add($arFields['BOOK_ID'], $arFields['NUM_PAGE'], $arFields['UID'], $arFields);
	}

	/**
	 * Удаляет по паре UID + ID, заведомо проверив, реально ли эта запись принадлежит пользователю
	 */
	public static function deleteWithTest($id, $uid=false)
	{

		global $USER;
		if ( !$uid )
			$uid = $USER->GetID();

		$entity_data_class = self::getEntity_data_class();
		$toDelete = $entity_data_class::getList(array(
			"select" => array("ID", "UF_BOOK_ID"),
			"filter" => array(
				'ID' 		=> $id,
				'UF_UID' 	=> $uid,
			),
		))->Fetch();

		if ( !$toDelete )
			return false;

		self::delete($toDelete['ID']);

		unset($_SESSION["BOOKS_EXTEND"][$toDelete["UF_BOOK_ID"]]["MARK"]);

		return true;
	}

	protected static function delete($id)
	{
		$id = intval($id);
		if($id <= 0)
			return false;

		$bookmark = self::getById($id);
		if(empty($bookmark))
			return false;

		//J::add('bookmark', 'delete', Array('BOOK_ID' => $bookmark['UF_BOOK_ID'], 'BOOK_LINK' => '/catalog/'.$bookmark['UF_BOOK_ID'].'/', 'DATE' => new DateTime()));

		$entity_data_class = self::getEntity_data_class();
		$entity_data_class::Delete($bookmark['ID']);
		Collections::removeLinkObject($bookmark['ID'], false, 'bookmarks');
	}

	public function getById($ID)
	{
		if(empty($ID))
			return false;

		$entity_data_class = self::getEntity_data_class();

		$rsData = $entity_data_class::getList(array(
			"select" => array('*'),
			"filter" => array(
				#'UF_UID' 		=> $USER->GetID(),
				'=ID'   			=> $ID
			),
		));

		return $rsData->Fetch();
	}


	public static function getMark($book_id, $page)
	{
		global $USER;
		$uid = $USER->GetID();

		$entity_data_class = self::getEntity_data_class();

		$rsData = $entity_data_class::getList(array(
			"select" => array('*'),
			"filter" => array(
				#'UF_UID' 		=> $USER->GetID(),
				'=UF_UID'   			=> $uid,
				'=UF_BOOK_ID'=> $book_id,
				'=UF_NUM_PAGE'=>$page
			),
		));

		return $rsData->Fetch();
	}
}