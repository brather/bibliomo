<?php
namespace Evk\Books;
use Bitrix\Main\Loader;
Loader::includeModule("iblock");
/**
 * Class BBKIndex
 * @property BBKIndex[] $children
 * @property BBKIndex $parent
 */
class BBKIndex
{
	public $section_id;
	public $section_code;
	public $name;
	public $children_index_codes = array();
	public $parent_index_code;
	public $children = array();
	public $parent;
	public $rubric;
	public $ext_rubric;
	public $see = array();
	public $see_also = array();
	public $related = array();
	public $depth = 1;
	public $element_count = 0;
	public $last = true;
	public function SetName($name)
	{
		$name = htmlspecialchars(trim($name));
		if(!empty($name))
		{
			$this->name = $name;
			$this->section_code = md5($name);
		}
	}
	public function __construct($name=null, BBKIndex $obParentIndex=null)
	{
		if(!is_null($name))
			$this->SetName($name);
		if(!is_null($obParentIndex))
		{
			$this->parent = $obParentIndex;
			$this->parent_index_code = $obParentIndex->section_code;
			$this->depth = $obParentIndex->depth + 1;
		}
	}
	public static function LoadFromBitrixByCode($code)
	{
		$rsData = \CIBlockSection::GetList(array(), array(
			"CODE" => $code,
			"IBLOCK_ID" => 15,
		), false, array("UF_*"));
		if($rsData && $arData = $rsData->Fetch())
		{
			return $arData;
		}
		return null;
	}
	public static function LoadFromBitrixByIndexCode($code)
	{
		$rsData = \CIBlockSection::GetList(array(), array(
			"CODE" => $code,
			"IBLOCK_ID" => 15,
		), false, array("UF_*"));
		if($rsData && $arData = $rsData->Fetch())
		{
			return $arData;
		}
		return null;
	}
	public function GetSectionCodeFromIndexName($indexName=null)
	{
		if(is_null($indexName))
			$indexName = $this->name;
		return (!empty($this->name)) ? md5($this->name) : null;
	}
	public function UpdateToBitrix($arData=array())
	{
		$arFields = array();
		$arFields["IBLOCK_ID"] = 15;
		if($arData["EXTERNAL_ID"] != $this->name)
			$arFields["EXTERNAL_ID"] = $this->name;
		if($this->section_code != $this->GetSectionCodeFromIndexName() || $arData["CODE"] != $this->GetSectionCodeFromIndexName())
			$arFields["CODE"] = $this->section_code;
		if(!empty($this->rubric))
		{
			if($arData["NAME"] != $this->rubric)
				$arFields["NAME"] = $this->rubric;
		}
		else
		{
			if($arData["NAME"] != $this->name)
				$arFields["NAME"] = $this->name;
		}
		if($arData["UF_RELATED"] != $this->related && !empty($this->related))
			$arFields["UF_RELATED"] = $this->related;
		if($arData["UF_SEE"] != $this->see && !empty($this->see))
			$arFields["UF_SEE"] = $this->see;
		if($arData["UF_SEE_ALSO"] != $this->see_also && !empty($this->see_also))
			$arFields["UF_SEE_ALSO"] = $this->see_also;
		if($arData["UF_EXT_RUBRIC"] != $this->ext_rubric && !empty($this->ext_rubric))
			$arFields["UF_EXT_RUBRIC"] = $this->ext_rubric;
		if(!empty($this->parent_index_code) && !empty($this->parent->section_id))
			$arFields["SECTION_ID"] = $this->parent->section_id;
		if($this->HasChildren())
			$arFields["UF_LAST"] = 0;
		else
			$arFields["UF_LAST"] = 1;
		if(count($arFields) > 1 && !empty($this->section_id))
		{
			$obSection = new \CIBlockSection;
			if($obSection->Update($this->section_id, $arFields))
				echo "Updated section: ".$this->section_id."<br/>";
			else
				echo $obSection->LAST_ERROR;
		}
	}
	public function GetCountBooksFromBBKCodes($arCode = array())
	{
		$obSearch = new SearchQuery();
		$arBooks = $obSearch->FindBooks(array(), array("UF_BBK" => $arCode), false , false, array("PROPERTY_BBK"));
		return print_r($arBooks, 1);
	}
	public function GetCountBooksFromBBKCode($code)
	{
		$obSearch = new SearchQuery();
		$arBooks = $obSearch->FindBooks(array(), array("PROPERTY_BBK" => $code));
		return count($arBooks);
	}
	public function SaveToBitrix()
	{
		$arData = $this->LoadFromBitrixByIndexCode($this->section_code);
		if(empty($arData))
		{
			$arFields = array();
			$arFields["IBLOCK_ID"] = 15;
			$arFields["EXTERNAL_ID"] = $this->name;
			$arFields["CODE"] = $this->section_code;
			if(!empty($this->rubric))
				$arFields["NAME"] = $this->rubric;
			else
				$arFields["NAME"] = $this->name;
			if(!empty($this->related))
				$arFields["UF_RELATED"] = $this->related;
			if($this->HasChildren())
				$arFields["UF_LAST"] = 0;
			else
				$arFields["UF_LAST"] = 1;
			if(!empty($this->see))
				$arFields["UF_SEE"] = $this->see;
			if(!empty($this->see_also))
				$arFields["UF_SEE_ALSO"] = $this->see_also;
			if(!empty($this->ext_rubric))
				$arFields["UF_EXT_RUBRIC"] = $this->ext_rubric;
			if(!empty($this->parent) && !empty($this->parent->section_id))
				$arFields["IBLOCK_SECTION_ID"] = $this->parent->section_id;
			$obSection = new \CIBlockSection;
			$this->section_id = $obSection->Add($arFields, false);
			if($this->section_id > 0)
				echo "Added section: ".$this->section_id."<br/>";
			else
				echo $obSection->LAST_ERROR;
		}
		/*
		elseif(is_array($arData))
		{
			$this->section_id = $arData["ID"];
			$this->UpdateToBitrix($arData);
		}
		*/
	}
	public function AddChildCode($name)
	{
		$name = htmlspecialchars(trim($name));
		$this->children_index_codes[] = $name;
	}
	public function AddChild($name)
	{
		$newObject = new self($name, $this);
		$this->children_index_codes[] = $newObject->section_code;
		$this->children[] = $newObject;
		return $newObject;
	}
	public function AddChildForExistsObject(BBKIndex $obChildIndex)
	{
		$this->children_index_codes[] = $obChildIndex->section_code;
		$this->children[] = $obChildIndex;
		return $obChildIndex;
	}
	public function PrintProperties()
	{
		$properties = array_keys(get_class_vars(__CLASS__));
		$return = "";
		foreach($properties as $property)
		{
			if((is_string($this->$property) || is_numeric($this->$property)) && strlen($this->$property) > 0)
			{
				$return .= sprintf('%s: %s<br/>', $property, $this->$property);
			}
		}
		if($this->HasChildren())
			$return .= sprintf('children: %s<br/>', $this->PrintChildrenCodes());
		return $return;
	}
	public function HasChildren()
	{
		return count($this->children) > 0;
	}
	public function GetChildren()
	{
		return $this->children;
	}
	public function SaveChildrenToBitrix()
	{
		if($this->HasChildren())
		{
			foreach($this->children as $index)
			{
				$index->SaveToBitrix();
				if($index->HasChildren())
					$index->SaveChildrenToBitrix();
			}
		}
	}
	public function PrintChildren()
	{
		$return = "";
		if(count($this->children) > 0)
		{
			foreach($this->children as $index)
			{
				$return .= $index->PrintProperties();
				if($index->HasChildren())
					$return .= $index->PrintChildren();
			}
		}
		return $return;
	}
	public function PrintChildrenCodes()
	{
		if(count($this->children_index_codes) > 0)
			return join(",", $this->children_index_codes);
		return null;
	}
	public static function GetBBKIndexFromBitrix($arData)
	{
		$obIndex = new self($arData["EXTERNAL_ID"]);
		$obIndex->section_id = $arData["ID"];
		$obIndex->section_code = $arData["CODE"];
		$obIndex->ext_rubric = $arData["UF_EXT_RUBRIC"];
		$obIndex->see = $arData["UF_SEE"];
		$obIndex->see_also = $arData["UF_SEE_ALSO"];
		$obIndex->related = $arData["DESCRIPTION"];
		$obIndex->rubric = $arData["NAME"];
		$obIndex->depth = $arData["DEPTH_LEVEL"];
		$obIndex->last = $arData["UF_LAST"];
		return $obIndex;
	}
	public function GetIndexNames()
	{
		$arNames = array();
		if(!$this->HasChildren())
			$arNames[] = $this->name;
		else
		{
			foreach($this->children as $index)
			{
				$arNames = array_merge($index->GetIndexNames(), $arNames);
			}
		}
		return $arNames;
	}
}

/*
	public $section_id;
	public $name;
	public $children_index_names = array();
	public $parent_index_name;
	public $children = array();
	public $parent;
	public $rubric;
	public $ext_rubric;
	public $see;
	public $see_also;
	public $related;
	public $depth = 0;
*/
/*
Array
(
    [ID] => 36
    [TIMESTAMP_X] => 30.06.2015 02:46:37
    [MODIFIED_BY] => 1
    [DATE_CREATE] => 30.06.2015 02:01:10
    [CREATED_BY] => 1
    [IBLOCK_ID] => 15
    [IBLOCK_SECTION_ID] =>
    [ACTIVE] => Y
    [GLOBAL_ACTIVE] => Y
    [SORT] => 500
    [NAME] => Библиотечно-библиографическая классификация. Средние таблицы
    [PICTURE] =>
    [LEFT_MARGIN] => 53
    [RIGHT_MARGIN] => 4328
    [DEPTH_LEVEL] => 1
    [DESCRIPTION] =>
    [DESCRIPTION_TYPE] => text
    [SEARCHABLE_CONTENT] => БИБЛИОТЕЧНО-БИБЛИОГРАФИЧЕСКАЯ КЛАССИФИКАЦИЯ. СРЕДНИЕ ТАБЛИЦЫ

    [CODE] => .
    [XML_ID] =>
    [TMP_ID] =>
    [DETAIL_PICTURE] =>
    [SOCNET_GROUP_ID] =>
    [LIST_PAGE_URL] => #SITE_DIR#/catalog_bbk/index.php
    [SECTION_PAGE_URL] => #SITE_DIR#/catalog_bbk/#SECTION_ID#/
    [IBLOCK_TYPE_ID] => collections
    [IBLOCK_CODE] => catalog_bbk
    [IBLOCK_EXTERNAL_ID] =>
    [EXTERNAL_ID] =>
    [UF_SEE] =>
    [UF_SEE_ALSO] =>
    [UF_EXT_RUBRIC] =>
    [UF_NAME_INDEX] => .
    [UF_LAST] =>
)
*/
