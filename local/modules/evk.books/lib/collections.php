<?
namespace Evk\Books;
\CModule::IncludeModule("highloadblock");
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;
use Bitrix\Main\Type\DateTime;
use Evk\Books\Books;

class Collections
{
	private function getEntity_data_class($HIBLOCK = HIBLOCK_COLLECTIONS_USERS)
	{
		$hlblock = HL\HighloadBlockTable::getById($HIBLOCK)->fetch();
		$entity = HL\HighloadBlockTable::compileEntity($hlblock);
		$entity_data_class = $entity->getDataClass();
		return $entity_data_class;
	}
	public static function isAdd()
	{
		global $USER;
		if (!$USER->IsAuthorized())
			return false;

		$uObj = new \nebUser();
		$role = $uObj->getRole();
		return ($role == 'user');
	}

	public static function GetList($arParams=array())
	{
		$entity_data_class = self::getEntity_data_class();
		$rsData = $entity_data_class::getList($arParams);
		return $rsData->fetchAll();
	}

	public static function getListCurrentUser($objectId = false, $type = false, $select = false){
		global $USER;
		$entity_data_class = self::getEntity_data_class();

		$rsData = $entity_data_class::getList(array(
			"select" => array("ID", "UF_NAME"),
			"filter" => array('UF_UID' => $USER->GetID()),
			"order"  => array('UF_SORT' => 'ASC', 'UF_DATE_ADD' => 'ASC')
		));

		if($rsData->getSelectedRowsCount() <= 0)
			return false;

		$arResult = array();

		if(!empty($objectId) and !empty($type))
			$arLink = self::getListLink($objectId, $type);

		while($arData = $rsData->Fetch())
		{
			if(array_key_exists($arData["ID"], $arLink["COLLECTION_BOOKS"]))
				$arData["BOOKS"] = $arLink["COLLECTION_BOOKS"][$arData["ID"]];
			else
				$arData["BOOKS"] = array();
			$arResult[] = $arData;
		}

		return $arResult;
	}

	public static function add($name, $objectId = false, $type = false){
		if(empty($name))
			return false;

		global $USER;
		$entity_data_class = self::getEntity_data_class();

		$dt = new DateTime();

		$arFields = array(
			'UF_DATE_ADD'		=> $dt,
			'UF_NAME'			=> $name,
			'UF_UID'			=>	$USER->GetID(),
		);

		$result = $entity_data_class::add($arFields);
		$saveID = $result->getId();
		if(!empty($objectId) and !empty($type) and $saveID > 0)
			self::addLinkObject($objectId, $saveID, $type);

		return $saveID;
	}

	public function delete($id){
		if((int)($id) <= 0)
			return false;

		global $USER;
		$entity_data_class = self::getEntity_data_class();

		$rsData = $entity_data_class::getList(array(
			"select" => array("ID"),
			"filter" => array('UF_UID' => $USER->GetID(), 'ID' => $id)
		));

		if($arData = $rsData->Fetch()){
			$entity_data_class::Delete($arData['ID']);
			self::removeAllLinkObject($arData['ID']);
		}

	}

	public function getListLink($idObject, $type)
	{
		if(empty($idObject) or empty($type))
			return false;

		if($type == 'books')
		{
			$rsBook = Books::getListCurrentUser($idObject);
			if($rsBook !== false && array_key_exists("ID", $rsBook))
				$idObject = $rsBook['ID'];
			else
				$idObject = array_values($rsBook);
		}

		$entity_data_class = self::getEntity_data_class(HIBLOCK_COLLECTIONS_LINKS_USERS);
		$rsData = $entity_data_class::getList(array(
			"select" => array("UF_COLLECTION_ID", "UF_OBJECT_ID"),
			"filter" => array('UF_TYPE' => $type, 'UF_OBJECT_ID' => $idObject)
		));
		$rsBookFlip = array_flip($rsBook);
//		Books::p(, );
		$arResult = array(
			"COLLECTIONS" => array(),
			"COLLECTION_BOOKS" => array(),
			"BOOK_COLLECTIONS" => array(),
		);
		while($arData = $rsData->Fetch())
		{
			$arResult["COLLECTIONS"][$rsBookFlip[$arData["UF_OBJECT_ID"]]] = $arData;
			$arResult["COLLECTION_BOOKS"][$arData["UF_COLLECTION_ID"]][] = $rsBookFlip[$arData["UF_OBJECT_ID"]];
			$arResult["BOOK_COLLECTIONS"][$rsBookFlip[$arData["UF_OBJECT_ID"]]][] = $arData["UF_COLLECTION_ID"];
		}
		return $arResult;
	}

	public static function addLinkObject($idObject, $idCollection, $type){
		if(empty($idObject) or empty($idCollection) or empty($type))
			return false;

		$entity_data_class = self::getEntity_data_class(HIBLOCK_COLLECTIONS_LINKS_USERS);
		if($type == 'books')
		{
			$rsBook = Books::getListCurrentUser($idObject);
			if($rsBook !== false)
				$idObject = $rsBook['ID'];
		}


		$rsData = $entity_data_class::getList(array(
			"select" => array("ID"),
			"filter" => array('UF_TYPE' => $type, 'UF_OBJECT_ID' => $idObject, 'UF_COLLECTION_ID' => $idCollection)
		));

		if($arData = $rsData->Fetch())
			return $arData['ID'];

		$arFields = array(
			'UF_TYPE'			=> $type,
			'UF_OBJECT_ID'		=> $idObject,
			'UF_COLLECTION_ID'	=> $idCollection,
		);

		$result = $entity_data_class::add($arFields);
		return $result->getId();
	}
	public static function removeLinkObject($idObject, $idCollection = false, $type = false)
	{
		if(empty($idObject))
			return false;

		$entity_data_class = self::getEntity_data_class(HIBLOCK_COLLECTIONS_LINKS_USERS);

		if($type == 'books')
		{
			$rsBook = Books::getListCurrentUser($idObject);
			if($rsBook !== false && array_key_exists('ID', $rsBook))
				$idObject = $rsBook['ID'];
			else
				$idObject = array_values($rsBook);
		}
		if($idCollection===0 && $type == 'books')
		{
			$arFilter = array('UF_OBJECT_ID' => $idObject);
			if(!empty($type))
				$arFilter['UF_TYPE'] = $type;
			$rsData = $entity_data_class::getList(array(
				"select" => array("ID", "UF_COLLECTION_ID"),
				"filter" => $arFilter
			));
			$collectionIDs = array();
			while($arData = $rsData->Fetch())
			{
				$collectionIDs[] = $arData["UF_COLLECTION_ID"];
				$entity_data_class::Delete($arData['ID']);
			}
			return (!empty($collectionIDs) ? join(",", $collectionIDs) : null);
		}
		$arFilter = array('UF_OBJECT_ID' => $idObject);
		if(!empty($type))
			$arFilter['UF_TYPE'] = $type;
		if(!empty($idCollection))
			$arFilter['UF_COLLECTION_ID'] = $idCollection;

		$rsData = $entity_data_class::getList(array(
			"select" => array("ID"),
			"filter" => $arFilter
		));

		while($arData = $rsData->Fetch())
			$entity_data_class::Delete($arData['ID']);
	}

	public function removeAllLinkObject($idCollection){
		if(empty($idCollection))
			return false;

		$entity_data_class = self::getEntity_data_class(HIBLOCK_COLLECTIONS_LINKS_USERS);

		$rsData = $entity_data_class::getList(array(
			"select" => array("ID"),
			"filter" => array('UF_COLLECTION_ID' => $idCollection)
		));

		while($arData = $rsData->Fetch())
			$entity_data_class::Delete($arData['ID']);
	}

}
?>