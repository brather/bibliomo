<?php
namespace Evk\Books;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc as Loc;
Loader::includeModule("iblock");
/**
 * Class SearchQuery
 * @package Evk\Books
 * @property QueryLogic $obLogic
 * @property \CDBResult $obNavigate
 */
class SearchQuery
{
	protected $arVarsReplace = array(
		"publishplace" => "PROPERTY_PUBLISH_PLACE",
		"publisher" => "PROPERTY_PUBLISH",
		"title" => "NAME",
		"authorbook" => "PROPERTY_AUTHOR",
		"library" => "PROPERTY_LIBRARIES",
		"ALL" => true,
	);
	protected $arVarsLogic = array(
		"OR",
		"AND",
		"NOT",
		"OR_NOT",
		"AND_NOT",
	);
	protected $arExistsVars = array(
		"logic",
		"text",
		"theme",
	);
	protected $arVars = array();
	protected $obLogic;
	protected $arVarsCount = 0;
	protected $arResult;
	protected $arParams;
	protected $IBLOCK_ID = 10;
	protected $SAFE_FILTER_NAME = "searchFilter";
	protected $queryExists = false;
	protected $nPageSize = 0;
	protected $iNumPage = 0;
	protected $arFilterVars = array();
	public $obNavigate = null;
	protected $sphinxEmpty = true;
	public $QUERY = "";
	public function __construct(&$arResult=array(), &$arParams=array())
	{
		$this->arResult = &$arResult;
		$this->arParams = &$arParams;
	}
	public function getById($ID, $arSelect=array())
	{
		$arData = $this->FindBooksSimple(array(), array("ID"=>$ID), false, false, $arSelect);
		return $arData;
	}
	public function setPageNum($num)
	{
		$this->iNumPage = intval($num);
	}
	public function setPageLimit($nPageSize)
	{
		$this->nPageSize = intval($nPageSize);
	}
	public function IsQueryExists()
	{
		return $this->checkQueryExists($_REQUEST);
	}
	public function getByArIds($arIDs)
	{
		$arElements = array();
		if(is_array($arIDs))
		{
			$this->obNavigate = \CIBlockElement::GetList(array(), array(
				"IBLOCK_ID" => IBLOCK_ID_BOOKS,
				"=ID" => $arIDs,
			));
			while($arData = $this->obNavigate->Fetch())
			{
				$arElements[] = $arData;
			}
		}
		return $arElements;
	}
	public function SendQueryToSphinx($arFilter=array(), $aSort=array(), $request=null)
	{
		$arrFilter = $this->GetQueryFromRequest($request);
		$search = new SearchSphinx();
		$arFilter = array_merge_recursive($arFilter, $arrFilter);

		return $search->SendQueryToSphinx($arFilter, $aSort, $this->arParams["S_STRICT"], true);
	}
	public static function Create(&$arResult=array(), &$arParams=array())
	{
		return new self($arResult, $arParams);
	}
	public function CreateQuery(&$arrFilter=array(), $arFilter=array(), $aSort=array(), $request=null)
	{
		$this->GetElementsFilterFromSphinx($arrFilter, $arFilter=array(), $aSort=array(), $request=null);
		$arFilter = $this->getFilter();
		$arrFilter = array_merge_recursive($arFilter, $this->getYearField($_REQUEST["publishyear_prev"], $_REQUEST["publishyear_next"]), $arrFilter);
	}
	public function IsSphinxEmpty()
	{
		return $this->sphinxEmpty;
	}
	protected function GetElementsFilterFromSphinx(&$arrFilter=array(), $arFilter=array(), $aSort=array(), $request=null)
	{
		$this->arResult["ELEMENTS"] = $this->SendQueryToSphinx($arFilter, $aSort, $request);

		if (is_array($this->arResult["ELEMENTS"]) && !empty($this->arResult["ELEMENTS"]))
		{
			$this->sphinxEmpty = $this->arResult["ELEMENTS"]["COUNT"] <= 0;
			$arIDs = array();
			foreach($this->arResult["ELEMENTS"]["ITEMS"] as $itemID=>$arItem)
			{
				$arIDs[$arItem["ID"]] = $itemID;
			}
			if(count($arIDs) > 0)
			{
				$arrFilter = array_merge_recursive($arrFilter, array(
					"=ID" => $arIDs,
				));
				$_SESSION["BOOKS_STRICT_IDS"] = $arIDs;
			}
		}
	}
	protected function getFilter()
	{
		$this->arVars = $this->get_vars_exists();
		$this->obLogic = new QueryLogic();
		$this->obLogic->AddQueryArray($this->arVars);
		return $this->obLogic->GetFilter();
	}
	protected function get_vars_exists()
	{
		if ($this->vars_exists()) {
			$arReturn = array();
			foreach ($_REQUEST[$this->arExistsVars[0]] as $key => $val) {
				$arVarExists = $this->get_var_exists($key);
				if (is_array($arVarExists)) {
					$arReturn = array_merge_recursive($arReturn, $arVarExists);
				}
			}
			$this->arFilterVars = $arReturn;
			$this->arVarsCount = count($arReturn);
			return $arReturn;
		}
		return array();
	}
	protected function get_var_exists($key)
	{
		$varExists = true;
		$arReturn = array();
		foreach ($this->arExistsVars as $name) {
			$varExists = ($varExists) ? $this->var_exists($_REQUEST[$name][$key]) : false;
			switch ($name) {
				case "logic": {
					$varExists = ($varExists) ? in_array($_REQUEST[$name][$key], $this->arVarsLogic) : false;
					break;
				}
				case "theme": {
					$varExists = ($varExists) ? array_key_exists($_REQUEST[$name][$key], $this->arVarsReplace) : false;
					break;
				}
			}
			$arReturn[$name][] = htmlspecialchars($_REQUEST[$name][$key]);
		}
		return ($varExists) ? $arReturn : null;
	}
	protected function vars_exists()
	{
		$varExists = true;
		foreach ($this->arExistsVars as $name) {
			$varExists = ($varExists) ? $this->var_exists($_REQUEST[$name]) : false;
		}
		return $varExists;
	}
	protected function var_exists($var)
	{
		return (isset($var) && ((is_array($var) && count($var) > 0) || (is_string($var) && strlen($var) > 0)));
	}
	protected function getYearField($prevYear=0, $nextYear=0)
	{
		$selectYear = 0;
		if(isset($_REQUEST["publishyear"]))
			$selectYear = intval($_REQUEST["publishyear"]);
		$prevYear = intval($prevYear);
		$nextYear = intval($nextYear);
		if($prevYear < PUBLISH_YEAR_MIN)
			$prevYear = PUBLISH_YEAR_MIN;
		if($nextYear > PUBLISH_YEAR_MAX || $nextYear == 0)
			$nextYear = PUBLISH_YEAR_MAX;
		if($nextYear < $prevYear)
			$nextYear = $prevYear;
		if($selectYear >= PUBLISH_YEAR_MIN && $selectYear <= PUBLISH_YEAR_MAX)
		{
			$prevYear = $nextYear = $selectYear;
		}
		else
			$selectYear = 0;
		$arReturn = array();
		if($prevYear > PUBLISH_YEAR_MIN || $nextYear < PUBLISH_YEAR_MAX)
		{
			$arReturn[0] = array(
				"LOGIC" => "OR",
			);
			$arReturn[0][] = array(
				">=PROPERTY_PUBLISH_YEAR" => intval($prevYear),
				"<=PROPERTY_PUBLISH_YEAR" => intval($nextYear),
			);
			if(!$selectYear)
				$arReturn[0][] = array("PROPERTY_PUBLISH_YEAR" => false);
		}
		return $arReturn;
	}
	public function FindBooksSimple($arSort=array("ID"=>"DESC"), $arFilter=array(), $aGroupBy=false, $aNavStartParams=false, $arSelectFields=array(), $single=false)
	{
		$arBooks = array();
		$arDefaultFilter = array(
			"IBLOCK_ID" => IBLOCK_ID_BOOKS,
			"IBLOCK_TYPE" => "library",
		);
		$arDefaultSelectFields = array(
			"ID",
			"IBLOCK_ID",
		);

		if(in_array("PROPERTY_FILE", $arSelectFields))
		{
			$arSelectFields[] = "PROPERTY_STATUS";
			$arSelectFields[] = "PROPERTY_COUNT_PAGES";
			$arSelectFields[] = "PROPERTY_COVER_NUMBER_PAGE";
		}

		$arSelectFields = array_merge($arSelectFields, $arDefaultSelectFields);

		if(in_array("CNT_MARKS", $arSelectFields))
		{
			$MARKS = array();
			unset($arSelectFields[array_search("CNT_MARKS", $arSelectFields)]);
		}
		if(in_array("CNT_QUOTES", $arSelectFields))
		{
			$QUOTES = array();
			unset($arSelectFields[array_search("CNT_QUOTES", $arSelectFields)]);
		}
		if(in_array("CNT_NOTES", $arSelectFields))
		{
			$NOTES = array();
			unset($arSelectFields[array_search("CNT_NOTES", $arSelectFields)]);
		}
		$arFilter = array_merge($arFilter, $arDefaultFilter);
		$arNav = array();
		if ($this->nPageSize > 0)
			$arNav["nPageSize"] = $this->nPageSize;
		if ($this->iNumPage > 0)
			$arNav["iNumPage"] = $this->iNumPage;
		if (count($arNav) > 0)
		{
			if(!is_array($aNavStartParams))
			{
				$aNavStartParams = $arNav;
			}
			else
			{
				$aNavStartParams = array_merge($aNavStartParams, $arNav);
			}
		}
		$this->obNavigate = \CIBlockElement::GetList($arSort, $arFilter, $aGroupBy, $aNavStartParams, $arSelectFields);
		$arLibraryIDs = array();
		$arFilesIDs = array();
		$i=0;
		while($arBook = $this->obNavigate->Fetch())
		{
			if(empty($arBook["PROPERTY_VOLUME_VALUE"]))
			{
				if(!empty($arBook["PROPERTY_COUNT_PAGES_VALUE"]))
					$arBook["PROPERTY_VOLUME_VALUE"] = $arBook["PROPERTY_COUNT_PAGES_VALUE"];
			}
			if(isset($arBook["NAME"]))
				$arBook["title"] = $arBook["NAME"];
			if(!empty($arBook["PROPERTY_AUTHOR_VALUE"]))
			{
				$arBook["authorbook"] = $arBook["PROPERTY_AUTHOR_VALUE"];
				$arBook["r_authorbook"] = $this->GetSearchParam(array("AUTHOR" => $arBook["authorbook"]));
			}
			if(!empty($arBook["PROPERTY_PUBLISH_VALUE"]))
			{
				$arBook["publisher"] = $arBook["PROPERTY_PUBLISH_VALUE"];
				$arBook["r_publisher"] = $this->GetSearchParam(array("PUBLISH" => $arBook["publisher"]));
			}
			if(!empty($arBook["PROPERTY_PUBLISH_PLACE_VALUE"]))
			{
				$arBook["publish_place"] = $arBook["PROPERTY_PUBLISH_PLACE_VALUE"];
				$arBook["r_publish_place"] = $this->GetSearchParam(array("PUBLISH_PLACE" => $arBook["publish_place"]));
			}
			if(!isset($arBook["ID"]))
				$ID = $i++;
			else
			{
				$ID = $arBook["ID"];
				$arBook["id"] = $arBook["~id"] = $ID;
			}
			$arBooks[$ID] = $arBook;
			if(is_array($arBook["PROPERTY_LIBRARIES_VALUE"]) && count($arBook["PROPERTY_LIBRARIES_VALUE"]) > 0)
				$arLibraryIDs = array_unique(array_merge($arLibraryIDs, $arBook["PROPERTY_LIBRARIES_VALUE"]));
			if(!empty($arBook["PROPERTY_FILE_VALUE"]))
				$arFilesIDs[] = $arBook["PROPERTY_FILE_VALUE"];
			$arBooksID[] = $arBook["ID"];
		}
		if(isset($MARKS))
			$MARKS = Bookmarks::getListCountForBooks($arBooksID);
		if(isset($NOTES))
			$NOTES = Notes::getListCountForBooks($arBooksID);
		if(isset($QUOTES))
			$QUOTES = Quotes::getListCountForBooks($arBooksID);
		$libIDUrl = array();
		if(!empty($arLibraryIDs))
		{
			$libIDUrl = $this->getLibraryList($arLibraryIDs);
			foreach($libIDUrl as $libKey=>$libVal)
			{
				$libIDUrl[$libKey]["SEARCH_URL"] = $this->GetSearchParam(array("LIBRARIES" => $libVal["ID"]));
			}
		}
		$arFiles = array();
		if(!empty($arFilesIDs))
			$arFiles = $this->getFileArray($arFilesIDs);
		foreach($arBooks as &$arBook)
		{
			$arItem["CNT_MARKS"] = $arItem["CNT_NOTES"] = $arItem["CNT_QUOTES"] = 0;
			if(is_array($libIDUrl) && count($libIDUrl))
			{
				foreach($arBook["PROPERTY_LIBRARIES_VALUE"] as $lib)
				{
					if(!is_array($arBook["LIBS"]))
						$arBook["LIBS"] = array();
					if(array_key_exists($lib, $libIDUrl))
						$arBook["LIBS"][$lib] = $libIDUrl[$lib];
				}
			}
			if(isset($arBook["ID"]))
				$arBook["DETAIL_PAGE_URL"] = $arBook["URL"] = sprintf("/catalog/%d/", $arBook["ID"]);
			if(!empty($arBook["PROPERTY_FILE_VALUE"]))
			{
				$arBook["FILE"] = $arFiles[$arBook["PROPERTY_FILE_VALUE"]];
				if(isset($arBook["ID"])
					&& $arBook["PROPERTY_STATUS_VALUE"] == 2
					&& intval($arBook["PROPERTY_COUNT_PAGES_VALUE"]) > 0)
					$arBook = array_merge($arBook, Books::GetBookUrls($arBook["ID"], 0, $arBook["PROPERTY_COVER_NUMBER_PAGE_VALUE"]));
				$arBook["COVER_NUMBER_PAGE"] = (intval($arBook["PROPERTY_COVER_NUMBER_PAGE_VALUE"]) > 0) ? intval($arBook["PROPERTY_COVER_NUMBER_PAGE_VALUE"]) : 1;

			}
			else
				$arItem["FILE"] = array();
			if(is_array($MARKS))
				$arBook["CNT_MARKS"] = intval($MARKS[$arBook["ID"]]);
			if(is_array($NOTES))
				$arBook["CNT_NOTES"] = intval($NOTES[$arBook["ID"]]);
			if(is_array($QUOTES))
				$arBook["CNT_QUOTES"] = intval($QUOTES[$arBook["ID"]]);
		}

		if($single)
		{
			$arBooks = array_values($arBooks);
			$arBooks = $arBooks[0];
		}
		return $arBooks;
	}
	protected function getLibraryList($arLibraryIDs)
	{
		$libIDUrl = array();
		if(is_array($arLibraryIDs) && count($arLibraryIDs))
		{
			$queryLibs = Array('IBLOCK_ID' => IBLOCK_ID_LIBRARY, '=ID' => $arLibraryIDs);
			$tmpLibrary = \Evk\Books\IBlock\Element::getList($queryLibs, 0, array("ID", "DETAIL_PAGE_URL"));
			foreach($tmpLibrary["ITEMS"] as $libItem)
			{
				$libIDUrl[$libItem["ID"]] = $libItem;
			}
		}
		return $libIDUrl;
	}
	protected function getFileArray($arFilesIDs)
	{
		$arFiles = array();
		if(is_array($arFilesIDs) && count($arFilesIDs))
		{
			$rsFiles = \CFile::GetList(array(), array("@ID" => join(",", $arFilesIDs)));
			while($arFile = $rsFiles->Fetch())
			{
				$arFiles[$arFile["ID"]] = $arFile;
			}
		}
		return $arFiles;
	}
	public function FindBooks($arSort=array("SORT"=>"ASC"), $arFilter=array(), $aGroupBy=false, $aNavStartParams=false, $arSelectFields=array(), $request=null)
	{
		$arFilter = array_merge($arFilter, array(
			"IBLOCK_ID" => IBLOCK_ID_BOOKS,
			"IBLOCK_TYPE" => "library",
		));
		if(!is_array($arSelectFields))
			$arSelectFields = array();
		$arSelectFields = array_merge_recursive($arSelectFields, array(
			"ID",
			"NAME",
			"IBLOCK_ID",
			"IBLOCK_TYPE",
			"PROPERTY_LIBRARIES",
			"PROPERTY_FILE",
			"PROPERTY_AUTHOR",
			"PROPERTY_PUBLISH_YEAR",
			"PROPERTY_PUBLISH_PLACE",
			"PROPERTY_PUBLISH",
			"PROPERTY_VOLUME",
			"PROPERTY_COUNT_PAGES",
			"PROPERTY_COVER_NUMBER_PAGE",
		));
		if(in_array("CNT_MARKS", $arSelectFields))
		{
			$MARKS = array();
			unset($arSelectFields[array_search("CNT_MARKS", $arSelectFields)]);
		}
		if(in_array("CNT_QUOTES", $arSelectFields))
		{
			$QUOTES = array();
			unset($arSelectFields[array_search("CNT_QUOTES", $arSelectFields)]);
		}
		if(in_array("CNT_NOTES", $arSelectFields))
		{
			$NOTES = array();
			unset($arSelectFields[array_search("CNT_NOTES", $arSelectFields)]);
		}
		if(is_array($request))
		{
			$this->FillFilterArray($request, $arFilter);
			$this->GetAllSearchElementsForSmartFilter($arFilter, $arFilter);
		}
		$arNav = array();
		if ($this->nPageSize > 0)
			$arNav["nPageSize"] = $this->nPageSize;
		if ($this->iNumPage > 0)
			$arNav["iNumPage"] = $this->iNumPage;
		if (count($arNav) > 0)
		{
			if(!is_array($aNavStartParams))
			{
				$aNavStartParams = $arNav;
			}
			else
			{
				$aNavStartParams = array_merge($aNavStartParams, $arNav);
			}
		}
		$arSortKeys = array_keys($arSort);
		$arSortValues = array_values($arSort);
		foreach($arSortValues as &$key)
		{
			if(stripos("nulls", $key)===false)
			{
				$key .= ",nulls";
			}
		}
		$arSort = array_combine($arSortKeys, $arSortValues);
		//print '<pre>' . print_r($arFilter, true) . '</pre>';
		//print '<pre>' . print_r($aGroupBy, true) . '</pre>';
		$this->obNavigate = \CIBlockElement::GetList($arSort, $arFilter, $aGroupBy, $aNavStartParams, $arSelectFields);
		$arResult["ITEMS"] = array();
		$arLibraryIDs = array();
		$arFilesIDs = array();
		$arBooksID = array();
		$i=0;
		while($arItem = $this->obNavigate->GetNext())
		{
			$arItem["title"] = $arItem["NAME"];
			if(isset($this->arResult["ELEMENTS"]) &&
				isset($this->arResult["ELEMENTS"]["ITEMS"]) &&
				array_key_exists($arItem["ID"], $this->arResult["ELEMENTS"]["ITEMS"]) &&
				count($this->arResult["ELEMENTS"]["ITEMS"][$arItem["ID"]]) > 0
			)
			{
				$arItem["SEARCH_ITEM"] = $this->arResult["ELEMENTS"]["ITEMS"][$arItem["ID"]];
				$arItem["NAME"] = $arItem["title"] = $arItem["SEARCH_ITEM"]["TITLE"];
			}
			else
			{
				if(strlen($this->QUERY) > 0)
				{
					$arItem["TEXT"] = preg_replace(sprintf('@(%s)@iu', $this->QUERY), '<b>$1</b>', $arItem["TEXT"]);
					$arItem["NAME"] = preg_replace(sprintf('@(%s)@iu', $this->QUERY), '<b>$1</b>', $arItem["NAME"]);
				}
			}
			if(strlen($this->QUERY) > 0)
			{
				$arItem["PROPERTY_PUBLISH_VALUE"] = preg_replace(sprintf('@(%s)@iu', $this->QUERY), '<b>$1</b>', $arItem["PROPERTY_PUBLISH_VALUE"]);
				$arItem["PROPERTY_AUTHOR_VALUE"] = preg_replace(sprintf('@(%s)@iu', $this->QUERY), '<b>$1</b>', $arItem["PROPERTY_AUTHOR_VALUE"]);
			}
			$arItem["NUM_ITEM"] = ++$i + ($this->obNavigate->NavPageNomer*$this->obNavigate->NavPageSize - $this->obNavigate->NavPageSize);
			$arResult["ITEMS"][] = $arItem;
			if(is_array($arItem["PROPERTY_LIBRARIES_VALUE"]) && count($arItem["PROPERTY_LIBRARIES_VALUE"]) > 0)
				$arLibraryIDs = array_unique(array_merge($arLibraryIDs, $arItem["PROPERTY_LIBRARIES_VALUE"]));
			if(!empty($arItem["PROPERTY_FILE_VALUE"]))
				$arFilesIDs[] = $arItem["PROPERTY_FILE_VALUE"];
			$arBooksID[] = $arItem["ID"];
		}
		$libIDUrl = $this->getLibraryList($arLibraryIDs);
		$arFiles = $this->getFileArray($arFilesIDs);
		if(isset($MARKS))
			$MARKS = Bookmarks::getListCountForBooks($arBooksID);
		if(isset($NOTES))
			$NOTES = Notes::getListCountForBooks($arBooksID);
		if(isset($QUOTES))
			$QUOTES = Quotes::getListCountForBooks($arBooksID);

		foreach($arResult["ITEMS"] as &$arItem)
		{
			$arItem["CNT_MARKS"] = $arItem["CNT_NOTES"] = $arItem["CNT_QUOTES"] = 0;
			$arItem["LIBS"] = array();
			foreach($arItem["PROPERTY_LIBRARIES_VALUE"] as $lib)
			{
				$arItem["LIBS"][$lib] = $libIDUrl[$lib];
			}
			if(!empty($arItem["PROPERTY_FILE_VALUE"]))
			{
				$arFile = $arFiles[$arItem["PROPERTY_FILE_VALUE"]];
				$book = new Books($arFile, 0, $arItem["ID"], $arItem["PROPERTY_COVER_NUMBER_PAGE_VALUE"]);
				$arItem["IMAGE_URL"] = $book->GetLink("preview");
				$arItem["COVER_NUMBER_PAGE"] = (intval($arItem["PROPERTY_COVER_NUMBER_PAGE_VALUE"]) > 0) ? intval($arItem["PROPERTY_COVER_NUMBER_PAGE_VALUE"]) : 1;
				$arItem["FILE"] = array_merge($arFile, array("FILE_PATH" => "/catalog/".$arItem["ID"]."/viewer/"));
			}
			else
			{
				$arItem["FILE"] = array();
			}
			if(isset($arItem["ID"]))
				$arItem["DETAIL_PAGE_URL"] = $arItem["URL"] = sprintf("/catalog/%d/", $arItem["ID"]);
			if(is_array($MARKS))
				$arItem["CNT_MARKS"] = intval($MARKS[$arItem["ID"]]);
			if(is_array($NOTES))
				$arItem["CNT_NOTES"] = intval($NOTES[$arItem["ID"]]);
			if(is_array($QUOTES))
				$arItem["CNT_QUOTES"] = intval($QUOTES[$arItem["ID"]]);
		}
		$arResult["NAV_STRING"] = $this->obNavigate->GetPageNavStringEx($navComponentObject, $this->arParams["PAGER_TITLE"], $this->arParams["PAGER_TEMPLATE"], $this->arParams["PAGER_SHOW_ALWAYS"]);
		$arResult["NAV_CACHED_DATA"] = $navComponentObject->GetTemplateCachedData();
		$arResult["NAV_RESULT"] = $this->obNavigate;
		return $arResult;
	}
	public function GetAllSearchElementsForSmartFilter(&$arFilter, $arrFilter)
	{
		global $USER;
		$arAllElements = array();
		$cache_array_elements_id = array($arFilter, ($this->arParams["CACHE_GROUPS"]==="N"? false: $USER->GetGroups()));
		$obCache = new \CPHPCache();
		$cache_path = "/".SITE_ID."/neb/catalog.section.elements/";
		if($obCache->InitCache($this->arParams["CACHE_TIME"], md5(serialize($cache_array_elements_id)), $cache_path))
		{
			extract($obCache->GetVars());
		}
		else
		{
			if($obCache->StartDataCache($this->arParams["CACHE_TIME"], md5(serialize($cache_array_elements_id)), $cache_path))
			{
				global $CACHE_MANAGER;
				$CACHE_MANAGER->StartTagCache($cache_path);
				$rsElements = \CIBlockElement::GetList(array('ID'=>"ASC"), $arFilter, false, false, Array("IBLOCK_ID", "ID"));
				$arAllElements = array();
				while($arItem = $rsElements->Fetch())
				{
					$arAllElements[] = $arItem["ID"];
				}
				$CACHE_MANAGER->RegisterTag("iblock_id_library_catalog_section_all");
				$CACHE_MANAGER->EndTagCache();
				$obCache->EndDataCache(compact('arAllElements'));
			}
		}
		$this->arResult["ALL_ITEM_IDS"] = $arAllElements;
	}
	public function CountFilter()
	{
		if(is_object($this->obLogic) && !empty($this->arFilterVars))
		{
			return count($this->arFilterVars["text"]);
		}
		return null;
	}
	public function FillFilterArray(&$_CHECK=null, &$arrFilter=array(), $arFilter=array(), $aSort=array("CUSTOM_RANK"=>"DESC", "RANK"=>"DESC", "DATE_CHANGE"=>"DESC"))
	{
		if(is_null($_CHECK))
		{
			$_CHECK = $_REQUEST;
		}
		if(!is_object($this->obLogic)) $this->obLogic = new QueryLogic();
		if (count($_CHECK) > 0)
		{
			$allCHECKED = array();
			foreach ($this->arResult["PROPS_ITEMS"] as $PID => $arItem)
			{
				foreach ($arItem["VALUES"] as $key => $ar)
				{
					if (
						isset($_CHECK[$ar["CONTROL_NAME"]])
						|| (
							isset($_CHECK[$ar["CONTROL_NAME_ALT"]])
							&& $_CHECK[$ar["CONTROL_NAME_ALT"]] == $ar["HTML_VALUE_ALT"]
						)
					) {
						if ($arItem["PROPERTY_TYPE"] == "N") {
							$this->arResult["PROPS_ITEMS"][$PID]["VALUES"][$key]["HTML_VALUE"] = htmlspecialcharsbx($_CHECK[$ar["CONTROL_NAME"]]);
							$this->arResult["PROPS_ITEMS"][$PID]["DISPLAY_EXPANDED"] = "Y";
						} elseif ($_CHECK[$ar["CONTROL_NAME"]] == $ar["HTML_VALUE"]) {
							$this->arResult["PROPS_ITEMS"][$PID]["VALUES"][$key]["CHECKED"] = true;
							$this->arResult["PROPS_ITEMS"][$PID]["DISPLAY_EXPANDED"] = "Y";
							$allCHECKED[$PID][$ar["VALUE"]] = true;
						} elseif ($_CHECK[$ar["CONTROL_NAME_ALT"]] == $ar["HTML_VALUE_ALT"]) {
							$this->arResult["PROPS_ITEMS"][$PID]["VALUES"][$key]["CHECKED"] = true;
							$this->arResult["PROPS_ITEMS"][$PID]["DISPLAY_EXPANDED"] = "Y";
							$allCHECKED[$PID][$ar["VALUE"]] = true;
						}
					}
				}
			}
		}

		$this->CreateQuery($arrFilter, $arFilter, $aSort, $_CHECK);
		$this->ModifyFilterArray($arrFilter);
		$this->arParams['q'] = htmlspecialchars(trim($_CHECK["q"]));
	}
	protected function getResultItems()
	{
		$items = $this->getIBlockItems($this->IBLOCK_ID);
		$this->arResult["PROPERTY_COUNT"] = count($items);
		$this->arResult["PROPERTY_ID_LIST"] = array_keys($items);
		return $items;
	}
	public function getIBlockItems($IBLOCK_ID)
	{
		$items = array();
		foreach (\CIBlockSectionPropertyLink::GetArray($IBLOCK_ID) as $PID => $arLink) {
			if ($arLink["SMART_FILTER"] !== "Y")
				continue;
			$rsProperty = \CIBlockProperty::GetByID($PID);
			$arProperty = $rsProperty->Fetch();
			if ($arProperty) {
				$items[$arProperty["ID"]] = array(
					"ID" => $arProperty["ID"],
					"IBLOCK_ID" => $arProperty["IBLOCK_ID"],
					"CODE" => $arProperty["CODE"],
					"NAME" => $arProperty["NAME"],
					"PROPERTY_TYPE" => $arProperty["PROPERTY_TYPE"],
					"USER_TYPE" => $arProperty["USER_TYPE"],
					"USER_TYPE_SETTINGS" => $arProperty["USER_TYPE_SETTINGS"],
					"DISPLAY_TYPE" => $arLink["DISPLAY_TYPE"],
					"DISPLAY_EXPANDED" => $arLink["DISPLAY_EXPANDED"],
					"VALUES" => array(),
				);
				if ($arProperty["PROPERTY_TYPE"] == "N") {
					$minID = $this->SAFE_FILTER_NAME . '_' . $arProperty['ID'] . '_MIN';
					$maxID = $this->SAFE_FILTER_NAME . '_' . $arProperty['ID'] . '_MAX';
					$items[$arProperty["ID"]]["VALUES"] = array(
						"MIN" => array(
							"CONTROL_ID" => $minID,
							"CONTROL_NAME" => $minID,
						),
						"MAX" => array(
							"CONTROL_ID" => $maxID,
							"CONTROL_NAME" => $maxID,
						),
					);
				}
			}
		}
		return $items;
	}
	public function fillItemValues(&$resultItem, $arProperty, $flag = null)
	{
		static $cache = array();

		if (is_array($arProperty)) {
			$key = $arProperty["VALUE"];
			$PROPERTY_TYPE = $arProperty["PROPERTY_TYPE"];
			$PROPERTY_USER_TYPE = $arProperty["USER_TYPE"];
			$PROPERTY_ID = $arProperty["ID"];
		} else {
			$key = $arProperty;
			$PROPERTY_TYPE = $resultItem["PROPERTY_TYPE"];
			$PROPERTY_USER_TYPE = $resultItem["USER_TYPE"];
			$PROPERTY_ID = $resultItem["ID"];
			$arProperty = $resultItem;
		}
		if ($PROPERTY_TYPE == "F") {
			return null;
		} elseif ($PROPERTY_TYPE == "N") {
			$convertKey = (float)$key;
			if (strlen($key) <= 0) {
				return null;
			}
			if (!isset($resultItem["VALUES"]["MIN"]) || !array_key_exists("VALUE", $resultItem["VALUES"]["MIN"]) || doubleval($resultItem["VALUES"]["MIN"]["VALUE"]) > $convertKey)
				$resultItem["VALUES"]["MIN"]["VALUE"] = preg_replace("/\\.0+\$/", "", $key);

			if (!isset($resultItem["VALUES"]["MAX"]) || !array_key_exists("VALUE", $resultItem["VALUES"]["MAX"]) || doubleval($resultItem["VALUES"]["MAX"]["VALUE"]) < $convertKey)
				$resultItem["VALUES"]["MAX"]["VALUE"] = preg_replace("/\\.0+\$/", "", $key);

			return null;
		} elseif ($PROPERTY_TYPE == "E" && $key <= 0) {
			return null;
		} elseif ($PROPERTY_TYPE == "G" && $key <= 0) {
			return null;
		} elseif (strlen($key) <= 0) {
			return null;
		}

		$htmlKey = htmlspecialcharsbx($key);
		if (isset($resultItem["VALUES"][$htmlKey])) {
			$resultItem["VALUES"][$htmlKey]["COUNT"]++;
			return $htmlKey;
		}

		$arUserType = array();
		if ($PROPERTY_USER_TYPE != "") {
			$arUserType = \CIBlockProperty::GetUserType($PROPERTY_USER_TYPE);
			if (isset($arUserType["GetExtendedValue"]))
				$PROPERTY_TYPE = "Ux";
			elseif (isset($arUserType["GetPublicViewHTML"]))
				$PROPERTY_TYPE = "U";
		}

		$file_id = null;

		switch ($PROPERTY_TYPE) {
			case "L":
				$enum = \CIBlockPropertyEnum::GetByID($key);
				if ($enum) {
					$value = $enum["VALUE"];
					$sort = $enum["SORT"];
				} else {
					return null;
				}
				break;
			case "E":
				if (!isset($cache[$PROPERTY_TYPE][$key])) {
					$arLinkFilter = array(
						"ID" => $key,
						"ACTIVE" => "Y",
						"ACTIVE_DATE" => "Y",
						"CHECK_PERMISSIONS" => "Y",
					);
					$rsLink = \CIBlockElement::GetList(array(), $arLinkFilter, false, false, array("ID", "IBLOCK_ID", "NAME", "SORT"));
					$cache[$PROPERTY_TYPE][$key] = $rsLink->Fetch();
				}
				$value = $cache[$PROPERTY_TYPE][$key]["NAME"];
				$sort = $cache[$PROPERTY_TYPE][$key]["SORT"];
				break;
			case "G":
				if (!isset($cache[$PROPERTY_TYPE][$key])) {
					$arLinkFilter = array(
						"ID" => $key,
						"GLOBAL_ACTIVE" => "Y",
						"CHECK_PERMISSIONS" => "Y",
					);
					$rsLink = \CIBlockSection::GetList(array(), $arLinkFilter, false, array("ID", "IBLOCK_ID", "NAME", "LEFT_MARGIN", "DEPTH_LEVEL"));
					$cache[$PROPERTY_TYPE][$key] = $rsLink->Fetch();
					$cache[$PROPERTY_TYPE][$key]['DEPTH_NAME'] = str_repeat(".", $cache[$PROPERTY_TYPE][$key]["DEPTH_LEVEL"]) . $cache[$PROPERTY_TYPE][$key]["NAME"];
				}

				$value = $cache[$PROPERTY_TYPE][$key]['DEPTH_NAME'];
				$sort = $cache[$PROPERTY_TYPE][$key]["LEFT_MARGIN"];
				break;
			case "U":
				if (!isset($cache[$PROPERTY_ID]))
					$cache[$PROPERTY_ID] = array();

				if (!isset($cache[$PROPERTY_ID][$key])) {
					$cache[$PROPERTY_ID][$key] = call_user_func_array(
						$arUserType["GetPublicViewHTML"],
						array(
							$arProperty,
							array("VALUE" => $key),
							array("MODE" => "SIMPLE_TEXT"),
						)
					);
				}
				$value = $cache[$PROPERTY_ID][$key];
				$sort = 0;
				break;
			case "Ux":
				if (!isset($cache[$PROPERTY_ID]))
					$cache[$PROPERTY_ID] = array();

				if (!isset($cache[$PROPERTY_ID][$key])) {
					$cache[$PROPERTY_ID][$key] = call_user_func_array(
						$arUserType["GetExtendedValue"],
						array(
							$arProperty,
							array("VALUE" => $key),
						)
					);
				}

				$value = $cache[$PROPERTY_ID][$key]['VALUE'];
				$file_id = $cache[$PROPERTY_ID][$key]['FILE_ID'];
				$sort = (isset($cache[$PROPERTY_ID][$key]['SORT']) ? $cache[$PROPERTY_ID][$key]['SORT'] : 0);
				break;
			default:
				$value = $key;
				$sort = 0;
				break;
		}
		$keyCrc = abs(crc32($htmlKey));
		$value = htmlspecialcharsex($value);
		$sort = (int)$sort;

		$filterPropertyID = $this->SAFE_FILTER_NAME . '_' . $PROPERTY_ID;
		$filterPropertyIDKey = $filterPropertyID . '_' . $keyCrc;
		$resultItem["VALUES"][$htmlKey] = array(
			"CONTROL_ID" => $filterPropertyIDKey,
			"CONTROL_NAME" => $filterPropertyIDKey,
			"CONTROL_NAME_ALT" => $filterPropertyID,
			"PROPERTY_ID" => $PROPERTY_ID,
			"HTML_VALUE_ALT" => $keyCrc,
			"HTML_VALUE" => "Y",
			"VALUE" => $value,
			"SORT" => $sort,
			"UPPER" => ToUpper($value),
			"FLAG" => $flag,
			"SHOW" => "N",
		);

		if ($file_id) {
			$resultItem["VALUES"][$htmlKey]['FILE'] = \CFile::GetFileArray($file_id);
		}
		$resultItem["VALUES"][$htmlKey]["COUNT"]++;
		return $htmlKey;
	}
	public function FillPropsForSmartFilter()
	{
		$this->arResult["PROPS_ITEMS"] = $this->getResultItems();
		$propertyEmptyValuesCombination = array();
		foreach ($this->arResult["PROPS_ITEMS"] as $PID => $arItem)
			$propertyEmptyValuesCombination[$arItem["ID"]] = array();
		if (!empty($this->arResult["PROPS_ITEMS"]))
		{
			$arElementFilter = array(
				"IBLOCK_ID" => IBLOCK_ID_BOOKS,
				"ACTIVE" => "Y",
			);
			$arElements = array();

			if (!empty($this->arResult["PROPERTY_ID_LIST"])) {
				$rsElements = \CIBlockElement::GetPropertyValues(IBLOCK_ID_BOOKS, $arElementFilter, false, array('ID' => $this->arResult["PROPERTY_ID_LIST"]));
				while ($arElement = $rsElements->Fetch())
					$arElements[$arElement["IBLOCK_ELEMENT_ID"]] = $arElement;
			}
			else {
				$rsElements = \CIBlockElement::GetList(array('ID' => 'ASC'), $arElementFilter, false, false, array('ID', 'IBLOCK_ID'));
				while ($arElement = $rsElements->Fetch()) {
					$arElements[$arElement["ID"]] = array();
				}
			}
			$uniqTest = array();
			foreach ($arElements as $arElement)
			{
				$propertyValues = $propertyEmptyValuesCombination;
				$uniqStr = '';
				foreach ($this->arResult["PROPS_ITEMS"] as $PID => $arItem) {
					if (is_array($arElement[$PID])) {
						foreach ($arElement[$PID] as $value) {
							$key = $this->fillItemValues($this->arResult["PROPS_ITEMS"][$PID], $value);
							$propertyValues[$PID][$key] = $this->arResult["PROPS_ITEMS"][$PID]["VALUES"][$key]["VALUE"];
							$this->arResult["PROPS_ITEMS"][$PID]["VALUES"][$key]["IBLOCK_ELEMENT_IDS"][] = $arElement["IBLOCK_ELEMENT_ID"];
							$uniqStr .= '|' . $key . '|' . $propertyValues[$PID][$key];
						}
					} elseif ($arElement[$PID] !== false) {
						$key = $this->fillItemValues($this->arResult["PROPS_ITEMS"][$PID], $arElement[$PID]);
						$propertyValues[$PID][$key] = $this->arResult["PROPS_ITEMS"][$PID]["VALUES"][$key]["VALUE"];
						$this->arResult["PROPS_ITEMS"][$PID]["VALUES"][$key]["IBLOCK_ELEMENT_IDS"][] = $arElement["IBLOCK_ELEMENT_ID"];
						$uniqStr .= '|' . $key . '|' . $propertyValues[$PID][$key];
					}
				}
				$uniqCheck = md5($uniqStr);
				if (isset($uniqTest[$uniqCheck]))
					continue;
				$uniqTest[$uniqCheck] = true;
			}
			foreach ($this->arResult["PROPS_ITEMS"] as $PID => &$arItem2) {
				$this->sortValuesByCount($arItem2["VALUES"]);
			}
		}
	}
	public function sortValuesByCount(&$arValues)
	{
		uasort($arValues, function ($b, $a) {
			if (array_key_exists("COUNT", $a) && array_key_exists("COUNT", $b))
				return (($a["COUNT"] > $b["COUNT"]) ? 1 : (($a["COUNT"] < $b["COUNT"]) ? -1 : 0));
			if (array_key_exists("COUNT", $a) && !array_key_exists("COUNT", $b))
				return 1;
			if (!array_key_exists("COUNT", $a) && array_key_exists("COUNT", $b))
				return -1;
			return 0;
		});
	}
	public function ModifyFilterArray(&$arrFilter)
	{
		foreach ($this->arResult["PROPS_ITEMS"] as $PID => &$arItem)
		{
			foreach ($arItem["VALUES"] as $key => &$ar) {
				if ($ar["CHECKED"]) {
					$filterKey = "=PROPERTY_" . $PID;
					$backKey = htmlspecialcharsback($key);

					if (!isset($arrFilter[$filterKey]))
						$arrFilter[$filterKey] = array($backKey);
					elseif (!is_array($arrFilter[$filterKey]))
						$arrFilter[$filterKey] = array($arrFilter[$filterKey], $backKey);
					elseif (!in_array($backKey, $arrFilter[$filterKey]))
						$arrFilter[$filterKey][] = $backKey;
				}
				else
				{
					/*
					if($this->CountFilter())
					{
						foreach($this->arFilterVars['text'] as $id=>$value)
						{
							if($this->arFilterVars['theme'][$id] == 'library')
								continue;
							$filterKey = $this->arVarsReplace[$this->arFilterVars['theme'][$id]];
							if (!isset($arrFilter[$filterKey]))
							{
								$ar["CHECKED"] = true;
								$arrFilter[$filterKey] = array(htmlspecialcharsback($value));
								break;
							}
						}
					}
					*/
				}
			}

		}
		$this->GetSmartFilterForExalead($arrFilter);
	}
	public function CheckFilterExists()
	{
		$arLogic = $this->obLogic->GetFilter();
		return count($arLogic) > 0;
	}
	public function CheckExtendedFilterExists()
	{
		if(is_object($this->obLogic) && (
				($this->IsQueryExists() && !$this->IsSphinxEmpty()) ||
				(!$this->IsQueryExists() && $this->IsSphinxEmpty())
			)
		)
		{
			return true;
		}
		return false;
	}
	public function GetResultParams()
	{
		foreach ($this->obLogic->GetList() as $obLogic) {
			$this->arResult['RESULT_PARAMS'][] = array(
				"logic" => $obLogic->GetLogic(),
				"text" => Loc::getMessage("SEARCH_PARAM_" . $obLogic->GetOriginTheme(), array("#VALUE#" => $obLogic->GetText())),
			);
		}

		$publishyear_prev = (isset($_REQUEST["publishyear_prev"])
			&& intval($_REQUEST["publishyear_prev"]) >= PUBLISH_YEAR_MIN
			&& intval($_REQUEST["publishyear_prev"]) <= PUBLISH_YEAR_MAX)
			? intval($_REQUEST["publishyear_prev"]) : PUBLISH_YEAR_MIN;
		$publishyear_next = (isset($_REQUEST["publishyear_next"])
			&& intval($_REQUEST["publishyear_next"]) >= PUBLISH_YEAR_MIN
			&& intval($_REQUEST["publishyear_next"]) <= PUBLISH_YEAR_MAX)
			? intval($_REQUEST["publishyear_next"]) : PUBLISH_YEAR_MAX;

		if($publishyear_prev > $publishyear_next)
			$publishyear_prev = $publishyear_next;
		if($publishyear_prev > PUBLISH_YEAR_MIN || $publishyear_next < PUBLISH_YEAR_MAX)
		{
			$this->arResult['RESULT_PARAMS'][] = array(
				"logic" => "AND",
				"text" => sprintf('дата публикации: с %d по %d год', $publishyear_prev, $publishyear_next)
			);
		}
	}
	public function GetSmartFilterForExalead($arrFilter)
	{
		$arrFilterExalead = array();
		$arKeys = array(
			48 => "authorbook",
			53 => "publisher",
			63 => "library",
			52 => "publishplace",
		);
		foreach ($arrFilter as $key => $val) {
			if (preg_match("@^=PROPERTY_(\d+)$@", $key, $m)) {
				if (array_key_exists($m[1], $arKeys)) {
					if ($m[1] == 63) {
						$stringValue = $this->arResult["PROPS_ITEMS"][$m[1]]["VALUES"][$val[0]]["VALUE"];
					} else {
						$stringValue = $val[0];
					}

					$this->arResult['RESULT_PARAMS'][] = array(
						"logic" => "AND",
						"text" => Loc::getMessage("SEARCH_PARAM_" . strtoupper($arKeys[$m[1]]), array("#VALUE#" => htmlspecialchars($stringValue))),
					);
					$arrFilterExalead["f_field"][$arKeys[$m[1]]] = urlencode(sprintf("f/%s/%s", $arKeys[$m[1]], strtolower($stringValue)));
				}
			}
		}
		if (count($arrFilterExalead["f_field"]) > 0)
		{
			$arrFilterExalead["dop_filter"] = "Y";
		}
		$this->arResult["SMART_FILTER"] = array_merge_recursive($_REQUEST, $arrFilterExalead);
		$this->GetResultParams();
	}
	public static function GetSearchParamForRequest($arParam = array())
	{
		$propertyNames = array(
			"AUTHOR" => 48,
			"LIBRARIES" => 63,
			"PUBLISH_PLACE" => 52,
			"PUBLISH" => 53,
		);
		if(is_array($arParam) && count($arParam) > 0)
		{
			$arParams = array();
			foreach($arParam as $PID=>$val)
			{
				$val = trim($val);
				if(empty($val)) continue;
				if(!is_numeric($PID) && array_key_exists($PID, $propertyNames))
				{
					$PID = $propertyNames[$PID];
				}
				elseif(!is_numeric($PID))
				{
					continue;
				}
				$htmlKey = htmlspecialcharsbx($val);
				$keyCrc = abs(crc32($htmlKey));
				$filterPropertyID = "searchFilter" . '_' . $PID;
				$filterPropertyIDKey = $filterPropertyID . '_' . $keyCrc;
				$arParams[$filterPropertyIDKey] = "Y";
			}
			return http_build_query($arParams);
		}
		return false;
	}
	public static function GetSearchParam($arParam = array())
	{
		if($str = self::GetSearchParamForRequest($arParam))
		{
			return "/search/?".$str;
		}
		return false;
	}
	public static function GetAutoComplete($pid, $q)
	{
		$arFind = array();
		$obSearch = new static();
		if($pid == "AUTHOR")
		{
			$PROPS = $obSearch->GetDataForSmartFilter();
			$arFind = self::findNeededBeginningStringInArray($q, array_keys($PROPS[48]["VALUES"]), "Автор");
		}
		elseif($pid == "TITLE")
		{
			$ITEMS = $obSearch->GetTitlesArray();
			$arFind = self::findNeededBeginningStringInArray($q, $ITEMS, "Название книги");
		}
		elseif($pid == "LIBRARY")
		{
			$PROPS = $obSearch->GetDataForSmartFilter();
			$arLibrary = array();
			foreach($PROPS[63]["VALUES"] as $val)
			{
				$arLibrary[] = $val["VALUE"];
			}
			$arFind = self::findNeededBeginningStringInArray($q, $arLibrary, "Библиотека");
		}
		return $arFind;
	}
	protected static function findNeededBeginningStringInArray($string, &$array, $category)
	{
		$arFind = array();
		$i = 0;
		foreach($array as $text)
		{
			$text = ltrim($text);
			if(stripos($text, $string)===0)
			{
				$arFind[] = array(
					"label" => $text,
					"original" => $text,
					"category" => $category,
				);
				if(++$i >= 20) break;
			}
		}
		return $arFind;
	}
	public function GetLibrariesArray()
	{

	}
	public function GetTitlesArray()
	{
		$cache_id = "CACHE_ID";
		$cache_path = "/".SITE_ID."/neb/search.page.titles/";
		if(!isset($this->arParams["CACHE_TIME"]))
			$cache_time = 3600000000;
		else
			$cache_time = intval($this->arParams["CACHE_TIME"]);
		$ITEMS = array();
		$phpCache = new \CPHPCache();
		if($phpCache->InitCache($cache_time, $cache_id, $cache_path))
		{
			$vars = $phpCache->GetVars();
			extract($vars);
		}
		else
		{
			if($phpCache->StartDataCache($cache_time, $cache_id, $cache_path))
			{
				$rsFind = \CIBlockElement::GetList(array("cnt"=>"desc"), array(
					"IBLOCK_ID" => IBLOCK_ID_BOOKS,
					"IBLOCK_TYPE" => "library",
				), array(
					"NAME"
				), false, array(
					"ID",
					"IBLOCK_ID",
					"NAME",
				));
				while($arFindElements = $rsFind->Fetch())
				{
					$ITEMS[] = $arFindElements["NAME"];
				}
				global $CACHE_MANAGER;
				$CACHE_MANAGER->StartTagCache($cache_path);
				$CACHE_MANAGER->RegisterTag("iblock_id_library_search_page_titles");
				$CACHE_MANAGER->EndTagCache();
				$phpCache->EndDataCache(array(
					"ITEMS" => $ITEMS,
				));
			}
		}
		return $ITEMS;
	}
	public function GetDataForSmartFilter()
	{
		$cache_id = "CACHE_ID";
		$cache_path = "/".SITE_ID."/neb/search.page.smart/";
		if(!isset($this->arParams["CACHE_TIME"]))
			$cache_time = 3600000000;
		else
			$cache_time = intval($this->arParams["CACHE_TIME"]);
		$PROPS_ITEMS = array();
		$phpCache = new \CPHPCache();
		if($phpCache->InitCache($cache_time, $cache_id, $cache_path))
		{
			$vars = $phpCache->GetVars();
			extract($vars);
		}
		else
		{
			if($phpCache->StartDataCache($cache_time, $cache_id, $cache_path))
			{
				$this->FillPropsForSmartFilter();
				$PROPS_ITEMS = $this->arResult["PROPS_ITEMS"];
				global $CACHE_MANAGER;
				$CACHE_MANAGER->StartTagCache($cache_path);
				$CACHE_MANAGER->RegisterTag("iblock_id_library_search_page_smart");
				$CACHE_MANAGER->EndTagCache();
				$phpCache->EndDataCache(array(
					"PROPS_ITEMS" => $PROPS_ITEMS,
				));
			}
		}
		return ($this->arResult["PROPS_ITEMS"] = $PROPS_ITEMS);
	}
	public function GetQueryFromRequest($request=null)
	{
		if(strlen($this->arParams["FILTER_NAME"])<=0 || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $this->arParams["FILTER_NAME"]))
			$arFILTERCustom = array();
		else
		{
			$arFILTERCustom = $GLOBALS[$this->arParams["FILTER_NAME"]];
			if(!is_array($arFILTERCustom))
				$arFILTERCustom = array();
		}
		if(is_null($request))
			$request = $_REQUEST;
		$arFILTERCustom = $this->checkInResults($request);
		$this->checkQueryExists($request);
		return $this->getFilterForSphinx($arFILTERCustom);
	}
	protected function getFilterForSphinx($arFILTERCustom=array())
	{
		$arFilter = array(
			"SITE_ID" => SITE_ID,
			"QUERY" => $this->arResult["REQUEST"]["~QUERY"],
		);
		return array_merge($arFILTERCustom, $arFilter);
	}
	protected function checkInResults($request)
	{
		$arFILTERCustom = array();
		if(isset($request["s_in_results"]) && $request["s_in_results"]=="Y")
		{
			if(isset($_SESSION["BOOKS_STRICT_IDS"]) && !empty($_SESSION["BOOKS_STRICT_IDS"]))
			{
				$arFILTERCustom["ITEM_ID"] = array_values($_SESSION["BOOKS_STRICT_IDS"]);
			}
		}
		else
		{
			unset($_SESSION["BOOKS_STRICT_IDS"]);
		}
		return $arFILTERCustom;
	}
	protected function checkQueryExists($request)
	{
		if(isset($request["q"]))
			$q = trim($request["q"]);
		else
			$q = false;
		if(strlen($q) > 0)
		{
			$this->arResult["REQUEST"]["~QUERY"] = $q;
			$this->arResult["REQUEST"]["QUERY"] = htmlspecialcharsex($q);
			$this->queryExists = true;
			$this->QUERY = $this->arResult["REQUEST"]["QUERY"];
		}
		else
		{
			$this->arResult["REQUEST"]["~QUERY"] = false;
			$this->arResult["REQUEST"]["QUERY"] = false;
			$this->queryExists = false;
		}
		$this->arResult["URL"] = $GLOBALS["APPLICATION"]->GetCurPage()."?q=".urlencode($q);
		return $this->queryExists;
	}
}