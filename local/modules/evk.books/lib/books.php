<?php
namespace Evk\Books;
\Bitrix\Main\Loader::includeModule("iblock");
\Bitrix\Main\Loader::includeModule("highloadblock");
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;
use Bitrix\Main\Type\DateTime;
use Evk\Books\Stat\BookDownload;

class Books
{
	protected $FILE_ID = 0;
	protected $BOOK_ID = 0;
	protected $PAGE_ID = 1;
	protected $WIDTH = 0;
	protected $HEIGHT = 0;
	protected $COVER_NUMBER_PAGE = 1;
	protected $COUNT_PAGES;
	protected $arFile = array();
	protected $tempDir;
	protected $previewDir;
	protected $detailDir;
	protected $filesDir;
	protected $normalDir;
	protected $pngFiles = array();
	protected $jpegFiles = array();
	protected $imageQuality = 70;
	protected $previewWidth = 162;
	protected $detailWidth = 1200;
	protected $normalWidth = 800;
	protected $landScope = "P"; // P - Portrait, A - Album
	protected $arLinks=array();
	public function GetLandScope()
	{
		return $this->landScope;
	}
	public static function getAllCnt()
	{
		$entity_data_class = self::getEntity_data_class();
		global $USER;
		$rsData = $entity_data_class::getList(array(
			"runtime" => array('cnt' => array('expression' => array('COUNT(*)'), 'data_type'=>'integer')),
			"select" => array('cnt'),
			"filter" => array('UF_UID' => $USER->GetID()),
		));
		$arData = $rsData->Fetch();
		return (int)$arData['cnt'];
	}
	public static function getReadCnt()
	{
		$entity_data_class = self::getEntity_data_class();
		global $USER;
		$rsData = $entity_data_class::getList(array(
			"runtime" => array('cnt' => array('expression' => array('COUNT(*)'), 'data_type'=>'integer')),
			"select" => array('cnt'),
			"filter" => array('UF_UID' => $USER->GetID(), '=UF_READING' => 2),
		));
		$arData = $rsData->Fetch();
		return (int)$arData['cnt'];
	}

	/**
	 * @return array
	 * Получения количества книг с PDF и без PDF
	 * array("BOOKS_WITHOUT_FILES" => 0, "BOOKS_WITH_FILES" => 0);
	 */
	public static function GetAllCount()
	{
		$arResult = array("BOOKS_WITHOUT_FILES" => 0, "BOOKS_WITH_FILES" => 0, "BOOKS_WITH_READY_FILES" => 0);
		$obCache = new \CPHPCache();
		$cache_time = 36000000;
		$cache_dir = sprintf("/%s/%s/", SITE_ID, "books_get_all_count_dir");
		$cache_id = md5($cache_dir);
		//Cache::ClearByTag($cache_id);
		if($obCache->InitCache($cache_time, $cache_id, $cache_dir))
		{
			extract($obCache->GetVars());
		}
		else
		{
			if($obCache->StartDataCache($cache_time, $cache_id, $cache_dir))
			{
				$obSearch = new SearchQuery();
				$arSearch = $obSearch->FindBooksSimple(array(), array("ACTIVE_DATE" => "Y", "ACTIVE" => "Y", "PROPERTY_FILE" => false), array("IBLOCK_ID"));
				$arResult["BOOKS_WITHOUT_FILES"] = intval($arSearch[0]["CNT"]);
				$arSearch = $obSearch->FindBooksSimple(array(), array("ACTIVE_DATE" => "Y", "ACTIVE" => "Y", "!PROPERTY_FILE" => false), array("IBLOCK_ID"));
				$arResult["BOOKS_WITH_FILES"] = intval($arSearch[0]["CNT"]);
				$arSearch = $obSearch->FindBooksSimple(array(), array("ACTIVE_DATE" => "Y", "ACTIVE" => "Y", "PROPERTY_STATUS" => 2), array("IBLOCK_ID"));
				$arResult["BOOKS_WITH_READY_FILES"] = intval($arSearch[0]["CNT"]);
				if($arResult["BOOKS_WITH_FILES"] || $arResult["BOOKS_WITHOUT_FILES"] || $arResult["BOOKS_WITH_READY_FILES"])
				{
					/**
					 * @var \CCacheManager $CACHE_MANAGER
					 */
					global $CACHE_MANAGER;
					$CACHE_MANAGER->StartTagCache($cache_dir);
					$CACHE_MANAGER->RegisterTag($cache_id);
					$CACHE_MANAGER->EndTagCache();
					$obCache->EndDataCache(compact("arResult"));
				}
				else
					$obCache->AbortDataCache();
			}
		}
		return $arResult;
	}
	public static function getReadingCnt()
	{
		return self::getReadCnt();
	}
	protected function getImageLinks($COVER_PAGE=1)
	{
		if(!empty($this->BOOK_ID))
		{
			$this->arLinks = array_merge($this->arLinks, self::GetBookUrls($this->BOOK_ID, $COVER_PAGE));
		}
	}
	public function GetLink($type="PREVIEW")
	{
		if(preg_match("@(DETAIL|NORMAL|PREVIEW|VIEWER|PAGE)@i", $type, $matches) && count($this->arLinks) > 0)
		{
			$type=strtoupper($matches[1]);
			$types = array(
				"DETAIL" => "IMAGE_URL_DETAIL",
				"NORMAL" => "IMAGE_URL_NORMAL",
				"PREVIEW" => "IMAGE_URL_PREVIEW",
				"VIEWER" => "DETAIL_PAGE_URL",
				"PAGE" => "VIEWER_URL",
			);
			return $this->arLinks[$types[$type]];
		}
		return null;
	}
	public function __construct($FILE_ID=0, $getArray=true, $BOOK_ID=0, $COVER_PAGE=1)
	{
		$COVER_PAGE = intval($COVER_PAGE) > 0 ? intval($COVER_PAGE) : 1;
		$this->tempDir = $_SERVER["DOCUMENT_ROOT"]."/upload/books_pages/temp/";
		$this->previewDir = $_SERVER["DOCUMENT_ROOT"]."/upload/books_pages/preview/";
		$this->detailDir = $_SERVER["DOCUMENT_ROOT"]."/upload/books_pages/detail/";
		$this->normalDir = $_SERVER["DOCUMENT_ROOT"]."/upload/books_pages/normal/";
		if(is_numeric($BOOK_ID) && $BOOK_ID > 0)
		{
			$this->BOOK_ID = intval($BOOK_ID);
			$this->getImageLinks($COVER_PAGE);
		}
		if(is_numeric($FILE_ID))
		{
			$this->FILE_ID = intval($FILE_ID);
		}
		$this->getRequestData();
		if($getArray)
		{
			if(is_numeric($this->FILE_ID))
			{
				$this->getFileArray();
			}
			elseif(is_array($FILE_ID))
			{
				$this->arFile = $FILE_ID;
				$this->getFileID();
			}
			else
			{
				return;
			}
			$this->getPdfFile();
			if($this->fileRecordExists())
			{
				$this->filesDir = $this->tempDir.$this->FILE_ID."/";
			}
		}
	}
	protected function getEntity_data_class($HIBLOCK = HIBLOCK_BOOKS_DATA_USERS)
	{
		$hlblock = HL\HighloadBlockTable::getById($HIBLOCK)->fetch();
		$entity = HL\HighloadBlockTable::compileEntity($hlblock);
		$entity_data_class = $entity->getDataClass();
		return $entity_data_class;
	}
	public static function add($objectId, $uid = false)
	{
		if(empty($objectId))
			return false;
		global $USER;
		if ( !$uid )
			$uid = $USER->GetID();

		$favoriteObject = self::getListCurrentUser($objectId, $uid);
		if($favoriteObject !== false)
			return $favoriteObject['ID'];

		$entity_data_class = self::getEntity_data_class();

		$dt = new DateTime();

		$arFields = array(
			'UF_DATE_ADD'		=> $dt,
			'UF_BOOK_ID'		=> $objectId,
			'UF_UID'			=> $uid,
		);

		$result = $entity_data_class::add($arFields);
		$saveID = $result->getId();
		return $saveID;
	}
	public static function isAvailable($book_id)
	{
		$arBook = self::GetByID($book_id);
		return is_array($arBook);
	}
	public static function deleteWithTest($id, $uid=false)
	{
		global $USER;
		if ( !$uid )
			$uid = $USER->GetID();
		$entity_data_class = self::getEntity_data_class();
		$toDelete = $entity_data_class::getList(array(
			"select" => array("ID"),
			"filter" => array(
				'ID' 		=> $id,
				'UF_UID' 	=> $uid,
			),
		))->Fetch();

		if ( !$toDelete )
		{
			//echo "No Data"; print_r($toDelete);
			return false;
		}

		$entity_data_class::Delete($toDelete['ID']);
		Collections::removeLinkObject($toDelete['ID']);
		return true;
	}
	protected function delete($objectId)
	{
		if(empty($objectId))
			return false;

		$arData = self::getListCurrentUser($objectId);
		if($arData === false)
			return false;

		$entity_data_class = self::getEntity_data_class();
		$entity_data_class::Delete($arData['ID']);
		Collections::removeLinkObject($arData['ID']);
	}
	public static function getListCurrentUser($objectId = false, $uid = false)
	{
		global $USER;
		if ( !$uid )
			$uid = $USER->GetID();

		$entity_data_class = self::getEntity_data_class();

		$arFilter = array('UF_UID' => $uid);
		if(!empty($objectId))
			$arFilter['=UF_BOOK_ID'] = $objectId;

		$rsData = $entity_data_class::getList(array(
			"select" => array("ID", "UF_BOOK_ID"),
			"filter" => $arFilter,
		));
		$arResult = array();
		if($rsData->getSelectedRowsCount() <= 0)
			return false;
		if(!empty($objectId) && !is_array($objectId))
		{
			$arResult = $rsData->Fetch();
		}
		else
		{
			while($arData = $rsData->Fetch())
				$arResult[$arData['UF_BOOK_ID']] = $arData['ID'];
		}
		return $arResult;
	}
	public function __call($name, $arguments)
	{
		if(strpos($name, "Get") === 0)
		{
			$var = substr($name, 3);
			if(isset($this->$var)) return $this->$var;
		}
		return null;
	}

	/**
	 * выводит страницу книги на просмотр JPG
	 * @param bool $return
	 * @param int $width
	 * @param int $COVER_NUMBER_PAGE
	 * @return bool|null|string
	 */
	public function ShowImage($return=false, $width=800, $COVER_NUMBER_PAGE=0)
	{
		$img = $this->getDataImage($return, $width, $COVER_NUMBER_PAGE);
		if(is_null($img))
		{
			$imageFile = file_get_contents($_SERVER["DOCUMENT_ROOT"]."/local/images/book_empty.jpg");
			$img = $this->printFile($imageFile, $return);
		}
		if($return)	return $img;
	}

	/**
	 * показать изображение с ресайзом JPG
	 * @param int $width
	 * @param int $height
	 */
	public function ShowImageResize($width=0, $height=0)
	{
		$imagePath = $this->getImagePath(1200);
		if(!is_null($imagePath))
		{
			$srcImg = imagecreatefromjpeg($imagePath);
			header('Content-Type: image/jpeg');
			$this->createResizeImage($srcImg, $width, $height);
		}
		else
		{
			$imagePath = $_SERVER["DOCUMENT_ROOT"]."/local/images/book_empty.jpg";
			$srcImg = imagecreatefromjpeg($imagePath);
			header('Content-Type: image/jpeg');
			$this->createResizeImage($srcImg, $width, $height);
		}
		exit;
	}
	public function GetImageSrc($width=800)
	{
		return $this->getImagePath($width);
	}
	public static function UpdateBookAccess($BOOK_ID, $FILE_ID=false, $COUNT_PAGES=1)
	{
		if(isset($_SESSION["BOOKS_ACCESS"]) && array_key_exists($BOOK_ID, $_SESSION["BOOKS_ACCESS"]))
		{
			if(is_numeric($FILE_ID) && intval($FILE_ID) > 0 && is_numeric($COUNT_PAGES) && intval($COUNT_PAGES) > 0)
			{
				$_SESSION["BOOKS_ACCESS"][$BOOK_ID] = array(
					"FILE_ID" => intval($FILE_ID),
					"COUNT_PAGES" => intval($COUNT_PAGES),
				);
			}
			else
			{
				$_SESSION["BOOKS_ACCESS"][$BOOK_ID] = false;
			}
		}
	}
	public static function DeleteBookAccess($BOOK_ID)
	{
		if(isset($_SESSION["BOOKS_ACCESS"]) && array_key_exists($BOOK_ID, $_SESSION["BOOKS_ACCESS"]))
		{
			unset($_SESSION["BOOKS_ACCESS"][$BOOK_ID]);
		}
	}
	protected static function getStaticData($BOOK_ID=0)
	{
		$BOOK_ID = intval($BOOK_ID);
		if($BOOK_ID <= 0)
			return false;
		$obCache = new \CPHPCache();
		$cache_id = "books_files_binding";
		$cache_time = 36000000;
		$cache_path = "/".SITE_ID."/neb/".$cache_id."/";
		$BOOK = null;
		if($obCache->InitCache($cache_time, $cache_id.$BOOK_ID, $cache_path))
		{
			extract($obCache->GetVars());
		}
		else
		{
			if($obCache->StartDataCache($cache_time, $cache_id.$BOOK_ID, $cache_path))
			{
				global $CACHE_MANAGER;
				$CACHE_MANAGER->StartTagCache($cache_path);
				if($BOOK = self::getStaticBookData($BOOK_ID))
				{
					$obCache->EndDataCache(compact(
						"BOOK"
					));
					$CACHE_MANAGER->RegisterTag($cache_id.$BOOK["ID"]);
					$CACHE_MANAGER->RegisterTag($cache_id);
					$CACHE_MANAGER->EndTagCache();
				}
				else
				{
					$obCache->AbortDataCache();
				}
			}
		}
		return $BOOK;
	}
	protected function getData($BOOK_ID=0)
	{
		$BOOK_ID = intval($BOOK_ID);
		if($BOOK_ID > 0)
			$this->BOOK_ID = $BOOK_ID;
		$obCache = new \CPHPCache();
		$cache_id = "books_files_binding";
		$cache_time = 36000000;
		$cache_path = "/".SITE_ID."/neb/".$cache_id."/";
		if($obCache->InitCache($cache_time, $cache_id.$this->BOOK_ID, $cache_path))
		{
			extract($obCache->GetVars());
		}
		else
		{
			if($obCache->StartDataCache($cache_time, $cache_id.$this->BOOK_ID, $cache_path))
			{
				global $CACHE_MANAGER;
				$CACHE_MANAGER->StartTagCache($cache_path);
				if($BOOK = $this->getBookData())
				{
					$obCache->EndDataCache(compact(
						"BOOK"
					));
					$CACHE_MANAGER->RegisterTag($cache_id.$BOOK["ID"]);
					$CACHE_MANAGER->RegisterTag($cache_id);
					$CACHE_MANAGER->EndTagCache();
				}
				else
				{
					$obCache->AbortDataCache();
				}
			}
		}
		if(isset($BOOK))
		{
			$this->FILE_ID = intval($BOOK["PROPERTY_FILE_VALUE"]);
			$this->COUNT_PAGES = ((intval($BOOK["PROPERTY_COUNT_PAGES_VALUE"])>0) ? intval($BOOK["PROPERTY_COUNT_PAGES_VALUE"]) : 1);
			$this->COVER_NUMBER_PAGE = (!empty($BOOK["PROPERTY_COVER_NUMBER_PAGE"])) ? intval($BOOK["PROPERTY_COVER_NUMBER_PAGE"]) : 1;
		}
	}
	protected function getDataImage($return=false, $width=800, $COVER_NUMBER_PAGE=0)
	{
		$this->getData();
		if($this->PAGE_ID > 0 && $this->PAGE_ID <= $this->COUNT_PAGES)
		{
			$img = $this->getImageFile($return, $width, $COVER_NUMBER_PAGE);
			if($return)
			{
				return $img;
			}
		}
		return null;
	}
	public static function GetByID($book_id, $arSelect=array())
	{
		if(intval($book_id) > 0)
		{
			$arFilter = array_merge(array("=ID" => intval($book_id)));
			$arBook = self::GetListAccess($arFilter, array(), $arSelect);
			if(count($arBook)>0)
				return current($arBook);
		}
		return null;

	}
	public static function GetListAccess($arFilter=array(), $arSort=array(), $arSelect=array(), $arNavParams=array())
	{
		$arBooks = array();
		$arDefaultSelect = array(
			"ID",
			"NAME",
			"IBLOCK_ID",
			"PROPERTY_FILE",
			"PROPERTY_AUTHOR",
			"PROPERTY_COUNT_PAGES",
			"PROPERTY_PUBLISH_YEAR",
		);
		if(is_array($arSelect))
			$arDefaultSelect = array_merge($arDefaultSelect, $arSelect);
		$arDefaultFilter = array
		(
			"IBLOCK_ID" => IBLOCK_ID_BOOKS,
			"IBLLCK_TYPE" => "library",
		);
		if(is_array($arFilter))
			$arDefaultFilter = array_merge($arDefaultFilter, $arFilter);
		if(!is_array($arSort))
			$arSort = array();
		if(!is_array($arNavParams))
			$arNavParams = array();
		if($tsBook = \CIBlockElement::GetList($arSort, $arDefaultFilter, false, $arNavParams, $arDefaultSelect))
		{
			while($arBook = $tsBook->Fetch())
			{
				$arBook["title"] = $arBook["NAME"];
				$arBook["id"] = $arBook["~id"] = $arBook["ID"];
				$arBook["DETAIL_PAGE_URL"] = sprintf("/catalog/%d/", $arBook["ID"]);
				if(!empty($arBook["PROPERTY_AUTHOR_VALUE"]))
				{
					$arBook["authorbook"] = $arBook["PROPERTY_AUTHOR_VALUE"];
					$arBook["r_authorbook"] = SearchQuery::GetSearchParam(array("AUTHOR" => $arBook["authorbook"]));
					$arBooks[$arBook["ID"]]["SEARCH_AUTHOR_URL"] = $arBook["r_authorbook"];
				}
				if(!empty($arBook["PROPERTY_FILE_VALUE"]))
				{
					$arBookImg = self::GetBookUrls($arBook["ID"]);
					$arBook = array_merge($arBook, $arBookImg);
				}
				$arBooks[$arBook["ID"]] = $arBook;
			}
		}
		return $arBooks;
	}

	/**
	 * @param $book_id
	 * @return array
	 */
	public static function GetBookUrls($book_id, $COVER_PAGE=1)
	{
		$arBooks = array();
		if($BOOK = self::getStaticData($book_id))
		{
			$arBooks["IMAGE_URL_DETAIL"] = sprintf("/local/tools/evk/getbooksresize.php?book_id=%d&width=%d&p=%d", $book_id, 1200, $COVER_PAGE);
			$arBooks["IMAGE_URL_NORMAL"] = sprintf("/local/tools/evk/getbooksresize.php?book_id=%d&width=%d&p=%d", $book_id, 800, $COVER_PAGE);
			$arBooks["IMAGE_URL_PREVIEW"] = $arBooks["IMAGE_URL"] = sprintf("/local/tools/evk/getbooksresize.php?book_id=%d&width=%d&p=%d", $book_id, 162, $COVER_PAGE);
			$arBooks["IMAGE_URL_RESIZE"] = sprintf("/local/tools/evk/getbooksresize.php?book_id=%d&p=%d", $book_id, $COVER_PAGE);
			$arBooks["VIEWER_URL"] = self::GetBookViewerUrl($book_id);
			return $arBooks;
		}
	}
	//public static function
	public static function GetBookViewerUrl($book_id)
	{
		return sprintf("/catalog/%d/viewer/", $book_id);
	}
	public static function GetBookDetailUrl($book_id)
	{
		return sprintf("/catalog/%d/", $book_id);
	}
	protected static function getLibraryIDFromUser()
	{
		$user = new nebUser();
		if($user->isLibrary())
		{
			$lib = $user->getLibrary();
			return $lib["ID"];
		}
		return 0;
	}
	public static function DownloadPDFByFileID($ID, $BOOK_ID=0, $arLibs=array())
	{
		$ID = intval($ID);
		echo $ID."_";
		$BOOK_ID = intval($BOOK_ID);
		if($ID > 0)
		{
			if($BOOK_ID <= 0)
			{
				$arBook = self::GetListAccess(array("=PROPERTY_FILE"=>$ID), array(), array("PROPERTY_LIBRARIES"));
				$arBook = current($arBook);
				$arLibs = $arBook["PROPERTY_LIBRARIES_VALUE"];
				$BOOK_ID = intval($arBook["ID"]);
			}
			$obFile = new \CFile();
			if($rsFile = $obFile->GetByID($ID))
			{
				if($arFile = $rsFile->Fetch())
				{
					if(preg_match("@\.pdf$@", $arFile["FILE_NAME"]))
					{
						$checkLib = true;
						$libID = intval(self::getLibraryIDFromUser());
						if($libID > 0)
							if(is_array($arLibs) && count($arLibs) && in_array($libID, $arLibs))
								$checkLib = false;
						if($checkLib)
							BookDownload::set($BOOK_ID);

						$filePDF = $_SERVER["DOCUMENT_ROOT"].$obFile->GetFileSRC($arFile);
						if(file_exists($filePDF))
						{
							$GLOBALS["APPLICATION"]->RestartBuffer();
							header('Content-Description: File Transfer');
							header('Content-Type: application/octet-stream');
							header('Content-Transfer-Encoding: binary');
							header('Content-Disposition: attachment; filename=' . basename($filePDF));
							header('Expires: 0');
							header('Cache-Control: must-revalidate');
							header('Pragma: public');
							header('Content-Length: ' . filesize($filePDF));
							readfile($filePDF);
							exit;
						}
					}
				}
			}
		}
	}
	public static function GetDownloadPDFLinkByFileID($ID, $P=false)
	{
		$ID = intval($ID);
		if($ID > 0)
		{
			$PARAM = sprintf("FILE_ID=%d", $ID);
			if($P)
				return $PARAM;
			return sprintf("/local/tools/evk/getbookpdf.php?%s", $PARAM);
		}
		return null;
	}
	public static function GetDownloadPDFLinkByBookID($ID, $P=false)
	{
		$ID = intval($ID);
		if($ID > 0)
		{
			$PARAM = sprintf("BOOK_ID=%d", $ID);
			if($P)
				return $PARAM;
			return sprintf("/local/tools/evk/getbookpdf.php?%s", $PARAM);
		}
		return null;
	}
	public static function DownloadPDFByBookID($ID)
	{
		$ID = intval($ID);
		if($ID > 0 && $arBook = self::GetByID($ID, array("PROPERTY_LIBRARIES")))
			self::DownloadPDFByFileID($arBook["PROPERTY_FILE_VALUE"], $ID, $arBook["PROPERTY_LIBRARIES_VALUE"]);
	}
	protected static function getStaticBookData($BOOK_ID=0)
	{
		if($BOOK_ID <= 0)
			return null;
		$arFilter = array(
			"IBLOCK_TYPE" => "library",
			"IBLOCK_ID" => IBLOCK_ID_BOOKS,
			"ID" => $BOOK_ID,
			"PROPERTY_STATUS" => 2,
		);
		$rsBook = \CIBlockElement::GetList(array(), $arFilter, false, false, array("ID", "IBLOCK_ID", "PROPERTY_FILE", "PROPERTY_STATUS", "PROPERTY_COUNT_PAGES", "PROPERTY_COVER_NUMBER_PAGE"));
		if($arBook = $rsBook->Fetch())
		{
			return $arBook;
		}
		return null;
	}
	protected function getBookData()
	{
		$arFilter = array(
			"IBLOCK_TYPE" => "library",
			"IBLOCK_ID" => IBLOCK_ID_BOOKS,
			"ID" => $this->BOOK_ID,
			"PROPERTY_STATUS" => 2,
		);
		$rsBook = \CIBlockElement::GetList(array(), $arFilter, false, false, array("ID", "IBLOCK_ID", "PROPERTY_FILE", "PROPERTY_STATUS", "PROPERTY_COUNT_PAGES", "PROPERTY_COVER_NUMBER_PAGE"));
		if($arBook = $rsBook->Fetch())
		{
			return $arBook;
		}
		return null;
	}
	protected function getRequestData()
	{
		if(isset($_REQUEST["book_id"]))
			$this->BOOK_ID = intval($_REQUEST["book_id"]);
		if(isset($_REQUEST["file_id"]))
			$this->FILE_ID = intval($_REQUEST["file_id"]);
		if(isset($_REQUEST["p"]))
			$this->PAGE_ID = intval($_REQUEST["p"]);
		if(isset($_REQUEST["width"]))
			$this->WIDTH = intval($_REQUEST["width"]);
	}
	public function createFilesForViewer()
	{
		if($this->createJpegFiles())
		{
			return $this->convertJpegFiles2JpgFiles();
		}
		return false;
	}
	public function createJpegFiles()
	{
		$pdfToPPMPath = '/usr/local/poppler/bin/pdftoppm';
		$filePath = escapeshellarg($this->arFile["FILE_PATH"]);
		$this->createDirIfNotExists($this->filesDir);
		$fileDestination = escapeshellarg($this->filesDir);
		$cmd = escapeshellcmd(sprintf('%s -scale-to-x %d -scale-to-y -1 -jpeg %s %s', $pdfToPPMPath, $this->detailWidth, $filePath, $fileDestination));
		$ret = shell_exec($cmd);
		return true;
	}
	public function createPngFiles()
	{
		$pdfToPPMPath = '/usr/bin/pdftoppm';
		$filePath = escapeshellarg($this->arFile["FILE_PATH"]);
		$this->createDirIfNotExists($this->filesDir);
		$fileDestination = escapeshellarg($this->filesDir);
		$cmd = escapeshellcmd(sprintf('%s -scale-to-x %d -png %s %s', $pdfToPPMPath, $this->detailWidth, $filePath, $fileDestination));
		$ret = shell_exec($cmd);
		/*
		Log::createPngFiles(__FILE__,__LINE__, 'shell_exec', $ret);
		if(is_null($ret))
			Log::createPngFiles(__FILE__,__LINE__,"Ошибка создания PNG-файлов из PDF-файла");
		else
			Log::createPngFiles(__FILE__,__LINE__,"Status: ".$ret);
		*/
		return true;
	}
	protected function getImagePath($width=800, $COVER_NUMBER_PAGE=0)
	{
		$this->getData();
		if($this->FILE_ID > 0)
		{
			$width = intval($width);
			switch($width)
			{
				case 1200:
				{
					$subDir = $this->detailDir;
					break;
				}
				case 800:
				{
					$subDir = $this->normalDir;
					break;
				}
				default:
				{
					$subDir = $this->previewDir;
				}
			}
			$page_id = (intval($COVER_NUMBER_PAGE) > 0) ? intval($COVER_NUMBER_PAGE) : (intval($this->PAGE_ID) > 0) ? intval($this->PAGE_ID): 1;
			$FILE_SRC_IMAGE = sprintf("%s%d/%d.jpg", $subDir, $this->FILE_ID, $page_id);
			return (file_exists($FILE_SRC_IMAGE)) ? $FILE_SRC_IMAGE : null;
		}
		else
		{
			return null;
		}
	}
	protected function getImageFile($return=false, $width=800, $COVER_NUMBER_PAGE=0)
	{
		$width = intval($width);
		if(!$return || ($return && $width <= 0))
		{
			$width = $this->WIDTH;
		}
		$imageFile = $this->getImagePath($width, $COVER_NUMBER_PAGE);
		if(!file_exists($imageFile))
		{
			$imageFile = $_SERVER["DOCUMENT_ROOT"]."/local/images/book_empty.jpg";
		}
		$img = $this->printFile($imageFile, $return);
		if($return)
		{
			return $img;
		}
	}
	protected function createDirIfNotExists($dir)
	{
		if(!file_exists($dir))
			mkdir($dir, 0755);
	}
	protected function getJpegFiles()
	{
		if(file_exists($this->filesDir) && is_dir($this->filesDir))
		{
			foreach(scandir($this->filesDir) as $file)
			{
				if($file == "." || $file == ".." || !preg_match("@(\d+)\.jpg@", $file, $m)) continue;
				$this->jpegFiles[intval($m[1])] = $this->filesDir.$file;
			}
			return (bool)count($this->jpegFiles);
		}
		return false;
	}
	protected function getPngFiles()
	{
		if(file_exists($this->filesDir) && is_dir($this->filesDir))
		{
			foreach(scandir($this->filesDir) as $file)
			{
				if($file == "." || $file == ".." || !preg_match("@(\d+)\.png$@", $file, $m)) continue;
				$this->pngFiles[intval($m[1])] = $this->filesDir.$file;
			}
			return (bool)count($this->pngFiles);
		}
		return false;
	}
	public function convertJpegFiles2JpgFiles()
	{
		if($this->getJpegFiles())
		{
			foreach($this->jpegFiles as $key=>$file)
			{
				if(file_exists($file))
				{
					$this->convertJpeg2Jpg($key, $file);
				}
			}
			if(is_dir($this->filesDir) && count(scandir($this->filesDir)) < 3)
			{
				if(!rmdir($this->filesDir))
					Log::convertJpegFiles2JpgFiles(__FILE__,__LINE__,sprintf("Не удалось удалить каталог: %s", $this->filesDir));
			}
			return true;
		}
		else
		{
			Log::convertJpegFiles2JpgFiles(__FILE__,__LINE__,sprintf("JPEG файлы не найдены: %s", $this->filesDir));
		}
		return false;
	}
	public function convertPngFiles2JpgFiles()
	{
		if($this->getPngFiles())
		{
			foreach($this->pngFiles as $key=>$file)
			{
				if(file_exists($file))
				{
					$this->convertPng2Jpg($key, $file);
				}
			}
			if(is_dir($this->filesDir) && count(scandir($this->filesDir)) < 3)
			{
				if(!rmdir($this->filesDir))
					Log::convertPngFiles2JpgFiles(__FILE__,__LINE__,sprintf("Не удалось удалить каталог: %s", $this->filesDir));
			}
			return true;
		}
		else
		{
			Log::convertPngFiles2JpgFiles(__FILE__,__LINE__,sprintf("PNG файлы не найдены: %s", $this->filesDir));
		}
		return false;
	}
	protected function copyJpegFileToDetail($key, $file, &$newFile=null)
	{
		$imageDir = $this->detailDir."/".$this->FILE_ID."/";
		$this->createDirIfNotExists($imageDir);
		$newFile = $imageDir.$key.".jpg";
		return rename($file, $newFile);
	}
	protected function convertJpeg2Jpg($key, $file)
	{
		if($this->copyJpegFileToDetail($key, $file, $newFile))
		{
			if(file_exists($newFile) && $image = imagecreatefromjpeg($newFile))
			{
				if($this->createThumbnail($image, $key) && $this->createNormal($image, $key))
				{
					return true;
				}
			}
			else
			{
				Log::imagecreatefromjpeg(__FILE__,__LINE__, "Ошибка");
			}
			return false;
		}
	}
	protected function convertPng2Jpg($key, $file)
	{
		if($image = imagecreatefrompng($file))
		{
			if($this->createDetail($image, $key) && $this->createThumbnail($image, $key) && $this->createNormal($image, $key))
			{
				if(!unlink($file))
					Log::convertPng2Jpg(__FILE__,__LINE__,sprintf("Не удалось удалить файл: %s", $file));
				return true;
			}
		}
		else
		{
			Log::imagecreatefrompng(__FILE__,__LINE__, "Ошибка");
		}
		return false;
	}
	protected function getFileID()
	{
		if(isset($this->arFile["ID"]) && is_numeric($this->arFile["ID"]))
			$this->FILE_ID = intval($this->arFile["ID"]);
	}
	protected function getPdfFile()
	{
		if($this->fileRecordExists())
		{
			$this->arFile["FILE_PATH"] = join(DIRECTORY_SEPARATOR, array($_SERVER["DOCUMENT_ROOT"], "upload", $this->arFile["SUBDIR"], $this->arFile["FILE_NAME"]));
			if(!file_exists($this->arFile["FILE_PATH"]))
			{
				$this->arFile = array();
			}
		}
		return $this->arFile;
	}
	protected function printFile($imageFile, $return = false)
	{
		$imgMime = getImageSize($imageFile);
		$fileContents = file_get_contents($imageFile);
		if($return)
		{
			return $fileContents;
		}
		else
		{
			$GLOBALS["APPLICATION"]->RestartBuffer();
			header(sprintf('Content-Type: %s;charset=utf-8', $imgMime["mime"]));
			echo $fileContents;
			exit;
		}
	}
	protected function fileRecordExists()
	{
		return count($this->arFile) > 0 && $this->issetKeys(array("ID", "SUBDIR", "FILE_NAME"), $this->arFile);
	}
	protected function issetKeys($keys, $array)
	{
		$check = true;
		if(is_array($keys))
		{
			foreach($keys as $key)
			{
				$check = ($check) ? (array_key_exists($key, $array) && is_string($array[$key]) && (strlen($array[$key]) > 0)) : false;
			}
		}
		else
		{
			$check = array_key_exists($keys, $array) && is_string($array[$keys]) && (strlen($array[$keys]) > 0);
		}
		return $check;
	}
	protected function getFileArray()
	{
		if($rsFile = \CFile::GetByID($this->FILE_ID))
		{
			if($arFile = $rsFile->Fetch())
			{
				$this->arFile = $arFile;
			}
		}
		return $this->arFile;
	}
	public static function pe()
	{
		global $APPLICATION;
		$APPLICATION->RestartBuffer();
		call_user_func_array(array(self, "p"), func_get_args());
		exit;
	}
	public static function p()
	{
		$args = func_get_args();
		$str = '';
		foreach($args as $arg)
		{
			if(is_bool($arg))
			{
				$str .= '<pre>' . (($arg)?"true":"false") . '</pre>';
			}
			elseif(is_null($arg))
			{
				$str .= '<pre>null</pre>';
			}
			elseif(is_string($arg))
			{
				$str .= '<pre>'. ((strlen($arg) <= 0) ? '<div style="color:red;">empty string</div>':$arg) .'</pre>';
			}
			elseif(is_array($arg) && count($arg) <= 0)
			{
				$str .= '<pre style="">Array{} Пустой массив</pre>';
			}
			else
			{
				$str .= '<pre>' . print_r($arg, 1) . '</pre>';
			}
		}
		echo $str;
	}
	public static function ps()
	{
		$args = func_get_args();
		$str = '';
		foreach($args as $arg)
		{
			if(is_bool($arg))
			{
				$str .= '<pre>' . (($arg)?"true":"false") . '</pre>';
			}
			elseif(is_null($arg))
			{
				$str .= '<pre>null</pre>';
			}
			elseif(is_string($arg))
			{
				$str .= '<pre>'. ((strlen($arg) <= 0) ? '<div style="color:red;">empty string</div>':htmlspecialchars($arg)) .'</pre>';
			}
			elseif(is_array($arg) && count($arg) <= 0)
			{
				$str .= '<pre style="">Array{} Пустой массив</pre>';
			}
			else
			{
				self::htmlSpecialCharsArray($arg);
				$str .= '<pre>' . print_r($arg, 1) . '</pre>';
			}
		}
		echo $str;
	}
	protected static function htmlSpecialCharsArray(&$array)
	{
		foreach($array as &$arr)
		{
			if(is_array($arr))
				self::htmlSpecialCharsArray($arr);
			elseif(is_string($arr))
				$arr = htmlspecialchars($arr);
		}
	}
	protected function createResizeImage(&$srcImg, $width=0, $height=0)
	{
		if($width > 1200) $width = 1200;
		if($height > 1200) $height = 1200;
		$origWidth = imagesx($srcImg);
		$origHeight = imagesy($srcImg);
		if($origWidth/$origHeight > 1)
			$this->landScope = "A";
		$ratioW = $ratioH = 1;
		if($width && $height)
		{
			$ratioW = $origWidth / $width;
			$ratioH = $origHeight / $height;
		}
		elseif($width)
		{
			$ratioW = $ratioH = $origWidth / $width;
		}
		elseif($height)
		{
			$ratioW = $ratioH = $origHeight / $height;
		}
		$normalHeight = $origHeight / $ratioH;
		$normalWidth = $origWidth / $ratioW;
		$normalImg = imagecreatetruecolor($normalWidth, $normalHeight);
		imagecopyresized($normalImg, $srcImg, 0, 0, 0, 0, $normalWidth, $normalHeight, $origWidth, $origHeight);
		imagejpeg($normalImg);
		imagedestroy($normalImg);
	}
	protected function createDetail(&$srcImg, $key)
	{
		$imageDir = $this->detailDir."/".$this->FILE_ID."/";
		$this->createDirIfNotExists($imageDir);
		return imagejpeg($srcImg, $imageDir.$key.".jpg", $this->imageQuality);
	}
	protected function createNormal(&$srcImg, $key)
	{
		$origWidth = imagesx($srcImg);
		$origHeight = imagesy($srcImg);
		if($origWidth/$origHeight > 1)
			$this->landScope = "A";
		$ratio = $origWidth / $this->normalWidth;
		$normalHeight = $origHeight / $ratio;
		$normalImg = imagecreatetruecolor($this->normalWidth, $normalHeight);
		imagecopyresized($normalImg, $srcImg, 0, 0, 0, 0, $this->normalWidth, $normalHeight, $origWidth, $origHeight);
		$imageDir = $this->normalDir."/".$this->FILE_ID."/";
		$this->createDirIfNotExists($imageDir);
		$return = imagejpeg($normalImg, $imageDir.$key.".jpg", $this->imageQuality);
		imagedestroy($normalImg);
		return $return;
	}
	protected function createThumbnail(&$srcImg, $key)
	{
		$origWidth = imagesx($srcImg);
		$origHeight = imagesy($srcImg);
		if($origWidth/$origHeight > 1)
			$this->landScope = "A";
		$ratio = $origWidth / $this->previewWidth;
		$thumbHeight = $origHeight / $ratio;
		$thumbImg = imagecreatetruecolor($this->previewWidth, $thumbHeight);
		imagecopyresized($thumbImg, $srcImg, 0, 0, 0, 0, $this->previewWidth, $thumbHeight, $origWidth, $origHeight);
		$imageDir = $this->previewDir."/".$this->FILE_ID."/";
		$this->createDirIfNotExists($imageDir);
		$return = imagejpeg($thumbImg, $imageDir.$key.".jpg", $this->imageQuality);
		imagedestroy($thumbImg);
		return $return;
	}
	public function CropImageForQuote($width, $citeTop, $citeLeft, $citeWidth, $citeHeight)
	{
		$srcImg = $this->getImagePath(800);
		$srcImg = imagecreatefromjpeg($srcImg);
		$origWidth = imagesx($srcImg);
		$ratio = $width / $origWidth;
		$citeTop2 = $citeTop / $ratio;
		$citeLeft2 = $citeLeft / $ratio;
		$citeHeight2 = $citeHeight / $ratio;
		$citeWidth2 = $citeWidth / $ratio;

		$thumbImg = imagecreatetruecolor($citeWidth, $citeHeight);
		if(imagecopyresized($thumbImg, $srcImg, 0, 0, $citeLeft2, $citeTop2, $citeWidth, $citeHeight, $citeWidth2, $citeHeight2))
		{
			$imageDir = $this->tempDir."quote/";
			$this->createDirIfNotExists($imageDir);
			global $USER;
			$filename = sprintf("%s%d_%d.jpg", $imageDir, $this->FILE_ID, $USER->GetID());
			if(imagejpeg($thumbImg, $filename, $this->imageQuality))
			{
				imagedestroy($thumbImg);
				return $filename;
			}
		}
		imagedestroy($thumbImg);
		return null;
	}
}