<?php
namespace Evk\Books;
\Bitrix\Main\Loader::includeModule("iblock");
\Bitrix\Main\Loader::includeModule("search");
class SearchSphinx
{
	protected $s_strict = false;
	protected $substring = false;
	protected $CACHE_TIME = 3600000000;
	public function SendQueryToSphinx($arFilter, $aSort=array(), $s_strict=false, $substring=false)
	{
		$arFilter = array_merge($arFilter, array(
			"MODULE_ID" => "iblock",
			"PARAM1" => "library",
			"PARAM2" => IBLOCK_ID_BOOKS,
			"CHECK_DATES" => "N",
		));
		$this->substring = $substring;
		$this->s_strict = $s_strict;
		$cache_path = "/".SITE_ID."/neb/search.page/";
		$obCache = new \CPHPCache();
		$arReturn = array();
		$cache_id = md5(serialize(array_merge($arFilter, array($this->s_strict, $this->substring))));
		if($obCache->InitCache($this->CACHE_TIME, $cache_id, $cache_path))
		{
			$vars = $obCache->GetVars();
			return $vars["arReturn"];
		}
		else
		{
			if($obCache->StartDataCache($this->CACHE_TIME, $cache_id, $cache_path))
			{
				$obSearch = new CSearch();

				$obSearch->SetOptions(array(
					"ERROR_ON_EMPTY_STEM" => true,
					"NO_WORD_LOGIC" => true,
				));
				$obSearch->Search($arFilter, $aSort, array(), false, $this->s_strict, $this->substring);
				$arReturn["ERROR_CODE"] = $obSearch->errorno;
				$arReturn["ERROR_TEXT"] = $obSearch->error;
				if($obSearch->errorno==0)
				{
					global $CACHE_MANAGER;
					$CACHE_MANAGER->StartTagCache($cache_path);
					$arReturn["ITEMS"] = array();
					$arReturn["COUNT"] = $obSearch->nSelectedCount;
					while($ar = $obSearch->GetNext())
					{
						$arReturn["ITEMS"][$ar["ITEM_ID"]] = array(
							"ID" => $ar["ID"],
							"ITEM_ID" => $ar["ITEM_ID"],
							"TITLE" => $ar["~TITLE_FORMATED"],
							"TAGS" => $ar["~TAGS_FORMATED"],
							"BODY" => $ar["~BODY_FORMATED"],
							"URL" => htmlspecialcharsbx($ar["URL"]),
							"GETMORF" => "Y",
						);
					}
					$CACHE_MANAGER->RegisterTag("iblock_id_library_search_page");
					$CACHE_MANAGER->EndTagCache();
					$obCache->EndDataCache(array(
						"arReturn" => $arReturn,
					));
				}
				else
				{
					$obCache->AbortDataCache();
				}
				return $arReturn;
			}
		}
	}
}