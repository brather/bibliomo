<?php
namespace Evk\Books\Tables;
use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

class NebLibsCityTable extends Entity\DataManager
{
	public static function getFilePath()
	{
		return __FILE__;
	}

	public static function getTableName()
	{
		return 'neb_libs_city';
	}

	public static function getMap()
	{
		return array(
            new Entity\IntegerField('ID', array(
                'primary' => true,
                'autocomplete' => true
            )),
            new Entity\IntegerField('UF_LIB_AREA'),
            new Entity\ReferenceField(
                'LIB_AREA',
                'Nota\Exalead\NebLibsAreaTable',
                array('=this.UF_LIB_AREA' => 'ref.ID')
            ),
            new Entity\StringField('UF_CITY_NAME'),
            new Entity\IntegerField('UF_IN_FAVORITES'),
            new Entity\IntegerField('UF_IN_LIST'),
            new Entity\StringField('UF_POS'),
            new Entity\IntegerField('UF_SORT'),
		);
	}
}
?>