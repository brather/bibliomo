<?
namespace Evk\Books\Tables;

use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

/**
 * Class StatBookViewTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> BOOK_ID string(255) mandatory
 * <li> UID int optional
 * <li> UID_GUEST string(50) mandatory
 * <li> X_TIMESTAMP datetime mandatory default 'CURRENT_TIMESTAMP'
 * </ul>
 *
 * @package Bitrix\Stat
 **/

class StatBookViewTable extends Entity\DataManager
{
	public static function getFilePath()
	{
		return __FILE__;
	}

	public static function getTableName()
	{
		return 'neb_stat_book_view';
	}

	public static function getMap()
	{
		return array(
			'ID' => array(
				'data_type' => 'integer',
				'primary' => true,
				'autocomplete' => true,
				'title' => Loc::getMessage('BOOK_VIEW_ENTITY_ID_FIELD'),
			),
			'BOOK_ID' => array(
				'data_type' => 'string',
				'required' => true,
				'validation' => array(__CLASS__, 'validateBookId'),
				'title' => Loc::getMessage('BOOK_VIEW_ENTITY_BOOK_ID_FIELD'),
			),
			'LIB_ID' => array(
				'data_type' => 'integer',
				'title' => Loc::getMessage('BOOK_VIEW_ENTITY_LIB_ID_FIELD'),
			),
			'UID' => array(
				'data_type' => 'integer',
				'title' => Loc::getMessage('BOOK_VIEW_ENTITY_UID_FIELD'),
			),
			'UID_GUEST' => array(
				'data_type' => 'string',
				'required' => true,
				'validation' => array(__CLASS__, 'validateUidGuest'),
				'title' => Loc::getMessage('BOOK_VIEW_ENTITY_UID_GUEST_FIELD'),
			),
			'X_TIMESTAMP' => array(
				'data_type' => 'datetime',
				'title' => Loc::getMessage('BOOK_VIEW_ENTITY_X_TIMESTAMP_FIELD'),
			),
		);
	}
	public static function validateBookId()
	{
		return array(
			new Entity\Validator\Length(null, 255),
		);
	}
	public static function validateUidGuest()
	{
		return array(
			new Entity\Validator\Length(null, 50),
		);
	}
}
?>