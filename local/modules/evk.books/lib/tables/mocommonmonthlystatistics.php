<?php
/**
 * User: agolodkov
 * Date: 04.08.2016
 * Time: 12:54
 */

namespace Evk\Books\Tables;


use Bitrix\Main\Entity\DataManager;

class MoCommonMonthlyStatisticsTable extends DataManager
{
    public static function getTableName()
    {
        return 'mo_common_monthly_statistics';
    }

    public static function getMap()
    {
        return [
            'id'                     => [
                'data_type'    => 'integer',
                'primary'      => true,
                'autocomplete' => true,
                'title'        => 'Id',
            ],
            'date'                   => [
                'data_type' => 'date',
                'required'  => true,
            ],
            'count_visitors'         => [
                'data_type' => 'integer',
                'required'  => true,
            ],
            'count_collections'      => [
                'data_type' => 'integer',
                'required'  => true,
            ],
            'count_ordered_books'    => [
                'data_type' => 'integer',
                'required'  => true,
            ],
            'count_issued_books'     => [
                'data_type' => 'integer',
                'required'  => true,
            ],
            'count_library_hit'      => [
                'data_type' => 'integer',
                'required'  => true,
            ],
            'count_collection_hit'   => [
                'data_type' => 'integer',
                'required'  => true,
            ],
            'count_viewer_hit'       => [
                'data_type' => 'integer',
                'required'  => true,
            ],
            'count_library_news_hit' => [
                'data_type' => 'integer',
                'required'  => true,
            ],
        ];
    }
}