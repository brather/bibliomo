<?php
/**
 * User: agolodkov
 * Date: 04.08.2016
 * Time: 10:52
 */

namespace Evk\Books\Tables;


use Bitrix\Main\Entity\DataManager;

class MoStatsEventTable extends DataManager
{
    public static function getTableName()
    {
        return 'mo_stats_event';
    }

    public static function getMap()
    {
        return [
            'id'        => [
                'data_type'    => 'integer',
                'primary'      => true,
                'autocomplete' => true,
                'title'        => 'Id',
            ],
            'event'     => [
                'data_type' => 'string',
                'required'  => true,
                'title'     => 'Event',
            ],
            'timestamp' => [
                'data_type' => 'datetime',
                'title'     => 'Timestamp',
            ],
        ];
    }

    public static function trackEvent($event)
    {
        return static::add(['event' => $event])->getId();
    }
}