<?php
namespace Evk\Books\Tables;

use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

/**
 * Class BiblioCardTable
 *
 * Fields:
 * <ul>
 * <li> Id int mandatory
 * <li> ALIS int mandatory
 * <li> IdFromALIS string(64) mandatory
 * <li> Author string(500) optional
 * <li> Name string optional
 * <li> SubName string optional
 * <li> PublishYear string(45) optional
 * <li> PublishPlace string(500) optional
 * <li> Publisher string(500) optional
 * <li> ISBN string(50) optional
 * <li> Language int optional
 * <li> CountPages string(150) optional
 * <li> Printing string(45) optional
 * <li> Format string(45) optional
 * <li> Series string(450) optional
 * <li> VolumeNumber string(45) optional
 * <li> VolumeName string(450) optional
 * <li> Collection string(100) optional
 * <li> AccessType int optional default 15
 * <li> isLicenseAgreement unknown optional default 'b'0''
 * <li> Responsibility string(500) optional
 * <li> Annotation string optional
 * <li> ContentRemark string optional
 * <li> CommonRemark string optional
 * <li> PublicationInformation string(255) optional
 * <li> pdfLink string(255) optional
 * <li> IdParent int optional
 * <li> CreationMethod int optional
 * <li> CreationActor int optional
 * <li> CreationDateTime datetime optional default '0000-00-00 00:00:00'
 * <li> UpdatingActor int optional
 * <li> UpdatingDateTime datetime optional default '0000-00-00 00:00:00'
 * <li> BBKText string(100) optional
 * <li> UDKText string(100) optional
 * <li> LanguageText string(32) optional
 * <li> UDKThematicsCodeSequences string optional
 * <li> IdRubrics string(500) optional
 * <li> Indexes string(500) optional
 * <li> BX_TIMESTAMP datetime mandatory default 'CURRENT_TIMESTAMP'
 * <li> BX_IS_UPLOAD int optional
 * <li> BX_ALIS int mandatory
 * <li> BX_UID int mandatory
 * <li> BX_LIB_ID int optional
 * <li> BX_DELETE int optional
 * </ul>
 *
 * @package Nota\Exalead
 **/

class BiblioCardTable extends Entity\DataManager
{
	public static function getFilePath()
	{
		return __FILE__;
	}

	public static function getTableName()
	{
		return 'tbl_common_biblio_card';
	}

	public static function getStatMap(){
		return Array('Name', 'Author', 'BBKText');
	}

	public static function getMap()
	{
		return array(
			'Id' => array(
				'data_type' => 'integer',
				'primary' => true,
				'autocomplete' => true,
				'title' => Loc::getMessage('BIBLIO_CARD_ENTITY_ID_FIELD'),
			),
			'ALIS' => array(
				'data_type' => 'integer',
				'required' => true,
				'title' => Loc::getMessage('BIBLIO_CARD_ENTITY_ALIS_FIELD'),
			),
			new Entity\ReferenceField(
				'LIB',
				'Nota\Exalead\BiblioAlisTable',
				array('=this.ALIS' => 'ref.Id')
			),
			new Entity\ReferenceField(
				'READ',
				'Nota\Exalead\Stat\StatBookRead',
				array('=this.IdFromALIS' => 'ref.BOOK_ID')
			),
			new Entity\ReferenceField(
				'VIEW',
				'Nota\Exalead\Stat\StatBookView',
				array('=this.IdFromALIS' => 'ref.BOOK_ID')
			),
			'IdFromALIS' => array(
				'data_type' => 'string',
				'required' => true,
				'validation' => array(__CLASS__, 'validateIdfromalis'),
				'title' => Loc::getMessage('BIBLIO_CARD_ENTITY_IDFROMALIS_FIELD'),
			),
			'Author' => array(
				'data_type' => 'string',
				'validation' => array(__CLASS__, 'validateAuthor'),
				'title' => Loc::getMessage('BIBLIO_CARD_ENTITY_AUTHOR_FIELD'),
			),
			'Name' => array(
				'data_type' => 'text',
				'title' => Loc::getMessage('BIBLIO_CARD_ENTITY_NAME_FIELD'),
			),
			'SubName' => array(
				'data_type' => 'text',
				'title' => Loc::getMessage('BIBLIO_CARD_ENTITY_SUBNAME_FIELD'),
			),
			'PublishYear' => array(
				'data_type' => 'string',
				'validation' => array(__CLASS__, 'validatePublishyear'),
				'title' => Loc::getMessage('BIBLIO_CARD_ENTITY_PUBLISHYEAR_FIELD'),
			),
			'PublishPlace' => array(
				'data_type' => 'string',
				'validation' => array(__CLASS__, 'validatePublishplace'),
				'title' => Loc::getMessage('BIBLIO_CARD_ENTITY_PUBLISHPLACE_FIELD'),
			),
			'Publisher' => array(
				'data_type' => 'string',
				'validation' => array(__CLASS__, 'validatePublisher'),
				'title' => Loc::getMessage('BIBLIO_CARD_ENTITY_PUBLISHER_FIELD'),
			),
			'ISBN' => array(
				'data_type' => 'string',
				'validation' => array(__CLASS__, 'validateIsbn'),
				'title' => Loc::getMessage('BIBLIO_CARD_ENTITY_ISBN_FIELD'),
			),
			'Language' => array(
				'data_type' => 'integer',
				'title' => Loc::getMessage('BIBLIO_CARD_ENTITY_LANGUAGE_FIELD'),
			),
			'CountPages' => array(
				'data_type' => 'string',
				'validation' => array(__CLASS__, 'validateCountpages'),
				'title' => Loc::getMessage('BIBLIO_CARD_ENTITY_COUNTPAGES_FIELD'),
			),
			'Printing' => array(
				'data_type' => 'string',
				'validation' => array(__CLASS__, 'validatePrinting'),
				'title' => Loc::getMessage('BIBLIO_CARD_ENTITY_PRINTING_FIELD'),
			),
			'Format' => array(
				'data_type' => 'string',
				'validation' => array(__CLASS__, 'validateFormat'),
				'title' => Loc::getMessage('BIBLIO_CARD_ENTITY_FORMAT_FIELD'),
			),
			'Series' => array(
				'data_type' => 'string',
				'validation' => array(__CLASS__, 'validateSeries'),
				'title' => Loc::getMessage('BIBLIO_CARD_ENTITY_SERIES_FIELD'),
			),
			'VolumeNumber' => array(
				'data_type' => 'string',
				'validation' => array(__CLASS__, 'validateVolumenumber'),
				'title' => Loc::getMessage('BIBLIO_CARD_ENTITY_VOLUMENUMBER_FIELD'),
			),
			'VolumeName' => array(
				'data_type' => 'string',
				'validation' => array(__CLASS__, 'validateVolumename'),
				'title' => Loc::getMessage('BIBLIO_CARD_ENTITY_VOLUMENAME_FIELD'),
			),
			'Collection' => array(
				'data_type' => 'string',
				'validation' => array(__CLASS__, 'validateCollection'),
				'title' => Loc::getMessage('BIBLIO_CARD_ENTITY_COLLECTION_FIELD'),
			),
			'AccessType' => array(
				'data_type' => 'integer',
				'title' => Loc::getMessage('BIBLIO_CARD_ENTITY_ACCESSTYPE_FIELD'),
			),
			/*'isLicenseAgreement' => array(
				'data_type' => 'UNKNOWN',
				'title' => Loc::getMessage('BIBLIO_CARD_ENTITY_ISLICENSEAGREEMENT_FIELD'),
			),*/
			'Responsibility' => array(
				'data_type' => 'string',
				'validation' => array(__CLASS__, 'validateResponsibility'),
				'title' => Loc::getMessage('BIBLIO_CARD_ENTITY_RESPONSIBILITY_FIELD'),
			),
			'Annotation' => array(
				'data_type' => 'text',
				'title' => Loc::getMessage('BIBLIO_CARD_ENTITY_ANNOTATION_FIELD'),
			),
			'ContentRemark' => array(
				'data_type' => 'text',
				'title' => Loc::getMessage('BIBLIO_CARD_ENTITY_CONTENTREMARK_FIELD'),
			),
			'CommonRemark' => array(
				'data_type' => 'text',
				'title' => Loc::getMessage('BIBLIO_CARD_ENTITY_COMMONREMARK_FIELD'),
			),
			'PublicationInformation' => array(
				'data_type' => 'string',
				'validation' => array(__CLASS__, 'validatePublicationinformation'),
				'title' => Loc::getMessage('BIBLIO_CARD_ENTITY_PUBLICATIONINFORMATION_FIELD'),
			),
			'pdfLink' => array(
				'data_type' => 'string',
				'validation' => array(__CLASS__, 'validatePdflink'),
				'title' => Loc::getMessage('BIBLIO_CARD_ENTITY_PDFLINK_FIELD'),
			),
			'IdParent' => array(
				'data_type' => 'integer',
				'title' => Loc::getMessage('BIBLIO_CARD_ENTITY_IDPARENT_FIELD'),
			),
			'CreationMethod' => array(
				'data_type' => 'integer',
				'title' => Loc::getMessage('BIBLIO_CARD_ENTITY_CREATIONMETHOD_FIELD'),
			),
			'CreationActor' => array(
				'data_type' => 'integer',
				'title' => Loc::getMessage('BIBLIO_CARD_ENTITY_CREATIONACTOR_FIELD'),
			),
			'CreationDateTime' => array(
				'data_type' => 'datetime',
				'title' => Loc::getMessage('BIBLIO_CARD_ENTITY_CREATIONDATETIME_FIELD'),
			),
			'UpdatingActor' => array(
				'data_type' => 'integer',
				'title' => Loc::getMessage('BIBLIO_CARD_ENTITY_UPDATINGACTOR_FIELD'),
			),
			'UpdatingDateTime' => array(
				'data_type' => 'datetime',
				'title' => Loc::getMessage('BIBLIO_CARD_ENTITY_UPDATINGDATETIME_FIELD'),
			),
			'BBKText' => array(
				'data_type' => 'string',
				'validation' => array(__CLASS__, 'validateBbktext'),
				'title' => Loc::getMessage('BIBLIO_CARD_ENTITY_BBKTEXT_FIELD'),
			),
			'UDKText' => array(
				'data_type' => 'string',
				'validation' => array(__CLASS__, 'validateUdktext'),
				'title' => Loc::getMessage('BIBLIO_CARD_ENTITY_UDKTEXT_FIELD'),
			),
			'LanguageText' => array(
				'data_type' => 'string',
				'validation' => array(__CLASS__, 'validateLanguagetext'),
				'title' => Loc::getMessage('BIBLIO_CARD_ENTITY_LANGUAGETEXT_FIELD'),
			),
			'UDKThematicsCodeSequences' => array(
				'data_type' => 'text',
				'title' => Loc::getMessage('BIBLIO_CARD_ENTITY_UDKTHEMATICSCODESEQUENCES_FIELD'),
			),
			'IdRubrics' => array(
				'data_type' => 'string',
				'validation' => array(__CLASS__, 'validateIdrubrics'),
				'title' => Loc::getMessage('BIBLIO_CARD_ENTITY_IDRUBRICS_FIELD'),
			),
			'Indexes' => array(
				'data_type' => 'string',
				'validation' => array(__CLASS__, 'validateIndexes'),
				'title' => Loc::getMessage('BIBLIO_CARD_ENTITY_INDEXES_FIELD'),
			),
			'BX_TIMESTAMP' => array(
				'data_type' => 'datetime',
				'required' => true,
				'title' => Loc::getMessage('BIBLIO_CARD_ENTITY_BX_TIMESTAMP_FIELD'),
			),
			'BX_IS_UPLOAD' => array(
				'data_type' => 'integer',
				'title' => Loc::getMessage('BIBLIO_CARD_ENTITY_BX_IS_UPLOAD_FIELD'),
			),
			'BX_ALIS' => array(
				'data_type' => 'integer',
				'required' => true,
				'title' => Loc::getMessage('BIBLIO_CARD_ENTITY_BX_ALIS_FIELD'),
			),
			'BX_UID' => array(
				'data_type' => 'integer',
				'required' => true,
				'title' => Loc::getMessage('BIBLIO_CARD_ENTITY_BX_UID_FIELD'),
			),
			'BX_LIB_ID' => array(
				'data_type' => 'integer',
				'title' => Loc::getMessage('BIBLIO_CARD_ENTITY_BX_LIB_ID_FIELD'),
			),
			'BX_DELETE' => array(
				'data_type' => 'integer',
				'title' => Loc::getMessage('BIBLIO_CARD_ENTITY_BX_DELETE_FIELD'),
			),
			'EX_IdFromALIS' => array(
				'data_type' => 'string',
				'title' => Loc::getMessage('BIBLIO_CARD_ENTITY_EX_IDFROMALIS_FIELD'),
			),
			'EX_IdCard' => array(
				'data_type' => 'integer',
				'title' => Loc::getMessage('BIBLIO_CARD_ENTITY_EX_ID_CARD_FIELD'),
			),
			'EX_pdfLink' => array(
				'data_type' => 'string',
				'title' => Loc::getMessage('BIBLIO_CARD_ENTITY_EX_PDFLINK_FIELD'),
			),    
		);
	}
	public static function validateIdfromalis()
	{
		return array(
			new Entity\Validator\Length(null, 64),
		);
	}
	public static function validateAuthor()
	{
		return array(
			new Entity\Validator\Length(null, 500),
		);
	}
	public static function validatePublishyear()
	{
		return array(
			new Entity\Validator\Length(null, 45),
		);
	}
	public static function validatePublishplace()
	{
		return array(
			new Entity\Validator\Length(null, 500),
		);
	}
	public static function validatePublisher()
	{
		return array(
			new Entity\Validator\Length(null, 500),
		);
	}
	public static function validateIsbn()
	{
		return array(
			new Entity\Validator\Length(null, 50),
		);
	}
	public static function validateCountpages()
	{
		return array(
			new Entity\Validator\Length(null, 150),
		);
	}
	public static function validatePrinting()
	{
		return array(
			new Entity\Validator\Length(null, 45),
		);
	}
	public static function validateFormat()
	{
		return array(
			new Entity\Validator\Length(null, 45),
		);
	}
	public static function validateSeries()
	{
		return array(
			new Entity\Validator\Length(null, 450),
		);
	}
	public static function validateVolumenumber()
	{
		return array(
			new Entity\Validator\Length(null, 45),
		);
	}
	public static function validateVolumename()
	{
		return array(
			new Entity\Validator\Length(null, 450),
		);
	}
	public static function validateCollection()
	{
		return array(
			new Entity\Validator\Length(null, 100),
		);
	}
	public static function validateResponsibility()
	{
		return array(
			new Entity\Validator\Length(null, 500),
		);
	}
	public static function validatePublicationinformation()
	{
		return array(
			new Entity\Validator\Length(null, 255),
		);
	}
	public static function validatePdflink()
	{
		return array(
			new Entity\Validator\Length(null, 255),
		);
	}
	public static function validateBbktext()
	{
		return array(
			new Entity\Validator\Length(null, 100),
		);
	}
	public static function validateUdktext()
	{
		return array(
			new Entity\Validator\Length(null, 100),
		);
	}
	public static function validateLanguagetext()
	{
		return array(
			new Entity\Validator\Length(null, 32),
		);
	}
	public static function validateIdrubrics()
	{
		return array(
			new Entity\Validator\Length(null, 500),
		);
	}
	public static function validateIndexes()
	{
		return array(
			new Entity\Validator\Length(null, 500),
		);
	}
}