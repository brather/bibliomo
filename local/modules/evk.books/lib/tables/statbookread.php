<?php
namespace Evk\Books\Tables;

use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

/**
 * Class StatBookReadTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> BOOK_ID string(255) mandatory
 * <li> UID int mandatory
 * <li> UID_GUEST string(255) mandatory
 * <li> X_TIMESTAMP date mandatory
 * </ul>
 *
 * @package Nota\Exalead\Stat
 **/

class StatBookReadTable extends Entity\DataManager
{
    public static function getFilePath()
    {
        return __FILE__;
    }

    public static function getTableName()
    {
        return 'neb_stat_book_read';
    }

    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
                'title' => Loc::getMessage('BOOK_READ_ENTITY_ID_FIELD'),
            ),
            'BOOK_ID' => array(
                'data_type' => 'string',
                'required' => true,
                'validation' => array(__CLASS__, 'validateBookId'),
                'title' => Loc::getMessage('BOOK_READ_ENTITY_BOOK_ID_FIELD'),
            ),
            new Entity\ReferenceField(
                'BOOK',
                'Evk\Books\Tables\BiblioCardTable',
                array('=this.BOOK_ID' => 'ref.IdFromALIS')
            ),
            'UID' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('BOOK_READ_ENTITY_UID_FIELD'),
            ),
            'UID_GUEST' => array(
                'data_type' => 'string',
                'validation' => array(__CLASS__, 'validateUidGuest'),
                'title' => Loc::getMessage('BOOK_READ_ENTITY_UID_GUEST_FIELD'),
            ),
            'X_TIMESTAMP' => array(
                'data_type' => 'date',
                'title' => Loc::getMessage('BOOK_READ_ENTITY_X_TIMESTAMP_FIELD'),
            ),
        );
    }
    public static function validateBookId()
    {
        return array(
            new Entity\Validator\Length(null, 255),
        );
    }
    public static function validateUidGuest()
    {
        return array(
            new Entity\Validator\Length(null, 255),
        );
    }
}