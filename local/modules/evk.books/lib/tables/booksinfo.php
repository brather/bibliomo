<?php
	namespace Evk\Books\Tables;
	use Bitrix\Main\Entity;
	use Bitrix\Main\Localization\Loc;
	Loc::loadMessages(__FILE__);

	/**
	 * Class InfoTable
	 * 
	 * Fields:
	 * <ul>
	 * <li> ID int mandatory
	 * <li> BOOK_ID string(100) mandatory
	 * <li> VIEWS int optional
	 * </ul>
	 *
	 * @package Bitrix\Books
	 **/

	class BooksInfoTable extends Entity\DataManager
	{
		public static function getFilePath()
		{
			return __FILE__;
		}

		public static function getTableName()
		{
			return 'neb_books_info';
		}

		public static function getMap()
		{
			return array(
				'ID' => array(
					'data_type' => 'integer',
					'primary' => true,
					'autocomplete' => true,
					'title' => Loc::getMessage('INFO_ENTITY_ID_FIELD'),
				),
				'BOOK_ID' => array(
					'data_type' => 'string',
					'required' => true,
					'validation' => array(__CLASS__, 'validateBookId'),
					'title' => Loc::getMessage('INFO_ENTITY_BOOK_ID_FIELD'),
				),
				'LIB_ID' => array(
					'data_type' => 'integer',
					'title' => Loc::getMessage('INFO_ENTITY_LIB_ID_FIELD'),
				),				
				'VIEWS' => array(
					'data_type' => 'integer',
					'title' => Loc::getMessage('INFO_ENTITY_VIEWS_FIELD'),
				),
			);
		}
		public static function validateBookId()
		{
			return array(
				new Entity\Validator\Length(null, 100),
			);
		}
	}
?>