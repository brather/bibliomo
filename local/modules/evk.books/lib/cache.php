<?php
namespace Evk\Books;

class Cache
{
	public static function ClearCountBooksCache()
	{
		$cache_dir = sprintf("/%s/%s/", SITE_ID, "books_get_all_count_dir");
		$cache_id = md5($cache_dir);
		self::ClearByTag($cache_id);
	}
	public static function ClearMainNewsBlockCache()
	{
		$cache_dir = sprintf("/%s/%s/", SITE_ID, "main_news_block");
		$cache_id = md5($cache_dir);
		self::ClearByTag($cache_id);
	}
	public static function ClearCountUsersCache()
	{
		$cache_dir = sprintf("/%s/%s/", SITE_ID, "users_get_count");
		$cache_id = md5($cache_dir);
		self::ClearByTag($cache_id);
	}
	public static function ClearCache()
	{
		self::ClearByTag("iblock_id_library_search_page_smart");
		self::ClearByTag("iblock_id_library_search_page");
		self::ClearByTag("iblock_id_library_catalog_section_all");
		self::ClearByTag("iblock_id_library_catalog_section");
		self::ClearByTag("iblock_id_library_search_page_titles");
		self::ClearByTag("neb_library_stat_books");
		self::ClearByTag("neb_collections_list_search_page");
		self::ClearByTag("books_files_binding");
		self::ClearCountBooksCache();
		self::ClearCountUsersCache();
	}
	public static function ClearByTag($tag)
	{
		if(is_object($GLOBALS['CACHE_MANAGER']))
			$GLOBALS["CACHE_MANAGER"]->ClearByTag($tag);
	}
}