<?php
namespace Evk\Books;
/**
 * Class BBKIndexContainer
 * @property BBKIndex[] $indexes
 */
class BBKIndexContainer implements \IteratorAggregate
{
	public $indexes = array();
	public $indexes_codes = array();
	public $indexes_names = array();
	public $arSection = array();
	public $children_depth = 3;
	public function getIterator()
	{
		return new \ArrayIterator($this->indexes);
	}
	public static function Create()
	{
		return new self();
	}
	public function GetIndexObjectsFromXmlFile($filename, $stop = 0)
	{
		$stop = intval($stop);
		$xmlReader = new \XMLReader();
		$xmlReader->open($filename);
		$byStep = ($stop > 0);
		while($xmlReader->read())
		{
			if($xmlReader->name == "record" && $xmlReader->nodeType == \XMLReader::ELEMENT)
			{
				if($byStep && $stop-- <= 0) break;
				$obIndex = new BBKIndex();
				do {
					if($xmlReader->nodeType == \XMLReader::ELEMENT)
					{
						if($xmlReader->name == "index")
						{
							$name = $xmlReader->readString();
							$obIndex->SetName($name);
						}
						elseif($xmlReader->name == "rubric")
						{
							$obIndex->rubric = htmlspecialchars($xmlReader->readString());
						}
						elseif($xmlReader->name == "child_index")
						{
							$childName = htmlspecialchars(trim($xmlReader->readString()));
							$obIndex->children_index_codes[] = md5($childName);
						}
						elseif($xmlReader->name == "see")
						{
							$obIndex->see[] = htmlspecialchars($xmlReader->readString());
						}
						elseif($xmlReader->name == "see_also")
						{
							$obIndex->see_also[] = htmlspecialchars($xmlReader->readString());
						}
						elseif($xmlReader->name == "ext_rubric")
						{
							$obIndex->ext_rubric = htmlspecialchars($xmlReader->readString());
						}
						elseif($xmlReader->name == "related")
						{
							$obIndex->related[] = htmlspecialchars($xmlReader->readString());
						}
						elseif($xmlReader->name == "prnt_index")
						{
							$parentName = htmlspecialchars(trim($xmlReader->readString()));
							$obIndex->parent_index_code = md5($parentName);
						}
					}
					if($xmlReader->name == "record" && $xmlReader->nodeType == \XMLReader::END_ELEMENT)
						break;
				}
				while($xmlReader->read());
				$this->Add($obIndex);
			}
		}
		$this->replaceIndexes();
		uasort($this->indexes, function($a, $b){
			return (($a->depth < $b->depth) ? -1 : (($a->depth > $b->depth) ? 1 : 0));
		});
		return $this;
	}
	protected function replaceIndexes()
	{
		foreach($this->indexes as &$index)
		{
			if(!empty($index->parent_index_code))
			{
				if($parentIndex = $this->FindIndexObjectBySectionCode($index->parent_index_code))
				{
					$index->parent = $parentIndex;
					$index->depth = $parentIndex->depth + 1;
				}
			}
			if(!empty($index->children_index_codes))
			{
				foreach($index->children_index_codes as $code)
				{
					if($newIndex = $this->FindIndexObjectBySectionCode($code))
					{
						$newIndex->depth = $index->depth + 1;
						$index->children[] = $newIndex;
					}
				}
			}
		}
	}
	public function ImportXMLFile($filename, $stop = 0)
	{
		$this->GetIndexObjectsFromXmlFile($filename, $stop)->SaveToBitrixIndexes();
	}
	public function Add(BBKIndex $index)
	{
		if(!in_array($index->section_code, $this->indexes_codes))
		{
			$this->indexes[] = $index;
			$this->indexes_codes[] = $index->section_code;
			$this->indexes_names[] = $index->name;
		}
	}
	public function AddBySectionID(BBKIndex $index, $section_id)
	{
		$section_id = intval($section_id);
		if($section_id > 0)
		{
			if(($parentID = $this->FindIndexObjectBySectionID($section_id)))
			{
				$obParent = $this->indexes[$parentID];
				$obParent->AddChildForExistsObject($index);
				$index->parent = $obParent;
				$index->parent_index_name = $obParent->name;
			}
		}
		$this->indexes[] = $index;
		$this->indexes_codes[] = $index->section_code;
		$this->indexes_names[] = $index->name;
	}
	public function Print_indexes()
	{
		$return = "";
		foreach($this->indexes as $id=>$index)
		{
			if($index->depth == 1)
			{
				$return .= $index->PrintProperties();
				if($index->HasChildren())
				{
					$return .= $index->PrintChildren();
				}
			}
		}
		return $return;
	}
	public function GetIndexPositionFromSectionConeOnIndexes($code)
	{
		if(in_array($code, $this->indexes_codes) && ($id = array_search($code, $this->indexes_codes))!==false)
			return $id;
		return false;
	}
	public function GetIndexPositionFromIndexName($indexName)
	{
		if(in_array($indexName, $this->indexes_names) && ($id = array_search($indexName, $this->indexes_names))!==false)
			return $id;
		return false;
	}
	public function FindIndexObjectByIndexName($indexName)
	{
		if($id = $this->GetIndexPositionFromIndexName($indexName))
			return $this->indexes[$id];
		return null;
	}
	public function FindIndexObjectBySectionCode($code)
	{
		if(($id = $this->GetIndexPositionFromSectionConeOnIndexes($code))!==false)
			return $this->indexes[$id];
		return null;
	}
	public function FindIndexObjectBySectionID($section_Id)
	{
		foreach($this->indexes as $id=>$index)
			if($index->section_id == $section_Id)
				return $id;
		return null;
	}
	public function SaveToBitrixIndexes()
	{
		foreach($this->indexes as $id=>$index)
		{
			if($index->depth == 1)
			{
				$index->SaveToBitrix();
				$index->SaveChildrenToBitrix();
			}
		}
		\CIBlockSection::ReSort(15);
	}
	public function GetAllIndexCodes()
	{
		return $this->indexes_codes;
	}
	protected function getChildren2(&$arSection)
	{
		$arCountsSection = array();
		global $DB;
		$sqlStr = sprintf('
				SELECT COUNT(*) COUNT, s.ID S_ID
				FROM
					b_iblock_section s, b_iblock_element_prop_s10 p, b_uts_iblock_15_section u
				WHERE
					s.IBLOCK_ID=%d AND
					u.UF_LAST=1 AND
					u.VALUE_ID=s.ID AND
					p.PROPERTY_56=s.XML_ID
				group by s.ID, s.XML_ID',
				15
			);
		if($rsSect = $DB->Query($sqlStr))
		{
			while($arSect = $rsSect->Fetch())
			{
				$arCountsSection[] = $arSect["S_ID"];
				$arSection[$arSect["S_ID"]]["COUNT_BOOKS"] = $arSect["COUNT"];
			}
		}
		return $arCountsSection;
	}
	protected function getChildren(&$arSection, $id=null)
	{
		$arChildren = array();
		if(!is_null($id) && array_key_exists($id, $arSection))
		{
			if(!empty($arSection[$id]["CHILDREN"]) && $arSection[$id]["DEPTH"] > $this->children_depth)
			{
				$arChildren = array_merge($arChildren, $arSection[$id]["CHILDREN"]);
				foreach($arSection[$id]["CHILDREN"] as $childID)
				{
					$arChildren = array_merge($arChildren, $this->getChildren($arSection, $childID));;
				}
			}
			return $arChildren;
		}
		else
		{
			foreach($arSection as $sectionID=>$section)
			{
				if($section["DEPTH"] <= $this->children_depth) continue;
				$arSection[$sectionID]["CHILDREN"] = $this->getChildren($arSection, $sectionID);
			}
		}
		return array();
	}
	protected function fillIndexes2(&$arSection, &$arSectionsID, $id=null, &$arID=array())
	{
		if(!is_null($id))
		{
			if(!empty($arSection[$id]["PARENT"]))
			{
				if(!in_array($arSection[$id]["PARENT"], $arID))
					$arID[] = $arSection[$id]["PARENT"];
				if(empty($arSection[$arSection[$id]["PARENT"]]["COUNT_BOOKS"]))
					$arSection[$arSection[$id]["PARENT"]]["COUNT_BOOKS"] = $arSection[$id]["COUNT_BOOKS"];
				else
					$arSection[$arSection[$id]["PARENT"]]["COUNT_BOOKS"] += $arSection[$id]["COUNT_BOOKS"];
				$arIDs = $this->fillIndexes2($arSection, $arSectionsID, $arSection[$id]["PARENT"]);
				$arID = array_merge($arID, $arIDs);
			}
		}
		else
		{
			foreach($arSectionsID as $sectID)
			{
				if(!in_array($sectID, $arID))
					$arID[] = $sectID;
				$arIDs = $this->fillIndexes2($arSection, $arSectionsID, $sectID);
				$arID = array_merge($arID, $arIDs);
			}
		}
		return $arID;
	}
	protected function fillIndexes(&$arSection, $arIndexes)
	{
		foreach($arSection as $sectionID=>$section)
		{
			foreach($section["CHILDREN"] as $child)
			{
				$arSection[$sectionID]["INDEXES"][] = $arIndexes[$child];
			}
		}
	}
	public function GetSectionIDByIndexName($indexName)
	{
		if(empty($this->arSection))
			$this->GetBBKIndexList();
		foreach($this->arSection as $id=>$section)
		{
			if($section["INDEX"] == $indexName)
				return $id;
		}
		return null;
	}
	public function GetBBKIndexList($arSort=array("UF_LAST"=>"ASC"), $arFilter=array())
	{
		unset($arFilter["SECTION_ID"]);

		$arDefaultFilter = array(
			"IBLOCK_ID" => 15,
		);
		$arFilter = array_merge($arDefaultFilter, $arFilter);
		$obCache = new \CPHPCache();
		$cache_time = 3600000000;
		$cache_path = "/".SITE_ID."/neb.section.bkk.all.list.index/";
		$cache_id = md5($cache_path.serialize(array($arFilter, $arSort)));
		$arSection = array();
		if($obCache->InitCache($cache_time, $cache_id, $cache_path))
		{
			$vars = $obCache->GetVars();
			extract($vars);
		}
		else
		{
			if($obCache->StartDataCache($cache_time, $cache_id, $cache_path))
			{
				if($rsData = \CIBlockSection::GetList($arSort, $arFilter, false, array(
					"IBLOCK_ID",
					"ID",
					"DEPTH_LEVEL",
					"XML_ID",
					"UF_LAST",
					"IBLOCK_SECTION_ID",
				)))
				{
					global $CACHE_MANAGER;
					$CACHE_MANAGER->StartTagCache($cache_path);
					$CACHE_MANAGER->RegisterTag("catalog_bbk_sections");
					$CACHE_MANAGER->EndTagCache();
					$arIndexes = array();
					while($arData = $rsData->Fetch())
					{
						$arIndexes[$arData["ID"]] = $arData["XML_ID"];
						if(!array_key_exists($arData["ID"], $arSection))
						{
							$arSection[$arData["ID"]] = array(
								"CHILDREN" => array(),
								"PARENT" => $arData["IBLOCK_SECTION_ID"],
								"DEPTH" => $arData["DEPTH_LEVEL"],
								"INDEX" => $arData["XML_ID"],
								"LAST" => $arData["UF_LAST"],
							);
							if(!empty($arData["IBLOCK_SECTION_ID"]) && array_key_exists($arData["IBLOCK_SECTION_ID"], $arSection))
							{
								$arSection[$arData["IBLOCK_SECTION_ID"]]["CHILDREN"][] = $arData["ID"];
							}
							elseif(!empty($arData["IBLOCK_SECTION_ID"]))
							{
								$arSection[$arData["IBLOCK_SECTION_ID"]] = array(
									"CHILDREN" => array($arData["ID"]),
									"PARENT" => null,
									"DEPTH" => 1,
									"LAST" => false,
								);
							}
						}
						else
						{
							$arSection[$arData["ID"]]["DEPTH"] = $arData["DEPTH_LEVEL"];
							$arSection[$arData["ID"]]["PARENT"] = $arData["IBLOCK_SECTION_ID"];
							$arSection[$arData["ID"]]["INDEX"] = $arData["XML_ID"];
							$arSection[$arData["ID"]]["LAST"] = $arData["UF_LAST"];
						}
					}
					$arCountsSection = $this->getChildren2($arSection);
					$this->fillIndexes2($arSection, $arCountsSection, null, $arID);
					$arID = array_unique($arID);
				}
				if(!empty($arSection))
				{
					$obCache->EndDataCache(compact(
						"arSection"
					));
				}
				else
				{
					$obCache->AbortDataCache();
				}
			}
		}
		$this->arSection = $arSection;
		return compact('arSection', 'arID');
	}
	public function getBooksFromBitrixByBBK($indexNames)
	{
		$indexNamesCount = array();
		if(!empty($indexNames))
		{
			foreach($indexNames as $iName)
			{
				$indexNamesCount[$iName] = 0;
			}
			$obSearch = new SearchQuery();
			$arBooks = $obSearch->FindBooksSimple(array(), array("=PROPERTY_BBK"=>$indexNames), false, false, array("ID", "PROPERTY_BBK"));
			foreach($arBooks as $arBook)
			{
				$indexNamesCount[$arBook["PROPERTY_BBK_VALUE"]]++;
			}
		}
		return $indexNamesCount;
	}
	public function GetBooksCount(array &$SECTIONS=array())
	{
		if(empty($this->arSection))
			$this->GetBBKIndexList();
		$indexNames = array();
		foreach($SECTIONS as &$SECTION)
		{
			if($SECTION["UF_LAST"])
			{
				if(!empty($SECTION["XML_ID"]))
					$indexNames[] = $SECTION["XML_ID"];
				$SECTION["BOOKS_COUNT"] = 0;
			}
			elseif($SECTION["DEPTH_LEVEL"] > $this->children_depth)
			{
				if($SECTION["UF_LAST"])
				{
					if(is_array($this->arSection[$SECTION["ID"]]["INDEXES"]))
						$indexNames = array_merge($indexNames, $this->arSection[$SECTION["ID"]]["INDEXES"]);
					$SECTION["BOOKS_COUNT"] = 0;
				}
			}
		}

		$indexNames = array_unique($indexNames);

		$arIndexNamesCount = $this->getBooksFromBitrixByBBK($indexNames);
		foreach($SECTIONS as &$SECTION)
		{
			if(is_array($this->arSection[$SECTION["ID"]]["INDEXES"]))
			{
				foreach($this->arSection[$SECTION["ID"]]["INDEXES"] as $indexName)
				{
					if(array_key_exists($indexName, $arIndexNamesCount))
					$SECTION["BOOKS_COUNT"] += $arIndexNamesCount[$indexName];
				}
			}
		}

		return compact("arBooks", "indexNames");
	}
}