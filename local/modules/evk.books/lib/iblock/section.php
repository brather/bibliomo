<?
namespace Evk\Books\IBlock;
use Evk\Books\Singleton;
use Evk\Books\NPHPCacheExt;

/*
Класс расширение, для работы с разделами инфоблока
#use Bitrix\NotaExt\Iblock\Section;
#$arResult = Section::getList(array('IBLOCK_ID' => 2));
#$arResult = Section::getByID(1);
#$arResult = Section::getRootSectionPr($ID, $arSelect = array(), $IBLOCK_ID = false)
*/
class Section extends Singleton
{
	function __construct()
	{
		\Bitrix\Main\Loader::IncludeModule("iblock");
	}
	public static function GetMixedList($arOrder=Array("SORT"=>"ASC"), $arFilter=Array(), $bIncCnt = false, $arSelectedFields = false)
	{
		global $DB;

		$arResult = array();

		$arSectionFilter = array (
			"IBLOCK_ID"		=>$arFilter["IBLOCK_ID"],
			"?NAME"			=>$arFilter["NAME"],
			">=ID"			=>$arFilter["ID_1"],
			"<=ID"			=>$arFilter["ID_2"],
			">=TIMESTAMP_X"		=>$arFilter["TIMESTAMP_X_1"],
			"<=TIMESTAMP_X"		=>$arFilter["TIMESTAMP_X_2"],
			"MODIFIED_BY"		=>$arFilter["MODIFIED_USER_ID"]? $arFilter["MODIFIED_USER_ID"]: $arFilter["MODIFIED_BY"],
			">=DATE_CREATE"		=>$arFilter["DATE_CREATE_1"],
			"<=DATE_CREATE"		=>$arFilter["DATE_CREATE_2"],
			"CREATED_BY"		=>$arFilter["CREATED_USER_ID"]? $arFilter["CREATED_USER_ID"]: $arFilter["CREATED_BY"],
			"CODE"			=>$arFilter["CODE"],
			"EXTERNAL_ID"		=>$arFilter["EXTERNAL_ID"],
			"ACTIVE"		=>$arFilter["ACTIVE"],

			"CNT_ALL"		=>$arFilter["CNT_ALL"],
			"ELEMENT_SUBSECTIONS"	=>$arFilter["ELEMENT_SUBSECTIONS"],
		);
		if (isset($arFilter["CHECK_PERMISSIONS"]))
		{
			$arSectionFilter['CHECK_PERMISSIONS'] = $arFilter["CHECK_PERMISSIONS"];
			$arSectionFilter['MIN_PERMISSION'] = (isset($arFilter['MIN_PERMISSION']) ? $arFilter['MIN_PERMISSION'] : 'R');
		}
		if(array_key_exists("SECTION_ID", $arFilter))
			$arSectionFilter["SECTION_ID"] = $arFilter["SECTION_ID"];

		$arPROPERTY_SECTION = $arFilter["PROPERTY_SECTION"];

		unset($arFilter["PROPERTY_SECTION"]);

		$arSectionFilter = array_merge($arSectionFilter, $arPROPERTY_SECTION);

		$obSection = new \CIBlockSection;

		$rsSection = $obSection->GetList($arOrder, $arSectionFilter, $bIncCnt);

		while($arSection = $rsSection->Fetch())
		{
			$arSection["TYPE"]="S";
			$arResult[]=$arSection;
		}

		$arElementFilter = array (
			"IBLOCK_ID"		=>$arFilter["IBLOCK_ID"],
			"?NAME"			=>$arFilter["NAME"],
			"SECTION_ID"		=>$arFilter["SECTION_ID"],
			">=ID"			=>$arFilter["ID_1"],
			"<=ID"			=>$arFilter["ID_2"],
			"=ID"			=> $arFilter["ID"],
			">=TIMESTAMP_X"		=>$arFilter["TIMESTAMP_X_1"],
			"<=TIMESTAMP_X"		=>$arFilter["TIMESTAMP_X_2"],
			"CODE"			=>$arFilter["CODE"],
			"EXTERNAL_ID"		=>$arFilter["EXTERNAL_ID"],
			"MODIFIED_USER_ID"	=>$arFilter["MODIFIED_USER_ID"],
			"MODIFIED_BY"		=>$arFilter["MODIFIED_BY"],
			">=DATE_CREATE"		=>$arFilter["DATE_CREATE_1"],
			"<=DATE_CREATE"		=>$arFilter["DATE_CREATE_2"],
			"CREATED_BY"		=>$arFilter["CREATED_BY"],
			"CREATED_USER_ID"	=>$arFilter["CREATED_USER_ID"],
			">=DATE_ACTIVE_FROM"	=>$arFilter["DATE_ACTIVE_FROM_1"],
			"<=DATE_ACTIVE_FROM"	=>$arFilter["DATE_ACTIVE_FROM_2"],
			">=DATE_ACTIVE_TO"	=>$arFilter["DATE_ACTIVE_TO_1"],
			"<=DATE_ACTIVE_TO"	=>$arFilter["DATE_ACTIVE_TO_2"],
			"ACTIVE"		=>$arFilter["ACTIVE"],
			"?SEARCHABLE_CONTENT"	=>$arFilter["DESCRIPTION"],
			"?TAGS"			=>$arFilter["?TAGS"],
			"WF_STATUS"		=>$arFilter["WF_STATUS"],

			"SHOW_NEW"		=> ($arFilter["SHOW_NEW"] !== "N"? "Y": "N"),
			"SHOW_BP_NEW"		=> $arFilter["SHOW_BP_NEW"]
		);
		if (isset($arFilter["CHECK_PERMISSIONS"]))
		{
			$arElementFilter['CHECK_PERMISSIONS'] = $arFilter["CHECK_PERMISSIONS"];
			$arElementFilter['MIN_PERMISSION'] = (isset($arFilter['MIN_PERMISSION']) ? $arFilter['MIN_PERMISSION'] : 'R');
		}

		foreach($arFilter as $key=>$value)
		{
			$op = \CIBlock::MkOperationFilter($key);
			$newkey = strtoupper($op["FIELD"]);
			if(
				substr($newkey, 0, 9) == "PROPERTY_"
				|| substr($newkey, 0, 8) == "CATALOG_"
			)
			{
				$arElementFilter[$key] = $value;
			}
		}

		if(strlen($arFilter["SECTION_ID"])<= 0)
			unset($arElementFilter["SECTION_ID"]);

		if(!is_array($arSelectedFields))
			$arSelectedFields = Array("*");

		if(isset($arFilter["CHECK_BP_PERMISSIONS"]))
			$arElementFilter["CHECK_BP_PERMISSIONS"] = $arFilter["CHECK_BP_PERMISSIONS"];

		$obElement = new \CIBlockElement;

		$rsElement = $obElement->GetList($arOrder, $arElementFilter, false, false, $arSelectedFields);
		while($arElement = $rsElement->Fetch())
		{
			$arElement["TYPE"]="E";
			$arResult[]=$arElement;
		}

		$rsResult = new \CDBResult;
		$rsResult->InitFromArray($arResult);

		return $rsResult;
	}
	/*
	* @method getList - метод для выборки разделов инфоблока
	* @param array $arFilter
	* @param array $arSelect
	* @param array $arSort
	* @param array $arNavStartParams
	*/
	public static function getList($arFilter, $arSelect = array(), $arSort = array(), $bIncCnt = false, $arNavStartParams = false)
	{
		return self::getInstance()->GetListPr($arFilter, $arSelect, $arSort, $bIncCnt, $arNavStartParams);
	}

	private function GetListPr($arFilter, $arSelect = array(), $arSort = array(), $bIncCnt = false, $arNavStartParams = false)
	{
		$arResult=array();

		$obCacheExt    	= new NPHPCacheExt();
		$arCacheParams 	= func_get_args();
		$arCacheParams['iblock_id'] = $arFilter['IBLOCK_ID'];  # если в параметрах указывается ID инфоблока
		$arCacheParams['SCRIPT_NAME'] = $_SERVER['SCRIPT_NAME'];  # Уникальный кешь для разных страниц где вызывается функция

		if (!$obCacheExt->InitCache(__CLASS__.__function__, $arCacheParams))
		{
			$obj = self::GetListRawPr($arFilter, $arSelect, $arSort, $bIncCnt, $arNavStartParams);

			if(!empty($arNavStartParams['nPageSize']))
				$arResult["NAV_STRING"] = $obj->GetPageNavStringEx($navComponentObject, $arNavStartParams['navigationTitle'], $arNavStartParams['templateName'], ($arNavStartParams['showAlways'] == 'Y' ? 'Y' : ''));

			if($obj === false)
				return false;

			while($ar_result = $obj->GetNext())
			{
				if(!empty($arNavStartParams['nPageSize']))
					$arResult['ITEMS'][] = $ar_result;
				else
					$arResult[] = $ar_result;
			}

			$obCacheExt->StartDataCache($arResult);
		}
		else
		{
			$arResult = $obCacheExt->GetVars();
		}

		return $arResult;
	}

	/*
	* @method getListRaw метод для получения объекта выборки элементов инфоблока
	* @param array $arFilter
	* @param array $arSelect
	* @param array $arSort
	* @param array $arNavStartParams
	*/
	public static function getListRaw($arFilter, $arSelect = array(), $arSort = array(), $bIncCnt = false, $arNavStartParams = false)
	{
		return self::getInstance()->GetListRawPr($arFilter, $arSelect, $arSort, $bIncCnt, $arNavStartParams);
	}

	private function GetListRawPr($arFilter, $arSelect = array(), $arSort = array(), $bIncCnt = false, $arNavStartParams = false)
	{

		/*if(empty($arFilter['IBLOCK_ID']))
		{
		$GLOBALS['APPLICATION']->ThrowException('IBLOCK_ID Empty.');
		return false;
		}*/

		$arSelect[] = 'ID';
		$arSelect[] = 'NAME';

		if(empty($arFilter['GLOBAL_ACTIVE']))
			$arFilter['GLOBAL_ACTIVE'] = 'Y';

		if(empty($arSort))
			$arSort = array('SORT' => 'DESC', 'NAME' => 'ASC');

		return \CIBlockSection::GetList($arSort, $arFilter, $bIncCnt, $arSelect, $arNavStartParams);
	}

	/*
	* @method getByID метод для получения полей раздела по его ID
	* @param integer $ID
	* @param array $arSelect
	* @param integer|null $IBLOCK_ID
	*/
	public static function getByID($ID, $arSelect = array(), $IBLOCK_ID = null)
	{
		return self::getInstance()->GetByIDPr($ID, $arSelect, $IBLOCK_ID);
	}

	private function GetByIDPr($ID, $arSelect = array(), $IBLOCK_ID = null)
	{
		if(empty($ID))
		{
			$GLOBALS['APPLICATION']->ThrowException('ID section empty.');
			return false;
		}

		if($IBLOCK_ID == null)
		{
			$arResult = self::GetListPr(array('ID' => $ID), array('ID', 'IBLOCK_ID'));
			if(!empty($arResult))
				$IBLOCK_ID = $arResult[0]['IBLOCK_ID'];
		}

		if(empty($IBLOCK_ID))
		{
			$GLOBALS['APPLICATION']->ThrowException('IBLOCK_ID Empty.');
			return false;
		}

		$arResult = self::GetListPr(array('IBLOCK_ID' => $IBLOCK_ID, 'ID' => $ID), $arSelect);
		if(!empty($arResult))
			return $arResult['0'];
		else
			return false;
	}


	/*
	* @method getRootSection() метод возвращает корневой раздел по id раздела одного из детей
	* @param integer $ID
	* @param array $arSelect
	* @param integer|null $IBLOCK_ID
	*/
	public static function getRootSection($ID, $IBLOCK_ID = false, $arSelect = array())
	{
		return self::getInstance()->getRootSectionPr($ID, $IBLOCK_ID, $arSelect);
	}

	private function getRootSectionPr($ID, $IBLOCK_ID = false, $arSelect = array())
	{
		if(empty($ID))
		{
			$GLOBALS['APPLICATION']->ThrowException('ID section empty.');
			return false;
		}

		if($IBLOCK_ID === false)
		{
			$arResult = self::GetListPr(array('ID' => $ID), array('ID', 'IBLOCK_ID'));
			if(!empty($arResult))
				$IBLOCK_ID = $arResult[0]['IBLOCK_ID'];
		}

		$obCacheExt    	= new NPHPCacheExt();
		$arCacheParams 	= func_get_args();
		$arCacheParams['iblock_id'] = $IBLOCK_ID;  # если в параметрах указывается ID инфоблока
		if (!$obCacheExt->InitCache(__CLASS__.__function__, $arCacheParams))
		{
			$nav = \CIBlockSection::GetNavChain($IBLOCK_ID, $ID, $arSelect);
			$arResult = $nav->GetNext();
			$obCacheExt->StartDataCache($arResult);
		}
		else
		{
			$arResult = $obCacheExt->GetVars();
		}

		return $arResult;
	}
}

?>