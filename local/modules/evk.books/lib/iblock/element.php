<?
namespace Evk\Books\IBlock;
use Evk\Books\Books;
use Evk\Books\Singleton;
use Evk\Books\NPHPCacheExt;

/*
 * Класс-расширение, для работы с элементами инфоблока.
 *
 * Примеры использования
 * Получение списка элементов с заданными параметрами
 * use Bitrix\NotaExt\Iblock\Element
 *       $arResult = Element::getList(array('IBLOCK_ID' => 2), 2, array('DETAIL_TEXT' , 'skip_other' => true), array(), array('nPageSize' => ''));
 * получение крайних элементов, относительно указанного
 *       $arResult = Element::getLeftRight(2, 4, array('DETAIL_TEXT'), array(), array(), 2);
 * получение элемента по его ID
 *       $arResult = Element::getByID(3, array('DETAIL_TEXT'));
 */

/**
 * Class Element
 * @package Evk\Books\IBlock
 */
class Element extends Singleton
{
	public function __construct()
	{
		\Bitrix\Main\Loader::IncludeModule("iblock");
	}

	/*
	* @method getList - метод для выборки элементов инфоблока
	* @param array $arFilter массив с параметрами для фильтрации
	* @param integer $intNavCount  сколько элементов выбирать, если нужно выбрать все нужно передать false
	* @param array $arSelect массив с параметрами для выборки по умолчанию - ID, IBLOCK_ID, NAME, DETAIL_PAGE_URL
	* @param array $arSort массив с параметрами для сортировки по умолчанию - array('SORT' => 'DESC', 'NAME' => 'ASC', 'ACTIVE_FROM' => 'DESC')
	* @param array $arNavParams массиы с параметрами для постранички, по умолчанию строится по nTopCount-сколько элементов выбирать, если нужно указать сколько элементов выбрать на странице, то нужно указать array('nPageSize' => '') , при этом значение берется из  $intNavCount. так же можно дополнять массив параметрами которые поддерживает CIBlockElement::GetList
	* @param array|boolean=false $arGroupBy массив для группировки, по умолчанию = false - не использовать группировку
	* @param boolean $getFullProp выбирать все свойства через метод $ob->GetProperties();, по умолчанию = false, выбирать свойства в $arSelect
	*/
	public static function getList($arFilter, $intNavCount = 1, $arSelect = array(), $arSort = array(), $arNavParams = array(), $arGroupBy = false, $getFullProp = false)
	{
		return self::getInstance()->GetListPr($arFilter, $intNavCount, $arSelect, $arSort, $arNavParams, $arGroupBy, $getFullProp);
	}

	private function GetListPr($arFilter, $intNavCount = 1, $arSelect = array(), $arSort = array(), $arNavParams = array(), $arGroupBy = false, $getFullProp = false)
	{
		$arResult = array();
		$obCacheExt    	= new NPHPCacheExt();
		$arCacheParams 	= func_get_args();
		$arCacheParams['iblock_id'] = $arFilter['IBLOCK_ID'];  # если в параметрах указывается ID инфоблока
		/*
		* Устанавливаем парметры навигационной цепочки
		*/
		if($intNavCount !== false and intval($intNavCount) > 0)
		{
			$intNavCount = intval($intNavCount);
			if(empty($arNavParams))
				$arNavParams = array('nTopCount' => $intNavCount);
			else
			{
				if(array_key_exists('nTopCount', $arNavParams))
					$arNavParams['nTopCount'] =  $intNavCount;
				elseif(array_key_exists('nPageSize', $arNavParams))
					$arNavParams['nPageSize'] =  $intNavCount;
			}
			if($intNavCount > 0 and !array_key_exists('nTopCount', $arNavParams))
			{
				$arNavigation = \CDBResult::GetNavParams($arNavParams);
				$arCacheParams['ar_nav'] = $arNavigation;
			}
		}
		else
		{
			$arNavParams = false;
		}

		$arCacheParams['nav_params'] = $arNavParams;

		if (!$obCacheExt->InitCache(__CLASS__.__function__, $arCacheParams))
		{
			$obj = self::GetListRawPr($arFilter, $arSelect, $arSort, $arNavParams, $arGroupBy);

			if($obj === false)
				return false;

			if(intval($intNavCount) == 1 and !array_key_exists('nElementID', $arNavParams) and !array_key_exists('nPageSize', $arNavParams))
			{
				if($ob = $obj->GetNextElement()){
					$arResult['ITEM'] = $ob->GetFields();
					if($getFullProp)
						$arResult['ITEM']['PROPERTIES'] = $ob->GetProperties();
				}
			}
			else
			{
				while($ob = $obj->GetNextElement()){
					$arFileds = $ob->GetFields();
					# выбрать все свойства
					if($getFullProp)
						$arFileds['PROPERTIES'] = $ob->GetProperties();
					$arResult['ITEMS'][] = $arFileds;
				}
			}

			/*
			* формирование навигационной цепочки
			*/

			if(intval($intNavCount) > 0 and !array_key_exists('nTopCount', $arNavParams))
			{
				$arResult["NAV_STRING"] = $obj->GetPageNavStringEx($navComponentObject, !empty($arNavParams['bDescPageNumbering']) ? $arNavParams['bDescPageNumbering'] : '', !empty($arNavParams['bTempaltePage']) ? $arNavParams['bTempaltePage'] : '', $arNavParams['bShowAll'] === true ? true : false);
				$arResult["NAV_CACHED_DATA"] = $navComponentObject->GetTemplateCachedData();
				$arResult["NAV_RESULT"] = $obj;
			}
			$obCacheExt->StartDataCache($arResult);
		}
		else
		{
			$arResult = $obCacheExt->GetVars();
		}
		return $arResult;
	}

	/*
	* @method getListRaw - метод для получение объекта выборки элементов инфоблока
	* @param array $arSelect
	* @param array $arSort
	* @param string $arNavParams
	* @param array $arGroupBy
	*/
	public static function getListRaw($arFilter, $arSelect = array(), $arSort = array(), $arNavParams = false, $arGroupBy = false)
	{
		return self::getInstance()->GetListRawPr($arFilter, $arSelect, $arSort, $arNavParams, $arGroupBy);
	}

	private function GetListRawPr($arFilter, $arSelect = array(), $arSort = array(), $arNavParams = false, $arGroupBy = false)
	{
		if(empty($arSort))
			$arSort=array('SORT' => 'DESC', 'NAME' => 'ASC', 'ACTIVE_FROM' => 'DESC');

		if(!in_array('skip_other', $arSelect))
		{
			$arSelect[]='ID';
			$arSelect[]='IBLOCK_ID';
			$arSelect[]='NAME';
			$arSelect[]='DETAIL_PAGE_URL';
		}
		else
			unset($arSelect[array_search('skip_other', $arSelect)]);

		if(empty($arFilter['ACTIVE']) and $arFilter['ACTIVE'] !== false)
			$arFilter['ACTIVE']='Y';
		elseif($arFilter['ACTIVE'] === false)
			unset($arFilter['ACTIVE']);

		if($arFilter['ACTIVE_DATE'] !== false)
			$arFilter['ACTIVE_DATE']='Y';
		elseif($arFilter['ACTIVE_DATE'] === false)
			unset($arFilter['ACTIVE_DATE']);

		return \CIBlockElement::GetList(
			$arSort,
			$arFilter,
			$arGroupBy,
			$arNavParams,
			$arSelect
		);

	}

	/*
	* @method  getByID - метод для выборки элемента по его ID
	* @param - integer $ID
	* @param - array $arSelect
	*/
	public static function getByID($ID, $arSelect = array())
	{
		return self::getInstance()->GetByIDPr($ID, $arSelect);
	}

	private function GetByIDPr($ID, $arSelect = array())
	{
		$ID = intval($ID);

		if(empty($ID))
		{
			$GLOBALS['APPLICATION']->ThrowException('ID section empty.');
			return false;
		}

		$arResultIblock = self::GetListPr(array('ID' => $ID), 1, $arSelect);

		if(empty($arResultIblock['ITEM']))
			return false;

		$key_property = preg_grep("#PROPERTY#", $arSelect);

		if(!empty($key_property))
		{
			$arResult = self::GetListPr(array('ID' => $ID, 'IBLOCK_ID' => $arResultIblock['ITEM'][0]['IBLOCK_ID']), 1, $arSelect);
			return $arResult['ITEM'];
		}
		else
		{
			return $arResultIblock['ITEM'];
		}
	}

	/*
	* @method getLeftRight - метод для получения соседних элементов относительно указанного
	* @param integer $IBLOCK_ID
	* @param integer $ELEMENT_ID
	* @param array $arSelect
	* @param array $arSort
	* @param array $arFilter
	* @param integer $nPageSize
	*/
	public static function getLeftRight($IBLOCK_ID, $ELEMENT_ID, $arSelect = array(), $arSort = array(), $arFilter = array(), $nPageSize = 1)
	{
		return self::getInstance()->GetLeftRightPr($IBLOCK_ID, $ELEMENT_ID, $arSelect, $arSort, $arFilter, $nPageSize);
	}
	private function GetLeftRightPr($IBLOCK_ID, $ELEMENT_ID, $arSelect = array(), $arSort = array(), $arFilter = array(), $nPageSize = 1)
	{
		if(empty($IBLOCK_ID))
		{
			$GLOBALS['APPLICATION']->ThrowException('IBLOCK_ID empty.');
			return false;
		}

		if(empty($ELEMENT_ID))
		{
			$GLOBALS['APPLICATION']->ThrowException('ELEMENT_ID empty.');
			return false;
		}

		$arFilter['IBLOCK_ID'] = $IBLOCK_ID;

		$arNavParams = array('nElementID' => $ELEMENT_ID, 'nPageSize' => '');

		$arResultGetList = self::GetListPr($arFilter, $nPageSize, $arSelect, $arSort, $arNavParams);

		if(empty($arResultGetList['ITEMS']))
			return false;

		$arResult = array();
		$arResult["NEXT"] = array();
		$arResult["PREV"] = array();
		$end = false;

		foreach($arResultGetList['ITEMS'] as $arFields)
		{
			if($arFields["ID"] == $ELEMENT_ID)
			{
				$end = true;
				$arResult["CURRENT"]["NO"] = $arFields["RANK"];
			}
			elseif($end)
			{
				$arResult["NEXT"][] = $arFields;
			}
			else
			{
				array_unshift($arResult["PREV"], $arFields);
			}
		}

		return $arResult;
	}
}
?>