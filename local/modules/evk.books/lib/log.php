<?php
namespace Evk\Books;

class Log
{
	public static $DIR;
	public static $IF = true;
	public function __callStatic($name, $args)
	{
		if((bool)self::$IF === false)
			return false;
		$time = time();
		$datetime = date("Y.m.d H:i:s");
		$line = $file = null;
		if(isset($args[0]) && is_numeric($args[0]))
		{
			$line = intval($args[0]);
		}
		elseif(isset($args[0]))
		{
			$file = $args[0];
		}
		if(isset($args[1]) && is_numeric($args[1]))
		{
			$line = intval($args[1]);
		}
		elseif(isset($args[1]))
		{
			$file = $args[1];
		}
		if((!isset(self::$DIR) || !is_dir(self::$DIR)) && file_exists($file))
		{
			self::$DIR = dirname($file);
		}
		elseif(!isset(self::$DIR) || !is_dir(self::$DIR))
		{
			self::$DIR = __DIR__;
		}
		$filename = self::$DIR."/".$name.date("_Ymd", $time).".log";
		$text = sprintf("%s F(%s) L(%s)\n", $datetime, $file, $line);
		if(count($args) > 2)
		{
			$args = array_slice($args, 2);
			foreach($args as $arg)
				if(is_array($arg))
					$text .= print_r($arg, 1);
				else
					$text .= $arg."\n";
		}
		$f = fopen($filename, "a+");
		flock($f, LOCK_EX);
		fwrite($f, $text);
		fflush($f);
		flock($f, LOCK_UN);
		fclose($f);
	}
}