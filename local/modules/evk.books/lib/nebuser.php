<?
namespace Evk\Books;

class nebUser extends \CUser{
	public $USER_ID = 0;
	public $role = 'user';
	public $EICHB = '';
	public $error = '';

	protected static $_arUserRegisterTypes;
	protected  $_userRegisterTypeId;
	protected  $_userRegisterTypeCode;

	public function __construct($USER_ID = 0)
	{
		global $USER;
		if(empty($USER_ID))
			$USER_ID = $USER->GetID();
		$this->USER_ID = $USER_ID;
	}

	/*
	@method    getRole
	Определяем роль текущего пользователя
	- обычный пользователь
	- администратор библиотеки
	- редактор библиотеки
	- контроллер библиотеки

	*/
	public function getRole()
	{
		if(empty($this->USER_ID))
			return false;

		$arGroupsID = self::GetUserGroup($this->USER_ID);

		$arFilter = Array
		(
			"ACTIVE"        => "Y",
		);
		$by="c_sort";
		$order="desc";
		$rsGroups = \CGroup::GetList($by, $order, $arFilter);
		while($arGroups = $rsGroups->Fetch())
		{
			if(empty($arGroups['STRING_ID']) or !in_array($arGroups['ID'], $arGroupsID)) continue;
			$arUsersGroups[$arGroups['ID']] = $arGroups['STRING_ID'];
		}
		/*
		Если свопадений не найдено, возвращаем роль по умолчанию 'user'
		*/
		if(!empty($arUsersGroups))
		{
			if(array_search(UGROUP_LIB_CODE_ADMIN, $arUsersGroups))
				$this->role = UGROUP_LIB_CODE_ADMIN;

			if(array_search(UGROUP_LIB_CODE_EDITOR, $arUsersGroups))
				$this->role = UGROUP_LIB_CODE_EDITOR;

			if(array_search(UGROUP_LIB_CODE_CONTORLLER, $arUsersGroups))
				$this->role = UGROUP_LIB_CODE_CONTORLLER;

		}

		return $this->role;
	}
	/*
	Является ли пользователь сотрубником библиотеки
	*/
	public function isLibrary()
	{
		$role = self::getRole();
		if($role == UGROUP_LIB_CODE_ADMIN or $role == UGROUP_LIB_CODE_EDITOR or $role == UGROUP_LIB_CODE_CONTORLLER)
			return true;
		else
			return false;
	}

	/*
	Получить поля библиотеки к которой привязан пользователь
	*/
	public function getLibrary()
	{
		$arUser = self::getUser();
		if(empty($arUser))
			return false;

		if(empty($arUser['UF_LIBRARY']))
		{
			$this->role = 'user';
			return false;
		}
		$arLibrary = IBlock\Element::getByID($arUser['UF_LIBRARY'], array('ID', 'NAME', 'DETAIL_PAGE_URL', 'PROPERTY_STATUS', 'PROPERTY_LIBRARY_LINK', 'skip_other'));
		return $arLibrary;
	}

	/*
	Установить ЕЭЧБ
	*/
	public function setEICHB(){

		$arUser = self::getUser();
		if(empty($arUser))
			return false;

		if(empty($arUser['UF_NUM_ECHB']))
		{
			$DATEFORMAT = date('dmyhms', MakeTimeStamp($arUser['DATE_REGISTER']));
			$EICHB = $arUser['ID'].$DATEFORMAT;
			if(strlen($EICHB) > 8)
				$EICHB = substr($EICHB, 0, 8);

			$user = new static;
			$fields = Array(
				"UF_NUM_ECHB" => $EICHB
			);
			$user->Update($arUser['ID'], $fields);

			J::add('eechb', 'add', Array('FIO' => $arUser['LAST_NAME'] . ' ' . $arUser['NAME'], 'NUMBER' => $EICHB));
		}
		else
		{
			$EICHB = $arUser['UF_NUM_ECHB'];
		}
		$this->EICHB = $EICHB;
	}

	/*
	Сделать пользователя верифицированным
	*/
	public function setActive()
	{
		$arUser = static::getUser();
		if(empty($arUser))
			return false;

		$user = new static;
		$fields = Array(
			"UF_STATUS" => USER_ACTIVE_ENUM_ID
		);
		$user->Update($arUser['ID'], $fields);
	}
	/*
	Получить поля текущего пользователя
	*/
	public function getUser(){
		if(empty($this->USER_ID))
			return false;

		$rsUser = static::GetByID($this->USER_ID);
		return $rsUser->Fetch();
	}

	/*
	Получить значения поля типа СПИСОК
	*/
	public static function getFieldEnum($arFilter)
	{
		if(empty($arFilter))
			return false;

		$arResult = array();

		if(!empty($arFilter['USER_FIELD_NAME']))
		{
			if(!is_array($arSort) || count($arSort)<=0)
			{
				$arSort = array($by=>$order);
			}
			$rsData = \CUserTypeEntity::GetList( $arSort, array('FIELD_NAME' => $arFilter['USER_FIELD_NAME']) );
			if($arRes = $rsData->Fetch())
			{
				unset($arFilter['USER_FIELD_NAME']);
				$arFilter['USER_FIELD_ID'] = $arRes['ID'];
			}
			else
				return false;

		}


		$rsGender = \CUserFieldEnum::GetList(array('SORT' => 'ASC'), $arFilter);
		while($arGender = $rsGender->GetNext())
		{
			if (strrpos($arGender["VALUE"], "/") !== false)
			{
				$arGenders = explode("/", $arGender["VALUE"]);
				if (LANGUAGE_ID == 'ru')
				{
					$arGender["VALUE"] = $arGenders[0];
				}
				elseif (LANGUAGE_ID == 'en')
				{
					$arGender["VALUE"] = $arGenders[1];
				}
			}
			$arResult[] = $arGender;
		}

		return $arResult;
	}

	/*
	Добавить пользователя
	*/
	public function Add($arFields)
	{
		$user = new parent;

		$ID = $user->Add($arFields);
		if(intval($ID) > 0)
			$this->USER_ID = $ID;
		else
			$this->error = $user->LAST_ERROR;
		return $ID;
	}

	public static function isShowProtectedContent()
	{
		/*
		global $USER;
		if($USER->IsAuthorized())
		{
			$user = new static;
			$arUser = $user->getUser();
			// заглушка по просьбе клиента, пускаем к закрытым изданиям всех у кого загружен паспорт
			if(!empty($arUser['UF_SCAN_PASSPORT1']) or !empty($arUser['UF_RGB_USER_ID']) or intval($arUser['UF_REGISTER_TYPE']) == 39)
				return true;
		}
		if(NebWorkplaces::checkAccess())
			return true;
		return false;
		*/
		return true;
	}

	/**
	 * Получение списка значений для UF_REGISTER_TYPE
	 * Ключи массива [ID] => [XML_ID, VALUE], при необходимости другие
	 * @return array
	 */
	public static function getAllUserRegisterTypes()
	{
		if (!empty(static::$_arUserRegisterTypes)){
			return static::$_arUserRegisterTypes;
		}

		$obCacheExt = new NPHPCacheExt();
		$arCacheParams['FIELD_NAME'] = 'UF_REGISTER_TYPE';
		$cacheTag = 'NEBUSER_ENUM';

		if ( !$obCacheExt->InitCache(__METHOD__, $arCacheParams, $cacheTag) ) {
			$arRegTypes = static::getFieldEnum(array('USER_FIELD_NAME' => 'UF_REGISTER_TYPE'));
			/*
			 * keys: id, USER_FIELD_ID, VALUE, DEF, SORT, XML_ID
			 */
			if (is_array($arRegTypes) && count($arRegTypes) > 0) {
				$arTypes = array();
				foreach ($arRegTypes AS $regType) {
					$arTypes[$regType['ID']] = array(
						//'USER_FIELD_ID' => $regType['USER_FIELD_ID'],
						'VALUE' => $regType['VALUE'],
						//'DEF' => $regType['DEF'],
						//'SORT' => $regType['SORT'],
						'XML_ID' => $regType['XML_ID']
					);
				}
			}

			static::$_arUserRegisterTypes = $arTypes;
			$obCacheExt->StartDataCache(
				static::$_arUserRegisterTypes
			);
		} else {
			static::$_arUserRegisterTypes = $obCacheExt->GetVars();
		}
		return static::$_arUserRegisterTypes;
	}

	/**
	 * Получение ID значения UF_REGISTER_TYPE для данного пользователя
	 *
	 * @return int
	 */
	public function getUserRegisterTypeId(){
		if (empty($this->_userRegisterTypeId)){

			$obCacheExt    	= new NPHPCacheExt();
			$arCacheParams['USER_ID'] = $this->USER_ID;
			$cacheTag = 'NEBUSER_UF_REGTYPE';

			if ( !$obCacheExt->InitCache(__METHOD__, $arCacheParams, $cacheTag) ) {
				$rsUser = static::GetList($by = array(), $order = array(),
					array(
						"ID" => $this->USER_ID,
					),
					array(
						"SELECT" => array(
							"USER_FIELD_ID" => "UF_REGISTER_TYPE",
						),
					)
				);
				if ($rsUser) {
					$arUser = $rsUser->Fetch();
				}
				$this->_userRegisterTypeId = $arUser['UF_REGISTER_TYPE'];

				$obCacheExt->StartDataCache(
					$this->_userRegisterTypeId
				);
			} else {
				$this->_userRegisterTypeId = $obCacheExt->GetVars();
			}
		}
		return $this->_userRegisterTypeId;
	}

	/**
	 * Получение XML_ID значения UF_REGISTER_TYPE для данного пользователя
	 *
	 * @return array() keys: XML_ID, VALUE;
	 */
	public function getUserRegisterTypeCode()
	{
		$arTypes = static::getAllUserRegisterTypes();
		return $arTypes[$this->getUserRegisterTypeId()];
	}
}
?>