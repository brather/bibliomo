<?php
namespace Evk\Books;
class SearchQueryLogic implements \IteratorAggregate
{
	public function getIterator()
	{
		return new \ArrayIterator($this);
	}


	/*
	protected $arVarsReplace = array(
		"publishplace" => "PROPERTY_PUBLISH_PLACE",
		"publisher" => "PROPERTY_PUBLISH",
		"title" => "NAME",
		"authorbook" => "PROPERTY_AUTHOR",
		"library" => "PROPERTY_LIBRARIES",
		"ALL" => true,
	);
	protected $arLogicQueries = array();
	protected $arLogic = array();
	protected $theme;
	protected $text;
	protected $logic;
	protected $id;
	protected $idOrigin;
	protected $themeOrigin;

	protected function Add(QueryLogic $ql)
	{
		$id = $this->arrayPush($this->arLogicQueries, $ql);
		$this->arLogicQueries[$id]->setID($id);
		return $id;
	}
	public static function Create()
	{
		return new self();
	}
	public function AddQueryArray($array)
	{
		foreach ($array["text"] as $key => $text)
		{
			$text = trim($text);
			if(!empty($text))
				$this->Add(new QueryLogic($array, $key));
		}

		$this->getFilterFields();
		return $this;
	}

	public function __construct($arVars = null, $key = null)
	{
		if (is_array($arVars) && is_numeric($key)) {
			$this->themeOrigin = $arVars['theme'][$key];
			$this->theme = $this->getThemeByKey($key, $arVars);
			$this->text = $arVars['text'][$key];
			$this->logic = $arVars['logic'][$key];
			$this->idOrigin = $key;
		}
	}

	public function GetFilter()
	{
		//Books::p('GetFilter', $this->arLogic);
		return $this->arLogic;
	}

	protected function getFilterFields()
	{
		if ($this->checkORExists()) {
			$key = $this->arrayPush($this->arLogic, array("LOGIC" => "OR"));
			$allLogic = &$this->arLogic[$key];
		} else {
			$allLogic = &$this->arLogic;
		}
		$this->addFilter($allLogic);
		//Books::p('getFilterFields', $allLogic);
		$this->filterArrayToString($allLogic);
		$this->markStrictFields($this->arLogic);
	}

	protected function markStrictFields(&$arLogic)
	{
		$isStrings = true;
		foreach ($arLogic as $key => $val) {
			if (!is_string($val) || $key == "LOGIC") {
				$isStrings = false;
			}
		}
		if ($isStrings) {
			$arNewLogic = array();
			$ex = false;
			foreach ($arLogic as $key => $val) {
				if($key=="PROPERTY_LIBRARIES")
					$key = sprintf("%s.NAME", $key);
				if (!isset($_REQUEST["s_strict"]) || $_REQUEST["s_strict"] != "Y") {
					if (strpos($key, "!") === 0) {
						$ex = true;
						$key = substr($key, 1);
					}
					if (strpos($val, " && ") !== false) {
						$arNewLogic[sprintf("%s%s%s", (($ex) ? "!" : ""), "?", $key)] = $val;
					} else {
						$arNewLogic[sprintf("%s%s%s", (($ex) ? "!" : ""), "%", $key)] = $val;
					}
				}
			}
			$arLogic = $arNewLogic;
		} else {
			foreach ($arLogic as $key => &$val) {
				if (is_array($val)) {
					$this->markStrictFields($val);
				}
			}
		}
	}

	protected function addFilter(&$allLogic)
	{
		if ($this->checkORExists()) {
			$key = $this->arrayPush($allLogic, array());
		}
		for ($i = 0; $i < $this->Count(); $i++) {
			$obLogic = $this->Get($i);
			if ($obLogic->logic == "OR" && $obLogic->getID() > 0) {
				$key = $this->arrayPush($allLogic, array());
			} elseif ($obLogic->logic != "OR" && $obLogic->getID() == 1 && $this->Get(0)->logic == "OR") {
				$key = $this->arrayPush($allLogic, array());
			}
			if ($this->checkORExists())
				$arLogic = &$allLogic[$key];
			else
				$arLogic = &$allLogic;
			if ($obLogic->logic == "OR" || $obLogic->logic == "AND") {
				if (is_string($obLogic->theme)) {
					$arLogic[$obLogic->theme][] = $obLogic->text;
				} elseif (is_bool($obLogic->theme)) {
					if (!$this->checkORExists()) {
						$key = $this->arrayPush($allLogic, array());
						$arLogic = &$allLogic[$key];
						$arLogic["LOGIC"] = "OR";
						foreach ($this->arVarsReplace as $keyString => $val) {
							if (is_string($val)) {
								$arLogic[][$val][] = $obLogic->text;
							}
						}
					} else {
						foreach ($this->arVarsReplace as $keyString => $val) {
							if (is_string($val)) {
								$arLogic[$val][] = $obLogic->text;
							}
						}
					}
				}
			} elseif ($obLogic->logic == "NOT") {
				$arLogic["!" . $obLogic->theme][] = $obLogic->text;
			}
		}
		//Books::p($allLogic);
	}

	protected function filterArrayToString(&$array = null)
	{
		if (is_array($array)) {
			$need = true;
			foreach ($array as $key => $value) {
				if (!is_string($value) || !is_numeric($key)) {
					$need = false;
				}
			}
			if ($need) {
				if (count($array) > 1)
					$array = "(" . join(" && ", $array) . ")";
				else
					$array = join("", $array);
			} else {
				foreach ($array as &$arLogic) {
					$this->filterArrayToString($arLogic);
				}
			}
		}
	}

	protected function checkORExists()
	{
		if ($this->Count() > 1) {
			for ($i = 0; $i < $this->Count(); $i++) {
				if ($this->Get($i)->logic == "OR") {
					return true;
				}
			}
		}
		return false;
	}

	protected function getThemeByKey($key, $arVars)
	{
		if (array_key_exists($arVars["theme"][$key], $this->arVarsReplace)) {
			return $this->arVarsReplace[$arVars["theme"][$key]];
		}
		return null;
	}

	protected function arrayPush(&$array, $value = array())
	{
		$i = 0;
		while (true) {
			if (!array_key_exists($i, $array)) {
				$array[$i] = $value;
				break;
			}
			$i++;
		}
		return $i;
	}

	public function Count()
	{
		return count($this->arLogicQueries);
	}

	public function Get($key)
	{
		if ($this->is_set($key))
			return $this->arLogicQueries[$key];
		return null;
	}

	protected function setID($id)
	{
		$this->id = $id;
	}

	public function getID()
	{
		return $this->id;
	}

	public function is_set($key)
	{
		return array_key_exists($key, $this->arLogicQueries);
	}

	public function is_empty()
	{
		return $this->Count() <= 0;
	}

	public function GetLogic()
	{
		return $this->logic;
	}

	public function GetText()
	{
		return $this->text;
	}
	public function GetIDOrigin()
	{
		return $this->idOrigin;
	}
	public function GetTheme()
	{
		return $this->theme;
	}
	public function GetThemeOrigin()
	{
		return $this->themeOrigin;
	}
	public function GetOriginTheme()
	{
		return $this->themeOrigin;
	}

	public function GetList()
	{
		return $this->arLogicQueries;
	}

	public function pa()
	{
		foreach ($this->arLogicQueries as $logic) {
			$logic->printProps();
		}
	}

	protected function printProps()
	{
		echo $this->id . " " . $this->logic . " " . $this->theme . " " . $this->text . "<br/>";
	}

	public function p($array, $return = 0)
	{
		$str = "<pre>" . print_r($array, 1) . "</pre>";
		if ($return)
			return $str;
		echo $str;
	}
	*/
}