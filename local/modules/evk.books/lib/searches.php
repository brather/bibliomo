<?
namespace Evk\Books;
\CModule::IncludeModule("highloadblock");
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;
use Bitrix\Main\Type\DateTime;

class Searches
{
	private function getEntity_data_class($HIBLOCK = HIBLOCK_SEARCHSERS_USERS){
		$hlblock = HL\HighloadBlockTable::getById($HIBLOCK)->fetch();
		$entity = HL\HighloadBlockTable::compileEntity($hlblock);
		$entity_data_class = $entity->getDataClass();
		return $entity_data_class;
	}

	public static function delete($objectId){
		if(empty($objectId))
			return false;

		global $USER;

		$entity_data_class = self::getEntity_data_class();

		$rsData = $entity_data_class::getList(array(
			"select" 	=> array("ID"),
			"filter" 	=> array('UF_UID' => $USER->GetID(), 'ID' => (int)$objectId)
		));
		if($arData = $rsData->Fetch())
		{
			$entity_data_class::Delete($arData['ID']);
			Collections::removeLinkObject($arData['ID'], false, 'searches');
		}
	}
	public static function isSave()
	{
		global $USER;
		if (!$USER->IsAuthorized())
			return false;
		$uobj = new \nebUser();
		$role = $uobj->getRole();
		if($role == 'user')
			return true;
		else
			return false;
	}
	public static function UpdateQueryName($saveId)
	{
		if(self::isSave())
		{
			global $USER;
			$entity_data_class = self::getEntity_data_class();
			$rsData = $entity_data_class::getList(array(
				"select" => array("ID"),
				"filter" => array('ID' => $saveId, 'UF_UID' => $USER->GetID())
			));
			if($arData = $rsData->Fetch())
			{
				$entity_data_class::update($arData['ID'], array('UF_NAME' => htmlspecialcharsbx(trim($_REQUEST['name']))));
			}
		}
	}
	public static function SaveQuery()
	{
		if(self::isSave())
		{
			global $USER;
			$evk_search_params_hash = md5(htmlspecialcharsbx($_REQUEST["evk_search_params"]).$USER->GetID()."A386");
			if($evk_search_params_hash == $_REQUEST["evk_search_params_hash"])
			{
				$entity_data_class = self::getEntity_data_class();
				$saveID = 0;
				$rsData = $entity_data_class::getList(array(
					"select" => array("ID"),
					"filter" => array(
						'UF_MD5' => $evk_search_params_hash,
						'UF_UID' => $USER->GetID()
					)
				));
				if($arData = $rsData->Fetch())
				{
					$saveID = $arData['ID'];
				}
				else
				{
					$dt = new DateTime();
					$arFields = array(
						'UF_DATE_ADD'		=>$dt,
						'UF_FOUND'			=>(int)$_REQUEST['found'],
						'UF_QUERY'			=>htmlspecialcharsbx(trim($_REQUEST['q'])),
						'UF_MORE_OPTIONS'	=>htmlspecialcharsbx($_REQUEST['more_options']),
						'UF_URL'			=>htmlspecialcharsbx($_REQUEST['url']),
						'UF_UID'			=>$USER->GetID(),
						'UF_EXALEAD_PARAM'	=> $_REQUEST["evk_search_params"],
						'UF_MD5'	=> $evk_search_params_hash,
					);
					$result = $entity_data_class::add($arFields);
					$saveID = $result->getId();
				}
				echo json_encode(array('ID' => $saveID));
			}
		}
	}
}
?>