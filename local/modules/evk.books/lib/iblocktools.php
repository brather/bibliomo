<?
namespace Evk\Books;

class IblockTools extends Singleton
{
	private $arIBlockIds;

	function __construct()
	{
		\Bitrix\Main\Loader::IncludeModule("iblock");
		$this->arIBlockIds = self::getIBlocks();
	}

	/**
	 * Get IBlock ID
	 * @param string $iblockCode IBlock CODE
	 * @return integer|null
	 */
	public static function getIBlockId($iblockCode)
	{
		if (empty($iblockCode)) {
			return false;
		}
		return self::getInstance()->getIBlockIdPr($iblockCode);
	}

	private function getIBlockIdPr($iblockCode)
	{
		if (isset($this->arIBlockIds[$iblockCode])) {
			return $this->arIBlockIds[$iblockCode];
		}

		return null;
	}

	private function getIBlocks()
	{
		$arResult = array();
		$obCacheExt = new NPHPCacheExt();
		$arCacheParams = func_get_args();
		$strTag = 'arIBlockIds';

		if (!$obCacheExt->InitCache(__CLASS__ . __function__, $arCacheParams, $strTag)) {
			$db = \CIBlock::GetList(
				array('ID' => 'ASC'),
				array(
					'ACTIVE' => 'Y',
				)
			);

			while ($arr = $db->Fetch()) {
				if (!empty($arr['CODE'])) {
					$arResult[$arr['CODE']] = $arr['ID'];
					$arResult[$arr['ID']] 	= $arr['ID'];
				}
			}
			$obCacheExt->StartDataCache($arResult);
		} else {
			$arResult = $obCacheExt->GetVars();
		}

		return $arResult;
	}

	/**
	 * Clear service cache
	 */
	private static function ClearIBlocksCache()
	{
		global $CACHE_MANAGER;
		$CACHE_MANAGER->ClearByTag("arIBlockIds");
		return true;
	}

	/*
	* Обработка событий изменения инфоблока
	*/
	public static function AddIBlocksEvent(&$arFields)
	{
		self::ClearIBlocksCache();
		if (!empty($arFields['SKIP']) and $arFields['SKIP'] == 'Y') {
			return true;
		}

		self::UpdateIBlocksEventAfter($arFields);
		self::UpdateIBlocksEventBefore($arFields);
	}

	public static function UpdateIBlocksEventBefore(&$arFields)
	{
		if (!empty($arFields['SKIP']) and $arFields['SKIP'] == 'Y') {
			return true;
		}

		if (!empty($arFields['CODE'])) {
			$arIbCodes = self::getIBlocks();
			if (!empty($arIbCodes[$arFields['CODE']]) and $arIbCodes[$arFields['CODE']] != $arFields['ID']) {
				global $APPLICATION;
				$APPLICATION->throwException("Инфоблок с символьным кодом (" . $arFields["CODE"] . ") уже существует.");
				return false;
			}
		}
	}

	public static function UpdateIBlocksEventAfter(&$arFields)
	{
		self::ClearIBlocksCache();

		if (!empty($arFields['SKIP']) and $arFields['SKIP'] == 'Y') {
			return true;
		}
		if (!empty($arFields['NAME'])) {
			$arFields['CODE'] = trim($arFields['CODE']);
			if (intval($arFields['ID']) > 0 and empty($arFields['CODE'])) {
				$strCode = \Cutil::translit($arFields['NAME'] . '_' . $arFields['ID'], "ru");
				$ib = new \CIBlock;
				$res = $ib->Update($arFields['ID'], array('CODE' => $strCode, 'SKIP' => 'Y'));
			}
		}
	}

	public static function DeleteIBlocksEvent($arFields)
	{
		self::ClearIBlocksCache();
	}


	/*
	* @method getYesValueProperty - метод, для получения ID значения свойства типа СПИСОК. Которое несет функциональную нагрузку переключателя ДА\НЕТ
	* @param integer $IBLOCK_ID ID Инфоблока
	* @param string|integer $CODE_ID Числовой или мнемонический код свойства
	*/
	public static function getYesValueProperty($IBLOCK_ID, $CODE_ID)
	{
		return self::getInstance()->getYesValuePropertyPr($IBLOCK_ID, $CODE_ID);
	}

	private function getYesValuePropertyPr($IBLOCK_ID, $CODE_ID)
	{

		if (empty($IBLOCK_ID)) {
			$GLOBALS['APPLICATION']->ThrowException('$IBLOCK_ID empty.');
			return false;
		}

		if (empty($CODE_ID)) {
			$GLOBALS['APPLICATION']->ThrowException('$CODE_ID Empty.');
			return false;
		}

		$enumID = false;

		$obCacheExt = new NPHPCacheExt();
		$arCacheParams = func_get_args();
		$arCacheParams['iblock_id'] = $IBLOCK_ID;

		if (!$obCacheExt->InitCache(__CLASS__ . __function__, $arCacheParams)) {
			$arFilter = Array(
				"IBLOCK_ID" => $IBLOCK_ID,
			);

			if (intval($CODE_ID) > 0 and intval($CODE_ID) == $CODE_ID) {
				$arFilter['PROPERTY_ID'] = $CODE_ID;
			} else {
				$arFilter['CODE'] = $CODE_ID;
			}

			$property_enums = \CIBlockPropertyEnum::GetList(Array(), $arFilter);
			if ($enum_fields = $property_enums->GetNext()) {
				$enumID = $enum_fields['ID'];
			}
			/*else
			{
			return false;
			} */

			$obCacheExt->StartDataCache($enumID);
		} else {
			$enumID = $obCacheExt->GetVars();
		}

		return $enumID;
	}


	/**
	 * Get IBlock property ID
	 * @param string $iblockCode IBlock CODE
	 * @param string $propCode Property CODE
	 * @return integer|null
	 */
	public static function GetPropertyId($iblockCode, $propCode)
	{
		return self::getInstance()->GetPropertyIdPr($iblockCode, $propCode);
	}

	private function GetPropertyIdPr($iblockCode, $propCode)
	{
		$iblockId = $this->getIBlockId($iblockCode);
		if (!$iblockId) {
			return null;
		}

		// @todo: обернуть в кэширование
		$rsProp = \CIBlockProperty::GetList(
			$arOrder = array(),
			$arFilter = array(
				'IBLOCK_ID' => $iblockId,
				'CODE' => $propCode
			)
		);
		if ($arProp = $rsProp->getNext())
		{
			return $arProp['ID'];
		}

		return null;
	}

	/**
	 * Get IBlock property enum value ID
	 * @param string $iblockCode IBlock CODE
	 * @param string $propCode Property CODE
	 * @param string $xmlId Property value XML_ID
	 * @return integer|null
	 */
	public static function GetPropertyEnumValueId($iblockCode, $propCode, $xmlId){
		return self::getInstance()->GetPropertyEnumValueIdPr($iblockCode, $propCode, $xmlId);
	}

	private function GetPropertyEnumValueIdPr($iblockCode, $propCode, $xmlId)
	{
		$propId = $this->GetPropertyId($iblockCode, $propCode);
		if (!$propId) {
			return null;
		}

		$rsEnum = \CIBlockPropertyEnum::GetList(
			array("SORT"=>"ASC", "VALUE"=>"ASC"),
			array(
				'PROPERTY_ID' => $propId,
				'XML_ID' => $xmlId
			)
		);
		if ($arEnum = $rsEnum->GetNext())
		{
			return $arEnum['ID'];
		}
		return null;
	}

	/**
	 * Get IBlock property enum value XML_ID
	 * @param string $iblockCode IBlock CODE / ID
	 * @param string $propCode Property CODE
	 * @param string $valueId Property value ID
	 * @return integer|null
	 */
	public static function GetPropertyEnumXmlId($iblockCode, $propCode, $valueId){
		return self::getInstance()->GetPropertyEnumXmlIdPr($iblockCode, $propCode, $valueId);
	}

	private function GetPropertyEnumXmlIdPr($iblockCode, $propCode, $valueId)
	{
		$propId = $this->GetPropertyId($iblockCode, $propCode);
		if (!$propId) {
			return null;
		}

		$rsEnum = \CIBlockPropertyEnum::GetList(
			array("SORT"=>"ASC", "VALUE"=>"ASC"),
			array(
				'PROPERTY_ID' => $propId,
				'ID' => $valueId
			)
		);
		if ($arEnum = $rsEnum->GetNext())
		{
			return $arEnum['XML_ID'];
		}
		return null;
	}
}
?>