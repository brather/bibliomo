<?php
namespace Evk\Books;
use Bitrix\Main\Loader;
Loader::includeModule("iblock");
class SearchQueryBooks implements \IteratorAggregate
{
	public function getIterator()
	{
		return new \ArrayIterator($this);
	}
	public function GetDataForSmartFilter()
	{

		$cache_id = "CACHE_ID";
		$cache_path = "/".SITE_ID."/neb/search.page.smart/";
		$cache_time = 3600000000;
		$PROPS_ITEMS = array();
		$phpCache = new \CPHPCache();
		if($phpCache->InitCache($cache_time, $cache_id, $cache_path))
		{
			extract($phpCache->GetVars());
		}
		else
		{
			if($phpCache->StartDataCache($cache_time, $cache_id, $cache_path))
			{
				//$this->FillPropsForSmartFilter();
				//$PROPS_ITEMS = $this->arResult["PROPS_ITEMS"];
				global $CACHE_MANAGER;
				$CACHE_MANAGER->StartTagCache($cache_path);
				$CACHE_MANAGER->RegisterTag("iblock_id_library_search_page_smart");
				$CACHE_MANAGER->EndTagCache();
				$phpCache->EndDataCache(array(
					"PROPS_ITEMS" => $PROPS_ITEMS,
				));
			}
		}
		return $PROPS_ITEMS;
	}

}