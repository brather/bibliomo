<?
namespace Evk\Books\Stat;

use Evk\Books\Tables\StatBookDownloadTable;

class BookDownload extends \Evk\Books\Singleton
{
	private $BOOK_ID;
	private $UID = 0;

	public function __construct()
	{
		global $USER;
		if(is_object($USER))
			$this->UID = $USER->GetID();
	}

	/**
	 * @param String $str
	 */
	public static function Query($str)
	{
		/**
		 * @var \CDatabase $DB
		 */
		$arResult = array();
		global $DB;
		if($rsData = $DB->Query($str, true))
		{
			while($arData = $rsData->Fetch())
			{
				$arResult[] = $arData;
			}
			return $arResult;
		}
		return null;
	}

	public static function set($BOOK_ID)
	{
		return self::getInstance()->setBookDownloadPr($BOOK_ID);
	}

	private function setBookDownloadPr($BOOK_ID)
	{
		if(empty($BOOK_ID))
			return false;
		$this->BOOK_ID = intval($BOOK_ID);
        self::add();
	}

	private function add()
	{
		if(empty($this->BOOK_ID))
			return false;
		$arFields = array(
			'BOOK_ID' => $this->BOOK_ID,
			'UID' => $this->UID,
		);
		StatBookDownloadTable::add($arFields);
	}
}
?>