<?
namespace Evk\Books\Stat;

use Evk\Books\Tables\StatBookReadTable;

class BookRead extends \Evk\Books\Singleton
{
	private $BOOK_ID;
	private $UID = 0;
	private $sessidUID = '';

	public function __construct()
	{
		self::setCookieUid();
	}
	public static function GetList($arQuery=array())
	{
		if($obResult = StatBookReadTable::getList($arQuery))
			return $obResult->fetchAll();
		return null;
	}
	/**
	 * @param String $str
	 */
	public static function Query($str)
	{
		/**
		 * @var \CDatabase $DB
		 */
		$arResult = array();
		global $DB;
		if($rsData = $DB->Query($str, true))
		{
			while($arData = $rsData->Fetch())
			{
				$arResult[] = $arData;
			}
			return $arResult;
		}
		return null;
	}

	public static function set($BOOK_ID){
		return self::getInstance()->setBookReadPr($BOOK_ID);
	}

	private function setBookReadPr($BOOK_ID){
		if(empty($BOOK_ID))
			return false;

		$this->BOOK_ID = $BOOK_ID;

        self::add();
	}

	private function add(){
		if(empty($this->BOOK_ID) or empty($this->sessidUID))
			return false;

		$arFields = array(
			'BOOK_ID' => $this->BOOK_ID,
			'UID' => $this->UID,
		);

        //такая запись существует?
        $row = StatBookReadTable::getRow(Array(
            'filter' => $arFields
        ));

        if(!$row){
            $arFields['UID_GUEST'] = $this->sessidUID;
            StatBookReadTable::add($arFields);
        }
	}

	private function setCookieUid(){
		$sessidUID = $GLOBALS['APPLICATION']->get_cookie("sessidUID");
		if(empty($sessidUID))
		{
			$sessidUID = bitrix_sessid();
			$GLOBALS['APPLICATION']->set_cookie("sessidUID", $sessidUID);
		}

		$this->sessidUID = $sessidUID;
		if(intval($_SESSION['SESS_AUTH']['USER_ID']))
			$this->UID = (int)$_SESSION['SESS_AUTH']['USER_ID'];

	}
}
?>