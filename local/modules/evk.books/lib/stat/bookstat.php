<?
namespace Evk\Books\Stat;
use Bitrix\Main\Entity;
use Evk\Books\Books;
use Evk\Books\Collections;
use Evk\Books\nebUser;

/**
 * Class BookStat
 * @package Evk\Books\Stat
 */
class BookStat
{
	const PERIOD_DAY = "DAY";
	const PERIOD_WEEK = "WEEK";
	const PERIOD_MONTH = "MONTH";
	const PERIOD_YEAR = "YEAR";
	public static $timeFormat = "YYYY-MM-DD HH:MI:SS";
	public static $dbTimeFormat = "Y-m-d H:i:s";
	public static $timePeriod = 86400;
	public static function GetPeriod($from, $to)
	{
		$ost = ($to-$from)%self::$timePeriod;
		$days = ($to-$from-$ost)/self::$timePeriod;
		if($days <= 28)
			$period = self::PERIOD_DAY;
		elseif($days <= 100)
			$period = self::PERIOD_WEEK;
		elseif($days <= 730)
			$period = self::PERIOD_MONTH;
		else
			$period = self::PERIOD_YEAR;
		return compact("days", "period");
	}
	/**
	 * @param $componentName
	 * @param int $libID
	 * @param int $from
	 * @param int $to
	 * @param bool $withFile
	 * @param int $interval
	 * @return array
	 */
	public static function GetStatFromBX($componentName, $libID=0, $from=0, $to=0, $withFile=false, $period=array(), $export=false)
	{
		$arDates = self::getDaysWithPeriod($from, $to, $period);
		$from = $arDates[count($arDates)-1]["startTM"];
		$to = $arDates[0]["endTM"];
		$cache = new \CPHPCache();
		$cache_id = md5("neb_library_stat_".$libID." ".$withFile." ".$from." ".$to);
		$cache_dir = "/".SITE_ID."/".$componentName;
		$cache_time = 3600000000;
		if($cache->InitCache($cache_time, $cache_id, $cache_dir))
		{
			$vars = $cache->GetVars();
			extract($vars);
		}
		else
		{
			if($cache->StartDataCache($cache_time, $cache_id, $cache_dir))
			{
				$arResult = array();
				$search = new \Evk\Books\SearchQuery();
				$arFilter = array();
				$arFilter{"PROPERTY_LIBRARIES"} = array($libID);
				if($withFile)
					$arFilter{"!PROPERTY_FILE"} = false;
				else
					$arFilter{"PROPERTY_FILE"} = false;
				$arFilter{"ACTIVE"} = "Y";
				$arFilter{"DATE_ACTIVE"} = "Y";
				if($to)
					$arFilter{"<=DATE_CREATE"} = ConvertTimeStamp($to, self::$timeFormat);
				$search1 = new \Evk\Books\SearchQuery();
				$arRes = $search1->FindBooksSimple(array("active"=>"asc"), $arFilter, array("ACTIVE"));
				if(is_array($arRes))
				{
					$countAll = intval($arRes[0]["CNT"]);
					if($from)
						$arFilter{">=DATE_CREATE"} = ConvertTimeStamp($from, self::$timeFormat);
					$arRes = $search->FindBooksSimple(array("created_date"=>"desc"), $arFilter, array("CREATED_DATE"));
					if(is_array($arRes))
					{
						foreach($arRes as &$row)
						{
							$row["DATE"] = str_replace(".", "-", $row["CREATED_DATE"]);
							unset($row["CREATED_DATE"]);
						}
						$arDate = self::mergeResultDates($arRes, $arDates, $countAll, !$export);
					}
					$arResult = array(
						"countAll" => $countAll,
						"stat" => array("dates" => $arDate["dates"], "values" => $arDate["values"]),
					);
				}
				else
				{
					$arDate = self::mergeResultDates(array(), $arDates, 0, !$export);
					return array(
						"countAll" => 0,
						"stat" => array("dates" => $arDate["dates"], "values" => $arDate["values"]),
					);
				}
				if(is_array($arResult) && count($arResult))
				{
					global $CACHE_MANAGER;
					$CACHE_MANAGER->StartTagCache($cache_dir);
					$CACHE_MANAGER->RegisterTag("neb_library_stat_books");
					$CACHE_MANAGER->EndTagCache();
					$cache->EndDataCache(compact("arResult"));
				}
				else
					$cache->AbortDataCache();
			}
		}
		return $arResult;
	}
	protected static function getDaysWithPeriod($from, $to, $period)
	{
		$arDates = array();
		$currentTime = time();
		$i=0;
		switch($period["period"])
		{
			case self::PERIOD_DAY:
			{
				do
				{
					$start = strtotime("midnight", $from);
					$end = strtotime(sprintf("+%d second", self::$timePeriod-1), $from);
					$arDates[] = array(
						"startTM" => $start,
						"endTM" => $end,
						"start" => date("Y-m-d H:i:s", $start),
						"end" => date("Y-m-d H:i:s", $end),
						"periodStr" => $period["period"],
						"title" => FormatDate("M d", $start), //date("d.m.y", $start),
						"count" => 0,
					);
					$from = strtotime("next day", $start);
					if($from > $currentTime)
						break;
					if(++$i > 200) break;
				} while($from < $to);
				break;
			}
			case self::PERIOD_WEEK:
			{
				do
				{
					if(date("D", $from) != "Mon")
						$start = strtotime("previous mon", $from);
					else
						$start = strtotime("midnight", $from);
					$end = strtotime(sprintf("next sun +%d second", self::$timePeriod-1), $start);
					$arDates[] = array(
						"startTM" => $start,
						"endTM" => $end,
						"start" => date("Y-m-d H:i:s", $start),
						"end" => date("Y-m-d H:i:s", $end),
						"periodStr" => $period["period"],
						"title" => FormatDate("M d", $start)." - ".FormatDate("M d", $end), //date("d.m.y", $start),
						"count" => 0,
					);
					$from = strtotime("next week", $start);
					if($from > $currentTime)
						break;
					if(++$i > 200) break;
				} while($from < $to);
				break;
			}
			case self::PERIOD_MONTH:
			{
				do
				{
					$start = strtotime("first day of this month", $from);
					$end = strtotime(sprintf("last day of this month +%d second", self::$timePeriod-1), $from);
					$arDates[] = array(
						"startTM" => $start,
						"endTM" => $end,
						"start" => date("Y-m-d H:i:s", $start),
						"end" => date("Y-m-d H:i:s", $end),
						"periodStr" => $period["period"],
						"title" => FormatDate("M Y", $start),
						"count" => 0,
					);
					$from = strtotime("next month", $start);
					if($from > $currentTime)
						break;
					if(++$i > 200) break;
				} while($from < $to);
				break;
			}
			case self::PERIOD_YEAR:
			{
				do
				{
					$start = strtotime("first day of this year", $from);
					$end = strtotime(sprintf("this year +11 month last day of this month +%d second", self::$timePeriod-1), $start);
					$arDates[] = array(
						"startTM" => $start,
						"endTM" => $end,
						"start" => date("Y-m-d H:i:s", $start),
						"end" => date("Y-m-d H:i:s", $end),
						"periodStr" => $period["period"],
						"title" => date("Y", $start),
						"count" => 0,
					);
					$from = strtotime("next year", $start);
					if($from > $currentTime)
						break;
					if(++$i > 200) break;
				} while($from < $to);
				break;
			}
		}
		return array_reverse($arDates);
	}
	public static function GetStatFromDB($table, $libID=0, $from=0, $to=0, $period=array(), $export=false)
	{
		$arDates = self::getDaysWithPeriod($from, $to, $period);
		$from = $arDates[count($arDates)-1]["startTM"];
		$to = $arDates[0]["endTM"];
		$sqlAll = sprintf(
			'
			SELECT
			  COUNT(%2$s) AS CNT
			FROM
			  %1$s
			WHERE
			  (1=1)
			  %3$s
			  %4$s',
			$table,
			"X_TIMESTAMP",
			(($to>0)?sprintf('AND X_TIMESTAMP<="%s"', date(self::$dbTimeFormat, $to)):''),
			(($libID>0)?sprintf('AND LIB_ID=%d', $libID):'')
		);
		if($arRes = self::query($sqlAll))
		{
			$countAll = intval($arRes[0]["CNT"]);
			$sql = sprintf(
				'
				SELECT
				  DATE_FORMAT(%2$s, "%%Y-%%m-%%d") AS DATE,
				  COUNT(%2$s) AS CNT
				FROM
				  %1$s
				WHERE
				  (1=1)
				  %3$s
				  %4$s
				  %5$s
				GROUP BY %6$s(%2$s)
				ORDER BY %2$s DESC',
				$table,
				"X_TIMESTAMP",
				(($from>0)?sprintf('AND X_TIMESTAMP>="%s"', date(self::$dbTimeFormat, $from)):''),
				(($to>0)?sprintf('AND X_TIMESTAMP<="%s"', date(self::$dbTimeFormat, $to)):''),
				(($libID>0)?sprintf('AND LIB_ID=%d', $libID):''),
				$period["period"]
			);
			$arRes = self::query($sql);
			$arDate = self::mergeResultDates($arRes, $arDates, $countAll, false);
			return array(
				"countAll" => $countAll,
				"stat" => array("dates" => $arDate["dates"], "values" => $arDate["values"]),
			);
		}
		else
		{
			$arDate = self::mergeResultDates(array(), $arDates, 0, false);
			return array(
				"countAll" => 0,
				"stat" => array("dates" => $arDate["dates"], "values" => $arDate["values"]),
			);
		}
	}

	protected static function mergeResultDates($arRes, $arDates, $countAll, $upTime=false)
	{
		$dates = $values = array();
		if($upTime)
		{
			foreach($arDates as &$arDate)
			{
				$dates[] = $arDate["title"];
				$arDate["count"] = $countAll;
				$values[] = &$arDate["count"];
			}
			unset($arDate);
			$minus = 0;
			$i=0;
			foreach($arDates as &$arDate)
			{
				foreach($arRes as $row)
				{
					$time = MakeTimeStamp($row["DATE"], "YYYY-MM-DD");
					if($time >= $arDate["startTM"] && $time <= $arDate["endTM"])
					{
						$minus += $row["CNT"];
					}
				}
				if(++$i > 1)
					$arDate["count"] -= $minus;
			}
		}
		else
		{
			foreach($arDates as &$arDate)
			{
				$dates[] = $arDate["title"];
				$values[] = &$arDate["count"];
			}
			unset($arDate);
			foreach($arDates as &$arDate)
			{
				foreach($arRes as $row)
				{
					$time = MakeTimeStamp($row["DATE"], "YYYY-MM-DD");
					if($time >= $arDate["startTM"] && $time <= $arDate["endTM"])
					{
						$arDate["count"] += $row["CNT"];
					}
				}
			}
		}
		$dates = array_reverse($dates);
		$values = array_reverse($values);
		return compact("dates", "values");
	}
	protected function query($sql)
	{
		/**
		 * @var \CDatabase $DB
		 */
		$arResult = array();
		global $DB;
		if($rsData = $DB->Query($sql, true))
		{
			while($arData = $rsData->Fetch())
			{
				$arResult[] = $arData;
			}
			return $arResult;
		}
		return null;
	}
	protected static function checkDatesForDaysInterval($arDate, $date1)
	{
		$date1 = MakeTimeStamp($date1, "YYYY-MM-DD");
		return self::getIndexFromIntervalDiapason($date1, $arDate);
	}
	protected static function getIndexFromIntervalDiapason($timestamp, $arDate)
	{
		for($i=0;$i<count($arDate);$i++)
		{
			if($timestamp <= $arDate[$i]["TM"])
			{
				return $i;
			}
		}
		return $i;
	}
	public static function CountBooksForLibrary($libID=0, $withFile=null)
	{
		if($libID <= 0) return 0;
		$arFilter = array();
		$arFilter{"PROPERTY_LIBRARIES"} = array($libID);
		if(!is_null($withFile))
		{
			if($withFile)
				$arFilter{"!PROPERTY_FILE"} = false;
			else
				$arFilter{"PROPERTY_FILE"} = false;
		}
		$arFilter{"ACTIVE"} = "Y";
		$arFilter{"DATE_ACTIVE"} = "Y";
		$search = new \Evk\Books\SearchQuery();
		$arRes = $search->FindBooksSimple(array("active"=>"asc"), $arFilter, array("ACTIVE"));
		return $arRes[0]["CNT"];
	}
	public static function CountBooksViewsForLibrary($libID=0)
	{
		if($libID <= 0) return 0;
		$rsViews = BookView::GetList(array(
			"select" => array("CNT"),
			"filter" => array("ID" => $libID),
			"runtime" => array(
				new Entity\ExpressionField("CNT", "count(*)"),
			),
		));
		$arResult = $rsViews->fetchAll();
		return $arResult[0]["CNT"];
	}
	public static function CountUsersForLibrary($libID=0)
	{
		if($libID <= 0) return 0;
		$by="ID";
		$order = "ASC";
		$arSkipUID = array();
		$rsUsers = \nebUser::GetList($by, $order, array("UF_LIBRARY" => $libID, 'ACTIVE' => 'Y', 'GROUPS_ID' => array(6, 7, 8, 9)), array('FIELDS' => array('ID')));
		while($arUser = $rsUsers->Fetch())
			$arSkipUID[] = $arUser['ID'];
		$rsUsers = \nebUser::GetList($by, $order, array(
			"UF_LIBRARY" => $libID,
			"ACTIVE" => "Y",
		), array(
			"FIELDS" => array("ID")
		));
		$i=0;
		while($arUser = $rsUsers->Fetch())
		{
			if(in_array($arUser['ID'], $arSkipUID))
				continue;
			++$i;
		}
		return $i;
	}
	public static function CountCollectionsForLibrary($libID=0)
	{
		if($libID <= 0) return 0;

		$result = \CIBlockSection::GetList(Array(), Array(
			'IBLOCK_ID' =>IBLOCK_ID_COLLECTION,
			"ACTIVE" => "Y",
			"GLOBAL_ACTIVE" => "Y",
			"UF_LIBRARY" => $libID,
			"CNT_ACTIVE" => "Y",
		), true, array(
			'UF_LIBRARY'
		));
		$arResult = array();
		$count = 0;
		while($arRes = $result->Fetch())
		{
			if(intval($arRes["ELEMENT_CNT"]))
				$count++;
			$arResult[] = $arRes;
		}

		return array(
			"count" => $count,
			'countAll' => count($arResult),
		);
	}
}
?>