<?
namespace Evk\Books\Stat;
use Evk\Books\Tables\StatBookViewTable;
use Evk\Books\Tables\BooksInfoTable;

/**
 * Class BookView
 * @package Evk\Books\Stat
 */
class BookView extends \Evk\Books\Singleton
{
	private $BOOK_ID;
	private $LIB_ID;
	private $UID = 0;
	private $sessidUID = '';

	/**
	 * @param String $str
	 */
	public static function Query($str)
	{
		/**
		 * @var \CDatabase $DB
		 */
		$arResult = array();
		global $DB;
		if($rsData = $DB->Query($str, true))
		{
			while($arData = $rsData->Fetch())
			{
				$arResult[] = $arData;
			}
			return $arResult;
		}
		return null;
	}
	public function __construct()
	{
		self::setCookieUid();
	}

	public static function set($BOOK_ID, $LIB_ID)
	{
		return self::getInstance()->setBookViewPr($BOOK_ID, $LIB_ID);
	}

	private function setBookViewPr($BOOK_ID, $LIB_ID)
	{
		if(empty($BOOK_ID))
			return false;

		$this->BOOK_ID = $BOOK_ID;

		$this->LIB_ID = $LIB_ID;

		$arViewsBook = explode("|", $GLOBALS['USER']->GetParam("ViewsBook"));
		if(empty($arViewsBook))
		{
			$GLOBALS['USER']->SetParam("ViewsBook", $BOOK_ID);
			self::add();
		}
		elseif(!in_array($BOOK_ID, $arViewsBook))
		{
			$arViewsBook[] = $BOOK_ID;
			$GLOBALS['USER']->SetParam("ViewsBook", join("|", $arViewsBook));
			self::add();
		}
	}

	private function add()
	{
		if(empty($this->BOOK_ID) or empty($this->sessidUID))
			return false;
		global $DB;

		$DB->StartUsingMasterOnly();
		$arFields = array(
			'BOOK_ID' => $this->BOOK_ID,
			'LIB_ID' => $this->LIB_ID,
			'UID' => $this->UID,
			'UID_GUEST' => $this->sessidUID
		);
		$rsRes = BooksInfoTable::getList(array(
			'select' 	=> array('ID', 'VIEWS', 'LIB_ID'),
			'filter' => array('=BOOK_ID' => $this->BOOK_ID)
		));
		$arViews = $rsRes->fetchAll();
		foreach($arViews as $arView)
		{
			if(is_numeric($this->LIB_ID))
			{
				BooksInfoTable::update($arView['ID'], array('VIEWS' => ($arView['VIEWS'] + 1)));
			}
			elseif(is_array($this->LIB_ID))
			{
				if(in_array($arView["LIB_ID"], $this->LIB_ID))
				{
					BooksInfoTable::update($arView['ID'], array('VIEWS' => ($arView['VIEWS'] + 1)));
				}
				else
				{
					BooksInfoTable::add(array('BOOK_ID' => $this->BOOK_ID, 'LIB_ID' => $arView["LIB_ID"], 'VIEWS' => 1));
				}
			}
		}
		if(empty($arViews))
		{
			if(is_numeric($this->LIB_ID))
			{
				BooksInfoTable::add(array('BOOK_ID' => $this->BOOK_ID, 'LIB_ID' => $this->LIB_ID, 'VIEWS' => 1));
				StatBookViewTable::add($arFields);
			}
			elseif(is_array($this->LIB_ID))
			{
				foreach($this->LIB_ID as $LIB)
				{
					BooksInfoTable::add(array('BOOK_ID' => $this->BOOK_ID, 'LIB_ID' => $LIB, 'VIEWS' => 1));
					$arFields["LIB_ID"] = $LIB;
					StatBookViewTable::add($arFields);
				}
			}
		}
		$DB->StopUsingMasterOnly();
	}

	private function setCookieUid(){
		$sessidUID = $GLOBALS['APPLICATION']->get_cookie("sessidUID");
		if(empty($sessidUID))
		{
			$sessidUID = bitrix_sessid();
			$GLOBALS['APPLICATION']->set_cookie("sessidUID", $sessidUID);
		}

		$this->sessidUID = $sessidUID;
		if(intval($_SESSION['SESS_AUTH']['USER_ID']))
			$this->UID = (int)$_SESSION['SESS_AUTH']['USER_ID'];

	}
	public static function GetList($arFilter=array())
	{
		return StatBookViewTable::getList($arFilter);
	}
}
?>