<?
	namespace Evk\Books;
	abstract class Singleton 
	{
		private static $instances = array();
		public function __construct() 
		{
			$class = get_called_class();
			if (array_key_exists($class, self::$instances))
				trigger_error("Tried to construct  a second instance of class \"$class\"", E_USER_WARNING);
		}

		/**
		 * @return singleton
		 */
		public static function getInstance() 
		{
			$class = get_called_class();
			if (array_key_exists($class, self::$instances) === false)
				self::$instances[$class] = new $class();
			return self::$instances[$class];
		}
	} 
?>