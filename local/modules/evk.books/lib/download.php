<?php
namespace Evk\Books;

class Download
{
	public static function DownloadImportLogFileFromTime($time)
	{
		global $APPLICATION;
		$DIR = $_SERVER["DOCUMENT_ROOT"]."/upload/import_log";
		if(!is_numeric($time))
			$time = strtotime($time);
		if(is_numeric($time))
			$logTime = date('d.m.Y_H.i.s', $time);
		$fileName = sprintf('importLog_%s.txt', $logTime);
		$fileFullName = sprintf('%s/%s', $DIR, $fileName);
		if(file_exists($fileFullName))
		{
			header('Content-Type: text/plain');
			$contentDisposition = sprintf('Content-Disposition: attachment; filename="%s"', $fileName);
			header($contentDisposition);
			$APPLICATION->RestartBuffer();
			echo file_get_contents($fileName);
			exit;
		}
	}
	public static function DownloadImportLogFile($fileName)
	{
		global $APPLICATION;
		$DIR = $_SERVER["DOCUMENT_ROOT"]."/upload/import_log";
		$fileFullName = sprintf('%s/%s', $DIR, $fileName);
		if(file_exists($fileFullName))
		{
			header('Content-Type: text/plain');
			$contentDisposition = sprintf('Content-Disposition: attachment; filename="%s"', $fileName);
			header($contentDisposition);
			$APPLICATION->RestartBuffer();
			echo file_get_contents($fileName);
			exit;
		}
	}
}