<?php
	use Bitrix\Main\Localization\Loc;
	Loc::loadMessages(__FILE__);

	if (class_exists('evk_books'))
	{
		return;
	}

	class evk_books extends CModule
	{
		public $MODULE_ID = 'evk.books';
		public $MODULE_VERSION = '0.0.1';
		public $MODULE_VERSION_DATE = '2015-05-13 18:46:00';
		public $MODULE_NAME = 'Books module';
		public $MODULE_DESCRIPTION = 'Модуль для работы с книгами и не только';
		public $MODULE_GROUP_RIGHTS = 'N';
		public $PARTNER_NAME = "evk";
		public $PARTNER_URI = "#";

		public function DoInstall()
		{
			RegisterModule($this->MODULE_ID);
			return true;
		}

		public function InstallDB()
		{
			return true;
		}

		public function InstallFiles($arParams = array())
		{
			return true;
		}

		public function DoUninstall()
		{
			UnRegisterModule($this->MODULE_ID);
			return true;
		}

		public function UnInstallDB(array $arParams = array())
		{
			return true;
		}

		public function UnInstallFiles(array $arParams = array())
		{
			return true;
		}
	}

?>