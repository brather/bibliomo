<?
$MESS["BIBLIO_CARD_ENTITY_ID_FIELD"] = "";
$MESS["BIBLIO_CARD_ENTITY_ALIS_FIELD"] = "";
$MESS["BIBLIO_CARD_ENTITY_IDFROMALIS_FIELD"] = "";
$MESS["BIBLIO_CARD_ENTITY_AUTHOR_FIELD"] = "";
$MESS["BIBLIO_CARD_ENTITY_NAME_FIELD"] = "";
$MESS["BIBLIO_CARD_ENTITY_SUBNAME_FIELD"] = "";
$MESS["BIBLIO_CARD_ENTITY_PUBLISHYEAR_FIELD"] = "";
$MESS["BIBLIO_CARD_ENTITY_PUBLISHPLACE_FIELD"] = "";
$MESS["BIBLIO_CARD_ENTITY_PUBLISHER_FIELD"] = "";
$MESS["BIBLIO_CARD_ENTITY_ISBN_FIELD"] = "";
$MESS["BIBLIO_CARD_ENTITY_LANGUAGE_FIELD"] = "";
$MESS["BIBLIO_CARD_ENTITY_COUNTPAGES_FIELD"] = "";
$MESS["BIBLIO_CARD_ENTITY_PRINTING_FIELD"] = "";
$MESS["BIBLIO_CARD_ENTITY_FORMAT_FIELD"] = "";
$MESS["BIBLIO_CARD_ENTITY_SERIES_FIELD"] = "";
$MESS["BIBLIO_CARD_ENTITY_VOLUMENUMBER_FIELD"] = "";
$MESS["BIBLIO_CARD_ENTITY_VOLUMENAME_FIELD"] = "";
$MESS["BIBLIO_CARD_ENTITY_COLLECTION_FIELD"] = "";
$MESS["BIBLIO_CARD_ENTITY_ACCESSTYPE_FIELD"] = "";
$MESS["BIBLIO_CARD_ENTITY_ISLICENSEAGREEMENT_FIELD"] = "";
$MESS["BIBLIO_CARD_ENTITY_RESPONSIBILITY_FIELD"] = "";
$MESS["BIBLIO_CARD_ENTITY_ANNOTATION_FIELD"] = "";
$MESS["BIBLIO_CARD_ENTITY_CONTENTREMARK_FIELD"] = "";
$MESS["BIBLIO_CARD_ENTITY_COMMONREMARK_FIELD"] = "";
$MESS["BIBLIO_CARD_ENTITY_PUBLICATIONINFORMATION_FIELD"] = "";
$MESS["BIBLIO_CARD_ENTITY_PDFLINK_FIELD"] = "";
$MESS["BIBLIO_CARD_ENTITY_IDPARENT_FIELD"] = "";
$MESS["BIBLIO_CARD_ENTITY_CREATIONMETHOD_FIELD"] = "";
$MESS["BIBLIO_CARD_ENTITY_CREATIONACTOR_FIELD"] = "";
$MESS["BIBLIO_CARD_ENTITY_CREATIONDATETIME_FIELD"] = "";
$MESS["BIBLIO_CARD_ENTITY_UPDATINGACTOR_FIELD"] = "";
$MESS["BIBLIO_CARD_ENTITY_UPDATINGDATETIME_FIELD"] = "";
$MESS["BIBLIO_CARD_ENTITY_BBKTEXT_FIELD"] = "";
$MESS["BIBLIO_CARD_ENTITY_UDKTEXT_FIELD"] = "";
$MESS["BIBLIO_CARD_ENTITY_LANGUAGETEXT_FIELD"] = "";
$MESS["BIBLIO_CARD_ENTITY_UDKTHEMATICSCODESEQUENCES_FIELD"] = "";
$MESS["BIBLIO_CARD_ENTITY_IDRUBRICS_FIELD"] = "";
$MESS["BIBLIO_CARD_ENTITY_INDEXES_FIELD"] = "";
$MESS["BIBLIO_CARD_ENTITY_BX_TIMESTAMP_FIELD"] = "";
$MESS["BIBLIO_CARD_ENTITY_BX_IS_UPLOAD_FIELD"] = "";
$MESS["BIBLIO_CARD_ENTITY_BX_ALIS_FIELD"] = "";
$MESS["BIBLIO_CARD_ENTITY_BX_UID_FIELD"] = "";
$MESS["BIBLIO_CARD_ENTITY_BX_LIB_ID_FIELD"] = "";
$MESS["BIBLIO_CARD_ENTITY_BX_DELETE_FIELD"] = "";
?>