<?php

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__DIR__ . '/index.php');

if (!check_bitrix_sessid())
{
	return;
}

define('ADMIN_MODULE_NAME', 'nota.collection');

?>

<? if ($ex = $APPLICATION->GetException()): ?>
	<?= CAdminMessage::ShowMessage(
		array(
			'TYPE'    => 'ERROR',
			'MESSAGE' => GetMessage('MOD_INST_ERR'),
			'DETAILS' => $ex->GetString(),
			'HTML'    => true
		)
	) ?>
	<form action="<?= $APPLICATION->GetCurPage() ?>">
		<input type="hidden" name="lang" value="<?= LANG ?>">
		<input type="submit" name="" value="<?= GetMessage('MOD_BACK') ?>">
	</form>
<? else: ?>
	<form method="post" action="<?= $APPLICATION->GetCurPage() ?>">
		<?= bitrix_sessid_post(); ?>
		<input type="hidden" name="lang" value="<?= LANG ?>">
		<input type="hidden" name="id" value="<?= ADMIN_MODULE_NAME ?>">
		<input type="hidden" name="uninstall" value="Y">
		<input type="hidden" name="step" value="2">
		<table>
			<tr>
				<td width="40%">
					<label for="savedata">Сохранить данные:</label>
				</td>
				<td >
					<input type="checkbox" name="savedata" id="savedata" checked="checked" value="Y">
				</td>
			</tr>
		</table>
		<input type="submit" name="inst" value="Применить">
	</form>
<? endif ?>