<?
	require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_before.php';
	use Bitrix\Main\Localization\Loc;
	Loc::loadMessages(__FILE__);

	if (!CModule::IncludeModule('nota.collection')) {
		echo CAdminMessage::ShowMessage(Loc::getMessage('NOTA_COLLECTION_ACCESS_DENIED'));
		return false;
	}

	use Nota\Collection\ElementsTable;

	$sTableID = 'tbl_collection_elements_list';

	$oSort = new CAdminSorting($sTableID, "SORT", "desc");
	$arOrder = (strtoupper($by) === "ID"? array($by => $order): array($by => $order, "ID" => "ASC"));
	$lAdmin = new CAdminList($sTableID, $oSort);
	$COLLECTION_ID = intval($COLLECTION_ID);

	$arFilterFields = Array(
		"find_id",
		"find_book_id",
	);

	$lAdmin->InitFilter($arFilterFields);

	$arFilter = array(
		"ID" => $find_id,
		"BOOK_ID" => !empty($find_book_id) ? '%'.$find_book_id.'%' : '',
	);

	$arFilter['COLLECTION_ID'] = $COLLECTION_ID;

	TrimArr($arFilter);

	if($lAdmin->EditAction())
	{
		foreach($FIELDS as $ID => $arFields)
		{
			$DB->StartTransaction();
			$ID = IntVal($ID);

			if(!$lAdmin->IsUpdated($ID))
				continue;

			if(!ElementsTable::Update($ID, $arFields))
			{
				$lAdmin->AddUpdateError('Udate error', $ID);
				$DB->Rollback();
			}
			$DB->Commit();
		}
	}

	if ($arID = $lAdmin->GroupAction()) 
	{
		if ($_REQUEST['action_target'] == 'selected') 
		{
			$rsData = ElementsTable::getList(array('select' => array('ID'), 'filter' => $arFilter));
			while ($arRes = $rsData->fetch()) 
				$arID[] = $arRes['ID'];
		}

		foreach ($arID as $ID) 
		{
			if(strlen($ID)<=0)
			continue;

			switch($_REQUEST['action'])
			{
				case "delete":
					$DB->StartTransaction();
					$result = ElementsTable::delete($ID);
					if (!$result->isSuccess()) 
					{
						$DB->Rollback();
						$lAdmin->AddGroupError(
							'Ошибка' .
							' (ID = ' . $ID . ': ' . implode('<br>', $result->getErrorMessages()) . ')',
							$ID
						);
					}

					$DB->Commit();
					break;
			}
		}
	}

	if($COLLECTION_ID > 0)
	{
		$rsSect = Nota\Collection\CollectionTable::getById($COLLECTION_ID);
		if($arSect = $rsSect->Fetch()){ 

			$chain = $lAdmin->CreateChain();
			$chain->AddItem(array(
				"TEXT" => htmlspecialcharsex($arSect['NAME']),
				"LINK" => htmlspecialcharsbx('collection_elements.php?COLLECTION_ID='.$COLLECTION_ID),
			));
			$lAdmin->ShowChain($chain);
		}
	}

	$arHeader = array(
		array(
			'id'      => 'ID',
			'content' => Loc::getMessage('NOTA_COLLECTION_ID'),
			'sort'    => 'ID',
			'default' => true
		),
		array(
			'id'      => 'BOOK_ID',
			'content' => Loc::getMessage('NOTA_COLLECTION_BOOK_ID'),
			'sort'    => 'BOOK_ID',
			'default' => true
		),
		array(
			'id'      => 'SORT',
			'content' => Loc::getMessage('NOTA_COLLECTION_SORT'),
			'sort'    => 'SORT',
			'default' => true
		),	
		array(
			'id'      => 'TIMESTAMP_X',
			'content' => Loc::getMessage('NOTA_COLLECTION_TIMESTAMP_X'),
			'sort'    => 'TIMESTAMP_X',
			'default' => true
		),			
	);


	$lAdmin->AddHeaders($arHeader);

	$arSelect = $lAdmin->GetVisibleHeaderColumns();
	if(!in_array('ID', $arSelect))
		$arSelect[] = 'ID'; 

	$rsData = ElementsTable::getList(array(
		'select' 	=> $arSelect,
		'filter' 	=> $arFilter,
		'order' 	=> $arOrder
	));

	$rsData = new CAdminResult($rsData, $sTableID);
	$rsData->NavStart();
	$lAdmin->NavText($rsData->GetNavPrint(''));

	while ($arRes = $rsData->NavNext()) 
	{
		$row =& $lAdmin->AddRow($arRes['ID'], $arRes, 'collection_element_edit.php?ID='.$arRes['ID'].'&COLLECTION_ID='.$COLLECTION_ID.'&lang='.LANGUAGE_ID, GetMessage("NOTA_COLLECTION_EDIT"));

		$row->AddViewField("BOOK_ID", '<a href="collection_element_edit.php?ID='.$arRes['ID'].'&COLLECTION_ID='.$COLLECTION_ID.'&lang='.LANGUAGE_ID.'" title="'.GetMessage("NOTA_COLLECTION_EDIT").'">'.$arRes['BOOK_ID'].'</a>');
		
		$row->AddInputField('BOOK_ID', array('size' => 50));
		$row->AddInputField('SORT', array('size' => 10));

		$row->AddActions(
			array(
				array(
					'ICON'   => 'edit',
					'TEXT'   => GetMessage('NOTA_COLLECTION_EDIT'),
					'ACTION' => $lAdmin->ActionRedirect('collection_element_edit.php?ID='.$arRes['ID'].'&COLLECTION_ID='.$COLLECTION_ID.'&lang='.LANGUAGE_ID)
				),
				array(
					'ICON'   => 'delete',
					'TEXT'   => GetMessage('NOTA_COLLECTION_DELETE'),
					"ACTION" =>"if(confirm('".GetMessageJS("NOTA_COLLECTION_DELETE_CONFIRM")."')) ".$lAdmin->ActionDoGroup($arRes['ID'], "delete", 'COLLECTION_ID='.$COLLECTION_ID),
				)
			)
		);
	}


	$lAdmin->AddAdminContextMenu(
		array(
			array(
				'TEXT'  => Loc::getMessage('NOTA_COLLECTION_ADD'),
				'LINK'  => 'collection_element_edit.php?lang='.LANGUAGE_ID.'&COLLECTION_ID='.$COLLECTION_ID,
				'TITLE' => Loc::getMessage('NOTA_COLLECTION_ADD'),
				'ICON'  => 'btn_new',
			)
		)
	);

	$lAdmin->AddGroupActionTable(array('delete' => Loc::getMessage('NOTA_COLLECTION_DELETE')));

	$lAdmin->CheckListMode();
	$APPLICATION->SetTitle(Loc::getMessage('NOTA_COLLECTION_TITLE'));

	$map = ElementsTable::getMap();

	require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_after.php';
?>
<form method="GET" action="" name="find_form">
	<input type="hidden" name="filter" value="Y">
	<?
		$oFilter = new CAdminFilter(
			$sTableID."_filter",
			array(
				$map['BOOK_ID']['title'],
				"ID",
			)
		);

		$oFilter->Begin();
	?>
	<tr>
		<td><b><?=GetMessage("NOTA_COLLECTION_BOOK_ID")?></b></td>
		<td><input type="text" name="find_book_id" value="<?=htmlspecialcharsbx($find_book_id)?>" size="40"></td>
	</tr>
	<tr>
		<td>ID:</td>
		<td><input type="text" name="find_id" value="<?=htmlspecialcharsbx($find_id)?>" size="15"></td>
	</tr>

	<?
		$oFilter->Buttons(array("table_id"=>$sTableID, "url"=>$APPLICATION->GetCurPageParam(), "form"=>"find_form"));
		$oFilter->End();
	?>
</form>
<?
	$lAdmin->DisplayList();
	require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_admin.php');
?>
