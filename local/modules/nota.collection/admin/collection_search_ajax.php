<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

if (!CModule::IncludeModule('nota.exalead')) {
	 echo CAdminMessage::ShowMessage(Loc::getMessage('NOTA_COLLECTION_ACCESS_DENIED'));
	return false;
}

use Nota\Exalead\SearchQuery;
use Nota\Exalead\SearchClient;

if($_REQUEST['id'])
{	
	$query = new SearchQuery();
	$query->getById(trim($_REQUEST['id']));
	$client = new SearchClient();
	$result = $client->getResult($query);

	
	if(count($result))
	{	
		$arExalead = array(
			'TITLE' => $result['title'],
			'URL' => '/local/tools/exalead/thumbnail.php?url=' . $result['URL'] . '&source='.$result['source'].'&width=80&height=125',
			'AUTHOR' => $result['authorbook'],
		);
	}	
}?>

<?if(count($arExalead)){?>
	<?if(!empty($arExalead['TITLE'])){?>
		<p><?=$arExalead['TITLE']?></p>
	<?}?>
	<?if(!empty($arExalead['AUTHOR'])){?>
		<p><?=$arExalead['AUTHOR']?></p>
	<?}?>
	<?if(!empty($arExalead['URL'])){?>
		<img src="<?=$arExalead['URL']?>" />
	<?}?>
<?}?>
<?die();?>