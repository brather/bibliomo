<?php
	use Bitrix\Main\Localization\Loc;
	Loc::loadMessages(__FILE__);

	global $USER;
	if (!$USER->IsAdmin())
		return false; 

	$aMenu[] = array(
		'parent_menu' => 'global_menu_content',
		'sort'        => 400,
		'text'        => Loc::getMessage('NOTA_COLLECTION_ADMIN_MENU_LIBRARY'),
		'title'       => Loc::getMessage('NOTA_COLLECTION_ADMIN_MENU_LIBRARY'),
		'url'         => 'collection_library.php'.'?lang='.LANGUAGE_ID,
		'items_id'    => 'menu_collection',
		'more_url' => array(
			'collection_library.php',
			'collection_library_edit.php'
		),   
	);
	$aMenu[] = array(
		'parent_menu' => 'global_menu_content',
		'sort'        => 410,
		'text'        => Loc::getMessage('NOTA_COLLECTION_ADMIN_MENU_TITLE'),
		'title'       => Loc::getMessage('NOTA_COLLECTION_ADMIN_MENU_TITLE'),
		'url'         => 'collection.php' . '?lang=' . LANGUAGE_ID,
		'items_id'    => 'menu_collection',
		'more_url' => array(
			'collection.php',
			'collection_edit.php',
			'collection_elements.php',
			'collection_element_edit.php',
		),
		
	);


	return $aMenu;
?>