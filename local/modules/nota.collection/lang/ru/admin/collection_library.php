<?
	$MESS["NOTA_COLLECTION_INDEX"] = 'Библиотеки';
	$MESS["NOTA_COLLECTION_ADD"] = 'Добавить библиотеку';
	$MESS["NOTA_COLLECTION_NAME"] = 'Название';
	$MESS["NOTA_COLLECTION_ID"] = 'ID библотеки';
	$MESS["NOTA_COLLECTION_DELETE_CONFIRM"] = 'Вы действительно хотите удалить библиотеку?';
	$MESS["NOTA_COLLECTION_ACCESS_DENIED"] = 'Доступ закрыт';
	$MESS["NOTA_COLLECTION_TIMESTAMP_X"] = 'Дата изм.';
	$MESS["NOTA_COLLECTION_SORT"] = 'Сортировка';
?>