<?
	$MESS["NOTA_COLLECTION_EDIT"] = 'Редактирование книги';
	$MESS["NOTA_COLLECTION_NEW"] = 'Добавление книги';
	$MESS["NOTA_COLLECTION_ELEMENT"] = 'Книга';
	$MESS["NOTA_COLLECTION_ELEMENTS"] = 'Книги';
	$MESS["NOTA_COLLECTION_ADD"] = 'Добавить';
	$MESS["NOTA_COLLECTION_ACCESS_DENIED"] = 'Доступ закрыт';
?>