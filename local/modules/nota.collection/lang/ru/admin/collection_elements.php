<?
	$MESS["NOTA_COLLECTION_ID"] = 'ID';
	$MESS["NOTA_COLLECTION_BOOK_ID"] = 'ID книги';
	$MESS["NOTA_COLLECTION_SORT"] = 'Сортировка';
	$MESS["NOTA_COLLECTION_TIMESTAMP_X"] = 'Дата изм.';

	$MESS["NOTA_COLLECTION_EDIT"] = 'Редактировать';
	$MESS["NOTA_COLLECTION_DELETE"] = 'Удалить';
	$MESS["NOTA_COLLECTION_DELETE_CONFIRM"] = 'Вы действительно хотите удалить книгу?';
	$MESS["NOTA_COLLECTION_ACCESS_DENIED"] = 'Доступ закрыт';

	$MESS["NOTA_COLLECTION_ADD"] = 'Добавить книгу';
	$MESS["NOTA_COLLECTION_TITLE"] = 'Книги в коллекции';
  
?>