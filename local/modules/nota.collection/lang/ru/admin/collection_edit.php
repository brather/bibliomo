<?
	$MESS["NOTA_COLLECTION_EDIT"] = 'Редатирование коллекции #ID#';
	$MESS["NOTA_COLLECTION_ADD"] = 'Добавление коллекции';
	$MESS["NOTA_COLLECTION_NEW"] = 'Добаввить коллекцию';
	$MESS["NOTA_COLLECTION_TAB"] = 'Параметры коллекции';
	$MESS["NOTA_COLLECTION_ERROR"] = 'Ошибка при добавлении изображения';
	$MESS["NOTA_COLLECTION_LIST"] = 'Коллекции';
	$MESS["NOTA_COLLECTION_Y"] = 'Да';
	$MESS["NOTA_COLLECTION_N"] = 'Нет';
	$MESS["NOTA_COLLECTION_ACCESS_DENIED"] = 'Доступ закрыт';
?>