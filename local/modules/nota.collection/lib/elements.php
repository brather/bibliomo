<?
	namespace Nota\Collection;

	use Bitrix\Main\Entity\DataManager;
	use Bitrix\Main\Localization\Loc;

	Loc::loadMessages(__FILE__);

	class ElementsTable extends DataManager
	{
		public static function getFilePath()
		{
			return __FILE__;
		}

		public static function getTableName()
		{
			return 'neb_collection_elements';
		}

		public static function getMap()
		{
			return array(
				'ID'      => array(
					'data_type'    => 'integer',
					'primary'      => true,
					'autocomplete' => true,
					'title'        => Loc::getMessage('ELEMENTS_ID'),
				),
				'BOOK_ID' => array(
					'data_type'  => 'string',
					'required'   => false,              
					'title'      => Loc::getMessage('ELEMENTS_BOOK_ID'),
				),
				'COLLECTION_ID'  => array(
					'data_type'  => 'string',
					'required'   => false,
					'title'      => Loc::getMessage('ELEMENTS_COLLECTION_ID'),
				),
				'SORT' => array(
					'data_type' => 'integer',
					'required' => true,
					'title' => Loc::getMessage('ELEMENTS_ENTITY_SORT_FIELD'),
				),
				'TIMESTAMP_X' => array(
					'data_type' => 'datetime',
					'required' => false,
					'title' => Loc::getMessage('ELEMENTS_ENTITY_TIMESTAMP_X_FIELD'),
				),

			);
		}
	}
