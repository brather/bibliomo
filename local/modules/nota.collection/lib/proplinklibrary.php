<?
	namespace Nota\Collection;
	/*
	Свойство - Привязка к библиотеке 

	*/
	#AddEventHandler("iblock", "OnIBlockPropertyBuildList", array("CIBlockPropertyRelatedMaterial","GetUserTypeDescription"));

	use Nota\Collection\LibraryTable;

	class PropLinkLibrary
	{
		function GetUserTypeDescription()
		{
			return array(
				"PROPERTY_TYPE" 			=> "S",
				"USER_TYPE" 				=> "link_library",
				"DESCRIPTION" 				=> '[NEB] Привязка к библиотеке',
				"GetPropertyFieldHtml"  	=> array(__CLASS__, "GetPropertyFieldHtml"),
			);
		}

		function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName)
		{
			$returnHTML = '';
			ob_start();

			$rsRes = LibraryTable::getList(array(
				'select' 	=> array('ID', 'NAME'),
				'order'		=> array('SORT' => 'DESC')
			));
		?>
		<select name="<?=htmlspecialcharsbx($strHTMLControlName["VALUE"])?>">
			<?
				while($arItem = $rsRes->fetch())
				{
				?>
				<option <?=$value['VALUE'] == $arItem['ID'] ? 'selected="selected"' : ''?> value="<?=$arItem['ID']?>"><?=$arItem['NAME']?></option>
				<?
				}
			?>
		</select>
		<?
			$returnHTML = ob_get_contents();
			ob_end_clean();
			return $returnHTML;
		}
	}
?>