<?
	namespace Nota\Collection;

	use Bitrix\Main\Entity;
	use Bitrix\Main\Localization\Loc;
	Loc::loadMessages(__FILE__);

	/**
	 * Class LibraryTable
	 * 
	 * Fields:
	 * <ul>
	 * <li> ID int mandatory
	 * <li> LIBRARY_ID int mandatory
	 * <li> PUBLICATIONS int mandatory
	 * <li> COLLECTIONS int mandatory
	 * <li> USERS int mandatory
	 * <li> VIEWS int mandatory
	 * <li> DATE datetime mandatory default 'CURRENT_TIMESTAMP'
	 * </ul>
	 *
	 * @package Bitrix\Stat
	 **/

	class LibraryStatTable extends Entity\DataManager
	{
		public static function getFilePath()
		{
			return __FILE__;
		}

		public static function getTableName()
		{
			return 'neb_stat_library';
		}

		public static function getMap()
		{
			return array(
				'ID' => array(
					'data_type' => 'integer',
					'primary' => true,
					'autocomplete' => true,
					'title' => Loc::getMessage('LIBRARY_ENTITY_ID_FIELD'),
				),
				'LIBRARY_ID' => array(
					'data_type' => 'integer',
					'required' => true,
					'title' => Loc::getMessage('LIBRARY_ENTITY_LIBRARY_ID_FIELD'),
				),
				'PUBLICATIONS' => array(
					'data_type' => 'integer',
					'required' => true,
					'title' => Loc::getMessage('LIBRARY_ENTITY_PUBLICATIONS_FIELD'),
				),
				'COLLECTIONS' => array(
					'data_type' => 'integer',
					'required' => true,
					'title' => Loc::getMessage('LIBRARY_ENTITY_COLLECTIONS_FIELD'),
				),
				'USERS' => array(
					'data_type' => 'integer',
					'required' => true,
					'title' => Loc::getMessage('LIBRARY_ENTITY_USERS_FIELD'),
				),
				'VIEWS' => array(
					'data_type' => 'integer',
					'required' => true,
					'title' => Loc::getMessage('LIBRARY_ENTITY_VIEWS_FIELD'),
				),
				'DATE' => array(
					'data_type' => 'datetime',
					'required' => true,
					'title' => Loc::getMessage('LIBRARY_ENTITY_DATE_FIELD'),
				),
			);
		}
}