<?php
	$MESS['EXALEAD_SETTINGS'] = 'Настройки подключения к Exalead';
	$MESS['VIEWER_SETTINGS'] = 'Настройки подключения к сервису выдачи';
	$MESS['EXALEAD_IP'] = 'IP адрес сервера Exalead';
	$MESS['EXALEAD_PORT'] = 'Номер порта сервера Exalead';
	$MESS['VIEWER_IP'] = 'IP адрес сервера API';
	$MESS['VIEWER_PORT'] = 'Номер порта сервера API';
    $MESS['VIEWER_PROTOCOL'] = 'Протокол доступа сервера API';
	//$MESS['VIEWER_KEY'] = 'Ключ сервера API rsl';
	
	$MESS['BOOKS_ADD_SETTINGS'] = 'Добавление книги';
	$MESS['BOOK_ADD_EXALEAD_IP'] = 'IP адрес сервера Exalead для добавления книги';
	$MESS['BOOK_ADD_EXALEAD_PORT'] = 'Порт';
	$MESS['BOOK_ADD_URL'] = 'Адрес куда будет отправляться запрос на добавление книги';
	$MESS['BOOK_ADD_LOGIN'] = 'Логин';
	$MESS['BOOK_ADD_PASSWORD'] = 'Пароль';
	$MESS['BOOK_ADD_KEY'] = 'Ключ авторизации';
?>