<?
	namespace Nota\Exalead;
	use Bitrix\NotaExt\NPHPCacheExt;
	/*
	API просмотровщика
	*/
	class RslViewer{
		private $ip = '';
		private $port = '';
        private $protocol = '';
		private $book_id = '';
		private $get_params = '';
		private $dop_params = '';
		private $key = '';
		private $arHeader = array();
		private $status;

		public function __construct($book_id)
		{
			if (empty($book_id))
				return false;

			$this->book_id = urlencode($book_id);

			if(!defined('RSL_WB'))
			{
				$this->ip = \COption::GetOptionString("nota.exalead", "viewer_ip");	
				$this->port = \COption::GetOptionString("nota.exalead", "viewer_port");
				$this->protocol = (trim(\COption::GetOptionString("nota.exalead", "viewer_protocol")) != ''? \COption::GetOptionString("nota.exalead", "viewer_protocol"): 'https');
			}
			else
			{
				$this->ip = "https://relar.rsl.ru";
				$this->port = "443";
				$this->key = "99Apaf8YwtAskq5sdfyNMnbD3SDU7qUH";
			}

		}

		public function setConnectParams($ip, $port, $key = '', $dop_params = '')
		{
			$this->ip = $ip;
			$this->port = $port;
			$this->key = $key;
			$this->dop_params = $dop_params;
		}

		public static function isAvailable($book_id){
			$isAvailable = false;

			$obCacheExt    	= new NPHPCacheExt();
			$arCacheParams = array('book_id' => $book_id);
			if ( !$obCacheExt->InitCache(__function__, $arCacheParams) ) 
			{
				$obj = new self($book_id);
				$res =  $obj->getDocumnetInfo();

				if($res !== false and $res['type'] != 'close' and (int)$res['pageCount'] > 0)
					$isAvailable = true;

				$obCacheExt->StartDataCache($isAvailable);					
			}
			else
			{
				$isAvailable = $obCacheExt->GetVars();
			}

			return $isAvailable;
		}

		/*		
		Отправляем запросы в rsl
		c возможностью задать заголовки и отключить получение тела ответа
		*/
		private function query($arHeader = array(), $dont_send_body_answer = false){
			session_write_close();
			$this->arHeader = array();

			if(empty($this->book_id))
				return false;

			$urlParams = '/'.$this->book_id.$this->get_params;
			$url = $this->protocol."://".trim($this->ip,'/').$urlParams;
			
			if(!empty($this->dop_params))
				$url .='?'.$this->dop_params;


			$ch = curl_init($url);
			if(!empty($this->port))
				curl_setopt($ch, CURLOPT_PORT , $this->port);
				
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_HTTPGET, true);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
			curl_setopt($ch, CURLOPT_TIMEOUT, 60);
			
			
			$result = curl_exec($ch);
			$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE); 
			curl_close($ch);

			$this->status = $httpCode;
			$araAcceptStatus = array(200, 206);	
			if(!in_array($httpCode, $araAcceptStatus))
				return false;

			return $result;
		}

		/*		
		11. Получение страницы документа, определенной геометрии 
		GET 
		http://{BASEURL}/{CODE}/document/pages/{NUM}/(images|thumbnails)/(width|height|max){VAL}.(jpeg|tiff)[?{GET_PARAMS}] 
		Аналогично предыдущему, однако, страница возвращаеся со значением высоты или ширины равным <VAL>. 
		• при использовании image есть ограничение: 300 < (width|height) <= 2000 
		• при использовании thumbnails есть ограничение: 50 <= (width|height) <= 300, также эти картинки будут кешироваться средствами nginx. 
		• при использовании max в качестве измерения выходное изображение будет вписано в квадрат <VAL>x<VAL>. 

		ПРИМЕР ЗАПРОСА: http://10.2.0.44:8080/01000000005/document/pages/1/images/width800.tiff 
		ПРИМЕР ОТВЕТА: {raw tiff image data} 

		*/
		public function getDocumentPage($width = 300, $num = 1)
		{
			$this->get_params = '/document/pages/'.$num.'/images/width'.$width.'.jpeg?ip='.$_SERVER['REMOTE_ADDR'];
			$result = self::query();
			return $result;
		}

		/*
		получить массив доступных ресурсов
		*/
		public function getResources(){
			$this->get_params = '/resources';
			$result = self::query(); 
			if($result)
				$result = json_decode($result, true); 
			return $result;
		}

		/*		
		Получиь информацию о документе
		*/
		public function getDocumnetInfo(){
			$this->get_params = '/document/info';
			$result = self::query();

			if($result)
				$result = json_decode($result, true); 
			return $result;

		} 

		/*		
		получаить размер файла - байт
		*/
		public function getResourcesExtSize($ext = 'pdf'){
			if(empty($ext))
				return false;

			$this->get_params = '/resources/'.$ext.'/size';
			$result = self::query();
			return $result;
		}
        /*
		ссылка на файл
		*/
        public function getResourcesLink($ext = 'pdf'){
            if(empty($ext))
                return false;

            $this->get_params = '/resources/'.$ext.'/link';
            $result = self::query();
            return $result;
        }

        /*
        скачивание документа
        */
		public function getFile($ext = 'pdf'){
			if(empty($ext))
				return false;
				
			session_write_close();
			$filesize = (int)self::getResourcesExtSize($ext);

            if ($filesize > 0) {
                if (ob_get_level()) ob_end_clean();

                $l_oLinkObject = json_decode(self::getResourcesLink($ext));

                foreach ($l_oLinkObject->headers AS $l_sKey => $l_sValue) {

                    if ($l_sKey != 'host') $l_aHeaders[] = $l_sKey . ': ' . $l_sValue;
                }
                // путь к файлу
                $ch = curl_init($l_oLinkObject->link."?ip=".$_SERVER['REMOTE_ADDR']);


                curl_setopt($ch, CURLOPT_HTTPHEADER, $l_aHeaders);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_HEADER, true);
                curl_setopt($ch, CURLINFO_HEADER_OUT, true);
                curl_setopt($ch, CURLOPT_NOBODY, true);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
                curl_exec($ch);
                $headerContentType = curl_getinfo($ch,CURLINFO_CONTENT_TYPE);
                curl_close($ch);
                preg_match('/^application\/([a-zA-Z]*)/',$headerContentType,$tmp);

                // тип файла
                $contentType = ($tmp[1] ? $tmp[1] :'pdf');

                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename='.str_replace(array(" ", ".", ",", "/", ";"), array("_", "", "", "", ""), trim($_REQUEST["name"])).'.'.$contentType);
                header('Content-Transfer-Encoding: binary');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                if ($filesize != 1) header('Content-Length: ' . $filesize);
                $ch = curl_init($l_oLinkObject->link."?ip=".$_SERVER['REMOTE_ADDR']);

                curl_setopt($ch, CURLOPT_HTTPHEADER, $l_aHeaders);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,false);

                curl_exec($ch);
                curl_close($ch);
                exit;
            }
			
		} 

		public function getDocumentPageSearch($width = 300, $num = 1,$search)
		{
			$this->get_params = '/document/pages/'.$num.'/search/'.$search.'/render/ppi150.jpeg';
			$result = self::query();
			return $result;
		}

		/*
		Количество страниц в документе
		*/
		public function getPagesCount(){
			$this->get_params = '/document/pages/count';
			$result = self::query();
			if($result)
				$result = json_decode($result, true); 
			return $result;
		}

		// получение номеров страниц в которых встречаются слова
		public function getPageSearch($search){
			if(empty($search))
				return false;

			$this->get_params = '/document/search/'.$search;
			$result = self::query();
			return $result;
		}

		public function getPageThumbl($width = 50, $num = 1)
		{
			$this->get_params = '/document/pages/'.$num.'/thumbnails/width'.$width.'.jpeg';
			$result = self::query();
			return $result;
		}

		//геометрия документа
		public function getGeometry($num = 1)
		{
			$this->get_params = '/document/pages/'.$num.'/geometry';
			$result = self::query();
			return $result;
		}

		public function isAvailableType(){
			$this->get_params = '/document/type';
			$result = self::query();
			return $result;

		}
	}  
?>