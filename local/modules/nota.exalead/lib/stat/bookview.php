<?
	namespace Nota\Exalead\Stat;
	use Nota\Exalead\Stat\StatBookViewTable;
	use Nota\Exalead\BooksInfoTable;

	class BookView extends \Bitrix\NotaExt\Singleton
	{
		private $BOOK_ID;
		private $LIB_ID;
		private $UID = 0; 
		private $sessidUID = ''; 

		public function __construct() 
		{
			self::setCookieUid();
		}

		public static function set($BOOK_ID, $LIB_ID){
			return self::getInstance()->setBookViewPr($BOOK_ID, $LIB_ID);
		}

		private function setBookViewPr($BOOK_ID, $LIB_ID){
			if(empty($BOOK_ID))
				return false;

			$this->BOOK_ID = $BOOK_ID;
			$this->LIB_ID = $LIB_ID;

			$arViewsBook = $GLOBALS['USER']->GetParam("ViewsBook");
			if(empty($arViewsBook))
			{
				$GLOBALS['USER']->SetParam("ViewsBook", $BOOK_ID);
				self::add();
			}
			elseif(strpos($arViewsBook, $BOOK_ID) === false)
			{
				$GLOBALS['USER']->SetParam("ViewsBook", $arViewsBook.'|'.$BOOK_ID);
				self::add();				
			}
		}

		private function add(){
			if(empty($this->BOOK_ID) or empty($this->sessidUID))
				return false;


			global $DB;
			$DB->StartUsingMasterOnly();

			$arFields = array(
				'BOOK_ID' => $this->BOOK_ID,
				'LIB_ID' => $this->LIB_ID,
				'UID' => $this->UID,
				'UID_GUEST' => $this->sessidUID
			);

			$rsRes = BooksInfoTable::getList(array(
				'select' 	=> array('ID', 'VIEWS'),
				'filter' => array('=BOOK_ID' => $this->BOOK_ID)
			));

			$arRes = $rsRes->fetch();
			if(empty($arRes))
				BooksInfoTable::add(array('BOOK_ID' => $this->BOOK_ID, 'LIB_ID' => $this->LIB_ID, 'VIEWS' => 1));
			else
				BooksInfoTable::update($arRes['ID'], array('VIEWS' => ($arRes['VIEWS'] + 1)));

			StatBookViewTable::add($arFields);
			$DB->StopUsingMasterOnly();
		}

		private function setCookieUid(){
			$sessidUID = $GLOBALS['APPLICATION']->get_cookie("sessidUID");
			if(empty($sessidUID))
			{
				$sessidUID = bitrix_sessid();
				$GLOBALS['APPLICATION']->set_cookie("sessidUID", $sessidUID);
			}

			$this->sessidUID = $sessidUID;
			if(intval($_SESSION['SESS_AUTH']['USER_ID']))
				$this->UID = (int)$_SESSION['SESS_AUTH']['USER_ID'];

		} 
	}
?>