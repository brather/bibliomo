<?
	/*
	скрипт обновления количества издания, которые выводятся на главной странице в правом углу
	запускать раз в сутки, в 00Ж00
	*/
	namespace Nota\Exalead\Agents;

	class SetCountBook
	{
		public static function set()
		{
			/* записей каталогов */
			$query = new \Nota\Exalead\SearchQuery('#all');	
			$query->setPageLimit(1);
			
			#$qp = $query->getParameters();		pre($qp,1);
			
			$client = new \Nota\Exalead\SearchClient();
			$result = $client->getResult($query);
			
			if(intval($result['COUNT']) > 0)
				\COption::SetOptionString('exalead','book_count_catalog', intval($result['COUNT']));

			/* электронных книг */
			$query = new \Nota\Exalead\SearchQuery('filesize>0');
			$query->setPageLimit(1);

			$client = new \Nota\Exalead\SearchClient();
			$result = $client->getResult($query);
			
			if(intval($result['COUNT']) > 0)
				\COption::SetOptionString('exalead','book_count', intval($result['COUNT']));

			return 'Nota\Exalead\Agents\SetCountBook::set();';
		}
	}
?>