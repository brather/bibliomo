<?
	namespace Nota\Exalead;
	/*
	Распарсиваем XML ответ
	*/
	class XmlParser{
		private $xml = '';
		public $arResult = Array();

		public function __construct($strXml)
		{
			if(empty($strXml))
				return false;
			$this->xml = simplexml_load_string($strXml);
		}

		public function search(){

			if(empty($this->xml))
				return false;



            if(isset($this->xml->spellcheckSuggestions)):

                $res = preg_split('#AND#', $this->xml->spellcheckSuggestions->SpellCheckSuggestions->SpellCheckSuggestion->attributes()->newStr);
                $res = preg_replace('#simple\:\((.*)\)#', '$1', $res[0]);
                $this->arResult['spellcheckSuggestion'] = trim($res);
            else:
                $this->arResult['spellcheckSuggestion'] = '';
            endif;

			$this->arResult['COUNT'] = $this->xml->attributes();
			$this->arResult['COUNT'] = (int)$this->arResult['COUNT']['nmatches'];

			$this->arResult['NHITS'] = $this->xml->attributes(); 
			$this->arResult['NHITS'] = (int)$this->arResult['NHITS']['nhits'];

			$this->arResult['START_ITEM'] = $this->xml->attributes(); 
			$this->arResult['START_ITEM'] = (int)$this->arResult['START_ITEM']['start'];

			if(empty($this->xml->hits->Hit))
				return false;
			/*
			Книги
			*/
			$arItems = array();
			$i = 0;
			foreach($this->xml->hits->Hit as $hit)
			{
				$i++;
				$arItem = array();
				$arItem['NUM_ITEM'] = intval($this->arResult['START_ITEM']) + $i;

				foreach($hit->groups->AnswerGroup as $agroup)
				{
					$agroupAtr = $agroup->attributes();

					if($agroupAtr['id'] == 'publisher'){
						$arItem['r_publisher'] = $agroup->categories->Category->attributes(); $arItem['r_publisher'] = (string)$arItem['r_publisher']['id'];					
					}elseif($agroupAtr['id'] == 'authorbook'){
						$arItem['r_authorbook'] = $agroup->categories->Category->attributes(); $arItem['r_authorbook'] = (string)$arItem['r_authorbook']['id'];
					}elseif($agroupAtr['id'] == 'library'){
						$arItem['r_library'] = $agroup->categories->Category->attributes(); $arItem['r_library'] = (string)$arItem['r_library']['id'];
					}

				}
				foreach($hit->metas->Meta as $meta)
				{
					$metaAtr = $meta->attributes();
					if (empty($arItem['filename']) && (string)$metaAtr['name'] == 'file_size' && (int)$meta->MetaString > 0)	
					{
						/**
						 * Заглушка, чтобы отображалась ссылка на PDF
						 */
						$arItem['fileExt'] = array('pdf');
					}

					if ((string)$metaAtr['name'] == 'file_size')
					{
						$arItem['filesize'] = (int)$meta->MetaString;
					}
					elseif($metaAtr['name'] == 'text')
					{
						foreach($meta->MetaText->TextSeg as $text)
						{
							$highlighted = $text->attributes();
							$highlighted = (string)$highlighted['highlighted'];
                            // убрать лишние символы. Доработка для РГБ
                            $text = preg_replace_callback ("/[^A-zА-яЁёÄäÖöÜüß\(\.,;:!?\-\)іѲѳѢѣѵѴѣ\d\s]|[\]\[\^]/u",
                                function ($matches)
                                {
                                    for ($i = 0; $i < count($matches); $i++) return '';
                                },
                                $text);
							if(!is_array($text))
							{
								if($highlighted == 'true')
								{
									$arItem['text'].= ' <strong>'.(string)$text.'</strong> ';
									$arItem['text_strong'] = true;
								}
								else
									$arItem['text'].= ' '.(string)$text.' ';



							}
						}
					}
					elseif($metaAtr['name'] == 'title')
					{
						#foreach($meta->MetaText->TextSeg as $title)
						#	$arItem['title'].= (string)$title;

						foreach($meta->MetaText->TextSeg as $text)
						{
							$highlighted = $text->attributes();	$highlighted = (string)$highlighted['highlighted'];
							if(!is_array($text))
							{
								if($highlighted == 'true')
								{
									$arItem['title'].= ' <strong>'.(string)$text.'</strong> ';
									$arItem['title_strong'] = true;
								}
								else
									$arItem['title'].= ' '.(string)$text.' ';
							}
						}
					}
					elseif($metaAtr['name'] == 'subtitle')
						$arItem['subtitle'] = (string)$meta->MetaString;
						
					elseif($metaAtr['name'] == 'dateindex')
						$arItem['dateindex'] = (string)$meta->MetaString;
						
					elseif($metaAtr['name'] == 'year')
						$arItem['year'] = (string)$meta->MetaString;
					elseif($metaAtr['name'] == 'idlibrary')
						$arItem['idlibrary'] = (string)$meta->MetaString;
					elseif($metaAtr['name'] == 'publishyear')
						$arItem['publishyear'] = (string)$meta->MetaString;
					elseif($metaAtr['name'] == 'library')
						$arItem['library'] = (string)$meta->MetaString;

					elseif($metaAtr['name'] == 'publisher')
						$arItem['publisher'] = (string)$meta->MetaString;

					elseif($metaAtr['name'] == 'idparent')
						$arItem['idparent'] = (string)$meta->MetaString;
					elseif($metaAtr['name'] == 'idparent2')
						$arItem['idparent2'] = (string)$meta->MetaString;

					elseif($metaAtr['name'] == 'authorbook'){
					
                        if ($meta->MetaString)
                            $arItem['authorbook'] = (string)$meta->MetaString;
                        elseif($meta->MetaText->TextSeg){
                           
							/*
							выделение поисковой фразы в поле "Автор"
							*/
							foreach($meta->MetaText->TextSeg as $text)
							{
								$highlighted = $text->attributes();	$highlighted = (string)$highlighted['highlighted'];
								if(!is_array($text))
								{
									if($highlighted == 'true')
									{
										$arItem['authorbook'].= ' <strong>'.(string)$text.'</strong> ';
										$arItem['authorbook_strong'] = true;
									}
									else
										$arItem['authorbook'].= ' '.(string)$text.' ';
								}
							}
						}
                    }
					elseif($metaAtr['name'] == 'responsibility')
						$arItem['responsibility'] = (string)$meta->MetaString;
						
					elseif($metaAtr['name'] == 'publishplace')
						$arItem['publishplace'] = (string)$meta->MetaString;

					elseif($metaAtr['name'] == 'countpages')
						$arItem['countpages'] = (string)$meta->MetaString;

					elseif($metaAtr['name'] == 'isprotected')
						$arItem['isprotected'] = (int)$meta->MetaString;

					elseif($metaAtr['name'] == 'filename' or $metaAtr['name'] == 'viewurl' or $metaAtr['name'] == 'file_name'){
						$arItem['filename'] = (string)$meta->MetaString;
						if(!empty($arItem['filename'])){
							$arTmpFile = explode(' ', $arItem['filename']);
							TrimArr($arTmpFile);
							if(!empty($arTmpFile))
							{
								foreach($arTmpFile as $file){
									$ext = GetFileExtension($file);
									if(!empty($ext))
										$arItem['fileExt'][] = $ext; 
								}
								if(!empty($arItem['fileExt']))
									$arItem['fileExt'] = array_unique($arItem['fileExt']);
							}
						}
					}
					elseif($metaAtr['name'] == 'id'){
						$arItem['id'] = (string)$meta->MetaString;
						$arItem['~id'] = urlencode(str_replace('/', '!|', $arItem['id']));
					}
					elseif($metaAtr['name'] == 'id_abis'){
						$arItem['id_abis'] = (string)$meta->MetaString;
					}
					elseif($metaAtr['name'] == 'is_skbr'){
						$arItem['is_skbr'] = (string)$meta->MetaString;
					}
					elseif($metaAtr['name'] == 'id_skbr'){
						$arItem['id_skbr'] = (string)$meta->MetaString;
					}
					elseif($metaAtr['name'] == 'series'){
						$arItem['series'] = (string)$meta->MetaString;
					}
					elseif($metaAtr['name'] == 'annotation'){
						$arItem['annotation'] = (string)$meta->MetaString;
					}
					elseif($metaAtr['name'] == 'publicurl'){
						$arItem['publicurl'] = (string)$meta->MetaString;
					}
					elseif($metaAtr['name'] == 'bbk'){
						$arItem['bbk'] = (string)$meta->MetaString;
					}
					elseif($metaAtr['name'] == 'contentnotes'){
						$arItem['contentnotes'] = (string)$meta->MetaString;
					}
					elseif($metaAtr['name'] == 'edition'){
						$arItem['edition'] = (string)$meta->MetaString;
					}
					elseif($metaAtr['name'] == 'isbn'){
						$arItem['isbn'] = (string)$meta->MetaString;
					}
					elseif($metaAtr['name'] == 'issn'){
						$arItem['issn'] = (string)$meta->MetaString;
					}
					elseif($metaAtr['name'] == 'notes'){
						$arItem['notes'] = (string)$meta->MetaString;
					}
					elseif($metaAtr['name'] == 'placepublish'){
						$arItem['placepublish'] = (string)$meta->MetaString;
					}
					elseif($metaAtr['name'] == 'udk'){
						$arItem['udk'] = (string)$meta->MetaString;
					}
					elseif($metaAtr['name'] == 'collection'){
                        $arMeta = (array)$meta;
                        $MetaString = $arMeta['MetaString'];
                        if (!is_array($MetaString))
                            $MetaString = array($MetaString);
						foreach($MetaString as $coll)
						{
							if(!is_array($coll))
								$arItem['collection'][] = $coll;
						}
					}

				}

				# Fix
				if(empty($arItem['year']) and !empty($arItem['publishyear']))
					$arItem['year'] = $arItem['publishyear'];

				$arItem['URL'] = $hit->attributes(); $arItem['URL'] = (string)$arItem['URL']['url'];
				$arItem['source'] = $hit->attributes();
				$arItem['source'] = (string)$arItem['source']['source'];

				# ссылка на закрытый просматровщик
				/*if($arItem['isprotected'] == 1 and $arItem['filesize'] > 0){
					$arItem['PROTECTED_URL'] = CLOSED_VIEWER_LINK.$arItem['~id'];
				}*/

				/* Проверка на чтение книги - показывать ли кнопку ЧИТАТЬ*/
				#if($arItem['filesize'] > 0 and $arItem['filesize'] < 100000000)
				if($arItem['filesize'] > 0)
					$arItem['IS_READ'] = 'Y';

                /*if(!empty($arItem['collection'])){

                    if(is_array($arItem['collection']))
                        $strCollection = implode(',', $arItem['collection']);
                    else
                        $strCollection = $arItem['collection'];

                    if($arItem['idlibrary'] == 199  and strpos('text'.$strCollection, 'open') === false){
                        $arItem['isprotected'] = 1;
                        $arItem['IS_READ'] = 'N';
                    }
                    if($arItem['idlibrary'] == 200  and strpos('text'.$strCollection, 'защищенные авторским правом') !== false){
                        $arItem['isprotected'] = 1;
                        $arItem['IS_READ'] = 'N';
                    }
                }*/

				if($arItem['library'] == 'РГБ')
					$arItem['source'] = 'RGB';

				if($arItem['source'] == 'VideoData')
				{
					$arItem['VIDEO_URL'] = '/catalog/'.$arItem['~id'].'/video/';
				}

				$arItem['DETAIL_PAGE_URL'] = '/catalog/'.$arItem['~id'].'/';	

				#if($arItem['idlibrary'] == 200)
				#$arItem['VIEWER_URL'] = 'http://electronic-library.ru/?id='.urlencode($arItem['id']);
				#else								
			    $arItem['VIEWER_URL'] = '/catalog/'.$arItem['~id'].'/viewer/';
                $arItem['PROTECTED_URL'] = CLOSED_VIEWER_LINK.$arItem['~id'];

				/*
				если $arItem['idlibrary'] == 199 и $arItem['filesize'] > 0 то тянем картинку с сервиса доступа
				иначе из Экзалиды
				*/
				if($arItem['idlibrary'] == 199 and $arItem['filesize'] > 0)
					$arItem['IMAGE_URL'] = '/local/tools/exalead/thumbnail.php?url='.$arItem['URL'].'&source=OUR_SERVICE';
				else
					$arItem['IMAGE_URL'] = '/local/tools/exalead/thumbnail.php?url='.$arItem['URL'].'&source='.$arItem['source'];
				/*				
				обрабатываем логику, - если нет поискового запроса, то нет выделенного текста. И если есть аннотация то показываем ее вместо текста
				ну и если нет текста, а есть аннотация то показываем ее  
				*/
				if(empty($arItem['text']) and !empty($arItem['annotation'])){
					$arItem['text'] = $arItem['annotation'];
				}else if(!empty($arItem['text']) and $arItem['annotation'] !== true and !empty($arItem['annotation'])){
					$arItem['text'] = $arItem['annotation'];
				}

				$arItems[] = $arItem; 
			}
			$this->arResult['ITEMS'] = $arItems;

			/*
			Группы для фильтрации
			*/

			$arGroupsKeyVal = array();
			$arGroups = array();
			foreach($this->xml->groups->AnswerGroup as $group)
			{
				$id = $group->attributes(); $id = strtoupper((string)$id['id']);

				$arGroup = array();

				if($id == 'YEARRANGE')
					$selector = $group->categories->Category->Category;
				else
					$selector = $group->categories->Category;

				$i=0;
				foreach($selector as $cat)
				{
					$title = $cat->attributes();
					$title = (string)$title['title'];

					if($title == 'Книга')
						$title = 'Книг';

					$idGroup = $cat->attributes(); $idGroup = (string)$idGroup['id'];
					$countGroup = $cat->attributes(); $countGroup = (string)$countGroup['count'];

					$arGroup[] = array(
						'id' => $idGroup,
						'count' => $countGroup,
						'title' => $title,
					);
					$arGroupsKeyVal[$idGroup] = $i; 
					$i++;
				}

				$arGroups[$id] = $arGroup; 
			}

			$this->arResult['GROUPS_KEY_VAL'] = $arGroupsKeyVal;		
			$this->arResult['GROUPS'] = $arGroups;		

		}

		/*		
		может быть вы имели в виду
		*/
		public function spellcheck(){
			if(empty($this->xml->SpellCheckSuggestion))
				return false;
			$this->arResult['REFINEMENT'] = $this->xml->SpellCheckSuggestion->attributes();
			$this->arResult['REFINEMENT'] = (string)$this->arResult['REFINEMENT']['newStr'];
		}
		/*
		автокомплит
		*/
		public function autocomplete(){
			if(count($this->xml->SuggestXMLEntry) <= 0)
				return false;

			foreach($this->xml->SuggestXMLEntry as $v){
				$entry = $v->attributes();
				$entry = (string)$entry['entry'];
				$this->arResult[] = $entry;
			}
		}


	}
?>
