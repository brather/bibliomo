<?php
namespace Nota\Exalead;

use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

/**
 * Class AlisTable
 *
 * Fields:
 * <ul>
 * <li> Id int mandatory
 * <li> Name string(255) mandatory
 * <li> Description string optional
 * <li> Type int mandatory
 * <li> Library int mandatory
 * <li> IpOrHostName string(64) optional
 * <li> Port int optional
 * <li> Login string(64) optional
 * <li> Password string(64) optional
 * <li> PathAndFileName string(500) optional
 * <li> BX_TIMESTAMP datetime mandatory default 'CURRENT_TIMESTAMP'
 * <li> BX_IS_UPLOAD int optional
 * </ul>
 *
 * @package Nota\Exalead
 **/

class BiblioAlisTable extends Entity\DataManager
{
    public static function getFilePath()
    {
        return __FILE__;
    }

    public static function getTableName()
    {
        return 'tbl_libraries_alis';
    }

    public static function getMap()
    {
        return array(
            'Id' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
                'title' => Loc::getMessage('ALIS_ENTITY_ID_FIELD'),
            ),
            'Name' => array(
                'data_type' => 'string',
                'required' => true,
                'validation' => array(__CLASS__, 'validateName'),
                'title' => Loc::getMessage('ALIS_ENTITY_NAME_FIELD'),
            ),
            'Description' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('ALIS_ENTITY_DESCRIPTION_FIELD'),
            ),
            'Type' => array(
                'data_type' => 'integer',
                'required' => true,
                'title' => Loc::getMessage('ALIS_ENTITY_TYPE_FIELD'),
            ),
            'Library' => array(
                'data_type' => 'integer',
                'required' => true,
                'title' => Loc::getMessage('ALIS_ENTITY_LIBRARY_FIELD'),
            ),
            'IpOrHostName' => array(
                'data_type' => 'string',
                'validation' => array(__CLASS__, 'validateIporhostname'),
                'title' => Loc::getMessage('ALIS_ENTITY_IPORHOSTNAME_FIELD'),
            ),
            'Port' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('ALIS_ENTITY_PORT_FIELD'),
            ),
            'Login' => array(
                'data_type' => 'string',
                'validation' => array(__CLASS__, 'validateLogin'),
                'title' => Loc::getMessage('ALIS_ENTITY_LOGIN_FIELD'),
            ),
            'Password' => array(
                'data_type' => 'string',
                'validation' => array(__CLASS__, 'validatePassword'),
                'title' => Loc::getMessage('ALIS_ENTITY_PASSWORD_FIELD'),
            ),
            'PathAndFileName' => array(
                'data_type' => 'string',
                'validation' => array(__CLASS__, 'validatePathandfilename'),
                'title' => Loc::getMessage('ALIS_ENTITY_PATHANDFILENAME_FIELD'),
            ),
            'BX_TIMESTAMP' => array(
                'data_type' => 'datetime',
                'required' => true,
                'title' => Loc::getMessage('ALIS_ENTITY_BX_TIMESTAMP_FIELD'),
            ),
            'BX_IS_UPLOAD' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('ALIS_ENTITY_BX_IS_UPLOAD_FIELD'),
            ),
        );
    }
    public static function validateName()
    {
        return array(
            new Entity\Validator\Length(null, 255),
        );
    }
    public static function validateIporhostname()
    {
        return array(
            new Entity\Validator\Length(null, 64),
        );
    }
    public static function validateLogin()
    {
        return array(
            new Entity\Validator\Length(null, 64),
        );
    }
    public static function validatePassword()
    {
        return array(
            new Entity\Validator\Length(null, 64),
        );
    }
    public static function validatePathandfilename()
    {
        return array(
            new Entity\Validator\Length(null, 500),
        );
    }
}