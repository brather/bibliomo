<?
	/*
	Отправка книги в Экзалиду
	*/
	namespace Nota\Exalead;
	use Nota\Exalead\BiblioCardTable;
	use Nota\Exalead\BiblioAlisTable;
	use Nota\Exalead\DateConverter;

	class BooksSend
	{
		public function getList()
		{
			$result = BiblioCardTable::getList(array(
				'select' => array('*', 'LIB_NAME' => 'LIB.Name', 'LIB_ID' => 'LIB.Library'),
				'filter' => array(
					'BX_IS_UPLOAD' => '0'
				)
			));

			while($res = $result->Fetch(new DateConverter)){
				if(!file_exists($_SERVER['DOCUMENT_ROOT'].$res['pdfLink']))
					continue;
				$arResult[] = $res;
			}

			return $arResult;
		}	

		public function send($arFields){
			if(empty($arFields))
				return false;

			trimArr($arFields);

			$url 		= \COption::GetOptionString("nota.exalead", "book_add_url");
			$login 		= \COption::GetOptionString("nota.exalead", "book_add_login");
			$password 	= \COption::GetOptionString("nota.exalead", "book_add_password");
			$key 		= \COption::GetOptionString("nota.exalead", "book_add_key");

			$upload = $_SERVER['DOCUMENT_ROOT'].$arFields['pdfLink'];
			if(!file_exists($upload))
				return false;

			$arFields['upload'] = "@".$upload;
			$arFields['KEY'] = $key;
			   
			$ch = curl_init(); 

			curl_setopt($ch, CURLOPT_URL, $url); 

			if(!empty($login) and !empty($password))
			{
				curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC ) ; 
				curl_setopt($ch, CURLOPT_USERPWD, $login.":".$password); 
			}

			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HEADER, false);  
			curl_setopt($ch, CURLOPT_POST, true); 
			curl_setopt($ch, CURLOPT_POSTFIELDS, $arFields); 
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30); 
			$result = curl_exec($ch);  
			curl_close($ch);

			if(empty($result))
				return false;

			$arResult = json_decode($result, true);
			if(empty($arResult))
				return false;

			return $arResult;
		}	

		public function setInfo($id, $arFields)
		{
			$id = intval($id);

			if(empty($arFields) or empty($id))
				return false;
			if(empty($arFields['IdFromALIS']) or empty($arFields['IdBxcard']))
				return false;

			$arFueldsUpdate = array(
				'EX_IdFromALIS'	=> $arFields['IdFromALIS'],
				'EX_IdCard' 	=> $arFields['IdExaleadCard'],
				'EX_pdfLink' 	=> $arFields['pdfLink'],
				'BX_IS_UPLOAD' 	=> 1
			);
			return BiblioCardTable::update($id, $arFueldsUpdate);	
		}	
	}
?>