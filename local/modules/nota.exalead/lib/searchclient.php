<?
	namespace Nota\Exalead;
	use Nota\Exalead\XmlParser;
	use Bitrix\NotaExt\NPHPCacheExt;
	use Bitrix\NotaExt\WriteLog;

	class SearchClient {
		private $obQuery;
		public $strNav;
		public $arNavParam;

		public function getResult(SearchQuery $query){
			$this->obQuery = $query;


			$result = '';
			$url = $query->getUrl();
			$params = self::prepParameters($query->getParameters());

			$obCacheExt    	= new NPHPCacheExt();
			$arCacheParams['url'] = $url;
			$arCacheParams['params'] = md5($params);
			if (!$obCacheExt->InitCache(__function__.$url, $arCacheParams)) 
			{

				$logF=new WriteLog(dirname(__FILE__).'/queryLog2.txt');
				#$logF->addLog('start_'.urldecode(self::prepParameters($query->getParameters())));		
				#$ptime = getmicrotime();

				$logF->addLog("params: ".print_r($params,1));
				$result = QueryGetData(
					$query->ip, 
					$query->port, 
					$url, 
					$params, 
					$error_number, 
					$error_text,
					'POST'
				);

				/*	pre('*****',1);
				pre($query->ip,1);
				pre($query->port,1);
				pre($url,1);
				pre($error_number,1);
				pre($error_text,1);
				pre($result,1);*/

				if(empty($result))
				return false;

				#$logF->addLog('$result = '.substr($result, 0, 20000));
				#$logF->addLog('end_'.round(getmicrotime()-$ptime, 3));

				switch ($query->getQueryType()) 
				{
					case 'raw':
						return $result;
						break;
					case 'search':
						$obXml = new XmlParser($result);
						if($obXml)
						{
							$obXml->search();
							$result =  $obXml->arResult;
						}
						else
							$result =  false;
						break;

					case 'spellcheck':
						$obXml = new XmlParser($result);
						if($obXml)
						{
							$obXml->spellcheck();
							$result =  $obXml->arResult;
						}
						else
							$result = false;

						break;

					case 'getById':
						$obXml = new XmlParser($result);
						if($obXml)
						{
							$obXml->search();
							$result =  $obXml->arResult['ITEMS'][0];
						}
						else
							$result = false;
						break;

					case 'suggest':
						$obXml = new XmlParser($result);
						if($obXml)
						{
							$obXml->autocomplete();
							$result =  $obXml->arResult;
						}
						else
							$result = false;
						break;
				}

				//$logF->addLog('endParse_'.round(getmicrotime()-$ptime, 3));

				$obCacheExt->StartDataCache($result);
			}
			else
			{
				$result = $obCacheExt->GetVars();
			}

			# подставляем параметр token для закрытого просмотровщика
			global $USER;
			if(!empty($result) and $USER->IsAuthorized())
			{
				if($query->getQueryType() == 'search' and !empty($result['ITEMS']))
				{
					$nebUser = new \nebUser();
					$arUser = $nebUser->getUser();

					if(!empty($arUser['UF_TOKEN']))
					{
						foreach($result['ITEMS'] as &$arItem)
						{
							if(!empty($arItem['PROTECTED_URL']))
								$arItem['PROTECTED_URL'] .= '&token='.$arUser['UF_TOKEN'];
						}
					}
				}
				elseif($query->getQueryType() == 'getById')
				{
					if(!empty($result['PROTECTED_URL']))
					{
						$nebUser = new \nebUser();
						$arUser = $nebUser->getUser();
						if(!empty($arUser['UF_TOKEN']))
							$result['PROTECTED_URL'] .= '&token='.$arUser['UF_TOKEN'];
					}
				}
			}

			return $result;
		}

		public function prepParameters($arParam)
		{
			$strQuery = http_build_query($arParam);
			#$strQuery = preg_replace('/r\[[0-9]\]/iu', 'r', $strQuery); 
			# заменяем r[0] на r для того что бы задать несколько уточнений т.к. формат r[0] экзалида не понимает
			$strQuery = preg_replace('/r%5B[0-9]%5D/iu', 'r', $strQuery); 
			return $strQuery;
		}
		/*
		постраничка		
		*/
		public function getDBResult($arResult){

			if(empty($arResult['ITEMS']))
				return false;

			unset($arResult['GROUPS_KEY_VAL']);	
			unset($arResult['GROUPS']);	

			/*	формируем массив для постранички*/
			$arResultFetch = array();
			if(intval($arResult['START_ITEM']) > 0)
				$arResultFetch = array_fill(0, ($arResult['START_ITEM']), array('id'=> '1', 'title' => '2', 'publisher' => '3'));

			$arResultFetch = array_merge($arResultFetch, $arResult['ITEMS']);

			$endar = ($arResult['COUNT'] - ($this->obQuery->nav['SIZEN'] + $arResult['START_ITEM']));

			if($endar > 0){
				# ставим ограничение, иначе заканчивается память в array_fill
				if($endar > 2000)
					$endar = 2000;
				$arTmp = array_fill(0, $endar, array('id'=> '1', 'title' => '2', 'publisher' => '3'));
				$arResultFetch = array_merge($arResultFetch, $arTmp);
			}

			$rs = new \CDBResult;
			$rs->InitFromArray($arResultFetch);
			$rs->NavStart($this->obQuery->nav['SIZEN']);
			$this->strNav = $rs->GetPageNavString('');

			$this->arNavParam = array(
				'NavNum' 		=> $rs->NavNum,
				'NavPageCount' 	=> $rs->NavPageCount,
				'NavPageNomer' 	=> $rs->NavPageNomer,
			);
			return $rs;
		}
	}
?>