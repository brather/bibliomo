<?
	namespace Nota\Exalead;
	use Nota\Exalead\SearchClient;
	use Bitrix\NotaExt\WriteLog;

	class SearchQuery{
		private $arParams = array();
		public $ip = '';
		public $port = '';
		private $url = '/search-api/search';
		private $queryType = 'search';
		public $nav;

		public function __construct($query = null)
		{
			if ($query)
			{
				$this->setQuery($query);
				static::setPageLimit(10);
				static::setParam('l', 'ru');
			}

			$this->ip = \COption::GetOptionString("nota.exalead", "exalead_ip");
			$this->port = \COption::GetOptionString("nota.exalead", "exalead_port");

		}

		public function setQuery($query){
			static::setParam('q', trim($query));
		}
		/*
		Получить обложку 
		*/
		public function getThumbnail($uri, $source = 'JapanConnector', $width = 300, $height = 410){
			if(empty($uri))
				return false;

			/*				
			Корректируем размер, что бы потом сделать ресайз
			*/
			$ns_widht = intval($width * 0.1) + $width;
			$ns_height = intval($height * 0.1) + $height;


			$this->queryType = 'thumbnail';
			$this->url = '/thumbnail';
			static::setParam('uri', trim($uri));
			static::setParam('source', $source);
			static::setParam('width', $ns_widht);
			static::setParam('height', $ns_height);

			$thumbnailFolder = "/upload/tmp_thumbnail/".substr($uri, -3).'/';
			$fileName = $thumbnailFolder.$uri."w".$width."h".$height.".png";
			//if(!file_exists($_SERVER["DOCUMENT_ROOT"].$fileName)){

			#$logF=new WriteLog($_SERVER['DOCUMENT_ROOT'].'/queryThumbnailLog.txt');
			#$logF->addLog('start_'.$uri);		
			#$ptime = getmicrotime();

			$arFile = \CFile::MakeFileArray('http://'.$this->ip.':'.$this->port.$this->url.'?'.http_build_query($this->arParams));
			if(substr($arFile['type'], 0, 4) == 'text') # если вместо картинки приходит ошибка в текстовом виде
				return false;
			#$logF->addLog('end_'.$uri.'_'.round(getmicrotime()-$ptime, 3));

			if(empty($arFile['tmp_name']))
				return false;

			# пока выключаем обрезку лишних полей, т.к. если у книги нет полей, а просто название на белом фоне, то функция не может нормально сработать	
			PrepThumbnail::crop($arFile['tmp_name']);

			\CFile::ResizeImage($arFile, array('width' => $width, 'height' => $height));

			CheckDirPath($_SERVER["DOCUMENT_ROOT"].$thumbnailFolder);
			CopyDirFiles($arFile['tmp_name'], $_SERVER["DOCUMENT_ROOT"].$fileName);

			//}

			return $fileName;

		}

		public function getById($id){
			if(empty($id))
				return false;
			$this->queryType = 'getById';
			$this->setQuery('id:"'.$id.'"');	
			static::setParam('l', 'ru');
		}

		public function getByArIds(array $arIds){
			TrimArr($arIds);
			if(empty($arIds))
				return false;
			$arIds = array_map(create_function('$v', 'return "id:\"".$v."\"";'), $arIds);
			$q = implode(" OR ", $arIds);
			$this->__construct($q);	
		}
		/*		
		Возможно вы имели в виду
		*/
		public function getSpellcheck($query){
			if(empty($query))
				return false;		
			$this->queryType = 'spellcheck';
			$this->url = '/spellcheck';
			$this->setQuery($query);
			$this->setParam('l', 'ru');
		}
		/*
		Подсказки в поисковой строке
		*/
		public function getSuggestAuthor($query){	
			$this->queryType = 'suggest';
			$this->url = '/suggest/service/suggest_author';
			if(empty($query))
				return false;
			$this->setQuery($query);
		}
		/*
		Подсказки в поисковой строке
		*/
		public function getSuggestTitle($query){
			$this->queryType = 'suggest';
			$this->url = '/suggest/service/suggest_title';
			if(empty($query))
				return false;
			$this->setQuery($query);
		}

		# результатов на странице
		public function setPageLimit($hf){
			if((int)$hf > 0)
				static::setParam('hf', intval($hf));
		}
		# позиция для следующей выборки
		public function setNextPagePosition($b){
			if((int)$b > 0)
				static::setParam('b', intval($b));
		}

		public function setUrl($url){
			$url = trim($url);
			if(!empty($url))
				$this->url = $url;
		}

		public function getUrl(){
			return $this->url;
		}
		public function getQueryType(){
			return $this->queryType;
		}
        public function setQueryType($type = 'search'){
            $this->queryType = $type;
        }

		public function setParam($param, $value, $isarray = false){
			if($isarray)
				$this->arParams[$param][] = trim($value);
			else
				$this->arParams[$param] = trim($value);
		}

		public function getParameter($param){
			if(empty($param))
				return false;
			return $this->arParams[$param];
		}

		public function getParameters(){
			return $this->arParams;
		}

		/*		
		Постраничка
		*/
		public function setNavParams(){
			$item_count = static::getParameter('hf');
			if((int)$item_count <= 0)
				$item_count = 15;
			$this->nav = \CDBResult::GetNavParams($item_count);

			if($this->nav['PAGEN'] > 1)
				static::setNextPagePosition((($this->nav['PAGEN'] - 1) * $item_count));
		}
	}


	class PrepThumbnail{

		function crop($file){
			if(empty($file))
				return false;

			if(!file_exists($file))
				return false;
			/*	
			Убираем белые поля
			*/
			$image = new \Imagick($file);
			$geo = $image->getImageGeometry();

			/*			
			Варианты цвета рамки
			*/
			$arSearchColor = array('000000', 'fefefe', 'ffffff','e5e4e4');
			$whiteColor = 'ffffff';

			if(!empty($geo))
			{

				$centerW = $geo['width']/2;
				$centerH = $geo['height']/2;
				/*
				вычисляем границы рамки
				*/
				$top = false;
				$i = 0;	$w = 0;
				while($top === false){
					$pixel = $image->getImagePixelColor($centerW, $i);
					$color = self::getHexByRGB($pixel->getColor());
					if(!in_array($color, $arSearchColor) or $w > 2)
						$top = $i;

					if($color == $whiteColor)
						$w++;

					$i++;
					if($i >= $centerW)
						$top = 0;
				}

				$left = false;
				$i = 0; $w = 0;
				while($left === false){
					$pixel = $image->getImagePixelColor($i, $centerH);
					$color = self::getHexByRGB($pixel->getColor());
					if(!in_array($color, $arSearchColor) or $w > 2)
						$left = $i;

					if($color == $whiteColor)
						$w++;

					$i++;
					if($i >= $centerH)
						$left = 0;
				}



				$right = false;
				$i = $geo['width']; $w = 0;
				while($right === false){
					$pixel = $image->getImagePixelColor($i, $centerH);
					$color = self::getHexByRGB($pixel->getColor());
					if(!in_array($color, $arSearchColor) or $w > 2)
						$right = $i;

					if($color == $whiteColor)
						$w++;

					$i--;
					if($i <= $centerW)
						$right = $geo['width'];
				}

				$bottom = false;
				$i = $geo['height']; $w = 0;
				while($bottom === false){
					$pixel = $image->getImagePixelColor($centerW, $i);
					$color = self::getHexByRGB($pixel->getColor());
					if(!in_array($color, $arSearchColor) or $w > 2)
						$bottom = $i;

					if($color == $whiteColor)
						$w++;						

					$i--;
					if($i <= $centerH)
						$bottom = $geo['height'];
				}

				#if($top > 0)
				$top+= 3;
				#if($left > 0)
				$left+= 3;

				$w = $right - $left - 2;
				$h = $bottom - $top;

				if($w < ($geo['width']/2)){
					$w = $geo['width'];
					$left = 0;
				}

				/*print_r($top); echo '<br />';		print_r($left); echo '<br />';		print_r($right); echo '<br />'; print_r($bottom); echo '<br />';		
				pre($geo,1);
				pre($w,1);
				exit();*/						

				if(intval($w) > 0 and intval($h) > 0){
					$image->cropImage($w, $h, $left, $top);
					$image->writeImage($file);
					$image->clear();
				}
			}				

		}

		private	function getHexByRGB($rgb){
			if (!is_array($rgb)) return false;
			return sprintf('%02x%02x%02x', $rgb['r'], $rgb['g'], $rgb['b']);
		}
	}
?>