<?
	/*
	Добавление книги в в таблицу для последующей отправки в экзалид
	*/
	namespace Nota\Exalead;

	\CModule::IncludeModule("highloadblock"); 
	use Bitrix\Highloadblock as HL; 
	use Bitrix\Main\Entity; 
	use Bitrix\Main\Type\DateTime;

    \CModule::IncludeModule('nota.journal');
    use Nota\Journal\J;

	class BooksAdd
	{
		/*	
		@method	add	Добавляем книгу

		###Описания полей таблицы tbl_common_biblio_card###
		@param $arFields array
		Id автоматически генерируется  
		IdFromALIS id АБИС, нужно присвоить уникальное значение в ЕИСУБе
		`Library` Id библиотеки из справочника
		`Author` Автор
		`Name` Название издания
		`SubName` Подзаголовок
		`PublishYear` Год издания,
		`PublishPlace` Место издания,
		`Publisher` Издатель,
		`ISBN`,
		`Language` язык,
		`CountPages`количество страниц
		`Format` формат
		`Series` серия
		`VolumeNumber` номер тома
		`VolumeName` название тома
		`Collection` Коллекция
		`AccessType` тип доступа
		`isLicenseAgreement` наличие лицензионного соглашения
		`Responsibility` Ответственность
		`Annotation` аннотация,
		`ContentRemark` Примечание на содержание,
		`CommonRemark` Примечание
		`PublicationInformation` информация о публикации,
		`pdfLink` Ссылка на pdf-файл,
		`CreationDateTime` дата создания
		`UpdatingDateTime` дата изменения
		`BBKText` шифры ББК
		`UDKText` шифры УДК,
		`LanguageText` язык издания

		Дополнительные параметры
		BX_TIMESTAMP - дата изменения\добавления 
		BX_IS_UPLOAD - признак, выгружена ли книга, если книга измененв, то надо установить в null
		BX_ALIS - ID книги в справочнике библиотек
		BX_UID - ID пользователя который добавил книгу
		BX_LIB_ID - ID Библиотеки из ИБ для которой была добавлена книга
		BX_DELETE - признак удаления \ 1 - удалено
		
		
		@param $libId ID библиотеки 

		*/
		public function add($arFields, $libId)
		{

			if(empty($arFields) or empty($arFields['ALIS']) or empty($libId))
				return false;

            if(empty($arFields['IdFromALIS'])){
                self::setIdFromALIS($arFields, $libId);
                if(empty($arFields['IdFromALIS']))
                    return false;
            }

			if(empty($arFields['pdfLink']) or !file_exists($_SERVER['DOCUMENT_ROOT'].$arFields['pdfLink']))
				return false;

			$copyFileName = "/upload/books_pdf/".pathinfo($_SERVER["DOCUMENT_ROOT"].$arFields['pdfLink'], PATHINFO_BASENAME);
			if(CopyDirFiles($_SERVER["DOCUMENT_ROOT"].$arFields['pdfLink'], $_SERVER["DOCUMENT_ROOT"].$copyFileName))
				$arFields['pdfLink'] = $copyFileName;
			else
				return false;

			global $DB, $USER;
			$strSql = 'select * from tbl_common_biblio_card where IdFromALIS = "'.$arFields['IdFromALIS'].'"';	
			$res = $DB->Query($strSql, false, $err_mess.__LINE__);
			if($arRes = $res->Fetch())
				return $arRes['Id'];

			$arFields['Language'] 		= 170;
			$arFields['AccessType'] 	= 1;
			$arFields['LanguageText'] 	= 'rus';
			$arFields['BX_ALIS'] 		= $arFields['ALIS'];
			$arFields['BX_UID'] 		= $USER->GetID();

            if($libId != LIB_ID_RIGHTHOLDER){
                J::add('rightholder', 'add', Array('BOOK_ID' => $arFields['IdFromALIS']));
            } else {
                J::add('book', 'add', Array('BOOK_ID' => $arFields['IdFromALIS'], 'AUTHOR' => $arFields['Author'], 'NAME' => $arFields['Name'], 'YEAR' => $arFields['PublishYear'], 'LIB' => $libId, 'LANG' => $arFields['LanguageText'], 'BBK' => $arFields['BBKText']));
            }

			foreach($arFields as &$Field)
				$Field = '"'.$DB->ForSql($Field).'"';
			return $DB->Insert("tbl_common_biblio_card", $arFields, $err_mess.__LINE__);
		}


        public function addPublication($arFields, $libId)
        {

            if(empty($arFields) or empty($arFields['ALIS']) or empty($libId))
                return false;

            if(empty($arFields['IdFromALIS'])){
                self::setIdFromALIS($arFields, $libId);
                if(empty($arFields['IdFromALIS']))
                    return false;
            }


            global $DB, $USER;
            $strSql = 'select * from tbl_common_biblio_card where IdFromALIS = "'.$arFields['IdFromALIS'].'"';
            $res = $DB->Query($strSql, false, $err_mess.__LINE__);
            if($arRes = $res->Fetch())
                return $arRes['Id'];

            $arFields['Language'] 		= 170;
            $arFields['AccessType'] 	= 1;
            $arFields['LanguageText'] 	= 'rus';
            $arFields['BX_ALIS'] 		= $arFields['ALIS'];
            $arFields['BX_UID'] 		= $USER->GetID();

            if($libId != LIB_ID_RIGHTHOLDER){
                J::add('rightholder', 'add', Array('BOOK_ID' => $arFields['IdFromALIS']));
            } else {
                J::add('book', 'add', Array('BOOK_ID' => $arFields['IdFromALIS'], 'AUTHOR' => $arFields['Author'], 'NAME' => $arFields['Name'], 'YEAR' => $arFields['PublishYear'], 'LIB' => $libId, 'LANG' => $arFields['LanguageText'], 'BBK' => $arFields['BBKText']));
            }

            foreach($arFields as &$Field)
                $Field = '"'.$DB->ForSql($Field).'"';
            return $DB->Insert("tbl_common_biblio_card", $arFields, $err_mess.__LINE__);
        }

        public function edit($bookId, $arFields)
        {
            if (intval($bookId) <= 0 || empty($arFields))
                return false;

            if(empty($arFields['pdfLink']) or !file_exists($_SERVER['DOCUMENT_ROOT'].$arFields['pdfLink']))
                return false;

            $copyFileName = "/upload/books_pdf/".pathinfo($_SERVER["DOCUMENT_ROOT"].$arFields['pdfLink'], PATHINFO_BASENAME);
            if ($arFields['pdfLink'] != $copyFileName)
            {
                if(CopyDirFiles($_SERVER["DOCUMENT_ROOT"].$arFields['pdfLink'], $_SERVER["DOCUMENT_ROOT"].$copyFileName))
                    $arFields['pdfLink'] = $copyFileName;
                else
                    return false;
            }

            $arFields['BX_IS_UPLOAD'] = '0';

            if($arFields['ALIS'] == LIB_ID_RIGHTHOLDER){
                J::add('rightholder', 'edit', Array('BOOK_ID' => $bookId));
            } else {
                J::add('book', 'edit', Array('BOOK_ID' => $bookId, 'AUTHOR' => $arFields['Author'], 'NAME' => $arFields['Name'], 'YEAR' => $arFields['PublishYear'], 'LIB' => $arFields['ALIS'], 'LANG' => $arFields['LanguageText'], 'BBK' => $arFields['BBKText']));
            }

            global $DB;
            foreach($arFields as &$Field)
            {
                $Field = '"'.$DB->ForSql($Field).'"';
            }

            return $DB->Update('tbl_common_biblio_card', $arFields, "WHERE ID='".$bookId."'", $err_mess.__LINE__);
        }

		/*		
		@method	 setIdFromALIS	Генерим поле IdFromALIS
		@param $arFields array
		@param $libId int
		*/
		private function setIdFromALIS(&$arFields, $libId)
		{
			$balnk = '000000';
			$IdFromALIS = substr_replace($balnk, $libId, strlen($balnk) - strlen($libId));
			$IdFromALIS .= '_';
			$IdFromALIS .= substr_replace($balnk, $arFields['ALIS'], strlen($balnk) - strlen($arFields['ALIS']));
			$IdFromALIS .= '_';
			$IdFromALIS .= substr(md5($arFields['Author'].$arFields['Name'].$arFields['PublishYear']), 0, 10);

			$arFields['IdFromALIS'] = $IdFromALIS;
		}

		/*
		@method setLibrary Проверяем есть ли данная баблиотека, добавляем если нет
		@param 	$id integer	ID Библиотеки из справочника библиотек
		@param 	$name string	
		*/

		public function setLibrary($id, $name = '')
		{
			if((int)$id <= 0)
				return false;

			global $DB;

			$hlblock = HL\HighloadBlockTable::getById(HIBLOCK_LIBRARY)->fetch();
			$entity = HL\HighloadBlockTable::compileEntity($hlblock); 
			$entity_data_class = $entity->getDataClass();

			$rsData = $entity_data_class::getList(array(
				"select" => array("ID", "UF_ID", "UF_NAME"),
				"filter" => array('UF_ID' => (int)$id),
			));

			if($arData = $rsData->Fetch())
				$name = $arData['UF_NAME'];

			$strSql = 'select * from tbl_libraries_alis where Library = '.$id;
			$res = $DB->Query($strSql, false, $err_mess.__LINE__);
			if($arRes = $res->Fetch())
				return $arRes['Id'];
			else
				return $DB->Insert("tbl_libraries_alis", array('Name' => "'".trim($DB->ForSql($name))."'", 'Library' => $id), $err_mess.__LINE__);

		}

		/*
		@method getListBooksCurrentUser - Получить список книг для текущего пользователя
		@param $arrFilter		
		@param $arSelect		
		@param $by		
		@param $sort		
		*/
		public function getListBooksCurrentUser($arrFilter, $arSelect = array(), $by = 'BX_TIMESTAMP', $order = 'DESC')
		{
			global $USER, $DB;
			$arFilter = array(
				'and BX_UID = '.$USER->GetID(),
				'and (BX_DELETE != 1 or BX_DELETE is null)'
			);

			if(!empty($arrFilter))
				$arFilter = array_merge($arFilter, $arrFilter);
			if(empty($arSelect))
				$arSelect = array('Id', 'Name', 'Author', 'BBKText', 'PublishYear', $DB->DateToCharFunction("BX_TIMESTAMP", 'SHORT').' BX_TIMESTAMP_', 'pdfLink', 'toDel', 'ISBN');

			return self::getListBooks($arFilter, $arSelect, $by, $order);

		}

        public function getListBooksLibrary($libraryId, $arrFilter, $arSelect = array(), $by = 'BX_TIMESTAMP', $order = 'DESC')
        {
            global $USER, $DB;
            $arFilter = array(
                'and BX_LIB_ID = '.$libraryId,
                'and (BX_DELETE != 1 or BX_DELETE is null)'
            );

            if(!empty($arrFilter))
                $arFilter = array_merge($arFilter, $arrFilter);

            if(empty($arSelect))
            {
                $arSelect = array(
                    'Id', 'Author', 'Name', 'SubName', 'PublishYear', 'Publisher', 'PublishPlace', 'ISBN', 'Printing', 'CreationDateTime', 'Annotation', 'Series', 'Format',
                    'VolumeNumber', 'VolumeName', 'ContentRemark', 'CommonRemark', 'pdfLink', 'CreationDateTime', 'BBKText', 'UDKText', $DB->DateToCharFunction("BX_TIMESTAMP", 'SHORT').' BX_TIMESTAMP_',
                    'pdfLink'
                );
            }

            return self::getListBooks($arFilter, $arSelect, $by, $order);

        }

		/*
		@method getListBooks
		@param $arrFilter		
		@param $arSelect		
		@param $by		
		@param $sort	
		*/
		public function getListBooks($arFilter, $arSelect = array('*'), $by = 'BX_TIMESTAMP', $order = 'DESC')
		{
			if(empty($arFilter))
				return false;

			global $DB;

			$strSql = 'select '.implode(",", $arSelect).' from tbl_common_biblio_card where 1=1 '.implode(" ", $arFilter).' ORDER BY '.$by .' '.$order;
			   #pre($strSql,1);	
			$res = $DB->Query($strSql, false, $err_mess.__LINE__);
			$arResult = array();
			while($arRes = $res->Fetch())
				$arResult[] = $arRes; 
			return $arResult;
		}
		/*
		@method remove
		@param id int		
		*/
		public function remove($id)
		{
			if((int)$id <= 0)
				return false;

			global $DB, $USER;	

			$strSql = 'select Id, Author, Name, PublishYear, ALIS, LanguageText, BBKText from tbl_common_biblio_card where BX_UID='.$USER->GetID().' and Id = '.(int)$id;
			$res = $DB->Query($strSql, false, $err_mess.__LINE__);
			if($arRes = $res->Fetch())
			{

                if($arRes['ALIS'] == LIB_ID_RIGHTHOLDER){
                    J::add('rightholder', 'delete', Array('BOOK_ID' => $arRes['Id']));
                } else {
                    J::add('book', 'delete', Array('BOOK_ID' => $arRes['Id'], 'AUTHOR' => $arRes['Author'], 'NAME' => $arRes['Name'], 'YEAR' => $arRes['PublishYear'], 'LIB' => $arRes['ALIS'], 'LANG' => $arRes['LanguageText'], 'BBK' => $arRes['BBKText']));
                }

				$arFields = array(
					'BX_DELETE' 	=> 1,
					'BX_IS_UPLOAD' 	=> 'null'
				);
				$DB->Update('tbl_common_biblio_card', $arFields, 'where Id = '.$arRes['Id']);
			}
		}
	}
?>