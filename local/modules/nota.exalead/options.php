<?
	if(!$USER->IsAdmin())
		return false;

	IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/options.php");
	IncludeModuleLangFile(__FILE__);
	

	$arExaleadOptions = array(
		array("exalead_ip", GetMessage("EXALEAD_IP"), false, array("text", 20)),
		array("exalead_port", GetMessage("EXALEAD_PORT"), false, array("text", 5)),
	);
	
	$arViewerOptions = array(
		array("viewer_ip", GetMessage("VIEWER_IP"), false, array("text", 20)),
		array("viewer_port", GetMessage("VIEWER_PORT"), false, array("text", 5)),
        array("viewer_protocol", GetMessage("VIEWER_PROTOCOL"), false, array('selectbox',	array("http" => "http", "https" => "https"))),
		#array("viewer_key", GetMessage("VIEWER_KEY"), false, array("text", 40)),
	);
	
	$arBooksAddOptions = array(
		#array("book_add_exalead_ip", GetMessage("BOOK_ADD_EXALEAD_IP"), false, array("text", 14)),
		#array("book_add_exalead_port", GetMessage("BOOK_ADD_EXALEAD_PORT"), false, array("text", 5)),
		array("book_add_url", GetMessage("BOOK_ADD_URL"), false, array("text", 50)),
		array("book_add_login", GetMessage("BOOK_ADD_LOGIN"), false, array("text", 20)),
		array("book_add_password", GetMessage("BOOK_ADD_PASSWORD"), false, array("text", 20)),
		array("book_add_key", GetMessage("BOOK_ADD_KEY"), false, array("text", 50)),
	);

    //https://redmine.notamedia.ru/issues/41818
    //https://redmine.notamedia.ru/issues/41946
    $arViewerOptionsEx = Array(
        array("viewer_version", 'Актуальная версия закрытого просмотровщика Windows', false, array("text", 10)),
        array("viewer_link", 'Адрес скачивания новой версии просмотровщика', false, array("text", 60)),
        array("viewer_link_mac", 'Адрес скачивания новой версии просмотровщика для OS X', false, array("text", 60)),
        array("viewer_ex_warning_show", 'Показывать окно «Предупреждение об ограниченном доступе»', false, array("checkbox")),
        array("viewer_ex_warning_text", 'Текст «Предупреждение об ограниченном доступе»', false, array('textarea', 10, 50)),
        array("viewer_ex_book_percent", 'Показывать какой процент книг', '10', array("text", 3)),
        array("viewer_ex_library_window_show", 'Показывать окно «подтверждение нахождения в библиотеке»', false, array("checkbox")),
        array("viewer_ex_library_window_text", 'Текст «подтверждение нахождения в библиотеке»', false, array('textarea', 10, 50)),
        array("viewer_ex_library_window_period", 'Периодичность показа окна «подтверждение нахождения в библиотеке»', false, array('selectbox',	array("1" => "По времени", "2" => "По страницам"))),
        array("viewer_ex_library_window_interval", 'Интервал между показами в секундах (не менее 5)/страницах', false, array("text", 5)),
        array("viewer_ex_library_window_grow", 'Нарастание количества показов', false, array("checkbox")),
        array("viewer_ex_library_window_grow_speed", 'Скорость нарастания в секундах/страницах', false, array("text", 5)),
        array("viewer_ex_books_show", 'Показывать книги после достижения заданного процента', false, array("checkbox")),
        array("viewer_ex_message", 'Сообщение, которое выводится после достижения заданного процента', false, array('textarea', 10,	50)),
    );

	$aTabs = array(
		array(
			"DIV" => "exalead_settings",
			"TAB" => GetMessage("EXALEAD_SETTINGS"),
			"TITLE" => GetMessage("EXALEAD_SETTINGS"),
			"OPTIONS" => array(),
		),
		array(
			"DIV" => "viewer_settings",
			"TAB" => GetMessage("VIEWER_SETTINGS"),
			"TITLE" => GetMessage("VIEWER_SETTINGS"),
			"OPTIONS" => array(),
		),
		array(
			"DIV" => "books_add",
			"TAB" => GetMessage("BOOKS_ADD_SETTINGS"),
			"TITLE" => GetMessage("BOOKS_ADD_SETTINGS"),
			"OPTIONS" => array(),
		),
        array(
            "DIV" => "viewer_settings_ex",
            "TAB" => 'Дополнительные настройки просмотровщика',
            "TITLE" => 'Дополнительные настройки просмотровщика',
            "OPTIONS" => array(),
        )
	);

	$tabControl = new CAdminTabControl("tabControl", $aTabs);

	if($REQUEST_METHOD=="POST" && strlen($Update.$Apply.$RestoreDefaults)>0 && check_bitrix_sessid())
	{
		if(strlen($RestoreDefaults)>0)
		{
			COption::RemoveOption($mid);
		}
		else
		{
			__AdmSettingsSaveOptions($mid, $arExaleadOptions);
			__AdmSettingsSaveOptions($mid, $arViewerOptions);
			__AdmSettingsSaveOptions($mid, $arBooksAddOptions);
			__AdmSettingsSaveOptions($mid, $arViewerOptionsEx);
		}

		if(strlen($Update)>0 && strlen($_REQUEST["back_url_settings"])>0)
			LocalRedirect($_REQUEST["back_url_settings"]);
		else
			LocalRedirect($APPLICATION->GetCurPage()."?mid=".urlencode($mid)."&lang=".urlencode(LANGUAGE_ID)."&back_url_settings=".urlencode($_REQUEST["back_url_settings"])."&".$tabControl->ActiveTabParam());
	}

	$tabControl->Begin();
?>

<form method="post" action="<?=$APPLICATION->GetCurPage()?>?mid=<?=urlencode($mid)?>&amp;lang=<?=LANGUAGE_ID?>">
	<?	
		$tabControl->BeginNextTab();
		__AdmSettingsDrawList($mid, $arExaleadOptions);

		$tabControl->BeginNextTab();	
		__AdmSettingsDrawList($mid, $arViewerOptions);	
		
		$tabControl->BeginNextTab();
		__AdmSettingsDrawList($mid, $arBooksAddOptions);

        $tabControl->BeginNextTab();
        __AdmSettingsDrawList($mid, $arViewerOptionsEx);

		$tabControl->Buttons();?>
	<input type="submit" name="Update" value="<?=GetMessage("MAIN_SAVE")?>" title="<?=GetMessage("MAIN_OPT_SAVE_TITLE")?>" class="adm-btn-save">
	<input type="submit" name="Apply" value="<?=GetMessage("MAIN_OPT_APPLY")?>" title="<?=GetMessage("MAIN_OPT_APPLY_TITLE")?>">
	<?if(strlen($_REQUEST["back_url_settings"])>0):?>
		<input type="button" name="Cancel" value="<?=GetMessage("MAIN_OPT_CANCEL")?>" title="<?=GetMessage("MAIN_OPT_CANCEL_TITLE")?>" onclick="window.location='<?echo htmlspecialcharsbx(CUtil::addslashes($_REQUEST["back_url_settings"]))?>'">
		<input type="hidden" name="back_url_settings" value="<?=htmlspecialcharsbx($_REQUEST["back_url_settings"])?>">
		<?endif?>
	<input type="submit" name="RestoreDefaults" title="<?echo GetMessage("MAIN_HINT_RESTORE_DEFAULTS")?>" OnClick="return confirm('<?echo AddSlashes(GetMessage("MAIN_HINT_RESTORE_DEFAULTS_WARNING"))?>')" value="<?echo GetMessage("MAIN_RESTORE_DEFAULTS")?>">
	<?=bitrix_sessid_post();?>
	<?$tabControl->End();?>
</form>
