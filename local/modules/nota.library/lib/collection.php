<?php
namespace Nota\Library;

\CModule::IncludeModule('iblock');
\CModule::IncludeModule('highloadblock');
use Bitrix\Highloadblock as HL;
\CModule::IncludeModule('nota.exalead');
\CModule::IncludeModule('nota.userdata');

use Nota\Exalead\Stat\StatBookReadTable;
use Nota\Exalead\Stat\StatBookViewTable;

class Collection{

    public static function add($name, $description, $background, $libraryID, $isShowInSlider, $isShowName){
        $section = new \CIBlockSection();
        $id = $section->Add(Array(
            'IBLOCK_ID' => IBLOCK_ID_COLLECTION,
            'NAME' => $name,
            'CODE' => \Cutil::translit($name, "ru"),
            'DETAIL_PICTURE' => \CFile::MakeFileArray($background),
            'DESCRIPTION' => $description,
            'UF_SLIDER' => $isShowInSlider,
            'UF_SHOW_NAME' => $isShowName,
            'UF_LIBRARY' => $libraryID,
        ));

        return $id ? true : $section->LAST_ERROR;
    }

    public static function getByID($id){

        $result = \CIBlockSection::GetList(Array(), Array('IBLOCK_ID' =>IBLOCK_ID_COLLECTION, 'ID' => $id), false, Array('UF_*'));
        $result = $result->Fetch();
        return $result ? $result : false;
    }

    public static function edit($id, $code, $name, $description, $background, $libraryID, $isShowInSlider, $isShowName){
        $section = new \CIBlockSection();
        $id = $section->Update($id, Array(
            'IBLOCK_ID' => IBLOCK_ID_COLLECTION,
            'NAME' => $name,
            'CODE' => $code,
            'DETAIL_PICTURE' => \CFile::MakeFileArray($background),
            'DESCRIPTION' => $description,
            'UF_SLIDER' => $isShowInSlider,
            'UF_SHOW_NAME' => $isShowName,
            'UF_LIBRARY' => $libraryID,
        ));

        return $id ? true : $section->LAST_ERROR;
    }

    public static function remove($collectionID){
        \CIBlockSection::Delete($collectionID);
    }

    public static function sortBooks($data){
        $element = new \CIBlockElement();
        foreach($data as $d){
            $element->Update($d['id'], Array(
                'SORT' => $d['sort']
            ));
        }
    }

    public static function addBook($collectionID, $bookID){
        $element = new \CIBlockElement();
        $element->Add(Array(
            'IBLOCK_ID' => IBLOCK_ID_COLLECTION,
            'IBLOCK_SECTION_ID' => $collectionID,
            'NAME' => 'Книга',
            'SORT' => 1,
            'PROPERTY_VALUES' => Array(
                'BOOK_ID' => $bookID,
                'TYPE_IN_SLIDER' => COLLECTION_SLIDER_TYPE_BASIC_ENUM_ID
            )
        ));
    }

    public static function toggleBookCover($bookID, $isCover){
        \CIBlockElement::SetPropertyValuesEx($bookID, false, Array('TYPE_IN_SLIDER' => Array('VALUE' => $isCover ? COLLECTION_SLIDER_TYPE_BASIC_ENUM_ID : COLLECTION_SLIDER_TYPE_COVER_ENUM_ID)));
    }

    public static function removeBook($bookID){
        \CIBlockElement::Delete($bookID);
    }

    public static function getBookStat($bookID, $viewOnly = false){
        //кол-во просмотров
        $rsData = StatBookViewTable::getList(array(
            "runtime" => array('CNT' => array('expression' => array('COUNT(*)'), 'data_type'=>'integer')),
            "select" => array('CNT'),
            "filter" => array('=BOOK_ID' => $bookID),
        ));
        $arData = $rsData->Fetch();
        $return['VIEW_CNT'] = (int)$arData['CNT'];

        if(!$viewOnly){
            //кол-во прочтений
            $rsData = StatBookReadTable::getList(array(
                "runtime" => array('CNT' => array('expression' => array('COUNT(*)'), 'data_type'=>'integer')),
                "select" => array('CNT'),
                "filter" => array('=BOOK_ID' => $bookID),
            ));
            $arData = $rsData->Fetch();
            $return['READ_CNT'] = (int)$arData['CNT'];

            //кол-во добавлений в избранное
            $hlblock = HL\HighloadBlockTable::getById(HIBLOCK_BOOKS_DATA_USERS)->fetch();
            $entity = HL\HighloadBlockTable::compileEntity($hlblock);
            $entity_data_class = $entity->getDataClass();
            $rsData = $entity_data_class::getList(array(
                "runtime" => array('CNT' => array('expression' => array('COUNT(*)'), 'data_type'=>'integer')),
                "select" => array('CNT'),
                "filter" => array('UF_BOOK_ID' => $bookID),
            ));
            $arData = $rsData->Fetch();
            $return['FAV_CNT'] = (int)$arData['CNT'];
        }
        return $return;
    }
}
