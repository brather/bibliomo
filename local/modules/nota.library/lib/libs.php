<?
	namespace Nota\Library;

	use Bitrix\Main\Entity;
	use Bitrix\Main\Localization\Loc;
	Loc::loadMessages(__FILE__);

	/**
	 * Class LibsTable
	 * 
	 * Fields:
	 * <ul>
	 * <li> ID int mandatory
	 * <li> UF_ID int optional
	 * <li> UF_PARENTID int optional
	 * <li> UF_NAME string optional
	 * <li> UF_DATE_OPEN string optional
	 * <li> UF_STRUCTURE string optional
	 * <li> UF_ADRESS string optional
	 * <li> UF_REGION string optional
	 * <li> UF_DISTRICT string optional
	 * <li> UF_TOWN string optional
	 * <li> UF_PHONE string optional
	 * <li> UF_COMMENT_1 string optional
	 * <li> UF_ADR_FIAS int optional
	 * <li> UF_REC_ID int optional
	 * <li> UF_ADMINISTRATIVEARE string optional
	 * <li> UF_LOCALITY string optional
	 * <li> UF_POS string optional
	 * <li> UF_AREA string optional
	 * <li> UF_AREA2 string optional
	 * <li> UF_DESCRIPTION string optional
	 * <li> UF_FULL_DESCRIPTION string optional
	 * <li> UF_CITY int optional
	 * <li> UF_DATE_JOIN datetime optional
	 * </ul>
	 *
	 * @package Bitrix\Libs
	 **/

	class LibsTable extends Entity\DataManager
	{
		/**
		 * Returns DB table name for entity.
		 *
		 * @return string
		 */
		public static function getTableName()
		{
			return 'neb_libs';
		}

		/**
		 * Returns entity map definition.
		 *
		 * @return array
		 */
		public static function getMap()
		{
			return array(
				'ID' => array(
					'data_type' => 'integer',
					'primary' => true,
					'autocomplete' => true,
					'title' => Loc::getMessage('LIBS_ENTITY_ID_FIELD'),
				),
				'UF_ID' => array(
					'data_type' => 'integer',
					'title' => Loc::getMessage('LIBS_ENTITY_UF_ID_FIELD'),
				),
				'UF_PARENTID' => array(
					'data_type' => 'integer',
					'title' => Loc::getMessage('LIBS_ENTITY_UF_PARENTID_FIELD'),
				),
				'UF_NAME' => array(
					'data_type' => 'text',
					'title' => Loc::getMessage('LIBS_ENTITY_UF_NAME_FIELD'),
				),
				'UF_DATE_OPEN' => array(
					'data_type' => 'text',
					'title' => Loc::getMessage('LIBS_ENTITY_UF_DATE_OPEN_FIELD'),
				),
				'UF_STRUCTURE' => array(
					'data_type' => 'text',
					'title' => Loc::getMessage('LIBS_ENTITY_UF_STRUCTURE_FIELD'),
				),
				'UF_ADRESS' => array(
					'data_type' => 'text',
					'title' => Loc::getMessage('LIBS_ENTITY_UF_ADRESS_FIELD'),
				),
				'UF_REGION' => array(
					'data_type' => 'text',
					'title' => Loc::getMessage('LIBS_ENTITY_UF_REGION_FIELD'),
				),
				'UF_DISTRICT' => array(
					'data_type' => 'text',
					'title' => Loc::getMessage('LIBS_ENTITY_UF_DISTRICT_FIELD'),
				),
				'UF_TOWN' => array(
					'data_type' => 'text',
					'title' => Loc::getMessage('LIBS_ENTITY_UF_TOWN_FIELD'),
				),
				'UF_PHONE' => array(
					'data_type' => 'text',
					'title' => Loc::getMessage('LIBS_ENTITY_UF_PHONE_FIELD'),
				),
				'UF_COMMENT_1' => array(
					'data_type' => 'text',
					'title' => Loc::getMessage('LIBS_ENTITY_UF_COMMENT_1_FIELD'),
				),
				'UF_ADR_FIAS' => array(
					'data_type' => 'integer',
					'title' => Loc::getMessage('LIBS_ENTITY_UF_ADR_FIAS_FIELD'),
				),
				'UF_REC_ID' => array(
					'data_type' => 'integer',
					'title' => Loc::getMessage('LIBS_ENTITY_UF_REC_ID_FIELD'),
				),
				'UF_ADMINISTRATIVEARE' => array(
					'data_type' => 'text',
					'title' => Loc::getMessage('LIBS_ENTITY_UF_ADMINISTRATIVEARE_FIELD'),
				),
				'UF_LOCALITY' => array(
					'data_type' => 'text',
					'title' => Loc::getMessage('LIBS_ENTITY_UF_LOCALITY_FIELD'),
				),
				'UF_POS' => array(
					'data_type' => 'text',
					'title' => Loc::getMessage('LIBS_ENTITY_UF_POS_FIELD'),
				),
				'UF_AREA' => array(
					'data_type' => 'text',
					'title' => Loc::getMessage('LIBS_ENTITY_UF_AREA_FIELD'),
				),
				'UF_AREA2' => array(
					'data_type' => 'text',
					'title' => Loc::getMessage('LIBS_ENTITY_UF_AREA2_FIELD'),
				),
				'UF_DESCRIPTION' => array(
					'data_type' => 'text',
					'title' => Loc::getMessage('LIBS_ENTITY_UF_DESCRIPTION_FIELD'),
				),
				'UF_FULL_DESCRIPTION' => array(
					'data_type' => 'text',
					'title' => Loc::getMessage('LIBS_ENTITY_UF_FULL_DESCRIPTION_FIELD'),
				),
				'UF_CITY' => array(
					'data_type' => 'integer',
					'title' => Loc::getMessage('LIBS_ENTITY_UF_CITY_FIELD'),
				),
				'UF_DATE_JOIN' => array(
					'data_type' => 'datetime',
					'title' => Loc::getMessage('LIBS_ENTITY_UF_DATE_JOIN_FIELD'),
				),
			);
		}
	}
?>