<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/iblock/admin_tools.php");
require_once(__DIR__ . "/../prolog.php");
\Bitrix\Main\Loader::IncludeModule(ADMIN_MODULE_NAME);
use Bitrix\NotaExt\Iblock\Element as IB;

?>

<?
$ID = CNotaLibrary::getLibraryID();
//TODO: что делать, если библиотека не установлена? такое может быть?
if($ID){

} else {
    //?
}


//TODO: переделать на константу. Почему в константах CODE вместо ID? IBLOCK_CODE_LIBRARY
$IBLOCK_ID = 4;

//CODE свойств, которые разрешено редактировать
$allowedProperties = Array(
    "ADDRESS",
    "SCHEDULE",
    "PHONE",
    "EMAIL",
    "SKYPE",
    "URL",
    "MAP",
    "CONTACTS",
    "VIEW_THEME",
    "VIEW_TEMPLATE",
);

$APPLICATION->AddHeadScript('/bitrix/js/iblock/iblock_edit.js');
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
?>

<?
$arIBlock = CIBlock::GetArrayByID($IBLOCK_ID);
$arIBlockElement = CIBlockElement::GetByID($ID);
$arIBlockElement = $arIBlockElement->Fetch();

$aTabs = array(
    array("DIV" => "edit1", "TAB" => $arIBlockElement["NAME"], "TITLE" => $arIBlockElement["NAME"])
);
//$tabControl = new CAdminForm("form_element_".$IBLOCK_ID, $aTabs);
$tabControl = new CAdminForm("tabControl", $aTabs, false);
$tabControl->SetShowSettings(false);


$tabControl->Begin(array("FORM_ACTION" => "/bitrix/admin/iblock_element_edit.php?return_url=".htmlspecialcharsbx($APPLICATION->GetCurPage())));
$tabControl->BeginNextFormTab();
?>

<?$tabControl->BeginEpilogContent();?>
<?=bitrix_sessid_post();?>
    <input type="hidden" name="Update" value="Y">
    <input type="hidden" name="type" value="library">
    <input type="hidden" name="ID" value="<?=$ID ?>">
    <input type="hidden" name="IBLOCK_ID" value="<?=$IBLOCK_ID ?>">
    <input type="hidden" name="CODE" value="<?=$arIBlockElement["CODE"] ?>">
    <input type="hidden" name="ACTIVE" value="<?=$arIBlockElement["ACTIVE"] ?>">
<? $tabControl->EndEpilogContent(); ?>

<?
$tabControl->AddEditField("NAME", "Библиотека:", true, array("size" => 50, "maxlength" => 255), $arIBlockElement["NAME"]);

$tabControl->BeginCustomField("PREVIEW_PICTURE", "Логотип", $arIBlock["FIELDS"]["PREVIEW_PICTURE"]["IS_REQUIRED"] === "Y");
if (!array_key_exists("PREVIEW_PICTURE", $_REQUEST) && $arIBlockElement) $str_PREVIEW_PICTURE = intval($arIBlockElement["PREVIEW_PICTURE"]);
?>
    <tr id="tr_PREVIEW_PICTURE" class="adm-detail-file-row">
        <td width="40%" class="adm-detail-valign-top"><? echo $tabControl->GetCustomLabelHTML() ?>:</td>
        <td width="60%">
            <?echo CFileInput::Show("PREVIEW_PICTURE", $str_PREVIEW_PICTURE,
                array(
                    "IMAGE" => "Y",
                    "PATH" => "Y",
                    "FILE_SIZE" => "Y",
                    "DIMENSIONS" => "Y",
                    "IMAGE_POPUP" => "Y",
                    "MAX_SIZE" => array(
                        "W" => COption::GetOptionString("iblock", "detail_image_size"),
                        "H" => COption::GetOptionString("iblock", "detail_image_size"),
                    ),
                ), array(
                    'upload' => true,
                    'medialib' => true,
                    'file_dialog' => true,
                    'cloud' => true,
                    'del' => true,
                    'description' => true,
                )
            );
            ?>
        </td>
    </tr>
<?
$tabControl->EndCustomField("PREVIEW_PICTURE", "");

$str_PREVIEW_TEXT_TYPE = $arIBlockElement["PREVIEW_TEXT_TYPE"];
$str_PREVIEW_TEXT = htmlspecialcharsbx($arIBlockElement["PREVIEW_TEXT"]);

$tabControl->BeginCustomField("PREVIEW_TEXT", GetMessage("IBLOCK_FIELD_PREVIEW_TEXT"), $arIBlock["FIELDS"]["PREVIEW_TEXT"]["IS_REQUIRED"] === "Y");
?>
    <tr class="heading" id="tr_PREVIEW_TEXT_LABEL">
        <td colspan="2"><? echo $tabControl->GetCustomLabelHTML() ?></td>
    </tr>

    <tr id="tr_PREVIEW_TEXT_EDITOR">
        <td colspan="2" align="center">
            <?CFileMan::AddHTMLEditorFrame(
                "PREVIEW_TEXT",
                $str_PREVIEW_TEXT,
                "PREVIEW_TEXT_TYPE",
                $str_PREVIEW_TEXT_TYPE,
                array(
                    'height' => 450,
                    'width' => '100%'
                ),
                "N",
                0,
                "",
                "",
                $arIBlock["LID"],
                true,
                false,
                array(
                    'toolbarConfig' => CFileman::GetEditorToolbarConfig("iblock_" . (defined('BX_PUBLIC_MODE') && BX_PUBLIC_MODE == 1 ? 'public' : 'admin')),
                    'saveEditorKey' => $IBLOCK_ID
                )
            );?>
        </td>
    </tr>

<?
$tabControl->EndCustomField("PREVIEW_TEXT",
    '<input type="hidden" name="PREVIEW_TEXT" value="' . $str_PREVIEW_TEXT . '">' .
        '<input type="hidden" name="PREVIEW_TEXT_TYPE" value="' . $str_PREVIEW_TEXT_TYPE . '">'
);

$arPROP_tmp = Array();
$properties = CIBlockProperty::GetList(Array("sort" => "asc", "name" => "asc"), Array("IBLOCK_ID" => "4", "ACTIVE" => "Y"));
while ($prop_fields = $properties->Fetch()) {
    $prop_values = Array();
    $prop_values_with_descr = Array();
    if (
        $bPropertyAjax
        && is_array($_POST["PROP"])
        && array_key_exists($prop_fields["ID"], $_POST["PROP"])
    ) {
        $prop_values = $_POST["PROP"][$prop_fields["ID"]];
        $prop_values_with_descr = $prop_values;
    } elseif ($bVarsFromForm) {
        if ($prop_fields["PROPERTY_TYPE"] == "F") {
            $db_prop_values = CIBlockElement::GetProperty($IBLOCK_ID, $ID, "id", "asc", Array("ID" => $prop_fields["ID"], "EMPTY" => "N"));
            while ($res = $db_prop_values->Fetch()) {
                $prop_values[$res["PROPERTY_VALUE_ID"]] = $res["VALUE"];
                $prop_values_with_descr[$res["PROPERTY_VALUE_ID"]] = array("VALUE" => $res["VALUE"], "DESCRIPTION" => $res["DESCRIPTION"]);
            }
        } elseif (is_array($PROP)) {
            if (array_key_exists($prop_fields["ID"], $PROP))
                $prop_values = $PROP[$prop_fields["ID"]];
            else
                $prop_values = $PROP[$prop_fields["CODE"]];
            $prop_values_with_descr = $prop_values;
        } else {
            $prop_values = "";
            $prop_values_with_descr = $prop_values;
        }
    } else {
        if ($historyId > 0) {
            $vx = $arResult["DOCUMENT"]["PROPERTIES"][(strlen(trim($prop_fields["CODE"])) > 0) ? $prop_fields["CODE"] : $prop_fields["ID"]];

            $prop_values = array();
            if (is_array($vx["VALUE"]) && is_array($vx["DESCRIPTION"])) {
                for ($i = 0, $cnt = count($vx["VALUE"]); $i < $cnt; $i++)
                    $prop_values[] = array("VALUE" => $vx["VALUE"][$i], "DESCRIPTION" => $vx["DESCRIPTION"][$i]);
            } else {
                $prop_values[] = array("VALUE" => $vx["VALUE"], "DESCRIPTION" => $vx["DESCRIPTION"]);
            }

            $prop_values_with_descr = $prop_values;
        } elseif ($ID > 0) {
            $db_prop_values = CIBlockElement::GetProperty($IBLOCK_ID, $ID, "id", "asc", Array("ID" => $prop_fields["ID"], "EMPTY" => "N"));
            while ($res = $db_prop_values->Fetch()) {
                if ($res["WITH_DESCRIPTION"] == "Y")
                    $prop_values[$res["PROPERTY_VALUE_ID"]] = Array("VALUE" => $res["VALUE"], "DESCRIPTION" => $res["DESCRIPTION"]);
                else
                    $prop_values[$res["PROPERTY_VALUE_ID"]] = $res["VALUE"];
                $prop_values_with_descr[$res["PROPERTY_VALUE_ID"]] = Array("VALUE" => $res["VALUE"], "DESCRIPTION" => $res["DESCRIPTION"]);
            }
        }
    }

    $prop_fields["VALUE"] = $prop_values;
    $prop_fields["~VALUE"] = $prop_values_with_descr;
    if (strlen(trim($prop_fields["CODE"])) > 0)
        $arPROP_tmp[$prop_fields["CODE"]] = $prop_fields;
    else
        $arPROP_tmp[$prop_fields["ID"]] = $prop_fields;
}
$PROP = $arPROP_tmp;

?>

<?foreach ($PROP as $prop_code => $prop_fields):

    if(!in_array($prop_code, $allowedProperties)) {
        $tabControl->BeginCustomField("PROPERTY_" . $prop_fields["ID"], $prop_fields["NAME"], $prop_fields["IS_REQUIRED"] === "Y");
        echo  _ShowHiddenValue('PROP[' . $prop_fields["ID"] . ']', $prop_fields["VALUE"]);
        $tabControl->EndCustomField("PROPERTY_" . $prop_fields["ID"]);
        continue;
    }

    $prop_values = $prop_fields["VALUE"];
    $tabControl->BeginCustomField("PROPERTY_" . $prop_fields["ID"], $prop_fields["NAME"], $prop_fields["IS_REQUIRED"] === "Y");
    ?>
    <tr id="tr_PROPERTY_<? echo $prop_fields["ID"]; ?>"<? if ($prop_fields["PROPERTY_TYPE"] == "F"): ?> class="adm-detail-file-row"<? endif ?>>
        <td class="adm-detail-valign-top" width="40%"><?if ($prop_fields["HINT"] != ""):
                ?><span id="hint_<? echo $prop_fields["ID"]; ?>"></span>
                <script
                    type="text/javascript">BX.hint_replace(BX('hint_<?echo $prop_fields["ID"];?>'), '<?echo CUtil::JSEscape($prop_fields["HINT"])?>');</script>&nbsp;<?
            endif;?><?echo $tabControl->GetCustomLabelHTML();?>:
        </td>
        <td width="60%"><? _ShowPropertyField('PROP[' . $prop_fields["ID"] . ']', $prop_fields, $prop_fields["VALUE"], (($historyId <= 0) && (!$bVarsFromForm) && ($ID <= 0)), $bVarsFromForm || $bPropertyAjax, 50000, $tabControl->GetFormName(), $bCopy); ?></td>
    </tr>
    <?
    $hidden = "";
    if (!is_array($prop_fields["~VALUE"]))
        $values = Array();
    else
        $values = $prop_fields["~VALUE"];
    $start = 1;
    foreach ($values as $key => $val) {
        if ($bCopy) {
            $key = "n" . $start;
            $start++;
        }

        if (is_array($val) && array_key_exists("VALUE", $val)) {
            $hidden .= _ShowHiddenValue('PROP[' . $prop_fields["ID"] . '][' . $key . '][VALUE]', $val["VALUE"]);
            $hidden .= _ShowHiddenValue('PROP[' . $prop_fields["ID"] . '][' . $key . '][DESCRIPTION]', $val["DESCRIPTION"]);
        } else {
            $hidden .= _ShowHiddenValue('PROP[' . $prop_fields["ID"] . '][' . $key . '][VALUE]', $val);
            $hidden .= _ShowHiddenValue('PROP[' . $prop_fields["ID"] . '][' . $key . '][DESCRIPTION]', "");
        }
    }
    $tabControl->EndCustomField("PROPERTY_" . $prop_fields["ID"], $hidden);
endforeach;?>

<?ob_start();?>
    <input type="submit" class="adm-btn-save" name="save" id="save" value="Сохранить">
<?
$buttons_add_html = ob_get_contents();
ob_end_clean();
$tabControl->Buttons(false, $buttons_add_html);

$tabControl->Show();
?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php"); ?>