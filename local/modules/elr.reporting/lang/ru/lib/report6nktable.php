<?
$MESS["REPORTING_ENTITY_ID_FIELD"] = "Идентификатор";
$MESS["REPORTING_ENTITY_LIBRARY_ID_FIELD"] = "Библиотека";
$MESS["REPORTING_ENTITY_USER_ID_FIELD"] = "ID пользователя";
$MESS["REPORTING_ENTITY_DATE_CREATE_FIELD"] = "Дата создания записи";
$MESS["REPORTING_ENTITY_COMMENT_FIELD"] = "Комментарий";
$MESS["ELR_6NK_ID_FILE_PDF"] = "ID файла PDF";
$MESS["ELR_6NK_ID_FILE_WORD"] = "ID файла Word";
$MESS["ELR_6NK_ID_FILE_EXCEL"] = "ID файла Excel";
$MESS["ELR_6NK_CONFIRMED"] = "Подтверждено";
?>