<?php
/**
 * Created by PhpStorm.
 * User: EfremovDm
 * Date: 19.12.2016
 * Time: 15:22
 */
namespace Elr\Reporting;

use \Bitrix\Main\Entity,
    \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

/**
 * Class Report6nkValuesTable
 * @package Elr\Reporting
 **/

class Report6nkValuesTable extends Entity\DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'elr_report_6nk_values';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
                'title' => Loc::getMessage('REPORTING_ENTITY_ID_FIELD'),
            ),
            'REPORT_ID' => array(
                'data_type' => 'integer',
                //'required' => true,
                //'validation' => array(__CLASS__, 'validateTaskId'),
                'title' => Loc::getMessage('REPORTING_ENTITY_REPORT_ID_FIELD'),
            ),
            'STRING' => array(
                'data_type' => 'string',
                //'validation' => array(__CLASS__, 'validateLibrary'),
                'title' => Loc::getMessage('REPORTING_ENTITY_STRING_FIELD'),
            ),
            'NUMBER' => array(
                'data_type' => 'string',
                'required' => true,
                'title' => Loc::getMessage('REPORTING_ENTITY_NUMBER_FIELD'),
            ),
            'VALUE' => array(
                'data_type' => 'string',
                'title' => Loc::getMessage('REPORTING_ENTITY_VALUE_FIELD'),
            )
        );


    }
    /**
     * Returns validators for TASK_ID field.
     *
     * @return array
     */
    public static function validateTaskId()
    {
        return array(
            new Entity\Validator\Length(null, 50),
        );
    }
    /**
     * Returns validators for LIBRARY field.
     *
     * @return array
     */
    public static function validateLibrary()
    {
        return array(
            new Entity\Validator\Length(null, 25),
        );
    }
}