<?php
/**
 * Created by PhpStorm.
 * User: EfremovDm
 * Date: 19.12.2016
 * Time: 15:22
 */
namespace Elr\Reporting;

use \Bitrix\Main\Entity,
    \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

/**
 * Class Report6nkTable
 * @package Elr\Reporting
 **/

class Report6nkTable extends Entity\DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'elr_report_6nk';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
                'title' => Loc::getMessage('REPORTING_ENTITY_ID_FIELD'),
            ),
            'LIBRARY_ID' => array(
                'data_type' => 'integer',
                //'required' => true,
                //'validation' => array(__CLASS__, 'validateTaskId'),
                'title' => Loc::getMessage('REPORTING_ENTITY_LIBRARY_ID_FIELD'),
            ),
            'USER_ID' => array(
                'data_type' => 'integer',
                //'validation' => array(__CLASS__, 'validateLibrary'),
                'title' => Loc::getMessage('REPORTING_ENTITY_USER_ID_FIELD'),
            ),
            'YEAR' => array(
                'data_type' => 'integer',
                //'validation' => array(__CLASS__, 'validateLibrary'),
                'title' => 'Год',
            ),
            new Entity\DatetimeField('DATE_UPDATE',
                array(
                    'title' => 'Дата и время обновления'
                )
            ),
            'DATE_CREATE' => array(
                'data_type' => 'datetime',
                //'required' => true,
                'title' => Loc::getMessage('REPORTING_ENTITY_DATE_CREATE_FIELD'),
            ),
            'COMMENT' => array(
                'data_type' => 'string',
                'title' => Loc::getMessage('REPORTING_ENTITY_COMMENT_FIELD'),
            ),
            new Entity\IntegerField('ID_PDF',
                array(
                    'title' => Loc::getMessage( "ELR_6NK_ID_FILE_PDF" )
                )
            ),
            new Entity\IntegerField('ID_WORD',
                array(
                    'title' => Loc::getMessage( "ELR_6NK_ID_FILE_WORD" )
                )
            ),
            new Entity\IntegerField('ID_EXCEL',
                array(
                    'title' => Loc::getMessage( "ELR_6NK_ID_FILE_EXCEL" )
                )
            ),
            new Entity\IntegerField('SCAN_FILE',
                array(
                    'title' => "ID загруженного файла"
                )
            ),
            new Entity\IntegerField('CONFIRMED',
                array(
                    'title' => Loc::getMessage( "ELR_6NK_CONFIRMED" )
                )
            ),
            new Entity\ReferenceField(
                'VALUES',
                '\Elr\Reporting\Report6nkValues',
                array('=this.ID' => 'ref.REPORT_ID')
            ),
        );


    }
    /**
     * Returns validators for TASK_ID field.
     *
     * @return array
     */
    public static function validateTaskId()
    {
        return array(
            new Entity\Validator\Length(null, 50),
        );
    }
    /**
     * Returns validators for LIBRARY field.
     *
     * @return array
     */
    public static function validateLibrary()
    {
        return array(
            new Entity\Validator\Length(null, 25),
        );
    }
}