<?php
/**
 * Created by PhpStorm.
 * User: EfremovDm
 * Date: 19.12.2016
 * Time: 15:22
 */
CModule::AddAutoloadClasses(
    "elr.reporting",
    array(
        'Elr\Reporting\Report6nkTable'       => 'lib/report6nktable.php',
        'Elr\Reporting\Report6nkValuesTable' => 'lib/report6nkvaluestable.php',
        'Elr\Reporting\ReportData'           => 'lib/reportData.php',
    )
);