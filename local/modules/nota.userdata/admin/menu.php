<?php
	use Bitrix\Main\Localization\Loc;
	Loc::loadMessages(__FILE__);

	global $USER;
	if (!$USER->IsAdmin())
		return false; 

	$aMenu[] = array(
		'parent_menu' => 'global_menu_settings',
		'sort'        => 10,
		'text'        => 'Экспорт пользователей',
		'title'       => 'Экспорт пользователей прошедших полную регистрацию',
		'url'         => 'userdata_export.php'.'?lang='.LANGUAGE_ID,
		'items_id'    => 'userdata_export',
		   
	);

	return $aMenu;
?>