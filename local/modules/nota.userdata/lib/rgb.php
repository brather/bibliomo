<?
	/*
	Работа с пользователем RGB
	*/
	namespace Nota\UserData;

	class rgb
	{
		static $url_add_user = 'https://relar.rsl.ru/external_users_api/verification_neb_user';
		static $secretKey = 'faladsf0213954fqaoptierhgoslq';

		static $CONSUMER_KEY 	= "elar";
		static $CONSUMER_SECRET = "pN22Wv1wNGTT";
		static $RgbUrl 			= 'http://sub.rsl.ru/xmlrpc';
		static $RgbUrl2 		= 'https://relar.rsl.ru/sub/xmlrpc';
		//static $RgbUrl2 = 'http://test.sub.rsl.ru/xmlrpc';

		/*
		@method add - добавление пользователя в RGB
		@param 	 $arFields	
		Для регистрации нужно отправить POST-запрос со следующими параметрами:
		 secretKey - ключ доступа (sdaf;hl43521=asdf)
		 uid - идентификатор пользователя, если пользователь уже регистрировался в РГБ. Если не
		регистрировался, то передавать данный параметр не нужно.
		 lastname - фамилия
		 firstname - имя
		 middlename - отчество
		 birthday - дата рождения (формат: dd/mm/yyyy)
		 citizenship - гражданство, номер из справочника
		 residence - проживание, номер из справочника
		 branch_of_knowledge - отрасль знаний, номер из справочника
		 reader_category - образование, номер из справочника
		 passport_number - номер паспорта. Пример: 1234 123456
		 email - электронная почта
		 password - пароль пользователя. Минимум 6 символов. Если передается uid, то
		спрашивать у пользователя и передавать пароль не нужно.
		 mobile_phone - мобильный телефон. Формат: +7(123)1234567
		 registration_address - адрес регистрации. Строка
		 home_address - адрес проживания. Строка
		 job_study_place - место работы или учебы. Строка
		 gender - пол (male или female)
		Ответ от сервера в формате json:
		{
		result: true/false (результат регистрации)
		errors: [] (массив с ошибками, если result = false
		}
		*/
		public static function add($arFields)
		{
			if(empty($arFields) or !is_array($arFields))
				return false;

			$arFieldsVar = array(
				'uid',
				'lastname',
				'firstname',
				'middlename',
				'birthday',
				'branch_of_knowledge',
				'reader_category',
				'passport_number',
				'email',
				'password',
				'mobile_phone',
				'registration_address',
				'home_address',
				'job_study_place',
				'gender',
			);	


			TrimArr($arFields);	

			$arPostFields = array(
				'secretKey' => self::$secretKey,
				'citizenship' => 134,
				'residence' => 1,
				'gender' => 'male',
			);	

			foreach($arFieldsVar as $key)
			{
				if(!empty($arFields[$key]))
					$arPostFields[$key] = trim($arFields[$key]);
			}

			$http = new \Bitrix\Main\Web\HttpClient();
			$strQueryText = $http->post(self::$url_add_user, $arPostFields);

			if(empty($strQueryText))
				return false;

			return json_decode($strQueryText, true);
		}

		/*		
		Добавление пользователя в RGB по ID Битрикса
		@method addUserBx		
		@param $UID int
		*/
		public function addUserBx($UID)
		{
			$UID = intval($UID);
			if($UID <= 0)
				return false;

			$nebUser = new \nebUser($UID);
			$arRes = $nebUser->getUser();

			$branch_of_knowledge = '';
			if(!empty($arRes['UF_BRANCH_KNOWLEDGE'])){
				$arBranch_of_knowledge = $nebUser->getFieldEnum(array('USER_FIELD_NAME' => 'UF_BRANCH_KNOWLEDGE', 'ID' => $arRes['UF_BRANCH_KNOWLEDGE']));
				if(!empty($arBranch_of_knowledge[0]['XML_ID']))
					$branch_of_knowledge = $arBranch_of_knowledge[0]['XML_ID'];
			}
			$reader_category = '';
			if(!empty($arRes['UF_BRANCH_KNOWLEDGE'])){
				$arReader_category = $nebUser->getFieldEnum(array('USER_FIELD_NAME' => 'UF_EDUCATION', 'ID' => $arRes['UF_EDUCATION']));
				if(!empty($arReader_category[0]['XML_ID']))
					$reader_category = $arReader_category[0]['XML_ID'];
			}

			$arFields = array(
				'lastname'				=> $arRes['LAST_NAME'],
				'firstname'				=> $arRes['NAME'],
				'middlename'			=> $arRes['SECOND_NAME'],
				'birthday'				=> ConvertDateTime($arRes['PERSONAL_BIRTHDAY'], 'DD/MM/YYYY'),
				'branch_of_knowledge' 	=> $branch_of_knowledge,
				'reader_category' 		=> $reader_category,
				'passport_number'		=> $arRes['UF_PASSPORT_NUMBER'].' '.$arRes['UF_PASSPORT_SERIES'],
				'email' 				=> $arRes['EMAIL'],
				'password' 				=> $arRes['PERSONAL_NOTES'],
				'mobile_phone' 			=> $arRes['PERSONAL_MOBILE'],
				'registration_address'	=> $arRes['PERSONAL_ZIP'].' '.$arRes['PERSONAL_CITY'].' '.$arRes['PERSONAL_STREET'].' '.$arRes['UF_CORPUS'].' '.$arRes['UF_STRUCTURE'].' '.$arRes['UF_FLAT'],
				'home_address' 			=> $arRes['WORK_ZIP'].' '.$arRes['WORK_CITY'].' '.$arRes['WORK_STREET'].' '.$arRes['UF_HOUSE2'].' '.$arRes['UF_STRUCTURE2'].' '.$arRes['UF_FLAT2'],
				'job_study_place' 		=> $arRes['WORK_COMPANY'],
			);

			$res = self::add($arFields);

			if(empty($res))
				return false;

			if((bool)$res['result'] === false)
				return $res;

			if((bool)$res['result'] === true)
			{
				/*			
				Если пользватель успешно добавлен в РГБ, попробуем узнать и записать его АЙДИ
				*/
				$response = self::getUserByCredential($arRes['EMAIL'], $arRes['PERSONAL_NOTES']);
				if(intval($response['uid']) > 0)
					$nebUser->Update($arRes['ID'], array('UF_RGB_USER_ID' => $response['uid'], 'UF_LIBRARY' => RGB_LIB_ID));

				return $response;
			}	

		}

		/*
		@method getUserByCredential получить информацию о пользователе по логину и паролю
		@param credential значение поля
		@param password 
		@param credential_field тип поля. Возможные значения: email, login, reader_number

		200000432234/123456
		
		Если пользователь не найден, возвращается ошибка server fault: 
		101 – неверное название поля credential_field
		404 – пользователь не найден 
		500 - системная ошибка
		*/
		public static function getUserByCredential($credential, $password, $credential_field = 'email')
		{
			return self::sendRgbReguest('sub.getUserByCredential', array($credential, $password, $credential_field));
		}

		/* метод еще не реализован в РГБ*/
		public static function getUserByUid($uidRgb)
		{
			return self::sendRgbReguest('sub.getUserByUid', array($uidRgb.''));
		}

		public static function isUserActive($uidRgb)
		{
			return self::sendRgbReguest('sub.isUserActive', array($uidRgb.''));
		}

		/*		
		Отправляем\получаем данные из РГБ согласно указанному методу
		*/
		private function sendRgbReguest($method, $arParams)
		{
			if(empty($method) or empty($arParams))
				return false;

			require_once(__DIR__.'/../extlibs/OAuth.php');

			# Initialize OAuth Consumer
			$consumer = new \OAuthConsumer(self::$CONSUMER_KEY, self::$CONSUMER_SECRET);

			# Prepare the request

			$request = \OAuthRequest::from_consumer_and_token($consumer, NULL, 'POST', self::$RgbUrl, NULL);
			$request->sign_request(new \OAuthSignatureMethod_HMAC_SHA1(), $consumer, NULL); 
			$auth_header = $request->to_header('api');

			$request = xmlrpc_encode_request($method, $arParams);

			#Отправляем запрос
			$curl = curl_init(self::$RgbUrl2);

			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $request);

			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_FAILONERROR, false);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl, CURLOPT_ENCODING, 'gzip'); 
			curl_setopt($curl, CURLOPT_HTTPHEADER, array($auth_header, 'Content-Type: text/xml', 'User-Agent: xmlrpclibPHP.py/1.0.1'));

			curl_setopt($curl, CURLINFO_HEADER_OUT, true);

			# Fetch the event
			$response = curl_exec($curl);
			#$info = curl_getinfo($curl);
			#$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
			curl_close($curl);

			if(empty($response))
				return false;

			$response = xmlrpc_decode($response);
			return $response;
		}

        public function pushUserIntoRGB($user){

            if(is_object($user)):
                $nebUser = $user;
            else:
                $nebUser = new \nebUser($user);
            endif;

            $arRes = $nebUser->getUser();

            $branch_of_knowledge = '';
            if(!empty($arRes['UF_BRANCH_KNOWLEDGE'])){
                $arBranch_of_knowledge = $nebUser->getFieldEnum(array('USER_FIELD_NAME' => 'UF_BRANCH_KNOWLEDGE', 'ID' => $arRes['UF_BRANCH_KNOWLEDGE']));
                if(!empty($arBranch_of_knowledge[0]['XML_ID']))
                    $branch_of_knowledge = $arBranch_of_knowledge[0]['XML_ID'];
            }
            $reader_category = '';
            if(!empty($arRes['UF_BRANCH_KNOWLEDGE'])){
                $arReader_category = $nebUser->getFieldEnum(array('USER_FIELD_NAME' => 'UF_EDUCATION', 'ID' => $arRes['UF_EDUCATION']));
                if(!empty($arReader_category[0]['XML_ID']))
                    $reader_category = $arReader_category[0]['XML_ID'];
            }

            $arFields = array('fields' => array(
                'lastname'				=> $arRes['LAST_NAME'],
                'firstname'				=> $arRes['NAME'],
                'middlename'			=> $arRes['SECOND_NAME'],
                'birthday'				=> ConvertDateTime($arRes['PERSONAL_BIRTHDAY'], 'DD/MM/YYYY'),
                'email'                 => $arRes['EMAIL'],
                'mobile_phone'          => $arRes['PERSONAL_MOBILE'],
                'citizenship'           => $arRes['UF_CITIZENSHIP'],
                'branch_of_knowledge' 	=> $branch_of_knowledge,
                'reader_category' 		=> $reader_category,
                'passport_number'		=> $arRes['UF_PASSPORT_NUMBER'].' '.$arRes['UF_PASSPORT_SERIES'],
                'registration_address'	=> $arRes['PERSONAL_ZIP'].' '.$arRes['PERSONAL_CITY'].' '.$arRes['PERSONAL_STREET'].' '.$arRes['UF_CORPUS'].' '.$arRes['UF_STRUCTURE'].' '.$arRes['UF_FLAT'],
                'home_address' 			=> $arRes['WORK_ZIP'].' '.$arRes['WORK_CITY'].' '.$arRes['WORK_STREET'].' '.$arRes['UF_HOUSE2'].' '.$arRes['UF_STRUCTURE2'].' '.$arRes['UF_FLAT2'],
                'job_study_place' 		=> $arRes['WORK_COMPANY'],

            ));


        /*
         1. В РГБ по умолчанию, пароль дата рождения в формате ДДММГГГГ.
        Сразу после записи читателя будем высылать ему письмо с просьбой сменить его.
        */
            if(!$arRes['UF_RGB_USER_ID']):
                $arFields['fields']['password'] = ConvertDateTime($arRes['PERSONAL_BIRTHDAY'], 'DDMMYYYY');
            endif;

        /*
        photo[userPassportFirstPage] - base64 закодированная первая страница паспорта
        photo[userPassportFirstPageExt] - расширение файла первой страницы паспорта (png, jpeg...)
        photo[userPassportSecondPage]] - base64 закодированная вторая страница паспорта
        photo[userPassportSecondPageExt] - расширение файла второй страницы паспорта (png, jpeg...)
        */
            $arFields['photo'] = array(
                'userPassportFirstPage'     => base64_encode(file_get_contents($_SERVER["DOCUMENT_ROOT"].$_REQUEST['scan1'])),
                'userPassportFirstPageExt'  => pathinfo($_SERVER["DOCUMENT_ROOT"].$_REQUEST['scan1'], PATHINFO_EXTENSION),

                'userPassportSecondPage'    => base64_encode(file_get_contents($_SERVER["DOCUMENT_ROOT"].$_REQUEST['scan2'])),
                'userPassportSecondPageExt' => pathinfo($_SERVER["DOCUMENT_ROOT"].$_REQUEST['scan2'], PATHINFO_EXTENSION),
                /*
            2. Поля про студентов опциональные, передавайте пустые значения
             */
                'userStudentPage' => '',
                'userStudentPageExt' => '',
            );


            $arFields['secretKey'] = self::$secretKey;


            $http = new \Bitrix\Main\Web\HttpClient();
            $result = json_decode($http->post(self::$url_add_user, $arFields), true);

            if(isset($result['id_request']) && ($result['id_request'])):
                $fields = Array(
                    "UF_ID_REQUEST" => $result['id_request'],
                );
                $nebUser->Update($user->USER_ID, $fields);

            endif;

            return $result;

        }//public function pushUserIntoRGB($user)
	}

?>