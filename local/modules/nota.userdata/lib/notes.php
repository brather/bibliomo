<?
	namespace Nota\UserData;

	\CModule::IncludeModule("highloadblock"); 
	use Bitrix\Highloadblock as HL; 
	use Bitrix\Main\Entity; 
	use Bitrix\Main\Type\DateTime;

	\CModule::IncludeModule('nota.exalead');
	use Nota\Exalead\SearchQuery;
	use Nota\Exalead\SearchClient;

    \CModule::IncludeModule('nota.journal');
    use Nota\Journal\J;

	/**
	 * Class Notes
	 * @package Nota\UserData
	 */
	class Notes
	{
		private function getEntity_data_class($HIBLOCK = HIBLOCK_NOTES_DATA_USERS){
			$hlblock = HL\HighloadBlockTable::getById($HIBLOCK)->fetch();
			$entity = HL\HighloadBlockTable::compileEntity($hlblock); 
			$entity_data_class = $entity->getDataClass();
			return $entity_data_class; 
		}

		public function getNotesList($bookID = false, $numPage = false, $uid = false)
		{
			global $USER;
			if ( !$uid )
				$uid = $USER->GetID();
			
			$entity_data_class = self::getEntity_data_class();
			$arFilter = array('UF_UID' => $uid);
			
			if ( (bool)$bookID )
				$arFilter['UF_BOOK_ID'] = $bookID;
				
			if ( (bool)$numPage )
				$arFilter['UF_NUM_PAGE'] = $numPage;
				
			$rsData = $entity_data_class::getList(array(
				"select" => array(
					'ID', 'UF_BOOK_ID', 'UF_NUM_PAGE', 'UF_TEXT', 'UF_NOTE_AREA',
					'UF_IMG_DATA', 'UF_TOP', 'UF_LEFT', 'UF_WIDTH', 'UF_HEIGHT', 'UF_DATE_ADD'
				),
				"filter" => $arFilter,
			));
			
			$arResult = array();
			
			if($rsData->getSelectedRowsCount() <= 0)
				return false;

			while($arData = $rsData->Fetch())
				$arResult[$arData['ID']] = array(
					'ID' 		=> $arData['ID'],
					'BOOK_ID' 	=> $arData['UF_BOOK_ID'],
					'NUM_PAGE' 	=> $arData['UF_NUM_PAGE'],
					'TEXT' 		=> $arData['UF_TEXT'],
					'NOTE_AREA' => $arData['UF_NOTE_AREA'],
					'IMG_DATA' 	=> $arData['UF_IMG_DATA'],
					'TOP' 		=> $arData['UF_TOP'],
					'LEFT' 		=> $arData['UF_LEFT'],
					'WIDTH' 	=> $arData['UF_WIDTH'],
					'HEIGHT' 	=> $arData['UF_HEIGHT'],
					'DATE_ADD' 	=> $arData['UF_DATE_ADD'],
				);
			
			return $arResult;
		}

		/**
		 * Получаем список заметок в книге для пользователя
		 *
		 * @param $bookID
		 * @param bool $uid
		 *
		 * @return array|bool
		 */
		public static function getNotesListJSON($bookID, $uid = false)
		{
			global $USER;

			if ( !$uid )
				$uid = $USER->GetID();

			if ( !(bool)$bookID || !(bool)$uid )
				return false;

			// Получаем список заметок
			$userNotesArray = self::getNotesList($bookID, false, $uid);
			if ( empty($userNotesArray) )
				return false;

			// Приводим ключики к нужному виду
			$jsonArray = array();
			foreach ( $userNotesArray as $userNote )
			{
				$jsonArray[] = array(
					'id' 			=> $userNote['ID'],
					'notepage' 		=> $userNote['NUM_PAGE'],
					'note' 			=> $userNote['TEXT'],
					'notearea' 		=> $userNote['NOTE_AREA'],
					'noteimg' 		=> $userNote['IMG_DATA'],
					'notetop' 		=> $userNote['TOP'],
					'noteleft' 		=> $userNote['LEFT'],
					'notewidth' 	=> $userNote['WIDTH'],
					'noteheight' 	=> $userNote['HEIGHT'],
				);
			}

			return $jsonArray;
		}

		/**
		 * @method add добавление цитаты
		 * @param $bookId
		 * @param $numPage
		 * @param $text
		 * @param bool $uid
		 * @param $arDataFields - Массив дополнительных параметов
		 * @param $arDataFields['NOTE_AREA'] - выделенный текст
		 * @param $arDataFields['IMG_DATA'] - data image base64
		 * @param $arDataFields['TOP'] - левый верхний угол: Y
		 * @param $arDataFields['LEFT'] - левый верхний угол: X
		 * @param $arDataFields['WIDTH'] - ширина блока выделения/картинки
		 * @param $arDataFields['HEIGHT'] - высота блока выделения/картинки
		 *
		 * @return int
		 */
		public function add($bookId, $numPage, $text, $uid = false, $arDataFields = array())
		{
			global $USER;
			if ( !$uid )
				$uid = $USER->GetID();
			
			/** Может быть несколько заметок на странице
			$noteObject = self::getNotesList($bookId, $numPage, $uid);
			if($noteObject !== false)	
				return $noteObject['ID'];
			*/

			$query = new SearchQuery();
			$query->getById($bookId);

			$client = new SearchClient();
			$resExalead = $client->getResult($query);

			$entity_data_class = self::getEntity_data_class();
			$dt = new DateTime();

			$arFields = array(
				'UF_UID'			=> $uid,
				'UF_BOOK_ID'		=> $bookId,
				'UF_NUM_PAGE'		=> $numPage,
				'UF_TEXT'			=> $text,
				'UF_DATE_ADD'		=> $dt,
				'UF_DATE_UPDATE' 	=> $dt,
				'UF_NOTE_AREA' 		=> isset($arDataFields['NOTE_AREA'])	? $arDataFields['NOTE_AREA'] : '',
				'UF_IMG_DATA' 		=> isset($arDataFields['IMG_DATA']) 	? $arDataFields['IMG_DATA'] : '',
				'UF_TOP' 			=> isset($arDataFields['TOP']) 			? $arDataFields['TOP'] : '',
				'UF_LEFT' 			=> isset($arDataFields['LEFT']) 		? $arDataFields['LEFT'] : '',
				'UF_WIDTH' 			=> isset($arDataFields['WIDTH']) 		? $arDataFields['WIDTH'] : '',
				'UF_HEIGHT' 		=> isset($arDataFields['HEIGHT']) 		? $arDataFields['HEIGHT'] : '',
				'UF_BOOK_NAME'		=> $resExalead['title'],
				'UF_BOOK_AUTHOR'	=> $resExalead['authorbook'],
			);

            J::add('note', 'add', Array('BOOK_ID' => $bookId, 'DATE' => $dt, 'TEXT' => $text, 'USER_COLLECTION' => $uid));

			$result = $entity_data_class::add($arFields);
			$saveID = $result->getId();
			return $saveID;
		}

		/**
		 * Для api был добавлен метод add(), на тот момент список его параметров был актуален
		 * Для добавления из массива добавлен метод addFromArray, который уже использует add()
		 *
		 * @method addFromArray добавление цитаты
		 * @param $arFields - Массив дополнительных параметов
		 * @param $arFields['BOOK_ID'] - выделенный текст
		 * @param $arFields['NUM_PAGE'] - выделенный текст
		 * @param $arFields['NOTE'] - выделенный текст
		 * @param $arFields['UID'] - выделенный текст
		 * @param $arFields['NOTE_AREA'] - выделенный текст
		 * @param $arFields['IMG_DATA'] - data image base64
		 * @param $arFields['TOP'] - левый верхний угол: Y
		 * @param $arFields['LEFT'] - левый верхний угол: X
		 * @param $arFields['WIDTH'] - ширина блока выделения/картинки
		 * @param $arFields['HEIGHT'] - высота блока выделения/картинки
		 *
		 * @return int
		 */
		public static function addFromArray($arFields)
		{
			return self::add($arFields['BOOK_ID'], $arFields['NUM_PAGE'], $arFields['NOTE'], $arFields['UID'], $arFields);
		}

		/**
		* Удаляет по паре UID + ID, заведомо проверив, реально ли эта запись принадлежит пользователю
		*/
		public function deleteWithTest($id, $uid)
		{
			$entity_data_class = self::getEntity_data_class();
			$toDelete = $entity_data_class::getList(array(
				"select" => array("ID"),
				"filter" => array(
					'ID' 		=> $id,
					'UF_UID' 	=> $uid,
				),
			))->Fetch();
			
			if ( !$toDelete )
				return false;

			self::delete($toDelete['ID']);
			return true;
		}

		public static function delete($id)
		{
			$id = intval($id);
			if($id <= 0)
				return false;

			$note = self::getById($id);
			if(empty($note))
				return false;

            J::add('note', 'delete', Array('BOOK_ID' => $note['UF_BOOK_ID'], 'DATE' => new DateTime(), 'TEXT' => $note['UF_TEXT'], 'USER_COLLECTION' => $note['UF_UID']));

			$entity_data_class = self::getEntity_data_class();
			$entity_data_class::Delete($note['ID']);
			Collections::removeLinkObject($note['ID'], false, 'notes');
		}

		public function getById($ID)
		{
			global $USER;
			if(empty($ID))
				return false;

			$entity_data_class = self::getEntity_data_class();

			$rsData = $entity_data_class::getList(array(
				"select" => array('*'),
				"filter" => array(
					#'UF_UID' 		=> $USER->GetID(),
					'=ID'   			=> $ID
				),
			));

			return $rsData->Fetch();
		}

		/**
		 * @param $ID
		 * @param $arDataFields
		 * @param $arDataFields['BOOK_ID'] - выделенный текст
		 * @param $arDataFields['NUM_PAGE'] - выделенный текст
		 * @param $arDataFields['NOTE'] - выделенный текст
		 * @param $arDataFields['UID'] - выделенный текст
		 * @param $arDataFields['NOTE_AREA'] - выделенный текст
		 * @param $arDataFields['IMG_DATA'] - data image base64
		 * @param $arDataFields['TOP'] - левый верхний угол: Y
		 * @param $arDataFields['LEFT'] - левый верхний угол: X
		 * @param $arDataFields['WIDTH'] - ширина блока выделения/картинки
		 * @param $arDataFields['HEIGHT'] - высота блока выделения/картинки
		 *
		 * @return bool|int
		 */
		public static function update($ID, $arDataFields)
		{
			global $USER;
			if ( !(bool)$arDataFields['UID'] )
				$arDataFields['UID'] = $USER->GetID();

			$entity_data_class = self::getEntity_data_class();
			$dt = new DateTime();

			$arFields = array(
				'UF_UID'			=> $arDataFields['UID'],
				'UF_BOOK_ID'		=> $arDataFields['BOOK_ID'],
				'UF_NUM_PAGE'		=> $arDataFields['NUM_PAGE'],
				'UF_TEXT'			=> $arDataFields['NOTE'],
				'UF_DATE_UPDATE' 	=> $dt,
				'UF_NOTE_AREA' 		=> isset($arDataFields['NOTE_AREA'])	? $arDataFields['NOTE_AREA'] : '',
				'UF_IMG_DATA' 		=> isset($arDataFields['IMG_DATA']) 	? $arDataFields['IMG_DATA'] : '',
				'UF_TOP' 			=> isset($arDataFields['TOP']) 			? $arDataFields['TOP'] : '',
				'UF_LEFT' 			=> isset($arDataFields['LEFT']) 		? $arDataFields['LEFT'] : '',
				'UF_WIDTH' 			=> isset($arDataFields['WIDTH']) 		? $arDataFields['WIDTH'] : '',
				'UF_HEIGHT' 		=> isset($arDataFields['HEIGHT']) 		? $arDataFields['HEIGHT'] : '',
			);

			// Теперь пробегаем по полям и проверяем, если поле пустое - мы его исключаем, что б не затереть
			// Если вдруг у кого-то появится необходимость что-то затереть - добавить сюда исключение
			// Я пока ни для одного из полей такой необходимости не вижу
			foreach ( $arFields as $key => $value )
			{
				if ( strlen($value) <= 0 || ( is_numeric($value) && $value <= 0) )
					unset($arFields[$key]);
			}

            J::add('note', 'edit', Array('BOOK_ID' => $arDataFields['BOOK_ID'], 'DATE' => $dt, 'TEXT' => $arDataFields['NOTE'], 'USER_COLLECTION' => $arDataFields['UID']));

			$result = $entity_data_class::update($ID, $arFields);
			return $result->isSuccess() ? $ID : false;
		}
	}