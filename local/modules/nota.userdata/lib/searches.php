<?
	namespace Nota\UserData;

	\CModule::IncludeModule("highloadblock"); 
	use Bitrix\Highloadblock as HL; 
	use Bitrix\Main\Entity; 
	use Bitrix\Main\Type\DateTime;
	use Nota\UserData\Collections;

	class Searches{

		private function getEntity_data_class($HIBLOCK = HIBLOCK_SEARCHSERS_USERS){
			$hlblock = HL\HighloadBlockTable::getById($HIBLOCK)->fetch();
			$entity = HL\HighloadBlockTable::compileEntity($hlblock); 
			$entity_data_class = $entity->getDataClass();
			return $entity_data_class; 
		}

		public function delete($objectId){
			if(empty($objectId))
				return false;

			global $USER;
			
			$entity_data_class = self::getEntity_data_class();

			$rsData = $entity_data_class::getList(array(
				"select" 	=> array("ID"),
				"filter" 	=> array('UF_UID' => $USER->GetID(), 'ID' => (int)$objectId)
			));
			if($arData = $rsData->Fetch()){
				$entity_data_class::Delete($arData['ID']);
				Collections::removeLinkObject($arData['ID'], false, 'searches');
			}
		}

	}

?>