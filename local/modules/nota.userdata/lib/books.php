<?
	namespace Nota\UserData;

	\CModule::IncludeModule("highloadblock"); 
	use Bitrix\Highloadblock as HL; 
	use Bitrix\Main\Entity; 
	use Bitrix\Main\Type\DateTime;
	use Nota\UserData\Collections;

	class Books
	{
		private function getEntity_data_class($HIBLOCK = HIBLOCK_BOOKS_DATA_USERS){
			$hlblock = HL\HighloadBlockTable::getById($HIBLOCK)->fetch();
			$entity = HL\HighloadBlockTable::compileEntity($hlblock); 
			$entity_data_class = $entity->getDataClass();
			return $entity_data_class; 
		}

		public function getListCurrentUser($objectId = false, $uid = false){
			global $USER;
			if ( !$uid )
				$uid = $USER->GetID();
			$entity_data_class = self::getEntity_data_class();
			$arFilter = array('UF_UID' => $uid);
			if(!empty($objectId)) 
				$arFilter['=UF_BOOK_ID'] = $objectId;
			$rsData = $entity_data_class::getList(array(
				"select" => array("ID", "UF_BOOK_ID"),
				"filter" => $arFilter,
			));
			$arResult = array();
			if($rsData->getSelectedRowsCount() <= 0)
				return false;
			$arResult = array();

			if(!empty($objectId))
			{
				$arResult = $rsData->Fetch();
			}
			else
			{
				while($arData = $rsData->Fetch())
					$arResult[$arData['UF_BOOK_ID']] = $arData['ID'];
			}
			return $arResult;
		}

		public function add($objectId, $uid = false){
			global $USER;
			if(empty($objectId))
				return false;

			if ( !$uid )
				$uid = $USER->GetID();

			$favoriteObject = self::getListCurrentUser($objectId, $uid);
			if($favoriteObject !== false)	
				return $favoriteObject['ID'];

			$entity_data_class = self::getEntity_data_class();

			$dt = new DateTime();

			$arFields = array(
				'UF_DATE_ADD'		=> $dt, 
				'UF_BOOK_ID'		=> $objectId,
				'UF_UID'			=> $uid,
			);

			$result = $entity_data_class::add($arFields);
			$saveID = $result->getId();
			return $saveID;
		}		

		public function delete($objectId){
			if(empty($objectId))
				return false;

			$arData = self::getListCurrentUser($objectId);
			if($arData === false)
				return false;

			$entity_data_class = self::getEntity_data_class();
			$entity_data_class::Delete($arData['ID']);
			Collections::removeLinkObject($arData['ID']);
		}
		/*
		все книг
		*/
		public function getAllCnt()
		{
			$entity_data_class = self::getEntity_data_class();
			global $USER;
			$rsData = $entity_data_class::getList(array(
				"runtime" => array('cnt' => array('expression' => array('COUNT(*)'), 'data_type'=>'integer')),
				"select" => array('cnt'),
				"filter" => array('UF_UID' => $USER->GetID()),
			));

			$arData = $rsData->Fetch();
			return (int)$arData['cnt'];
		}
		/*
		Читаю
		*/
		public function getReadingCnt(){
			$entity_data_class = self::getEntity_data_class();
			global $USER;
			$rsData = $entity_data_class::getList(array(
				"runtime" => array('cnt' => array('expression' => array('COUNT(*)'), 'data_type'=>'integer')),
				"select" => array('cnt'),
				"filter" => array('UF_UID' => $USER->GetID(), '=UF_READING' => 1),
			));

			$arData = $rsData->Fetch();
			return (int)$arData['cnt'];
		}

		/*
		Прочитал
		*/
		public function getReadCnt(){
			$entity_data_class = self::getEntity_data_class();
			global $USER;
			$rsData = $entity_data_class::getList(array(
				"runtime" => array('cnt' => array('expression' => array('COUNT(*)'), 'data_type'=>'integer')),
				"select" => array('cnt'),
				"filter" => array('UF_UID' => $USER->GetID(), '=UF_READING' => 2),
			));

			$arData = $rsData->Fetch();
			return (int)$arData['cnt'];
		}

		/**
		 * Удаляет по паре UID + ID, заведомо проверив, реально ли эта запись принадлежит пользователю
		 */
		public function deleteWithTest($id, $uid)
		{
			$entity_data_class = self::getEntity_data_class();
			$toDelete = $entity_data_class::getList(array(
				"select" => array("ID"),
				"filter" => array(
					'ID' 		=> $id,
					'UF_UID' 	=> $uid,
				),
			))->Fetch();

			if ( !$toDelete )
				return false;

			$entity_data_class::Delete($toDelete['ID']);
			Collections::removeLinkObject($toDelete['ID']);
			return true;
		}
}