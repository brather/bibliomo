<?php
use Nota\UserData\AuthorBookTable;
use Nota\UserData\BookTable;
use Nota\UserData\ThemeBookTable;

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Type\DateTime;

CModule::IncludeModule('nota.userdata');
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

Loc::loadMessages(__FILE__);

class QuotesMyLibrary extends CBitrixComponent
{
    public function onPrepareComponentParams($arParams)
    {
        $arParams['CACHE_TIME'] = $arParams['CACHE_TIME'];
        return $arParams;
    }
	
	public function getBooks()
    {
    	global $USER;
			
        $rsBook = BookTable::getList(array(
            'select' => array_keys(BookTable::getMap()),
            'filter' => array('USER_ID' => $USER->GetID()),
            'order' => array()
        ));

        while ($arBook = $rsBook->Fetch()) {
        	if($arBook['STATUS'] == 'READ')
			{
				$name = 'READ';
			}
			else 
			{
				$name = 'OLD';
			}
	        $this->arResult['BOOK'][$name]['VALUES'][] = array(
				'EXALEAD_ID' => $arBook['EXALEAD_ID'],
				'AUTHOR_ID' => $arBook['AUTHOR_ID'],
				'THEME_ID' => $arBook['THEME_ID'],
				'NAME' => $arBook['NAME'],	
				'DATE' => $arBook['DATE'],
				'STATUS' => $arBook['STATUS'],	
			);
			$arResult['AUTHOR_ID'][] = $arBook['AUTHOR_ID'];
			$arResult['THEME_ID'][] = $arBook['THEME_ID'];
			$arResult['EXALEAD_ID'][] = $arBook['EXALEAD_ID'];
			
        }
		$this->arResult['BOOK']['READ']['COUNT'] = count($this->arResult['BOOK']['READ']['VALUES']);
		$this->arResult['BOOK']['OLD']['COUNT'] = count($this->arResult['BOOK']['OLD']['VALUES']);
		$this->arResult['BOOK']['COUNT'] = count($this->arResult['BOOK']['READ']['VALUES']) + count($this->arResult['BOOK']['OLD']['VALUES']);
		
		return $arResult;
	}
	
	public function getTmemeBooks($arId)
    {
    	
    	global $USER;
        $rsBook = ThemeBookTable::getList(array(
            'select' => array_keys(ThemeBookTable::getMap()),
            'filter' => array('EXALEAD_ID' => $arId['THEME_ID']),
            'order' => array()
        ));
		
        while ($arBook = $rsBook->Fetch()) 
        {
        	
			$rsCount = BookTable::getList(array(
	            'select' => array_keys(BookTable::getMap()),
	            'filter' => array('THEME_ID' => $arBook['EXALEAD_ID']),
	            'order' => array()
	        ));
			
			$count = 0;
			
	        while ($arCount = $rsCount->Fetch()) 
	        {
		        $count++;
	        }
			
	        $this->arResult['THEME'][$arBook['EXALEAD_ID']]['VALUES'] = array(
				'ID' => $arBook['ID'],
				'EXALEAD_ID' => $arBook['EXALEAD_ID'],
				'NAME' => $arBook['NAME'],	
			);
			 $this->arResult['THEME'][$arBook['EXALEAD_ID']]['COUNT'] = $count;	
        }
	}
	
	public function getAuthorBooks($arId)
    {
			
    	global $USER;
        $rsBook = AuthorBookTable::getList(array(
            'select' => array_keys(AuthorBookTable::getMap()),
            'filter' => array('EXALEAD_ID' => $arId['AUTHOR_ID']),
            'order' => array()
        ));
		
        while ($arBook = $rsBook->Fetch()) 
        {
        	
			$rsCount = BookTable::getList(array(
	            'select' => array_keys(BookTable::getMap()),
	            'filter' => array('AUTHOR_ID' => $arBook['EXALEAD_ID']),
	            'order' => array()
	        ));
			
			$count = 0;
			
	        while ($arCount = $rsCount->Fetch()) 
	        {
		        $count++;
	        }
			
	        $this->arResult['AUTHOR'][$arBook['EXALEAD_ID']]['VALUES'] = array(
				'ID' => $arBook['ID'],
				'EXALEAD_ID' => $arBook['EXALEAD_ID'],
				'NAME' => $arBook['NAME'],
				'SECOND_NAME' => $arBook['SECOND_NAME'],
				'LAST_NAME' => $arBook['LAST_NAME'],	
			);
			 $this->arResult['AUTHOR'][$arBook['EXALEAD_ID']]['COUNT'] = $count;	
        }
	 }

	public function getDateBooks($arDate)
    {
		$i = 0;
		foreach($arDate as $arDates)
		{
			$k = 0;		
			$rsBook = BookTable::getList(array(
	            'select' => array_keys(BookTable::getMap()),
	            'filter' => array(
	            '>DATE' => DateTime::createFromUserTime($arDates['FIRST']),
				'<DATE' => DateTime::createFromUserTime($arDates['END']),
				),
	            'order' => array()
	        ));
			
			while ($arBook = $rsBook->Fetch()) 
        	{
        		$k ++;	
				
				$format = "DD.MM.YYYY";
				$new_format = "YYYY";
				$first = CDatabase::FormatDate($arDates['FIRST'], $format, $new_format);
				$end = CDatabase::FormatDate($arDates['END'], $format, $new_format);
				
        		$this->arResult['DATES'][$i]['DATE'] = array(
					'FIRST' => $first,
					'END' => $end
				);
				
				
				
				
				

				$this->arResult['DATES'][$i]['COUNT'] = $k;
			}
			$i ++;		
		}
	 }

	public function getArrayDates()
    {
		$result = array(
			0 => array(
				'FIRST' => '01.01.1900',
				'END' => '01.01.1950',
			),
			1 => array(
				'FIRST' => '01.01.1950',
				'END' => '01.01.2000',
			),
			2 => array(
				'FIRST' => '01.01.2000',
				'END' => '01.01.2050',
			),
		);
		return $result;
	 }

	public function executeComponent()
    {
		
       	if ($this->StartResultCache(false, array())) {
       		
            if (!CModule::includeModule('nota.userdata')) {
                $this->AbortResultCache();
                throw new Exception(Loc::getMessage('QUOTES_MODULE_NOT_INSTALLED'));
            }

			$arDate = $this->getArrayDates();
			$arId = $this->getBooks();
			$this->getAuthorBooks($arId);
			$this->getTmemeBooks($arId);		
			$this->getDateBooks($arDate);
			
			$this->includeComponentTemplate();
        }
    }
}

?>