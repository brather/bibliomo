<?
require_once($_SERVER['DOCUMENT_ROOT'] . '/local/modules/notaext/autoloader.php');

// Константы
require_once($_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/include/constants.php");

// Полезные функции
require_once($_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/include/helpers/functions.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/include/helpers/user.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/include/helpers/plan_digitization.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/include/helpers/CalcDistance.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/include/helpers/library.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/include/helpers/save_query.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/include/helpers/collections_user.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/include/helpers/NebWorkplaces.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/include/classes/orm/OrderTable.php");
//require_once($_SERVER["DOCUMENT_ROOT"]."/local/php_interface/include/helpers/lang.php"); # подключается в dbconn.php

// Хэндлеры
require_once($_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/include/event_handlers/update_collections_count_in_library.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/include/event_handlers/add_update_book.php");

//редирект на /profile/ для библиотекаря
require_once($_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/include/event_handlers/library_manager_redirect_after_login.php");

// авторизация по нескольким полям
require_once($_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/include/event_handlers/befireLogin.php");

// фиксируем тип регистрации
require_once($_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/include/event_handlers/register_user_set_type_register.php");

// Пользовательские поля

// События социальной сети и регистрации пользователя.
require_once($_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/include/event_handlers/social_services.php");

// Авторизация через ЕСИА
require_once($_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/include/event_handlers/esia_auth.php");

// перед удалением учетной записи
require_once($_SERVER["DOCUMENT_ROOT"] . "/local/modules/elr.useraccess/lib/elruser.php");

#use Nota\Collection\PropLinkLibrary;
#AddEventHandler("iblock", "OnIBlockPropertyBuildList", array("Nota\Collection\PropLinkLibrary","GetUserTypeDescription"));

AddEventHandler("main", "OnBeforeUserDelete", "ElrOnBeforeUserDelete");

function ElrOnBeforeUserDelete($USER_ID)
{
    $u = \CUser::GetByID($USER_ID)->Fetch();
    global $DB;

    if ($u['UF_MERGED_ID'] == -1)
    {
        $db = $DB->Query("SELECT `VALUE_ID` FROM `b_uts_user` WHERE `UF_MERGED_ID` = ".$USER_ID);

        $oU = new \CUser();
        while ($id = $db->Fetch())
        {
            if ($id['VALUE_ID'] > 0)
            {
                $oU->Update($id['VALUE_ID'], array("UF_MERGED_ID" => 0));
            }
        }
    }
}

AddEventHandler("main", "OnBeforeProlog", "MyOnBeforePrologHandler", 50);

function MyOnBeforePrologHandler()
{
    CJSCore::Init(array("jquery"));
    global $APPLICATION;
    global $USER;
    if ($USER->IsAuthorized()) {
        $APPLICATION->AddHeadScript("/js/prolog/script.js");
        $APPLICATION->SetAdditionalCSS("/js/prolog/style.css");
    }
}

AddEventHandler("main", "OnEndBufferContent", "OnEndBufferContentHandler");
function OnEndBufferContentHandler(&$content)
{
    global $USER;
if ($USER->IsAuthorized()) {
    if ($_REQUEST['action'] == 'load_file_dialogs' && in_array(6, CUser::GetUserGroup($USER->GetID()))) {
        if (strripos(serialize($_REQUEST), 'PREVIEW_TEXT')) {
            $type = 'PREVIEW_TEXT';
        } else {
            $type = 'DETAIL_TEXT';
        }

        $reqId = $_REQUEST['reqId'];
        $content = '<script>function OpenFileBrowser' . $type . '(){$("#myFile_' . $type . '").trigger("click");}</script><script>
    function getXmlHttp' . $type . '(){
        var xmlhttp;
        try {
            xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {
            try {
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (E) {
                xmlhttp = false;
            }
        }
        if (!xmlhttp && typeof XMLHttpRequest!="undefined") {
            xmlhttp = new XMLHttpRequest();
        }
        return xmlhttp;
    }
    function SimpleFileUpload' . $type . '(t){
        var xhr = getXmlHttp' . $type . '();
        var file = document.getElementById("myFile_' . $type . '").files[0];
        var formData = new FormData();
        xhr.open("post", "/ajax/AddFile.php", true);
        xhr.onreadystatechange = function(e) {
            if ( 4 == this.readyState ) {
                $("#"+t).prev().val(xhr.responseText);
                $("#"+t).prev().focus();
                $("#"+t).prev().blur();

            }
        };
        formData.append("thefile", file);
        xhr.send(formData);

    }
</script><input type="button" class="adm-btn" style="float:left;" value="..." id="bx_open_file_link_medialib_button_' . $type . '" title="Выбрать файл из структуры сайта с использованием файлового диалога" onclick="OpenFileBrowser' . $type . '();"/>
<input type=file name="myFile_' . $type . '" id="myFile_' . $type . '" onchange="SimpleFileUpload' . $type . '(\'bx_open_file_medialib_button_' . $type . '\')" /><input type="button" class="adm-btn" style="float:left;" value="..." id="bx_open_file_medialib_button_' . $type . '" title="Выбрать файл из структуры сайта с использованием файлового диалога" onclick="OpenFileBrowser' . $type . '();"/><script>top.BXHtmlEditorAjaxResponse["' . $reqId . '"] = {"result":true};</script>
';
    }
}
}

?>
