<?php
	/**
	 * Константы для проекта.
	 * Пожалуйста, обратите внимание, что для тиражируемых решений, модулей нужно свести использование констант к минимуму - они могут конфликтовать с другими решениями и/или модулями.
	 */
	define("MARKUP", "/local/templates/.default/markup/");
	define("IBLOCK_CODE_COLLECTIONS", 'kollektsii_6');
	define("IBLOCK_CODE_LIBRARY", 'biblioteki_4');
	define("IBLOCK_ID_LIBRARY_NEWS", '3');
	define("IBLOCK_ID_COLLECTION", '6');
	define("IBLOCK_ID_LIBRARY", '4');
	define("IBLOCK_ID_BOOKS", '10');
	define("IBLOCK_ID_IMPORT_PROFILE", '14');

	define("IBLOCK_PCC_ORDER", '8');

	define("PUBLISH_YEAR_MIN", 1000);

	define("PUBLISH_YEAR_MAX", date("Y"));

	define("ID_PRESIDENT_LIBRARY", 2411);

	define("HIBLOCK_PLAN_DIGIT", 1);
	define("HIBLOCK_SEARCHSERS_USERS", 2);
	define("HIBLOCK_COLLECTIONS_USERS", 3);
	define("HIBLOCK_COLLECTIONS_LINKS_USERS", 5);
	define("HIBLOCK_BOOKS_DATA_USERS", 4);
	define("HIBLOCK_BOOKMARKS_DATA_USERS", 10);
	define("HIBLOCK_NOTES_DATA_USERS", 11);
	define("HIBLOCK_QUO_DATA_USERS", 9);

	define("HIBLOCK_LIBRARY_CITY", 8);
	define("HIBLOCK_LIBRARY_AREA", 7);
	define("HIBLOCK_LIBRARY", 6);

	define("PLAN_DIGIT_PERIOD_FINISH", 30); #на какой период ставить окончание периода оцифровки

	define("UGROUP_LIB_CODE_ADMIN", 'library_admin');
	define("UGROUP_LIB_CODE_EDITOR", 'library_editor');
	define("UGROUP_LIB_CODE_CONTORLLER", 'library_contorller');

	define("USER_ACTIVE_ENUM_ID", 4);

	define("COLLECTION_SLIDER_TYPE_COVER_ENUM_ID", 2); //обложка коллекции
	define("COLLECTION_SLIDER_TYPE_BASIC_ENUM_ID", 3); //обычная книга

	# ссылка на закрытый просматровщик
	#spd:https://relar.rsl.ru?id=01003489481
	define("CLOSED_VIEWER_LINK", 'spd:http://нэб.рф:8081/?id=');
	define("ADD_COLLECTION_URL", '/local/tools/collections/');

	define("LIBRARY_DEFAULT_CITY", "592"); //город по-умолчанию для библиотеки. ID берется из HL блока NebLibsCity. 592 == Москва

	define('RGB_LIB_ID', 55); # ID Библиотеки РГБ в Инфоблоке IBLOCK_ID=4
	/*	
	Емайл поддержки + Емайл для притензий, используется при заполнении договора, при добавлении пользователя через ЛК Библиотеки
	*/
	define('SUPPORT_EMAIL', 'support@elar.ru'); 
	define('CLAIM_EMAIL', 'claim@elar.ru'); 

    // Не забыть поправить слайдер выбора диапазона лет в расширенном поиске
    // verstka/neb/js/script.js line 972 cont.slider min max
    define('SEARCH_BEGIN_YEAR', 1000);


?>