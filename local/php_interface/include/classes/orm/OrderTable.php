<?php

use Bitrix\Main\Entity;
use Evk\Books\Books;

class OrderTable extends Entity\DataManager
{
	public static $userID = 0;

	public static function onBeforeDelete($event)
	{
		$nebUser = new nebUser(self::getUserID());
		if (!$nebUser->isLibrary())
		{
			$orderID = $event->getParameter('id');
			$res = self::getList(array(
				'filter' => array('=UF_USER_ID' => self::getUserID(), '=ID' => $orderID)
			));
			if(!$res->fetch())
			{
				$result = new Entity\EventResult;
				$result->addError(new Entity\FieldError($event->getEntity()->getField('ID')));
				return $result;
			}
		}

	}

	/*public static function onAfterAdd($event)
	{
		$to      = 'e.sulagaev@notamedia.com';
	$subject = 'the subject';
	$message = implode(",", $event);
	$headers = 'From: webmaster@example.com' . "\r\n" .
		'Reply-To: webmaster@example.com' . "\r\n" .
		'X-Mailer: PHP/' . phpversion();

	mail($to, $subject, $message, $headers);
		$arEventFields = array(
			"BOOK_NAME" => "Книга какая-то",
			"USER_NAME" => "Пользователь",
		);
		CEvent::Send("BOOK_NEW_ORDER", "s1", $arEventFields);
	}*/

	public static function changeStatus($orderID)
	{
		$nebUser = new nebUser(self::getUserID());

		if (!$nebUser->isLibrary())
		{
			$res = self::getList(array(
				'select' => array('UF_RECEIVED'),
				'filter' => array('=UF_USER_ID' => self::getUserID(), '=ID' => $orderID)
			));
		}
		else
		{
			$res = self::getList(array(
				'select' => array('UF_RECEIVED'),
				'filter' => array('=ID' => $orderID)
			));
		}
		if(!($item = $res->fetch()))
			return;
		$status = (($item['UF_RECEIVED']) ? false : true);
		self::update($orderID, array('UF_RECEIVED' => $status));
	}

	public static function addUserBook($bookID)
	{
		if (!self::bookExists($bookID))
		{
			global $DB;
			$USER_ID = self::getUserID();
			$LIBRARY_ID = self::getLibraryID();
			$date = new Bitrix\Main\Type\DateTime(date('d.m.Y'),'d.m.Y');
			$result = self::add(array(
				'UF_USER_ID' => $USER_ID,
				'UF_BOOK_ID' => $bookID,
				'UF_LIBRARY_ID' => $LIBRARY_ID,
				'UF_RECEIVED' => false,
				'UF_ORDER_REVOKED_US' => false,
				'UF_ORDER_REVOKED_LI' => false,
				'UF_DESIRE_DATE_STAT' => false,
				'UF_COMMENT' => "",
				'UF_DATE_ORDER' => $date,
			));

			if($result->isSuccess())
			{
				$nebUser = new nebUser($USER_ID);
				$arUser = $nebUser->getUser();
				$rsUser = CUser::GetList(($by="ID"), ($order="desc"), array("UF_LIBRARY" => $LIBRARY_ID, "GROUPS_ID" => array(6)),array("SELECT"=>array("EMAIL")));
				$admin_library = $rsUser->Fetch();
				$BOOK = new Books(0,0);
				$fields = $BOOK->GetByID($bookID);
				$arEventFields = array(
					"BOOK_ID"		=> $bookID,
					"LOGIN"			=> $arUser["LOGIN"],
					"NAME"			=> $arUser["NAME"],
					"LAST_NAME"		=> $arUser["LAST_NAME"],
					"SECOND_NAME"	=> $arUser["SECOND_NAME"],
					"USER_ID"		=> self::getUserID(),
					"BOOK_NAME"		=> $fields["title"],
					"LIBRARY_EMAIL" => $admin_library["EMAIL"],
				);
				CEvent::Send("BOOK_NEW_ORDER", "s1", $arEventFields);
			}
		}
	}

	public static function getLibOrders()
	{
		$res = self::getList(array(
			'filter' => array('=UF_LIBRARY_ID' => self::getLibraryID()),
			'select' => array('UF_USER_ID', 'COUNT'),
			'runtime' => array(
				new Entity\ExpressionField('COUNT', 'COUNT(*)')
			)
		));
		$arRes = array();
		while($item = $res->fetch())
			$arRes[] = $item;
		return $arRes;
	}
	public static function getLibOrdersFilterBooks()
	{
		$res = self::getList(array(
			'filter' => array('=UF_LIBRARY_ID' => self::getLibraryID()),
			'select' => array('UF_USER_ID', 'UF_BOOK_ID'),
			'runtime' => array()
		));
		$arRes = array();
		while($item = $res->fetch()){
			$resBook = CIBlockElement::GetByID($item["UF_BOOK_ID"]);
			if($ar_res = $resBook->GetNext()){
				$arRes[] = $item;
			}
		}
		$sortedData = array();
		foreach($arRes as $row) {
			$sortedData[$row['UF_USER_ID']]++;
		}
		$i = 0;
		foreach($sortedData as $key => $ElsortedData) {
           $Return[$i]["UF_USER_ID"] = $key;
           $Return[$i]["COUNT"] = $ElsortedData;
			$i++;
		}
		return $Return;
	}
	public static function getUserOrders($libUser = 0)
	{
		if(empty($libUser))
			$libUser = self::getUserID();
		$res = self::getList(array(
			'filter' => array('=UF_USER_ID' => $libUser)
		));
		$arRes = array();
		while($item = $res->fetch())
			$arRes[] = $item;
		return $arRes;
	}

	public static function bookExists($bookID)
	{
		$nebUser = new nebUser(self::getUserID());
		$res = self::getList(array(
			'filter' => array('=UF_USER_ID' => self::getUserID(), '=UF_BOOK_ID' => $bookID)
		));
		return ($item = $res->fetch());
	}

	protected static function getLibraryID()
	{
		$nebUser = new nebUser(self::getUserID());
		$lib = $nebUser->getLibrary();
		return $lib['ID'];
	}

	protected static function getLibraryEmail()
	{
		$nebUser = new nebUser(self::getUserID());
		$lib = $nebUser->getLibrary();
		return $lib['EMAIL'];
	}

	protected static function getUserID()
	{
		global $USER;
		return $USER->GetID();
	}

	public static function getFilePath()
	{
		return __FILE__;
	}

	public static function getTableName()
	{
		return 'neb_orders';
	}
	public static function DelOrderLib($order)
	{
		$rezult = self::Delete($order);
		return $rezult;
	}
	public static function UpdateOrderLib($order, $ORDER_REVOKED_LI, $DATE_OF_ISSUE, $DATE_RETURN, $UF_COMMENT_US, $DESIRE_DATE_STAT)
	{
		if($DESIRE_DATE_STAT) $DESIRE_DATE_STAT_BU = true; else $DESIRE_DATE_STAT_BU = false;
		if($ORDER_REVOKED_LI) $ORDER_REVOKED_LI_BU = true; else $ORDER_REVOKED_LI_BU = false;
		if($DATE_OF_ISSUE) $date__is = new Bitrix\Main\Type\DateTime(date('d.m.Y',strtotime($DATE_OF_ISSUE)),'d.m.Y'); else $date__is = '';
		if($DATE_RETURN) $date_r = new Bitrix\Main\Type\DateTime(date('d.m.Y',strtotime($DATE_RETURN)),'d.m.Y'); else $date_r = '';
		if(strlen($UF_COMMENT_US) > 0) $UF_COMMENT = $UF_COMMENT_US; else $UF_COMMENT = '';
		$arBxData = array(
			'UF_ORDER_REVOKED_LI' => $ORDER_REVOKED_LI_BU, //Заказ анулирован библ.
			'UF_DATE_OF_ISSUE' => $date__is, //Дата выдачи
			'UF_DATE_RETURN' => $date_r, //Дата возврата
			'UF_COMMENT' => $UF_COMMENT, //Комент
			'UF_DESIRE_DATE_STAT' => $DESIRE_DATE_STAT_BU, //Подтв-e желаемой даты получения
		);
		$rezult = self::update($order, $arBxData);
		return $rezult;
	}
	public static function UpdateOrder($order, $ORDER_REVOKED_US, $UF_DESIRE_DATE)
	{
		if($ORDER_REVOKED_US) $ORDER_REVOKED_US_BU = true; else $ORDER_REVOKED_US_BU = false;
		if($UF_DESIRE_DATE) $date = new Bitrix\Main\Type\DateTime(date('d.m.Y',strtotime($UF_DESIRE_DATE)),'d.m.Y'); else $date = '';
		$arBxData = array(
			'UF_ORDER_REVOKED_US' => $ORDER_REVOKED_US_BU, //Заказ анулирован польз.
			'UF_DESIRE_DATE' => $date, //Желаемая дата получения
		);
		$rezult = self::update($order, $arBxData);
		return $rezult;
	}
	public static function getMap()
	{
		return array(
			new Entity\IntegerField('ID', array(
				'primary' => true,
				'autocomplete' => true
			)),
			new Entity\StringField('UF_USER_ID', array(
				'required' => true
			)),
			new Entity\StringField('UF_BOOK_ID', array(
				'required' => true
			)),
			new Entity\StringField('UF_LIBRARY_ID', array(
				'required' => true
			)),
			new Entity\BooleanField('UF_RECEIVED'),
			new Entity\StringField('UF_COMMENT', array(

			)),


			new Entity\BooleanField('UF_ORDER_REVOKED_US'),
			new Entity\BooleanField('UF_ORDER_REVOKED_LI'),
			new Entity\BooleanField('UF_DESIRE_DATE_STAT'),

			new Entity\DatetimeField('UF_DATE_ORDER', array(
				'default_value' => null,
			)),
			new Entity\DatetimeField('UF_DESIRE_DATE', array(
				'default_value' => null,
			)),
			new Entity\DatetimeField('UF_DATE_OF_ISSUE', array(
				'default_value' => null,
			)),
			new Entity\DatetimeField('UF_DATE_RETURN', array(
				'default_value' => null,
			)),
		);
	}
}

