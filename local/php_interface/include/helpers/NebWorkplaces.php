<?

/**
 * Рабочие места ЕИСУБ в читальных залах библиотек
 */
use \Bitrix\Main;
use Bitrix\NotaExt\NPHPCacheExt;
 
class NebWorkplaces
{
	/**
	 * Код инфоблока "Рабочие места"
	 */
	protected static $iblockCode = 'workplaces';
	
	/**
	 * Определяет находится ли пользователь в читальном зале одной из библиотек (ИБ "Рабочие места")
	 * @return bool
	 */
	public static function checkAccess()
	{
		$userIp = $_SERVER['REMOTE_ADDR'];

		$libraryIps = static::getIpList();
		foreach ($libraryIps as $key => $libraryIp)
		{
			if (static::checkIp($userIp, $libraryIp))
			{
				return true;
			}
		}
		return false;
	}

    /**
     * Определяет находится ли IP в читальном зале одной из библиотек (ИБ "Рабочие места")
     * @return bool
     */
    public static function checkAccessEx($ip)
    {
        $libraryIps = static::getIpList();
        foreach ($libraryIps as $key => $libraryIp)
        {
            if (static::checkIp($ip, $libraryIp))
            {
                return true;
            }
        }

        return false;
    }
	
	/**
	 * Возвращает закешированный массив ip адресов и диапазонов ip адресов библиотек
	 * @return array
	 */
	public static function getIpList()
	{
		$result = array();
		
		if ($iblockId = \Bitrix\NotaExt\Iblock\IblockTools::getIBlockId(static::$iblockCode))
		{
			$obCacheExt    	= new NPHPCacheExt();
			$arCacheParams 	= func_get_args();
			$arCacheParams['iblock_id'] = $iblockId;
			$cacheTag 	   	= "iblock_id_" . $iblockId;
			
			if ( !$obCacheExt->InitCache(__function__, $arCacheParams, $cacheTag) ) 
			{
				if (!Main\Loader::includeModule('iblock'))
					throw new Main\LoaderException('iblock module not installed!');
				
				$sort = array('ID' => 'ASC');
				$filter = array('IBLOCK_ID' => $iblockId, 'ACTIVE' => 'Y');
				$select = array('ID', 'PROPERTY_IP');
				
				$rsWorkplaces = \CIBlockElement::GetList($sort, $filter, false, false, $select);
				while ($workplace = $rsWorkplaces->Fetch())
				{
					if (is_array($workplace['PROPERTY_IP_VALUE']))
					{
						foreach ($workplace['PROPERTY_IP_VALUE'] as $key => $val)
							$result[] = $val;
					}
					else
					{
						$result[] = $workplace['PROPERTY_IP_VALUE'];
					}
				}
				
				$result = array_unique($result);
				
				$obCacheExt->StartDataCache($result);
			}
			else
			{
				$result = $obCacheExt->GetVars();
			}
		}
		
		return $result;
	}
	
	/**
	 * Проверяет принадлежность IP пользователя к переданному ip или диапазону
	 * @param $userIp string IP пользователя
	 * @param $libraryIp IP или диапазон IP библиотеки
	 * @return bool
	 */
	protected static function checkIp($userIp, $libraryIp)
	{
		$userIp = ip2long($userIp);
		
		// Диапазон
		if (strpos($libraryIp, '-') !== false)
		{
			$parts = explode('-', $libraryIp);
			$left = ip2long(trim($parts[0]));
			$right = ip2long(trim($parts[1]));
			
			if (($userIp >= $left) && ($userIp <= $right))
			{
				return true;
			}
			
			return false;
		}

		$libraryIp = ip2long($libraryIp);

		return $libraryIp === $userIp;
	}
}