<?
	class nebLibrary{

		public function getLibByCode($code){
			if(empty($code))
				return false;

			$arResult = Bitrix\NotaExt\Iblock\Element::getList(	
				array('IBLOCK_ID' => Bitrix\NotaExt\Iblock\IblockTools::getIBlockId('IBLOCK_CODE_LIBRARY'), 'CODE' => $code), 
				1, 
				array(
					'PROPERTY_PUBLICATIONS', 
					'PROPERTY_COLLECTIONS', 
					'skip_other'
				)
			);

			return $arResult;
		}

		public static function showCollection($code){
			if(empty($code))
				return false;

			$arResult = self::getLibByCode($code);
			if(!empty($arResult['ITEM']['PROPERTY_COLLECTIONS_VALUE']))
				return true;
			else
				return false;
		}

		public static function showPublication($code){
			if(empty($code))
				return false;

			$arResult = self::getLibByCode($code);
			if(!empty($arResult['ITEM']['PROPERTY_PUBLICATIONS_VALUE']))
				return true;
			else
				return false;
		}

		public function getVoteRight()
		{
			$NUser = new nebUser();
			$role = $NUser->getRole();
			$right = 'D';

			if($role == UGROUP_LIB_CODE_ADMIN)
				$right = 'W';
			elseif($role == UGROUP_LIB_CODE_EDITOR or $role == UGROUP_LIB_CODE_CONTORLLER)
				$right = 'R';
				
			return $right;
		}

		public function getLibs()
		{
			$arResult = Bitrix\NotaExt\Iblock\Element::getList(
				array('IBLOCK_ID' => 4), 
				false, 
				array(
					'ID', 'NAME', 'skip_other'
				)      
			);         

			return $arResult;
		}

	}

?>
