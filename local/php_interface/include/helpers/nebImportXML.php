<?php

/*
class nebImportXML
Класс для импорта библиотечных записей из XML файла
Евгений Сулагаев Jenek-LoKids@yandex.ru
*/

class nebImportXML
{
    function __construct($library_id, $table_name = "neb_import_xml")
    {
        $this->_library_id = intval($library_id);
        $this->_table_name = strtolower($table_name);
    }

    public function createTemporaryTable($ID_PROF = false)                                                // Функция создания временной таблицы
    {
        if (!is_object($this) || strlen($this->_table_name) <= 0) {
            $ob = new nebImportXML;
            return $ob->createTemporaryTable();
        } else {
            global $DB;
            if (defined("MYSQL_TABLE_TYPE") && strlen(MYSQL_TABLE_TYPE) > 0)
                $DB->Query("SET storage_engine = '" . MYSQL_TABLE_TYPE . "'", true);
            if ($arSettings = $this->getSettings($ID_PROF)) {

                $fields_list = "
					ID int(11) not null auto_increment,
					NAME text,
				";
                foreach ($arSettings as $code => $value) {
                    $fields_list .= $code . " varchar(255),
					";
                }
                $fields_list .= "XML_ID varchar(255),
				PRIMARY KEY (ID)";
                $res = $DB->DDL("create table " . $this->_table_name . "(" . $fields_list . ")");
            } else {
                $res = $DB->DDL("create table " . $this->_table_name . "
					(
						ID int(11) not null auto_increment,
						NAME text,
						TITLE varchar(255),
						SUBTITLE varchar(255),
						AUTHOR varchar(255),
						PART_NO varchar(255),
						PART_TITLE varchar(255),
						PUBLISH_YEAR varchar(255),
						PUBLISH_PLACE varchar(255),
						PUBLISH varchar(255),
						BBK varchar(255),
						SERIA varchar(255),
						VOLUME varchar(255),
						YEAR_PUBLIC varchar(255),
						ISBN varchar(255),
						STATUS varchar(255),
						COUNT_PAGES varchar(255),
						LIBRARY varchar(255),
						XML_ID varchar(255),
						PRIMARY KEY (ID)
					)
				");
            }
            return $res;
            // return false;
        }
    }

    public function DropTemporaryTable()                                                // Функция удаления временной таблицы
    {
        if (!isset($this) || !is_object($this) || strlen($this->_table_name) <= 0) {
            $ob = new nebImportXML;
            return $ob->DropTemporaryTable();
        } else {
            global $DB;
            if ($DB->TableExists($this->_table_name))
                return $DB->DDL("drop table " . $this->_table_name);
            else
                return true;
        }
    }

    public function getSettings($ID_PROF)                                                         // Функция получения профиля настроек для импорта текущей библиотеки
    {
        if (CModule::IncludeModule("iblock")) {
            $res = CIBlockElement::GetList(Array("timestamp_x" => "desc"), Array("ID" => $ID_PROF, "IBLOCK_ID" => IBLOCK_ID_IMPORT_PROFILE, "ACTIVE" => "Y"), false, false, array());
            if ($ob = $res->GetNextElement(true, false)) {
                $arProperties = $ob->GetProperties();
                unset($arProperties["LIBRARIES"]);
                foreach ($arProperties as $prop) {
                    $arSettings[$prop["CODE"]] = $prop["VALUE"];
                }

                return $arSettings;
            } else
                return false;
        } else
            return false;
    }

    public function ReadXML($ABS_FILE_NAME, $ID_PROF)
    {
        $xml = simplexml_load_file($ABS_FILE_NAME);
        if (strlen($ID_PROF) > 0) {
            $arSettings = $this->getSettings($ID_PROF);
        } else {
            $arSettings = array();
        }

        $set = '';
        foreach ($arSettings as $Settings) {
            $set .= $Settings;
        }
        if (strlen($set) > 0)
            //&& ($this->_library_id == ID_PRESIDENT_LIBRARY))		// Если есть профиль настроек и библиотека "президентская", для которой формат файла другой, то используем другой алгоритм получения данных
            //if (($arSettings != false) && ($this->_library_id == 58))		// ЗАМЕНИЛ ВРЕМЕННО АЙДИШНИК ПРЕЗИДЕНТСКОЙ БИБЛИОТЕКИ НА АЙДИШНИК МЫТИЩИНСКОЙ. СКОРЕЕ ВСЕГО НУЖНО БУДЕТ ВЕРНУТЬ ОБРАТНО ПОСЛЕ ПОКАЗА.
        {
            foreach ($xml->record as $collection) {
                $record = array();
                foreach ($collection->datafield as $field) {
                    $attrs = $field->attributes();
                    $subrecords = array();
                    foreach ($field->subfield as $subfield) {
                        $childAttrs = $subfield->attributes();
                        $subrecords[(string)$childAttrs['code']] = (string)$subfield;
                    }
                    $record[(string)$attrs['tag']] = $subrecords;
                }
                $arFields = array();
                $arSettingsTest = array(
                    "AUTHOR" => "700-a,b/701-a,b/702-a,b;200-f,g;710-a,b/711-a,b/712-a,b",    // Автор
                    "PART_NO" => "200-h;500-h;510-h;513-h;520-h;541-h;605-h",                    // Номер части
                    // "SERIA"			=> "225-a,h,i,v",
                    // "PART_TITLE"	=> "200-g;200-e",
                    "PART_TITLE" => "200-i;500-i;510-i;513-i;520-i;541-i;605-i",
                    "PUBLISH_YEAR" => "100-a[9_12];210-d",
                    // "PUBLISH_PLACE" => "102-a;210-a;620-a,b,d",
                    "PUBLISH_PLACE" => "210-a;102-a;620-a,b,d",
                    "PUBLISH" => "210-c",
                    "TITLE" => "200-a;501-a;503-a;510-a;512-a;513-a;514-a;515-a;516-a;517-a;518-a;520-a;530-a;531-a;532-a;540-a;541-a;545-a;605-a",    // Заглавие части
                );
                foreach ($arSettings as $code => $value) {

                    $arParts = explode(';', $value);

                    foreach ($arParts as $part) {
                        $arFullVals = array();
                        $arRule = explode('/', $part);
                        foreach ($arRule as $rule) {
                            $arVals = array();
                            $arSubRule = explode('-', $rule);
                            $arTags = explode(',', $arSubRule[1]);
                            foreach ($arTags as $tag) {
                                $pattern = "/^([a-z]{1})\[(\d+)\_(\d+)\]/";
                                if (preg_match($pattern, $tag, $matches)) {
                                    $slice = substr($record[$arSubRule[0]][$matches[1]], $matches[2], $matches[3] - $matches[2] + 1);
                                    if (!empty($slice))
                                        $arVals[] = $slice;
                                } else
                                    if (!empty($record[$arSubRule[0]][$tag]))
                                        $arVals[] = $record[$arSubRule[0]][$tag];
                            }
                            if (!empty($arVals))
                                $arFullVals[] = implode(" ", $arVals);
                        }
                        if (!empty($arFullVals)) {
                            $arFields[$code] = implode(", ", $arFullVals);
                            break;
                        }
                        /*$arRule = explode('/', $part);
                        // if (strpos($arRule[1], ",") !== false)
                        // {
                            $arSubRule = explode(',', $arRule[1]);
                            $arVals = array();
                            foreach ($arSubRule as $subRule)
                            {
                                if (!empty($record[$arRule[0]][$subRule]))
                                    $arVals[] = $record[$arRule[0]][$subRule];
                            }
                            $arFields[$code] = implode(", ", $arVals);
                        // }
                        // if (!empty($record[$arRule[0]][$arRule[1]]))
                        // {
                            // $arFields[$code] = $record[$arRule[0]][$arRule[1]];
                            // break;														// Если нашли элемент, заканчиваем перебор, иначе ищем какое-либо значение в следующем условии
                        // }*/
                    }
                }
                $arFields["NAME"] = $arFields["TITLE"];
                $arFields["XML_ID"] = $collection->control->__toString();

                if (!$this->ReadXMLToDatabase($arFields))
                    return "Ошибка при записи книги во временную таблицу";
            }

        } else {
            // Преобразовываем объект в массив, так как названия полей объекта некорректные для вызова методов
            $arSettingsDif = array(
                "AUTHOR" => "200/f;",
                "PART_NO" => "200/h;500/h;510/h;513/h;520/h;541/h;605/h",
                "PART_TITLE" => "200/i;500/i;510/i;513/i;520/i;541/i;605/i",
                "PUBLISH_YEAR" => "100/a[9_12];210/d",
                "ISBN" => "010/a",
                "BBK" => "686/a,b,c,v",
                "PUBLISH_PLACE" => "210/a;102/a;620/a,b,d",
                "PUBLISH" => "210/c",
                "VOLUME" => "215/a",
                "SERIA" => "225/a,h,i,v",
                "TITLE" => "200/a;501/a;503/a;510/a;512/a;513/a;514/a;515/a;516/a;517/a;518/a;520/a;530/a;531/a;532/a;540/a;541/a;545/a;605/a",    // Заглавие части
            );
            foreach ($xml->collection as $collection) {
                $record2 = array();
                foreach ($collection->record->field as $field) {
                    $attrs = $field->attributes();
                    $subrecords = array();
                    foreach ($field->subfield as $subfield) {
                        $childAttrs = $subfield->attributes();
                        $subrecords[(string)$childAttrs['code']] = (string)$subfield;
                    }
                    $record2[(string)$attrs['tag']] = $subrecords;
                }
                foreach ($arSettingsDif as $code => $value) {
                    if (strpos($value, ";") !== false) {
                        $part = explode(';', $value);
                        $arVal1 = explode('/', $part[0]);
                        $arVal2 = explode('/', $part[1]);
                        if (!empty($record2[$arVal1[0]][$arVal1[1]]))
                            $arFields[$code] = $record2[$arVal1[0]][$arVal1[1]];
                        else
                            $arFields[$code] = $record2[$arVal2[0]][$arVal2[1]];
                    } else {
                        $arVal = explode('/', $value);
                        $arFields[$code] = $record2[$arVal[0]][$arVal[1]];
                    }
                }

                $arFields["XML_ID"] = $collection->record->control->__toString();
                $arFields["NAME"] = $arFields["TITLE"];
                //file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/gg2.php', "<pre>" . print_r($arFields, 1) . "</pre>" . "<pre>" . print_r($record2, 1) . "</pre><br/><br />", FILE_APPEND);


                if (!$this->ReadXMLToDatabase($arFields))
                    return "Ошибка при записи книги во временную таблицу";

            }
           /* $json = json_encode($xml);
            $arXML = json_decode($json, TRUE);
            file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/gg2.php', "<pre>".print_r($record2,1)."</pre>");
            foreach ($arXML["collection"] as $record) {
                $arFields = array();
                if (strlen($set) > 0)                                                // Если есть профиль настроек - используем данные оттуда
                {
                    foreach ($arSettings as $code => $value) {
                        if (strpos($value, ";") !== false) {
                            $part = explode(';', $value);
                            $arVal1 = explode('/', $part[0]);
                            $arVal2 = explode('/', $part[1]);
                            if (!empty($record[$arVal1[0]][$arVal1[1]]))
                                $arFields[$code] = $record[$arVal1[0]][$arVal1[1]];
                            else
                                $arFields[$code] = $record[$arVal2[0]][$arVal2[1]];
                        } else {
                            $arVal = explode('/', $value);
                            $arFields[$code] = $record[$arVal[0]][$arVal[1]];
                        }
                    }
                } else                                                                    // Иначе если профиля нет - используем "стандартные" (статичные) поля
                {
                    $arSettings = $this->getSettings(2412);
                    $arSettingsDif = array(
                        "AUTHOR" => "700-a,b/701-a,b/702-a,b;200-f,g;710-a,b/711-a,b/712-a,b",    // Автор
                        "PART_NO" => "200-h;500-h;510-h;513-h;520-h;541-h;605-h",                    // Номер части
                        "PART_NO" => "200-i;500-i;510-i;513-i;520-i;541-i;605-i",
                        // "SERIA"			=> "225-a,h,i,v",
                        // "PART_TITLE"	=> "200-g;200-e",
                        "PART_TITLE" => "200-i;500-i;510-i;513-i;520-i;541-i;605-i",
                        "PUBLISH_YEAR" => "100-a[9_12];210-d",
                        // "PUBLISH_PLACE" => "102-a;210-a;620-a,b,d",
                        "PUBLISH_PLACE" => "210-a;102-a;620-a,b,d",
                        "PUBLISH" => "210-c",
                        "TITLE" => "200-a;501-a;503-a;510-a;512-a;513-a;514-a;515-a;516-a;517-a;518-a;520-a;530-a;531-a;532-a;540-a;541-a;545-a;605-a",    // Заглавие части
                    );

                    foreach ($arSettings as $code => $value) {
                        if (strpos($value, ";") !== false) {
                            $part = explode(';', $value);
                            $arVal1 = explode('/', $part[0]);
                            $arVal2 = explode('/', $part[1]);
                            if (!empty($record[$arVal1[0]][$arVal1[1]]))
                                $arFields[$code] = $record[$arVal1[0]][$arVal1[1]];
                            else
                                $arFields[$code] = $record[$arVal2[0]][$arVal2[1]];
                        } else {
                            $arVal = explode('/', $value);
                            $arFields[$code] = $record[$arVal[0]][$arVal[1]];
                        }
                    }



                    if (!empty($record["FIELD.200"]["SUBFIELD.A"]))
                        $arFields["TITLE"] = $record["FIELD.200"]["SUBFIELD.A"];
                    else
                        $arFields["TITLE"] = $record["FIELD.461"]["SUBFIELD.C"];

                    $arFields["SUBTITLE"] = $record["FIELD.461"]["SUBFIELD.E"];
                    if (!empty($record["FIELD.200"]["SUBFIELD.F"]))
                    {
                        $arFields["AUTHOR"] = $record["FIELD.200"]["SUBFIELD.F"];
                    }
                    $arFields["PART_NO"] = $record["FIELD.200"]["SUBFIELD.V"];
                    $arFields["YEAR_PUBLIC"] = $record["FIELD.210"]["SUBFIELD.D"];
                    $arFields["ISBN"] = $record["FIELD.10"]["SUBFIELD.A"];
                    $arFields["COUNT_PAGES"] = $record["FIELD.215"]["SUBFIELD.A"];
                    $arFields["LIBRARY"] = $record["FIELD.902"]["SUBFIELD.A"];
                    $arFields["XML_ID"] = $record["built-in.461"]["control.001"];



                }

                if (!empty($arFields["AUTHOR"]))										// Если имеется автор - заполняем название в виде "заголовок + / + автор"
                    $arFields["NAME"] = $arFields["TITLE"]." / ".$arFields["AUTHOR"];
                else																	// Иначе просто заголовок будет являться названием книги
                    $arFields["NAME"] = $arFields["TITLE"];W
                $arFields["NAME"] = $arFields["TITLE"];


                if (!$this->ReadXMLToDatabase($arFields))
                    return "Ошибка при записи книги во временную таблицу";
            }*/
        }
        return true;
    }

    public function ReadXMLToDatabase($arFields)                                        // Запись полученного массива полей в БД
    {
        global $DB;
        $err_mess = '';
        if ($DB->TableExists($this->_table_name)) {
            $DB->PrepareFields($this->_table_name);
            $arLoadFields = array();
            foreach ($arFields as $key => $field) {
                $arLoadFields[$key] = "'" . mysql_real_escape_string(trim($field)) . "'";
            }
            $ID = $DB->Insert($this->_table_name, $arLoadFields, $err_mess . __LINE__);
            return intval($ID);
        }
    }

    public function GetPropsFromFields($arFields)                                        // Функция сопоставления полей, полученных в функции ReadXML с кодами свойств в БД Битрикс
    {
        $PROP = array();
        if ($arSettings = $this->getSettings()) {
            $properties = CIBlockProperty::GetList(Array("id" => "asc"), Array("ACTIVE" => "Y", "IBLOCK_ID" => IBLOCK_ID_BOOKS));
            while ($prop_fields = $properties->GetNext()) {
                if (array_key_exists($prop_fields["CODE"], $arFields) && !empty($arFields[$prop_fields["CODE"]]))
                    $PROP[$prop_fields["CODE"]] = $arFields[$prop_fields["CODE"]];
            }
            //$PROP["STATUS"] = 2;
        } else {
            $PROP["AUTHOR"] = $arFields["AUTHOR"];            // авторы
            $PROP["PART_NO"] = $arFields["PART_NO"];        // номер части
            $PROP["PART_TITLE"] = $arFields["TITLE"];        // заглавие части
            $PROP["PUBLISH_YEAR"] = $arFields["YEAR_PUBLIC"];            // год издания
            $PROP["ISBN"] = $arFields["ISBN"];                    // isbn

            //$PROP["STATUS"] = 2;
        }
        return $PROP;
    }

    public function GetItemFromDB($start, $limit)                                        // Функция получения limit элементов из таблицы БД для записи в ИБ
    {
        global $DB;
        $err_mess = '';
        $strSql = "
			SELECT * FROM " . $this->_table_name . " ORDER BY ID LIMIT $start, $limit";
        $res = $DB->Query($strSql, false, $err_mess . __LINE__);
        return $res;
    }

    public function GetCountItemsFromDB()                                                // Функция получения количества элементов в таблице БД
    {
        global $DB;
        $err_mess = '';
        $obCount = $DB->Query("SELECT COUNT(*) AS COUNT FROM " . $this->_table_name, false, $err_mess . __LINE__);
        $arCount = $obCount->Fetch();
        return $arCount["COUNT"];
    }
}

class nebImportXMLHelp
{
    public function ShowSelectBoxLibrary($select_name)                                    // Функция отображения input'а или selectbox'а наименования библиотеки в зависимости от роли пользователя
    {
        $arSelect = Array("ID", "NAME");
        $arFilter = Array("IBLOCK_ID" => IBLOCK_ID_LIBRARY, "ACTIVE" => "Y");
        $resLib = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
        while ($obLib = $resLib->GetNextElement()) {
            $arFields = $obLib->GetFields();
            $arLibs[$arFields["ID"]] = $arFields["NAME"];
        }
        $input = '';
        global $USER;
        if ($USER->IsAdmin())
            $role = 'admin';
        else {
            $uobj = new nebUser();
            $role = $uobj->getRole();
        }
        if ($role == 'library_admin') {
            $library = $uobj->getLibrary();
            $input = '<input type="hidden" id="' . $select_name . '" name="' . $select_name . '" value="' . $library["ID"] . '" />';
            $input .= '<input type="text" class="input" value="' . $arLibs[$library["ID"]] . '" disabled="disabled"/>';
            /*$input = '<select id="'.$select_name.'" name="'.$select_name.'" disabled class="js_select">';
            foreach ($arLibs as $lib)
            {
                if ($lib["ID"] == $library["ID"])
                    $selected = " selected";
                else
                    $selected = "";
                $input .= '<option value="'.$lib["ID"].'"'.$selected.'>['.$lib["ID"].'] '.$lib["NAME"].'</option>';
            }
            $input .= '</select>';*/
        } elseif ($role == "admin") {
            $input = '<select class = "SelectProfile" id="' . $select_name . '" name="' . $select_name . '">';
            $i = 0;
            foreach ($arLibs as $id => $name) {
                if ($i++ == 0)
                    $selected = " selected";
                else
                    $selected = "";
                $input .= '<option value="' . $id . '"' . $selected . '>[' . $id . '] ' . $name . '</option>';
            }
            $input .= '</select>';
        }
        return $input;
    }

    public function ShowActiveSettings()                                                // Функция определяет имеется ли профиль у этой библиотеки
    {
        $uobj = new nebUser();
        $library = $uobj->getLibrary();
        $res = CIBlockElement::GetList(Array("timestamp_x" => "desc"), Array("IBLOCK_ID" => IBLOCK_ID_IMPORT_PROFILE, "ACTIVE" => "Y", "PROPERTY_LIBRARIES" => $library["ID"]), false, false, array());
        if ($ob = $res->GetNextElement(true, false)) {
            $arFields = $ob->GetFields();
            return $arFields["NAME"];
        } else
            return false;
    }

    public function ShowActiveSettingsUser()                                                // Функция определяет имеется ли профиль у этого пользователя
    {
        //$uobj = new nebUser();
        //$library  = $uobj->getLibrary();
        global $USER;
        $idUser = $USER->GetID();
        $res = CIBlockElement::GetList(Array("timestamp_x" => "desc"), Array("IBLOCK_ID" => IBLOCK_ID_IMPORT_PROFILE, "ACTIVE" => "Y", "PROPERTY_USER" => $idUser, "PROPERTY_DEFAULT" => "Y"), false, false, array());
        if ($ob = $res->GetNextElement(true, false)) {
            $arFields = $ob->GetFields();
            return $arFields["NAME"];
        } else
            return false;
    }

    public function GetProfileUser()
    {

        global $USER;
        $idUser = $USER->GetId();
        $arSelect = Array("ID", "IBLOCK_ID", "NAME", "PROPERTY_USER");
        $arFilter = Array("IBLOCK_ID" => IBLOCK_ID_IMPORT_PROFILE, "ACTIVE" => "Y", "PROPERTY_USER" => $idUser);
        $res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
        $key = 0;
        $arOption = array();
        while ($ob = $res->GetNextElement()) {
            $arFields = $ob->GetFields();
            $arOption[$key]["ID"] = $arFields["ID"];
            $arOption[$key]["NAME"] = $arFields["NAME"];
            $arOption[$key]["USER"] = $arFields["PROPERTY_USER_VALUE"];
            $key++;
        }
        return $arOption;

    }
}

class CPublicMessage
{
    var $message;

    function ShowMessage($arParams)
    {
        if (!empty($arParams) && is_array($arParams)) {
            $message = $arParams["MESSAGE"];
            $details = $arParams["DETAILS"];
            if ($arParams["TYPE"] == "ERROR")
                echo '<div class="info-message error">
							<span class="info-message-title">
								' . $message . '
							</span>
							<div class="info-message-details">
								' . $details . '
							</div>
						</div>';
            else
                echo '<div class="info-message">
							<span class="info-message-title">
								' . $message . '
							</span>
							<div class="info-message-details">
								' . $details . '
							</div>
						</div>';
        }
    }
}

Class ImportLog
{
    protected $file = '';
    protected $link = '';
    protected $pLog;

    public function __construct($file = '')
    {
        if (strlen($file) <= 0)
            return false;

        if (!is_dir($_SERVER['DOCUMENT_ROOT'] . '/upload/import_log'))
            mkdir($_SERVER['DOCUMENT_ROOT'] . '/upload/import_log');

        $this->file = $_SERVER['DOCUMENT_ROOT'] . '/upload/import_log/' . $file;
        $this->link = '/upload/import_log/' . $file;
        $this->pLog = fopen($this->file, "a+");
        if (filesize($this->file) <= 0) {
            $this->addLog('Начало импорта. Дата и время начала: ' . date("d.m.Y H:i:s"));
            $this->addLog('-------------------------------------------------------------------');
        }
    }

    public function addLog($text, $last = false)
    {
        if ($last == false)
            fputs($this->pLog, $text . "\n");
        else {
            fputs($this->pLog, $text . "\n");
            fputs($this->pLog, "-------------------------------------------------------------------\n");
            fputs($this->pLog, "Импорт данных завершен. Дата и время окончания: " . date('d.m.Y H:i:s'));
        }
    }

    public function getDownloadLink()
    {
        return $this->link;
    }

    private function CloseLogFile()
    {
        fclose($this->pLog);
    }

    public function __destruct()
    {
        $this->CloseLogFile();
    }
}

?>