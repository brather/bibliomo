<?
use Bitrix\NotaExt\Utils as Ut;

if (class_exists('Bitrix\NotaExt\Utils')) {
    function pre($ar, $show = false)
    {
        Ut::pre($ar, $show);
    }

    function GetEnding($number, $zero = 'ий', $n2_1 = 'ие', $n2_4 = 'ия')
    {
        return Ut::GetEnding($number, $zero, $n2_1, $n2_4);
    }

    function trimming_line($string, $length = 70, $strEnd = '…')
    {
        return Ut::trimming_line($string, $length, $strEnd);
    }
}

function SortingExalead($By, $Path = false, $sByVar = "by", $sOrderVar = "order", $Anchor = "nav_start", $arRemoveParams = array(), $getIndexPage = null)
{
    global $APPLICATION;

    global $$sByVar, $$sOrderVar;
    $by = $$sByVar;
    $order = $$sOrderVar;
    $orderVar = 'asc';
    $class = '';
    if (strtoupper($By) == strtoupper($by)) {
        if (strtoupper($order) == "DESC") {
            $class = "sort up";

        } else {
            $class = "sort down";
            $orderVar = 'desc';
        }
    }

    $url = $APPLICATION->GetCurPageParam($sByVar . '=' . $By . '&' . $sOrderVar . '=' . $orderVar, array_merge($arRemoveParams, array($sByVar, $sOrderVar, "dop_filter")), $getIndexPage) . '#' . $Anchor;
    return 'href="' . $url . '" class="' . $class . '"';
}


function getNumberNeb($num)
{
    if (empty($num))
        return false;

    $num = $num . '';
    $len = strlen($num) - 1;
    $result = '';
    for ($i = $len; $i >= 0; $i--) {
        $result .= '<span class="b-portalinfo_number' . ($i % 3 == 0 ? ' mr ' : '') . ' iblock">' . $num[$len - $i] . '</span>' . PHP_EOL;
    }
    return $result;
}


function image_to_base64($path_to_image)
{
    $type = pathinfo($path_to_image, PATHINFO_EXTENSION);
    $image = file_get_contents($path_to_image);
    $base64 = 'data:image/' . $type . ';base64,' . base64_encode($image);
    return $base64;
}

function debugPre($ar, $bVarDamp = false, $bHide = false) {

    if ($bHide)
        echo '<!-- Debug: ';
    else
        echo '<pre>';

    if ($bVarDamp)
        var_dump($ar);
    else
        print_r($ar);

    if ($bHide)
        echo '-->';
    else
        echo '</pre>';
}

function debugLog($ar, $sFile = '/debug.log') {
    $sFilePath = $_SERVER["DOCUMENT_ROOT"] . $sFile ;
    $current = file_get_contents($sFilePath);
    $current .= date("d.m.Y H:m:s")."\n".print_r($ar, true)."\n";
    file_put_contents($sFilePath, $current);
}

function p($d) {
    global $USER;
//    if($USER->IsAdmin()){
    ?><pre style="color: green; border: 1px solid green; background: white;"><? print_r ($d) ?></pre><?
//    }
}

class nebSort
{

    /*     производим сортировку массива по полю  */
    private static $DateSortField = '';

    private function SetdatesortField($field)
    {
        self::$DateSortField = $field;
    }

    // Сортировать массив по полю типа - дата
    static function SortFieldTypeDate($arr, $field)
    {
        self::SetdatesortField($field);
        uasort($arr, array(self, 'SortFieldTypeD'));
        return $arr;
    }

    private function SortFieldTypeD($f1, $f2)
    {
        if (strtotime($f1[self::$DateSortField]) < strtotime($f2[self::$DateSortField])) return 1;
        elseif (strtotime($f1[self::$DateSortField]) > strtotime($f2[self::$DateSortField])) return -1;
        else return 0;
    }

    // Сортировать массив по полю типа - число
    static function SortFieldTypeNum($arr, $field, $sort = 'desc')
    {
        self::SetdatesortField($field);
        if ($sort == 'desc')
            uasort($arr, array(self, 'SortFieldTypeNumberDesc'));
        else
            uasort($arr, array(self, 'SortFieldTypeNumber'));
        return $arr;

    }

    private function SortFieldTypeNumber($f1, $f2)
    {
        if ($f1[self::$DateSortField] > $f2[self::$DateSortField]) return 1;
        elseif ($f1[self::$DateSortField] < $f2[self::$DateSortField]) return -1;
        else return 0;
    }

    private function SortFieldTypeNumberDesc($f1, $f2)
    {
        if ($f1[self::$DateSortField] < $f2[self::$DateSortField]) return 1;
        elseif ($f1[self::$DateSortField] > $f2[self::$DateSortField]) return -1;
        else return 0;
    }
}

?>