<?
	/*
	class nebEsiaUuth
	Временное решенеи для интеграции с ЕСИА
	@method getLink	
	@method DecryptAnswer($SamplResponse)
	*/

	ini_set('soap.wsdl_cache_enabled', 0);

	
		class nebEsiaAuth
	{
		#public $wsdlurl = 'http://10.4.130.5/esia/Esia.asmx?WSDL';
		public $wsdlurl = 'http://213.208.168.9/esia/Esia.asmx?WSDL';
		private $client;

		public function __construct($wsdlurl = '')
		{
			if(empty($wsdlurl))
				$wsdlurl = $this->wsdlurl;

			$this->client = new SoapClient(
				$wsdlurl,
				array(  
					"trace"      => 1,  
					"exceptions" => 1,
					'encoding' => 'UTF-8',
				)
			);
		}

		public function getLink()
		{
			$resLink =  $this->client->GenerateLink();
			if(empty($resLink->GenerateLinkResult))
				return false;

			return $resLink->GenerateLinkResult;
		}

		public function DecryptAnswer($SamplResponse)
		{
			$params = array('eisaAnswer' => $SamplResponse);
			$resAnswer = $this->client->DecryptAnswer($params);

			if(empty($resAnswer->DecryptAnswerResult))
				return false;

			return json_decode($resAnswer->DecryptAnswerResult,1);
		}
	}


?>