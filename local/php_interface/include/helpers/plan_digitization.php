<?
	#План на оцифровку  
	class PlanDigitalization{

		/*	
		Может литекущий пользователь добавлять в план на оцифровку книги
		*/
		public static function isAdd(){
			global $USER;
			if (!$USER->IsAuthorized())
				return false;

			$uobj = new nebUser();
			$role = $uobj->getRole();

			if($role == UGROUP_LIB_CODE_ADMIN or $role == UGROUP_LIB_CODE_EDITOR or $role == UGROUP_LIB_CODE_CONTORLLER)
				return true;
			else
				return false;
		}


		public static function getBookInPlan(){
			if(!self::isAdd())
				return false;

			$obUser = new nebUser();
			if(!$obUser->isLibrary())
				return false;

			$Library = $obUser->getLibrary();
			if(empty($Library['ID']))
				return false;

			CModule::IncludeModule("highloadblock");
			$hlblock 	=  Bitrix\Highloadblock\HighloadBlockTable::getById(HIBLOCK_PLAN_DIGIT)->fetch();
			$entity 	= Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);

			$entity_data_class = $entity->getDataClass(); 

			$rsData = $entity_data_class::getList(array(
				"select" => array("ID", 'UF_EXALEAD_BOOK'),
				"filter" => array('UF_LIBRARY' => $Library['ID'])
			));

			$arResult = array();

			while($arData = $rsData->Fetch())
				$arResult[] = $arData['UF_EXALEAD_BOOK'];
				
			return $arResult; 
		} 
	}

?>