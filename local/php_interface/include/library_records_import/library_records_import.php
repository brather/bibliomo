<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
CModule::IncludeModule("iblock");
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/iblock/prolog.php");
IncludeModuleLangFile(__FILE__);
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_js.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/include/helpers/nebImportXML.php");

// Получение роли пользователя. Парсер доступен только администратору сайта и администратору библиотеки
if ($USER->IsAdmin())
    $role = 'admin';
else {
    $uobj = new nebUser();
    $role = $uobj->getRole();
}
if (!($role == 'library_admin' || $role == 'admin'))
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

// Количество элементов записываемых или обновляемых) в БД
if (!isset($INTERVAL))
    $INTERVAL = 10;
else
    $INTERVAL = intval($INTERVAL);

@set_time_limit(0);

$arErrors = array();

if ($_SERVER["REQUEST_METHOD"] == "POST" && $_REQUEST["Import"] == "Y") {
    //Initialize NS variable which will save step data
    if (array_key_exists("NS", $_POST) && is_array($_POST["NS"])) {
        $NS = $_POST["NS"];
        if (array_key_exists("charset", $NS) && $NS["charset"] === "false") $NS["charset"] = false;
        if (array_key_exists("PREVIEW", $NS) && $NS["PREVIEW"] === "false") $NS["PREVIEW"] = false;
        if (array_key_exists("bOffer", $NS) && $NS["bOffer"] === "false") $NS["bOffer"] = false;
    } else {
        $NS = array(
            "STEP" => 0,
            "URL_DATA_FILE" => $_REQUEST["URL_DATA_FILE"],
            "ACTION" => $_REQUEST["outFileAction"],
            "LIBRARY_ID" => $_REQUEST["id_library"],
            "SelectProfileInput" => $_REQUEST["SelectProfileInput"],
        );
    }

    $ABS_FILE_NAME = false;
    $WORK_DIR_NAME = false;
    if (isset($NS["URL_DATA_FILE"]) && (strlen($NS["URL_DATA_FILE"]) > 0)) {
        $filename = trim(str_replace("\\", "/", trim($NS["URL_DATA_FILE"])), "/");
        $FILE_NAME = rel2abs($_SERVER["DOCUMENT_ROOT"], "/" . $filename);
        if ((strlen($FILE_NAME) > 1) && ($FILE_NAME === "/" . $filename) && ($APPLICATION->GetFileAccessPermission($FILE_NAME) >= "W")) {
            $ABS_FILE_NAME = $_SERVER["DOCUMENT_ROOT"] . $FILE_NAME;
            $WORK_DIR_NAME = substr($ABS_FILE_NAME, 0, strrpos($ABS_FILE_NAME, "/") + 1);
        }
    }

    $obImportXML = new nebImportXML;

    if (!check_bitrix_sessid()) {
        $arErrors[] = GetMessage("IBLOCK_CML2_ACCESS_DENIED");
    } elseif ($ABS_FILE_NAME) {
        if ($NS["STEP"] < 1) {

            if (nebImportXML::DropTemporaryTable()) {
                if (nebImportXML::CreateTemporaryTable())
                    $NS["STEP"]++;
                else
                    $arErrors[] = GetMessage("IBLOCK_CML2_TABLE_CREATE_ERROR");
            }
        } elseif ($NS["STEP"] < 2) {
            if (file_exists($ABS_FILE_NAME) && is_file($ABS_FILE_NAME)) {
                if ($result = $obImportXML->ReadXML($ABS_FILE_NAME) == true)
                    $NS["STEP"]++;
                else
                    $arErrors[] = $result;
            }
        } elseif ($NS["STEP"] < 3) {
            if (!empty($NS["current_position"]))
                $k = $NS["current_position"];
            else
                $k = 0;
            $count_items = $obImportXML->GetCountItemsFromDB();
            $obItem = $obImportXML->GetItemFromDB($k, $INTERVAL);
            while ($arFields = $obItem->Fetch()) {
                $el = new CIBlockElement;

                $PROP = $obImportXML->GetPropsFromFields($arFields);        // получить массив свойств. Соответствия поле - свойство прописывается в этой функции
                $PROP[63] = array($NS["LIBRARY_ID"]);                        // id библиотеки

                $arLoadProductArray = Array(
                    "MODIFIED_BY" => $USER->GetID(), // элемент изменен текущим пользователем
                    "IBLOCK_ID" => IBLOCK_ID_BOOKS,
                    "PROPERTY_VALUES"=> $PROP,
                    "NAME" => $arFields["NAME"],
                    "XML_ID" => $arFields["XML_ID"],
                    "ACTIVE"         => "Y",
                );
                $res = CIBlockElement::GetList(Array(), Array("NAME" => $arFields["NAME"]), false, false, array("ID"));
                if ($ob = $res->GetNextElement()) {
                    if ($NS["ACTION"] == "N") {
                        //Update
                        $arUpdateFields = $ob->GetFields();
                        global $DB;
                        $err_mess = '';
                        $strSql = "UPDATE b_iblock_element SET NAME='" . $arFields['NAME'] . "',XML_ID='" . $arFields['XML_ID'] . "',ACTIVE='Y' WHERE ID=" . $arUpdateFields["ID"];

                        $res = $DB->Query($strSql, true, $err_mess . __LINE__);
                        if ($res) {
                            foreach ($PROP as $k => $pr) {
                                CIBlockElement::SetPropertyValuesEx($arUpdateFields["ID"], false, array($k => $pr));
                            }
                            $NS["DONE"]["UPD"]++;
                        }

                    }
                } else {
                    //Add
                    if ($el->Add($arLoadProductArray))
                        $NS["DONE"]["ADD"]++;
                    else {
                        $NS["DONE"]["ERR"]++;
                        $NS["DONE"]["ERR_LIST"][$arFields["ID"]] = "Adding #" . $arFields["ID"] . ": " . $el->LAST_ERROR;
                    }
                }
            }
            $k += $INTERVAL;
            if ($k < $count_items) {
                $NS["current_position"] = $k;
            } else
                $NS["STEP"]++;
        } elseif ($NS["STEP"] < 4) {
            $NS["STEP"]++;
        }
    } else {
        $arErrors[] = GetMessage("IBLOCK_CML2_FILE_ERROR");
    }

    ?>
    <script>
        CloseWaitWindow();
    </script>
    <?

    foreach ($arErrors as $strError)
        CAdminMessage::ShowMessage($strError);
    foreach ($arMessages as $strMessage)
        CAdminMessage::ShowMessage(array("MESSAGE" => $strMessage, "TYPE" => "OK"));

    if (count($arErrors) == 0) {
        if ($NS["STEP"] < 4) {
            $progressItems = array(
                GetMessage("IBLOCK_CML2_TABLES_DROPPED"),
            );
            $progressTotal = 0;
            $progressValue = 0;

            if ($NS["STEP"] < 1)
                echo GetMessage("IBLOCK_CML2_TABLES_CREATION");
            elseif ($NS["STEP"] < 2)
                echo "<b>" . GetMessage("IBLOCK_CML2_TABLES_CREATION") . "</b>";
            else {
                echo GetMessage("IBLOCK_CML2_TABLES_CREATED");
                $progressItems[] = GetMessage("IBLOCK_CML2_TABLES_CREATED");
            }

            if ($NS["STEP"] < 2) {
                $progressItems[] = GetMessage("IBLOCK_CML2_FILE_READING");
                $progressItems[] = GetMessage("IBLOCK_CML2_FILE_PROGRESS2");
            } elseif ($NS["STEP"] < 3) {
                $progressItems[] = "<b>" . GetMessage("IBLOCK_CML2_ELEMENTS") . "</b><br>#PROGRESS_BAR#";
                $progressTotal = $count_items;
                $progressValue = $NS["current_position"];
            } else
                $progressItems[] = GetMessage("IBLOCK_CML2_ELEMENTS_DONE");

            if ($NS["STEP"] < 4) {
                // $progressItems[] = GetMessage("IBLOCK_CML2_ELEMENTS_DONE");
                // $progressItems[] = GetMessage("IBLOCK_CML2_ELEMENTS_DONE");
            }

            CAdminMessage::ShowMessage(array(
                "DETAILS" => "<p>" . implode("</p><p>", $progressItems) . "</p>",
                "HTML" => true,
                "TYPE" => "PROGRESS",
                "PROGRESS_TOTAL" => $progressTotal,
                "PROGRESS_VALUE" => $progressValue,
            ));

            if ($NS["STEP"] > 0)
                echo '<script>DoNext(' . CUtil::PhpToJSObject(array("NS" => $NS)) . ');</script>';
        } else {
            $progressItems = array(
                GetMessage("IBLOCK_CML2_ADDED", array("#COUNT#" => intval($NS["DONE"]["ADD"]))),
                GetMessage("IBLOCK_CML2_UPDATED", array("#COUNT#" => intval($NS["DONE"]["UPD"]))),
                GetMessage("IBLOCK_CML2_WITH_ERRORS", array("#COUNT#" => intval($NS["DONE"]["ERR"]))),
            );
            $progressErrItems = array();
            foreach ($NS["DONE"]["ERR_LIST"] as $err) {
                $progressErrItems[] = $err;
            }

            CAdminMessage::ShowMessage(array(
                "MESSAGE" => GetMessage("IBLOCK_CML2_DONE"),
                "DETAILS" => "<p>" . implode("</p><p>", $progressItems) . "</p>",
                "HTML" => true,
                "TYPE" => "PROGRESS",
                "BUTTONS" => array(
                    array(
                        "VALUE" => GetMessage("IBLOCK_CML2_ELEMENTS_LIST"),
                        "ONCLICK" => "window.location = '" . CUtil::JSEscape(CIBlock::GetAdminElementListLink(IBLOCK_ID_BOOKS, array('find_el_y' => 'Y'))) . "';",
                    ),
                ),
            ));
            if (!empty($progressErrItems)) {
                CAdminMessage::ShowMessage(array(
                    "MESSAGE" => "Ошибки при импорте",
                    "DETAILS" => "<p>" . implode("</p><p>", $progressErrItems) . "</p>",
                    "HTML" => true,
                    "TYPE" => "PROGRESS",
                ));
            }

            echo '<script>EndImport();</script>';
        }
    } else {
        echo '<script>EndImport();</script>';
    }

    require($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/main/include/epilog_admin_js.php");
}

$APPLICATION->SetTitle(GetMessage("IBLOCK_CML2_TITLE"));
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
?>
<div id="tbl_iblock_import_result_div"></div>
<?
$aTabs = array(
    array(
        "DIV" => "edit1",
        "TAB" => GetMessage("IBLOCK_CML2_TAB"),
        "ICON" => "main_user_edit",
        "TITLE" => GetMessage("IBLOCK_CML2_TAB_TITLE"),
    ),
);
$tabControl = new CAdminTabControl("tabControl", $aTabs, true, true);
?>
<script language="JavaScript" type="text/javascript">
    var running = false;
    var oldNS = '';
    function DoNext(NS) {
        var interval = parseInt(document.getElementById('INTERVAL').value);
        var queryString = 'Import=Y'
            + '&lang=<?echo LANG?>'
            + '&<?echo bitrix_sessid_get()?>'
            + '&INTERVAL=' + interval;

        if (!NS) {
            queryString += '&SelectProfileInput=' + document.getElementById('SelectProfileInput').value;
            queryString += '&URL_DATA_FILE=' + document.getElementById('URL_DATA_FILE').value;
            if (document.getElementById('outFileAction_N').checked)
                queryString += '&outFileAction=' + document.getElementById('outFileAction_N').value;
            if (document.getElementById('outFileAction_A').checked)

                queryString += '&outFileAction=' + document.getElementById('outFileAction_A').value;
            queryString += '&id_library=' + document.getElementById('id_library').value;
        }

        if (running) {
            ShowWaitWindow();
            BX.ajax.post(
                'library_records_import.php?' + queryString,
                NS,
                function (result) {
                    document.getElementById('tbl_iblock_import_result_div').innerHTML = result;
                }
            );
        }
    }
    function StartImport() {
        running = document.getElementById('start_button').disabled = true;
        DoNext();
    }
    function EndImport() {
        running = document.getElementById('start_button').disabled = false;
    }
</script>
<form method="POST" action="<? echo $APPLICATION->GetCurPage() ?>?lang=<? echo htmlspecialcharsbx(LANG) ?>" name="form1"
      id="form1">
    <?
    $rsIBlockType = CIBlockType::GetList(array("sort" => "asc"), array("ACTIVE" => "Y"));
    $arIBlockType = array("reference" => array(), "reference_id" => array());
    while ($arr = $rsIBlockType->Fetch()) {
        if ($ar = CIBlockType::GetByIDLang($arr["ID"], LANGUAGE_ID)) {
            $arIBlockType["reference"][] = "[" . $arr["ID"] . "] " . $ar["~NAME"];
            $arIBlockType["reference_id"][] = $arr["ID"];
        }
    }

    $tabControl->Begin();
    $tabControl->BeginNextTab();
    ?>
    <tr class="adm-detail-required-field">
        <td width="40%"><? echo GetMessage("IBLOCK_CML2_URL_DATA_FILE") ?>:</td>
        <td width="60%">
            <input type="text" id="URL_DATA_FILE" name="URL_DATA_FILE" size="30"
                   value="<?= htmlspecialcharsbx($URL_DATA_FILE) ?>">
            <input type="button" value="<? echo GetMessage("IBLOCK_CML2_OPEN") ?>" OnClick="BtnClick()">
            <?
            CAdminFileDialog::ShowScript
            (
                Array(
                    "event" => "BtnClick",
                    "arResultDest" => array("FORM_NAME" => "form1", "FORM_ELEMENT_NAME" => "URL_DATA_FILE"),
                    "arPath" => array("SITE" => SITE_ID, "PATH" => "/upload"),
                    "select" => 'F',// F - file only, D - folder only
                    "operation" => 'O',
                    "showUploadTab" => true,
                    "showAddToMenuTab" => false,
                    "fileFilter" => 'xml',
                    "allowAllFiles" => true,
                    "SaveConfig" => true,
                )
            );
            ?>
        </td>
    </tr>
    <tr class="adm-detail-required-field">
        <td>Библиотека:</td>
        <td><? echo nebImportXMLHelp::ShowSelectBoxLibrary('id_library') ?></td>
        <!--input type="text" id="INTERVAL" name="library" size="5" value=""-->
    </tr>
    <tr>
        <td class="adm-detail-valign-top"><? echo GetMessage("IBLOCK_CML2_ACTION") ?>:</td>
        <td>
            <input type="radio" name="outFileAction" value="N" id="outFileAction_N" checked="checked"><label
                for="outFileAction_N">Добавить новые и обновить существующие</label><br>
            <input type="radio" name="outFileAction" value="A" id="outFileAction_A"><label for="outFileAction_A">Добавить
                новые, не изменяя существующие</label><br>
            <!--input type="radio" name="outFileAction" value="D" id="outFileAction_D"><label for="outFileAction_D"><? echo GetMessage("IBLOCK_CML2_ACTION_DELETE") ?></label><br-->
        </td>
    </tr>
    <tr>
        <td><? echo GetMessage("IBLOCK_CML2_INTERVAL") ?>:</td>
        <td>
            <input type="text" id="INTERVAL" name="INTERVAL" size="5" value="<? echo intval($INTERVAL) ?>">
        </td>
    </tr>
    <!--tr>
		<td><? echo GetMessage("IBLOCK_CML2_IMAGE_RESIZE") ?>:</td>
		<td>
			<input type="checkbox" id="GENERATE_PREVIEW" name="GENERATE_PREVIEW" value="Y" checked>
		</td>
	</tr-->
    <!--<div class="fieldrow nowrap">
		<div class="fieldcell iblock left-50">
			<label for="settings02">Профиль настроек</label>

			<div class="field validate">
				<?
    /*$profile = nebImportXMLHelp::GetProfileUser();
    if (count($profile) == 0) {
        $profile = "НЕ ДОБАВЛЕН! Будет использоваться стандартный!";?>
        <label><?=$profile;?></label>
    <?} else {
        ?>

        <select class="SelectProfile" id="SelectProfileImport">
            <? foreach ($profile as $arProfile):?>
                <option value="<?= $arProfile['ID']; ?>"><?= $arProfile['NAME']; ?></option>
            <? endforeach; ?>
        </select>

    <? }*/ ?>
			</div>

		</div>
		<div class="fieldcell iblock right-50">

		</div>
	</div>-->
    <input id="SelectProfileInput" value="" type="hidden" name="SelectProfileInput">
    <? $tabControl->Buttons(); ?>
    <input type="button" id="start_button" value="<? echo GetMessage("IBLOCK_CML2_START_IMPORT") ?>"
           OnClick="StartImport();" class="adm-btn-save">
    <input type="button" id="stop_button" value="<? echo GetMessage("IBLOCK_CML2_STOP_IMPORT") ?>"
           OnClick="EndImport();">
    <? $tabControl->End(); ?>
</form>
<script type="text/javascript">
    $(document).ready(function () {
        $('body').on('change', '#SelectProfile', function () {
            $.get('/ajax/SelectProfile.php?id=' + $(this).val(), function (data) {
                $("#profile_select").html(data);
            });
        });
        $('body').on('click', '#DefaultPr', function () {
            $.get('/ajax/Select.php?idDef=' + $(this).attr('rel'), function (data) {
            });
        });
        $('body').on('change', '#SelectProfileImport', function () {
            $('#SelectProfileInput').val($(this).val());
        });
    });
</script>
<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
?>
