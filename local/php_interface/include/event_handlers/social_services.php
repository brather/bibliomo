<?

AddEventHandler("main", "OnBeforeUserAdd", Array("nebSocialServicesAuth", "OnBeforeUserAdd_main"));

class nebSocialServicesAuth
{
    public function OnBeforeUserAdd_main(&$arFields)
    {
        if($arFields["EXTERNAL_AUTH_ID"] == "socservices" && $arFields['UF_REGISTER_TYPE'] != '40')
        {
            // Если регистрируемся через соц. сеть, то регистрацию отменяем.
            $error = "Не удалось авторизоваться на сайте через социальную сеть. Сделайте привязку к соответствующей соцсети в настройках профиля.";
            $GLOBALS["APPLICATION"]->ThrowException($error);
            $_SESSION["EXTERNAL_AUTH_ERROR"] = $error;
            return false;
        }
    }
}

?>