<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/socialservices/classes/general/authmanager.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/socialservices/classes/general/openid.php');

AddEventHandler('socialservices', 'OnAuthServicesBuildList', array('CSocServHandlers', 'GetDescription'));

class CSocServHandlers {
    public static function GetDescription() {
        return array(array(
            'ID' => 'Esia',
            'CLASS' => 'CSocServEsia',
            'CSS_CLASS' => 'esialink',
            'NAME' => 'Esia Auth',
            'ICON' => 'esia',
        ));
    }
}

class CEsiaInterface {
    //const AUTH_URL = 'https://demo.neb.elar.ru/WebServices/Esia.php?act=do_login';

    public function GetAuthUrl() {
        //return self::AUTH_URL;
        return 'http://'.SITE_SERVER_NAME . '/WebServices/Esia.php?act=do_login';
    }
}

class CSocServEsia extends CSocServAuth {//extends CSocServOpenID

    const ID = 'Esia';

    public function GetSettings() {
        return array(
        );
    }

    public function GetFormHtml($arParams) {
        $url = CEsiaInterface::GetAuthUrl();
        return '<a href="javascript:void(0)" onclick="BX.util.popup(\''.htmlspecialcharsbx(CUtil::JSEscape($url)).'\', 800, 600)">Esia</a>';
    }

    public function Authorize($arFields) {
        $GLOBALS['APPLICATION']->RestartBuffer();

        $this->AuthorizeUser($arFields);

        $url = 'https://demo.neb.elar.ru';
        //$url = SITE_SERVER_NAME;
        echo '<script type="text/javascript">
				if(window.opener) {
					window.opener.location = \''.CUtil::JSEscape($url).'\';
				}
				window.close();
			</script>';
        die();
    }
}