<?
	// фиксируем тип регистрации
	AddEventHandler("main", "OnAfterUserAdd", Array("setTypeUserRegister", "OnAfterUserAddHandler"));
    AddEventHandler("main", "OnBeforeUserUpdate", Array("setTypeUserRegister", "OnBeforeUserUpdateHandler"));

class setTypeUserRegister
	{
		function OnAfterUserAddHandler(&$arFields)
		{
			if(intval($arFields['ID']) > 0)
			{
                $user = new CUser;
				$type = $user->GetParam("REGISTER_TYPE");
				if(!empty($type))
				{
					$arType = nebUser::getFieldEnum(array('USER_FIELD_NAME' => 'UF_REGISTER_TYPE', 'XML_ID' => $type));
					if(!empty($arType[0]['ID']))
					{
						// поле UF_STATUS
						$status = $arType[0]['ID'] == 39 || $arType[0]['ID'] == 40 ? 4 : 42;
						
						// группа Правообладатель
						if ($arType[0]['ID'] == 41) $arFields['GROUP_ID'][] = 9;
						
						//$user = new CUser;
						$user->Update($arFields['ID'], array('UF_REGISTER_TYPE' => $arType[0]['ID'], 'UF_STATUS' => $status, 'GROUP_ID' => $arFields['GROUP_ID']));
					}
					
				}
			}
		}
        function OnBeforeUserUpdateHandler(&$arFields){
            if (!empty($arFields['ID']))
            {
                /* Обновление происходит не из под системы */
                global $USER;
                if (is_object($USER))
                {
                    $arFields['UF_CHANGED'] = 1;
                }

                $nebUser = new NebUser($arFields['ID']);

                $regTypeId = $nebUser->getUserRegisterTypeId();

                if ($regTypeId != $arFields['UF_REGISTER_TYPE']){
                    // Очищаем кэш
                    global $CACHE_MANAGER;
                    $CACHE_MANAGER->ClearByTag("NEBUSER_UF_REGTYPE");
                }
            }
        }
	}

?>
