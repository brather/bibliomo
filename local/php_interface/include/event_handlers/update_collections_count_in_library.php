<?
	/*
	Подсчитываем колличество коллекций в библиотеке
	при операциях с коллекциями
	*/
	AddEventHandler("iblock", "OnAfterIBlockSectionAdd", Array("setCountCollectionInLibrary", "set"));
	AddEventHandler("iblock", "OnAfterIBlockSectionUpdate", Array("setCountCollectionInLibrary", "set"));
	AddEventHandler("iblock", "OnBeforeIBlockSectionDelete", Array("setCountCollectionInLibrary", "set"));

	class setCountCollectionInLibrary{
		public function set($arFields){
			$SECTION_ID = 0;

			/* ID раздела */
			if(!empty($arFields) and is_array($arFields) and intval($arFields['ID']) > 0)
				$SECTION_ID = $arFields['ID'];
			elseif(!is_array($arFields) and intval($arFields) > 0)
				$SECTION_ID = $arFields;
			/*
			секция коллекции?
			*/
			$res = CIBlockSection::GetByID($SECTION_ID);
			$ar_res = $res->GetNext();
			if($ar_res['IBLOCK_CODE'] == IBLOCK_CODE_COLLECTIONS)
			{
				/*			
				ID привызанной библиотеки
				*/
				$arFilter = Array('ID'=>$SECTION_ID, 'IBLOCK_ID'=>$ar_res['IBLOCK_ID'], 'ACTIVE'=>'Y');  
				$db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, false, array('ID', 'IBLOCK_ID', 'UF_LIBRARY'));  
				$ar_result = $db_list->GetNext();
				if(intval($ar_result['UF_LIBRARY']) > 0){
					/*
					Сколько коллекций в библиотеке?
					*/
					$arFilter = Array('IBLOCK_ID'=>$ar_res['IBLOCK_ID'], 'ACTIVE'=>'Y', 'UF_LIBRARY' =>$ar_result['UF_LIBRARY']);  
					$db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, false, array('ID', 'IBLOCK_ID'));					
					$cnt = $db_list->SelectedRowsCount();
					/*
					Запишем
					*/
					CIBlockElement::SetPropertyValuesEx($ar_result['UF_LIBRARY'], false, array('COLLECTIONS' => $cnt));
				}
			}
		}
	}
?>