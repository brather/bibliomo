<?php
//define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/pdfs/logs/log_".date("Ymd").".log");

\Evk\Books\Log::$DIR = $_SERVER["DOCUMENT_ROOT"]."/_test/";
//\Evk\Books\Log::$IF = $_SERVER["HTTP_HOST"] == "nebmo.e.krivonosov.dev.notamedia.ru";

\Evk\Books\Log::$IF = true;
CModule::IncludeModule("iblock");
CModule::IncludeModule('nota.library');
CModule::IncludeModule("evk.books");

AddEventHandler("main", "OnAfterUserAdd", Array("NotaMediaBooks", "OnAfterUserAdd"));

AddEventHandler("main", "OnUserDelete", Array("NotaMediaBooks", "OnUserDelete"));

AddEventHandler("iblock", "OnBeforeIBlockElementAdd", Array("NotaMediaBooks", "OnBeforeIBlockElementAdd"));

AddEventHandler("iblock", "OnAfterIBlockElementAdd", Array("NotaMediaBooks", "OnAfterIBlockElementAdd"));

AddEventHandler("iblock", "OnAfterIBlockElementDelete", Array("NotaMediaBooks", "OnAfterIBlockElementDelete"));

AddEventHandler("iblock", "OnAfterIBlockElementUpdate", Array("NotaMediaBooks", "OnAfterIBlockElementUpdate"));

// AddEventHandler("search", "OnSearchGetFileContent", array("NotaMediaBooks", "OnSearchGetFileContent_pdf"));

AddEventHandler("search", "BeforeIndex", Array("NotaMediaBooks", "BeforeIndex"));

AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", Array("NotaMediaBooks", "OnBeforeIBlockElementUpdate"));

AddEventHandler("iblock", "OnBeforeIBlockElementDelete", Array("NotaMediaBooks", "OnBeforeIBlockElementDelete"));

AddEventHandler("main", "OnFileDelete", array("NotaMediaBooks", "OnFileDelete"));

AddEventHandler("main", "OnAfterUserLogout", array("NotaMediaBooks", "OnAfterUserLogout"));

AddEventHandler("iblock", "OnBeforeIBlockSectionAdd", Array("NotaMediaBooks", "OnBeforeIBlockSectionAdd"));

AddEventHandler("iblock", "OnAfterIBlockSectionAdd", Array("NotaMediaBooks", "OnAfterIBlockSectionAdd"));

AddEventHandler("iblock", "OnAfterIBlockSectionUpdate", Array("NotaMediaBooks", "OnAfterIBlockSectionUpdate"));

AddEventHandler("iblock", "OnBeforeIBlockSectionDelete", Array("NotaMediaBooks", "OnBeforeIBlockSectionDelete"));

class NotaMediaBooks
{
    public function OnAfterUserAdd(&$arFields)
    {
        if($arFields["ID"]>0)
        {
            \Evk\Books\Cache::ClearCountUsersCache();
        }
    }
    public function OnUserDelete($ID)
    {
        \Evk\Books\Cache::ClearCountUsersCache();
    }
    public function OnBeforeIBlockSectionAdd(&$arFields)
    {
        //\Evk\Books\Log::OnBeforeIBlockSectionAdd(__FILE__,__LINE__,$arFields);
        if($arFields["IBLOCK_ID"] == 6)
        {
            if(CNotaLibrary::isStaff())
            {
                $libID = CNotaLibrary::getLibraryID();
                $arFields["UF_LIBRARY"] = $libID;
            }
        }
    }

    public function OnAfterIBlockElementAdd(&$arFields)
    {
        //\Evk\Books\Log::OnAfterIBlockElementAdd(__FILE__,__LINE__,$arFields);
        if($arFields["IBLOCK_ID"] == 10)
        {
            self::clearSearchCache();
        }
        elseif($arFields["IBLOCK_ID"] == 4)
        {
            \Evk\Books\Cache::ClearByTag('neb_library_list_coordinates');
        }
        elseif($arFields["IBLOCK_ID"] == 3 || $arFields["IBLOCK_ID"] == 9)
        {
            \Evk\Books\Cache::ClearMainNewsBlockCache();
        }
    }
    public function OnAfterIBlockElementDelete(&$arFields)
    {
        //\Evk\Books\Log::OnAfterIBlockElementDelete(__FILE__,__LINE__, $arFields);
        if($arFields["IBLOCK_ID"] == 10)
        {
            self::clearSearchCache();
        }
        elseif($arFields["IBLOCK_ID"] == 4)
        {
            \Evk\Books\Cache::ClearByTag('neb_library_list_coordinates');
        }
        elseif($arFields["IBLOCK_ID"] == 3 || $arFields["IBLOCK_ID"] == 9)
        {
            \Evk\Books\Cache::ClearMainNewsBlockCache();
        }
    }
    public function OnAfterIBlockElementUpdate(&$arFields)
    {
        //\Evk\Books\Log::OnAfterIBlockElementUpdate(__FILE__,__LINE__,$arFields);
        if($arFields["IBLOCK_ID"] == 10)
        {
            self::clearSearchCache();
        }
        elseif($arFields["IBLOCK_ID"] == 4)
        {
            \Evk\Books\Cache::ClearByTag('neb_library_list_coordinates');
        }
        elseif($arFields["IBLOCK_ID"] == 3 || $arFields["IBLOCK_ID"] == 9)
        {
            \Evk\Books\Cache::ClearMainNewsBlockCache();
        }
    }
    public function OnAfterIBlockSectionUpdate(&$arFields)
    {
        //\Evk\Books\Log::OnAfterIBlockSectionUpdate(__FILE__,__LINE__,$arFields);
        if($arFields["IBLOCK_ID"] == 15)
        {
            \Evk\Books\Cache::ClearByTag("catalog_bbk_sections");
        }
    }
    public function OnAfterIBlockSectionDelete(&$arFields)
    {
        //\Evk\Books\Log::OnAfterIBlockSectionDelete(__FILE__,__LINE__,$arFields);
        if($arFields["IBLOCK_ID"] == 15)
        {
            \Evk\Books\Cache::ClearByTag("catalog_bbk_sections");
        }
    }
    public function OnAfterIBlockSectionAdd(&$arFields)
    {
        //\Evk\Books\Log::OnAfterIBlockSectionAdd(__FILE__,__LINE__,$arFields);
        if($arFields["IBLOCK_ID"] == 15)
        {
            \Evk\Books\Cache::ClearByTag("catalog_bbk_sections");
        }
    }
    public function OnAfterUserLogout($arFields)
    {
        //\Evk\Books\Log::OnAfterUserLogout(__FILE__,__LINE__,$arFields);
        unset($_SESSION["BOOKS_EXTEND"], $_SESSION["BOOKS_ACCESS"]);
    }
    public function OnFileDelete($arFile)
    {
        //\Evk\Books\Log::OnFileDelete(__FILE__,__LINE__,$arFile);
        if($arFile["MODULE_ID"] == "iblock")
        {
            $detailPath = $_SERVER["DOCUMENT_ROOT"]."/upload/books_pages/detail/".$arFile["ID"]."/";
            $previewPath = $_SERVER["DOCUMENT_ROOT"]."/upload/books_pages/preview/".$arFile["ID"]."/";
            $normalPath = $_SERVER["DOCUMENT_ROOT"]."/upload/books_pages/normal/".$arFile["ID"]."/";

            //\Evk\Books\Log::OnFileDelete(__FILE__,__LINE__,$detailPath, $previewPath, $normalPath);
            if(file_exists($detailPath) && is_dir($detailPath) &&
                file_exists($previewPath) && is_dir($previewPath) &&
                file_exists($normalPath) && is_dir($normalPath)
            )
            {
                //\Evk\Books\Log::OnFileDelete(__FILE__,__LINE__,"exists",$detailPath, $previewPath, $normalPath);
                self::clearDirectory($detailPath);
                self::clearDirectory($previewPath);
                self::clearDirectory($normalPath);
            }
        }
    }

    public function OnBeforeIBlockElementUpdate(&$arFields)
    {
        //\Evk\Books\Log::OnBeforeIBlockElementUpdate(__FILE__,__LINE__, $arFields);
        if($arFields["IBLOCK_ID"] == 10)
        {
            return self::processUpdateBook($arFields, "Update");
        }
    }

    public function OnBeforeIBlockElementAdd(&$arFields)
    {
        //\Evk\Books\Log::OnBeforeIBlockElementAdd(__FILE__,__LINE__, $arFields);
        if($arFields["IBLOCK_ID"] == 10)
        {
            return self::processUpdateBook($arFields, "Add");
        }
        elseif($arFields["IBLOCK_ID"] == 3)
        {
            $libID = CNotaLibrary::getLibraryID();
            if(!empty($libID))
            {
                self::modifyPropertyValues($arFields["IBLOCK_ID"], $arFields["PROPERTY_VALUES"]);
                $arFields["PROPERTY_VALUES"]["LIBRARY"] = $libID;
            }
        }
    }

    protected static function clearSearchCache()
    {
        if (defined('BX_COMP_MANAGED_CACHE') && is_object($GLOBALS['CACHE_MANAGER']))
        {
            \Evk\Books\Cache::ClearByTag("catalog_bbk_sections");
            \Evk\Books\Cache::ClearCache();
        }
    }
    protected static function modifyProperty(&$VALUES, $arNames)
    {
        if(is_array($VALUES))
        {
            $NEW_VALUES = array();
            foreach($VALUES as $KEY=>&$VALUE)
            {
                if(in_array($KEY, array_values($arNames)))
                {
                    $NEW_VALUES[$KEY] = $VALUE;
                    continue;
                }
                $NEW_VALUE = array();
                if(is_array($VALUE))
                {
                    foreach($VALUE as $key2=>$value2)
                    {
                        if(is_array($value2) && preg_match('@^n?\d+(:\d+)?$@', $key2) && array_key_exists("VALUE", $value2))
                        {
                            if(count($VALUE) > 1)
                            {
                                $NEW_VALUE[] = $value2["VALUE"];
                            }
                            else
                            {
                                $NEW_VALUE = $value2["VALUE"];
                            }
                        }
                        elseif(!is_array($value2))
                        {
                            unset($VALUE[$key2]);
                            if(count($VALUE) > 1)
                                $NEW_VALUE[] = $value2;
                            else
                                $NEW_VALUE = $value2;
                        }
                    }
                }
                if(is_numeric($KEY) && array_key_exists($KEY, $arNames))
                {
                    $NEW_VALUES[$arNames[$KEY]] = $NEW_VALUE;
                }
            }
            $VALUES = $NEW_VALUES;
        }
    }
    protected static function modifyPropertyValues($IBLOCK_ID, &$VALUES)
    {
        $IBLOCK_ID = intval($IBLOCK_ID);
        if($IBLOCK_ID <= 0)
            return false;
        $rsProp = CIBlockProperty::GetList(array(), array("IBLOCK_ID"=>$IBLOCK_ID));
        $arNames = array();
        while($arProp = $rsProp->Fetch())
        {
            $arNames[$arProp["ID"]] = $arProp["CODE"];
        }
        self::modifyProperty($VALUES, $arNames);
    }

    protected static function processUpdateBook(&$arFields, $action)
    {
        global $USER;

        self::modifyPropertyValues($arFields["IBLOCK_ID"], $arFields["PROPERTY_VALUES"]);

        if($USER->IsAdmin())
            return null;

        //\Evk\Books\Log::processUpdateBook(__FILE__,__LINE__,$action,$arFields);

        $HASH_NAME_AUTHOR = self::getHashFromArgs(
            $arFields["NAME"],
            $arFields["PROPERTY_VALUES"]["AUTHOR"],
            $arFields["PROPERTY_VALUES"]["PUBLISH_PLACE"],
            $arFields["PROPERTY_VALUES"]["PUBLISH"],
            $arFields["PROPERTY_VALUES"]["PUBLISH_YEAR"]
        );

        $boolFileHash = false;

        if( (isset($arFields["PROPERTY_VALUES"]["FILE"]["error"])
                && $arFields["PROPERTY_VALUES"]["FILE"]["error"] == 0)
            || (!isset($arFields["PROPERTY_VALUES"]["FILE"]["error"])
                && isset($arFields["PROPERTY_VALUES"]["FILE"]["size"])
                && $arFields["PROPERTY_VALUES"]["FILE"]["size"] > 0
            )
        )
        {
            $filePath = $arFields["PROPERTY_VALUES"]["FILE"]["tmp_name"];
            if(file_exists($filePath))
            {
                $fileHash = md5_file($filePath);
                if($action == "Update")
                    $boolFileHash = $arFields["PROPERTY_VALUES"]["FILE_HASH"] != $fileHash;
                elseif($action == "Add")
                    $arFields["PROPERTY_VALUES"]["FILE_HASH"] = $fileHash;
            }
        }

        if(!$boolFileHash && self::checkHashFileExists($arFields, array(
                "ACTION" => $action,
                'HASH_NAME_AUTHOR' => $HASH_NAME_AUTHOR,
        )))
        {
            return false;
        }
        if($boolFileHash)
        {
            $arFields["PROPERTY_VALUES"]["FILE_HASH"] = $fileHash;
            $arFields["PROPERTY_VALUES"]["STATUS"] = 0;
            self::checkFIleMeta($filePath, $pdfInfo);
            if(array_key_exists("Pages", $pdfInfo) && is_numeric($pdfInfo["Pages"]) && intval($pdfInfo["Pages"])>0)
            {
                $arFields["PROPERTY_VALUES"]["COUNT_PAGES"] = intval($pdfInfo["Pages"]);
            }
            else
            {
                $arFields["PROPERTY_VALUES"]["COUNT_PAGES"] = 0;
                $arFields["PROPERTY_VALUES"]["STATUS"] = 3;
            }
            $arFields["PROPERTY_VALUES"]["FILE_META"] = serialize($pdfInfo);
        }
        $arFields["PROPERTY_VALUES"]["HASH_NAME_AUTHOR"] = $HASH_NAME_AUTHOR;
        //\Evk\Books\Log::processUpdateBook(__FILE__,__LINE__,$action,$arFields);
        return null;
    }

    public static function getHashFromArgs()
    {
        $args = array();
        foreach(func_get_args() as $arg)
        {
            if(isset($arg) && strlen($arg))
                $args[] = $arg;
        }
        if(count($args))
            return md5(join("|", $args));
    }

    public static function clearDirectory($dir)
    {
        //\Evk\Books\Log::clearDirectory(__FILE__,__LINE__,$dir);
        if(!empty($dir) && strlen($dir) > 2)
        {
            foreach(scandir($dir) as $file)
            {
                if($file == "." || $file == "..") continue;
                $file = rtrim($dir, "/")."/".$file;
                //\Evk\Books\Log::clearDirectory(__FILE__,__LINE__,$file);
                if(file_exists($file) && is_file($file))
                {
                    unlink($file);
                    //if(unlink($file))
                    //    \Evk\Books\Log::clearDirectory(__FILE__,__LINE__,$file." deleted");
                    //else
                    //    \Evk\Books\Log::clearDirectory(__FILE__,__LINE__,$file." not deleted");
                }
            }
            rmdir($dir);
        }
    }

    public function OnBeforeIBlockElementDelete($ID)
    {
        //\Evk\Books\Log::OnBeforeIBlockElementDelete(__FILE__,__LINE__,$ID);
        global $USER;
        if($USER->IsAdmin())
            return null;
        $rsElem = CIBlockElement::GetByID($ID);
        if($obItem = $rsElem->GetNextElement())
        {
            $arItem = $obItem->GetFields();
            $arItem["PROPERTIES"] = $obItem->GetProperties();
            //\Evk\Books\Log::OnBeforeIBlockElementDelete(__FILE__,__LINE__,$arItem);
            $nebUser = new \Evk\Books\nebUser();
            $lib = $nebUser->getLibrary();
            if($arItem["IBLOCK_ID"] == IBLOCK_ID_BOOKS)
            {
                if(count($arItem["LIBRARIES"]["VALUE"]) > 1)
                {
                    if($lib)
                    {
                        $arLibs = array();
                        if(in_array($lib["ID"], $arItem["LIBRARIES"]["VALUE"]))
                        {
                            $arLibs = array_diff($arItem["LIBRARIES"]["VALUE"], array($lib["ID"]));
                        }
                        if(!empty($arLibs))
                        {
                            $GLOBALS["DB"]->StartTransaction();
                            CIBlockElement::SetPropertyValuesEx($ID, $arItem["IBLOCK_ID"], array(
                                "LIBRARIES" => $arLibs,
                            ));
                            $GLOBALS["DB"]->Commit();
                        }
                    }
                    else
                    {
                        $GLOBALS["APPLICATION"]->ThrowException("Невозможно удалить книгу! Данная книга привязана к нескольким библиотекам");
                        return false;
                    }
                    // Удаление книги из коллекций библиотеки.
                    if($lib)
                    {
                        $rsCollectionSections = CIBlockSection::GetList(array(), array(
                            "IBLOCK_ID" => 6,
                            "UF_LIBRARY" => $lib["ID"],
                        ), false, array(
                            'ID',
                            'IBLOCK_ID',
                            'UF_LIBRARY',
                        ));
                        $arCollectionIDs = array();
                        while($arCollectionSections = $rsCollectionSections->Fetch())
                        {
                            $arCollectionIDs[] = $arCollectionSections["ID"];
                        }
                        $rsCollectionBooks = CIBlockElement::GetList(array(), array(
                            "IBLOCK_ID" => 6,
                            "PROPERTY_BOOK_ID" => $ID,
                            "SECTION_ID" => $arCollectionIDs,
                        ), false, false, array(
                            'ID',
                            'IBLOCK_ID',
                            "PROPERTY_BOOK_ID",
                        ));
                        while($arCollectionBooks = $rsCollectionBooks->Fetch())
                        {
                            CIBlockElement::Delete($arCollectionBooks["ID"]);
                        }
                    }
                    $GLOBALS["APPLICATION"]->ThrowException("Книга была отвязана от вашей библиотеки.");
                    return false;
                }
                // Удаление привязанной книои из всех коллекций
                $rsCollectionBooks = CIBlockElement::GetList(array(), array(
                    "IBLOCK_ID" => 6,
                    "PROPERTY_BOOK_ID" => $arItem["ID"],
                ), false, false, array(
                    'ID',
                    'IBLOCK_ID',
                    "PROPERTY_BOOK_ID",
                ));
                while($arCollectionBooks = $rsCollectionBooks->Fetch())
                {
                    CIBlockElement::Delete($arCollectionBooks["ID"]);
                }
                // / Удаление привязанной книои из коллекций
                self::clearSearchCache();
            }
            elseif($arItem["IBLOCK_ID"] == 4)
            {
                \Evk\Books\Cache::ClearByTag('neb_library_list_coordinates');
            }
            elseif($arItem["IBLOCK_ID"] == 3 || $arItem["IBLOCK_ID"] == 9)
            {
                \Evk\Books\Cache::ClearMainNewsBlockCache();
            }
        }
    }

    public function BeforeIndex($arFields)
    {
        //\Evk\Books\Log::BeforeIndex(__FILE__,__LINE__,$arFields);
        if(!CModule::IncludeModule("iblock"))
            return $arFields;
        if($arFields["MODULE_ID"] == "iblock" && $arFields["PARAM2"] == 10)
        {
            $arProps = array(
                "AUTHOR",
                "PUBLISH_PLACE",
                "PUBLISH",
                "PUBLISH_YEAR",
                "FILE",
                "PART_TITLE",
                "SERIA",
                "PART_NO",
                "VOLUME",
                // "LIBRARIES",
                "BBK",
                "ISBN",
            );
            $arSelect = array_map(function($a){
                return "PROPERTY_".$a;
            }, $arProps);
            $arSelect = array_merge($arSelect, array("ID", "IBLOCK_ID", "NAME"));
            $rsElem = CIBlockElement::GetList(array(), array("=ID" => $arFields["ITEM_ID"]), false, false, $arSelect);
            if($arElem = $rsElem->Fetch())
            {
                // $arFields["BODY"] = $arElem["NAME"]."\n";
                $arFields["BODY"] = "";
                foreach($arProps as $prop)
                {
                    if($prop == "FILE") continue;
                    $prop = sprintf('PROPERTY_%s_VALUE', $prop);
                    if(array_key_exists($prop, $arElem) && !self::is_empty($arElem[$prop]))
                    {
                        $arFields["BODY"] .= $arElem[$prop]."\n";
                    }
                }
                if(array_key_exists("PROPERTY_FILE_VALUE", $arElem) && intval($arElem["PROPERTY_FILE_VALUE"]) > 0)
                {
                    $file_id = intval($arElem["PROPERTY_FILE_VALUE"]);
                    $rsFile = CFile::GetByID($file_id);
                    if($arFile = $rsFile->Fetch())
                    {
                        $FULL_PATH = $_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR."upload".DIRECTORY_SEPARATOR.$arFile["SUBDIR"].DIRECTORY_SEPARATOR.$arFile["FILE_NAME"];
                        if (self::checkFIleMeta($FULL_PATH))
                        {
                            if(($text = self::getTextFromPdf($FULL_PATH))!==false)
                            {
                                $arFields["BODY"] .= $text;
                            }
                        }
                    }
                }
                /*
                $arTags = array();
                foreach($arProps as $arProp)
                {
                    $prop_key = sprintf('PROPERTY_%s_VALUE', $arProp);
                    if(array_key_exists($prop_key, $arElem) && !empty($arElem[$prop_key]))
                    {
                        $arTags[] = $arElem[$prop_key];
                    }
                }
                $arFields["TAGS"] = join(",", $arTags);
                */
            }
            //\Evk\Books\Log::BeforeIndex(__FILE__,__LINE__,'Final Version:', $arFields);
        }
        elseif($arFields["MODULE_ID"] == "iblock" && $arFields["PARAM2"] == 15)
        {
            $ITEM_ID = preg_replace("@\D@", "", $arFields["ITEM_ID"]);
            if(is_numeric($ITEM_ID) && $ITEM_ID > 0)
            {
                $rsData = CIBlockSection::GetList(array(), array("ID" => $ITEM_ID));
                if($arData = $rsData->Fetch())
                {
                    $arFields["BODY"] = $arData["XML_ID"];
                }
            }
        }
        return $arFields;
    }
    protected static function is_empty($string)
    {
        $string = trim($string);
        return ((strlen($string) <= 0) || preg_match("@null@i", $string));
    }
    protected static function strip_tags($text)
    {
        return preg_replace("@<[^>]*>@", "", $text);
    }
    protected static function p($arr)
    {
        echo "<pre>".print_r($arr,1)."</pre>";
    }
    public static function checkFIleMeta($filePath, &$pdfInfo=array())
    {
        $pdfInfo = array();
        $pdfInfoPath = '/usr/bin/pdfinfo'; // полный путь к pdfinfo
        $filePathEscape = escapeshellarg($filePath);
        foreach (explode("\n", shell_exec(escapeshellcmd($pdfInfoPath . ' ' . $filePathEscape))) as $str)
        {
            list($key, $val) = count(explode(':', $str)) == 2 ? explode(':', $str) : array('', '');
            if (trim($key) && trim($val))
            {
                $pdfInfo[trim($key)] = trim($val);
            }
        }
        return !empty($pdfInfo) && (!isset($pdfInfo['Error']) || !$pdfInfo['Error']);
    }
    protected static function checkHashFileExists($arFields, $arBool=array())
    {

        //\Evk\Books\Log::checkHashFileExists(__FILE__,__LINE__,'arFields', $arFields);

        $arSelect = Array(
            "ID",
            "IBLOCK_ID",
            "NAME",
            "XML_ID",
            "PROPERTY_LIBRARIES",
            "PROPERTY_FILE_HASH",
            "PROPERTY_FILE",
            "PROPERTY_AUTHOR",
            "PROPERTY_PUBLISH_PLACE",
            "PROPERTY_PUBLISH",
            "PROPERTY_PUBLISH_YEAR",
            "PROPERTY_HASH_NAME_AUTHOR",
            'PROPERTY_ISBN'
        );
        $arFilter = Array(
            "IBLOCK_ID" => IBLOCK_ID_BOOKS,
        );

        $arFields["PROPERTY_VALUES"]['ISBN'] = trim($arFields["PROPERTY_VALUES"]['ISBN']);
        $arFields["PROPERTY_VALUES"]['XML_ID'] = trim($arFields["PROPERTY_VALUES"]['XML_ID']);


        $HASH_NAME_AUTHOR = "";
        $check = false;
        if(!empty($arFields["PROPERTY_VALUES"]['ISBN']))
        {
            $check = true;
            $arFilter["PROPERTY_ISBN"] = $arFields["PROPERTY_VALUES"]["ISBN"];
        }
        elseif($arFields["XML_ID"])
        {
            $check = true;
            $arFilter["XML_ID"] = $arFields["XML_ID"];
        }
        elseif(!empty($arFields["PROPERTY_VALUES"]['FILE_HASH']))
        {
            $check = true;
            $arFilter["PROPERTY_FILE_HASH"] = $arFields["PROPERTY_VALUES"]["FILE_HASH"];
        }
        elseif(!empty($arFields["PROPERTY_VALUES"]["HASH_NAME_AUTHOR"]))
        {
            $HASH_NAME_AUTHOR = $arFields["PROPERTY_VALUES"]["HASH_NAME_AUTHOR"];
        }
        elseif(!empty($arBool["HASH_NAME_AUTHOR"]))
        {
            $HASH_NAME_AUTHOR = $arBool["HASH_NAME_AUTHOR"];
        }
        if(!empty($HASH_NAME_AUTHOR))
        {
            $check = true;
            $arFilter["PROPERTY_HASH_NAME_AUTHOR"] = $HASH_NAME_AUTHOR;
        }
        if($check)
        {
            $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1), $arSelect);
            if($arExistsFields = $res->Fetch())
            {
                //\Evk\Books\Log::checkHashFileExists(__FILE__,__LINE__,'arExistsFields', $arExistsFields);
                if($arExistsFields["ID"] != $arFields["ID"])
                {
                    $LIBS = $arExistsFields["PROPERTY_LIBRARIES_VALUE"];
                    if(isset($arFields["PROPERTY_VALUES"]["LIBRARIES"]))
                    {
                        foreach($arFields["PROPERTY_VALUES"]["LIBRARIES"] as $lib)
                        {
                            if(!in_array($lib, $LIBS))
                                $LIBS[] = $lib;
                        }
                    }
                    $LIBS = array_unique($LIBS);
                    $arUpdateProperties = array(
                        "LIBRARIES" => $LIBS,
                    );
                    $arUpdateProperties["HASH_NAME_AUTHOR"] = NotaMediaBooks::getHashFromArgs(
                        $arFields["NAME"],
                        $arFields["PROPERTY_AUTHOR_VALUE"],
                        $arFields["PROPERTY_PUBLISH_PLACE_VALUE"],
                        $arFields["PROPERTY_PUBLISH_VALUE"],
                        $arFields["PROPERTY_PUBLISH_YEAR_VALUE"]
                    );
                    $GLOBALS["DB"]->StartTransaction();
                    $el = new CIBlockElement();
                    $el->SetPropertyValuesEx($arExistsFields["ID"], $arExistsFields["IBLOCK_ID"], $arUpdateProperties);
                    $GLOBALS["DB"]->Commit();
                    $GLOBALS["APPLICATION"]->ThrowException("Данная книга уже есть в системе. Книга была добавлена к вашей библиотеке.");
                    return true;
                }
            }
        }
        return false;
    }
    protected static function getTextFromPdf($filePath)
    {
        $pdfToTextPath = '/usr/bin/pdftotext'; // полный путь к pdftotext
        $filePathEscape = escapeshellarg($filePath);
        $cmd = escapeshellcmd($pdfToTextPath . ' -nopgbrk ' . $filePathEscape . ' -');
        $text = shell_exec($cmd);
        if (!empty($text)) return htmlspecialchars(self::strip_tags($text));
        return false;
    }
}
?>