<?
	/*
	Теперь авторизоваться можно и по мылу и по ЕЭЧБ
	*/
	AddEventHandler("main", "OnBeforeUserLogin", Array("nebUserLogin", "OnBeforeUserLoginHandler"));
	class nebUserLogin
	{
		// создаем обработчик события "OnBeforeUserLogin"
		function OnBeforeUserLoginHandler(&$arFields)
		{
			if(!empty($arFields['LOGIN']))
			{
				$login = false;
				$login = self::getUserFilter('=EMAIL', $arFields['LOGIN']);

				if($login === false)
					$login = self::getUserFilter('UF_NUM_ECHB', $arFields['LOGIN']);

				if($login !== false)
					$arFields['LOGIN'] = $login;

			}
		}

		public function getUserFilter($field, $value)
		{
			if(empty($field) or empty($value))
				return false;

			$arFilter = Array(    
				"ACTIVE"              => "Y",    
			);

			$arFilter[$field] = $value; 

			$rsUser = CUser::GetList(($by="id"), ($order="desc"), $arFilter);
			if(!$arUser = $rsUser->Fetch())
				return false;

			return $arUser['LOGIN'];
		}
	}

?>