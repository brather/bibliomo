<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
Use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>
<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="ru"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="ru"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="ru"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="ru"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<script src="<?=SITE_TEMPLATE_PATH?>/js/modernizr.min.js"></script>
	<meta name="viewport" content="width=device-width"/>
	<title><?$APPLICATION->ShowTitle()?></title>
	<?$APPLICATION->ShowHead();?>
	<link rel="icon" href="<?=MARKUP?>favicon.ico" type="image/x-icon" />
	<?$APPLICATION->SetAdditionalCSS(MARKUP."css/main.css")?>
	<?$APPLICATION->AddHeadScript(MARKUP.'js/libs/jquery.min.js');?>
	<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
	<script>window.jQuery.ui || document.write('<script src="<?$APPLICATION->AddHeadScript(MARKUP.'js/jquery.ui.min.js');?>">\x3C/script>')</script>
	<?$APPLICATION->AddHeadScript(MARKUP.'js/jquery.ui.touch-punch.min.js');?>
	<?$APPLICATION->AddHeadScript(MARKUP.'js/plugins.js');?>
	<?$APPLICATION->AddHeadScript(MARKUP.'js/slick.min.js');?>
	<?$APPLICATION->AddHeadScript(MARKUP.'js/jquerypp.custom.js');?>
	<?$APPLICATION->AddHeadScript(MARKUP.'js/jquery.bookblock.js');?>
	<?$APPLICATION->AddHeadScript(MARKUP.'js/script.js');?>
	<?$APPLICATION->AddHeadScript(MARKUP.'js/jquery.md5.js');?>
	<?CJSCore::Init(array("fx"));?>
	<?/*/?>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/script.js"></script>
	<?/*/?>
	<script>
		$(function() {
			$('html').addClass('mainhtml');
		}); // DOM loaded
	</script>
</head>
<body>
<div class="oldie hidden"><?=GetMessage("WARNING_POPUP_MESSAGE")?></div>
<div id="panel">
	<?$APPLICATION->ShowPanel();?>
</div>
<div class="b-searchlayer"></div>
<div class="oldie hidden">Вы пользуетесь устаревшей версией браузера. <br/>Не все функции будут работать в нем корректно. <br/>Смените на более новую версию</div>
<div class="homepage<?=(defined("MARGIN_MAP") && MARGIN_MAP)?" margin_map":"";?>" >
	<header id="header" class="header">
		<div class="wrapper clearfix">
			<a href="/" class="b-header_logo"><?=Loc::getMessage("UNION_INFORMATION_SYSTEM")?></a>
			<?$APPLICATION->IncludeComponent(
				"bitrix:menu",
				"top",
				array(
					"ROOT_MENU_TYPE" => "top_".LANGUAGE_ID,
					"MENU_CACHE_TYPE" => "A",
					"MENU_CACHE_TIME" => "3600",
					"MENU_CACHE_USE_GROUPS" => "N",
					"MENU_CACHE_GET_VARS" => array(
					),
					"MAX_LEVEL" => "1",
					"CHILD_MENU_TYPE" => "left_".LANGUAGE_ID,
					"USE_EXT" => "N",
					"DELAY" => "N",
					"ALLOW_MULTI_SELECT" => "N"
				),
				false
			);?>
			<?
			$APPLICATION->IncludeComponent("bitrix:system.auth.form", "",
				Array(
					"FORGOT_PASSWORD_URL" => "/auth/",
					"SHOW_ERRORS" => "Y"
				),
				false
			);
			?>
		</div>	<!-- /.wrapper -->
	</header>
	<?$APPLICATION->IncludeComponent(
	"neb:search.form",
	".default",
	array(
		"PAGE" => ($APPLICATION->GetCurPage()=="/"?"MAIN":$APPLICATION->GetCurPage()),
		"PLACEHOLDER" => "Поиск книг по всем разделам портала",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600"
	),
	false
);?>
<?if(defined("STATIC_PAGE") && STATIC_PAGE):?>
	<div class="wrapper">
		<section class="b-static-content">
			<h1><?$APPLICATION->ShowTitle(false)?></h1>
<?endif;?>
