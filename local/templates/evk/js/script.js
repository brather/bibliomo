/* Author:
 NOTAMEDIA http://notamedia.ru/
 */
var OLDBROWSER=!Modernizr.backgroundsize||!Modernizr.svg, WINDOW, HTML, DOCUMENT, BODY;
var backgroundsize=Modernizr.backgroundsize;

(function($) { //create closure
    $.fn.cleanWS = function(options) {
        this.each(function() {
            var iblock = this,
                par = iblock.parentNode,
                prev = iblock.previousSibling,
                next = iblock.nextSibling;
            while (prev) {
                var newprev = prev.previousSibling;
                if (prev.nodeType == 3 && prev.nodeValue) {
                    for (var i = prev.nodeValue.length - 1; i > -1; i--) {
                        var cc = prev.nodeValue.charCodeAt(i);
                        if (cc == 9 || cc == 10 || cc == 32) {
                            prev.nodeValue = prev.nodeValue.slice(0, i);
                        } else {
                            break;
                        }
                    }
                }
                if (prev.nodeType == 8) par.removeChild(prev); // remove comment
                prev = newprev;
            }
            while (next) {
                var newnext = next.nextSibling;
                if (next.nodeType == 3 && next.nodeValue) {
                    while (next.nodeValue.length) {
                        var cc = next.nodeValue.charCodeAt(0);
                        if (cc == 9 || cc == 10 || cc == 32) {
                            next.nodeValue = next.nodeValue.slice(1);
                        } else {
                            break;
                        }
                    }
                }
                if (next.nodeType == 8) par.removeChild(next); // remove comment
                next = newnext;
            }

        });
    }
    //end of closure
})(jQuery);

// uipopup
(function($) { //create closure
    $.fn.uipopup = function(options) {
        this.each(function() {

//			$(this).on('click', function(e) {				
//				e.preventDefault()\;
            if(!$(this).hasClass('grad')){$(".ui-dialog .ui-dialog-content").dialog('close');}

            var lnk = $(this),
                popup = $('.popup'), H = (lnk.data('height')) ? lnk.data('height') : 595,
                W = (lnk.data('width')) ? lnk.data('width') : 560,
                url = lnk.attr('href'), clone = popup.clone(), hp = $('.homepage ');
            var scroll_pos = $(window).scrollTop();


            /*

             if (lnk.closest('.ui-dialog').hasClass('popup')) {
             clone.dialog("destroy").remove();
             ajaxclone.dialog("destroy").remove();
             }
             */
            if (lnk.hasClass('ajax_opener')) {
                var ajaxclone = $('<div class="uipopupcontainer hidden"></div>') , closebtn = $('<a href="#" class="closepopup">Закрыть окно</a>');
                ajaxclone.appendTo('body');
                var prevUrl = false;
                ajaxclone.dialog({
                    closeOnEscape: true,
                    dialogClass: 'no-close',
                    modal: true,
                    draggable: false,
                    resizable: false,
                    width:($(window).width() > 995) ? W : "100%",
                    height: H,
                    dialogClass: 'ajaxpopup',
                    position: (lnk.data('posmy') && lnk.data('posat')) ? { my: (lnk.data('posmy'))?lnk.data('posmy'):"", at: (lnk.data('posat'))?lnk.data('posat'):"", of: lnk } : { my: "center top", at: "center top", of: window },
                    open: function() {
                        $('.ui-widget-content').css({'background':'none'});
                        $.ajax({
                            url:  lnk.attr('href'),
                            method: 'GET',
                            success: function(html) {
                                ajaxclone.html(html);
                                $('.ui-widget-content').css({'background':'#ffffff'});
                                if ( lnk.hasClass('closein')) {if(lnk.hasClass('coverlay')){ closebtn = ajaxclone.find('.closepopup');}else{ closebtn.appendTo(ajaxclone);}} else {if(!lnk.hasClass('button_red')&&!lnk.hasClass('coverlay')) closebtn.appendTo('body');}
                                ajaxclone.find('.js_multipleslider').mulislider();
                                $('.iblock', ajaxclone).cleanWS();
                                $('.b-form', ajaxclone ).initForm();
                                function initGraph(graph) {
                                    var cont = graph, tit = lnk.data('graph-title'), url = cont.data('sorce'), name =lnk.data('graph-name'), x =lnk.data('graph-x'), y = lnk.data('graph-y');
                                    var xname = [];
                                    if(x) { xname = x.split(','); }
                                    $.ajax({
                                        url: url,
                                        type: 'GET',
                                        async: true,
                                        dataType: "json",
                                        success: function (data) {
                                            cont.highcharts({
                                                chart: {
                                                    // Edit chart spacing
                                                    spacingBottom: 15,
                                                    spacingTop: 12,
                                                    spacingLeft: 10,
                                                    spacingRight: 20,

                                                    // Explicitly tell the width and height of a chart
                                                    width: null,
                                                    height: null
                                                },

                                                title: {
                                                    text: tit,
                                                    x: -20, //center
                                                    style: {
                                                        fontSize: '22px'
                                                    }
                                                },
                                                subtitle: {
                                                    text: '',
                                                    x: -20
                                                },
                                                xAxis: {
                                                    categories: xname,
                                                    labels: {
                                                        style: { fontSize:'14px' }
                                                    }
                                                },
                                                yAxis: {
                                                    title: {
                                                        text: y,
                                                        style: { fontSize:'14px' }
                                                    },
                                                    plotLines: [{
                                                        value: 0,
                                                        width: 1,
                                                        color: '#808080'
                                                    }],
                                                    labels: {
                                                        style: {
                                                            fontSize: '14px'
                                                        }
                                                    }
                                                },

                                                tooltip: {
                                                    valueSuffix: 'просмотра'
                                                },
                                                plotOptions: {
                                                    line: {
                                                        dataLabels: {
                                                            enabled: true,
                                                            style: { fontSize: '14px' }
                                                        },
                                                        enableMouseTracking: false
                                                    }
                                                },
                                                legend: {
                                                    itemStyle: {
                                                        fontSize: '14px'
                                                    }
                                                },
                                                series: [data]
                                            });
                                        }
                                    });
                                }
                                var gg = $('#highcharts', ajaxclone);
                                initGraph(gg);

                                $('.b-fieldeditable', ajaxclone).fieldEditable();
                                $('.js_select', ajaxclone).selectReplace();
                                $('input.checkbox:not(".custom")', ajaxclone).replaceCheckBox();

                                if(lnk.hasClass('noclose')){
                                    closebtn.addClass('noclose');
                                }
                                var overlay = $('.ui-widget-overlay');
                                if ( lnk.data('overlaybg') == 'dark' ) { overlay.addClass('dark'); }

                                if (lnk.hasClass('coverlay')) {
                                    $('.book_slider', ajaxclone).bookslider();
                                    $('.js_threeslide', ajaxclone).threeslide();
                                    $('.js_flex_bgimage', ajaxclone).flexbgimage();
                                    $('.js_description', ajaxclone).descrPopup();

                                    $(window).scrollTop(0);

                                    // amb stopPropagation
                                    var bookimg = ajaxclone.find('.b-bookframe_img');
                                    closebtn = ajaxclone.find('.closepopup');
                                    closebtn.addClass('bookclose');
                                    overlay.css({'background':'#ffffff',
                                        'background-size':'100% auto','opacity': 0});
                                    /*overlay.blurjs({
                                     source: '.ui-widget-overlay',
                                     radius:4,
                                     overlay: 'rgba(255,255,255,0.4)'
                                     });*/
                                    bookimg.on('load', function(){
                                        overlay.animate({'opacity': 1}, 1500, 'easeOutQuad');
                                    });
                                    // amb finish

                                    /*bookimg.click(function(e){
                                     e.preventDefault();
                                     ajaxclone.dialog("close");

                                     });		*/

                                    closebtn.click(function(e){
                                        e.preventDefault();
                                        ajaxclone.dialog("close");

                                    });
                                    ajaxclone.closest('.ui-dialog ').addClass('bookcard');
                                    // А вот так  меняется ссылка
                                    if(url != window.location.href){
                                        //var location = window.history.location || window.location;
                                        //history.pushState(null, null, this.href);
                                        prevUrl = window.location.href;
                                        window.history.pushState(null, null, url);
                                    }
                                    ajaxclone.height( $('.b-bookpopup').outerHeight());


                                }
                                if ( lnk.data('popupsize') == 'high') { hp.height(ajaxclone.height()); }






                            }
                        });

                        ajaxclone.removeClass('hidden');
                        closebtn.show();

                        /*$(window).one('resize.popup', function(){
                         ajaxclone.dialog("close");
                         });*/


                        closebtn.click(function(e){
                            e.preventDefault();
                            ajaxclone.dialog("close");

                        });
                    },

                    close: function() {
                        ajaxclone.remove();
                        if (lnk.hasClass('coverlay')) { $(window).scrollTop(scroll_pos); }
                        hp.height('');
                        hp.css('overflow', '');
                        if(prevUrl)
                            window.history.pushState(null, null, prevUrl);
                        closebtn.hide();
                    }
                });

                $('.ui-widget-overlay').click(function() {
                    //Close the dialog
                    ajaxclone.dialog("close");

                });


                /*$('.btrefuse ').on('click', function() {
                 alert(1);
                 $(ajaxclone.closest('.ui-dialog')).remove();
                 ajaxclone.remove();
                 return false;
                 }); */


            } else {

                clone.dialog({
                    closeOnEscape: true,
                    dialogClass: 'no-close',
                    modal: true,
                    draggable: false,
                    resizable: false,
                    width: ($(window).width() > 540) ? W : 300,
                    height: ($(window).width() > 540) ? H: H+225,
                    dialogClass: 'commonpopup',
                    position: (lnk.data('posmy') && lnk.data('posat')) ? { my: (lnk.data('posmy'))?lnk.data('posmy'):"", at: (lnk.data('posat'))?lnk.data('posat'):"", of: lnk } : { my: "right top", at: "right bottom", of: lnk },

                    open: function(){
                        /*window.setTimeout(function() {
                         clone.dialog("option", "position", {
                         my: "right top",
                         at: "right bottom",
                         of: lnk
                         });
                         }, 50); */
                        /*$(window).one('resize.clone', function(){
                         clone.dialog("close");
                         }); */

                        if(clone.hasClass('change_log_pass')){
                            $('<a href="#" class="closepopup">Закрыть окно</a>').appendTo(clone);
                            $('.ui-widget-overlay').addClass('dark');
                            $('.change_log_pass .closepopup').click(function(e){
                                e.preventDefault();
                                clone.dialog("close");

                            });
                        }

                        clone.find('.b-form').initForm();
                    },
                    close: function() {
                        clone.remove();

                    }
                });


                $('.ui-widget-overlay').click(function() {
                    //Close the dialog
                    clone.dialog("close");

                });

            }




//
//			});
        });
    }
    //end of closure
})(jQuery);




var touch = ($('html').hasClass('no-touch'))? false : true;

$(function() {
    if(document.all && !window.atob){
        // код для IE9 и ниже
        $('.oldie').oldiePopup();
        return false;
    }

    WINDOW = $(window);
    HTML = $('html');
    DOCUMENT = $(document);
    BODY = $('body');


    $('.iblock').cleanWS();
    //$( '.bookblock' ).bookblock();
    var Page = (function() {

        var config = {
                $bookBlock : $( '.bookblock' ),
                $navNext : $( '.js_bookblock_next' ),
                $navPrev : $( '.js_bookblock_prev' )

            },
            init = function() {
                config.$bookBlock.bookblock( {
                    speed : 800,
                    shadowSides : 0.8,
                    shadowFlip : 0.7
                } );
                initEvents();
            },
            initEvents = function() {

                var $slides = config.$bookBlock.children();

                // add navigation events
                config.$navNext.on( 'click touchstart', function() {
                    config.$bookBlock.bookblock( 'next' );
                    return false;
                } );

                config.$navPrev.on( 'click touchstart', function() {
                    config.$bookBlock.bookblock( 'prev' );
                    return false;
                } );


                // add swipe events
                $slides.on( {
                    'swipeleft' : function( event ) {
                        config.$bookBlock.bookblock( 'next' );
                        return false;
                    },
                    'swiperight' : function( event ) {
                        config.$bookBlock.bookblock( 'prev' );
                        return false;
                    }
                } );

                // add keyboard events
                $( document ).keydown( function(e) {
                    var keyCode = e.keyCode || e.which,
                        arrow = {
                            left : 37,
                            up : 38,
                            right : 39,
                            down : 40
                        };

                    switch (keyCode) {
                        case arrow.left:
                            config.$bookBlock.bookblock( 'prev' );
                            break;
                        case arrow.right:
                            config.$bookBlock.bookblock( 'next' );
                            break;
                    }
                } );
            };

        return { init : init };

    })();
    if( $('.bookblock').length ) { Page.init(); }

    $('.scroll-pane').jScrollPane();
    //$('#highcharts').initGraph();
    //$('.b-bookpage').cardPage();
    $('.b-quote_item').quoteInit();
    $('.js_slider_single').slick({
        dots: true,
        cssEase: 'ease',
        speed: 600
    });
    $('.js_slider_single_nodots').each(function(){
        if($(this).find('> div').length > 1){

            $(this).slick({
                dots: false,
                cssEase: 'ease',
                speed: 600
            });
        }
    });
    $('.js_scroll').mCustomScrollbar();
    $('.b-changecity').cityChoose();
    $('.b-libfilter').libFilter();
    $('.js_digital').digitalForm();
    $('.b-search_save').searchSave();
    /*$('.closepage').closePage(); */
    $( '.tabs').tabs();
    $('.js_editcollection').editCollection();
    $('.b-popularslider').popularSlider();
    $('.js_description').descrPopup();
    $('.tbcalendar').datepicker();
    $('.js_multipleslider').mulislider();
    $('.searchnum_set').searchNum();
    $('.echbcell').ticketCheck();
    $('.b-form').initForm();
    $('.seldate').seldate(); // всегда до кастомизации select

    $('.js_register').registerPopup();
    $('.b-fieldeditable').fieldEditable();

    $( ".js_sortable" ).sortable();
    $( ".js_sortable" ).disableSelection();

    $('.js_flex_bgimage').flexbgimage();
    $('input.radio').replaceRadio();
    $('input.checkbox:not(".custom")').replaceCheckBox();


    $('.js_select').selectReplace();
    $('.b-sidenav').sideAccord(); // аккордеон менюшки сайдбара
    $('.knob').knobFunk();
    $('.js_fourslider').fourSlider();
    $('.b-newslider').newsSlider();
    $('.js_threeslide').threeslide();


    $('.b-gostlist li').copyFunction();

    $('.searchform').searchforminit(); // инициализация формы поиска
    $('.js_simpleslide').slick({
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        onInit:function(slickSlider){
            $('.b-epublishing .b-publishing_tit').prepend('<div class="js_simpleslide_num"><span class="count">1</span> / <span class="all">' + slickSlider.slideCount + '</span>');

        },
        onAfterChange:function(slickSlider,i){
            $('.js_simpleslide_num .count').html(i + 1);

        }
    });
    /*$('.js_simpleslide').on('afterChange', function (event, slick, currentSlide, nextSlide) {
     console.log($(slide.$slides.get(index)).attr('id'));
     });*/
    //$('.popup_opener').uipopup();
    DOCUMENT.on('click','.popup_opener', function(e){
        e.preventDefault();
        $(this).uipopup();
    });


    $('.tbpasport').inputmask("9999 999999");
    $('.maskphone').inputmask("+7 (999) 999-99-99");
    // отключаем пока, удаляем с перезагрузкой страницы
    //$('.b-searchresult_tag .del').removeTag(); //удаление параметра поиска


    $('.js_filter').filterInit(); // инициализация фильтра

    $('.pass_status').checkPass();

    $('.js_ajax_morebook').ajaxUpdate();
    $('.js_ajax_bookupload').ajax_bookupload();

    $('.b-rolledtext').rolledText();

    $('.set_opener, .sort_opener').openHideBlock();
    $('.b-razdel .js_select').doItOnChange();
    $('.add_books_to_digitizing').add_books_to_digitizing();
    $('.add_books_to_fond').add_books_to_fond();

    DOCUMENT.on( "click", "em.error",function(e) {
        $(this).fadeOut();
    });


    DOCUMENT.on( "click", ".b-bookadd",function(e) {
        e.preventDefault();
        $('.selectionClone').remove();
        var cont = $(this), topPos = cont.offset(), par = cont.closest('.meta'),
            hint = $('.b-hint', par), block = $('.selectionBlock'),
            clone = block.clone(true) ,
            form = $('.b-selectionadd', clone),
            myfavslink = $('.b-openermenu', clone),
            item = cont.closest('.b-result-docitem'),
            metalabel = par.siblings('.metalabel');

        clone.addClass('selectionClone');

        if (cont.hasClass('fav')) {
            $('body').append('<div class="b-removepopup"></div>');
            var htmltxt = $('body').find('.b-removepopup.hidden').html();

            var removepopup = $('body > .b-removepopup:not(".hidden")');
            removepopup.html(htmltxt);
            var bt_remove = $('.btremove', removepopup),
                bt_cancel = $('.gray', removepopup);


            removepopup.css('top', topPos.top + 74);
            removepopup.show();
            if (!par.hasClass('minus') ) { metalabel.html(cont.data('plus')); metalabel.removeClass('minuslb'); }


            bt_remove.click(function(e){
                e.preventDefault();

                if ( !(cont.data('normitem') == 'Y')) {

                    item.fadeTo('slow', 0.5, function() { // удаляем книжку
                        $.ajax({
                            url: cont.data('url'),
                            complete: function(data){ // ответ от сервера
                                if(cont.data('callback') && cont.data('callback').length > 0) // выполняем callback функцию если она прописана
                                    eval(cont.data('callback'));
                            }
                        });

                        $(this).remove();
                    });
                }
                else
                {
                    $.ajax({
                        url: cont.data('url'),
                        complete: function(data){ // ответ от сервера
                            if(cont.data('callback') && cont.data('callback').length > 0) // выполняем callback функцию если она прописана
                                eval(cont.data('callback'));
                        }
                    });
                }

                if ( !(cont.data('remove') == 'removeonly')) { // для старниц, где нет добавления
                    cont.removeClass('fav');
                    par.removeClass('minus');
                    metalabel.html(cont.data('plus'));
                    metalabel.removeClass('minuslb');

                    hint.html(hint.data('plus'));
                }

                removepopup.remove();

            });
            bt_cancel.click(function(e){
                e.preventDefault();
                removepopup.remove();
            });


        } else {
            cont.addClass('fav');
            par.addClass('minus');

            metalabel.html(cont.data('minus'));
            metalabel.addClass('minuslb');
            DOCUMENT.off("click.meta").on("click.meta", function(e){
                var targ = $(e.target);
                if (!targ.hasClass('selectionClone') && targ.parents('.selectionClone').length < 1) {
                    closeNavbar();
                    DOCUMENT.off("click.meta").off("keyup.meta");
                }
            });

            DOCUMENT.off("keyup.meta").on('keyup.meta', function(e) {
                if (e.keyCode == 27) {
                    closeNavbar();
                    DOCUMENT.off("click.meta").off("keyup.meta");
                }   // esc
            });
            DOCUMENT.on('click','.selectionClone .close', function(e){
                closeNavbar();
                return false;
            });
            var nbT; //таймер 3с для исчезновения всплывашки
            function closeNavbar(){
                clone.remove();
                if(cont.closest('.slick-slider').hasClass('narrow_panel')){
                    cont.closest('.slick-list').css({'z-index':'0'});
                    cont.closest('.slick-slider').css({'overflow': 'hidden'});
                    cont.closest('.b-bookboard_main').css({'overflow': 'hidden'});
                }
            }

            nbT = window.setTimeout(function(){
                closeNavbar();
            },2000);

            clone.mouseenter(function() {
                window.clearTimeout(nbT);
            })
                .mouseleave(function() {
                    nbT = window.setTimeout(function(){
                        closeNavbar();
                    },2000);

                });


            hint.html(hint.data('minus'));
            par.append(clone);
            clone.show();


            myfavslink.click(function(e){
                e.preventDefault();
                clone.remove();
            });

            form.formAddSelection();
            $.ajax({
                url: cont.data('collection'),
                method: 'GET',
                success: function(html){
                    form.before(html).fadeIn('slow');
                    if(cont.closest('.slick-slider').hasClass('narrow_panel')){
                        cont.closest('.slick-list').css({'z-index':'1'});
                        cont.closest('.slick-slider').css({'overflow': 'visible'});
                        cont.closest('.b-bookboard_main').css({'overflow': 'visible'});
                    }
                    $('input.checkbox:not(".custom")', clone).replaceCheckBox();



                }
            });
            return false;

        }


    });

    DOCUMENT.on( "click", ".js_openmfavs",function(e) {
        e.preventDefault();
        var link = $(this), menu = link.siblings('.b-favs'), form =$('.b-selectionadd', menu);
        if(!(link.hasClass('open'))) {
            $.ajax({
                url: link.data('favs'),
                method: 'GET',
                success: function(html){
                    menu.find('.b-selectionlist').remove(); // EA
                    form.before(html).fadeIn('slow');
                    $('input.checkbox:not(".custom")', menu).replaceCheckBox();
                    form.formAddSelection();
                    menu.slideDown(200, 'easeOutQuad', function() { //EA
                        link.addClass('open');
                    });
                }
            });
            DOCUMENT.off("click.bfavs").on("click.bfavs", function(e){
                var targ = $(e.target);
                if (!targ.hasClass('b-favs') && targ.parents('.b-favs').length < 1) {
                    menu.slideUp(200, 'easeOutQuad', function() { //EA
                        link.removeClass('open');
                    });
                    DOCUMENT.off("click.bfavs").off("keyup.bfavs");
                }
            });
            DOCUMENT.off("keyup.bfavs").on('keyup.bfavs', function(e) {
                if (e.keyCode == 27) {
                    menu.slideUp(200, 'easeOutQuad', function() { //EA
                        link.removeClass('open');
                    });
                    DOCUMENT.off("click.bfavs").off("keyup.bfavs");
                }   // esc
            });
            //return false;
        } else {
            menu.slideUp(200, 'easeOutQuad', function() { //EA
                link.removeClass('open');
                DOCUMENT.off("click.bfavs").off("keyup.bfavs");
            });
        }
    });


    DOCUMENT.on( "click", ".js_passrecovery",function(e) {
        e.preventDefault();
        var link =$(this);
        var passform = $('.b-passrform');
        passform.addClass('cont_fade_popup');
        if (link.hasClass('open')) {
            passform.fadeOut(200, 'easeOutQuad', function(){
                link.removeClass('open');
                DOCUMENT.off("click.qbqbqb").off("keyup.qbqbqb");
            });

        } else {
            passform.fadeIn(200, 'easeOutQuad', function(){
                link.addClass('open');
                DOCUMENT.off("click.qbqbqb").on("click.qbqbqb", function(e){
                    var targ = $(e.target);
                    if (!targ.hasClass('cont_fade_popup') && targ.parents('.cont_fade_popup').length < 1) {
                        passform.fadeOut(200, 'easeOutQuad', function(){
                            link.removeClass('open');
                        });
                        DOCUMENT.off("click.qbqbqb").off("keyup.qbqbqb");
                    }
                });
                DOCUMENT.off("keyup.qbqbqb").on('keyup.qbqbqb', function(e) {
                    if (e.keyCode == 27) {
                        passform.fadeOut(200, 'easeOutQuad', function() { //EA
                            link.removeClass('open');
                        });
                        DOCUMENT.off("click.qbqbqb").off("keyup.qbqbqb");
                    }   // esc
                });

                DOCUMENT.on("click",'.closepopup', function(e) {
                    passform.fadeOut(200, 'easeOutQuad', function() { //EA
                        link.removeClass('open');
                    });
                    DOCUMENT.off("click.qbqbqb").off("keyup.qbqbqb");
                });
            });
        }
    });

    DOCUMENT.on( "click", ".b-headernav_login_lnk",function() {
        $('.b-headernav_login_lnk').removeClass('current');
        $(this).addClass('current');
    });


    //$('.js_ajax_loadresult').click(function(e){  // ajax заглушка на кнопку обновить
    DOCUMENT.on( "click", ".js_ajax_loadresult", function(e){  // ajax заглушка на кнопку обновить
        e.preventDefault();
        var a = $(this), resultdoc = $('.b-result-doc');
        a.css('background-image', 'url("/local/templates/.default/markup/i/ajax_loader1.gif")');
        $.ajax({
            url: a.attr('href'),
            method: 'GET',
            success: function(html){
                var ajaxinner = $($(html).html());
                a.closest('div.rel').before(html).fadeIn('slow');
                $('.knob').knobFunk();
                $('.iblock').cleanWS();
                a.css('background-image', 'url("/local/templates/.default/markup/i/ico_load.png")');
            }
        });
        return false;
    });

    $('.js_ajax_loaddata').click(function(e){  // ajax заглушка на кнопку обновить
        e.preventDefault();
        var a = $(this), resultdoc = a.closest('.b-sidenav_cont_list');
        $.ajax({
            url: a.attr('href'),
            method: 'GET',
            success: function(html){
                $(html).insertBefore(a.parent()).fadeIn('slow');
                $('.iblock').cleanWS();
                $('input.checkbox:not(".custom")').replaceCheckBox();
            }
        });
        return false;
    });

    $('.js_ajax_theme').on('click', function(e){  // ajax заглушка на кнопку следующие темы
        e.preventDefault();
        var a = $(this), BLOCK =$('.b-sidenav_cont_list');
        $.ajax({
            url: a.attr('href'),
            method: 'GET',
            success: function(html){
                $(html).insertBefore(a.parent()).fadeIn('slow');
                /* a.before(html).fadeIn('slow');*/
                $('input.checkbox:not(".custom")').replaceCheckBox();
            }

        });
        return false;
    });



    DOCUMENT.on( "click", ".js_profilemenu, .js_openmenu",function(e) {
        e.preventDefault();
        var link = $(this), menu = link.siblings('ul');
        menu.addClass('slidedown_list_container');
        if (link.hasClass('open')) {
            menu.slideUp(200, 'easeOutQuad', function(){
                link.removeClass('open');
                DOCUMENT.off("click.qbqbqb").off("keyup.qbqbqb");
            });
            //quoteaction.removeClass('visible');

        } else {
            menu.slideDown(200, 'easeOutQuad', function(){
                link.addClass('open');
                DOCUMENT.off("click.qbqbqb").on("click.qbqbqb", function(e){
                    var targ = $(e.target);
                    if (!targ.hasClass('slidedown_list_container') && targ.parents('.slidedown_list_container').length < 1) {
                        menu.slideUp(200, 'easeOutQuad', function(){
                            link.removeClass('open');
                        });
                        DOCUMENT.off("click.qbqbqb").off("keyup.qbqbqb");
                    }
                });
                DOCUMENT.off("keyup.qbqbqb").on('keyup.qbqbqb', function(e) {
                    if (e.keyCode == 27) {
                        menu.slideUp(200, 'easeOutQuad', function() { //EA
                            link.removeClass('open');
                        });
                        DOCUMENT.off("click.qbqbqb").off("keyup.qbqbqb");
                    }   // esc
                });

            });
            //quoteaction.addClass('visible');
        }
    });





    $('.js_moreopen').opened();;
    $( 'input[autocomplete="off"]' ).autocompliteFunc();
    $('.loadingimg').bindImageLoad();

    $('.mappage').mappage();
    $('.fmap').mapones();

    DOCUMENT.on('click','.canceldeletebook', function(e){
        e.preventDefault();
        var popup = $(this).closest('.uipopupcontainer ');
        popup.dialog('close');
    }).on('click','.deletebook', function(e){
        e.preventDefault();
        var link = $(this), popup = link.closest('.uipopupcontainer ');
        $.ajax({
            url: link.attr('href'),
            cache: false
        }).done(function(html) {
            popup.dialog('close');
            //тут она как то удаляется и обновляется список книг
        });
    });
    $('.book_slider').bookslider();




    $('.b-editlibform').addrowForm();
    $('.b-slidernews').slick({
        dots: false,
        infinite: true,
        cssEase: 'ease',
        slide: '.slide',
        slidesToShow: 1,
        slidesToScroll: 1,
        speed: 600
    });
    $('.b-mainblock .b-newslider').slick({
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 3,
        cssEase: 'ease',
        speed: 600
    });


    // uipopup
    DOCUMENT.on('click','.btrefuse', function(e){
        e.preventDefault();

        var popup = $(this).closest('.uipopupcontainer');
        popup.remove();
        //$('.closepopup').remove();
        //popup.dialog('close');
    });




}); // DOM loaded
$(window).load(function(){
    if ($(window).width() > 993 ) {
        $('.js_bookinfo_height').bookinfo_height(); // выравнивает высоту столбцов
    }
    $('.loadimg').remove();

});




(function() { //create closure
    $.fn.fourSlider = function() {
        this.each(function() {

            var cont = $(this);
            cont.slick({
                dots: false,
                cssEase: 'ease',

                slidesToShow: 4,
                slidesToScroll: 4,
                speed: 600
            });

        });
    }
    //end of closure
})(jQuery);

(function() { //create closure
    $.fn.newsSlider = function() {
        this.each(function() {

            var cont = $(this);

            if (cont.parents().hasClass('b-side')) {
                cont.slick({
                    dots: false,
                    cssEase: 'ease',
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    speed: 600
                });

            } else {

                cont.slick({
                    dots: false,
                    cssEase: 'ease',

                    slidesToShow: 4,
                    slidesToScroll: 4,
                    speed: 600
                });
            }


        });


    }
    //end of closure
})(jQuery);

(function() { //create closure
    $.fn.threeslide = function() {
        this.each(function() {
            var cont = $(this);
            cont.slick({
                dots: false,
                cssEase: 'ease',
                slide: '.slide',
                slidesToShow: 3,
                slidesToScroll: 1,
                speed: 600
            });
        });
    }
    //end of closure
})(jQuery);


(function() { //create closure
    $.fn.bookslider = function() {
        this.each(function() {
            var cont = $(this);
            cont.slick({
                dots: false,
                cssEase: 'ease',
                slide: '.slide',
                slidesToShow: 5,
                slidesToScroll: 1,
                speed: 600
            });
        });
    }
    //end of closure
})(jQuery);



(function(cash) { //create closure
    $.fn.dateslider = function(options) {
        this.each(function() {
            var cont = $(this), tbdate_prev = $( "#js_searchdate_prev"), tbdate_next = $( "#js_searchdate_next"),
                date_prev = $("<span class='slider_span'></span>"), date_next = $("<span class='slider_span'></span>");
            var curYear = new Date().getFullYear();
            var beginYear = 900;
            cont.slider({				range: true,
                min: beginYear,
                max: curYear,
                values: [ beginYear, curYear ],
                create: function( event, ui ) {

                    cont.slider( "values", 0, tbdate_prev.val());
                    cont.slider( "values", 1, tbdate_next.val());

                    /*$('a:eq(0)', cont).append(date_prev);
                     $('a:eq(1)', cont).append(date_next);


                     date_prev.text(tbdate_prev.val());
                     date_next.text(tbdate_next.val());*/
                },
                slide: function( event, ui ) {
                    tbdate_prev.val( ui.values[ 0 ]);
                    tbdate_next.val( ui.values[ 1 ] );
                    /*date_prev.text(ui.values[ 0 ]);
                     date_next.text(ui.values[ 1 ]);*/
                }
            });
            tbdate_prev.keyup(function(e){
                if(tbdate_prev.val().length == 4){
                    cont.slider( "values", 0, tbdate_prev.val());
                }
            });
            tbdate_next.keyup(function(e){
                if(tbdate_next.val().length == 4){
                    cont.slider( "values", 1, tbdate_next.val());
                }
            });


        });
    }
    //end of closure
})(jQuery);

(function(cash) { //create closure
    $.fn.copyFunction = function(options) {
        this.each(function() {
            var cont = $(this), par = cont.parents('.b-gostlist'),
                par_ofset = par.offset(), item = cont.closest('.b-quote_item');


            var copylink = new ZeroClipboard(cont, {
                moviePath: "ZeroClipboard.swf"
            });

            copylink.on("load", function(copylink) {

                copylink.on("complete", function(copylink, args) {
                    // выводим результат
                    //  console.log('args.text');

                });

            });
            copylink.on( "ready", function( readyEvent ) {

                copylink.on( "aftercopy", function( event ) {
                    // `this` === `client`
                    // `event.target` === the element that was clicked
                    // event.target.style.display = "none";
                    var copyinfo = $('<div class="b-copyinfo">'+par.attr('data-ok-text')+'</div>').appendTo(item);
                    copyinfo.fadeIn(400);

                    var nbT;
                    function closePopup(){
                        copyinfo.fadeOut();
                        copyinfo.remove();
                    }

                    nbT = window.setTimeout(function(){

                        closePopup();
                        window.clearTimeout(nbT);

                    },2000);


                } );
            } );


        });
    }
    //end of closure
})(jQuery);


(function() { //create closure
    $.fn.quoteInit = function() {
        this.each(function() {
            var cont = $(this),
                img = $('.b-quoteimg', cont), text = $('p', cont), ta = $('.input', cont),
                button = $('.b-quoteformat', cont), par = cont.parents('.b-quote_list');

            $('.b-quoteformat', cont).click(function(e){
                e.preventDefault();
                var lnk = $(this);

                if(lnk.hasClass('btpdf')){
                    text.addClass('hidden');
                    ta.addClass('hidden');
                    img.removeClass('hidden');

                    $(this).text(lnk.data('text'));
                    $(this).removeClass('btpdf');
                    $(this).addClass('bttxt');
                    cont.removeClass('textshow');
                } else
                if(lnk.hasClass('bttxt')){

                    text.addClass('hidden');
                    img.addClass('hidden');

                    ta.removeClass('hidden');
                    ta.focus();

                    $(this).text(lnk.data('pdf'));
                    $(this).removeClass('bttxt');
                    $(this).addClass('btpdf');
                    cont.removeClass('textshow');
                }

                var oldtxt = '';
                ta.on('focus', function(){
                    oldtxt = ta.val();
                });
                ta.on('blur', function(){
                    text.text(ta.val());
                    $(this).addClass('hidden');
                    text.removeClass('hidden');
                    cont.addClass('textshow');
                    img.addClass('hidden');
                    if (oldtxt != ta.val()) {

                        $.ajax({
                            url: par.data('action'),
                            type: 'post',
                            data: { quotetext: ta.val(), id: ta.attr('id') }
                        }).done(function(data) {
                            data = $.parseJSON(data);
                            if (data.status == 'success') {
                                //alert(data);
                                location.reload();
                            } else {
                                //alert(data);
                            }
                            return false;
                        });

                    }
                    oldtxt = '';
                });



                /*ta.focusout(function(){
                 var ta_val = ta.val();
                 text.text(ta_val);

                 $.ajax({
                 url: par.data('action'),
                 type: 'post',
                 data: { name: "John", location: "Boston" }
                 }).done(function(data) {
                 data = $.parseJSON(data);
                 if (data.status == 'success') {
                 alert(data);
                 location.reload();

                 } else {
                 alert(data);
                 }
                 return false;
                 });
                 $(this).addClass('hidden');
                 text.removeClass('hidden');
                 cont.addClass('textshow');
                 img.addClass('hidden');
                 }); */


            });




        });
    }
    //end of closure
})(jQuery);





(function(cash) { //create closure
    $.fn.removeTag = function(options) {
        this.each(function() {
            var del = $(this);

            del.click(function(e){
                e.preventDefault();
                var tag = $(this).parents('.b-searchresult_tag');
                tag.fadeTo('slow', 0.5, function() {
                    /* раскомментировать
                     $.ajax({
                     url: a.attr('href'),
                     complete: function(data){ // ответ от сервера
                     */

                    // имитация положительного ответа сервера

                    /* раскомментировать
                     }
                     });
                     */
                    $(this).remove();
                });

            });

        });
    }
    //end of closure
})(jQuery);


(function($) { //create closure
    $.fn.libFilter = function(options) {
        this.each(function() {
            var cont = $(this), btremove = $('.b-libfilter_remove', cont),
                checks = $('input[type="checkbox"]', cont), removepopup = $('.b-removepopup', cont), form = $('.b-selectionadd', cont);
            checks.change(function() {
                var stylecheck = $(this).parents('.checkbox');
                if($(this).attr('checked')){
                    stylecheck.addClass('show');
                }else{
                    stylecheck.removeClass('show');
                }
                if(checks.parents('.checkbox').hasClass('show')){  //если есть что удалять
                    btremove.show();
                } else { //если нечего
                    btremove.hide();
                }
            });

            btremove.click(function(e){
                e.preventDefault();
                removepopup.fadeIn();

            });
            $('.formbutton', removepopup).click(function(e){
                e.preventDefault();
                removepopup.fadeOut();	 // тут добавить удаление
            });

            form.formAddSelection();

        });
    }
    //end of closure
})(jQuery);


(function(cash) { //create closure
    $.fn.knobFunk = function(options) {
        this.each(function() {
            var cont = $(this);
            cont.knob({
                'draw' : function (v) {
                    var inp = this.i, val = inp.val(), label = inp.parents('.b-loadprogress').find('.b-loadlabel');
                    label.text(val + '%');
                }
            });

        });
    }
    //end of closure
})(jQuery);






(function(cash) { //create closure аккордеон справа
    $.fn.sideAccord = function(options) {
        this.each(function() {
            var cont = $(this), title = $('.b-sidenav_title', cont), searchEx = $('.b-sidenav_srch', cont);
            title.click(function(e){
                e.preventDefault();
                var bt = $(this), accblock = bt.next('.b-sidenav_cont');
                bt.toggleClass('open');
                accblock.slideToggle();
            });
            searchEx.click(function(e){
                e.preventDefault();
                var par = $(this).closest('.b-sidenav_cont'), ul = $('.b-sidenav_cont_list', par);
                ul.slideToggle();
            });

        });
    }
    //end of closure
})(jQuery);

(function(cash) { //create closure аккордеон справа
    $.fn.libfilterCheck = function(options) {
        this.each(function() {
            var cont = $(this);
            cont.addClass('checkbox');
            cont.replaceCheckBox();
        });
    }
    //end of closure
})(jQuery);



(function(cash) { //create closure
    $.fn.bookinfo_height = function(options){
        this.each(function(){

            var H = 0, el = $(this).children();
            el.height('').removeClass('evenlyready');
            el.each(function(){
                H = Math.max(H, $(this).height());

            });
            el.height(H).addClass('evenlyready');
        });

    }
    //end of closure
})(jQuery);




(function(cash) { //create closure
    $.fn.searchforminit = function(options){
        this.each(function(){
            var form = $(this), btn = $('.js_addsearchrow'),  search_row = $('.js_search_row'),
                extraform_lnk = $('.js_extraform', form), extraform = $('.b-search_ex', form);
            var iselect = $('.js_search_row select', form);

            $('.js_searchslider', form).dateslider(); // инициализация слайдера



            extraform_lnk.on('click', function(e){
                e.preventDefault();
                if (!($(this).hasClass('open'))) {
                    extraform_lnk.addClass('open');
                    extraform_lnk.text(extraform_lnk.data('extra'));
                    $('.b-searchlayer').fadeIn();
                }

                else {
                    extraform_lnk.removeClass('open');
                    extraform_lnk.text(extraform_lnk.data('default'));
                    $('.b-searchlayer').fadeOut();
                }

                extraform.slideToggle();
            });
            $('.b-searchlayer').on('click', function(){
                $('.b-searchlayer').fadeOut();
                extraform_lnk.removeClass('open');
                extraform.slideUp();
                extraform_lnk.text(extraform_lnk.data('default'));
            });

            DOCUMENT.on('submit',form,function(e){

                var vr =  extraform.find('.visiblerow'), inp = vr.find('input:text');

                inp.each(function(){
                    if ($(this).val() !== '') { extraform.addClass('open'); }

                });
            });

            form.find('.b-searchaddrow:not(".b-searchdelrow")').on('click', function(e){
                e.preventDefault();
                var clone_row = search_row.clone(true);

                search_row.before(clone_row);
                clone_row.removeClass('js_search_row hidden');
                clone_row.addClass('visiblerow');
                var n = $('.visiblerow', form).length - 1,
                    logic= $('.ddl_logic', clone_row), logic_name = logic.attr("name") + '['+ n+ ']',
                    theme = $('.theme', clone_row), theme_name = theme.attr("name") + '['+ n+ ']',
                    text = $('.b_search_txt', clone_row), text_name = text.attr("name") + '['+ n+ ']',
                    access = $('.b-access', clone_row), access_name = access.attr("name") + '[' + n + ']';

                logic.attr("name", logic_name);
                theme.attr("name", theme_name);
                text.attr("name", text_name);
                access.attr("name", access_name);


                clone_row.children().removeAttr('disabled');
                var select = $('select', clone_row);
                select.removeAttr('disabled');



                select.selectReplace();

            });

            form.find('.b-searchdelrow').on('click', function(e){
                e.preventDefault();
                $(this).parent().remove();

            });
            $('select.ddl_theme', form).bind('change', function(e){
                e.preventDefault();
                var sel =$(this);
                var way = sel.val();
                if(way == 'foraccess'){
                    sel.closest('.b_search_row').find('input.b_search_txt').addClass('hidden');
                    sel.closest('.b_search_row').find('.inpselblock.hidden select').removeClass('hidden');
                    sel.closest('.b_search_row').find('.inpselblock.hidden').removeClass('hidden').find('.inpseltxt').width('270px');
                }else {
                    sel.closest('.b_search_row').find('.b-access').parent().addClass('hidden');
                    sel.closest('.b_search_row').find('input.b_search_txt').removeClass('hidden');
                }
                if(!sel.find('option:selected').data('asrc')){
                    sel.closest('.b_search_row').find('input.b_search_txt').removeClass('ui-autocomplete-input');
                    sel.closest('.b_search_row').find('input.b_search_txt').prev('.ui-helper-hidden-accessible').remove();
                    sel.closest('.b_search_row').find('input.b_search_txt').removeAttr('autocomplete');
                    sel.closest('.b_search_row').find('input.b_search_txt').removeAttr('data-src');
                    var clone = sel.closest('.b_search_row').find('input.b_search_txt');
                    sel.closest('.b_search_row').find('input.b_search_txt').remove();
                    clone.insertBefore(sel.closest('.b_search_row').find('.b-searchaddrow'));
                    sel.closest('.b_search_row').find('input.b_search_txt').autocompliteFunc();

                }else{
                    sel.closest('.b_search_row').find('input.b_search_txt').removeClass('ui-autocomplete-input');
                    sel.closest('.b_search_row').find('input.b_search_txt').prev('.ui-helper-hidden-accessible').remove();
                    var clone = sel.closest('.b_search_row').find('input.b_search_txt');
                    sel.closest('.b_search_row').find('input.b_search_txt').remove();
                    clone.insertBefore(sel.closest('.b_search_row').find('.b-searchaddrow'));
                    sel.closest('.b_search_row').find('input.b_search_txt').attr('autocomplete','off');
                    sel.closest('.b_search_row').find('input.b_search_txt').attr('data-src',sel.find('option:selected').data('asrc'));
                    sel.closest('.b_search_row').find('input.b_search_txt').autocompliteFunc();

                }
                return false;
            });
            var btnsub = $('.js_btnsub', form);
            /*btnsub.bind('change', function(e){
             form.trigger('submit');
             });*/
            var clean = $('.js_cleaninp', form);
            var dt_prev = $('.js_searchslider', form).slider( "option", "min" );
            var dt_next = $('.js_searchslider', form).slider( "option", "max" );

            clean.on('click', function(e){
                form.find('input.b-text').val('');
                form.find('input[type="checkbox"]').removeAttr('checked');
                form.find('input[type="checkbox"]').closest('.field').removeClass('checked');
                form.find('select.ddl_theme').removeAttr('selected');
                form.find('select.ddl_theme').parent().find('.inpseltxt').text(form.find('select.ddl_theme option:eq(0)').text());
                $('.optcontainer').find('.selected').removeClass('selected');
                $('.optcontainer').find('a:first-child').addClass('selected');
                form.find('select.b-access').removeAttr('selected').parent().addClass('hidden');
                form.find('select.b-access').closest('.b_search_row').find('input.b_search_txt').removeClass('hidden');
                form.find('.js_searchslider').slider( "values", [ dt_prev, dt_next ] );
                form.find('#js_searchdate_prev').val(dt_prev);
                form.find('#js_searchdate_next').val(dt_next);
                return false;
            });

        });

    }
    //end of closure
})(jQuery);


// flexible background from image
(function(cash) { //create closure
    $.fn.flexbgimage = function(options) {
        this.each(function() {
            var img = $(this);
            if(img.get(0).tagName != 'IMG') return false;
            var par = img.parent(), fleximg = new Image();
            // картинки превратить в фон
            par.css('background-image','url("'+img.attr('src')+'")');
            if (img.data('bgposition')) {
                par.css('background-position',img.data('bgposition'));
            } else {
                par.css('background-position','50% 50%');
            }
            if (backgroundsize) par.css('background-size', 'cover');
            par.data('src', img.attr('src'));
            fleximg.src = par.data('src');
            // убить оригинальный img
            img.css('opacity', 0);
            function flexbg(e){
                if (backgroundsize) return false;
                var w = par.width(), h = par.height();
                var bK = w/h, iK = par.data('imgW')/par.data('imgH');
                var bsize = (iK>bK)? 'auto 100%' : '100% auto';
                par.css('background-size', bsize);
            }
            par.on('reFlexBgImage', flexbg);
            // отловить загрузку картинки
            $(fleximg).bind('load',function(){
                if (!img.data('loaded')) {
                    img.data('w',img.width()).data('h',img.height()).data('loaded',true);
                    par.data('imgW',img.data('w')).data('imgH',img.data('h'));
                    par.trigger('checkload');
                    par.addClass('js_flex_bgimage').trigger('reFlexBgImage');
                    img.remove();
                }
            });
            try {
                if (fleximg.complete && !img.data('loaded')) {
                    //      $(fleximg).trigger('load');
                }
            } catch (e) {
            }
        });
    }
    //end of closure
})(jQuery);

(function(cash) { //create closure ф-и для фильтра
    $.fn.filterInit = function(options) {
        this.each(function() {
            var cont = $(this), lnk_list = $('.b-filter_list', cont), lnk_items = $('.b-filter_items', cont),
                list = $('.b-filter_list_wrapper'), items =$('.b-filter_items_wrapper'),
                pages = $('.b-filter_num'), paging = $('.b-paging'), ajaxresult = $('.b-result-docmore');
            lnk_list.click(function(e){
                e.preventDefault();
                lnk_items.removeClass('current');
                $(this).addClass('current');
                items.hide(100);
                list.fadeIn(400);
            });
            pages.click(function(e){
                e.preventDefault();
                var lnk = $(this);
                pages.removeClass('current');
                lnk.addClass('current');
                if (lnk.hasClass('b-filter_num_paging')) {
                    paging.removeClass('hidden');
                    ajaxresult.addClass('hidden');
                } else {
                    paging.addClass('hidden'); ajaxresult.removeClass('hidden');
                }

            });

            lnk_items.click(function(e){
                e.preventDefault();
                lnk_list.removeClass('current');
                $(this).addClass('current');
                list.hide(100);
                items.fadeIn(400);
            });


        });
    }
    //end of closure
})(jQuery);









(function($) { //create closure
    $.fn.initForm = function(options){
        this.each(function(){
            var form = $(this),  agreebox = $('.agreebox', form), bsubmit = form.find('button'), shwn = $('.showon', form);


            var addr = $('.addrindently', form);
            addr.change(function(){
                if($(this).attr('checked')){
                    form.find('.inly input').attr('disabled','disabled');
                }else{
                    form.find('.inly input').removeAttr('disabled');

                }

            });


            form.find('.seldate').each(function(){
                var block = $(this);
                block.find('select').on('change',function(){
                    testSelDate(block);
                });
            });

            form.submit(function(e) {
                var form = $(this);



                form.formValidator();

                form.find('.seldate').each(function(){
                    var block = $(this), realseldate = block.find('.realseldate');
                    if (testSelDate(block)=='error') {
                        form.data('valid',false);
                    } else {
                        realseldate.removeClass('error');
                    }
                });

                if (form.data('valid')=== true) {
                    // $.ajax({
                    //     url: form.attr('action'),
                    //     type: 'post',
                    //     data: form.serialize(),
                    //     cache: false
                    // }).done(function(data) {
                    //     data = $.parseJSON(data);
                    //     if (data.status == 'success') {
                    //         alert(data.msg);
                    //         location.reload();
                    //     } else {
                    //         alert(data.msg);
                    //     }
                    //     return false;
                    // });
                    if(form.hasClass('b-profile_passform')){
                        e.preventDefault();
                        var c = bsubmit.parent().find('.messege_ok').clone(true);
                        c.removeClass('hidden');
                        $.ajax({
                            url: form.attr('action'),
                            type: 'post',
                            data: form.serialize(),
                            cache: false
                        }).done(function(text) {
                            if (text == '1') {
                                form.find('input').val('').removeClass('ok');
                                form.find('.validate').removeClass('ok');
                                c.dialog({
                                    closeOnEscape: true,
                                    modal: true,
                                    draggable: false,
                                    resizable: false,
                                    width: 480,
                                    dialogClass: 'ajaxpopup',
                                    position: "center",
                                    open: function(){
                                        $('.ui-button').addClass('formbutton');
                                        $('.ui-widget-overlay').addClass('black');
                                        c.find('.closepopup').click(function() {
                                            //Close the dialog
                                            c.dialog( "destroy" ).remove();
                                            return false;
                                        });
                                    },
                                    buttons: {
                                        Ok: function() {
                                            $( this ).dialog( "destroy" ).remove();
                                        }
                                    },
                                    close: function() {
                                        popup.dialog("close").remove();
                                    }
                                });

                                $('.ui-widget-overlay').click(function() {
                                    //Close the dialog
                                    c.dialog( "destroy" ).remove();
                                });
                                return false;
                            } else {
                                $('<em class="error" style="display:block;">'+text+'</em>').insertBefore(form.find('.fieldrow:last-child button'));
                                return false;
                            }
                        });
                    }else{
                        return true;
                    }

                } else {

                    var firstError = form.find('.field.error:eq(0)');
                    var errormess = firstError.find('em.error:visible');
                    var set = {
                        'font-size': errormess.css('font-size'),
                        'color': errormess.css('color'),
                        'background-color': errormess.css('background-color'),
                        'margin-left':  parseInt(errormess.css('margin-left')),
                        'opacity': 1
                    };
                    errormess = form.find('em.error:visible');
                    errormess.css({/*'font-size': '40px',*/ 'color': '#fff', /*'background-color': '#f00',*/ 'margin-left': -350, 'opacity': 0});
                    $.scrollTo(firstError, {
                        'offset':-50,
                        'duration':500,
                        'easing': 'easeOutCubic',
                        'onAfter': function(){
                            window.setTimeout(function(){
                                errormess.animate(set, 750, 'easeOutCubic', function(){
                                    errormess.css({'font-size': '', 'color': '', 'background-color': '', 'margin-left': '', 'opacity': ''});
                                });
                            }, 200);
                        }
                    });



                    firstError.trigger('focus');

                    return false;
                }

                //	return false;
            });

            agreebox.on('change',function(){

                if(agreebox.attr ("checked")) {
                    bsubmit.prop("disabled", false);

                    bsubmit.removeClass('btdisable');
                } else {
                    bsubmit.prop("disabled", true);

                    bsubmit.addClass('btdisable');
                }


            });
            shwn.on('change',function(){

                if(shwn.attr ("checked")) {
                    shwn.closest('.checkwrapper').next().find('input').prop("disabled", false);
                    shwn.closest('.checkwrapper').next().find('.field').removeClass('disbl');
                    shwn.closest('.checkwrapper').next().find('label').removeClass('disbl');
                } else {
                    shwn.closest('.checkwrapper').next().find('input').prop("disabled", true);
                    shwn.closest('.checkwrapper').next().find('.field').addClass('disbl');
                    shwn.closest('.checkwrapper').next().find('label').addClass('disbl');
                }


            });
            var scan = form.find('.setscan input[type="file"]');
            scan.on('change',function(){

                if($(this).val() !='') {
                    bsubmit.prop("disabled", false);

                    bsubmit.removeClass('btdisable');
                } else {
                    bsubmit.prop("disabled", true);

                    bsubmit.addClass('btdisable');
                }
            });
            var fullreg = form.find('.radiolist #rb1');

            fullreg.on('change bind',function(){

                if(fullreg.attr("checked")) {
                    fullreg.closest('.radiolist').find('.sub_radio input').prop("disabled", false);
                    fullreg.closest('.radiolist').find('.sub_radio .disable').prev().find('input').prop("disabled", true);
                    fullreg.closest('.radiolist').find('.sub_radio .b-radio:first-child input').attr('checked','checked');
                    fullreg.closest('.radiolist').find('.sub_radio .b-radio:first-child .radio').addClass('checked');
                } else {
                    fullreg.closest('.radiolist').find('.sub_radio input').removeAttr('checked');
                    fullreg.closest('.radiolist').find('.sub_radio .radio').removeClass('checked');
                    fullreg.closest('.radiolist').find('.sub_radio input').prop("disabled", true);
                }


            });




        });
    }
    //end of closure
})(jQuery);


(function($) { //create closure сохранить поисковой запрос
    $.fn.searchSave = function(options) {
        this.each(function() {
            var cont = $(this), form = $('.searchsave_form', cont);

            $("body").on('submit', '.searchsave_form', function(e) {
                e.preventDefault();

                var popup = $('.b-searchsave_popup');

                $.ajax({
                    type: "POST",
                    url: form.attr('action'),
                    data: form.serialize(),
                    dataType: "json"
                })
                    .success(function(json) {
                        // имитация положительного ответа сервера
                        if(json.ID <= 0)
                            return false;

                        //var tbox = form.find('.input');
                        var tbox = $('.b-searchsave_popup .input');
                        popup.fadeIn();
                        DOCUMENT.off("click.b-search_save").on("click.b-search_save", function(e){
                            var targ = $(e.target);
                            if (!targ.hasClass('b-searchsave_popup') && targ.parents('.b-searchsave_popup').length < 1) {
                                closePopup();
                                DOCUMENT.off("click.b-search_save").off("keyup.b-search_save");
                            }
                        });

                        DOCUMENT.off("keyup.b-search_save").on('keyup.b-search_save', function(e) {
                            if (e.keyCode == 27) {
                                closePopup();
                                DOCUMENT.off("click.b-search_save").off("keyup.b-search_save");
                            }   // esc
                        });
                        popup.find('.close').on('click',function(e){
                            closePopup();
                            return false;
                        });
                        var nbT;
                        function closePopup(){
                            popup.fadeOut();
                        }

                        nbT = window.setTimeout(function(){
                            closePopup();
                        },2000);

                        popup.mouseenter(function() {

                            window.clearTimeout(nbT);
                        })
                            .mouseleave(function() {
                                nbT = window.setTimeout(function(){
                                    closePopup();
                                },2000);

                            });




                        window.clearTimeout(nbT);

                        tbox.keypress(function(e){
                            if(e.keyCode==13){
                                $.ajax({
                                    type: "POST",
                                    url: form.attr('action'),
                                    data:{saveId: json.ID, name: tbox.val()}
                                })
                                    .success(function(json) {
                                        // имитация положительного ответа сервера
                                        closePopup();
                                        return false;

                                    });
                            }
                        });
                    });
            });
        });
    }
    //end of closure
})(jQuery);



(function($){//create closure
    $.fn.ajaxUpdate = function(options){
        var link = $(this), BOOKBLOCK = link.prev('.b-bookboard_list');

        link.click(function(e){
            e.preventDefault();
            BX.showWait('js_ajax_morebook');
            $.ajax({
                url: link.attr('href'),
                method: 'GET',
                success: function(html){
                    var ajaxinner = $($(html).html()), lnk = $('.js_ajax_morebook', ajaxinner);
                    BOOKBLOCK.remove();
                    link.before(html).fadeIn('slow');
                    $('.js_ajax_morebook').ajaxUpdate();
                    $('.iblock').cleanWS();

                    //$('.popup_opener').uipopup(); !!! listener set up over DOCUMENT
                    link.remove();
                    BX.closeWait();
                }
            });
            return false;
        });

    }//close closure
})(jQuery);



(function($){//create closure
    $.fn.ajax_bookupload = function(options){
        var link = $(this), BOOKPOP = $('.b_bookpopular');

        link.click(function(e){
            e.preventDefault();
            $.ajax({
                url: link.attr('href'),
                method: 'GET',
                success: function(html){
                    var ajaxinner = $($(html).html()), lnk = $('.js_ajax_morebook', ajaxinner);
                    BOOKPOP.remove();
                    link.before(html).fadeIn('slow');
                    $('.js_ajax_bookupload').ajax_bookupload();
                    $('.iblock').cleanWS();
                    //$('.popup_opener').uipopup(); !!! listener set up over DOCUMENT
                    link.remove();
                }
            });
            return false;
        });

    }//close closure
})(jQuery);



/* $('.js_ajax_bookupload').click(function(e){ // ajax заглушка на кнопку обновить
 e.preventDefault();
 var a = $(this), BOOKPOP = $('.b_bookpopular');
 $.ajax({
 url: a.attr('href'),
 method: 'GET',
 success: function(html){

 BOOKPOP.remove();
 a.before(html).fadeIn('slow');
 $('.iblock').cleanWS();

 }


 });

 return false;
 });
 */




(function() {
    $.fn.opened = function(options) {
        this.each(function() {
            var a = $(this), par =a.closest('ul');
            a.click(function(e){
                e.preventDefault();
                par.find('li.hidden').removeClass('hidden');
                a.parent().addClass('hidden');
            });

        });
    }
    //end of closure
})(jQuery);

(function() {  //чекбокс читальельского билета и его проверка
    $.fn.ticketCheck = function(options) {
        this.each(function() {
            var cont = $(this),
                cbox =cont.find('input.checkbox'),
                label = cont.find('label.disable'), input = cont.find('.input');


            cbox.on('change',function(){

                if(cbox.attr("checked")) {

                    input.prop("disabled", false);
                    label.removeClass('disable');
                } else {

                    input.prop("disabled", true);
                    label.addClass('disable');
                }
            });

        });
    }
    //end of closure
})(jQuery);


(function() {
    $.fn.autocompliteFunc = function(options) {
        this.each(function() {
            var sInput = $(this);

            $.widget( "custom.catcomplete", $.ui.autocomplete, {
                _create: function() {
                    this._super();
                    this.widget().menu( "option", "items", "> :not(.ui-autocomplete-category)" );
                },
                _renderMenu: function( ul, items ) {
                    var that = this,
                        currentCategory = "";
                    $.each( items, function( index, item ) {
                        var li;
                        if ( item.category != currentCategory ) {
                            ul.append( "<li class='ui-autocomplete-category'>" + item.category + "</li>" );
                            currentCategory = item.category;
                        }
                        li = that._renderItemData( ul, item );
                        if ( item.category ) {
                            li.attr( "aria-label", item.category + " : " + item.label );
                        }
                    });
                },
                select: function(e, ui) {},
                response: function(e, ui) {}
            });
            if(sInput.data('src')){
                sInput.catcomplete({
                    delay: 0,
                    minLength: 3,
                    source: sInput.data('src')
                });
            }
        });
    }
    //end of closure
})(jQuery);


(function($) { //create closure
    $.fn.seldate = function() {
        var block = $(this), realinp = block.find('.realseldate'), presetdate = realinp.val();
        if (presetdate && presetdate!=''){
            presetdate = presetdate.split('.');
            var dd = block.find('.sel_day'), mm = block.find('.sel_month'), yy = block.find('.sel_year');
            dd.val(presetdate[0]);
            mm.val(presetdate[1]);
            yy.val(presetdate[2]);
        }
    }
})(jQuery);



(function() {  //настройки количества результатов поиска
    $.fn.searchNum = function() {
        this.each(function() {
            var cont = $(this),
                a =cont.find('a'),
                hf = cont.find('input');

            a.click(function(e){
                e.preventDefault();
                a.removeClass('current');
                $(this).addClass('current ');
                hf.val(parseInt($(this).text()));
            });

        });
    }
    //end of closure
})(jQuery);

(function() {  // cлайдер из  5 фото
    $.fn.mulislider = function() {
        this.each(function() {
            var cont = $(this);
            cont.slick({
                infinite: true,
                slidesToShow: 5,
                slidesToScroll: 5,
                cssEase: 'ease',
                speed: 600
            });

        });
    }
    //end of closure
})(jQuery);


(function() {  //страница карточки книги
    $.fn.cardPage = function() {
        this.each(function() {
            var cont = $(this),
                bookimg = cont.find('.b-bookframe_img'),
                bg = $('.b-book_bg');

            bg.css({'background':'url("'+bookimg.attr('src')+'") no-repeat scroll 0 0 rgba(0, 0, 0, 0)','background-size':'100% auto','opacity': 0});
            bg.blurjs({
                source: '.b-book_bg',
                radius:4,
                overlay: 'rgba(255,255,255,0.4)'
            });
            bookimg.on('load', function(){
                bg.animate({'opacity': 1}, 1500, 'easeOutQuad');
            });


        });
    }
    //end of closure
})(jQuery);

(function() {  //страница карточки книги
    $.fn.digitalForm = function() {
        this.each(function() {
            var cont = $(this), lnk = $('.b-digital_desc', cont),  close = $('.close', cont);
            var BODY = $('body')
            lnk.click(function(e){
                e.preventDefault();
                BODY.trigger('heightchangestart');


                var tr = $(this).closest('tr'), descrTR = tr.next('tr.scrolled'),
                    digitalpopup = $('.b-infobox[data-link="digital"]', descrTR),
                    descrpopup = $('.b-infobox[data-link="descr"]', descrTR);

                if (!$(this).hasClass('current')) {
                    $('.b-infobox', cont).hide();
                    if ($(this).hasClass('b-digital')) {
                        digitalpopup.slideDown(250, 'easeOutQuad', function(){
                            BODY.trigger('heightchangeend');
                        });
                    } else {
                        descrpopup.slideDown(250, 'easeOutQuad', function(){
                            BODY.trigger('heightchangeend');
                        });
                    }
                }
                else {
                    return false;
                }

                lnk.removeClass('current');
                $(this).addClass('current');
            });
            close.click(function(e){
                e.preventDefault();
                BODY.trigger('heightchangestart');
                var box = $(this).closest('.b-infobox');
                box.slideUp(250, 'easeOutQuad', function(){
                    BODY.trigger('heightchangeend');
                });
                lnk.removeClass('current');
            });


        });
    }
    //end of closure
})(jQuery);

(function() {  //страница карточки книги
    $.fn.descrPopup = function() {
        this.each(function() {
            var cont = $(this), lnk = $('.js_descr_link', cont),  close = $('.close', cont);
            lnk.click(function(e){
                e.preventDefault();

                var descrpopup = $('.b-infobox[data-link="descr"]', cont);

                descrpopup.slideToggle();


            });
            close.click(function(e){
                e.preventDefault();
                var box = $(this).closest('.b-infobox');
                box.slideUp();

            });


        });
    }
    //end of closure
})(jQuery);




(function() {  //страница карточки книги
    //	$.fn.addDigital = function() {
    //this.each(function() {
    //	var plus_ico = $(this);

    $( document ).on( "click", ".plusico_wrap .plus_ico",function(e) {
        var lnk = $(this),  par = $(this).closest('.plusico_wrap'), hint = par.find('.b-hint'), html_remove, html_add;


        if (lnk.hasClass('plus_digital') ) {
            e.preventDefault();
            html_remove = hint.data('minus'),
                html_add = hint.data('plus');
            par.toggleClass('minus');
            if ( par.hasClass('minus')) {
                hint.html(html_remove);
            } else {
                hint.html(html_add);
            }
        }



        if (lnk.hasClass('lib') ) {
            html_remove = hint.data('minus'),
                html_add = hint.data('plus');

            /*удаление*/
        }





    });
    //	});
    //}
    //end of closure
})(jQuery);


(function() {  //популярные издания
    $.fn.popularSlider = function() {
        this.each(function() {
            var cont = $(this), items = $('.b-result-docitem', cont), num = 1, list;
            if (cont.data('slides') == 'one') {

                cont.slick({
                    infinite: true,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    cssEase: 'ease',
                    speed: 600
                });


            }
            else {

                items.each(function(i,elem){
                    $(this).append( "<span class='num'>" + num + ".</span>" );
                    num++;

                    if ( i == 0 ) {
                        list = $("<div class='b-result-doc b-result-docfavorite'></div>").appendTo(cont);
                    }
                    $(this).appendTo(list);
                    var slidenum = 6;

                    if (cont.data('slides') == 'three' ) { slidenum = 3; }
                    if (i%slidenum==0 && i > 0) {
                        list = $("<div class='b-result-doc b-result-docfavorite'></div>").appendTo(cont);
                        $(this).appendTo(list);

                    }


                });
                cont.addClass('slidetype');

                var lists = $('.b-result-docfavorite', cont);
                lists.wrap('<div />');

                cont.slick({
                    dots: false,
                    cssEase: 'ease',
                    speed: 600
                });



            }



        });
    }
    //end of closure
})(jQuery);


(function() {  //популярные издания
    $.fn.editCollection = function() {
        this.each(function() {
            var link = $(this), block = $('.js_addbook'),
                h = parseInt($(window).height()) - parseInt($('footer').outerHeight()),
                close = $('.backpage', block),
                hp = $(".homepage");


            link.click(function(e){
                e.preventDefault();
                block.height(h + 70);
                block.fadeIn();

                $("html, body").animate({ scrollTop: 0 }, "fast");
                hp.height(h);
                hp.css('overflow', 'hidden');

            });
            close.click(function(e){
                block.hide();
                hp.height('auto');
                hp.css('overflow', 'auto');
            });
            $( window ).resize(function() {
                block.hide();
                hp.height('auto');
                hp.css('overflow', 'auto');
            });

        });
    }
    //end of closure
})(jQuery);
var map;
// maps page
(function($) { //create closure
    $.fn.mappage = function(options) {
        this.each(function() {

            var defaults = {};
            var errors = 0,
                msg = '',
                o;
            if (typeof options != 'string') {
                o = $.extend(defaults, options);
            } else {
                o = defaults;
            }
            var cont = $(this);
            ymaps.ready(init); // on MAP READY
            var maproot = $('#ymap'), form = cont.find('.b-map_search_filtr'), exmarker = cont.find('.marker_lib.hidden');

            function remove_map_clone()
            {
                if($('#ymaps_clone').length)
                    $('#ymaps_clone').remove();
            }
            function clone_map_clone(map)
            {
                remove_map_clone();
                var map_clone = $('ymaps.ymaps-b-balloon').clone();
                map_clone.attr('id', "ymaps_clone").css({'left':'0px', 'top':'0px'});
                $('div.b-map').prepend(map_clone);
                map_clone.find('ymaps.ymaps-b-balloon__close').one("click", function(){
                    remove_map_clone();
                });
                map.balloon.close();
            }
            function init() {
                map = new ymaps.Map("ymap", {
                    center: [(maproot.data('lat')) ? maproot.data('lat') : 55.76, (maproot.data('lng')) ? maproot.data('lng') : 37.64],
                    zoom: (maproot.data('zoom')) ? maproot.data('zoom') : 10,
                });


                // РЎРѕР·РґР°РґРёРј РїРѕР»СЊР·РѕРІР°С‚РµР»СЊСЃРєРёР№ РјР°РєРµС‚ РїРѕР»Р·СѓРЅРєР° РјР°СЃС€С‚Р°Р±Р°.
                ZoomLayout = ymaps.templateLayoutFactory.createClass("<div>" +
                "<div id='zoom-in' class='zoom_b'></div><br>" +
                "<div id='zoom-out' class='zoom_s'></div>" +
                "</div>", {

                    // РџРµСЂРµРѕРїСЂРµРґРµР»СЏРµРј РјРµС‚РѕРґС‹ РјР°РєРµС‚Р°, С‡С‚РѕР±С‹ РІС‹РїРѕР»РЅСЏС‚СЊ РґРѕРїРѕР»РЅРёС‚РµР»СЊРЅС‹Рµ РґРµР№СЃС‚РІРёСЏ
                    // РїСЂРё РїРѕСЃС‚СЂРѕРµРЅРёРё Рё РѕС‡РёСЃС‚РєРµ РјР°РєРµС‚Р°.
                    build: function () {
                        // Р’С‹Р·С‹РІР°РµРј СЂРѕРґРёС‚РµР»СЊСЃРєРёР№ РјРµС‚РѕРґ build.
                        ZoomLayout.superclass.build.call(this);

                        // РџСЂРёРІСЏР·С‹РІР°РµРј С„СѓРЅРєС†РёРё-РѕР±СЂР°Р±РѕС‚С‡РёРєРё Рє РєРѕРЅС‚РµРєСЃС‚Сѓ Рё СЃРѕС…СЂР°РЅСЏРµРј СЃСЃС‹Р»РєРё
                        // РЅР° РЅРёС…, С‡С‚РѕР±С‹ РїРѕС‚РѕРј РѕС‚РїРёСЃР°С‚СЊСЃСЏ РѕС‚ СЃРѕР±С‹С‚РёР№.
                        this.zoomInCallback = ymaps.util.bind(this.zoomIn, this);
                        this.zoomOutCallback = ymaps.util.bind(this.zoomOut, this);

                        // РќР°С‡РёРЅР°РµРј СЃР»СѓС€Р°С‚СЊ РєР»РёРєРё РЅР° РєРЅРѕРїРєР°С… РјР°РєРµС‚Р°.
                        $('#zoom-in').bind('click', this.zoomInCallback);
                        $('#zoom-out').bind('click', this.zoomOutCallback);
                    },

                    clear: function () {
                        // РЎРЅРёРјР°РµРј РѕР±СЂР°Р±РѕС‚С‡РёРєРё РєР»РёРєРѕРІ.
                        $('#zoom-in').unbind('click', this.zoomInCallback);
                        $('#zoom-out').unbind('click', this.zoomOutCallback);

                        // Р’С‹Р·С‹РІР°РµРј СЂРѕРґРёС‚РµР»СЊСЃРєРёР№ РјРµС‚РѕРґ clear.
                        ZoomLayout.superclass.clear.call(this);
                        remove_map_clone();
                    },

                    zoomIn: function () {
                        var map = this.getData().control.getMap();
                        // Р“РµРЅРµСЂРёСЂСѓРµРј СЃРѕР±С‹С‚РёРµ, РІ РѕС‚РІРµС‚ РЅР° РєРѕС‚РѕСЂРѕРµ
                        // СЌР»РµРјРµРЅС‚ СѓРїСЂР°РІР»РµРЅРёСЏ РёР·РјРµРЅРёС‚ РєРѕСЌС„С„РёС†РёРµРЅС‚ РјР°СЃС€С‚Р°Р±РёСЂРѕРІР°РЅРёСЏ РєР°СЂС‚С‹.
                        this.events.fire('zoomchange', {
                            oldZoom: map.getZoom(),
                            newZoom: map.getZoom() + 1
                        });
                        remove_map_clone();
                    },

                    zoomOut: function () {
                        var map = this.getData().control.getMap();
                        this.events.fire('zoomchange', {
                            oldZoom: map.getZoom(),
                            newZoom: map.getZoom() - 1
                        });
                        remove_map_clone();
                    }
                }),

                    zoomControl = new ymaps.control.SmallZoomControl({
                        layout: ZoomLayout
                    });

                map.controls.add(zoomControl, {right: 0, top: 0});


                /*map.controls.add('zoomControl', {
                 left: 20,
                 top: maproot.height()/2
                 }); */

                map.balloon.events.add('open', function(e){
                    clone_map_clone(map);
                });

                map.events.add('click', function(e) {
                    if(map.balloon.isOpen()) {
                        map.balloon.close();
                    }
                    remove_map_clone();
                });
                map.events.add('wheel', function(e) {
                    remove_map_clone();
                });
                var clusterIcon = [{
                        href: '/local/templates/.default/markup/i/clustericon.png',
                        size: [55, 55],
                        offset: [-20, -20]
                    }],
                    clusterNumbers = [10,40],
                    clusterer = new ymaps.Clusterer({
                        clusterIcons: clusterIcon,
                        clusterNumbers: clusterNumbers
                    });
                clusterer.options.set({gridSize: 80});
                map.behaviors.enable(['scrollZoom']);
                if (!maproot.data('path')) {
                    alert('Не указан путь к файлу с точками');
                    return false;
                }
                var arr =[];
                function resetPlacemarks(libs) {
                    for (var i = 0; i < libs.length; i++) {
                        var id = libs[i].id;
                        libs[i].pmset = new ymaps.GeoObjectCollection({}, {
                            iconImageHref: libs[i].placemarkicon,
                            iconImageSize: libs[i].placemarksize,
                            iconImageOffset: [-libs[i].placemarksize[0] / 2, -libs[i].placemarksize[1] / 2]
                        });

                        for (var k = 0; k < libs[i].regions.length; k++) { // добавить в нее точки
                            var city = libs[i].regions[k].city,
                                coordinates = libs[i].regions[k].coordinates,
                                metro = libs[i].regions[k].metro; // metro array (minimum, one item)
                            for (var m = 0; m < metro.length; m++) {
                                var station = metro[m].station;
                                for (var p = 0; p < metro[m].points.length; p++) {
                                    var point = metro[m].points[p];

                                    var placemark = new ymaps.GeoObject({
                                        geometry: {
                                            type: "Point",
                                            coordinates: [point.coordinates[0], point.coordinates[1]]
                                        },
                                        properties: {

                                            balloonContent: '<span class="marker_lib">' +
                                            ((point.img) ? '<img alt="" src="'+ point.img+'">': '') +
                                            ((point.link) ? '<a href="'+ point.link+'" class="b-elar_name_txt">' + point.name + '</a><br />':'<span class="b-elar_name_txt">' + point.name + '</span>')+
                                            (point.status ? '<span class="b-elar_status">'+ point.status + '</span><br />' : '') +
                                            ((point.site) ? '<span class="b-libsite"><a target="_blank" href="'+ point.mail+'">' + point.site + '</a></span>' : '') +
                                            ((point.fond) ? '<span class="b-libfond">Р’ С„РѕРЅРґРµ Р±РёР±Р»РёРѕС‚РµРєРё Р±РѕР»РµРµ <strong>'+ point.fond + '</strong> РёР·РґР°РЅРёР№</span>' : '') +
                                            '<span class="b-map_elar_info"><span class="b-map_elar_infoitem addr"><span>'+exmarker.find('.b-map_elar_info .addr span:first-child').text()+'</span>' + ((station) ? ' Рј. ' + station + ', ' : '') + point.address + '</span>' +
                                            ((point.phone) ? '<span class="b-map_elar_infoitem phone">'+ point.phone+'</span>': '') +
                                            ((point.mail) ? '<span class="b-map_elar_infoitem mail"><a target="_blank" href="mailto:'+ point.mail+'">' + point.mail +'</a></span>': '') +
                                            ((point.available) ? '<span class="b-map_elar_infoitem"><span>'+exmarker.find('.b-map_elar_info .graf  span:first-child').text()+'</span>' + point.available + '</span>':'')+'</span>'+
                                            '<span class="b-mapcard_act clearfix">' + ((point.parten) ? '<span class="right neb b-mapcard_status">'+exmarker.find('.b-mapcard_act .b-mapcard_status').text()+'</span>':'') +
                                            ((point.link) ? '<a href="'+ point.link+'" class="button_mode">'+exmarker.find('.b-mapcard_act .button_mode').text()+'</a></span></span>':'')
                                        }
                                    }, {
                                        balloonCloseButton: true,
                                        balloonOffset: [207, 160],
                                        balloonMaxWidth: 300,
                                        balloonMinWidth: 300,
                                        balloonMaxHeight: 600,
                                        balloonMinHeight: 600,
                                        balloonAutoPan: true,
                                        hideIconOnBalloonOpen: false,
                                        iconImageHref: '/local/templates/.default/markup/i/pin_mo.png', //i/pin.png
                                        iconImageSize: [34, 35],
                                        iconImageOffset: [-17, -24]

                                    });
                                    libs[i].pmset.add(placemark);
                                    clusterer.add(placemark);
                                    arr.push(placemark);
                                    if ($('.b-nebmap').length) {
                                        placemark.events.add('click', function () {
                                            console.log(map.balloon);
                                        });

                                    }


                                }
                            }
                        }
                        //map.geoObjects.add(libs[i].pmset);
                        map.geoObjects.add(clusterer);
                    }
                }
                maproot.on('libsloaded', function(e, libs) {
                    resetPlacemarks(libs);
                });

                $.getJSON(maproot.data('path'), function(json) {
                    $.each(json, function(key, val) {
                        maproot.trigger('libsloaded', [val]);
                    });
                });



                form.on('submit', function(e) {
                    e.preventDefault();
                    ymaps.geocode(field.val(), {
                        boundedBy: map.getBounds(),
                        results: 1
                    }).then(function(res) {
                            var firstGeoObject = res.geoObjects.get(0);
                            if (! firstGeoObject) { setMsk(); } else {

                                var coords = firstGeoObject.geometry.getCoordinates(),
                                    bounds = firstGeoObject.properties.get('boundedBy');
                                //				                bounds = firstGeoObject.properties.get('boundedBy');
                                map.setBounds(bounds, {
                                    checkZoomRange: true // проверяем наличие тайлов на данном масштабе.
                                });

                            }
                        },
                        function (err) {

                        });
                });


                /*				$( document ).on( "click", ".setMsk",function(e) {
                 e.preventDefault();
                 setMsk();

                 });
                 function setMsk(){
                 field.val('Москва');
                 ymaps.geocode('Москва', {
                 boundedBy: map.getBounds(),
                 results: 1
                 }).then(
                 function(res) {

                 var firstGeoObject = res.geoObjects.get(0),
                 coords = firstGeoObject.geometry.getCoordinates(),
                 bounds = firstGeoObject.properties.get('boundedBy');
                 //				                bounds = firstGeoObject.properties.get('boundedBy');
                 map.setBounds(bounds, {
                 checkZoomRange: true // проверяем наличие тайлов на данном масштабе.
                 });


                 });
                 }*/

                DOCUMENT.on( "click", ".setLinkPoint",function(e) {
                    e.preventDefault();
                    if($(this).hasClass("setPointAndReload")){
                        //при клике на "Да, верно" ставим город в куку и обновляем страницу
                        setCityAndReload($(this).data("city-id"));
                    } else {
                        //при клике на название города ставим его на карте
                        setLinkPoint($(this).data("lat"), $(this).data("lng"));
                    }
                });
                function setLinkPoint(link_lat, link_lng){
                    ymaps.geocode(link_lat + ", " + link_lng, {
                        boundedBy: map.getBounds(),
                        results: 1
                    }).then(
                        function(res) {

                            var firstGeoObject = res.geoObjects.get(0),
                                coords = firstGeoObject.geometry.getCoordinates(),
                                bounds = firstGeoObject.properties.get('boundedBy');
                            console.log(bounds);
                            //				                bounds = firstGeoObject.properties.get('boundedBy');
                            map.setBounds(bounds, {
                                //checkZoomRange: true // проверяем наличие тайлов на данном масштабе.
                            });

                            map.setZoom(10);
                        });
                }

                var availabletext = [],
                    boundedBy = [],
                    field = $(".searchonmap");


                field.autocomplete({
                    source: availabletext,
                    minLength: 5,
                    appendTo: field.parents('.b-map_search_filtr')
                }).click(function() {
                    $(this).autocomplete('search');
                }).keyup(function(event) {
                    var inp = $(this),
                        val = inp.val();
                    availabletext = [], boundedBy = [];
                    if (val.length > 4) { //autocomplit
                        //////////////////////////////////////////////////////////////////
                        //map.setBounds(map.getBounds())
                        ymaps.geocode(val, {
                            boundedBy: map.getBounds(),
                            results: 5
                        }).then(function(res) {
                            res.geoObjects.each(function(key) {
                                if (availabletext.indexOf(key.properties.get('name')) < 0) {
                                    availabletext.push(key.properties.get('name'));
                                    boundedBy.push(key.properties.get('boundedBy'));
                                    boundedBy.push(map.getBounds());
                                }
                            });
                        });
                        inp.autocomplete("option", "source", availabletext);
                        /////////////////////////////////////////////////////////////////
                    }

                });

                cont.parent().find('.b-elar_address_link').each(function(){
                    var maplink = $(this);
                    if (!maplink.data('lat') || !maplink.data('lng')) return false;
                    maplink.click(function(e){
                        e.preventDefault();
                        //$.scrollTo('.b-map_search_filtr', {
                        //    duration: 650,
                        //    easing: 'easeOutBack'
                        //});
                        setTimeout(function(){
                            map.panTo([maplink.data('lat'),maplink.data('lng')], {
                                flying: false,
                                delay: 0,
                                duration: 600
                            });
                        },0);

                        map.setZoom(16, {duration: 600});
                        setTimeout(function(){
                            for(var m = 0; m < arr.length; m++){
                                var coords = arr[m].geometry.getCoordinates();
                                if(coords[0] == maplink.data('lat') && coords[1] == maplink.data('lng')){
                                    map.geoObjects.add(arr[m]);
                                    arr[m].balloon.open();
                                    map.setCenter([maplink.data('lat'),maplink.data('lng')],16);
                                    clone_map_clone(map);
                                }
                            }
                        }, 300);
                    });
                });
            }
        });
    }
    //end of closure
})(jQuery);



(function() { //create closure
    $.fn.registerPopup = function() { // попап регистрации

        this.each(function() {
            var lnk = $(this), popup = $('.b_register_popup').clone( true ),
                H = (lnk.data('height')) ? lnk.data('height') : 595,
                W = (lnk.data('width')) ? lnk.data('width') : 560;
            lnk.click(function(){
                popup.dialog({
                    closeOnEscape: true,
                    dialogClass: 'no-close',
                    modal: true,
                    draggable: false,
                    resizable: false,
                    width:W,
                    height: H,
                    dialogClass: 'commonpopup',
                    position: {
                        my: "right top",
                        at: "right bottom",
                        of: lnk
                    },
                    open: function(){
                        // window.setTimeout(function() {
                        // 	popup.dialog("option", "position", {
                        // 		my: "right top",
                        // 		at: "right bottom",
                        // 		of: lnk
                        // 	});
                        // 	}, 50);

                        popup.find('.b-form').initForm();
                    },
                    close: function() {
                        popup.dialog("close").remove();
                    }
                });

                $('.ui-widget-overlay').click(function() {
                    //Close the dialog
                    popup.dialog("close");

                });
            });

        });
    }
    //end of closure
})(jQuery);



//функция устанавливает cookie c нужным id города и обновляет страницу
function setCityAndReload(city_id){
    var date = new Date( new Date().getTime() + 1000 * 60 * 60 * 24 * 365); //1 год
    document.cookie="library_city="+city_id+"; path=/; expires="+date.toUTCString();
    if(window.location.href.indexOf('/special/library/') > -1)
    {
        window.location.href = '/special/library/';
    }
    else
    {
        window.location.href = '/library/';
    }
}

(function() { //create closure
    $.fn.cityChoose = function() { // попап регистрации

        this.each(function() {
            var bt_popup = $('.js_changecity'),
                popup = $('.b-citychangepopup').clone( true ), line = $('.b-citypopup'),
                close_line = $('.close', line);


            close_line.click(function(e){
                e.preventDefault();
                line.fadeOut(300);
            });


            bt_popup.click(function(e){
                e.preventDefault();
                line.fadeOut(300);
                popup.dialog({
                    closeOnEscape: true,
                    dialogClass: 'no-close',
                    modal: true,
                    draggable: false,
                    resizable: false,
                    width:600,
                    dialogClass: 'citypopup',
                    open: function(){
                        window.setTimeout(function() {
                            popup.dialog("option", "position", {
                                my: "center top+68",
                                at: "center top",
                                of: window
                            });
                        }, 50);

                        $('.iblock', popup).cleanWS();
                        $('input.checkbox:not(".custom")', popup).replaceCheckBox();
                        var overlay = $('.ui-widget-overlay');
                        overlay.addClass('black'), region = $('.b-handcityregion', popup);


                        var form  = $('.b-map_search_form', popup), tbox = $('.input', form);

                        region.click(function(){

                            var reg = $(this), par = reg.closest('.b-handcityitem'), list = $('.b-cityquisklist', par);
                            par.toggleClass('open');
                            list.slideToggle();
                        });

                        $('.b-city', form).click(function(e){
                            e.preventDefault();
                            //tbox.val($(this).text());
                            setCityAndReload($(this).data('city-id'));
                        });


                        var availabletext = [],
                            boundedBy = [];



                        tbox.autocomplete({
                            source: availabletext,
                            minLength: 3,
                            appendTo: tbox.parents(form),
                            select: function( event, ui ) {
                                $(form).find(".formbutton").attr("data-city-id", ui.item.id);
                            }
                        }).click(function() {
                            $(this).autocomplete('search');
                        }).keyup(function(event) {
                            var inp = $(this),
                                ajax_path = $(inp).data('path'),
                                val = inp.val();
                            availabletext = [], boundedBy = [];
                            if (val.length > 2) { //autocomplit
                                /*										//////////////////////////////////////////////////////////////////
                                 //map.setBounds(map.getBounds())
                                 ymaps.geocode(val, {
                                 boundedBy: map.getBounds(),
                                 results: 5
                                 }).then(function(res) {
                                 res.geoObjects.each(function(key) {
                                 if (availabletext.indexOf(key.properties.get('name')) < 0) {
                                 availabletext.push(key.properties.get('name'));
                                 boundedBy.push(key.properties.get('boundedBy'));
                                 boundedBy.push(map.getBounds());
                                 }
                                 });
                                 });
                                 inp.autocomplete("option", "source", availabletext);
                                 /////////////////////////////////////////////////////////////////*/

                                inp.autocomplete("option", "source", ajax_path);
                            }

                        });


                        form.on('submit', function(e) {
                            e.preventDefault();
                            //$('.searchonmap').val( tbox.val());
                            //$('.b-map_search_filtr').submit();
                            var city_id = $(form).find(".formbutton").attr("data-city-id");
                            if(city_id > 0) setCityAndReload(city_id);
                            line.fadeOut(300);
                            popup.dialog("close");
                        });



                    },
                    close: function() {
                        popup.dialog("close").remove();
                    }
                });
                DOCUMENT.on( "click", ".closepopup",function(e) {
                    e.preventDefault();
                    popup.dialog("close");

                });
                $('.ui-widget-overlay').click(function() {
                    //Close the dialog
                    popup.dialog("close");

                });
            });

        });
    }
    //end of closure
})(jQuery);

// maps page
(function($) { //create closure
    $.fn.mapones = function(options) {
        this.each(function() {
            var defaults = {};
            var errors = 0,
                msg = '',
                o;
            if (typeof options != 'string') {
                o = $.extend(defaults, options);
            } else {
                o = defaults;
            }
            var cont = $(this);
            ymaps.ready(init); // on MAP READY
            var map, placemark, bal = cont.parent().find('.marker_lib.hidden');
            var html = bal.html(), maproot = $('#ymap');

            function init() {
                map =  new ymaps.Map("fmap", {
                    center: [(cont.data('lat')) ? cont.data('lat') : 55.76, (cont.data('lng')) ? cont.data('lng') : 37.64],
                    zoom: (cont.data('zoom')) ? cont.data('zoom') : 10,
                    type: 'yandex#publicMap'
                });

                map.controls.add('zoomControl', {
                    left: 20,
                    top: maproot.height()/2 + 10
                });
                map.events.add('click', function(e) {
                    if(map.balloon.isOpen()){
                        map.balloon.close();
                    }
                });
                map.behaviors.enable(['scrollZoom']);
                placemark = new ymaps.Placemark(map.getCenter(), {
                    balloonContent: '<span class="marker_lib">'+html+'</span>'
                }, {
                    balloonCloseButton: true,
                    balloonOffset: (cont.width() < 410)? [175, 180]:[207, 180],
                    balloonMaxWidth: (cont.width() < 410)? 350:410,
                    balloonMinWidth: (cont.width() < 410)? 350:410,
                    balloonAutoPan: true,
                    hideIconOnBalloonOpen: false,
                    iconLayout: 'default#image',
                    iconImageHref: '/local/templates/.default/markup/i/pin.png', //i/pin.png
                    iconImageSize: [34, 48],
                    iconImageOffset: [-17, -24]
                });
                map.geoObjects.add(placemark);
                placemark.balloon.open();

            }
        });
    }
    //end of closure
})(jQuery);


(function(cash) { //create closure
    $.fn.addrowForm = function(options){
        this.each(function(){
            var form = $(this), btn = $('.b-addrow', form), irow = $('.phonerow', form);


            var n = 0;
            btn.on('click', function(e){
                e.preventDefault();
                n=n+1;
                var clone_row = irow.clone(true);
                var idinp = clone_row.find('input').attr('id');
                clone_row.find('input').val('');
                clone_row.find('input').attr('id', idinp+'_'+n);
                clone_row.removeClass('phonerow');
                clone_row.insertBefore(btn);
            });

        });

    }
    //end of closure
})(jQuery);


(function($) {
    $.fn.formAddSelection = function() {
        this.each(function() {
            var form = $(this), tbox = $('.input', form), addlink = $('.b-selectionaddtxt', form);
            addlink.click(function(){
                $(this).addClass('hidden');
                tbox.removeClass('hidden')
            });
            tbox.focusout(function(){
                addlink.removeClass('hidden');
                $(this).addClass('hidden');
            });
            form.submit(function(e) {
                /* раскомментировать
                 $.ajax({
                 url: a.attr('href'),
                 complete: function(data){ // ответ от сервера
                 */

                // имитация положительного ответа сервера

                /* раскомментировать
                 }
                 });
                 */


            });
            tbox.keypress(function(e){

                if(e.keyCode==13){
                    /* раскомментировать
                     $.ajax({
                     url: a.attr('href'),
                     complete: function(data){ // ответ от сервера
                     */

                    // имитация положительного ответа сервера

                    /* раскомментировать
                     }
                     });
                     */
                    // alert(tbox.val());
                }
            });

        });
    }
    //end of closure
})(jQuery);




(function($) {
    $.fn.fieldEditable = function() { //редактируемое поле в таблице
        this.each(function() {
            var cont = $(this), text = $('.b-fieldtext', cont), edit = $('.b-fieldeedit', cont), tbox = $('.txt', cont),
                titblock = $('.b-fondtitle', cont), editblock = $('.b-fieldeditform', cont), bt = $('.b-editablsubmit', cont),
                mt = editblock.css('margin-top').replace("px", "");

            tbox.val(text.text());

            function hideeditfield(){
                titblock.removeClass('hidden');
                editblock.addClass('hidden');
                editblock.css({
                    'margin-top': mt,
                    'visibility': 'hidden'
                });
            }
            function showeditfield(){
                titblock.addClass('hidden');
                editblock.removeClass('hidden');
                editblock.css({
                    'margin-top': 0,
                    'visibility': 'visible'
                });

            }

            edit.click(function(e){
                e.preventDefault();
                showeditfield();
            });

            bt.click(function(e){
                e.preventDefault();
                text.text(tbox.val());
                hideeditfield();
            });

            tbox.keypress(function(e){

                if(e.keyCode==13){
                    text.text(tbox.val());
                    hideeditfield();
                }
            });


        });
    }
    //end of closure
})(jQuery);

(function(cash) { //create closure
    $.fn.bindImageLoad = function() {
        this.each(function(){

            var img = $(this), w = img.width(), h = img.height(), par = img.closest('a');

            if(!img.data('title'))
                img.data('title', '');
            if(!img.data('autor'))
                img.data('autor', '')

            par.css('position', 'relative');
            $('<span class="loadimg bbox"><div class="b-bookhover_tit">' + img.data('title') + '</div> <span class="b-autor">' + img.data('autor') + '</span></span>').appendTo(par);


            var loadimg = par.find('.loadimg');
            loadimg.height(h + 20);
            loadimg.width(w + 50);



            /*	$(img).bind('load',function(){
             if (!img.data('loaded')) {
             img.data('w',img.width()).data('h',img.height()).data('loaded',true);
             scitem.data('imgW',img.data('w')).data('imgH',img.data('h'));

             }
             });

             try {
             if (!img.data('loaded')) {
             }
             } catch (e) {
             }



             */

        });

    }
    //end of closure
})(jQuery);



// добавление новой подборки
$(document).on( "submit", "form.b-selectionadd.collection",function(e) {

    var form = $(this);
    var formAction = form.attr('action');
    var openButton = form.closest('.b-shresult_selection, .b-myselection_list').find('.js_openmfavs');
    if(openButton.length == 0){
        var openButton = form.closest('.meta.minus').find('.b-bookadd.fav');
        openButton.data('favs', openButton.data('collection'));
        formAction = formAction +'?id=' + form.closest('.b-result-docitem').attr('id');
    }

    var menu = form.closest('.b-shresult_selection, .b-myselection_list').find('.b-favs');
    if(menu.length == 0){
        var menu = form.closest('.meta.minus').find('.selectionBlock.selectionClone');
    }

    $.ajax({
        type: 	form.attr('method'),
        url: 	formAction,
        data: 	form.serialize(),
        dataType: "json"
    })
        .success(function(json) {
            form.find('.b-selectionaddtxt').removeClass('hidden');
            form.find('input.input').addClass('hidden');

            if(openButton.data('favs') && openButton.data('favs').length > 0){
                // обновляем список
                $.ajax({
                    url: openButton.data('favs'),
                    method: 'GET',
                    success: function(html){
                        menu.find('.b-selectionlist').remove();
                        form.before(html).fadeIn();
                        $('input.checkbox:not(".custom")', menu).replaceCheckBox();
                        form.formAddSelection();
                    }
                });

                if(openButton.data('callback') && openButton.data('callback').length > 0) // выполняем callback функцию если она прописана
                    eval(openButton.data('callback'));
            }
            else
            {
                if(form.data('callback') && form.data('callback').length > 0) // выполняем callback функцию если она прописана
                    eval(form.data('callback'));

            }
        });

    return false;
});

// добавление объекта в подборку
$(document).on( "change", "input.checkbox.InCollection",function(e) {
    var input = $(this);
    var fav = input.closest('.b-shresult_selection, .b-result_selection, .b-myselection_list').find('.js_openmfavs');
    if(fav.length == 0){
        var fav = input.closest('.meta.minus').find('.b-bookadd.fav');
        if(fav.length > 0)
            fav.data('favs', fav.data('collection'));
    }

    $.ajax({
        url: fav.data('favs'),
        method: 'POST',
        data: { idCollection : input.val(), action : input.attr('checked') },
        success: function(html){
            if(fav.data('callback') && fav.data('callback').length > 0) // выполняем callback функцию если она прописана
                eval(fav.data('callback'));

        }
    });
});


(function($) { //create closure
    $.fn.share = function(options) {
        this.each(function() {
            var block = $(this);

            if (!block.hasClass('ready')) {
                var fb = 'http://www.facebook.com/share.php?u=%url%&t=%title%';
                var odn = 'http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1';
                var vk = 'http://vkontakte.ru/share.php?url=%url%&title=%title%&image=%image%';

                // fb
                block.find('.fblink ').on('click', function(e) {
                    e.preventDefault();
                    return ;
                    window.open(encodeURI(fb.replace(/\%url\%/, $(this).attr('href')).replace(/\%title\%/, ''+$(this).attr('title')+'')), 'displayWindow', 'width=700,height=400,left=200,top=100,location=no, directories=no,status=no,toolbar=no,menubar=no');
                });
                // mail
                block.find('.odnlink').on('click', function(e) {
                    e.preventDefault();
                    return ;
                    window.open(encodeURI(odn.replace(/\%url\%/, $(this).attr('href')).replace(/\%title\%/, ''+$(this).attr('title')+'')), 'displayWindow', 'width=700,height=400,left=200,top=100,location=no, directories=no,status=no,toolbar=no,menubar=no');
                });
                // vk
                block.find('.vklink').on('click', function(e) {
                    e.preventDefault();
                    alert($(this).data('image'));
                    return ;
                    window.open(encodeURI(vk.replace(/\%url\%/, $(this).attr('href')).replace(/\%title\%/, $(this).attr('title')).replace(/\%image\%/, $(this).data('image'))), 'displayWindow', 'width=700,height=400,left=200,top=100,location=no, directories=no,status=no,toolbar=no,menubar=no');
                });

                block.addClass('ready');
            }

        });
    }
    //end of closure
})(jQuery);


(function(cash) { //create closure
    $.fn.rolledText = function(){
        this.each(function(){
            var cont = $(this), text =  cont.find('.b-rolled_in'), h = cont.data('height'), next = cont.find('.slidetext');

            if(text.length){

                blok_height = text.css('height').replace('px','');

                if( blok_height > h ) {
                    text.css('height',h);


                }
                next.click(function(e){
                    e.preventDefault();
                    text.css('height',blok_height);
                    text.addClass('b-full');

                    // $(this).text('Скрыть');
                    next.hide();
                });


            }
        });
    }
    //end of closure
})(jQuery);



/*(function(cash) { //create closure
 $.fn.closePage = function(){
 this.each(function(){
 var cont = $(this);
 cont.click(function(e){
 e.preventDefault();
 console.log(window.name);
 winref = window.open('', 'thatname', '', true);
 winref.close();
 // window.open('','_self').close();

 });



 });
 }
 //end of closure
 })(jQuery);
 */


(function() { //create closure
    $.fn.add_books_to_digitizing = function(){
        this.each(function(){
            var A = $(this), T;
            A.on('click', function(e){
                e.preventDefault();
                var popup = $('.digitizing_popup');
                if (popup.length>0) {
                    popup.remove();
                }
                popup =  $('<div class="digitizing_popup"></div>').appendTo('BODY');
                var loadtext = (A.data('loadtxt'))? A.data('loadtxt') : '';
                var loading = $('<span class="digitizing_popup_loading">'+ loadtext +'</span>').appendTo(popup);

                loading.hide();

                popup.dialog({
                    modal: true,
                    draggable: false,
                    resizable: false,
                    //maxWidth: 480,
                    create: function(e,ui){

                        if (A.data('width')) popup.dialog("option", "width", A.data('width'));
                        var title = $('<h2>'+A.text()+'</h2>').appendTo(popup);
                        popup.dialog("option", "position", { my: "center top", at: "center top", of: $('.innersection') });

                        var bottomclose = $('<a class="button_mode bottompopupclose" href="#">Сохранить</a>').appendTo(popup);
                        bottomclose.on('click', function(e){
                            e.preventDefault();
                            popup.dialog('close');
                        });
                        var toppopupclose = $('<a href="#" class="button_mode toppopupclose">Сохранить</a>').appendTo(popup);
                        toppopupclose.on('click', function(e){
                            e.preventDefault();
                            //alert('dddd')
                            popup.dialog('close');
                        });

                    },
                    open: function(){
                        $('.ui-widget-overlay').addClass('digitoverlay').on('click', function(e){
                            popup.dialog('close');
                        });


                        loading.show();
                        var popframe = $('<iframe src="'+A.attr('href')+'" id="ifr_popup" height="200" scrolling="no" frameborder="0" ></iframe>').appendTo(popup);
                        popframe.width('100%');

                        var domframe = popframe.get(0);
                        domframe.onload = function(e){
                            var ifr = e.target;
                            var ifrwnd = ifr.contentWindow;
                            var ifrdoc = (ifr.contentWindow || ifr.contentDocument);
                            if (ifrdoc.document) ifrdoc = ifrdoc.document;
                            ifrdoc = $(ifrdoc);
                            var BODY = ifrwnd.$('body');
                            if(BODY.outerHeight(true) > 350){
                                popframe.height(BODY.outerHeight(true));
                            }
                            BODY.on('heightchangestart', function(){

                                BODY.css({'height':BODY.height(), 'overflow':'hidden'});
                            });
                            BODY.on('heightchangeend', function(){

                                BODY.css({'height':'auto', 'overflow':''});
                                if(BODY.outerHeight(true) > 350){
                                    popframe.height(BODY.outerHeight(true));
                                }
                            });


                            loading.hide();
                        };

                    },
                    close: function(e,ui){
                        popup.dialog('destroy');
                        popup.remove();
                        location.reload();
                    }
                });

            });
        });
    }//end of closure
})(jQuery);

(function() { //create closure
    $.fn.add_books_to_fond = function(){
        this.each(function(){
            var A = $(this), T;
            A.on('click', function(e){
                e.preventDefault();
                var popup = $('.fond_popup');
                if (popup.length>0) {
                    popup.remove();
                }
                popup =  $('<div class="fond_popup"></div>').appendTo('BODY');
                var loading = $('<span class="digitizing_popup_loading">'+A.data('loadtxt')+'</span>').appendTo(popup);

                loading.hide();

                popup.dialog({
                    modal: true,
                    draggable: false,
                    resizable: false,
                    minHeight: 810,
                    dialogClass: 'ajaxpopup',
                    //maxWidth: 480,
                    create: function(e,ui){

                        if (A.data('width')) popup.dialog("option", "width", A.data('width'));
                        var title = $('<h2>'+A.text()+'</h2>').appendTo(popup);
                        popup.dialog("option", "position", { my: "center top", at: "center top", of: $(window) });
                        var toppopupclose = $('<a class="closepopup" href="#">Закрыть окно</a>').appendTo(popup);
                        toppopupclose.on('click', function(e){
                            e.preventDefault();
                            //alert('dddd')
                            popup.dialog('close');
                        });


                    },
                    open: function(){
                        $('.ui-widget-overlay').addClass('dark').on('click', function(e){
                            popup.dialog('close');
                        });
                        loading.show();
                        var popframe = $('<iframe src="'+A.attr('href')+'" height="750" scrolling="no" frameborder="0"></iframe>').appendTo(popup);
                        popframe.width('100%');

                        var domframe = popframe.get(0);
                        domframe.onload = function(e){
                            var ifr = e.target;
                            var ifrwnd = ifr.contentWindow;
                            var ifrdoc = (ifr.contentWindow || ifr.contentDocument);
                            if (ifrdoc.document) ifrdoc = ifrdoc.document;
                            ifrdoc = $(ifrdoc);
                            var BODY = ifrwnd.$('body');
                            //popframe.height(BODY.outerHeight(true));
                            BODY.on('heightchangestart', function(){

                                BODY.css({'height':BODY.height(), 'overflow':'hidden'});
                            });
                            BODY.on('heightchangeend', function(){

                                BODY.css({'height':'auto', 'overflow':''});
                                //popframe.height(BODY.outerHeight(true));
                            });
                            popframe.contents().find('.btrefuse').on('click', function(e){
                                popup.dialog('close');
                            });
                            loading.hide();


                        };
                    },
                    close: function(e,ui){
                        popup.dialog('destroy');
                        popup.remove();
                    }
                });

            });
        });
    }//end of closure
})(jQuery);

(function() { //create closure
    $.fn.oldiePopup = function() { // попап регистрации

        this.each(function() {
            var popup = $(this).clone( true );
            popup.removeClass('hidden');
            popup.dialog({
                closeOnEscape: true,
                modal: true,
                draggable: false,
                resizable: false,
                width: 480,
                dialogClass: 'ajaxpopup',
                position: "center",
                open: function(){
                    $('.ui-widget-overlay').addClass('black');
                },
                close: function() {
                    popup.dialog("close").remove();
                }
            });

            $('.ui-widget-overlay').click(function() {
                //Close the dialog
                popup.dialog("close");

            });

        });
    }
    //end of closure
})(jQuery);

(function() { //create closure
    $.fn.openHideBlock = function() { // показывать скрытые блоки

        this.each(function() {
            var lnk =$(this);
            if(!lnk.hasClass('sort')){
                lnk.on('click',function(e) {
                    e.preventDefault();
                    $('<a href="#" class="close"></a>').prependTo($('.b-sidenav'));
                    $('.b-sidenav').fadeIn();
                    $('.b-sidenav .close').on('click', function(e) {
                        e.preventDefault();
                        $(this).parent().fadeOut();
                        $(this).remove();
                    });
                });
            }else {
                var lock = true;
                lnk.on('click',function(e) {
                    e.preventDefault();
                    if(lock){
                        $('.sort_wrap').css({'display':'block'});
                        if($('.sort_wrap div').length == 0){
                            $('.sort_wrap').find('a').wrapAll('<div></div>');
                        }
                        lock=false;
                    }else{
                        $('.sort_wrap').css({'display':'none'});
                        lock=true;
                    }


                });
            }

        });
    }
    //end of closure
})(jQuery);


(function($) { //create closure
    $.fn.doItOnChange = function(options){
        this.each(function(){
            var inp = $(this)
            $(this).change(function(){

                var way = $(this).val();
                if(typeof(ajaxUpdate) == "function")
                    ajaxUpdate(way);
                else
                    window.location = way;

            });
        });
    }
//end of closure
})(jQuery);

$(document).ready(function(){
    $('#regform input').on('focus', function(){
        var elParent;
        if ($(this).attr('name') == 'UF_PASSPORT_SERIES')
            elParent = $(this).parent().parent();
        else
            elParent = $(this).parent();
        elParent.find('em.error').click();
        elParent.removeClass('error');
    });
    $('#regform a.selopener').bind('click', function(){
        var elParent;
        elParent = $(this).parent().parent();
        elParent.find('em.error').click();
        elParent.removeClass('error');
    });
    $('#setphoto_scan1, #setphoto_scan2').on('click', function(){
        var elParent;
        elParent = $(this).parent().parent();
        elParent.find('em.error').click();
        elParent.removeClass('error');
    });
});

function messageOpen(cont) { // всплывающее сообщение
    /*	if(h !== undefined) {
     document.location=h;
     var time = (new Date()).getTime();
     setTimeout(function(){
     var now = (new Date()).getTime();
     if((now-time)<400) {

     }
     }, 300);*/
    var c = $(cont).clone(true);
    c.removeClass('hidden');
    c.dialog({
        closeOnEscape: true,
        modal: true,
        draggable: false,
        resizable: false,
        width: 400,
        dialogClass: 'ajaxpopup',
        position: "center center",
        open: function(){
            $('.ui-button').addClass('formbutton');
            $('.ui-widget-overlay').addClass('black');
            c.find('.closepopup').click(function() {
                //Close the dialog
                c.dialog( "destroy" ).remove();
                return false;
            });
        },
        buttons: {
            Ok: function() {
                $( this ).dialog( "destroy" ).remove();
            }
        },
        close: function() {
            popup.dialog("close").remove();
        }
    });

    $('.ui-widget-overlay').click(function() {
        //Close the dialog
        c.dialog( "destroy" ).remove();
    });


};

// device detector
function testDevice() {
    var _doc_element, _find, _user_agent;
    window.device={};
    _doc_element=window.document.documentElement;
    _user_agent=window.navigator.userAgent.toLowerCase();
    device.ios=function(){return device.iphone() || device.ipod() || device.ipad();};
    device.iphone=function(){return _find('iphone');};
    device.ipod=function(){return _find('ipod');};
    device.ipad=function(){return _find('ipad');};
    device.android=function(){return _find('android');};
    device.androidPhone=function(){return device.android() && _find('mobile');};
    device.androidTablet=function(){return device.android() && !_find('mobile');};
    device.blackberry=function(){return _find('blackberry') || _find('bb10') || _find('rim');};
    device.blackberryPhone=function(){return device.blackberry() && !_find('tablet');};
    device.blackberryTablet=function(){return device.blackberry() && _find('tablet');};
    device.windows=function(){return _find('windows');};
    device.windowsPhone=function(){return device.windows() && _find('phone');};
    device.windowsTablet=function(){return device.windows() && _find('touch');};
    device.fxos=function(){return (_find('(mobile;') || _find('(tablet;')) && _find('; rv:');};
    device.fxosPhone=function(){return device.fxos() && _find('mobile');};
    device.fxosTablet=function(){return device.fxos() && _find('tablet');};
    device.meego=function(){return _find('meego');};
    device.mobile=function(){return device.androidPhone() || device.iphone() || device.ipod() || device.windowsPhone() || device.blackberryPhone() || device.fxosPhone() || device.meego();};
    device.tablet=function(){return device.ipad() || device.androidTablet() || device.blackberryTablet() || device.windowsTablet() || device.fxosTablet();};
    device.portrait=function(){return Math.abs(window.orientation)!==90;};
    device.landscape=function(){return Math.abs(window.orientation)===90;};
    _find = function(needle){return _user_agent.indexOf(needle) !== -1;};
}
testDevice();
