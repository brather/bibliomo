<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
} ?>
<?
/**
 * @var CMain $APPLICATION
 * @var array $arResult
 * @var array $arParams
 */
CJSCore::RegisterExt("bootstrap_ext", Array(
    "js"    =>  "/bitrix/js/elr.useraccess/bootstrap.js",
    "css"   =>  "/bitrix/css/elr.useraccess/bootstrap.min.css",
    "rel"   =>  array('jquery2'),
    "skip_core" => "N"
));
CJSCore::Init(array("bootstrap_ext","date","core"));

?>
<? $APPLICATION->SetTitle("Статистика"); ?>
<link rel="stylesheet" href="/local/components/elr/info.monitoring/templates/.default/style.css">
<div class="iblock b-lib_fulldescr">
    <div class='b-add_digital js_digital'>
        <form
            action="<?= $APPLICATION->GetCurPage() ?><? if ($arParams['FORMAT']
                == 'view'
            ): ?>?form=view<? endif; ?>#tab-statistics" method="GET" name="form1">
            <div class="form-group">
                <div style="display: inline-block; width: 150px">
                    <div class="input-group" style="margin-bottom: 15px">
                        <input style="margin-right: 0" type="text" class="form-control" id="PERIOD_START" name="from" placeholder="с" value="<?=($_REQUEST['from']) ? $_REQUEST['from'] : date('d.m.Y', (time() - (86400 * 7)));?>" />
                        <span class="input-group-addon" onclick="BX.calendar({node: $('#PERIOD_START')[0], field: $('#PERIOD_START')[0], bTime: false });"><span class="glyphicon glyphicon-calendar"></span></span>
                    </div>
                    <div style="display: inline-block; width: 150px">
                        <div class="input-group">
                            <input style="margin-right: 0"  type="text" class="form-control" id="PERIOD_END" name="to" placeholder="по" value="<?= ($_REQUEST['to']) ? $_REQUEST['to'] : date('d.m.Y');?>" />
                            <span class="input-group-addon" onclick="BX.calendar({node: $('#PERIOD_END')[0], field: $('#PERIOD_END')[0], bTime: false });"><span class="glyphicon glyphicon-calendar"></span></span>
                        </div>
                    </div>
                </div>
            </div>
            <input type='hidden' value='<?= $arParams['FORMAT'] ?>' name='form'>
            <div class="divchekblok">
                <span>Количество изданий</span>
                <input class="checkbox" id="check"
                       type="checkbox"
                       name="count_items"
                       value="Y"
                       checked="checked"><br>
                <span>Количество изданий c электронной копией</span> <input
                    class="checkbox" id="check" type="checkbox"
                    name="count_digitalitems" value="Y" checked="checked"><br>
                <span>Количество прочитанных книг библиотеки</span> <input
                    class="checkbox" id="check" type="checkbox"
                    name="count_readbook" value="Y" checked="checked"><br>
            </div>
			<input type="submit" class="bt_typelt bbox" value="<?=GetMessage("LIBRARY_STAT_REFRESH");?>"
                   style='float:none;'>
			<div class="b-digitizing_actions stat-buttons-block">
				<a class="button_mode" target="_blank" href="<?=$APPLICATION->GetCurPageParam('export=Y', array('export'));?>">Статистика</a>
			</div>
        </form>
        <br>

        <?php if (isset($arResult['grphicData'])
            && !empty($arResult['grphicData'])
        ) { ?>
            <div style="width:100%; overflow:hidden;">
                <ul class="uleditstat">
                    <?php foreach ($arResult['grphicData'] as $dataItem) { ?>
						<li>
							<h3><?php echo $dataItem['name'] ?></h3>
							<div id="<?php echo $dataItem['id'] ?>" width="100%"></div>
						</li>
                    <?php } ?>
                </ul>
            </div>
            <script>
                <?php foreach ($arResult['grphicData'] as $key => $dataItem)
                {
                    if(!isset($dataItem['stat']))
                    {
                        continue;
                    }
                    $stat = $dataItem['stat'];
                 ?>
				$(function () {
                    $('#<?php echo $dataItem['id'] ?>').highcharts({
                        title: false,
                        xAxis: {
                            categories: <?php echo json_encode($stat['dates']); ?>
                        },
                        yAxis: {
                            title: {
                                text: '<?php echo $dataItem['name'] ?>'
                            },
                            plotLines: [{
                                value: 100,
                                width: 3,
                                color: '#808080'
                            }],
                            min: 0,
							labels: {
								formatter: function () {
									if (this.value >= 1000000000){
										return this.value/1000000000+' млрд';
									}else if (this.value >= 1000000){
										return this.value/1000000+' млн';
									}else if(this.value >= 1000){
										return this.value/1000+' тыс';
									}
									return this.value
								}
							}
                        },
                        legend: true,
                        series: [{
                            name: '<?php echo $dataItem['name'] ?>',
                            data: <?php echo json_encode($stat['values']); ?>
                        }]
                    });
                });
                <?php } unset($stat); ?>
            </script>
        <? } ?>
        <?php echo htmlspecialcharsBack($arResult['STR_NAV']) ?>
    </div>
    <br><br>
</div>


