<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?$APPLICATION->SetTitle("Статистика"); ?>
<?
 //$APPLICATION->SetAdditionalCSS('/style.css'); 
//$APPLICATION->AddHeadScript('/bitrix/js/jquery.js');?>

<div class="iblock b-lib_fulldescr">
<div class='b-breadcrumb' style='margin-bottom:15px;'>Количество посещений страницы библиотеки в ЕИСУБ - <?=intval($arResult['ITEM']['SHOW_COUNTER'])?></div>

<!--<div class="b-breadcrumb bot">
	<span>Разделы</span>
    <ul class="b-breadcrumblist">
        <li><a href="?form=read&from=<? //=$arParams['DATE_FROM']?>&to=<? //=$arParams['DATE_TO']?>&PAGEN_1=1">Количество прочитанных книг библиотеки</a></li>
        <li><a href="?form=view&from=<? //=$arParams['DATE_FROM']?>&to=<? //=$arParams['DATE_TO']?>&PAGEN_1=1">Количество просмотренных книг библиотеки</a></li>
                    
    </ul>
</div>-->
<div class='b-add_digital js_digital'>
<form action="<?=$APPLICATION->GetCurPage()?>?<? if ($arParams['FORMAT'] == 'view'): ?>form=view<? endif; ?>" method="GET" name="form1">
с <? echo CalendarDate("from", $arParams['DATE_FROM'], "form1", "10", "class=\"b-text\"")?>
по <? echo CalendarDate("to", $arParams['DATE_TO'], "form1", "10", "class=\"b-text\"")?>

<input type='hidden' value='<?=$arParams['FORMAT']?>' name='form'>
<span>Количество изданий</span><div> <input class="checkbox" id="check" type="checkbox" name="count_items" value="Y" checked="checked"></div>
<span>Количество изданий c электронной копией</span><input class="checkbox" id="check" type="checkbox" name="count_digitalitems" value="Y" checked="checked">
<span>Количество прочитанных книг библиотеки</span><input class="checkbox" id="check" type="checkbox" name="count_readbook" value="Y" checked="checked">
<input type="submit" class="b-search_bth bbox" value="Обновить" style='float:none;'>
</form>
<?
 $heightblock=400;
 $widthblock=600;
 $minstep=15;
 $padding=100;
 $AxisYitem=10; // количество делений по оси Y
 $AxisXitem=10; // количество делений по оси X

 $countdate = count($arResult['STAT']);
 $divitemwidth=$widthblock;
 if($widthblock/$countdate < $minstep) $divitemwidth = $minstep; else $divitemwidth = $widthblock/$countdate;

  //echo $countdate;
// var array = [ elem0, elem1, elem2, ... ]

 $masdate="";
 $maxedit=$arResult['STAT'][0]['maxedit'];
 $maxeditdig=$arResult['STAT'][0]['maxeditdig'];
 $div="";
 $divscale="";
 

 
 
 //Определение коэффициентов увиличения и уменьшения
  if($maxedit>0 && $maxedit>$heightblock)
    $koefeditmore =  koefmore($heightblock, $maxedit);

  if($maxeditdig>0 && $maxeditdig>$heightblock)
   $koefeditdigmore =  koefmore($heightblock, $maxeditdig);
   
   if($maxedit>0 && $maxedit<$heightblock)
    $koefeditless =  koefless($heightblock, $maxedit);
	
	if($maxeditdig>0 && $maxeditdig<$heightblock)
    $koefeditdigless =  koefless($heightblock, $maxeditdig);



if($maxeditdig/$AxisYitem>=$AxisYitem)
{
  $step = $maxeditdig/$AxisYitem;
  for($i=1; $i<=$AxisYitem; $i++)
  {   
	$count=$step*$i;
    $divscaleY="<div style='height:".$step."px'>".$count."</div>".$divscaleY;
  }
   //$height=$koefeditdigless*($maxeditdig-(($AxisYitem-1)*$step))-11;// 11 количество border - ов
   //$divscaleY="<div style='height:".$height."px; border-top: 1px #ccc solid;'>".$maxeditdig."</div>".$divscaleY;
}	

if($maxeditdig/$AxisYitem<$AxisYitem)
{
  $step = floor($maxeditdig/$AxisYitem);
  for($i=$AxisYitem-1; $i>0; $i--)
  {   
    $height=$koefeditdigless*$step;
	$count=$step*$i;
    $divscaleY.="<div style='height:".$height."px;'>".$count."</div>";
  }
   $height=$koefeditdigless*($maxeditdig-(($AxisYitem-1)*$step))-11;// 11 количество border - ов
   $divscaleY="<div style='height:".$height."px; border-top: 1px #ccc solid;'>".$maxeditdig."</div>".$divscaleY;
}


if($maxeditdig<=$AxisYitem)
{
  
    $height=$koefeditdigless*$maxeditdig-1;
    $divscaleY="<div style='height:".$height."px; border-top: 1px #ccc solid;'>".$maxeditdig."</div>";

}
	
	//isset($koefeditdigmore) ? $height=floor(/$koefeditmore) : "";
   // isset($koefeditdigless) ? $height=floor($val['counteditdig']*$koefeditdigless) : "";
	
 $leftscaleheight=floor($maxeditdig/10);
 $i=1;

 while($i<10)
 {
  $i++;  
  $divscale.="<div>".$leftscaleheight*$i."</div>";
 }
 
 //echo $koefeditdigless;
//print_r($arResult['ITEMS']);
 $bg=1;
 
 foreach($arResult['STAT'] as $val)
 {
   isset($koefeditdigmore) ? $height=floor($val['counteditdig']/$koefeditmore) : "";
   isset($koefeditdigless) ? $height=floor($val['counteditdig']*$koefeditdigless) : "";
   if($bg==1){$bg=2; $bgcol="#316275;";} else{$bg=1; $bgcol="#248495;";}
   $div.= "<div id='".$val['counteditdig']."'  style='height:".intval($height)."px; background:".$bgcol."'></div>\n";// прибавляем дополнительный отступ по текст сверху
   
   if($countdate-1 > 0)// формируем массив для JS
   {
     $masdate.=$val['datestat'].", ";
     $masedit.=$val['counteditdig'].", ";
   }
   else
   {
     $masdate.=$val['datestat'];
     $masedit.=$val['counteditdig'];
   }
   
 }
 $masdate="var masdate = [".$masdate."]";
 $masedit="var masedit = [".$masedit."]";
 

 
?>
<STYLE type="text/css">
#stat_container
{
   width: <? echo $widthblock;?>px;
   height: <? echo $heightblock?>px;
}

#stat_container div
{
  width: <? echo $divitemwidth;?>px;
}
#stat_container div span
{
	color:#fff;
	width: 15px;
	height:10px;
	display:block;
	background:#000;
	border-top: 1px #999 solid;
	font-size:9px;
	text-align:center;
	vertical-align:bottom;
	font-size:8px;
	line-height:5px;
}

</STYLE>
<script>
<? 
echo $masdate.";\n";
echo $masedit.";\n";
echo "bg1=".$bg1.";\n";
echo "bg2=".$bg2.";\n";
echo "heightblock=".$heightblock.";\n";
echo "widthblock=".$widthblock.";\n";
?>
</script>

<table width="630" height="400" class="stat_table" border="0">
  <tr>
    <td width="30" height="10"></td>
    <td class="stattabtopline"></td>
  </tr>
  <tr>
    <td valign="top" class="tdaxisY"><? echo $divscaleY; ?></td>
    <td valign="bottom"><div id="stat_container">
		 <? echo $div;?>
        </div>
</td>
  </tr>
  <tr>
    <td height="30">&nbsp;</td>
    <td>4</td>
  </tr>
</table>


<?=htmlspecialcharsBack($arResult['STR_NAV'])?>
</div>
<br><br>
</div>

