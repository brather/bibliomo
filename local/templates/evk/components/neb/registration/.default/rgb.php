<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<section class="innersection innerwrapper clearfix">
	<div class="b-registration rel">
		<h2 class="mode"><?=GetMessage("REG_RGB_TITLE");?></h2>
		<form action="" class="b-form b-form_common b-regform" method="post" id="regform">
			<?
			if($arResult['CHECK_STATUS'] === false)
			{
			?>
			<div class="nookresult">
				<p class="error"><?if(!empty($arResult['ERROR_MESSAGE'])){echo $arResult['ERROR_MESSAGE'];}else echo GetMessage("REG_RGB_ERROR_RGB_DATA_NOT_FOUND");?></p>
				<p><a href="<?=$arResult['FOLDER'].$arResult['URL_TEMPLATES']['full']?>" class="black"><?=GetMessage("REG_RGB_ERROR_COMPLETE_FULL_REG");?></a></p>
			</div>
			<?
			}
			?>
			<?=bitrix_sessid_post()?>
			<hr/>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<label for="settings01"><?=GetMessage("REG_RGB_LAST_NAME");?></label>
					<div class="field validate">
						<input type="text" data-validate="fio" data-required="required" value="<?=htmlspecialcharsbx($_REQUEST['LAST_NAME'])?>" id="settings01" data-maxlength="30" data-minlength="2" name="LAST_NAME" class="input">
						<em class="error required"><?=GetMessage("REG_RGB_ERROR_FILL_IT");?></em>		
						<em class="error validate"><?=GetMessage("REG_RGB_ERROR_INVALID_FORMAT");?></em>	
						<em class="error maxlength"><?=GetMessage("REG_RGB_ERROR_MORE_THAN_30");?></em>							
					</div>
				</div>
			</div>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<label for="settings02"><?=GetMessage("REG_RGB_NAME");?></label>
					<div class="field validate">
						<input type="text" data-required="required" data-validate="fio" value="<?=htmlspecialcharsbx($_REQUEST['NAME'])?>" id="settings02" data-maxlength="30" data-minlength="2" name="NAME" class="input" >	
						<em class="error required"><?=GetMessage("REG_RGB_ERROR_FILL_IT");?></em>		
						<em class="error validate"><?=GetMessage("REG_RGB_ERROR_INVALID_FORMAT");?></em>	
						<em class="error maxlength"><?=GetMessage("REG_RGB_ERROR_MORE_THAN_30");?></em>												
					</div>
				</div>
			</div>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<label for="settings22"><?=GetMessage("REG_RGB_SECOND_NAME");?></label>
					<div class="field validate">
						<input type="text" data-validate="fio" value="<?=htmlspecialcharsbx($_REQUEST['SECOND_NAME'])?>" id="settings22" data-maxlength="30" data-minlength="2" name="SECOND_NAME" class="input" >
						<em class="error required"><?=GetMessage("REG_RGB_ERROR_FILL_IT");?></em>		
						<em class="error validate"><?=GetMessage("REG_RGB_ERROR_INVALID_FORMAT");?></em>	
						<em class="error maxlength"><?=GetMessage("REG_RGB_ERROR_MORE_THAN_30");?></em>													
					</div>
				</div>
			</div>

			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<label for="settings21" class="disable"><?=GetMessage("REG_RGB_CARD_NUMBER");?></label>
					<div class="field validate">
						<input type="text" data-validate="number" value="<?=htmlspecialcharsbx($_REQUEST['RGB_CARD_NUMBER'])?>" id="settings21" data-required="required" data-maxlength="30" data-minlength="2" name="RGB_CARD_NUMBER" class="input">	
						<em class="error required"><?=GetMessage("REG_RGB_ERROR_FILL_IT");?></em>
						<em class="error validate"><?=GetMessage("REG_RGB_ERROR_INVALID_FORMAT");?></em>	
					</div>
				</div>
			</div>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<label for="settings05"><?=GetMessage("REG_RGB_PASSWORD");?></label>
					<div class="field validate">
						<input type="password" data-validate="password" data-required="required" value="" id="settings05" data-maxlength="30" name="PASSWORD" class="pass_status input">
						<em class="error required"><?=GetMessage("REG_RGB_ERROR_FILL_IT");?></em>
						<em class="error validate"><?=GetMessage("REG_RGB_ERROR_AT_LEAST_6");?></em>
						<em class="error maxlength"><?=GetMessage("REG_RGB_ERROR_PASSWORD_MORE_THAN_30");?></em>
					</div>
				</div>
			</div>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<label for="settings55"><?=GetMessage("REG_RGB_PASSWORD_CONFIRM");?></label>
					<div class="field validate">
						<input data-identity="#settings05" type="password" data-required="required" value="" id="settings55" data-maxlength="30" name="CONFIRM_PASSWORD" class="input" data-validate="password">										
						<em class="error identity"><?=GetMessage("REG_RGB_ERROR_PASSWORD_IDENTITY");?></em>
						<em class="error required"><?=GetMessage("REG_RGB_ERROR_FILL_IT");?></em>
						<em class="error validate"><?=GetMessage("REG_RGB_ERROR_AT_LEAST_6");?></em>
						<em class="error maxlength"><?=GetMessage("REG_RGB_ERROR_PASSWORD_MORE_THAN_30");?></em>
					</div>
				</div>
			</div>
			<hr>
            
			<div class="checkwrapper">
				<label><?=GetMessage("REG_RGB_AGREE_FOR_POPUP");?> </label> <a href="/local/components/neb/registration/templates/.default/ajax_agreement.php" target="_blank" class="popup_opener ajax_opener closein" data-width="955" data-height="auto"><?=GetMessage("REG_RGB_AGREE_FOR_POPUP_TERMS");?></a>
			</div>
			<div class="fieldrow nowrap fieldrowaction">
				<div class="fieldcell ">
					<div class="field clearfix divdisable">
						<button class="formbutton btdisable" value="1" type="submit" disabled="disabled"><?=GetMessage("REG_RGB_SIGN_UP");?></button>
                        <div class="b-hint"><?=GetMessage("REG_RGB_READ_TERMS");?></div>
					</div>
				</div>
			</div>			
		</form>
	</div><!-- /.b-registration-->
</section>