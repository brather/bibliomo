<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<section class="innersection innerwrapper clearfix">
	<div class="b-registration rel">
		<h2 class="mode"><?=GetMessage("REG_RGB_OK_MESSAGE");?></h2>
		<div class="succsestxt rgbok">
			<div class="iblock">
				<p><span><?=GetMessage("REG_RGB_OK_LOGIN");?</span></p>
				<p class="nechb"><?=$arResult['RGB_CARD_NUMBER']?></p>
			</div>
			<div class="iblock">
				<p><span><?=$arResult['REG_RGB_OK_CARD_NUMBER']?></span></p>
				<p class="nechb"><?=$arResult['EICHB']?></p>
			</div>
			<p>
				<a href="/profile/" class="formbutton"><?=$arResult['REG_RGB_OK_LINK_LK']?></a>
				<a href="/search/" class="black"><?=$arResult['REG_RGB_OK_LINK_SEARCH']?></a>
				<a href="/upload/documents/user_agreement_neb.docx" target="_blank" class="black l-print"><?=$arResult['REG_RGB_OK_PRINT_AGREE']?></a>
			</p>
		</div>
	</div><!-- /.b-registration-->
</section>