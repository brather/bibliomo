<?php require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

$APPLICATION->IncludeComponent(
	"neb:library.list",
	"main",
	Array(
		"IBLOCK_TYPE" => "library",
		"IBLOCK_ID" => 4,
		"CACHE_TIME" => 3600,
		"ROWS_PER_PAGE" => "20",
		"JSON" => true,
	),
	false
);