<?
	if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
		die();
	use \Bitrix\Main\Localization\Loc as Loc;
	Loc::loadMessages(__FILE__);
?>
<?
$arParams['PREVIEW_TEXT'] = trim($arParams['PREVIEW_TEXT']);
if(empty($arParams['PREVIEW_TEXT']) && empty($arResult['ITEMS'])):?>
	<p>Книги отсутствуют</p>
<?else:?>
	<?if($arParams['PREVIEW_TEXT']):?>
		<?=htmlspecialchars_decode($arParams['PREVIEW_TEXT'])?>
	<?endif;?>
	<?if(!empty($arResult['ITEMS'])):?>
		<?php if(!$arResult["ajax_new_books"]):?>
			<h2>Новые поступления</h2>
		<?php endif;?>
		<ul class="b-ebooklist">
			<?foreach($arResult['ITEMS'] as $arBook):?>
				<li class="iblock">
					<a href="<?=$arBook["DETAIL_PAGE_URL"]?>" class="b-ebook">
						<?php if(!empty($arBook["IMAGE_URL_RESIZE"])):?>
							<img src="<?=sprintf('%s%s&p=%d', $arBook["IMAGE_URL_RESIZE"], '&w=124', $arBook["COVER_NUMBER_PAGE"])?>" alt="<?=$arBook["NAME"]?>">
						<?php else:?>
							<img src="/local/images/book_empty.jpg" alt="<?=$arBook["NAME"]?>">
						<?php endif;?>
						<div class="b-ebooktitle"><?=$arBook["NAME"]?></div>
						<div class="b-ebook_autor">
							<span class="b-ebook_year"><?=$arBook["PROPERTY_PUBLISH_YEAR_VALUE"]?></span>
							<span class="b-ebook_name"><?=$arBook["PROPERTY_AUTHOR_VALUE"]?></span></div>
					</a>
				</li>
			<?endforeach;?>
		</ul>
		<?if(!empty($arResult["NavNextPageLink"])):?>
			<div class="b-wrap_more">
				<a href="<?=$arResult["NavNextPageLink"]?>" class="ajax_add b-morebuttton">Показать еще</a>
			</div>
		<?endif;?>
	<?endif;?>
<?endif;?>
