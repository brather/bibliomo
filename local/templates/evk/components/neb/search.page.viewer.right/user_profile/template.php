<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use \Bitrix\Main\Localization\Loc as Loc;
Loc::loadMessages(__FILE__);
?>
	<script type="text/javascript">
		$(function(){
			$('.b-right_side .b-menu_side a.active').each(function(){
				$(this).closest(".b-menu_sidebox").prev('.b-menu_sidetit').click();
			});
		});
	</script>
<div class="b-right_side">
	<div class="b-menu_side">
		<?
		$SMART = $arResult["SMART"]['COLLECTIONS'];
		unset($arResult["SMART"]['COLLECTIONS']);
		?>
		<div class="b-menu_sidetit <?=$SMART["FIELD_ID"]?>"><?=$SMART["NAME"]?></div>
		<div class="b-menu_sidebox <?=$SMART["FIELD_ID"]?>">
			<ul class="b-menu_sidelist"
				data-tag-side-widget
				data-remove-url="/local/tools/collections/remove.php"
				data-edit-url="/local/tools/collections/edit.php">
				<? if ($_REQUEST['reloadRightCollection'] == 'Y' and $_SERVER['HTTP_X_REQUESTED_WITH']
					== 'XMLHttpRequest'
				) {
					$APPLICATION->RestartBuffer();
				} ?>
				<? foreach ($SMART["ITEMS"] as $item): ?>
                    <li data-tag-side-item="<?= $item['VALUE'] ?>">
						<span class="num items-count" id="<?=((is_numeric($item["VALUE"])) ? $SMART["FIELD_ID"].$item["VALUE"] : '')?>"><?=$item["COUNT"]?></span>
						<span class="name"><a href="<?=$item["LINK"]?>"<?=(($item["CHECKED"]) ? ' class="active" style="color: blue;"':'')?> data-tag-title><?=$item['UF_NAME']?></a></span>
						<button class="item-delete ico_delete_button" data-tag-delete title="<?php echo GetMessage("USER_RIGHT_REMOVE");?>"></button>
						<button class="item-save ico_save_button" data-tag-rename title="Сохранить"></button>
						<button class="item-edit ico_edit_button" data-tag-edit title="<?php echo GetMessage("USER_RIGHT_EDIT");?>"></button>
					</li>
				<?php endforeach;?>
				<? if ($_REQUEST['reloadRightCollection'] == 'Y' and $_SERVER['HTTP_X_REQUESTED_WITH']
					== 'XMLHttpRequest'
				) {
					exit();
				} ?>
			</ul>
			<div data-add-tag-widget-placeholder>
				<a href="#" style="text-decoration: underline;">создать подборку</a>
			</div>
		</div>
		<?foreach($arResult["SMART"] as $SMART):?>
			<?if(count($SMART["ITEMS"]) <= 0) continue;?>
			<div class="b-menu_sidetit <?=$SMART["FIELD_ID"]?>"><?=$SMART["NAME"]?></div>
			<div class="b-menu_sidebox <?=$SMART["FIELD_ID"]?>">
                <ul class="b-menu_sidelist">
                    <? foreach ($SMART["ITEMS"] as $itemName => $item): ?>
						<li>
							<span class="num" id="<?=((is_numeric($item["VALUE"])) ? $SMART["FIELD_ID"].$item["VALUE"] : '')?>"><?=$item["COUNT"]?></span>
							<span class="name"><a href="<?=$item["LINK"]?>"<?=(($item["CHECKED"]) ? ' class="active" style="color: blue;"':'')?>><?=$itemName?></a></span>
						</li>
					<?php endforeach;?>
				</ul>
			</div>
		<?php endforeach;?>
	</div>
</div><!-- b-right-side -->