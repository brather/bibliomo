<?
    $MESS['LIB_SEARCH_PAGE_VIEWER_RIGHT_AUTHORS'] = 'Authors';
    $MESS['LIB_SEARCH_PAGE_VIEWER_RIGHT_DATE'] = 'Date';
    $MESS['LIB_SEARCH_PAGE_VIEWER_RIGHT_PUBLISH'] = 'Publisher';
    $MESS['LIB_SEARCH_PAGE_VIEWER_RIGHT_LIBS'] = 'Libraries';
    $MESS['LIB_SEARCH_PAGE_VIEWER_RIGHT_PLACE'] = 'Place of publication';

    $MESS['LIB_SEARCH_PAGE_VIEWER_RIGHT_MORE'] = 'more';
?>