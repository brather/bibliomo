<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?php if($arResult{"AJAX_NEWS_DETAIL"})
	$APPLICATION->RestartBuffer();
	?>
	<div class="b-news-detail" style="min-height: 250px">
		<h2 class="mode"><?=$arResult['ITEM']['NAME']?></h2>
			<?
			if(!empty($arResult['ITEM']['DETAIL_PICTURE']))
			{
				?>
				<img class="b-news-detail-picture" src="<?=$arResult['ITEM']['DETAIL_PICTURE']['src']?>" alt="" align="left" />
				<?
			}
			?>
			<div class="descr">
				<span class="b-news-time"><?=$arResult['ITEM']['FORMAT_ACTIVE_FROM']?></span>
				<h3><?=$arResult['ITEM']['NAME']?></h3>
			</div>

		<?php if(!empty($arResult['ITEM']['~DETAIL_TEXT'])):?>
			<span class="detail-text"><?=$arResult['ITEM']['~DETAIL_TEXT']?></span>
		<?php endif;?>
		<?
			if(!empty($arResult['ITEM']['GALLERY']))
			{
				?>
				<div class="b-slidernews">
					<?

						foreach($arResult['ITEM']['GALLERY'] as $arItem){
							if(empty($arItem['FILE']))
								continue;
						?>
						<div class="slide">
							<img src="<?=$arItem['FILE']?>" alt="" <?=($arParams['ID'] == 'preview') ? 'style="max-width:600px;max-height:340px;"':''?>>
							<div class="text"><?=$arItem['DESC']?></div>
						</div>
						<?
						}
					?>
				</div>
				<?
				}
			?>

			<?
			if(!empty($arResult['ITEM']['PROPERTY_YOUTUBE_CODE']))
			{
				?>
				<b>YouTube:</b>
				<p><iframe width="100%" height="394" src="//www.youtube.com/embed/<?=$arResult['ITEM']['PROPERTY_YOUTUBE_CODE']?>" frameborder="0" allowfullscreen></iframe></p>
				<?
			}
			?>
			<?
			if(!empty($arResult['ITEM']['PROPERTY_RUTUBE_CODE']))
			{
				?>
				<b>RuTube:</b>
				<p><iframe width="100%" height="394" src="//rutube.ru/play/embed/<?=$arResult['ITEM']['PROPERTY_RUTUBE_CODE']?>" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowfullscreen></iframe></p>
				<?
			}
			?>
			<?
			if(!empty($arResult['ITEM']['PROPERTY_MAIL_RU_CODE']))
			{
				?>
				<b>MailRu:</b>
				<p><iframe width="100%" height="394" src='https://videoapi.my.mail.ru/videos/embed<?=$arResult['ITEM']['PROPERTY_MAIL_RU_CODE']?>' frameborder='0' webkitallowfullscreen mozallowfullscreen allowfullscreen></p>
				<?
			}
			?>
		<?
			if(!empty($arResult['ITEM']['PROPERTY_FILE_VALUE']))
			{
				if(empty($arResult['ITEM']['PROPERTY_FILE_DESCRIPTION']))
					$arResult['ITEM']['PROPERTY_FILE_DESCRIPTION'] =  pathinfo($arResult['ITEM']['PROPERTY_FILE_VALUE'], PATHINFO_FILENAME);
				?>
				<p><a href="<?=$arResult['ITEM']['PROPERTY_FILE_VALUE']?>" class="ic<?=$arResult['ITEM']['PROPERTY_FILE_VALUE_EXT']?>"><span><?=$arResult['ITEM']['PROPERTY_FILE_DESCRIPTION']?></span></a></p>
				<?
			}
		?>
	</div><!-- /.b-news-detail -->
	<?
		global $arrFilterLibraryNews;
		$arrFilterLibraryNews = array('PROPERTY_LIBRARY' => $arResult['ITEM']['PROPERTY_LIBRARY_VALUE'], '!PROPERTY_LIBRARY' => false, '!ID' => $arResult['ITEM']['ID']);
		$APPLICATION->IncludeComponent("bitrix:news.list", "", Array(
			"DETAIL_EXIST" => true,
			"IBLOCK_TYPE" => "news",	// Тип информационного блока (используется только для проверки)
			"IBLOCK_ID" => "3",	// Код информационного блока
			"NEWS_COUNT" => "10",	// Количество новостей на странице
			"SORT_BY1" => "ACTIVE_FROM",	// Поле для первой сортировки новостей
			"SORT_ORDER1" => "DESC",	// Направление для первой сортировки новостей
			"SORT_BY2" => "SORT",	// Поле для второй сортировки новостей
			"SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
			"FILTER_NAME" => "arrFilterLibraryNews",	// Фильтр
			"FIELD_CODE" => array(	// Поля
				0 => "NAME",
				1 => "PREVIEW_TEXT",
			),
			"PROPERTY_CODE" => array(	// Свойства
			),
			"CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
			"DETAIL_URL" => $arParams['NEWS_DETAIL_URL'],	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
			"AJAX_MODE" => "N",	// Включить режим AJAX
			"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
			"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
			"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
			"CACHE_TYPE" => "A",	// Тип кеширования
			"CACHE_TIME" => 3600,	// Время кеширования (сек.)
			"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
			"CACHE_GROUPS" => "N",	// Учитывать права доступа
			"PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
			"ACTIVE_DATE_FORMAT" => "j F",	// Формат показа даты
			"SET_TITLE" => "N",	// Устанавливать заголовок страницы
			"SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
			"SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
			"SET_META_DESCRIPTION" => "N",	// Устанавливать описание страницы
			"SET_STATUS_404" => "N",	// Устанавливать статус 404, если не найдены элемент или раздел
			"INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
			"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
			"HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
			"PARENT_SECTION" => "",	// ID раздела
			"PARENT_SECTION_CODE" => "",	// Код раздела
			"INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
			"DISPLAY_DATE" => "Y",	// Выводить дату элемента
			"DISPLAY_NAME" => "Y",	// Выводить название элемента
			"DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
			"DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
			"PAGER_TEMPLATE" => "",	// Шаблон постраничной навигации
			"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
			"DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
			"PAGER_TITLE" => "Новости",	// Название категорий
			"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
			"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
			"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
			"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
			"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
			),
			$component
		);?>
<?php if($arResult{"AJAX_NEWS_DETAIL"})
	exit;
?>