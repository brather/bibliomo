<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
	use \Bitrix\Main\Localization\Loc as Loc;
	Loc::loadMessages(__FILE__);
	if(empty($arResult['POPULAR_BOOKS']))
		return false;
?>
<?
	if(!empty($arResult['POPULAR_BOOKS']))
	{
		if(!$arParams["AJAX"]):?>
			<h2>Последние добавленные издания</h2>
		<?endif;?>
		<ul class="b-ebooklist">
			<?foreach($arResult['POPULAR_BOOKS'] as $arBook):?>
			<li class="iblock">
				<a href="<?=$arBook["DETAIL_PAGE_URL"]?>" class="b-ebook">
					<?if(isset($arBook["IMAGE_URL_RESIZE"])):?>
						<img src="<?=sprintf('%s%s&p=%d', $arBook["IMAGE_URL_RESIZE"], '&w=124', $arBook["COVER_NUMBER_PAGE"])?>" alt="<?=$arBook["NAME"]?>"/>
					<?else:?>
						<img src="/local/images/book_empty.jpg" alt="<?=$arBook["NAME"]?>"/>
					<?endif;?>
					<div class="b-ebooktitle"><?=$arBook["NAME"]?></div>
					<div class="b-ebook_autor"><span class="b-ebook_year"><?=$arBook["PROPERTY_PUBLISH_YEAR_VALUE"]?></span><span class="b-ebook_name"><?=$arBook["PROPERTY_AUTHOR_VALUE"]?></span></div>
				</a>
			</li>
			<?endforeach;?>
		</ul>
		<div class="b-wrap_more">
			<a href="<?=$arResult["NEXT_PAGE_LINK"]?>" class="ajax_add b-morebuttton">Показать еще</a>
		</div>
		<?
	}
?>