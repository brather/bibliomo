<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use \Bitrix\Main\Localization\Loc as Loc;
Loc::loadMessages(__FILE__);
?>
<?
	if(empty($arResult['SECTIONS'])) {
	?>
		<p>Коллекции отсутствуют</p>
	<?
		return false;
	}
?>
<!--div class="b-filter b-filternums">
	<div class="b-filter_wrapper">
		<a <?=SortingExalead("CNT")?>><?=Loc::getMessage('COLLECTION_LIST_SORT_BY_COUNT'); ?></a>
		<a <?=SortingExalead("NAME")?>><?=Loc::getMessage('COLLECTION_LIST_SORT_BY_TITLE'); ?></a>
		<a <?=SortingExalead("DATE_CREATED")?>><?=Loc::getMessage('COLLECTION_LIST_SORT_BY_DATE'); ?></a>
			<span class="b-filter_act">
				<span class="b-filter_show"><?=Loc::getMessage('COLLECTION_LIST_SHOW'); ?></span>
				<a href="<?=$APPLICATION->GetCurPageParam("pagen=10", array("pagen", "dop_filter", "PAGEN_1"));?>" class="b-filter_num b-filter_num_paging<?=$arParams['ITEM_COUNT'] == 10 ? ' current' : ''?>">10</a>
				<a href="<?=$APPLICATION->GetCurPageParam("pagen=25", array("pagen", "dop_filter", "PAGEN_1"));?>" class="b-filter_num b-filter_num_paging<?=$arParams['ITEM_COUNT'] == 25 ? ' current' : ''?>">25</a>
				<a href="<?=$APPLICATION->GetCurPageParam("pagen=30", array("pagen", "dop_filter", "PAGEN_1"));?>" class="b-filter_num b-filter_num_paging<?=$arParams['ITEM_COUNT'] == 30 ? ' current' : ''?>">30</a>
			</span>
	</div>
</div--><!-- /.b-filter -->

<form class="b-collection_search clearfix" action="#tab-collection">
	<select class="js_select mode" name="library">
		<option value=""><?=Loc::getMessage("COLLECTION_LIST_ALL_LIBRARIES")?></option>
		<?foreach($arResult["LIBRARY"] as $library_id=>$library):?>
			<option value="<?=$library_id?>"><?=$library["NAME"]?></option>
		<?endforeach;?>
	</select>
	<input type="text" name="find_collections" placeholder="<?=Loc::getMessage("COLLECTION_LIST_FIND_PLACEHOLDER");?>" class="b-collection_tb iblock" kl_virtual_keyboard_secure_input="on" value="<?=$arResult["find_collections"]?>">
	<input type="submit" value="<?=Loc::getMessage("COLLECTION_LIST_FIND_BUTTON");?>" class="b-collection_bt bt_typelt iblock">
</form>
<div class="b-collection_list">
	<?
	 $countOnPage = 30;// выводим 10 элементов
	 if($arParams['COUNTONPAGE'])
	 $countOnPage=$arParams['COUNTONPAGE'];
	 $page=1;			
	 if(isset($_GET['PAGEN_1'])) 
	 $page = intval($_GET['PAGEN_1']);
	 $startarr=$page*$countOnPage-$countOnPage;

	 //$stoparr=$page*$countOnPage-1;
	 $arSubRes=$arResult['SECTIONS'];
	 $arResult['SECTIONS'] = array_slice($arResult['SECTIONS'], $startarr,  $countOnPage);
?>		
		<nav class="b-collection_list">
			<?foreach($arResult["SECTIONS"] as $arSection):?>
				<a class="iblock js_flex_bgimage" href="<?=$arSection['SECTION_PAGE_URL']?>" style="background-image: url(&quot;<?=$arSection["PICTURE"]?>&quot;); background-position: 50% 50%; background-size: 100% auto; background-repeat: no-repeat;">
					<h3><?=$arSection["NAME"]?></h3>
					<div class="b-inselection"><?=$arSection['ELEMENT_CNT']?> <?=GetEnding($arSection['ELEMENT_CNT'], Loc::getMessage('COLLECTION_LIST_BOOK_5'), Loc::getMessage('COLLECTION_LIST_BOOK_1'), Loc::getMessage('COLLECTION_LIST_BOOK_2'))?> <?=Loc::getMessage('COLLECTION_LIST_IN_COL');?></div>
					<?if($arSection['UF_ADDED_FAVORITES'] > 0):?>
						<div class="b-lib_counter">
							<div class="b-lib_counter_lb">
								Добавили <?=number_format($arSection['UF_ADDED_FAVORITES'], 0, '', ' ');?> человек
							</div>
						</div>
					<?endif;?>
				</a>
			<?endforeach;?>
		</nav>
<? foreach($arResult['SECTIONS'] as $collection): 
/*?>
		<div class="b-collection-item rel">
			<h2><?=$collection['NAME']?></h2>
			<div class="b-colllist_title"><?=Loc::getMessage('COLLECTION_LIST_LIB_PROFILE_STAT'); ?></div>
			<div class="b-collstat">
				<a href="/profile/collection/<?=$collection['ID']?>/" class="button_mode"><?=$collection['ELEMENT_CNT']?> <?=GetEnding($arSection['ELEMENT_CNT'], Loc::getMessage('COLLECTION_LIST_LIB_PROFILE_BOOK_5'), Loc::getMessage('COLLECTION_LIST_LIB_PROFILE_BOOK_1'), Loc::getMessage('COLLECTION_LIST_LIB_PROFILE_BOOK_2'))?> <?=Loc::getMessage('COLLECTION_LIST_LIB_PROFILE_IN_COL'); ?></a>
				<?if($collection['UF_ADDED_FAVORITES'] > 0):?>
					<div class="b-lib_counter">
						<div class="b-lib_counter_lb"><?=Loc::getMessage('COLLECTION_LIST_LIB_PROFILE_FAV'); ?></div>
						<div class="icofav">х <?=number_format($collection['UF_ADDED_FAVORITES'], 0, '', ' ');?></div>
					</div>
				<?endif;?>
				<?if($collection['UF_VIEWS'] > 0):?>
					<div class="b-lib_counter">
						<div class="b-lib_counter_lb"><?=Loc::getMessage('COLLECTION_LIST_LIB_PROFILE_VIEW'); ?></div>
						<div class="icoviews">х <?=number_format($collection['UF_VIEWS'], 0, '', ' ');?></div>
					</div>
				<?endif;?>
				<a href="<?=$collection['SECTION_PAGE_URL']?>" class="b-btlibpage"><?=Loc::getMessage('COLLECTION_LIST_LIB_PROFILE_COL_PAGE'); ?></a>
			</div>
			<div class="b-collectslider ">
				<div class="js_fourslider">
					<? foreach($arResult['BOOKS_IN_SECTIONS'][$collection['ID']] as $bookID):?>
						<?if($book = $arResult['BOOKS'][$bookID]):?>
							<div>
								<div class="b-collslider_img">
									<a class="popup_opener ajax_opener coverlay" data-width="955" href="<?=$book['DETAIL_PAGE_URL']?>">
										<?if(!empty($book['IMAGE_URL_PREVIEW'])):?>
											<img class="loadingimg" src="<?=$book['IMAGE_URL_PREVIEW']?>" alt="<?=$book['NAME']?>">
										<?else:?>
											<img class="loadingimg" src="/local/images/book_empty.jpg" alt="<?=$book['NAME']?>">
										<?endif;?>
									</a>
								</div>
								<div class="b-lib_counter">
									<div class="icoviews">х <?=number_format($book['STAT']['VIEW_CNT'], 0, '', ' ')?></div>
								</div>
							</div>
						<?endif;?>
					<?endforeach;?>
				</div>
			</div><!-- /.b-collectslider -->
			<div class="b-colllist_title"><?=Loc::getMessage('COLLECTION_LIST_LIB_PROFILE_COL_MANAGE'); ?></div>
			<div class="b-collaction">
				<a onclick="return confirm('Коллекция <?=$collection['NAME']?> будет удалена. Вы уверены?')" href="<?=$APPLICATION->GetCurPage() . '?'.bitrix_sessid_get().'&COLLECTION_ID=' . $collection['ID'] . '&action=remove'?>" class="right button_red"><?=Loc::getMessage('COLLECTION_LIST_LIB_PROFILE_COL_DELETE'); ?></a>
				<a class="button_blue btadd closein add_books_to_fond" data-loadtxt="загрузка..." href="<?=$this->__folder . '/addBookForm.php?'.bitrix_sessid_get().'&COLLECTION_ID=' . $collection['ID']?>"  data-width="635" ><?=Loc::getMessage('COLLECTION_LIST_LIB_PROFILE_COL_ADD_BOOK'); ?></a>
				<a href="/profile/collection/edit/<?=$collection['ID']?>/" class=""><?=Loc::getMessage('COLLECTION_LIST_LIB_PROFILE_COL_EDIT'); ?></a>
			</div>
		</div><!-- /.b-collection-item -->
	<? 
	*/endforeach; ?>
</div><!-- /.b-collection_list -->
<?
$elements=$arSubRes;
$elementsPage = array_slice($elements, $page * $countOnPage, $countOnPage);
//echo renderElementsPage($elementsPage);
$navResult = new CDBResult();
$navResult->NavPageCount = ceil(count($elements) / $countOnPage);
$navResult->NavPageNomer = $page;
$navResult->NavNum = 1;
$navResult->NavPageSize = $countOnPage;
$navResult->NavRecordCount = count($elements);

$APPLICATION->IncludeComponent('bitrix:system.pagenavigation', '', array(
    'NAV_RESULT' => $navResult,
));
?>


