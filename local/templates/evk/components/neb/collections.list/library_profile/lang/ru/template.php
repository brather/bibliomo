<?php
$MESS['COLLECTION_LIST_SORT_BY_COUNT'] = 'По количеству книг';
$MESS['COLLECTION_LIST_SORT_BY_TITLE'] = 'По названию';
$MESS['COLLECTION_LIST_SORT_BY_DATE'] = 'По дате';
$MESS['COLLECTION_LIST_ADD_TO_MY_COL'] = 'Добавить в мои подборки';
$MESS['COLLECTION_LIST_REMOVE_FROM_MY_COL'] = 'Удалить из моих подборок';

$MESS['COLLECTION_LIST_SORT'] = 'Сортировать';
$MESS['COLLECTION_LIST_TITLE'] = 'Коллекции';
$MESS['COLLECTION_LIST_ALL_LIBRARIES'] = 'Все библиотеки';
$MESS['COLLECTION_LIST_FIND_PLACEHOLDER'] = 'Поиск коллекции по названию';

$MESS['COLLECTION_LIST_FIND_BUTTON'] = 'Искать';
$MESS['COLLECTION_LIST_SHOW'] = 'Показать';

$MESS['COLLECTION_LIST_LOAD_ELSE'] = 'Загрузить еще';
$MESS['COLLECTION_LIST_BOOK_5'] = 'книг';
$MESS['COLLECTION_LIST_BOOK_1'] = 'книга';
$MESS['COLLECTION_LIST_BOOK_2'] = 'книги';
$MESS['COLLECTION_LIST_IN_COL'] = 'в подборке';

$MESS['COLLECTION_LIST_AUTHOR'] = 'Автор';
$MESS['COLLECTION_LIST_BY_LIB'] = 'по библиотеке';
$MESS['COLLECTION_LIST_MORE'] = 'следующие';
$MESS['COLLECTION_LIST_FIND_PLACEHOLDER'] = 'Найти коллекцию';
$MESS['COLLECTION_LIST_FIND_BUTTON'] = 'Найти';
$MESS['COLLECTION_LIST_NOT_FOUND'] = 'Коллекции не найдены';
$MESS['COLLECTION_LIST_FIND_BUTTON_CLEAR'] = 'Очистить';
