<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use \Bitrix\Main\Localization\Loc as Loc;
Loc::loadMessages(__FILE__);
?>
<?if(!$arResult['AJAX']):?>
<div class="wrapper">
	<section class="b-inner clearfix">
		<h1><?=Loc::getMessage("COLLECTION_LIST_TITLE")?></h1>
		<form class="b-collection_search clearfix">
			<select class="js_select mode" name="library">
				<option value=""><?=Loc::getMessage("COLLECTION_LIST_ALL_LIBRARIES")?></option>
				<?foreach($arResult["LIBRARY"] as $library_id=>$library):?>
					<option value="<?=$library_id?>"><?=$library["NAME"]?></option>
				<?endforeach;?>
			</select>
			<input type="text" name="find_collections" placeholder="<?=Loc::getMessage("COLLECTION_LIST_FIND_PLACEHOLDER");?>" class="b-collection_tb iblock" kl_virtual_keyboard_secure_input="on" value="<?=$arResult["find_collections"]?>">
			<input type="submit" value="<?=Loc::getMessage("COLLECTION_LIST_FIND_BUTTON");?>" class="b-collection_bt bt_typelt iblock">
		</form>
<?endif;?>
		<nav class="b-collection_list">
			<?foreach($arResult["SECTIONS"] as $arSection):?>
				<a class="iblock js_flex_bgimage" href="<?=$arSection['SECTION_PAGE_URL']?>" style="background-image: url(&quot;<?=$arSection["PICTURE"]?>&quot;); background-position: 50% 50%; background-size: 100% auto; background-repeat: no-repeat;">
					<h3><?=$arSection["NAME"]?></h3>
					<div class="b-inselection"><?=$arSection['ELEMENT_CNT']?> <?=GetEnding($arSection['ELEMENT_CNT'], Loc::getMessage('COLLECTION_LIST_BOOK_5'), Loc::getMessage('COLLECTION_LIST_BOOK_1'), Loc::getMessage('COLLECTION_LIST_BOOK_2'))?> <?=Loc::getMessage('COLLECTION_LIST_IN_COL');?></div>
				</a>
			<?endforeach;?>
		</nav>
		<?if(!empty($arResult['NEXT_COLLECTIONS'])):?>
			<div class="b-wrap_more">
				<a class="ajax_add b-morebuttton" href="<?=$arResult['NEXT_COLLECTIONS']?>"><?=Loc::getMessage("COLLECTION_LIST_LOAD_ELSE")?></a>
			</div>
		<?endif;?>
<?if(!$arResult['AJAX']):?>
	</section>
</div>
<?endif;?>