<?	require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
	//var_dump($arResult);
?>
<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="ru"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="ru"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="ru"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="ru"> <!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<script src="<?=MARKUP?>js/libs/modernizr.min.js"></script>
		<meta name="viewport" content="width=device-width"/>

		<title><?$APPLICATION->ShowTitle()?></title>
		<?$APPLICATION->ShowHead();?>
		<link rel="icon" href="<?=MARKUP?>favicon.ico" type="image/x-icon" />
		<?$APPLICATION->SetAdditionalCSS(MARKUP.'css/style.css');?>
		<?$APPLICATION->AddHeadScript(MARKUP.'js/libs/jquery.min.js');?>
		<?$APPLICATION->AddHeadScript('/local/templates/.default/js/script.js');?>
		<?$APPLICATION->AddHeadScript(MARKUP.'js/slick.min.js');?>
		<?$APPLICATION->AddHeadScript(MARKUP.'js/plugins.js');?>
		<?$APPLICATION->AddHeadScript(MARKUP.'js/jquery.knob.js');?>
		<?$APPLICATION->AddHeadScript(MARKUP.'js/script.js');?>
		<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
		<?if($APPLICATION->GetCurPage() == '/'){?>
			<script>
				$(function() {
					$('html').addClass('mainhtml'); // это для морды
				}); // DOM loaded
			</script>
			<?}?>
		<script type="text/javascript" src="//vk.com/js/api/openapi.js?115"></script>
		<script type="text/javascript">VK.init({apiId: 4525156, onlyWidgets: true});</script>
		<script>
			$(document).ready(function(){
				$(function() {
					var $items = $('.addBookTabs li');
					$items.click(function() {
						$items.removeClass('act');
						$(this).addClass('act');
						var index = $items.index($(this));
						$('.addBookContent>div').hide().eq(index).show();
						return false;
					}).eq(0).click();
				});
			});
		</script>
		<style>
			.addBookTabs{
				height: 34px;
				padding-bottom: 24px;
				margin-bottom: 10px;
				border-bottom: 1px solid #c3cccf;
			}
			.addBookTabs ul{
				list-style: none;
			}
			.addBookTabs ul li{
				float: left;
				width: 30%;
				margin: 0 5% 0 0;
			}			
			.addBookTabs ul li a{
				display: block;
				color: #ffffff;
				text-align: center;
				font: 14px/14px Arial, Helvetica, sans-serif;
				font-weight: normal;
				padding: 10px 0;
				text-decoration: none;
				background-image: linear-gradient(#f87579, #F79FA2);
			}
			.addBookTabs ul li.act a{
				  background-image: linear-gradient(#f5454b, #f87579);
				  font-weight: bold;
			}
			.b_bookfond_popup_content{
				display: none;
			}
			.js_scroll {
				height: 680px;
			}
			.b_bookfond_popup_content .b-addbookform, .b_bookfond_popup_content .b-pdfadd{
				padding-top: 0;
				border-top: 0;
			}
		</style>
	</head>

	<body>
		<base target="_self">
		<div class="addBookTabs">
			<ul>
				<li id="exalead" class="act"><a href="#">Добавить из Exalead</a></li>
				<li id="byHand"><a href="#">Добавить вручную</a></li>
			</ul>
		</div>
		<div class="addBookContent">
			<div class="b_bookfond_popup_content js_scroll">
				<?
					global $navParent;
					$navParent = '_self';
				?>
				<?$APPLICATION->IncludeComponent(
						"exalead:search.form",
						"",
						Array(
							"PAGE" => (''),
							"POPUP_VIEW" => 'Y',
							"ACTION_URL" => $APPLICATION->GetCurPage()
						)
					);?>
				<?
					$nebUser = new nebUser();
					$lib = $nebUser->getLibrary();
					
					CModule::IncludeModule("highloadblock"); 
					use Bitrix\Highloadblock as HL; 
					use Bitrix\Main\Entity; 

					if(!empty($lib['PROPERTY_LIBRARY_LINK_VALUE']))
					{
						$hlblock = HL\HighloadBlockTable::getById(HIBLOCK_LIBRARY)->fetch();
						$entity = HL\HighloadBlockTable::compileEntity($hlblock); 
						$entity_data_class = $entity->getDataClass(); 

						$rsData = $entity_data_class::getById($lib['PROPERTY_LIBRARY_LINK_VALUE']);
						$arData = $rsData->Fetch();
					}

				?>
				<?if(isset($_REQUEST['q'])):?>
					<?$APPLICATION->IncludeComponent(
						"exalead:search.page",
						"popup_library_funds",
						Array(
							'ID_LIBRARY' => $arData['UF_ID']
						),
						false
					);?>
				<?endif;?>
			</div>
			<div class="b_bookfond_popup_content js_scroll">
				<form class="b-form b-form_common b-addbookform" name="iblock_add" action="/profile/funds/manage/" target="_top" method="post" enctype="multipart/form-data">
					<?=bitrix_sessid_post()?>
					<input type="hidden" name="action" value="addBookByHand" />
					<input type="hidden" name="XML_ID" value="" id="XML_ID" />
					<div class="fieldrow nowrap">
						<div class="fieldcell iblock">
							<label for="NAME">Заглавие</label>
							<div class="field validate">
								<input type="text" data-required="required" value="" id="NAME" data-minlength="2" name="Name" class="input" >
								<em class="error required">Поле обязательно для заполнения</em>
							</div>
						</div>
					</div>
					<div class="fieldrow nowrap">
						<div class="fieldcell iblock">
							<label for="AUTHOR">Автор (сведения об интеллектуальной ответственности)</label>
							<div class="field validate">
								<input type="text"  data-validate="fio" value="" id="AUTHOR" data-minlength="2" name="Author" class="input w50p" >
								<em class="error required">Поле обязательно для заполнения</em>
								<em class="error validate">Поле заполнено неверно</em>
							</div>
						</div>
					</div>
					<div class="fieldrow nowrap">
						<div class="fieldcell iblock">
							<label for="settings22">Подзаголовок</label>
							<div class="field validate">
								<input type="text" value="" id="settings22" data-minlength="2" name="SubName" class="input" >
								<em class="error required">Поле обязательно для заполнения</em>
							</div>
						</div>
					</div>
					<div class="fieldrow nowrap">
						<div class="fieldcell iblock">
							<label for="partNumber">Номер части</label>
							<div class="field validate">
								<input type="text" value="" id="partNumber" data-minlength="0" name="PartNumber" class="input" >
								<em class="error required">Поле обязательно для заполнения</em>
							</div>
						</div>
					</div>
					<div class="fieldrow nowrap">
						<div class="fieldcell iblock mt0 w20pc">
							<label for="YEAR">Год издания</label>
							<div class="field validate">
								<input type="text"  data-validate="number" value="" id="YEAR" data-minlength="2" name="PublishYear" class="input b-yeartb" >
								<em class="error required">Поле обязательно для заполнения</em>
								<em class="error validate">Поле заполнено неверно</em>
							</div>
						</div>
					</div>
					<div class="fieldrow nowrap">
						<div class="fieldcell iblock">
							<label for="settings20">Издательство или издающая организация (точное название)</label>
							<div class="field validate">
								<input type="text" value="" id="settings20" data-minlength="2" name="Publisher" class="input" >
								<em class="error required">Поле обязательно для заполнения</em>
								<em class="error validate">Поле заполнено неверно</em>
							</div>
						</div>
					</div>
					<div class="fieldrow nowrap">
						<div class="fieldcell iblock">
							<label for="settings19">Место издания</label>
							<div class="field validate">
								<input type="text" value="" id="settings19" data-minlength="2" name="PublishPlace" class="input w50p" >
								<em class="error required">Поле обязательно для заполнения</em>
								<em class="error validate">Поле заполнено неверно</em>
							</div>
						</div>
					</div>
					<div class="fieldrow nowrap">
						<div class="fieldcell iblock mt10">
							<label for="settings18">Международные стандартные книжные номера (ISBN)</label>
							<div class="field validate">
								<input type="text" value="" id="settings18" data-minlength="2" name="ISBN" class="input w50p" >
								<em class="error required">Поле обязательно для заполнения</em>
							</div>
						</div>
					</div>
					<div class="fieldrow nowrap">
						<div class="fieldcell iblock w50">
							<label for="settings07">Код ББК</label>
							<div class="field validate">
								<input type="text" value="" id="settings07" data-minlength="0" name="BBK" class="input " >
								<em class="error required">Поле обязательно для заполнения</em>
							</div>
						</div>
					</div>
					<!--div class="checkwrapper">
						<input class="checkbox addrindently" type="checkbox" name="Closed" id="cb_closed"><label for="cb_closed" class="black">Закрытое издание</label>
							<script type="text/javascript">
								$( document ).ready(function() {
									$('#cb_closed').change(function(){
										$(this).attr('value', +$(this)[0].checked);
									});		
								});
							</script>									
					</div-->
					<div class="fieldrow nowrap">
						<div class="fieldcell iblock b-pdfadd">

							<?$APPLICATION->IncludeComponent(
								"notaext:plupload",
								"fond_add_books",
								array(
									"MAX_FILE_SIZE" => "512",
									"FILE_TYPES" => "pdf",
									"DIR" => "tmp_books_pdf",
									"FILES_FIELD_NAME" => "books_pdf",
									"MULTI_SELECTION" => "N",
									"CLEANUP_DIR" => "Y",
									"UPLOAD_AUTO_START" => "Y",
									"RESIZE_IMAGES" => "N",
									"UNIQUE_NAMES" => "Y",
									"ALREADY_UPLOADED_FILE" => $arResult['FORM']['pdfLink']
								),
								false
							);?>

							<!--div class="right">
								<p>Дата добавления </p>
								<div class="b-fieldeditable">
									<div class="b-fondtitle"><span class="b-fieldtext"><?=$arResult['FORM']['CreationDateTime']?></span><a class="b-fieldeedit" href="#"></a></div>
									<div class="b-fieldeditform"><span class="txt_fld iblock"><input type="text" name="CreationDateTime" class="txt txt_size2"></span><a class="b-editablsubmit iblock" href="#"></a></div>
								</div>
							</div-->
						</div>
					</div>
					<div class="fieldrow nowrap fieldrowaction">
						<div class="fieldcell ">
							<div class="field clearfix">
								<a href="<?=$APPLICATION->GetCurPage()?>" class="formbutton gray right btrefuse">Отказаться</a>
								<button class="formbutton left" value="1" type="submit">Разместить произведение</button>
							</div>
						</div>
					</div>
				</form>				
			</div>
		</div>
	</body>
</html>