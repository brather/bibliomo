<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/**
 * @global $arResult
 */
global $APPLICATION;
?>

<?php if(!$arResult['AJAX_FUNDS_MANAGE']):?>
<div class="b-addsearchmode">
	<div class="b-actionmenu two">
		<a class="button_blue bbox btadd" href="/profile/funds/add/" >Добавить книгу</a>
		<a class="button_blue bbox btadd closein add_books_to_fond" data-loadtxt="загрузка..." href="<?=$this->__folder . '/importBooks.php?'.bitrix_sessid_get()?>"  data-width="920" >Импорт из XML-файла</a>
		<a class="button_blue bbox btadd" href="/bitrix/admin/nota_library_book_list.php?IBLOCK_ID=10&type=library" >Редактировать записи</a>
	</div>
    <div class="b-search_field">
		<form method="get">
	        <input type="text" name="qbook" class="b-search_fieldtb b-text" id="asearch" value="<?=htmlspecialcharsbx($_REQUEST['qbook'])?>" autocomplete="off" placeholder="Поиск книги по названию или (и) автору">
	        <!--<select name="qbooktype" id=""class="js_select b_searchopt">
	            <option <?/*=$_REQUEST['qbooktype'] == 'all' ? 'selected="selected"' : ''*/?> value="all">по всем полям</option>
	            <option <?/*=$_REQUEST['qbooktype'] == 'Name' ? 'selected="selected"' : ''*/?> value="Name">по названию</option>
	            <option <?/*=$_REQUEST['qbooktype'] == 'Author' ? 'selected="selected"' : ''*/?> value="Author">по автору</option>
	        </select>-->
	        <input type="submit" class="b-search_bth bt_typelt bbox" value="Найти">
		</form>
    </div><!-- /.b-search_field-->
</div>
<?php endif;?>

<div class="b-fondlist">
	<?foreach($arResult['BOOKS'] as $arBook):?>
	<li class="iblock">
		<a href="<?=$arBook["DETAIL_PAGE_URL"]?>" class="b-ebook">
			<?if(isset($arBook["IMAGE_URL_RESIZE"])):?>
				<img src="<?=sprintf('%s%s&p=%d', $arBook["IMAGE_URL_RESIZE"], '&w=124', $arBook["COVER_NUMBER_PAGE"])?>" alt="<?=$arBook["NAME"]?>"/>
			<?else:?>
				<img src="/local/images/book_empty.jpg" alt="<?=$arBook["NAME"]?>"/>
			<?endif;?>
			<div class="b-ebooktitle"><?=$arBook["NAME"]?></div>
			<div class="b-ebook_autor">
				<?if(!empty($arBook["PROPERTY_PUBLISH_YEAR_VALUE"])):?>
					<span class="b-ebook_year"><?=$arBook["PROPERTY_PUBLISH_YEAR_VALUE"]?></span>
				<?endif;?>
				<span class="b-ebook_name"><?=$arBook["PROPERTY_AUTHOR_VALUE"]?></span>
			</div>
			<div class="b-ebook_date">
				<span>Дата добавления: <?=date('d m Y', strtotime($arBook["DATE_CREATE"]))?></span>
			</div>
		</a>
	</li>
	<?endforeach;?>
</div>
<?php if($arResult["NEXT_PAGE"]):?>
	<div class="b-epublishing_more b-funds_manage_more">
		<script type="text/javascript">
			$(function(){
				$('.b-funds_manage_more a.b-morebuttton').click(function(){
					BX.showWait($('.b-funds_manage_more'));
					$.get($(this).attr('href'), function(data){
						$('.b-funds_manage_more').replaceWith(data);
						BX.closeWait();
					});
					return false;
				});
			});
		</script>
		<a class="b-morebuttton" href="<?=$arResult["NEXT_PAGE"]?>">Показать ещё</a>
	</div>
<?php endif;?>

<?php if(!$arResult['AJAX_FUNDS_MANAGE']):?>
	<script>
	$(".plus_ico").click(function (e) {
		e.preventDefault()
		$(this).hide().after("<div style='margin-left:-30px;'>Отправлено</div>");;
		$(".b-hint").hide();
	})
	</script>
	<style>
		th.year_cell{
			width: 90px;
		}
		.b-formsubmit .b-successlink.errortext {
			color: #F64F54;
		}
	</style>
<?php endif;?>