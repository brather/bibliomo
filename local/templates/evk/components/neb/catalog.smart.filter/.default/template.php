<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/**
 * @var CBitrixCatalogSmartFilter $component
 * @var CBitrixComponentTemplate $this
 */
use \Bitrix\Main\Localization\Loc as Loc;
Loc::loadMessages(__FILE__);
?>
<?if($arParams["EXISTS_COUNT"] > 0):?>
		<div class="b-right_side">
			<div class="b-menu_side">
				<script type="text/javascript">
					$(function(){
						$('.b-right_side .b-menu_side a.active').each(function(){
							$(this).closest(".b-menu_sidebox").prev('.b-menu_sidetit').click();
						});
					});
				</script>
			<form method="post">
				<?
				foreach($arParams["ITEMS"] as $itemName=>$arItem):
					if($arItem["EXISTS_COUNT"] <= 0) continue;
				?>
					<div class="b-menu_sidetit"><?=$arItem["TITLE"];?></div>
					<div class="b-menu_sidebox <?=$arItem["CODE"]?>">
						<ul class="b-menu_sidelist">
							<?
							$k = 0;
							foreach($arItem["VALUES"] as $valueName=>$arValue):
								if($arValue["SHOW"] == "Y")
								{
									?>
									<li>
										<span class="num" id="lia<?=$k?>"><?=$arValue['EXISTS_COUNT']?></span>
										<span class="name"><a href="<?=$arValue["URL"]?>"<?=(($arValue["CHECKED"]) ? ' class="active" style="color: blue;"':'')?>><?=$arValue["VALUE"]?></a></span>
									</li>
									<?
									$k++;
								}
							endforeach;?>
						</ul>
					</div>
				<?endforeach;?>
			</form>
			</div>
		</div><!-- b-right-side -->
<?endif;?>
<?
//\Evk\Books\Books::p($arParams["ITEMS"]);
?>