<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
global $APPLICATION;
?>
<?if(empty($arResult['ITEMS'])):?>
	<p>Книги отсутствуют</p>
<?else:?>

	<div class="b-fondlist">
		<?foreach($arResult['ITEMS'] as $arBook):?>
			<li class="iblock">
				<a href="<?=$arBook["DETAIL_PAGE_URL"]?>" class="b-ebook">
					<?if(!empty($arBook["IMAGE_URL"])):?>
						<img src="<?=$arBook['IMAGE_URL']?>" alt="<?=$arBook["NAME"]?>"/>
					<?else:?>
						<img src="/local/images/book_empty.jpg" alt="<?=$arBook["NAME"]?>"/>
					<?endif;?>
					<div class="b-ebooktitle"><?=$arBook["NAME"]?></div>
					<div class="b-ebook_autor">
						<?if(!empty($arBook["PROPERTY_PUBLISH_YEAR_VALUE"])):?>
							<span class="b-ebook_year"><?=$arBook["PROPERTY_PUBLISH_YEAR_VALUE"]?></span>
						<?endif;?>
						<span class="b-ebook_name"><?=$arBook["PROPERTY_AUTHOR_VALUE"]?></span>
					</div>
					<div class="b-ebook_date">
						<span>Дата добавления: <?=date('d m Y', strtotime($arBook["DATE_CREATE"]))?></span>
					</div>
				</a>
			</li>
		<?endforeach;?>
	</div>
	<div class="b-epublishing_more">
		<script type="text/javascript">
			$(function(){
				$('.b-epublishing_more a.b-ajax-funds').click(function(e){
					e.preventDefault();
					var replacer = $(this).closest('.b-epublishing_more');
					BX.showWait($(this));
					$.get($(this).attr('href'), function(data){
						replacer.replaceWith(data);
						BX.closeWait();
					}, 'html');
					return false;
				});
			});
		</script>
		<?if($arResult["NEXT_PAGE"]):?>
			<a href="<?=$arResult["NEXT_PAGE"]?>" class="b-morebuttton b-ajax-funds">Показать ещё</a>
		<?endif;?>
	</div>
<?endif;?>
