<?
$MESS['LIB_SEARCH_PAGE_TEMPLATE_FOUND_ZERO'] = 'Найдено 0 документов';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_NOTHING_FOUND'] = 'К сожалению, ничего не найдено';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_SUGGEST'] = 'Возможно вы искали';

$MESS['LIB_SEARCH_PAGE_TEMPLATE_FOUND_5'] = 'По вашему запросу «<b>#QUERY#</b>» найдено #QUANTITY# книг';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_FOUND_1'] = 'По вашему запросу «<b>#QUERY#</b>» найдена #QUANTITY# книга';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_FOUND_2'] = 'По вашему запросу «<b>#QUERY#</b>» найдены #QUANTITY# книги';

$MESS['LIB_SEARCH_PAGE_TEMPLATE_FOUND2_5'] = 'Найдено #QUANTITY# книг';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_FOUND2_1'] = 'Найдена #QUANTITY# книга';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_FOUND2_2'] = 'Найдены #QUANTITY# книги';

$MESS['LIB_SEARCH_PAGE_TEMPLATE_FOUND2_5'] = 'Найдено #QUANTITY# книг';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_FOUND2_1'] = 'Найдена #QUANTITY# книга';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_FOUND2_2'] = 'Найдены #QUANTITY# книги';

$MESS['LIB_SEARCH_PAGE_TEMPLATE_SAVE_QUERY'] = 'Сохранить поисковый запрос';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_QUERY_SAVED'] = 'Поисковый запрос сохранен';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_ADD_TITLE'] = 'Добавить название';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_SPECIFY_SEARCH'] = 'уточнить поиск';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_SEARCH_ON_WEBSITE'] = 'Вы искали на портале';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_IN_WEBSITE'] = 'Искать только в отсканированных изданиях';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_IN_LIBRARY'] = 'По каталогу печатных изданий';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_SEARCH_EMPTY'] = 'Введите поисковое выражение';
$MESS['LIB_SEARCH_PAGE_SETTINGS'] = 'Настройка';
$MESS['LIB_SEARCH_PAGE_FOUND_MORE_300000_RESULT'] = 'Найдено более 300 000 результатов';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_FOUND_COPIES'] = 'экземпляров изданий';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_LOGIC_OR'] = 'ИЛИ';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_LOGIC_AND'] = 'И';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_LOGIC_NOT'] = 'НЕ';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_LOGIC_OR_NOT'] = 'ИЛИ-НЕ';
$MESS['LIB_SEARCH_PAGE_TEMPLATE_LOGIC_AND_NOT'] = 'И-НЕ';
?>