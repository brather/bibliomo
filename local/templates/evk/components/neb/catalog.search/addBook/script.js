$(function(){
    $('.b-search-page').delegate('a.b-morebuttton', 'click', function(){
        $.get($(this).attr('href'), function(data){
            var newData = $(data).find('.b-search-result');
            var newSmart = $(data).find('.b-right_side .b-menu_side');
            $('.b-search-result .b-epublishing_more').remove();
            $('.b-right_side .b-menu_side').replaceWith(newSmart);
            $('.b-search-result').append(newData.html());
        });
        return false;
    })
});