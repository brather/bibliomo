<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
use \Bitrix\Main\Localization\Loc as Loc;

Loc::loadMessages(__FILE__);
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponentNebCatalogSection $component */
?>
<?$APPLICATION->IncludeComponent(
	"neb:exalead.search.page",
	"popup_library_funds",
	Array(
		"ITEM_COUNT" => $arParams["PAGE_ELEMENT_COUNT"],
		"IS_FULL_SEARCH" => $arParams["IS_FULL_SEARCH"],
		"S_STRICT" => $arParams["S_STRICT"],
	),
	false
);?>
