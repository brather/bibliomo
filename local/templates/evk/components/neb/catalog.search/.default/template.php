<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
use \Bitrix\Main\Localization\Loc as Loc;

Loc::loadMessages(__FILE__);
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponentNebCatalogSection $component */
?>
<div class="wrapper">
	<section class="b-inner clearfix b-search-page">
		<h1>Список результатов</h1>
		<?
		if ($arParams["SEARCH_IN_NEB"]):
			$APPLICATION->IncludeComponent(
				"neb:exalead.search.page",
				"",
				Array(
					"ITEM_COUNT" => $arParams["PAGE_ELEMENT_COUNT"],
					"IS_FULL_SEARCH" => $arParams["IS_FULL_SEARCH"],
					"S_STRICT" => $arParams["S_STRICT"],
				),
				false
			);
		else:
		?>
			<?
			if (empty($arResult['ITEMS'])) {
				if (!$arParams["IS_EMPTY_QUERY"]):
					?>
					Найдено 0 документов
				<?
				endif;
				?>
				<h2>
					<?php if ($arParams["IS_EMPTY_QUERY"]): ?>
						Введите поисковое выражение
					<?php else: ?>
						К сожалению, ничего не найдено
					<?php endif;?>
				</h2>
			<?
			} else {
				if (!empty($arResult['RESULT_PARAMS'])) {
					?>
					<div class="b-searchresult_portal">
						<?=Loc::getMessage('LIB_SEARCH_PAGE_TEMPLATE_SEARCH_ON_WEBSITE');?>:
						<?
						$i = 0;
						foreach ($arResult['RESULT_PARAMS'] as $param)
						{
							$i++;
							?>
							<?=($i > 1 || $param['logic'] == "NOT") ? Loc::getMessage('LIB_SEARCH_PAGE_TEMPLATE_LOGIC_'.$param['logic']) . ' ' : ''?>
							<span class="b-searchresult_tag"><?=$param['text']?></span>
						<?
						}
						?>
					</div>
				<?
				}
				?>
				<?
				/*
							if ($arParams['IS_SAVE_QUERY'] === true)
							{
								?>
								<div class="b-search_save">
									<form action="<?=$this->__folder.'/save_query.php'?>" class="searchsave_form" method="POST">
										<?=bitrix_sessid_post()?>
										<input type="hidden" name="url" value="<?=$APPLICATION->GetCurPageParam("", array("clear_cache", "debug")); ?>">
										<input type="hidden" name="q" value="<?=htmlspecialcharsbx($arParams['q'])?>">
										<input type="hidden" name="more_options" value="<?=!empty($arResult['RESULT_PARAMS']) ? htmlspecialcharsbx(serialize($arResult['RESULT_PARAMS'])) : ''?>">
										<input type="hidden" name="found" value="<?=$arResult["NAV_RESULT"]->nSelectedCount?>">
										<input type="hidden" name="evk_search_params" value="<?=$arResult["EVK_SEARCH_PARAMS"]?>">
										<input type="hidden" name="evk_search_params_hash" value="<?=$arResult["EVK_SEARCH_PARAMS_HASH"]?>">
													<span class="b-searchresult_save rel">
														<input type="submit" class="b-searchresult_bt" value="<?=Loc::getMessage('LIB_SEARCH_PAGE_TEMPLATE_SAVE_QUERY');?>">
													</span>
									</form>
									<div class="b-searchsave_popup">
										<p><?=Loc::getMessage('LIB_SEARCH_PAGE_TEMPLATE_QUERY_SAVED');?></p>
										<div class="b-search_tb">
											<input placeholder="<?=Loc::getMessage('LIB_SEARCH_PAGE_TEMPLATE_ADD_TITLE');?>" type="text" name="query_name" class="input" id="query_name">
										</div>
									</div>
								</div>
							<?
							}
				*/
				?>
				<?php


				if (!empty($arParams["q"])): ?>
					<span class="b-searchres_lb">
					<?=GetEnding($arResult["NAV_RESULT"]->nSelectedCount, Loc::getMessage("LIB_SEARCH_PAGE_TEMPLATE_FOUND_5", array(
							"#QUERY#" => htmlspecialchars($arParams["q"]),
							"#QUANTITY#" => $arResult["NAV_RESULT"]->nSelectedCount
						)
					), Loc::getMessage("LIB_SEARCH_PAGE_TEMPLATE_FOUND_1", array(
							"#QUERY#" => htmlspecialchars($arParams["q"]),
							"#QUANTITY#" => $arResult["NAV_RESULT"]->nSelectedCount
						)
					), Loc::getMessage("LIB_SEARCH_PAGE_TEMPLATE_FOUND_2", array(
							"#QUERY#" => htmlspecialchars($arParams["q"]),
							"#QUANTITY#" => $arResult["NAV_RESULT"]->nSelectedCount
						)
					)
					)?>.
						</span>
				<?php else:?>
					<span class="b-searchres_lb">
						<?=GetEnding($arResult["NAV_RESULT"]->nSelectedCount, Loc::getMessage("LIB_SEARCH_PAGE_TEMPLATE_FOUND2_5", array(
								"#QUERY#" => htmlspecialchars($arParams["q"]),
								"#QUANTITY#" => $arResult["NAV_RESULT"]->nSelectedCount
							)
						), Loc::getMessage("LIB_SEARCH_PAGE_TEMPLATE_FOUND2_1", array(
								"#QUERY#" => htmlspecialchars($arParams["q"]),
								"#QUANTITY#" => $arResult["NAV_RESULT"]->nSelectedCount
							)
						), Loc::getMessage("LIB_SEARCH_PAGE_TEMPLATE_FOUND2_2", array(
								"#QUERY#" => htmlspecialchars($arParams["q"]),
								"#QUANTITY#" => $arResult["NAV_RESULT"]->nSelectedCount
							)
						)
						)?>.
					</span>
				<?endif;?>
				<?/*/?>
			<div class="b-search-sort">
				<select class="js_select mode" id="result_sort" style="width: 270px;">
					<option <?=SortingExalead("property_publish_year")?>>По дате</option>
					<option <?=SortingExalead("relevant")?>>По редевантности</option>
				</select>
			</div>
			<?/*/
				?>
                <br>
                <br>
                Сортировать:
				<? foreach ($arParams['sorts'] as $sort) {
                    $order = null;
                    if (isset($_REQUEST['by']) && $sort == $_REQUEST['by']) {
                        $order = ($sort === $_REQUEST['by'] ? ('asc' === $_REQUEST['order'] ? 'desc' : 'asc') : 'asc');
                    }
					?>
					<a <?= SortingExalead($sort) ?>>
						<?= Loc::getMessage('LIB_SEARCH_SORT_' . $sort) ?>
					</a><?= (isset($order) ? '&nbsp;' . ('desc' === $order ? '&darr;' : '&uarr;') : '') ?>&nbsp;|&nbsp;
				<? } ?>
				<div class="b-sidetable">
					<?$APPLICATION->IncludeComponent(
						"neb:search.page.viewer.left",
						"",
						Array(
							"PARAMS" => $arParams,
							"RESULT" => $arResult,
						),
						$component
					);
					?>
					<?$APPLICATION->IncludeComponent(
						"neb:catalog.smart.filter",
						"",
						Array(
							"COMPONENT_TEMPLATE" => ".default",
							"IBLOCK_TYPE" => "library",
							"IBLOCK_ID" => "10",
							"SECTION_ID" => "",
							"SECTION_CODE" => "",
							"FILTER_NAME" => $arParams["FILTER_NAME"],
							"HIDE_NOT_AVAILABLE" => "N",
							"TEMPLATE_THEME" => "blue",
							"FILTER_VIEW_MODE" => "vertical",
							"ITEMS" => $arResult["PROPS_ITEMS"],
							"ALL_ITEM_IDS" => $arResult["ALL_ITEM_IDS"],
							"CACHE_TYPE" => "A",
							"CACHE_TIME" => $arParams["CACHE_TIME"],
							"CACHE_GROUPS" => "Y",
							"SAVE_IN_SESSION" => "N",
							"INSTANT_RELOAD" => "N",
							"PRICE_CODE" => array(),
							"XML_EXPORT" => "N",
							"SECTION_TITLE" => "-",
							"SECTION_DESCRIPTION" => "-",
							"POPUP_POSITION" => "left",
							"AJAX" => $arParams["AJAX"],
						),
						$component
					);
					?>
				</div>
			<?
			}
			?>
		<?endif;?>
	</section>
</div>