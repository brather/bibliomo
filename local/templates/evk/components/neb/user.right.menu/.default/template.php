<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<div class="b-right_side">
	<div class="b-menu_side">
		<div class="b-menu_sidetit"><?=GetMessage("USER_RIGHT_TITLE");?></div>
		<div class="b-menu_sidebox">
			<form action="<?=$this->__folder?>/remove.php">
				<div class="js_sortable_del list-collections-menu">
					<ul class="b-menu_sidelist">
						<? if ( !empty($arResult['COLLECTIONS']) ) {
						if($_REQUEST['reloadRightCollection'] == 'Y' and $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')
							$APPLICATION->RestartBuffer();
							foreach($arResult['COLLECTIONS'] as $arItem):?>
								<li>
									<span class="num"><?=(int)$arItem['cnt']?></span>
									<span class="name"><a href="<?=$arItem['URL']?>"<?=(($item["CHECKED"]) ? ' class="active" style="color: blue;"':'')?>><?=$arItem['UF_NAME']?></a> <input class="checkbox" type="checkbox" name="right_menu_collections[]" value="<?=$arItem['ID']?>"></span>
								</li>
							<?php endforeach;?>
							<li>
								<div class="b-libfilter_action clearfix">
									<a href="#" class="b-libfilter_remove"></a>
									<form action="<?=ADD_COLLECTION_URL?>add.php" method="post" class="b-selectionadd collection" data-callback="reloadRightMenuBlock()">
										<input type="submit" value="+" data-collection="ajax_favs.html" class="b-selectionaddsign">
										<span class="b-selectionaddtxt "><?=GetMessage("USER_RIGHT_CREATE_COLLECTION");?></span>
										<input type="text" class="input hidden" name="name">
									</form>
								</div>
								<div class="b-removepopup"><p><?=GetMessage("USER_RIGHT_REMOVE_COLLECTION");?></p><a class="formbutton btremove" href="#"><?=GetMessage("USER_RIGHT_REMOVE");?></a><a class="formbutton gray" href="#"><?=GetMessage("USER_RIGHT_RETAIN");?></a></div>
							</li>
						<?
							if($_REQUEST['reloadRightCollection'] == 'Y' and $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')
								exit();
						}
						elseif ( $_REQUEST['reloadRightCollection'] == 'Y' and $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' )
						{
							$APPLICATION->RestartBuffer();
							exit();
						}?>
					</ul>
				</div>
			</form>
		</div>
	</div>
</div><!-- b-right-side -->
