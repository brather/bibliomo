<?
	if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/**
 * @var CBitrixComponentNebUserFavorites $component
 */
?>
	<?if(!$arParams["ajax_favorites"]):?>
<style>
	div.collection-list {
		position: relative;
		z-index: 100;
	}
</style>
	<script type="text/javascript">
		$(function(){
			var addBookResult = function(data){
				var s, obj1, obj2, num1, num2;
				if((s = /^(\d+)(a|d|r)(\d*)$/.exec(data)))
				{
					obj1 = $('.b-menu_sidebox.collections').find('#collections'+s[1]);
					num1 = parseInt(obj1.text());
					if(isNaN(num1)) num1=0;
					if(s[3].length)
					{
						obj2 = $('.b-menu_sidebox.collections').find('#collections'+s[3]);
						num2 = parseInt(obj2.text());
						if(isNaN(num2)) num2=0;
					}
					switch(s[2])
					{
						case "a":
						{
							obj1.text(num1+1);
							break;
						}
						case "d":
						{
							obj1.text(num1-1);
							break;
						}
						case "r":
						{
							obj1.text(num1+1);
							if(s[3].length)
								obj2.text(num2-1);
							break;
						}
					}
				}
			};
			var collectionList= $('div.collection-list');
			$('a.collection-link').click(function(e) {
				if($.contains(collectionList, e.target)) {
					e.preventDefault();
				}
			});
			collectionList.find('input.collection').change(function () {
				var collectionID = $(this).val();
				var params = {
					'id': $(this).parents('.collection-list').data("book_id"),
					'idCollection': collectionID,
					't': 'books',
					'multiple': true
				};
				if ($(this).is(':checked')) {
					params.action = 'checked';
					$.post('<?=ADD_COLLECTION_URL?>list.php', params, addBookResult);
				} else {

					$.post('<?=ADD_COLLECTION_URL?>list.php', params, addBookResult);
				}
			});

			$('#select_sort_order1').change(function(){
				window.location.href = $(this).find('option:selected').attr('href');
			});
		});
	</script>
		<div class="b-left_side">
			<form class="b-collection_search clearfix" action="<?=POST_FORM_ACTION_URI?>">
				<select id="select_sort_order1" class="js_select mode">
					<option<?=(($arResult["sort_by"] == 'name')?' selected="selected"':'')?> value="name" <?=SortingExalead("name")?>>По алфавиту</option>
					<option<?=(($arResult["sort_by"] == 'property_publish_year')?' selected="selected"':'')?> value="property_publish_year" <?=SortingExalead("property_publish_year")?>>По дате издания</option>
					<option<?=(($arResult["sort_by"] == 'property_author')?' selected="selected"':'')?> value="property_author" <?=SortingExalead("property_author")?>>По автору</option>
				</select>
				<input type="text" class="b-collection_tb iblock" placeholder="Поиск книги по названию или (и) автору" name="find_favorites_books" id="find_favorites_books" value="<?=$arResult["find_favorites_books"]?>">
				<input type="submit" class="b-collection_bt bt_typelt iblock" value="Искать">
			</form>
	<?endif;?>
		<?if(!empty($arResult["ITEMS"])):?>
			<ul class="b-ebooklist">
				<?foreach($arResult["ITEMS"] as $arBook):?>
					<li class="iblock">
						<a href="<?=$arBook["DETAIL_PAGE_URL"]?>" class="b-ebook collection-link">
							<?if(!empty($arBook["IMAGE_URL_RESIZE"])):?>
								<img src="<?=sprintf('%s%s&p=%d', $arBook["IMAGE_URL_RESIZE"], '&w=124', $arBook["COVER_NUMBER_PAGE"])?>" alt="<?=$arBook["NAME"]?>">
							<?php else:?>
								<img src="/local/images/book_empty.jpg" alt="<?=$arBook["NAME"]?>">
							<?endif?>
							<div class="b-ebooktitle"><?=$arBook["NAME"]?></div>
							<div class="b-ebook_autor">
								<?if(!empty($arBook["PROPERTY_PUBLISH_YEAR_VALUE"])):?>
									<span class="b-ebook_year"><?=$arBook["PROPERTY_PUBLISH_YEAR_VALUE"]?></span>
								<?endif?>
								<?if(!empty($arBook["PROPERTY_AUTHOR_VALUE"])):?>
									<span class="b-ebook_name"><?=$arBook["PROPERTY_AUTHOR_VALUE"]?></span>
								<?endif?>
							</div>
							<div class="b-ebook_stat">
								<span class="ico_bookmark"><?=$arBook["CNT_MARKS"]?></span>
								<span class="ico_note"><?=$arBook["CNT_NOTES"]?></span>
								<span class="ico_quote"><?=$arBook["CNT_QUOTES"]?></span>
							</div>
							<div class="b-selection bbox">
								<div class="collection-list" data-book_id="<?= $arBook["ID"] ?>">
									<?
									foreach ($arResult["COLLECTIONS"] as $COLLECTION) {
										$inCollection = false;
										if (in_array($arBook["ID"], $COLLECTION["BOOKS"])) {
											$inCollection = true;
										}
										?>
										<div class="checkwrapper">
											<input class="checkbox js_btnsub collection"
												   type="checkbox"
												   name="collection[]"
												   id="collection<?= $COLLECTION['ID'] ?>"
												   value="<?= $COLLECTION['ID'] ?>"
												<?= (true === $inCollection) ? 'checked' : '' ?>
												>
											<label
												for="collection<?= $COLLECTION['ID'] ?>">
												<?= $COLLECTION['UF_NAME'] ?>
											</label>
										</div>
										<?
									}
									?>
								</div>
							</div>
						</a>
					</li>
				<?endforeach;?>
			</ul>
			<?if($arParams["ajax_link"]):?>
				<div class="b-wrap_more">
					<a href="<?=$arParams["ajax_link"]?>" class="ajax_add b-morebuttton">Показать еще</a>
				</div>
			<?endif;?>
		<?else:?>
			Совпадений не найдено
		<?endif;?>
<?if(!$arParams["ajax_favorites"]):?>
	</div><!-- b-left-side -->
<?endif;?>