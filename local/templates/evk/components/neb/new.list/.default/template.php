<?
	if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
		die();
	use \Bitrix\Main\Localization\Loc as Loc;
	Loc::loadMessages(__FILE__);
	if(empty($arResult['ITEMS']))
		return false;
?>
<div class="b-epublishing">
	<div class="b-publishing_tit">
		<h3><?=Loc::getMessage("NEW_LIST_TEMPLATE_DIGITAL_BOOKS")?> <sup><?=count($arResult['ITEMS'])?></sup></h3>
		<span class="date">На <?=FormatDate("d F Y", time())?></span>
	</div>
	<div class="slider js_simpleslide">
		<?php foreach($arResult['ITEMS'] as $arElement) : ?>
			<div>
				<a href="<?=$arElement["DETAIL_PAGE_URL"]?>" class="b-ebook">
					<img src="<?=sprintf('%s%s&p=%d', $arElement["IMAGE_URL_RESIZE"], '&w=124', $arElement["COVER_NUMBER_PAGE"])?>" alt="<?=htmlspecialcharsbx($arElement["NAME"])?>">
					<div class="b-ebooktitle"><?=$arElement["NAME"]?></div>
					<div class="b-ebook_autor"><span class="b-ebook_year"><?=$arElement["PROPERTY_PUBLISH_YEAR_VALUE"]?></span><span class="b-ebook_name"><?=$arElement["PROPERTY_AUTHOR_VALUE"]?></span></div>
				</a>
			</div>
		<?php endforeach;?>
	</div>
	<div class="b-epublishing_more">
		<a href="/search/?is_full_search=Y" class="b-morebuttton"><?=Loc::getMessage("NEW_LIST_TEMPLATE_SEE_ALL")?></a>
	</div>
</div><!-- /.b-epublishing -->
