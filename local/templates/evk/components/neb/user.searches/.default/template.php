<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?/*?>
<div class="b-filter">
	<div class="b-filter_wrapper">			
		<a ><?=str_repeat('&nbsp;',80)?></a>
		<a <?=SortingExalead("uf_date_add")?>><?=GetMessage("PROFILE_SEARCHES_SORT_DATE");?></a>
		<span class="b-filter_act">

			<span class="b-filter_show"><?=GetMessage("PROFILE_SEARCHES_SHOW");?></span>
			<a href="<?=$APPLICATION->GetCurPageParam("pagen=15", array("pagen"));?>" class="b-filter_num <?=($arParams['ITEM_COUNT'] == 15) ? ' current' : ''?>">15</a>
			<a href="<?=$APPLICATION->GetCurPageParam("pagen=30", array("pagen"));?>" class="b-filter_num <?=($arParams['ITEM_COUNT'] == 30) ? ' current' : ''?>">30</a>
			<a href="<?=$APPLICATION->GetCurPageParam("pagen=45", array("pagen"));?>" class="b-filter_num <?=($arParams['ITEM_COUNT'] == 45) ? ' current' : ''?>">45</a>
		</span>
	</div>
</div><!-- /.b-filter -->
<a href="#" class="set_opener iblock right"><?=GetMessage("PROFILE_SEARCHES_SETTINGS");?></a>
<?*/?>
<div class="b-shresult_list">
	<?
		if(!empty($arResult['ITEMS']))
		{
			$i = 0;
			foreach($arResult['ITEMS'] as $arItem)
			{
				$i++;
			?>
			<div class="b-shresult_item rel b-result-docitem b-ebook">
				<div class="meta minus">
					<div class="b-hint rel"><span><?=GetMessage("PROFILE_SEARCHES_REMOVE");?></span> <?=GetMessage("PROFILE_SEARCHES_FROM_SEARCHES");?></div>
					<a href="#" data-callback="reloadRightMenuBlock()" data-url="<?=$APPLICATION->GetCurPageParam('id='.$arItem['ID'].'&action=remove', array("id", "action"));?>" class="b-bookadd fav" data-remove="removeonly"></a>
				</div><!-- meta -->
				<div class="num"><?=$i?>.</div>
				<p class="b-shresult-name"><?=GetMessage("PROFILE_SEARCHES_SAVED_REQUEST");?>: <?if(!empty($arItem['UF_NAME'])){?><strong><?=$arItem['UF_NAME']?></strong>, <?}?><span class="timelabel"><?=$arItem['UF_DATE_ADD']?></span></p>
				<div class="b-shresult_info iblock <?=empty($arItem['UF_MORE_OPTIONS']) ? 'nomore' : ''?>">
					<em class=""><?=GetMessage("PROFILE_SEARCHES_YOU_SEARCHED");?>:</em>
					<?
						if(!empty($arItem['UF_QUERY']) or !empty($arItem['UF_NAME']))
						{
						?>
						<h2><a href="<?=$arItem['UF_URL']?>"><?=empty($arItem['UF_QUERY'])? $arItem['UF_NAME'] : $arItem['UF_QUERY']?></a></h2>
						<?
						}
					?>
					<div class="b-shresult_find"><em><?=GetMessage("PROFILE_SEARCHES_FOUND");?>:</em> <?=$arItem['UF_FOUND']?> <a href="<?=$arItem['UF_URL']?>"><?=GetEnding($arItem['UF_FOUND'], 'книг', 'книга', 'книги')?></a> <!--<span class="sh_high">(+3)</span>--> </div>
				</div>
				<?
					if(!empty($arItem['UF_MORE_OPTIONS'])){
					?>
					<div class="b-shresult_more iblock">
						<em><?=GetMessage("PROFILE_SEARCHES_REFINE");?>: </em>
						<?
							foreach($arItem['UF_MORE_OPTIONS'] as $arV)
							{
								if(strpos($arV['text'], ':') !== false){
									$arT = explode(':', $arV['text']);
								?>
								<p><?=$arV['logic']?> <?=$arT[0]?> <a><?=$arT[1]?></a></p>
								<?
								}
								else
								{
								?>
								<p><?=$arV['logic']?> <a><?=$arV['text']?></a></p>
								<?
								}
							}
						?>
					</div>
					<?
					}
				?>
				<div class="b-shresult_act iblock">
					<a href="<?=$arItem['UF_URL']?>" class="b-litebt"><?=GetMessage("PROFILE_SEARCHES_REDO");?></a>
					<div class="b-shresult_selection rel">
						<a href="#" class="b-openermenu js_openmfavs" data-callback="reloadRightMenuBlock()" data-favs="<?=ADD_COLLECTION_URL?>list.php?a=select&t=searches&id=<?=urlencode($arItem['ID'])?>"><?=GetMessage("PROFILE_SEARCHES_MY_COLLETIONS");?></a>
						<div class="b-favs">
							<form class="b-selectionadd collection" action="<?=ADD_COLLECTION_URL?>add.php" method="post">
								<input type="hidden" name="t" value="searches"/>
								<input type="hidden" name="id" value="<?=$arItem['ID']?>"/>
								<input type="submit" class="b-selectionaddsign" value="+">
								<span class="b-selectionaddtxt"><?=GetMessage("PROFILE_SEARCHES_CREATE_COLLECTION");?></span>
								<input type="text" name="name" class="input hidden">
							</form>
						</div><!-- /.b-favs  -->
					</div>
				</div>
			</div><!-- /.b-shresult_item -->
			<?
			}
		}
	?>
	<?=$arResult["NAV_STRING"]?>
</div><!-- /.b-shreult_list -->