<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/**
 * @var CBitrixComponentSearchPageDetail $component
 * @global $APPLICATION
 */
use \Bitrix\Main\Localization\Loc as Loc;
Loc::loadMessages(__FILE__);
if (empty($arResult['BOOK']))
	return false;

//$asset = \Bitrix\Main\Page\Asset::getInstance();
//$asset->addJs('/local/templates/evk/js/social-likes.min.js');

?>
<div class="wrapper">
	<section class="b-inner clearfix">
		<div class="b-bookcover">
			<?if(!empty($arResult['BOOK']["IMAGE_URL_RESIZE"])):?>
				<img src="<?=sprintf('%s%s&p=%d', $arResult['BOOK']["IMAGE_URL_RESIZE"], '&w=270', $arResult['BOOK']["COVER_NUMBER_PAGE"])?>" alt="<?=$arResult['BOOK']["NAME"]?>"/>
			<?else:?>
				<img src="/local/images/book_empty.jpg" alt="<?=$arResult['BOOK']["NAME"]?>"/>
			<?endif;?>
			<?
			if(!empty($arResult['BOOK']['VIEWER_URL']) && $arResult['BOOK']["PROPERTY_STATUS_VALUE"] == 2)
			{
				?>
				<a href="<?=$arResult['BOOK']['VIEWER_URL']?>" target="_blank" class="b-read_bt bt_typelt">Читать</a>
			<?
			}
			?>
			<? if($arResult["CAN_ORDER_BOOKS"]) :?>

				<? if(!OrderTable::bookExists($arResult['BOOK']['ID'])) :?>
					<a data-id="<?=$arResult['BOOK']['ID']?>" onclick="addBook()" class="b-read_bt2 bt_typelt addorder">Заказать</a>
				<? else :?>
					<a data-id="<?=$arResult['BOOK']['ID']?>" onclick="return false" class="b-read_bt2 bt_typelt addorder disabled" disabled="disabled">Заказана</a>
				<? endif; ?>
			<?else:?>
				<a class="b-read_bt2 bt_typelt addorder disabled" href="javascript:void(null)" onclick="return false;">
					Заказать
					<div class="tooltip">
						<p>Вы не можете заказать книгу поскольку не являетесь читателем библиотеки в которой хранится данная книга</p>
					</div>
				</a>
			<? endif; ?>
		</div><!-- /.b-bookcover -->
		<div class="b-bookdescr">
			<div class="b-bookdescr_title">
				<div class="b-bookaction">
				<span class="b-bookshare rel">
					<a href="#"></a>
					<nav class="b-line_social b-bookshare_list">
						<?php
						$description = array();
						if(!empty($arResult['BOOK']["authorbook"])):?>
							<?php $description[] = $arResult['BOOK']["authorbook"]?>
						<?php endif;?>
						<?if(!empty($arResult['BOOK']["PROPERTY_PUBLISH_YEAR_VALUE"])):?>
							<?php $description[] = $arResult['BOOK']["PROPERTY_PUBLISH_YEAR_VALUE"]?>
						<?endif;?>
						<?if(!empty($arResult['BOOK']["publish_place"])):?>
							<?php $description[] = $arResult['BOOK']["publish_place"]?>
						<?endif;?>
						<script type="text/javascript">
							$(function(){
								$('.b-line_social').share();
							});
						</script>
						<?
						$asset = \Bitrix\Main\Page\Asset::getInstance();

						$title = htmlspecialchars($arResult['BOOK']["NAME"]);
						$image = sprintf('http://%s%s&w=270&p=%d', $_SERVER["SERVER_NAME"], $arResult['BOOK']["IMAGE_URL_RESIZE"], $arResult['BOOK']["COVER_NUMBER_PAGE"]);
						$url = sprintf('http://%s%s', $_SERVER["SERVER_NAME"], $APPLICATION->GetCurDir());
						$description = sprintf('Название: %s; ', $arResult['BOOK']["NAME"]);

						if(!empty($arResult['BOOK']["PROPERTY_PART_NO_VALUE"]))
							$description .= sprintf("Номер части: %s; ", $arResult['BOOK']["PROPERTY_PART_NO_VALUE"]);

						if(!empty($arResult['BOOK']["PROPERTY_PART_TITLE_VALUE"]))
							$description .= sprintf("Подзаголовок: %s; ", $arResult['BOOK']["PROPERTY_PART_TITLE_VALUE"]);

						if(!empty($arResult['BOOK']["PROPERTY_VOLUME_VALUE"]))
							$description .= sprintf("Количество страниц: %s; ", $arResult['BOOK']["PROPERTY_VOLUME_VALUE"]);

						if(!empty($arResult['BOOK']["PROPERTY_PUBLISH_YEAR_VALUE"]))
							$description .= sprintf("Год издания: %s; ", $arResult['BOOK']["PROPERTY_PUBLISH_YEAR_VALUE"]);

						if(!empty($arResult['BOOK']["PROPERTY_PUBLISH_VALUE"]))
							$description .= sprintf("Издательство: %s; ", $arResult['BOOK']["PROPERTY_PUBLISH_VALUE"]);

						if(!empty($arResult['BOOK']["PROPERTY_SERIA_VALUE"]))
							$description .= sprintf("Серия: %s; ", $arResult['BOOK']["PROPERTY_SERIA_VALUE"]);

						if(!empty($arResult['BOOK']["publish_place"]))
							$description .= sprintf("Место издания: %s; ", $arResult['BOOK']["publish_place"]);

						if(!empty($arResult['BOOK']["LIBS"]))
						{
							$arLibs = array();
							foreach($arResult['BOOK']["LIBS"] as $LIB)
							{
								if(!empty($LIB["NAME"]))
									$arLibs[] = $LIB["NAME"];
							}
							$libs = join("; ", $arLibs);
							$library = '';
							if(count($arLibs) > 1)
								$library = 'Библиотеки';
							elseif(count($arLibs) == 1)
								$library = 'Библиотеки';
							if(!empty($library))
								$description .= sprintf("%s: %s; ", $library, $libs);
						}

						if(!empty($arResult['BOOK']["PROPERTY_BBK_VALUE"]))
							$description .= sprintf("Код ББК: %s; ", $arResult['BOOK']["PROPERTY_BBK_VALUE"]);

						if(!empty($arResult['BOOK']["PROPERTY_ISBN_VALUE"]))
							$description .= sprintf("Код ISBN: %s; ", $arResult['BOOK']["PROPERTY_ISBN_VALUE"]);
						$description = htmlspecialchars(trim($description));
						$facebook_metas = <<<FB
<meta property="og:title" content="$title" />
<meta property="og:image" content="$image" />
<meta property="og:url" content="$url" />
<meta property="og:description" content="$description" />
FB;
						$asset->addString($facebook_metas);
						?>
						<a href="<?=$url?>" class="ok odnlink" title="<?=$title?>" data-description="<?=$description?>" data-image="<?=$image?>">Одноклассники</a>
						<a href="<?=$url?>" class="vk vklink" title="<?=$title?>" data-description="<?=$description?>" data-image="<?=$image?>">Вконтакте</a>
						<a href="<?=$url?>" class="fb fblink">Facebook</a>
					</nav>
				</span>
					<?if($arResult["IS_USER_AUTH"]):?>
					<a data-collection="/local/tools/collections/list.php?t=books&id=<?=$arResult["BOOK"]["ID"]?>&get=id" data-url="/local/tools/collections/removeBook.php?id=<?=$arResult['USER_BOOKS']?>" href="#" class="b-book_add favorite-book<?if(!empty($arResult['USER_BOOKS'])):?> active<?endif;?>"></a>
					<?endif;?>
				</div>
				<h1><?=$arResult['BOOK']["NAME"]?></h1>
				<?php if(!empty($arResult['BOOK']["authorbook"])):?>
					<div class="b-bookdescr_autor"><?=$arResult['BOOK']["authorbook"]?></div>
				<?php endif;?>
			</div><!-- /.b-bookdescr_title -->
			<ul class="b-bookinfo">
				<?if(!empty($arResult['BOOK']["PROPERTY_PART_NO_VALUE"])):?>
					<li>
						<span class="title">Номер части</span>
						<span class="value"><?=$arResult['BOOK']["PROPERTY_PART_NO_VALUE"]?></span>
					</li>
				<?endif;?>
				<?if(!empty($arResult['BOOK']["PROPERTY_PART_TITLE_VALUE"])):?>
					<li>
						<span class="title">Подзаголовок</span>
						<span class="value"><?=$arResult['BOOK']["PROPERTY_PART_TITLE_VALUE"]?></span>
					</li>
				<?endif;?>
				<?if(!empty($arResult['BOOK']["PROPERTY_VOLUME_VALUE"])):?>
					<li>
						<span class="title">Количество страниц</span>
						<span class="value"><?=$arResult['BOOK']["PROPERTY_VOLUME_VALUE"]?></span>
					</li>
				<?endif;?>
				<?if(!empty($arResult['BOOK']["PROPERTY_PUBLISH_YEAR_VALUE"])):?>
				<li>
					<span class="title">Год издания</span>
					<span class="value"><?=$arResult['BOOK']["PROPERTY_PUBLISH_YEAR_VALUE"]?></span>
				</li>
				<?endif;?>
				<?if(!empty($arResult['BOOK']["PROPERTY_PUBLISH_VALUE"])):?>
				<li>
					<span class="title">Издательство</span>
					<span class="value"><?=$arResult['BOOK']["PROPERTY_PUBLISH_VALUE"]?></span>
				</li>
				<?endif;?>
				<?if(!empty($arResult['BOOK']["PROPERTY_SERIA_VALUE"])):?>
				<li>
					<span class="title">Серия</span>
					<span class="value"><?=$arResult['BOOK']["PROPERTY_SERIA_VALUE"]?></span>
				</li>
				<?endif;?>
				<?if(!empty($arResult['BOOK']["publish_place"])):?>
					<li>
						<span class="title">Место издания</span>
						<span class="value"><a href="<?=$arResult['BOOK']["r_publish_place"]?>" class="tdu"><?=$arResult['BOOK']["publish_place"]?></a></span>
					</li>
				<?endif;?>
				<?if(!empty($arResult['BOOK']["LIBS"])):?>
				<li>
					<span class="title">Библиотека</span>
					<span class="value">
						<?
						$arLibs = array();
						foreach($arResult['BOOK']["LIBS"] as $LIB)
						{
							$arLibs[] = sprintf('<a href="%s" class="tdu">%s</a>', $LIB["DETAIL_PAGE_URL"], $LIB["NAME"]);
						}
						echo join(", ", $arLibs);
						?>
					</span>
				</li>
				<?endif;?>
				<?if(!empty($arResult['BOOK']["PROPERTY_BBK_VALUE"])):?>
					<li>
						<span class="title">Код ББК</span>
						<span class="value"><?=$arResult['BOOK']["PROPERTY_BBK_VALUE"]?></span>
					</li>
				<?endif;?>
				<?if(!empty($arResult['BOOK']["PROPERTY_ISBN_VALUE"])):?>
					<li>
						<span class="title">Код ISBN</span>
						<span class="value"><?=$arResult['BOOK']["PROPERTY_ISBN_VALUE"]?></span>
					</li>
				<?endif;?>
			</ul>
		</div><!-- /.b-bookdescr -->
	</section>
</div>
<script>
	function addBook() {
		bid = $(".addorder").attr('data-id');
		$.ajax({
			type: "GET",
			url: "/local/tools/add_book_order.php?BOOK_ID="+bid,
			success: function(response){
				if (response == "OK")
				{
					$(".addorder").addClass("disabled");
					$(".addorder").text("Заказана");
				}
			}
		})
	}
</script>
<style>
	a.bt_typelt.addorder.disabled {
	  background-color: #CDBEB4;
		cursor:auto;
	}
	a.b-read_bt2 {
	  padding: 16px 10px;
	  width: 156px;
	  text-align: center;
	  min-width: 0;
	  margin: 10px 20px 0 0;
	  font-size: 16px;
	}
	a.bt_typelt.addorder {
	{
		cursor: pointer;
	}
</style>

<?
//\Evk\Books\Books::p($arResult);
?>
