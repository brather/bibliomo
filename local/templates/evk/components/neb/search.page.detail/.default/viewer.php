<?use \Bitrix\Main\Localization\Loc as Loc;
/**
 * @var CBitrixComponentSearchPageDetail $component
 * @var CBitrixComponentTemplate $this
 */
Loc::loadMessages(__FILE__);
$arResult['BOOK']['URL'] = urldecode($arResult['VARIABLES']['BOOK_ID']);
global $USER;
if (CModule::IncludeModule('evk.books')) {
	\Evk\Books\Tables\MoStatsEventTable::trackEvent('viewer_page');
}
?>
<!DOCTYPE html>
<html>
	<head>
		<title><?=$component->GetField(($string_key = "NAME"))?></title>
		<meta charset="utf-8">
		<link href="/local/templates/.default/markup/css/main.css" type="text/css"  data-template-style="true"  rel="stylesheet" />
		<link href="/local/templates/.default/markup/css/styles.css" type="text/css"  data-template-style="true"  rel="stylesheet" />
		<link rel="stylesheet" href="/local/templates/.default/markup/js/jquery.arcticmodal-0.3.css">
		<link rel="stylesheet" href="/local/templates/.default/markup/js/themes/simple.css">
		<script type="text/javascript" src="/local/templates/.default/markup/js/libs/jquery.min.js"></script>
		<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
		<script src="/local/templates/evk/js/modernizr.min.js"></script>
		<script type="text/javascript" src="/local/templates/.default/markup/js/plugins.js"></script>
		<script src="/local/templates/.default/markup/js/jquery.arcticmodal-0.3.min.js"></script>
		<script type="text/javascript" src="/bitrix/js/jqRotate.js"></script>
		<script type="text/javascript" src="/bitrix/js/jquery.Jcrop.js"></script>
		<script type="text/javascript" src="/local/templates/.default/markup/js/jquery.mousewheel.min.js"></script>
		<script type="text/javascript" src="/local/templates/.default/markup/js/script_viewer.js"></script>
	</head>
	<body>
	<?
	//\Evk\Books\Books::p($arResult);
	?>
	<div class="page-book">
	<div class=""></div>
	<span class="turn-page">Свернуть экран</span>
		<!-- left -->
		<div class="left">
			<a href="/" class="logo"></a>
		</div>
		<!-- end left -->

		<!-- right -->
		<div class="right">
			<div class="top-right-tools">
				<ul>
						<li><span class="save" title="Сохранить страницу"></span></li>
						<li class="sub">
							<span class="scale" title="Масштаб страницы"></span>
							<ul style="height: 0;">
								<li><span class="reduce" title="Уменьшить"></span></li>
								<li><span class="increase" title="Увеличить"></span></li>
								<li><span class="scale-page" title="Смотреть страницу во весь экран"></span></li>
							</ul>
						</li>

					<?php if($arResult["ROLE"] != ''):?>
						<li><span class="bookmark-tools" title="Добавить страницу в закладки"></span></li>
						<li><span class="favorite-book<?if(!empty($arResult['USER_BOOKS'])):?> active<?endif;?>" title="<?if(!empty($arResult['USER_BOOKS'])):?>В личном кабинете<?else:?>Добавить в личный кабинет<?endif;?>" data-collection="/local/tools/collections/list.php?t=books&id=<?=$arResult["BOOK"]["ID"]?>&get=id" data-url="/local/tools/collections/removeBook.php?id=<?=$arResult['USER_BOOKS']?><?=$component->CheckExistsField(($string_key = "URL"))?>"></span></li>
					<?php endif;?>
						<li><span class="properties-book" title="Описание книги"></span></li>
						<li><span class="question" title="Руководство пользователя"><a target="_blank" href='/upload/documents/nebmo_rukov_polzov.pdf'></a></span></li>
				</ul>
			</div>
				<div class="right-tools">
					<div class="properties-book-content">
						<?if($arResult['BOOK']['authorbook']):?><div><h3><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_AUTHOR')?></h3> <span><?=$arResult['BOOK']['authorbook']?></span></div><?endif;?>
						<?if($arResult['BOOK']['title']):?><div><h3><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_TITLE')?></h3> <span><?=$arResult['BOOK']['title']?></span></div><?endif;?>
						<?if($arResult['BOOK']['PROPERTY_PUBLISH_YEAR_VALUE']):?><div><h3><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_YEAR')?></h3> <span><?=$arResult['BOOK']['PROPERTY_PUBLISH_YEAR_VALUE']?></span></div><?endif;?>
						<?if($arResult['BOOK']['PROPERTY_PUBLISH_VALUE']):?><div><h3><span><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_PUBLISH')?></h3> <span><?=$arResult['BOOK']['PROPERTY_PUBLISH_VALUE']?></span></div><?endif;?>
						<?if($arResult['BOOK']['PROPERTY_BBK_VALUE']):?><div><h3>BBK:</h3> <span><?=$arResult['BOOK']['PROPERTY_BBK_VALUE']?></span></div><?endif;?>
						<?if($arResult['BOOK']['PROPERTY_VOLUME_VALUE']):?><div><h3><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_PAGES')?></h3> <span><?=$arResult['BOOK']['PROPERTY_VOLUME_VALUE']?></span></div><?endif;?>
						<?if($arResult['BOOK']['PROPERTY_SERIA_VALUE']):?><div><h3><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_SERIE')?></h3> <span><?=$arResult['BOOK']['PROPERTY_SERIA_VALUE']?></span></div><?endif;?>
						<?if($arResult['BOOK']['PROPERTY_PUBLISH_PLACE_VALUE']):?><div><h3><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_PUBLISH_PLACE')?></h3> <span><?=$arResult['BOOK']['PROPERTY_PUBLISH_PLACE_VALUE']?></span></div><?endif;?>
						<?if($arResult['BOOK']['PROPERTY_ISBN_VALUE']):?><div><h3>ISBN</h3> <span><?=$arResult['BOOK']['PROPERTY_ISBN_VALUE']?></span></div><?endif;?>
						<?if($arResult['BOOK']['PROPERTY_LIBRARIES_VALUE']):?>
							<div>
								<h3><?=Loc::getMessage('LIB_SEARCH_PAGE_DETAIL_LIB')?></h3>
								<?
								$LIBRARIES = $arResult['BOOK']['LIBS'];
								foreach($LIBRARIES as $key=>$LIBRARY):?>
									<span><?=$LIBRARY["NAME"]?></span>
									<?if($key < count($LIBRARIES)-1) echo ", "?>
								<?endforeach;?>
							</div>
						<?endif;?>
					</div>
					<?php if($arResult["ROLE"] != ''):?>
						<div>
							<span class="quote">Цитаты</span>
							<div class="quote-content">
								<?foreach($component->GetField(($string_key = "QUOTES")) as $quotes):?>
								<div>
									<a p="<?=$quotes["UF_PAGE"]?>"><span><?=$quotes["UF_PAGE"]?> страница</span>
									<img src='<?=$quotes["UF_IMG_DATA"]?>'>
									</a>
									<span class='delete' id='<?=$quotes["ID"]?>'></span>
								</div>
								<?endforeach;?>
							</div>
						</div>
						<div>
							<span class="bookmark">Закладки</span>
							<div class="bookmark-content">
								<?foreach ($component->GetField(($string_key = "MARK")) as $mark):?>
								<div>
									<a p="<?=$mark['NUM_PAGE']?>"><span><?=$mark['NUM_PAGE']?></span> страница</a>
									<span class='delete' id='<?=$mark['ID']?>'></span>
								</div>
								<?endforeach;?>
							</div>
						</div>
						<div>
							<span class="note">Заметки</span>
							<div class="note-content">
								<?foreach($component->GetField(($string_key = "NOTE")) as $note):?>
								<div>
									<a p="<?=$note["NUM_PAGE"]?>"><span><?=$note["NUM_PAGE"]?> страница</span>
									<span><?=$note["TEXT"]?></span>
									</a>
									<span class='delete' id='<?=$note["ID"]?>'></span>
								</div>
								<?endforeach;?>
							</div>
						</div>
					<?php endif;?>
				</div>
			</div>
			<!-- end right -->

			<!-- main -->
			<div class="main">
				<div class="prev-page"></div>
				<div class="view-content">
				<div class="book-title">
					<?=$arResult['BOOK']['title']?>
					<span><?=$arResult['BOOK']['PROPERTY_AUTHOR_VALUE']?></span>
				</div>
			<div class="view-page">
				<span class="add-bookmark" title="Добавить страницу в закладки"></span>
			<div class="viewer">
				<div class='viewer_img'>
					<img id='image' class='zoomer-image'>
				</div>
			</div>
			<div class="preloader"></div>
				<div class="quote-book">
					<div class="quote-icon"></div>
					<div class="quote-book-content"></div>
				</div>
				<div class="note-book">
					<div class="note-icon"></div>
					<div class="note-book-content"></div>
				</div>
			<div id='selectedrect' style='display:none'>
				<div id='selected_rect_menu'><a href='#' id='b-textLayer_quotes'>Добавить в цитаты</a>
				<a href='' id='b-textLayer_notes'>Создать заметку</a></div>
				<div class='layer_notes' style='display:none;'>
					Введите текст заметки
					<textarea id='text_note'></textarea><input type='button' value='Добавить' id='add_note'>
				</div>
				</div>
					</div>
					<input type='hidden' id="page-number" value='<?=(intval($_REQUEST['page'])>0?intval($_REQUEST['page']):1)?>'>
					<input type='hidden' id="book_id" value='<?=$arResult['BOOK']['ID']?>'>
					<input type='hidden' id="count_page" value='<?=$arResult['BOOK']['PROPERTY_COUNT_PAGES_VALUE']?>'>
				</div>
				<div class="navigation">
					<ul>
						<li><span class="first" p=1></span></li>
						<li><span class="prev"></span></li>
						<li><span class='page_1'></span></li>
						<li><span class='page_2'></span></li>
						<li><span class="page_3"></span></li>
						<li>...</li>
						<li><span p=<?=$arResult['BOOK']['PROPERTY_COUNT_PAGES_VALUE']?>><?=$arResult['BOOK']['PROPERTY_COUNT_PAGES_VALUE']?></span></li>
						<li><span class="next"></span></li>
						<li><span class="last" p=<?=$arResult['BOOK']['PROPERTY_COUNT_PAGES_VALUE']?>></span></li>
					</ul>
					<span class="toggle-page-navigation"></span>
					<div class="page-navigation">
						<label>Переход к странице</label>
						<input type="text" class='page'>
						<span class="go"></span>
					</div>
				</div>
				<div class="next-page"></div>
			</div>
			<div style="display: none;">
				<div class="box-modal" id="exampleModal">
					<div class="box-modal_close arcticmodal-close">закрыть</div>
					<p></p>
				</div>
			</div>
			<!-- end main -->
			<!-- footer -->
			<footer>
				<div class="wrapper">
					<div class="clearfix">
						<?$APPLICATION->IncludeFile("/local/include_areas/copyright.php", Array(), Array(
							"MODE" => "text",
						));?>
						<?$APPLICATION->IncludeComponent("bitrix:menu", "bottom", array(
							"ROOT_MENU_TYPE" => "bottom1_" . LANGUAGE_ID,
							"MENU_CACHE_TYPE" => "N",
							"MENU_CACHE_TIME" => "3600",
							"MENU_CACHE_USE_GROUPS" => "Y",
							"MENU_CACHE_GET_VARS" => array(
							),
							"MAX_LEVEL" => "1",
							"CHILD_MENU_TYPE" => "left",
							"USE_EXT" => "N",
							"DELAY" => "N",
							"ALLOW_MULTI_SELECT" => "N"
						),
							false
						);?>
						<?$APPLICATION->IncludeComponent("bitrix:menu", "bottom", array(
							"ROOT_MENU_TYPE" => "bottom2_" . LANGUAGE_ID,
							"MENU_CACHE_TYPE" => "N",
							"MENU_CACHE_TIME" => "3600",
							"MENU_CACHE_USE_GROUPS" => "Y",
							"MENU_CACHE_GET_VARS" => array(
							),
							"MAX_LEVEL" => "1",
							"CHILD_MENU_TYPE" => "left",
							"USE_EXT" => "N",
							"DELAY" => "N",
							"ALLOW_MULTI_SELECT" => "N"
						),
							false
						);?>
						<?$APPLICATION->IncludeComponent("bitrix:menu", "bottom", array(
							"ROOT_MENU_TYPE" => "bottom3_" . LANGUAGE_ID,
							"MENU_CACHE_TYPE" => "N",
							"MENU_CACHE_TIME" => "3600",
							"MENU_CACHE_USE_GROUPS" => "Y",
							"MENU_CACHE_GET_VARS" => array(
							),
							"MAX_LEVEL" => "1",
							"CHILD_MENU_TYPE" => "left",
							"USE_EXT" => "N",
							"DELAY" => "N",
							"ALLOW_MULTI_SELECT" => "N"
						),
							false
						);?>
					</div>
					<div class="b-footer_row clearfix">
						<div class="b-copyrule"><?=Loc::getMessage("MAIN_FOOTER_COPY_RULE")?></div>
						<a target="_blank" href="http://mk.mosreg.ru/" class="b-footer_support">
							<?=Loc::getMessage("DEVELOPER_INFORMATION")?>
						</a>
						<div class="b-nota">
							<p><?=Loc::getMessage("DEVELOPER_MAKING")?></p>
							<a href="http://notamedia.ru" target="_blank"><img width="126" src="<?=MARKUP?>/i/nota_logo.svg" alt="Notamedia" ></a>
						</div>
					</div>
				</div><!-- /.wrapper -->
			  
			</footer>
			<!-- end footer -->
			<img class='big_image' style='display:none;' >
			<div class="prev-page prev-big-img"></div>
			<div class="next-page next-big-img"></div>
		</div>
	</body>
</html>
