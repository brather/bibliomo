<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
	use \Bitrix\Main\Localization\Loc as Loc;
	Loc::loadMessages(__FILE__);
?>
	<?
		if(empty($arResult['ITEMS']))
		{
		?>
			<?
				if(!empty($arParams['q']))
				{
					?>
					На портале НЭБ найдено 0 документов
					<?
				}
			?>
				<h2>
					<?php if (empty($arParams["q"])): ?>
						Введите поисковое выражение
					<?php else: ?>
						К сожалению, на портале НЭБ ничего не найдено
					<?php endif;?>
				</h2>
				<?
				/*
					if(!empty($arResult['REFINEMENT'])){
					?>
					<span><?=Loc::getMessage('LIB_SEARCH_PAGE_NEB_TEMPLATE_SUGGEST');?> <a href="<?=$APPLICATION->GetCurPageParam('q='.$arResult['REFINEMENT'], array('q','dop_filter'));?>"><?=$arResult['REFINEMENT']?></a></span>
					<?
					}
				*/
				?>
		<?
		}
		if(!empty($arResult['ITEMS']))
		{
		?>
			<?
			if(!empty($arResult['RESULT_PARAMS']) or !empty($arParams['~q2']))
			{
				?>
				<div class="b-searchresult_portal">
					<?
					if(!empty($arResult['RESULT_PARAMS']))
					{
						?>
						<div class="b-searchresult_portal">
							<?=Loc::getMessage('LIB_SEARCH_PAGE_NEB_TEMPLATE_SEARCH_ON_WEBSITE');?>:
							<?
							$i = 0;
							foreach($arResult['RESULT_PARAMS'] as $param)
							{
								$i++;
								?>
								<?=($i > 1 || $param['logic'] == "НЕ") ? $param['logic'].' ' : ''?> <span class="b-searchresult_tag"><?=$param['text']?> <a href="<?=$param['url']?>" class="del"></a></span>
							<?
							}
							?>
						</div>
					<?
					}
					?>
				</div><!-- /.b-searchresult_info -->
			<?
			}
			?>
		<?
			if($arResult['NHITS'] >= 300000)
			{
				echo Loc::getMessage('LIB_SEARCH_PAGE_NEB_FOUND_MORE_300000_RESULT');
			}
			else
			{
				$query_str_evk = '';
				if(!$arParams["IS_FULL_SEARCH"])
					$query_str_evk = "s_tc=Y";
				if(!$arResult["publishyear_next_max"])
					$query_str_evk = sprintf("%spublishyear_next=%d", (!empty($query_str_evk)?'&':''), $arResult["publishyear_next"]);
				if(!$arResult["publishyear_prev_min"])
					$query_str_evk = sprintf("%spublishyear_prev=%d", (!empty($query_str_evk)?'&':''), $arResult["publishyear_prev"]);

				if(!empty($arParams['~q2']))
				{
					?>
					<span class="b-searchres_lb">
					<?=GetEnding($arResult['NHITS'], Loc::getMessage("LIB_SEARCH_PAGE_NEB_TEMPLATE_FOUND_NEB_5", array(
						"#QUERY#" => htmlspecialchars($arParams["~q2"]),
						"#QUANTITY#" => sprintf('<a style="text-decoration: underline;" target="_blank" href="http://xn--90ax2c.xn--p1ai%s">%s</a>', $APPLICATION->GetCurPageParam($query_str_evk, array(
							"clear_cache",
							"debug",
							"s_tc",
							'publishyear_next',
							'publishyear_prev',
						)), $arResult['NHITS'])
					)
				), Loc::getMessage("LIB_SEARCH_PAGE_NEB_TEMPLATE_FOUND_NEB_1", array(
						"#QUERY#" => htmlspecialchars($arParams["~q2"]),
						"#QUANTITY#" => sprintf('<a style="text-decoration: underline;" target="_blank" href="http://xn--90ax2c.xn--p1ai%s">%s</a>', $APPLICATION->GetCurPageParam($query_str_evk, array(
							"clear_cache",
							"debug",
							"s_tc",
							'publishyear_next',
							'publishyear_prev',
						)), $arResult['NHITS'])
					)
				), Loc::getMessage("LIB_SEARCH_PAGE_NEB_TEMPLATE_FOUND_NEB_2", array(
						"#QUERY#" => htmlspecialchars($arParams["~q2"]),
						"#QUANTITY#" => sprintf('<a style="text-decoration: underline;" target="_blank" href="http://xn--90ax2c.xn--p1ai%s">%s</a>', $APPLICATION->GetCurPageParam($query_str_evk, array("clear_cache", "debug", "s_tc",
							'publishyear_next',
							'publishyear_prev',
						)), $arResult['NHITS'])
					)
				)
				)?>.
					</span>
				<?
				}
				else
				{?>
					<span class="b-searchres_lb">
					<?=GetEnding($arResult['NHITS'], Loc::getMessage("LIB_SEARCH_PAGE_NEB_TEMPLATE_FOUND_5", array(
						"#QUANTITY#" => sprintf('<a style="text-decoration: underline;" target="_blank" href="http://xn--90ax2c.xn--p1ai%s">%s</a>', $APPLICATION->GetCurPageParam($query_str_evk, array("clear_cache", "debug", "s_tc",
							'publishyear_next',
							'publishyear_prev',
						)), $arResult['NHITS'])
					)
				), Loc::getMessage("LIB_SEARCH_PAGE_NEB_TEMPLATE_FOUND_1", array(
						"#QUANTITY#" => sprintf('<a style="text-decoration: underline;" target="_blank" href="http://xn--90ax2c.xn--p1ai%s">%s</a>', $APPLICATION->GetCurPageParam($query_str_evk, array("clear_cache", "debug", "s_tc",
							'publishyear_next',
							'publishyear_prev',
						)), $arResult['NHITS'])
					)
				), Loc::getMessage("LIB_SEARCH_PAGE_NEB_TEMPLATE_FOUND_2", array(
						"#QUANTITY#" => sprintf('<a style="text-decoration: underline;" target="_blank" href="http://xn--90ax2c.xn--p1ai%s">%s</a>', $APPLICATION->GetCurPageParam($query_str_evk, array("clear_cache", "debug", "s_tc",
							'publishyear_next',
							'publishyear_prev',
						)), $arResult['NHITS'])
					)
				)
				)?>.
					</span>
				<?
				}
			}
		?>

			<div class="b-sidetable">
			<?
				$APPLICATION->IncludeComponent(
					"neb:search.page.viewer.left",
					"exalead",
					Array(
						"PARAMS" => $arParams,
						"RESULT" => $arResult,
					),
					$component
				);
			?>
		</div><!-- /.b-sidetable -->
		<?
		}
	?>