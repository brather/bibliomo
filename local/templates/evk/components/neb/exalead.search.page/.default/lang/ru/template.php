<?
	$MESS['LIB_SEARCH_PAGE_NEB_TEMPLATE_FOUND_ZERO'] = 'В ЕИСУБ найдено 0 документов';
	$MESS['LIB_SEARCH_PAGE_NEB_TEMPLATE_NOTHING_FOUND'] = 'К сожалению на портале ЕИСУБ, ничего не найдено';
	$MESS['LIB_SEARCH_PAGE_NEB_TEMPLATE_SUGGEST'] = 'Возможно вы искали';

	$MESS['LIB_SEARCH_PAGE_NEB_TEMPLATE_FOUND_5'] = 'В ЕИСУБ найдено #QUANTITY# книг';
	$MESS['LIB_SEARCH_PAGE_NEB_TEMPLATE_FOUND_1'] = 'В ЕИСУБ найдена #QUANTITY# книга';
	$MESS['LIB_SEARCH_PAGE_NEB_TEMPLATE_FOUND_2'] = 'В ЕИСУБ найдены #QUANTITY# книги';

	$MESS['LIB_SEARCH_PAGE_NEB_TEMPLATE_FOUND_NEB_5'] = 'По вашему запросу «<b>#QUERY#</b>» в НЭБ найдено #QUANTITY# книг';
	$MESS['LIB_SEARCH_PAGE_NEB_TEMPLATE_FOUND_NEB_1'] = 'По вашему запросу «<b>#QUERY#</b>» в НЭБ найдена #QUANTITY# книга';
	$MESS['LIB_SEARCH_PAGE_NEB_TEMPLATE_FOUND_NEB_2'] = 'По вашему запросу «<b>#QUERY#</b>» в НЭБ найдены #QUANTITY# книги';

	$MESS['LIB_SEARCH_PAGE_NEB_TEMPLATE_DOCUMENT_5'] = 'результатов';
	$MESS['LIB_SEARCH_PAGE_NEB_TEMPLATE_DOCUMENT_1'] = 'результата';
	$MESS['LIB_SEARCH_PAGE_NEB_TEMPLATE_DOCUMENT_2'] = 'результат';

	$MESS['LIB_SEARCH_PAGE_NEB_TEMPLATE_SAVE_QUERY'] = 'Сохранить поисковый запрос';
	$MESS['LIB_SEARCH_PAGE_NEB_TEMPLATE_QUERY_SAVED'] = 'Поисковый запрос сохранен';
	$MESS['LIB_SEARCH_PAGE_NEB_TEMPLATE_ADD_TITLE'] = 'Добавить название';
	$MESS['LIB_SEARCH_PAGE_NEB_TEMPLATE_SPECIFY_SEARCH'] = 'уточнить поиск';
	$MESS['LIB_SEARCH_PAGE_NEB_TEMPLATE_SEARCH_ON_WEBSITE'] = 'Вы искали на портале ЕИСУБ';
	$MESS['LIB_SEARCH_PAGE_NEB_TEMPLATE_IN_WEBSITE'] = 'Искать только в отсканированных изданиях';
	$MESS['LIB_SEARCH_PAGE_NEB_TEMPLATE_IN_LIBRARY'] = 'По каталогу печатных изданий';
	$MESS['LIB_SEARCH_PAGE_NEB_TEMPLATE_SEARCH_EMPTY'] = 'Введите поисковое выражение';
	$MESS['LIB_SEARCH_PAGE_NEB_SETTINGS'] = 'Настройка';
	$MESS['LIB_SEARCH_PAGE_NEB_FOUND_MORE_300000_RESULT'] = 'Найдено более 300 000 результатов на портале ЕИСУБ';
	$MESS['LIB_SEARCH_PAGE_NEB_TEMPLATE_FOUND_COPIES'] = 'экземпляров изданий';

?>