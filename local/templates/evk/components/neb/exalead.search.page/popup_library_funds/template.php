<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
	use \Bitrix\Main\Localization\Loc as Loc;
	Loc::loadMessages(__FILE__);
?>
<script type="text/javascript">
	$( document ).ajaxSuccess(function( event, xhr, settings )
	{
		if(settings.url.indexOf('/local/templates/evk/components/neb/profile/.default/popup_funds_add.php') > -1)
		{
			var fields = {};
			var container = $('table.b-result-doc');
			if(container.find('input[name="BOOK_NUM_ID"]:radio:checked').length == 1)
			{
				var chBox = container.find('input[name="BOOK_NUM_ID"]:radio:checked');
				var chBoxClosest = chBox.closest('div.b-radio');
				fields['BOOK_ID'] = {
					'VALUE':chBoxClosest.find('input[name="BOOK_ID"]').val(),
					'NAME':'XML_ID'
				};
				fields['BOOK_TITLE'] = {
					'VALUE':chBoxClosest.find('input[name="BOOK_TITLE"]').val(),
					'NAME':'NAME'
				};
				fields['BOOK_AUTHOR'] = {
					'VALUE':chBoxClosest.find('input[name="BOOK_AUTHOR"]').val(),
					'NAME':'AUTHOR'
				};
				fields['BOOK_YEAR'] = {
					'VALUE':chBoxClosest.find('input[name="BOOK_YEAR"]').val(),
					'NAME':'YEAR'
				};
				fields['BOOK_PAGES'] = {
					'VALUE':chBoxClosest.find('input[name="BOOK_PAGES"]').val(),
					'NAME':'VOLUME'
				};
				fields['BOOK_PUBLISHER'] = {
					'VALUE':chBoxClosest.find('input[name="BOOK_PUBLISHER"]').val(),
					'NAME':'Publisher'
				};
				$('.XML_ID_TEXT').show();
				$('#XML_ID_TEXT').text(chBoxClosest.find('input[name="BOOK_ID"]').val());
				for(var field in fields)
				{
					if(fields[field]['VALUE'])
						$('.b_bookfond_popup_add').find('input#'+fields[field]['NAME']).val(fields[field]['VALUE']);
				}
			}
		}
	});
</script>
<form>
	<div class="fieldrow">
		<table class="b-result-doc">
            <?$i = 0;?>
			<?foreach($arResult["ITEMS"] as $item):?>
				<tr class="b-result-docitem">
					<td>
						<h2><span class="num"><?=$item['NUM_ITEM']?>.</span><a href="<?=$item['DETAIL_PAGE_URL']?>" target="_blank"><?=$item['title']?></a></h2>
						<ul class="b-resultbook-info">
							<?if(!empty($item['authorbook'])):?><li><span><?=GetMessage('LIB_SEARCH_PAGE_TEMLATE_BOOK_AUTHOR');?>:</span> <?=$item['authorbook']?></li><?endif;?>
							<?if(!empty($item['publishyear'])):?><li><span><?=GetMessage('LIB_SEARCH_PAGE_TEMLATE_BOOK_YEAR');?>:</span> <?=$item['publishyear']?></li><?endif;?>
							<?if(!empty($item['countpages'])):?><li><span><?=GetMessage('LIB_SEARCH_PAGE_TEMLATE_BOOK_PAGES');?>:</span> <?=$item['countpages']?></li><?endif;?>
							<?if(!empty($item['publisher'])):?><li><span><?=GetMessage('LIB_SEARCH_PAGE_TEMLATE_BOOK_PUBLISHER');?>:</span> <?=$item['publisher']?></li><?endif;?>
							<?if(!empty($item['library'])):?><li><span>	<em><?=GetMessage('LIB_SEARCH_PAGE_TEMLATE_BOOK_SOURCE');?>:</em> <?=$item['library']?></li><?endif;?>
						</ul>
					</td>
					<td>
						<div class="b-radio">
							<input type="radio" name="BOOK_NUM_ID" value="<?=$item['NUM_ITEM']?>" class="radio" id="libraryFundsBook" <?if($i++ == 0):?>checked="checked" <?endif;?>>
							<input type="hidden" name="BOOK_ID" value="<?=trim(htmlspecialchars(strip_tags($item['id'])))?>">
							<input type="hidden" name="BOOK_TITLE" value="<?=trim(htmlspecialchars(strip_tags($item['title'])))?>">
							<input type="hidden" name="BOOK_AUTHOR" value="<?=trim(htmlspecialchars(strip_tags($item['authorbook'])))?>">
							<input type="hidden" name="BOOK_YEAR" value="<?=trim(htmlspecialchars(strip_tags($item['publishyear'])))?>">
							<input type="hidden" name="BOOK_PAGES" value="<?=trim(htmlspecialchars(strip_tags($item['countpages'])))?>">
							<input type="hidden" name="BOOK_PUBLISHER" value="<?=trim(htmlspecialchars(strip_tags($item['publisher'])))?>">
						</div>
					</td>
				</tr>
			<?endforeach;?>
		</table><!-- /.b-result-doc -->
	</div>
</form>
	<?=$arResult['STR_NAV']?>

