<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
	use \Bitrix\Main\Localization\Loc as Loc;
	Loc::loadMessages(__FILE__);
?>
<?
if(isset($_REQUEST["AJAX_RESULT"]) && $_REQUEST["AJAX_RESULT"] == "Y")
	$APPLICATION->RestartBuffer();

	if ( (int)$arResult['VALUES']['USER_ID'] > 0 && $arResult["USE_EMAIL_CONFIRMATION"] === "Y" )
	{
	?>
	<section class="innersection innerwrapper clearfix" id="panel-register">
		<div class="b-formsubmit">
			<span class="b-successlink">Вы были успешно зарегистрированы.</span>
			<p>Вам на почту было отправлено сообщение с кодом подтверждения. Пройдите по ссылке в сообщении, чтобы подтвердить адрес электронной почты и активировать аккаунт.</p>
		</div>
	</section>
	<?
		if(isset($_REQUEST["AJAX_RESULT"]) && $_REQUEST["AJAX_RESULT"] == "Y")
			exit;
		return;
	}
?>
<section class="innersection innerwrapper clearfix" id="panel-register">
	<style type="text/css">
		#panel-agreement
		{
			position: absolute;
			top: 0;
			background-color: #fff;
			z-index: 1;
			padding: 40px;
		}
		#div_iframe
		{
			height: 500px;
			width: 100%;
			overflow-y: scroll;
			font-family: "noto sans";
			font-size: 0.8em;
			margin: 20px 0;
			border: 1px solid #DFDFDF;
			padding: 10px;
		}
	</style>
	<script type="text/javascript">
		function setlogin(){
			$('input#LOGIN').val($('input#settings04').val());
		}
		$(function(){
			$('#regform').unbind('submit').submit(function(e){
				e.preventDefault();
				var regform = $(this);
				$.post(regform.attr('action'), regform.serialize(), function(data){
					$('#panel-register').replaceWith(data);
					setTimeout(function(){
						$(window).scrollTop(0);
					}, 200);
				});
			});

			$('#regform button.formbutton').attr('disabled', true);

			$('.open_agreement').click(function(e){
				e.preventDefault();
				BX.showWait();
				if($('#panel-agreement').length <= 0)
				{
					$('#panel-register').before('<div id="panel-agreement"></div>');
				}

				$('#panel-agreement').load($(this).attr('href'), function(){
					BX.closeWait();

					$('#panel-agreement')
						.width($('#panel-register').outerWidth())
						.height($('#panel-register').outerHeight());

					$('#panel-agreement .closepopup').click(function(){
						$('#panel-agreement').remove();
					});
					$('#panel-agreement #agreement').click(function(){
						$('input[name="agreebox"]').val(1);
						$('#regform button.formbutton').attr('disabled', false);
						$('#panel-agreement').remove();
					});
					$(window).scrollTop(0);
				});
			});
		});
	</script>
	<div class="b-registration rel">
		<h2 class="mode"><?=Loc::getMessage('MAIN_REGISTER_TITLE');?></h2>
		<?
			if (count($arResult["ERRORS"]) > 0)
			{
				foreach ($arResult["ERRORS"] as $key => $error)
				{
					if (intval($key) == 0 && $key !== 0)
					{
						$arResult["ERRORS"][$key] = str_replace("#FIELD_NAME#", "&quot;".GetMessage("REGISTER_FIELD_".$key)."&quot;", $error);
					}
				}
				?>
				<p><?php ShowError(implode("<br />", $arResult["ERRORS"]).'<br />');?></p>
				<?
			}
			elseif($arResult["USE_EMAIL_CONFIRMATION"] === "Y" && (int)$arResult['VALUES']['USER_ID'] > 0 )
			{
				?>
				<p><?=GetMessage("REGISTER_EMAIL_WILL_BE_SENT")?></p>
				<?
			}
		?>
		<form action="<?=POST_FORM_ACTION_URI?>" class="b-form b-form_common b-regform" method="post" name="regform" enctype="multipart/form-data" id="regform">
			<input type="hidden" name="AJAX_RESULT" value="Y">
			<?
				if($arResult["BACKURL"] <> ''){
				?>
				<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
				<?
				}
			?>
			<!--input type="hidden" name="REGISTER[LOGIN]" id="LOGIN" value="" /-->

			<p class="note"><?=Loc::getMessage('MAIN_REGISTER_IMPORTANT');?></p>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock left-50">
					<em class="hint">*</em>
					<label for="settings02"><?=Loc::getMessage('MAIN_REGISTER_FIRSTNAME');?></label>
					<div class="field validate">
						<input type="text" tabindex="1" data-required="required" data-validate="fio" value="<?=$arResult["VALUES"]['NAME']?>" id="settings02" data-maxlength="30" data-minlength="2" name="REGISTER[NAME]" class="input" >										
						<em class="error required"><?=Loc::getMessage('MAIN_REGISTER_REQ');?></em>		
						<em class="error validate"><?=Loc::getMessage('MAIN_REGISTER_WRONG_FORMAT_CLEAR');?></em>	
						<em class="error maxlength"><?=Loc::getMessage('MAIN_REGISTER_MORE_30_SYMBOLS');?></em>		
					</div>
				</div>
				<div class="fieldcell iblock right-50">
					<em class="hint">*</em>
					<label for="settings04"><?=Loc::getMessage('MAIN_REGISTER_EMAIL');?></label>
					<div class="field validate">
						<input type="text" class="input" name="REGISTER[EMAIL]" data-minlength="2" data-maxlength="50" id="settings04" data-validate="email" value="<?=$arResult["VALUES"]['EMAIL']?>" tabindex="5" data-required="true">
						<em class="error validate"><?=Loc::getMessage('MAIN_REGISTER_EMAIL_FORMAT');?></em>
						<em class="error required"><?=Loc::getMessage('MAIN_REGISTER_REQ');?></em>
					</div>
				</div>
			</div>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock left-50">
					<em class="hint">*</em>
					<label for="settings01"><?=Loc::getMessage('MAIN_REGISTER_LASTNAME');?></label>
					<div class="field validate">
						<input type="text" data-validate="fio" tabindex="2" data-required="required" value="<?=$arResult["VALUES"]['LAST_NAME']?>" id="settings01" data-maxlength="30" data-minlength="2" name="REGISTER[LAST_NAME]" class="input">										
						<em class="error required"><?=Loc::getMessage('MAIN_REGISTER_REQ');?></em>		
						<em class="error validate"><?=Loc::getMessage('MAIN_REGISTER_WRONG_FORMAT_CLEAR');?></em>	
						<em class="error maxlength"><?=Loc::getMessage('MAIN_REGISTER_MORE_30_SYMBOLS');?></em>	
					</div>
				</div>
				<div class="fieldcell iblock right-50">
					<em class="hint">*</em>
					<label for="settings05"><?=Loc::getMessage('REGISTER_FIELD_PASSWORD');?> (<?=Loc::getMessage('MAIN_REGISTER_MIN_6S');?>)</label>
					<div class="field validate">
						<input type="password" data-validate="password" tabindex="6" data-required="true" value="<?=$arResult["VALUES"]['PASSWORD']?>" id="settings05" data-maxlength="30" data-minlength="6" name="REGISTER[PASSWORD]" class="pass_status input">
						<em class="error required"><?=Loc::getMessage('MAIN_REGISTER_REQ');?></em>
						<em class="error validate"><?=Loc::getMessage('MAIN_REGISTER_PASSWORD_LENGTH_MIN');?></em>
						<em class="error maxlength"><?=Loc::getMessage('MAIN_REGISTER_PASSWORD_LENGTH_MAX');?></em>
					</div>
				</div>
			</div>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock left-50">
					<label for="settings22"><?=Loc::getMessage('MAIN_REGISTER_MIDNAME');?></label>
					<div class="field validate">
						<input type="text" data-validate="fio" value="<?=$arResult["VALUES"]['SECOND_NAME']?>" id="settings22" data-maxlength="30" data-minlength="2" tabindex="3" data-required="false"  name="REGISTER[SECOND_NAME]" class="input" >										
						<em class="error required"><?=Loc::getMessage('MAIN_REGISTER_REQ');?></em>
						<em class="error validate"><?=Loc::getMessage('MAIN_REGISTER_WRONG_FORMAT_CLEAR');?></em>
						<em class="error maxlength"><?=Loc::getMessage('MAIN_REGISTER_MORE_30_SYMBOLS');?></em>
					</div>
				</div>
				<div class="fieldcell iblock right-50">
					<em class="hint">*</em>
					<label for="settings55"><?=Loc::getMessage('MAIN_REGISTER_CONFIRM_PASSWORD');?></label>
					<div class="field validate">
						<input data-identity="#settings05" type="password" tabindex="7" data-required="true" value="<?=$arResult["VALUES"]['CONFIRM_PASSWORD']?>" id="settings55" data-maxlength="30" data-minlength="2" name="REGISTER[CONFIRM_PASSWORD]" class="input" data-validate="password">								
						<em class="error identity "><?=Loc::getMessage('MAIN_REGISTER_PASSWORD_MISMATCH');?></em>
						<em class="error required"><?=Loc::getMessage('MAIN_REGISTER_REQ');?></em>
						<em class="error validate"><?=Loc::getMessage('MAIN_REGISTER_PASSWORD_LENGTH_MIN');?></em>
						<em class="error maxlength"><?=Loc::getMessage('MAIN_REGISTER_PASSWORD_LENGTH_MAX');?></em>
					</div>
				</div>
			</div>

			<div class="fieldrow fieldrowaction">
				<div class="fieldcell iblock left-50">
					<em class="hint">*</em>
					<label for="settings02">Логин</label>
					<div class="field validate">
						<input type="text" tabindex="4" data-required="required" data-validate="fio" value="<?=$arResult["VALUES"]['LOGIN']?>" id="settings02" data-maxlength="30" data-minlength="2" name="REGISTER[LOGIN]" class="input" >
						<em class="error required"><?=Loc::getMessage('MAIN_REGISTER_REQ');?></em>		
						<em class="error validate"><?=Loc::getMessage('MAIN_REGISTER_WRONG_FORMAT_CLEAR');?></em>	
						<em class="error maxlength"><?=Loc::getMessage('MAIN_REGISTER_MORE_30_SYMBOLS');?></em>		
					</div>
				</div>
				<div class="fieldcell iblock right-50">
					<div class="field clearfix">
						<span class="rel tooltip_box">
							<button type="submit" value="1" class="formbutton bt_typelt"><?=Loc::getMessage('MAIN_REGISTER_REGISTER');?></button>
							<input type="hidden" name="register_submit_button" value="<?=GetMessage("AUTH_REGISTER")?>" />
							<div class="tooltip">
								<p>Вы не можете зарегистрироваться, не прочитав правила использования</p>
							</div>
						</span>
					</div>
					<div class="checkwrapper">
						<?=Loc::getMessage('MAIN_REGISTER_AGREE_FOR_POPUP');?> <a href="/local/templates/evk/components/neb/registration/.default/ajax_agreement.php" class="open_agreement"><?=Loc::getMessage('MAIN_REGISTER_AGREE_FOR_POPUP_TERMS');?></a>
						<?//popup_opener ajax_opener closein?>
					</div>
				</div>
			</div>
		</form>
	</div><!-- /.b-registration-->
</section>
<?
if(isset($_REQUEST["AJAX_RESULT"]) && $_REQUEST["AJAX_RESULT"] == "Y")
	exit;
?>