<?
define("STOP_STATISTICS", true);
define("NOT_CHECK_PERMISSIONS", true);
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

CModule::IncludeModule("evk.books");
use Evk\Books\SearchQuery;
use \Bitrix\Main\Localization\Loc as Loc;

Loc::loadMessages(__FILE__);
Loc::loadMessages(__DIR__.'/autocomplete.php');
$q = trim(htmlspecialcharsEx($_REQUEST['term']));
if(empty($q))
	exit();
if(check_bitrix_sessid())
{
	header('Content-type: application/json');
	$arResult = SearchQuery::GetAutoComplete("AUTHOR", $q);
	if(!empty($arResult))
		echo json_encode($arResult);
}
?>