<?
	if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
		die();
	use \Bitrix\Main\Localization\Loc as Loc;
	Loc::loadMessages(__FILE__);
?>
<section class="mainsection" >
	<form action="<?=!empty($arParams['ACTION_URL'])? $arParams['ACTION_URL'] : '/search/'?>" class="searchform<?=($APPLICATION->GetCurDir()=="/search/") ? " visible":""?>">
		<?
		if(!empty($_REQUEST['pagen'])){?><input type="hidden" name="pagen" value="<?=(int)$_REQUEST['pagen']?>"/><?}
		if(!empty($_REQUEST['longpage'])){?><input type="hidden" name="longpage" value="y"/><?}
		if(!empty($_REQUEST['debug'])){?><input type="hidden" name="debug" value="1"/><?}
		if($arParams["IS_ADD_BOOK_PAGE"] && $arResult["s_neb"]){?><input type="hidden" name="s_neb" value="1"/><?}?>


		<?foreach($arParams['REQUEST_PARAMS'] as $k => $v):?>
			<input type="hidden" name="<?=$k?>" value="<?=$v?>"/>
		<?endforeach;?>
		<div class="b-search wrapper rel">
			<div class="b-search_field">
				<div class="clearfix b-search_def">
					<div class="b-search_bth"><input type="submit" value=""></div>
					<span class="b-search_fieldbox">
						<input type="text" name="q" class=" bbox b-search_fieldtb" id="asearch" placeholder="<?=htmlspecialchars($arParams["PLACEHOLDER"]);?>" autocomplete="off" data-src="<?=$this->__folder.'/autocomplete.php?'.bitrix_sessid_get()?>" value="<?=htmlspecialcharsEx($_REQUEST['q'])?>">
					</span>
					<?if($APPLICATION->GetCurDir()=="/search/"):?>
						<a href="#" class="b-searchmode js_extraform" data-extra="Расширенный поиск" data-default="Скрыть поиск" >Скрыть поиск</a>
					<?else:?>
						<a href="#" class="b-searchmode js_extraform" data-extra="Скрыть поиск" data-default="Расширенный поиск" >Расширенный поиск</a>
					<?endif;?>
				</div>
			</div>
			<div class="b-search_ex">
				<div class="wrapper rel bbox">
					<div class="b_search_set clearfix">
						<div class="checkwrapper">
							<input class="checkbox js_btnsub" type="checkbox" name="is_full_search"<?if($arResult["is_full_search"]) echo ' checked="checked"'?> id="cb3" value="Y">
							<label for="cb3"><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_FOR_FULL_TEXT'); ?></label>
						</div>
						<div class="checkwrapper">
							<input class="checkbox js_btnsub" type="checkbox" name="s_strict"<?if($arResult["s_strict"]) echo ' checked="checked"'?> id="cb4" value="Y">
							<label for="cb4" ><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_STRICT_SEARCH'); ?></label>
						</div>
						<?if(!$arParams["IS_ADD_BOOK_PAGE"]):?>
							<div class="checkwrapper">
								<input class="checkbox js_btnsub" type="checkbox" name="s_neb"<?if($arResult["s_neb"]) echo ' checked="checked"'?> id="cb5" value="Y">
								<label for="cb5" ><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_NEB_SEARCH'); ?></label>
								<div class="hint">
									<div class="tooltip">
										<p>При выборе данной опции поисковый запрос будет осуществлён в каталоге Национальной электронной библиотеки, которая объединяет фонды публичных библиотек России федерального, регионального, муниципального уровня, библиотек научных и образовательных учреждений, а также правообладателей</p>
									</div>
								</div>
							</div>
						<?endif;?>
					</div> <!-- /.b_search_set -->
					<?php foreach($arResult["arLogic"] as $key=>$logic):?>
						<div class="b_search_row visiblerow">
							<?php if($key==0):?>
								<select name="<?=$logic["logic"]["fieldName"]?>" id="" class="js_select ddl_logic ">
									<option value="AND"<?if($logic["logic"]["fieldValue"] == "AND") echo ' selected="selected"';?>>&nbsp;</option>
									<option value="NOT"<?if($logic["logic"]["fieldValue"] == "NOT") echo ' selected="selected"';?>><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_NOT'); ?></option>
								</select>
							<?php else:?>
								<select name="<?=$logic["logic"]["fieldName"]?>" id="" class="js_select ddl_logic ">
									<option value="OR"<?if($logic["logic"]["fieldValue"] == "OR") echo ' selected="selected"';?>><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_OR'); ?></option>
									<option value="AND"<?if($logic["logic"]["fieldValue"] == "AND") echo ' selected="selected"';?>><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_AND'); ?></option>
									<option value="OR_NOT"<?if($logic["logic"]["fieldValue"] == "OR_NOT") echo ' selected="selected"';?>><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_OR_NOT'); ?></option>
									<option value="AND_NOT"<?if($logic["logic"]["fieldValue"] == "AND_NOT") echo ' selected="selected"';?>><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_AND_NOT'); ?></option>
								</select>
							<?php endif;?>
							<select name="<?=$logic["theme"]["fieldName"]?>" class="js_select ddl_theme">
								<option value="ALL"<?if($logic["theme"]["fieldValue"] == "ALL") echo ' selected="selected"';?>><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_ALL'); ?></option>
								<option value="authorbook"<?if($logic["theme"]["fieldValue"] == "authorbook") echo ' selected="selected"';?> data-asrc="<?=$this->__folder.'/authors.php?'.bitrix_sessid_get()?>"><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_AUTHORBOOK'); ?></option>
								<option value="title"<?if($logic["theme"]["fieldValue"] == "title") echo ' selected="selected"';?> data-asrc="<?=$this->__folder.'/title.php?'.bitrix_sessid_get()?>"><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_TITLE'); ?></option>
								<option value="publisher"<?if($logic["theme"]["fieldValue"] == "publisher") echo ' selected="selected"';?>><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_PUBLISHER'); ?></option>
								<option value="publishplace"<?if($logic["theme"]["fieldValue"] == "publishplace") echo ' selected="selected"';?>><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_PUBLISHPLACE'); ?></option>
								<option value="library"<?if($logic["theme"]["fieldValue"] == "library") echo ' selected="selected"';?> data-asrc="<?=$this->__folder.'/libraries.php?'.bitrix_sessid_get()?>"><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_LIBRARY'); ?></option>
							</select>
							<input type="text" name="<?=$logic["text"]["fieldName"]?>" class="b-text b_search_txt" value="<?=$logic["text"]["fieldValue"]?>" >
							<?php if($key == 0):?>
								<a href="#" class="b-searchaddrow"><span class="b-searchaddrow_plus">+</span><span class="b-searchaddrow_lb js_addsearchrow">Добавить условие</span></a>
							<?php else:?>
								<a href="#" class="b-searchdelrow b-searchaddrow"><span class="b-searchaddrow_minus">-</span><span class="b-searchaddrow_lb js_delsearchrow">Удалить условие</span></a>
							<?php endif;?>
						</div>
					<?php endforeach;?>

					<div class="b_search_row js_search_row hidden" >
						<select name="logic[]" disabled="disabled" class="ddl_logic">
							<option value="OR"<?if($logic["logic"]["fieldValue"] == "OR") echo ' selected="selected"';?>><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_OR'); ?></option>
							<option value="AND"<?if($logic["logic"]["fieldValue"] == "AND") echo ' selected="selected"';?>><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_AND'); ?></option>
							<option value="OR_NOT"<?if($logic["logic"]["fieldValue"] == "OR_NOT") echo ' selected="selected"';?>><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_OR_NOT'); ?></option>
							<option value="AND_NOT"<?if($logic["logic"]["fieldValue"] == "AND_NOT") echo ' selected="selected"';?>><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_AND_NOT'); ?></option>
						</select>
						<select name="theme[]" disabled="disabled" id="" class="ddl_theme">
							<option value="ALL"><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_ALL'); ?></option>
							<option value="authorbook" data-asrc="<?=$this->__folder.'/authors.php?'.bitrix_sessid_get()?>"><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_AUTHORBOOK'); ?></option>
							<option value="title" data-asrc="<?=$this->__folder.'/title.php?'.bitrix_sessid_get()?>"><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_TITLE'); ?></option>
							<option value="publisher"><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_PUBLISHER'); ?></option>
							<option value="publishplace"><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_PUBLISHPLACE'); ?></option>
							<option value="library" data-asrc="<?=$this->__folder.'/libraries.php?'.bitrix_sessid_get()?>"><?=Loc::getMessage('LIB_SEARCH_FORM_TEMPLATE_LIBRARY'); ?></option>
						</select>
						<input type="text" disabled="disabled" name="text[]" class="b-text b_search_txt">
						<a href="#" class="b-searchdelrow b-searchaddrow"><span class="b-searchaddrow_minus">-</span><span class="b-searchaddrow_lb js_delsearchrow">Удалить условие</span></a>
					</div>

					<div class="b_search_date">
						<div class="b_search_datelb ">Годы публикации:</div>
						<input class="b-text" type="text" id="js_searchdate_prev" name="publishyear_prev" value="<?=intval($_REQUEST['publishyear_prev']) > 0 ? (int)$_REQUEST['publishyear_prev']:SEARCH_BEGIN_YEAR;?>"  />
						<div class="b_searchslider iblock js_searchslider"></div>
						<input class="b-text"  type="text" id="js_searchdate_next" name="publishyear_next" value="<?=intval($_REQUEST['publishyear_next']) > 0 ? (int)$_REQUEST['publishyear_next']:date('Y');?>" />
						<div class="b-search_sliderdate">
							<?/*/?>
							<span>XI</span>
							<span>XII</span>
							<span>XIII</span>
							<span>XIV</span>
							<span>XV</span>
							<span>XVI</span>
							<span>XVII</span>
							<span>XVIII</span>
							<span>XIX</span>
							<span>1900</span>
							<span>1910</span>
							<span>1920</span>
							<span>1930</span>
							<span>1940</span>
							<span>1950</span>
							<span>1960</span>
							<span>1970</span>
							<span>1980</span>
							<span>1990</span>
							<span>2000</span>
							<span><?=date("Y")?></span>
							<?/*/?>
						</div>
					</div>

				</div>
			</div><!-- /.b-search_ex -->
		</div> <!-- /.b-search -->
	</form>
</section>