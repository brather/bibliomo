<?
define("STOP_STATISTICS", true);
define("NOT_CHECK_PERMISSIONS", true);
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("evk.books");
use Bitrix\Main\Localization\Loc as Loc;
use Evk\Books\SearchQuery;

Loc::loadMessages(__FILE__);
$q = trim(htmlspecialcharsEx($_REQUEST['term']));
if(empty($q))
	exit();

if(check_bitrix_sessid())
{
	header('Content-type: application/json');
	$arAuthor = SearchQuery::GetAutoComplete("AUTHOR", $q);
	$arTitle = SearchQuery::GetAutoComplete("TITLE", $q);
	//$arLibrary = SearchQuery::GetAutoComplete("LIBRARY", $q);
	$arResult = array_merge($arAuthor, $arTitle);
	if(!empty($arResult))
		echo json_encode($arResult);
}
?>