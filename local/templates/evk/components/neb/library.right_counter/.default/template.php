<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use \Bitrix\Main\Localization\Loc as Loc;
Loc::loadMessages(__FILE__);
?>
<div class="b-onelib_side bbox">
	<?
	if(!empty($arResult['PREVIEW_PICTURE']))
	{
		?>
		<img src="<?=$arResult['PREVIEW_PICTURE']['src']?>" alt="<?=$arResult['NAME']?>"/>
		<?
	}
	?>
	<div class="b-libstatistic">
		<div class="b-libstatistic_item books">
			<div class="b-libstatistic_tit"><?=number_format($arResult['PROPERTY_PUBLICATIONS_VALUE'], 0, '', ' ')?> <?=GetEnding($arResult['PROPERTY_PUBLICATIONS_VALUE'], Loc::getMessage('LIBRARY_RIGHT_COUNTER_PUB_5'), Loc::getMessage('LIBRARY_RIGHT_COUNTER_PUB_1'), Loc::getMessage('LIBRARY_RIGHT_COUNTER_PUB_2'))?></div>
			<span><?=Loc::getMessage('LIBRARY_RIGHT_COUNTER_FUND');?></span>
		</div>
		<div class="b-libstatistic_item collection">
			<div class="b-libstatistic_tit"><?=number_format($arResult['PROPERTY_COLLECTIONS_VALUE'], 0, '', ' ')?> <?=GetEnding($arResult['PROPERTY_COLLECTIONS_VALUE'], Loc::getMessage('LIBRARY_RIGHT_COUNTER_COL_5'), Loc::getMessage('LIBRARY_RIGHT_COUNTER_COL_1'), Loc::getMessage('LIBRARY_RIGHT_COUNTER_COL_2'))?></div>
			<span>Собрала библиотека</span>
			<a href="/profile/#tab-collection" class="green tdu b-collection-link">Смотреть коллекции</a>
			<script type="text/javascript">
				$(function(){
					$('.b-collection-link').click(function(e){
						$('a.ui-tabs-anchor[href$="#tab-collection"]').click();
						e.PreventDefault();
					});
				});
			</script>
		</div>
		<div class="b-libstatistic_item reader">
			<div class="b-libstatistic_tit"><?=number_format($arResult['PROPERTY_USERS_VALUE'], 0, '', ' ')?> <?=GetEnding($arResult['PROPERTY_USERS_VALUE'], Loc::getMessage('LIBRARY_RIGHT_COUNTER_USERS_5'), Loc::getMessage('LIBRARY_RIGHT_COUNTER_USERS_1'), Loc::getMessage('LIBRARY_RIGHT_COUNTER_USERS_2'))?></div>
		</div>
	</div>
	<div class="b-libsinfo_item">
		<div class="ico_address ico_label">Адрес</div>
		<p><?=$arResult["PROPERTY_ADDRESS_VALUE"];?> <a href="#" class="green tdu nowrap">Смотреть на карте</a></p>
	</div>
	<div class="b-libsinfo_item">
		<div class="ico_graph ico_label">График работы</div>
		<div class="b-libtime">
			<div class="b-libtime_item iblock"><span class="label">Пн</span><div style="width: 100%"></div></div>
			<div class="b-libtime_item iblock"><span class="label">Вт</span><div style="width: 100%"></div></div>
			<div class="b-libtime_item iblock"><span class="label">Ср</span><div style="width: 100%"></div></div>
			<div class="b-libtime_item iblock"><span class="label">Чт</span><div style="width: 100%"></div></div>
			<div class="b-libtime_item iblock"><span class="label">Пт</span><div style="width: 100%"></div></div>
			<div class="b-libtime_item iblock"><span class="label">Сб</span><div style="width: 80%"></div></div>
			<div class="b-libtime_item iblock"><span class="label">Вс</span><div style="width: 0"></div></div>
		</div>
		<?=$arResult["~PROPERTY_SCHEDULE_VALUE"]?>
		<?/*/?>
		<div class="b-libtimeline clearfix">
			<span class="time">с 09:00 до 20:00</span>
			<span class="day">Пн - Пт:</span>
		</div>
		<div class="b-libtimeline clearfix">
			<span class="time">с 09:00 до 19:00</span>
			<span class="day">Сб:</span>
		</div>
		<div class="b-libtimeline clearfix">
			<span class="time">выходной</span>
			<span class="day">Bc:</span>
		</div>
		<?/*/?>
	</div>
	<?if(!empty($arResult["~PROPERTY_PHONE_VALUE"])):?>
		<div class="b-libsinfo_item">
			<div class="ico_phone ico_label">Телефоны</div>
			<?foreach($arResult["~PROPERTY_PHONE_VALUE"]["VALUE"] as $phone):?>
				<p><?=$phone?></p>
			<?endforeach;?>
		</div>
	<?endif;?>
	<?if(!empty($arResult["PROPERTY_EMAIL_VALUE"])):?>
		<div class="b-libsinfo_item">
			<div class="ico_mail ico_label">Электронная почта</div>
			<a href="#" class="green tdu"><?=$arResult["PROPERTY_EMAIL_VALUE"]?></a>
		</div>
	<?endif;?>
	<?if(!empty($arResult["~PROPERTY_CONTACTS_VALUE"])):?>
		<div class="b-libsinfo_item">
			<div class="ico_contact ico_label">Контактное лицо</div>
			<?foreach($arResult["~PROPERTY_CONTACTS_VALUE"]["VALUE"] as $key=>$contact):?>
			<div class="b-contact_name"><?=$contact?></div>
			<div class="b-contact_status"><?=$arResult["~PROPERTY_CONTACTS_VALUE"]["DESCRIPTION"][$key]?></div>
			<?endforeach;?>
		</div>
	<?endif?>
</div><!-- /.b-onelib_side -->
<?/*/?>
<div class="b-libfond <?=$arParams['CLASS']?>">
	<?
		if(!empty($arResult['PREVIEW_PICTURE']))
		{
		?>
		<div class="b-fond_img">
			<img src="<?=$arResult['PREVIEW_PICTURE']['src']?>" alt="<?=$arResult['NAME']?>">
		</div>
		<?
		}
	?>
	<?
		if(!empty($arResult['PROPERTY_PUBLICATIONS_VALUE']))
		{
		?>
		<span class="b-fondinfo_number_lb"><?=Loc::getMessage('LIBRARY_RIGHT_COUNTER_FUND');?></span>
		<span class="b-portalinfo_numbers">
			<?=getNumberNeb($arResult['PROPERTY_PUBLICATIONS_VALUE']);?>
		</span>
		<span class="b-fondinfo_number_lb"><?=GetEnding($arResult['PROPERTY_PUBLICATIONS_VALUE'], Loc::getMessage('LIBRARY_RIGHT_COUNTER_PUB_5'), Loc::getMessage('LIBRARY_RIGHT_COUNTER_PUB_1'), Loc::getMessage('LIBRARY_RIGHT_COUNTER_PUB_2'))?></span>
		<?
		}
	?>
	<?
		if(!empty($arResult['PROPERTY_COLLECTIONS_VALUE']))
		{
			?>
			<div class="b-lib_collect">
				<span class="b-fondinfo_number_lb"><?=Loc::getMessage('LIBRARY_RIGHT_COUNTER_INC');?></span>
				<span class="b-portalinfo_numbers">
					<?=getNumberNeb($arResult['PROPERTY_COLLECTIONS_VALUE']);?>
				</span>
				<span class="b-fondinfo_number_lb"><?=GetEnding($arResult['PROPERTY_COLLECTIONS_VALUE'], Loc::getMessage('LIBRARY_RIGHT_COUNTER_COL_5'), Loc::getMessage('LIBRARY_RIGHT_COUNTER_COL_1'), Loc::getMessage('LIBRARY_RIGHT_COUNTER_COL_2'))?></span>
				<?
					if(!empty($arParams['COLLECTION_URL']))
					{
					?>
					<a href="<?=$arParams['COLLECTION_URL']?>" class="button_mode button_revers"><?=Loc::getMessage('LIBRARY_RIGHT_COUNTER_VIEW_ALL');?></a>
					<?
					}
				?>
			</div>
			<?
		}
	?>
	<?
		if(!empty($arResult['PROPERTY_USERS_VALUE']))
		{
		?>
		<div class="b-lib_counter">
			<div class="b-lib_counter_lb"><?=Loc::getMessage('LIBRARY_RIGHT_COUNTER_READER');?></div>
			<div class="icouser">х <?=number_format($arResult['PROPERTY_USERS_VALUE'], 0, '', ' ')?></div>
		</div>
		<?
		}
	?>
	<?
		if(!empty($arResult['PROPERTY_VIEWS_VALUE']))
		{
		?>
		<div class="b-lib_counter">
			<div class="b-lib_counter_lb"><?=Loc::getMessage('LIBRARY_RIGHT_COUNTER_VIEWERS');?></div>
			<div class="icoviews">х <?=number_format($arResult['PROPERTY_VIEWS_VALUE'], 0, '', ' ')?></div>
		</div>
		<?
		}
	?>
</div>
<?/*/?>
