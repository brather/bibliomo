<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use \Bitrix\Main\Localization\Loc as Loc;
Loc::loadMessages(__FILE__);?>
<div class="wrapper">
	<section class="b-inner clearfix map-inner">
		<div class="clearfix onelib js_oneheight">
			<span id="collection_side_after"></span>
			<div id="collection_side" class="b-onelib_side bbox">
				<div class="b-onelib_side_title-libstatistic">
					<img class="b-onelib_side_title-image" src="<?=$arResult['SECTION']['PICTURE']?>" />
					<ul>
						<li class="b-libstatistic-dnf books">
							<span><?=$arResult['SECTION']['CNT']?> <?=GetEnding($arResult['SECTION']['CNT'], Loc::getMessage('COLLECTION_DETAIL_BOOK_5'), Loc::getMessage('COLLECTION_DETAIL_BOOK_1'), Loc::getMessage('COLLECTION_DETAIL_BOOK_2'))?> <?=Loc::getMessage('COLLECTION_DETAIL_IN_COL'); ?></span>
							<!--dnf>Обновлено 22.12.2015</dnf-->
						</li>
						<li class="b-libstatistic-dnf favorites">
							<span><?=intval($arResult['SECTION']['UF_ADDED_FAVORITES'])?> добавили в избранное</span>
							<!--dnf>Последний раз 22.12.2015</dnf-->
						</li>
						<li class="b-libstatistic-dnf views">
							<span><?=intval($arResult['SECTION']['UF_VIEWS'])?> <?=GetEnding($arResult['SECTION']['UF_VIEWS'], 'просмотров', 'просмотр', 'просмотра');?></span>
							<!--dnf>За 2 года 134 дня</dnf-->
						</li>
					</ul>
				</div>
				<?$APPLICATION->IncludeComponent(
					"neb:catalog.smart.filter",
					"",
					Array(
						"COMPONENT_TEMPLATE" => ".default",
						"IBLOCK_TYPE" => "library",
						"IBLOCK_ID" => "10",
						"SECTION_ID" => "",
						"SECTION_CODE" => "",
						"FILTER_NAME" => $arParams["FILTER_NAME"],
						"HIDE_NOT_AVAILABLE" => "N",
						"TEMPLATE_THEME" => "blue",
						"FILTER_VIEW_MODE" => "vertical",
						"ITEMS" => $arResult["PROPS_ITEMS"],
						"ALL_ITEM_IDS" => $arResult["ALL_ITEM_IDS"],
						"CACHE_TYPE" => "A",
						"CACHE_TIME" => $arParams["CACHE_TIME"],
						"CACHE_GROUPS" => "Y",
						"SAVE_IN_SESSION" => "N",
						"INSTANT_RELOAD" => "N",
						"PRICE_CODE" => array(),
						"XML_EXPORT" => "N",
						"SECTION_TITLE" => "-",
						"SECTION_DESCRIPTION" => "-",
						"POPUP_POSITION" => "left",
						"AJAX" => $arParams["AJAX"],
					),
					$component
				);?>
			</div><!-- /.b-onelib_side -->
			<div class="b-onelib_info b-inner b-titlefz">
				<h1><?=$arResult["SECTION"]["NAME"]?></h1>
				<form class="b-collection_search">
					<input type="text" class="b-collection_tb iblock" placeholder="Поиск книги по названию или (и) автору" name="qbook" value="<?=$_REQUEST["qbook"]?>">
					<input type="submit" class="b-collection_bt bt_typelt iblock" value="Искать">
					<select class="js_select" name="search_by">
						<option value="author" <?=($_REQUEST["search_by"] == "author" ? "selected='selected'" : "")?>>по автору</option>
						<option value="title" <?=($_REQUEST["search_by"] == "title" ? "selected='selected'" : "")?>>по названию</option>
					</select>
				</form>

				<?if (!empty($arResult["ITEMS"])) {?>
					<ul class="b-ebooklist">
						<?foreach ($arResult["ITEMS"] as $arItem){?>
							<li class="iblock">
								<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="b-ebook">
									<?if (!empty($arItem["IMAGE_URL"]))
										$src_image = $arItem["IMAGE_URL"];
									else
										$src_image = "/local/images/book_empty.jpg";?>
									<img src="<?=$src_image?>" alt="<?=$arItem["NAME"]?>">
									<div class="b-ebooktitle"><?=$arItem["NAME"]?></div>
									<div class="b-ebook_autor">
										<span class="b-ebook_year"><?=$arItem["PROPERTY_PUBLISH_YEAR_VALUE"]?></span>
										<span class="b-ebook_name"><?=$arItem["PROPERTY_AUTHOR_VALUE"]?></span>
									</div>
								</a>
							</li>
						<?}?>
					</ul>
				<?} else {?>
					<p>Книги в коллекции отсутствуют</p>
				<?}?>
				<!--div class="b-wrap_more">
					<a href="ajax_addonelisb.php" class="ajax_add b-morebuttton">Показать еще</a>
				</div-->
			</div><!-- /.b-onelib_info -->
		</div><!-- /.b-clearfix -->
	</section>
</div>