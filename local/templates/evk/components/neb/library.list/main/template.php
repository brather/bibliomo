<?
	if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
	use \Bitrix\Main\Localization\Loc as Loc;
	Loc::loadMessages(__FILE__);
?>
<div class="b-map rel mappage">
	<div class="b-elar_usrs bbox">
		<div class="b-publishing_tit">
			<h3><a href="/library/">Библиотеки <sup><?=count($arResult{"LIBRARY_INFO"}["ITEMS"])?></sup></a></h3>
		</div>
		<div class="b-elar_usrs_scroll scroll-pane">
			<ol class="b-elar_usrs_list">
				<?foreach($arResult["LIBRARY_INFO"]["ITEMS"] as $arLib):?>
					<li>
						<div class="b-elar_address">
							<div class="b-elar_name iblock">
								<span class="b-elar_name_txt"><?=$arLib["NAME"]?></span>
							</div>
							<a href="#" class="b-elar_address_link" data-lat="<?=$arLib["UF_POS"][1]?>" data-lng="<?=$arLib["UF_POS"][0]?>"><?=$arLib["PROPERTY_ADDRESS_VALUE"]?></a>
						</div>
					</li>
				<?endforeach;?>
			</ol>
		</div>
	</div><!-- /.b-elar_usrs -->
	<div class="b-nebmap">
		<div class="ymap" id="ymap" data-path="<?=$templateFolder."/ajax.php";?>" data-lat="55.76" data-lng="37.64" data-zoom="8"></div>
	</div>
</div><!-- /.b-map -->
