<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/**
 * @var array $arResult
 */
$templateFolder = $this->__folder;
foreach($arResult["LIBRARY_INFO"]["ITEMS"] as &$ITEM)
{
	$file = sprintf("%s/images/%d.png", $templateFolder, $ITEM["PROPERTY_STATUS_ENUM_ID"]);

	if(file_exists($_SERVER["DOCUMENT_ROOT"].$file))
	{
		$ITEM["STATUS_ICON"] = $file;
	}
	else
	{
		$ITEM["STATUS_ICON"] = ""; // "/local/templates/.default/markup/i/neb.jpg";
	}
}
?>
