<?
	if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
	use \Bitrix\Main\Localization\Loc as Loc;
	Loc::loadMessages(__FILE__);
?>
<?if(!$arResult["JSON2"]):?>
<div class="wrapper">
	<section class="b-inner clearfix map-inner">
		<h1 class="ml">Библиотеки</h1>
		<div class="tabs b-libtabs">
			<ul class="clearfix">
				<li><a href="#tabs-1" class="ico_address">На карте</a></li>
				<li><a href="#tabs-2" class="ico_list">Списком</a></li>
			</ul>
			<div id="tabs-1">
				<div class="b-map rel mappage">
					<a name="b-ymaps-anchor"></a>
					<div class="b-elar_usrs bbox">
						<div class="b-publishing_tit">
							<h3>Библиотеки <sup><?=count($arResult{"LIBRARY_INFO"}["ITEMS"])?></sup></h3>
						</div>
						<div class="b-elar_usrs_scroll scroll-pane">
							<ol class="b-elar_usrs_list">
								<?foreach($arResult["LIBRARY_INFO"]["ITEMS"] as $arLib):?>
									<li>
										<div class="b-elar_address">
											<div class="b-elar_name iblock">
												<span class="b-elar_name_txt"><a href="<?=$arLib["DETAIL_PAGE_URL"]?>"><?=$arLib["NAME"]?></a></span>
											</div>
											<a href="#" class="b-elar_address_link" data-lat="<?=$arLib["UF_POS"][1]?>" data-lng="<?=$arLib["UF_POS"][0]?>"><?=$arLib["PROPERTY_ADDRESS_VALUE"]?></a>
										</div>
									</li>
								<?endforeach;?>
								<?/*foreach($arResult["ITEMS_PAGEN"] as $arLib):?>
									<li>
										<div class="b-elar_address">
											<div class="b-elar_name iblock">
												<span class="b-elar_name_txt"><?=$arLib["UF_NAME"]?></span>
											</div>
											<a href="#" class="b-elar_address_link" data-lat="<?=$arLib["UF_POS"][1]?>" data-lng="<?=$arLib["UF_POS"][0]?>"><?=$arLib["UF_ADRESS"]?></a>
										</div>
									</li>
								<?endforeach;*/?>
							</ol>
						</div>
					</div><!-- /.b-elar_usrs -->
					<div class="b-nebmap">
						<div class="ymap" id="ymap" data-path="<?=$templateFolder."/ajax.php";?>" data-lat="55.76" data-lng="37.64" data-zoom="8"></div>
						<span class="marker_lib hidden" style="position: absolute; top: 0;left: 0; padding: 70px 30px 30px;  width: 240px;"></span>
					</div>
				</div><!-- /.b-map -->
			</div>
			<div id="tabs-2">
<?endif;?>
				<ul class="b-libslist">
					<?foreach($arResult["LIBRARY_INFO"]["ITEMS"] as $LIB):?>
						<li>
							<?if(!empty($LIB["PROPERTY_STATUS_VALUE"]) && !empty($LIB["STATUS_ICON"])):
								?>
								<a href="#" class="b-libico">
									<img src="<?=$LIB["STATUS_ICON"]?>" alt="<?=$LIB["NAME"]?>">
									<span class="b-libico_label"><?=$LIB["PROPERTY_STATUS_VALUE"]?></span>
								</a>
							<?endif;?>
							<h2><a href="<?=$LIB["DETAIL_PAGE_URL"]?>"><?=$LIB["NAME"]?></a></h2>
							<div class="b-libsinfo">
								<span class="ico_address"><?=$LIB["PROPERTY_ADDRESS_VALUE"]?>
									<a href="#b-ymaps-anchor" class="tdu" data-lat="<?=$LIB["UF_POS"][1]?>" data-lng="<?=$LIB["UF_POS"][0]?>" >Смотреть на карте</a>
								</span>
								<?if(!empty($LIB["PROPERTY_PHONE_VALUE"])):?>
									<span class="ico_phone"><?=join(",<br/>", $LIB["PROPERTY_PHONE_VALUE"])?></span>
								<?endif;?>
								<?if(!empty($LIB["PROPERTY_EMAIL_VALUE"])):?>
									<a href="#" class="ico_mail tdu"><?=$LIB["PROPERTY_EMAIL_VALUE"]?></a>
								<?endif;?>
							</div>
						</li>
					<?endforeach;?>
				</ul>
				<?if($arResult["SHOW_MORE_BUTTON"]):?>
					<div class="b-wrap_more">
						<a href="<?=$arResult["SHOW_MORE_PAGE"]?>" class="ajax_add b-morebuttton">Загрузить еще</a>
					</div>
				<?endif;?>
	<script type="text/javascript">
		$(function(){
			$('.ico_address a.tdu')
				.unbind("click")
				.click(function(){
				$('#ui-id-1').click();
				$('.b-elar_usrs_list').find('a.b-elar_address_link[data-lat="'+$(this).data("lat")+'"][data-lng="'+$(this).data("lng")+'"]').click();
			});
		});
	</script>
<?if(!$arResult["JSON2"]):?>
			</div>
		</div>
	</section>
</div>
<?endif;?>