<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<section class="innersection innerwrapper clearfix ">
	<div class="b-searchresult" style="border: medium none;">
		<?$APPLICATION->IncludeComponent("bitrix:menu", "lk_left", Array(
				"ROOT_MENU_TYPE" => "left",	// Тип меню для первого уровня
				"MENU_CACHE_TYPE" => "N",	// Тип кеширования
				"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
				"MENU_CACHE_USE_GROUPS" => "N",	// Учитывать права доступа
				"MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
					0 => "",
				),
				"MAX_LEVEL" => "2",	// Уровень вложенности меню
				"CHILD_MENU_TYPE" => "",	// Тип меню для остальных уровней
				"USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
				"DELAY" => "N",	// Откладывать выполнение шаблона меню
				"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
				),
				false
			);?>               
	</div>

	<div class="b-mainblock left mt10">
		<?
			$APPLICATION->IncludeComponent(
				"neb:books.popular",
				"lk_library",
				Array(
					"LIBRARY_ID" => $arResult['LIBRARY']["ID"],
					"ITEM_COUNT" => 12,
					"CACHE_TIME" => $arParams["CACHE_TIME"],
				),
				$component
			);
		?>
	</div><!-- /.b-mainblock -->


	<div class="b-side right mt">
		<div class="b-profile_side">
			<div class="b-profile_photo">
				<img alt="user photo" src="<?=(intval($arResult['USER']['PERSONAL_PHOTO']) > 0)? CFile::GetPath($arResult['USER']['PERSONAL_PHOTO']) :  MARKUP.'i/user_photo.png'?>">
			</div>
			<div class="b-profile_name"><?=$arResult['USER']['NAME'].' '.$arResult['USER']['LAST_NAME']?></div>
            <div class="b-profile_email"><?=$arResult['USER']['EMAIL']?></div>
			<div class="b-profile_reg">Зарегистрирован: <?=ConvertTimeStamp(MakeTimeStamp($arResult['USER']['DATE_REGISTER']))?> <?if(empty($arResult['USER']['UF_STATUS'])){?>ограниченный доступ<?}else{?>полный доступ<?}?></div>
			<ul class="b-profile_info">
				<li><a href="/profile/edit/">настройки профиля</a></li>
			</ul>
		</div>
	</div>


</section>