<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use \Bitrix\Main\Localization\Loc as Loc;
Loc::loadMessages(__FILE__);
global $APPLICATION;
?>
	<div class="wrapper">
		<section class="b-inner clearfix map-inner">
			<div class="clearfix onelib js_oneheight">
				<div class="b-onelib_info b-titlefz" style="margin-right: 0;">
					<h1><?=$arResult["USER"]["LAST_NAME"]?> <?=$arResult["USER"]["NAME"]?> <?=$arResult["USER"]["SECOND_NAME"]?></h1>
					<div class="b-libmain_info">
						<div class="b-libmain_status iblock dark">
							<?if($arResult["LIBRARY"]["PROPERTY_STATUS_ENUM_ID"] == 1):?>
								<img src="/local/templates/.default/markup/i/neb.png" alt="<?=$arResult["LIBRARY"]["PROPERTY_STATUS_VALUE"]?>">
								<span><?=$arResult["LIBRARY"]["NAME"]?></span>
							<?endif?>
						</div>
					</div>
					<div class="tabs b-onelibtabs">
                        <div class="menu-top">
						<?$APPLICATION->IncludeComponent("bitrix:menu", "lk_left", Array(
							"ROOT_MENU_TYPE" => "left",	// Тип меню для первого уровня
							"MENU_CACHE_TYPE" => "N",	// Тип кеширования
							"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
							"MENU_CACHE_USE_GROUPS" => "N",	// Учитывать права доступа
							"MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
								0 => "",
							),
							"MAX_LEVEL" => "1",	// Уровень вложенности меню
							"CHILD_MENU_TYPE" => "",	// Тип меню для остальных уровней
							"USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
							"DELAY" => "N",	// Откладывать выполнение шаблона меню
							"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
						),
							false
						);?>
                        </div>
                        <? $APPLICATION->IncludeComponent(
                            "neb:library.right_counter",
                            "",
                            Array(
                                "IBLOCK_ID" => \Evk\Books\IblockTools::getIBlockId(IBLOCK_CODE_LIBRARY),
                                "LIBRARY_ID" => $arResult['LIBRARY']["ID"],
                                "CACHE_TIME" => $arParams["CACHE_TIME"],
                            ),
                            $component
                        ); ?>
                        <div class="tabs-container" style="margin-right: 350px;">
                            <div id="tab-lk" style="display: none;">
                                <div class="b-actionmenu two">
                                    <a href="/bitrix/admin/iblock_section_edit.php?IBLOCK_ID=6&type=collections&ID=0&lang=ru&IBLOCK_SECTION_ID=&find_section_section=&from=iblock_section_admin" class="iblock bbox addcol">Добавить коллекцию</a>
                                    <a href="/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=3&type=news&ID=0&lang=ru&IBLOCK_SECTION_ID=0&find_section_section=0&from=iblock_list_admin" class="iblock bbox news">Добавить новость</a>
                                    <?/*?><a href="/profile/readers/new/" class="iblock bbox addreader">Добавить читателя</a><?*/?>
                                </div>
                                <?
                                $APPLICATION->IncludeComponent(
                                    "neb:books.popular",
                                    "lk_library",
                                    Array(
                                        "LIBRARY_ID" => $arResult['LIBRARY']["ID"],
                                        "ITEM_COUNT" => 6,
                                        "CACHE_TIME" => $arParams["CACHE_TIME"],
                                    ),
                                    $component
                                );
                                ?>
                            </div>
                            <div id="tab-orders" data-title="Заказ литературы">
                                <div class="b-ordertable" id="OrderTableId">
                                    <?$APPLICATION->IncludeComponent(
                                        "neb:orders",
                                        "library",
                                        array(
                                            // "BOOK_IBLOCK_ID" => "10",
                                            // "BOOK_IBLOCK_TYPE" => "library"
                                        ),
                                        false
                                    );?>
                                </div>
                            </div>
                            <div id="tab-statistics" style="display: none;">
                                <div class="b-profile_lkact">
                                    <? $APPLICATION->IncludeComponent(
                                    "neb:library.stat",
                                    "templatestat",
                                    Array(
                                        "DATE_FROM" => $_REQUEST['from'],
                                        "DATE_TO" => $_REQUEST['to'],
                                        "FORMAT" => $_REQUEST['form'],
                                        "ITEM_COUNT" => 10
                                    ),
                                    false);?>
                                </div>
                            </div>
                            <div id="tab-funds" style="display: none;">
                                <? $APPLICATION->IncludeComponent(
                                "neb:library.funds.manage",
                                "",
                                Array(
                                    "LIBRARY_ID" => $arResult['LIBRARY']['ID'],
                                    "COUNTONPAGE" => 30
                                ),
                                    false
                                );
                                ?>
                            </div>
                            <div id="tab-collection" style="display: none;">
                                <?
                                $APPLICATION->IncludeComponent(
                                    "neb:collections.list",
                                    "library_profile",
                                    array(
                                        "PAGE_URL" => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['section'],
                                        "CACHE_TYPE" => "A",
                                        "CACHE_TIME" => "3600",
                                        "IBLOCK_ID" => IBLOCK_ID_COLLECTION,
                                        "LIBRARY_ID" => $arResult['LIBRARY']['ID'],
                                        "SHOW_EMPTY_COLLECTION" => true,
                                    ),
                                    $component
                                );
                                ?>
                            </div>
                            <? if ( CSite::InGroup( array(1,6) ) ):?>
                                <div id="tabs-5" data-title="Пользователи" class="useraccess" style="overflow: hidden;">
                                    <? $APPLICATION->IncludeComponent("elr:user.list", ".default",
                                        array(
                                            'PER_PAGE' => 10,
                                            'CACHE_TIME' => 0
                                        ),
                                        false
                                    )?>
                                </div>
                            <? endif; ?>
                            <? if (CSite::InGroup(array(1,6))) { ?>
                                <div id="tabs-6" data-title="Библиотеки" class="lib-list">
                                    <? $APPLICATION->IncludeComponent("elr:lib.list", ".default",
                                        array(
                                            'PER_PAGE' => 10,
                                            'CACHE_TIME' => 0
                                        ),
                                        false
                                    )?>
                                </div>
                            <? } ?>
                            <div id="tabs-7" data-title="Справочник услуг" class="services-list">
                                <? $APPLICATION->IncludeComponent("elr:services.list", ".default",
                                    array(
                                        'PER_PAGE' => 10,
                                        'CACHE_TIME' => 0
                                    ),
                                    $component
                                )?>
                            </div>
                        </div>
					</div><!-- /.tabs -->
				</div><!-- /.b-onelib_info -->
			</div><!-- /.b-clearfix -->
		</section>
	</div>
