<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();?>
<?
	$APPLICATION->SetTitle("Получение единого электронного читательского билета. Шаг 3 из 3");
?>
<section class="innersection innerwrapper clearfix">
	<div class="b-registration rel">
		<h2 class="mode">Регистрация. подтверждение сведений. Шаг 3 из 3</h2>
		<form action="<?=$arResult['FOLDER'].$arResult['URL_TEMPLATES']['full_access_4']?>" class="b-form b-form_common b-regform">
			<div class="descr_pay">
				<p>Подтвердить введенные Вами ранее сведения можно с помощью:</p>
				<ul class="b-commonlist">
					<li>
						<a href="#">банковской карты</a>
						<p>Для подтверждения данных банковской картой Вы будете перенаправленны на страницу патежной системы Uniteller. Данные банковской карты необходимо внести для идентификации пользователя. 
							<br/>Обращаем внимание!
							<br/>- карта должна быть выдана на ваше имя;<br/>
							- на карте будет заблокирована сумма 5 рублей 01 копейка, данная сумма будет <br/>
							- возвращена на карту в ближайшее время, окончательный срок зависит от банка;<br/>
							запись в РГБ осущетвляется бесплатно!</p>
					</li>
					<li><a href="#">универсальной электронной карты</a>
						<p>Для подтверждения данных универсальной электронной карты Вы будете перенаправленны на страницу проверки данных универсальной электронной карты.</p></li>
				</ul>
			</div>
			<div class="fieldrow nowrap fieldrowaction">
				<div class="field clearfix">
					<a href="<?=$arResult['FOLDER'].$arResult['URL_TEMPLATES']['full_access_2']?>" class="button_back">Назад</a>
					<a href="<?=$arResult['FOLDER'].$arResult['URL_TEMPLATES']['full_access_4']?>" class="formbutton bankcard">Банковская карта</a>
					<a href="<?=$arResult['FOLDER'].$arResult['URL_TEMPLATES']['full_access_4']?>" class="formbutton unicard">Универсальная электронная карта</a>
				</div>
			</div>
			<p class="savetxt"><a href="/">Сохранить</a> и перейти на портал без подтверждения</p>
		</form>
	</div><!-- /.b-registration-->



</section>