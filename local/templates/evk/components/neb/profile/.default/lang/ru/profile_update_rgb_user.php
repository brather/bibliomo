<?php
$MESS["UPDATE_RGB_TITLE"] = 'Обновление информации на портале';
$MESS["UPDATE_RGB_TITLE_BROWSER"] = 'Дополнение персональных данных РГБ';
$MESS["UPDATE_RGB_LAST_NAME"] = 'Фамилия';
$MESS["UPDATE_RGB_NAME"] = 'Имя';
$MESS["UPDATE_RGB_SECOND_NAME"] = 'Отчество';
$MESS["UPDATE_RGB_CARD_NUMBER"] = 'Номер читательского билета РГБ';
$MESS["UPDATE_RGB_PASSWORD_LK_RGB"] = 'Пароль от личного кабинета РГБ';
$MESS["UPDATE_RGB_CONFIRM_PASSWORD"] = 'Подтвердить пароль';
$MESS["UPDATE_RGB_DATA_NOT_FOUND"] = 'Данные по номеру читательского билета не найдены в РГБ';
$MESS["UPDATE_RGB_FILL_IN"] = 'Заполните';
$MESS["UPDATE_RGB_INVALID_FORMAT"] = 'Неверный формат';
$MESS["UPDATE_RGB_MORE_THAN_30_CHAR"] = 'Более 30 символов';
$MESS["UPDATE_RGB_AT_LEAST_6_CHAR"] = 'Пароль менее 6 символов';
$MESS["UPDATE_RGB_PASSWORD_MORE_THAN_30"] = 'Пароль более 30 символов';
$MESS["UPDATE_RGB_PASSWORD_MISMATCH"] = 'Пароль не совпадает с введенным ранее';
$MESS["UPDATE_RGB_AGREE_FOR_POPUP"] = 'Прежде чем зарегистрироваться, необходимо ознакомиться с';
$MESS["UPDATE_RGB_AGREE_FOR_POPUP_TERMS"] = 'правилами использования';
$MESS["UPDATE_RGB_READ_TERMS"] = 'Прочтите правила использования';
$MESS["UPDATE_RGB_UPDATE"] = 'Обновить';
?>