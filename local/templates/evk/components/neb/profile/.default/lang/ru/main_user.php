<?php
    $MESS['PROFILE_MU_REGISTER'] = 'Зарегистрирован';
    $MESS['PROFILE_MU_ACCESS_R'] = 'ограниченный доступ';
    $MESS['PROFILE_MU_ACCESS_F'] = 'полный доступ';
    $MESS['PROFILE_MU_PROFILE'] = 'настройки профиля';
    $MESS['PROFILE_MU_ACCESS_GET'] = 'получить полный доступ';
	$MESS['PROFILE_MU_CLOSE_WINDOW'] = 'Закрыть окно';
	$MESS['PROFILE_MU_ACCESS_QUESTION'] = 'Доступ к закрытому изданию возможен только для пользователей, прошедших полную регистрацию. Перейти к регистрации?';	
	$MESS['PROFILE_MU_YES'] = 'Да';	
	$MESS['PROFILE_MU_NO'] = 'Нет';	
    $MESS['PROFILE_MU_VIEW_ALL_MESSAGES'] = 'Мои сообщения';
    $MESS['PROFILE_MU_NUMBER_LC'] = 'Номер ЕЭЧБ';
?>