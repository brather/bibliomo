<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
?>
<section class="innersection innerwrapper clearfix ">
	
	<div class="b-searchresult">
		<? $APPLICATION->IncludeComponent("bitrix:menu", "lk_left", Array(
				"ROOT_MENU_TYPE" => "left",	// Тип меню для первого уровня
				"MENU_CACHE_TYPE" => "N",	// Тип кеширования
				"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
				"MENU_CACHE_USE_GROUPS" => "N",	// Учитывать права доступа
				"MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
					0 => "",
				),
				"MAX_LEVEL" => "2",	// Уровень вложенности меню
				"CHILD_MENU_TYPE" => "",	// Тип меню для остальных уровней
				"USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
				"DELAY" => "N",	// Откладывать выполнение шаблона меню
				"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
				),
				false
			); ?>        
	</div><!-- /.b-searchresult-->
	
	<div class="b-breadcrumb bot">
        <h2>Фонды библиотек</h2>
        <?//<span>Разделы</span>?>

        <?/*$APPLICATION->IncludeComponent("bitrix:menu", "breadcrumbslist", Array(
                "ROOT_MENU_TYPE" => "funds",	// Тип меню для первого уровня
                "MENU_CACHE_TYPE" => "N",	// Тип кеширования
                "MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
                "MENU_CACHE_USE_GROUPS" => "N",	// Учитывать права доступа
                "MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
                    0 => "",
                ),
                "MAX_LEVEL" => "1",	// Уровень вложенности меню
                "CHILD_MENU_TYPE" => "",	// Тип меню для остальных уровней
                "USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                "DELAY" => "N",	// Откладывать выполнение шаблона меню
                "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
            ),
            false
        );*/?>
	</div><!-- /.b-breadcrumb-->
	
	<? $APPLICATION->IncludeComponent(
	"neb:library.funds.manage", 
	"", 
	Array(
		"LIBRARY_ID" => $arResult['LIBRARY']['ID'], 
		"COUNTONPAGE" => 30
	),
		false
	);
	?>
	
</section>