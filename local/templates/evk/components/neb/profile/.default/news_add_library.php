<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use \Bitrix\Main\Localization\Loc as Loc;
Loc::loadMessages(__DIR__.'/main_library.php'); // Для того, что б не копировать один и тот же текст
Loc::loadMessages(__FILE__);
?>
<section class="innersection innerwrapper clearfix ">
	<div class="b-mainblock left">
		<div class="b-searchresult">
			<?$APPLICATION->IncludeComponent("bitrix:menu", "lk_left", Array(
					"ROOT_MENU_TYPE" => "left",	// Тип меню для первого уровня
					"MENU_CACHE_TYPE" => "N",	// Тип кеширования
					"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
					"MENU_CACHE_USE_GROUPS" => "N",	// Учитывать права доступа
					"MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
						0 => "",
					),
					"MAX_LEVEL" => "2",	// Уровень вложенности меню
					"CHILD_MENU_TYPE" => "",	// Тип меню для остальных уровней
					"USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
					"DELAY" => "N",	// Откладывать выполнение шаблона меню
					"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
				),
				false
			);?>
		</div><!-- /.b-searchresult-->

		<?$APPLICATION->IncludeComponent(
			"neb:library.news.edit",
			".default",
			array(
				"IBLOCK_TYPE" => "news",
				"IBLOCK_ID" => "3",
				"NEWS_ID" => $arResult['VARIABLES']['ID'],
				"URL_LIST" => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['news'],
				"URL_DETAIL" => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['news']
			),
			false
		);?>
	</div><!-- /.b-mainblock -->
	<div class="b-side right b-sidesmall">
		<a href="<?=$arResult['LIBRARY']['DETAIL_PAGE_URL']?>" class="b-btlibpage"><?=Loc::getMessage('PROFILE_ML_PAGE'); ?></a>

		<?
		$APPLICATION->IncludeComponent(
			"neb:library.right_counter",
			"",
			Array(
				"IBLOCK_ID" => Bitrix\NotaExt\Iblock\IblockTools::getIBlockId(IBLOCK_CODE_LIBRARY),
				"LIBRARY_ID" => $arResult['LIBRARY']["ID"],
				"CACHE_TIME" => $arParams["CACHE_TIME"],
			),
			$component
		);
		?>
	</div><!-- /.b-side -->
</section>