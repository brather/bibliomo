function Md5Value(){
    var trBook;
    var tr;
    $(".trBook").each(function () {
        trBook = '';
        tr = $(this);
        $("textarea[data-val=" + $(this).attr('rel') + "], input[data-val=" + $(this).attr('rel') + "]").each(function () {
            trBook += $(this).val();
        });
        $('#md5sum_' + tr.attr('rel')).val($.md5(trBook));
    });
}
function Md5ValueCheck(id){
    var trBook;
    var tr;
    var e;
    e = false;
    $("#tr_" + id).each(function () {
        trBook = '';
        tr = $(this);
        $("textarea[data-val=" + $(this).attr('rel') + "], input[data-val=" + $(this).attr('rel') + "]").each(function () {
            trBook += $(this).val();
        });
        if($.md5(trBook) == $('#md5sum_' + tr.attr('rel')).val()){
            e = false;
        }
        else{
            e = true;
        }
    });
    return e;
}
$(document).ready(function () {
    Md5Value();
    $('body').on('click', '.inputRadio', function () {
        if (!$(this).hasClass('ClickNo')) {
            var id;
            id = $(this).attr('rel');
            if ($(this).hasClass('active')) {
                $(this).removeClass('active');
                $('#' + id).val('');
            }
            else {
                $(this).addClass('active');
                $('#' + id).val(1);
            }
        }
    });
    $('body').on('click', '.UpdateOrder', function () {
        if(Md5ValueCheck($(this).attr('rel'))){
            $.get('/ajax/UpdateOrderLib.php?id=' + $(this).attr('rel') + '&revoked=' + $('#ORDER_REVOKED_LI_' + $(this).attr('rel')).val() + '&uid=' + $('#SINGLE_USER').val() + '&date_issue=' + $('#date_fld1_' + $(this).attr('rel')).val() + '&date_return=' + $('#date_fld2_' + $(this).attr('rel')).val() + '&comment=' + $('#UF_COMMENT_'+ $(this).attr('rel')).val() + '&date=' +  $('#DESIRE_DATE_STAT_' + $(this).attr('rel')).val(), function (data) {
                $("#OrderTableId").html(data);
            });
        }
        else{
            return false;
        }
    });
    $('body').on('click', '.DelOrder', function () {
        $.get('/ajax/DelOrderLib.php?id=' + $(this).attr('rel') + '&uid=' + $('#SINGLE_USER').val(), function (data) {
            $("#OrderTableId").html(data);
            $("#resultDel").html("<h3>Заказ успешно удален.</h3>");
        })
    });
    $('body').on('click', '.UpdateOrderUser', function () {
        if(Md5ValueCheck($(this).attr('rel'))){
            $.get('/ajax/UpdateOrder.php?id=' + $(this).attr('rel') + '&revoked=' + $('#ORDER_REVOKED_US_' + $(this).attr('rel')).val() + '&desire=' + $('#date_fld2_' + $(this).attr('rel')).val(), function (data) {
                $("#OrderTableId").html(data);
            })
        }
        else{
            return false;
        }
    });

  /*  document.hideRightBlock = function(){
        if ($('div.b-onelib_side.bbox').offset().left > 0){
            $('div.b-onelib_side.bbox').hide();

        }else{
            $('div.b-onelib_side.bbox').show();

        }
    };*/

    if(location.hash == "#tabs-5" || location.hash == "#tabs-6" ){
        $('div.b-onelib_side.bbox').hide();
        $('div.tabs-container').css({marginRight: 0});
    }

    $(document).on('click', "a.ui-tabs-anchor", function(e){
        var t = $(e.currentTarget);

        if (t.attr('href') == "#tabs-5" || t.attr('href') == "#tabs-6"  ){
            $('div.b-onelib_side.bbox').hide();
            $('div.tabs-container').css({marginRight: 0});
        }else{
            $('div.b-onelib_side.bbox').show();
            $('div.tabs-container').css({marginRight: '350px'});
        }
    });
});