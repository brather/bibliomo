<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
\Bitrix\Main\Page\Asset::getInstance()->addJs(MARKUP . 'js/profile.js');
?>
<div class="wrapper">
    <section class="b-inner clearfix b-readerpo">
        <ul class="b-profile_menu">
            <li>
                <a href="/profile/edit/" class="ico_edit">Настройки профиля</a>
            </li>
            <li>
                <a href="/faq/" class="ico_help">Помощь</a>
            </li>
        </ul>
        <h1><?= $arResult['USER']['NAME'] . ' ' . $arResult['USER']['LAST_NAME'] ?></h1>
        <div class="tabs b-onelibtabs menu-top">
            <? $APPLICATION->IncludeComponent("bitrix:menu", "lk_left", Array(
                "ROOT_MENU_TYPE" => "left",    // Тип меню для первого уровня
                "MENU_CACHE_TYPE" => "N",    // Тип кеширования
                "MENU_CACHE_TIME" => "3600",    // Время кеширования (сек.)
                "MENU_CACHE_USE_GROUPS" => "N",    // Учитывать права доступа
                "MENU_CACHE_GET_VARS" => array(    // Значимые переменные запроса
                    0 => "",
                ),
                "MAX_LEVEL" => "2",    // Уровень вложенности меню
                "CHILD_MENU_TYPE" => "",    // Тип меню для остальных уровней
                "USE_EXT" => "Y",    // Подключать файлы с именами вида .тип_меню.menu_ext.php
                "DELAY" => "N",    // Откладывать выполнение шаблона меню
                "ALLOW_MULTI_SELECT" => "N",    // Разрешить несколько активных пунктов одновременно
            ),
                false
            ); ?>
            <div id="tabs-1" data-title="<?= $arResult['USER']['NAME'] . ' ' . $arResult['USER']['LAST_NAME'] ?>">
                <div class="b-sidetable">
                    <? $arReturn = $APPLICATION->IncludeComponent("neb:user.favorites", "", Array(), false); ?>
                    <? $APPLICATION->IncludeComponent("neb:search.page.viewer.right", "user_profile", Array(
                        "RESULT" => $arReturn["arResult"],
                        "PARAMS" => $arReturn["arParams"],
                    ), false); ?>
                </div>
            </div>
            <div id="tabs-2" data-title="Заказ литературы">
                <div class="b-ordertable" id="OrderTableId">
                    <? $APPLICATION->IncludeComponent(
                        "neb:orders",
                        ".default",
                        array(
                            "BOOK_IBLOCK_ID" => "10",
                            "BOOK_IBLOCK_TYPE" => "library"
                        ),
                        false
                    ); ?>
                </div>

            </div>
            <div id="tabs-3" data-title="Поисковые запросы">
                <div class="b-sidetable">
                    <div class="b-left_side">
                        <? $APPLICATION->IncludeComponent("neb:user.searches", "", Array(), false); ?>
                    </div>
                    <div class="b-right_side">
                        <? $APPLICATION->IncludeComponent("neb:user.right.menu", "",
                            Array(
                                'TYPE' => 'searches',
                                'COLLECTION_URL' => $arResult['FOLDER'] . $arResult['URL_TEMPLATES']['searches_collection'],
                                'ALL_BOOKS_URL' => $arResult['FOLDER'] . $arResult['URL_TEMPLATES']['my_library'],
                                'READING_BOOKS_URL' => $arResult['FOLDER'] . $arResult['URL_TEMPLATES']['my_library_reading'],
                                'READ_BOOKS_URL' => $arResult['FOLDER'] . $arResult['URL_TEMPLATES']['my_library_read'],
                            )
                            , false); ?>
                    </div>
                </div>
            </div>
            <? if (CSite::InGroup(array(1, 6))): ?>
                <div id="tabs-5" data-title="Пользователи" class="useraccess" style="overflow: hidden;">
                    <? $APPLICATION->IncludeComponent("elr:user.list", ".default",
                        array(
                            'PER_PAGE' => 10,
                            'CACHE_TIME' => 0
                        ),
                        false
                    ) ?>
                </div>
            <? endif; ?>
            <? if ($USER->IsAdmin()) { ?>
                <div id="tabs-6" data-title="Библиотеки" class="lib-list">
                    <? $APPLICATION->IncludeComponent("elr:lib.list", ".default",
                        array(
                            'PER_PAGE' => 10,
                            'CACHE_TIME' => 0
                        ),
                        false
                    ) ?>
                </div>
            <? } ?>
            <div id="tabs-7" data-title="Справочник услуг" class="services-list">
                <? $APPLICATION->IncludeComponent("elr:services.list", ".default",
                    array(
                        'PER_PAGE' => 10,
                        'CACHE_TIME' => 0
                    ),
                    false
                ) ?>
            </div>
        </div>
    </section>
</div>