<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<section class="innersection innerwrapper clearfix">
	<div class="b-mainblock left">
		<div class="b-searchresult">
			<?$APPLICATION->IncludeComponent("bitrix:menu", "lk_left", Array(
					"ROOT_MENU_TYPE" => "left",	// Тип меню для первого уровня
					"MENU_CACHE_TYPE" => "N",	// Тип кеширования
					"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
					"MENU_CACHE_USE_GROUPS" => "N",	// Учитывать права доступа
					"MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
						0 => "",
					),
					"MAX_LEVEL" => "2",	// Уровень вложенности меню
					"CHILD_MENU_TYPE" => "",	// Тип меню для остальных уровней
					"USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
					"DELAY" => "N",	// Откладывать выполнение шаблона меню
					"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
					),
					false
				);?>                   
		</div><!-- /.b-searchresult-->
	<div class=" rel b-adduserinner">
		<?$APPLICATION->IncludeComponent(
				"neb:main.register", 
				"lib_new_user", 
				array(
					"SHOW_FIELDS" => array(
						0 => "EMAIL",
						1 => "NAME",
						2 => "SECOND_NAME",
						3 => "LAST_NAME",
						4 => "PERSONAL_GENDER",
						5 => "PERSONAL_BIRTHDAY",
						6 => "PERSONAL_STREET",
						7 => "PERSONAL_CITY",
						8 => "PERSONAL_ZIP",
						9 => "WORK_COMPANY",
						10 => "PERSONAL_MOBILE",
						11 => "WORK_STREET",
						12 => "WORK_CITY",
						13 => "WORK_ZIP",
						14 => "PERSONAL_NOTES",
					),
					"REQUIRED_FIELDS" => array(
						0 => "EMAIL",
						1 => "NAME",
						3 => "LAST_NAME",
					),
					"AUTH" => "N",
					"LIBRARY_ID" => $arResult['LIBRARY']["ID"],
					"MESSAGE" => 'Поздравляем, вы добавлены в читатели библиотеки "'.$arResult['LIBRARY']["NAME"].'"',
					"USE_BACKURL" => "N",
					"SUCCESS_PAGE" => "",
					"SET_TITLE" => "Y",
					"USER_PROPERTY" => array(
						0 => "UF_LIBRARY",
						1 => "UF_STATUS",
						2 => "UF_PASSPORT_NUMBER",
						3 => "UF_ISSUED",
						4 => "UF_CORPUS",
						5 => "UF_STRUCTURE",
						6 => "UF_FLAT",
						7 => "UF_PASSPORT_SERIES",
						8 => "UF_BRANCH_KNOWLEDGE",
						8 => "UF_EDUCATION",
						9 => "UF_HOUSE2",
						10 => "UF_STRUCTURE2",
						11 => "UF_FLAT2",
						12 => "UF_PLACE_REGISTR",
						13 => "UF_SCAN_PASSPORT1",
						14 => "UF_SCAN_PASSPORT2",
						15 => "UF_CITIZENSHIP"
						
					),
					"USER_PROPERTY_NAME" => ""
				),
				$component
			);?>
		</div><!-- /.-->
	</div><!-- /.b-mainblock -->
	<div class="b-side right b-sidesmall">
		<a href="<?=$arResult['LIBRARY']['DETAIL_PAGE_URL']?>" class="b-btlibpage">Страница библиотеки<br>на портале</a>
		<?
			$APPLICATION->IncludeComponent(
				"neb:library.right_counter",
				"",
				Array(
					"IBLOCK_ID" => Bitrix\NotaExt\Iblock\IblockTools::getIBlockId(IBLOCK_CODE_LIBRARY),
					"LIBRARY_ID" => $arResult['LIBRARY']["ID"],
					"CACHE_TIME" => $arParams["CACHE_TIME"],
				),
				$component
			);
		?>
	</div><!-- /.b-side -->
</section>