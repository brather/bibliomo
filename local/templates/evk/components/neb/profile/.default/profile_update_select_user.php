<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use \Bitrix\Main\Localization\Loc as Loc;
Loc::loadMessages(__FILE__);
?>
<?
$APPLICATION->SetTitle("Выбор способа дополнения персональных данных");
?>

<section class="innersection innerwrapper clearfix">
    <div class="b-registration rel">
        <h2 class="mode"><?=Loc::getMessage('REG_TITLE');?></h2>
        <form action="<?=$APPLICATION->GetCurPage();?>" method="post" class="b-form b-form_common b-regform" id="regform">
            <input type="hidden" name="action" value="update_profile"/>
            <hr/>
            <div class="fieldrow nowrap radiolist">
                <div class="fieldcell iblock mt10">
                    <label for="settings12"><?=Loc::getMessage('REG_SELECT');?>:</label>
                    <div class="field validate">
                        <div class="b-radio">
                            <input type="radio" name="type" value="update" class="radio" id="rb4" checked="checked">
                            <label for="rb4"><?=Loc::getMessage('REG_STANDART');?></label>
                        </div>
                        <div class="b-radio">
                            <input type="radio" name="type" value="update_rgb" class="radio" id="rb1">
                            <label for="rb1"><?=Loc::getMessage('REG_RGB');?></label>
                        </div>
                    </div>
                </div>
            </div>
            <hr/>
            <div class="fieldrow nowrap fieldrowaction">
                <div class="fieldcell">
                    <div class="field clearfix">
                        <button class="formbutton" value="1" type="submit"><?=Loc::getMessage('REG_NEXT');?></button>
                    </div>
                </div>
            </div>
        </form>
    </div><!-- /.b-registration-->
</section>