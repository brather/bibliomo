<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<section class="innersection innerwrapper clearfix">
	<div class="b-mainblock left">
		<div class="b-searchresult">
			<?$APPLICATION->IncludeComponent("bitrix:menu", "lk_left", Array(
					"ROOT_MENU_TYPE" => "left",	// Тип меню для первого уровня
					"MENU_CACHE_TYPE" => "N",	// Тип кеширования
					"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
					"MENU_CACHE_USE_GROUPS" => "N",	// Учитывать права доступа
					"MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
						0 => "",
					),
					"MAX_LEVEL" => "2",	// Уровень вложенности меню
					"CHILD_MENU_TYPE" => "",	// Тип меню для остальных уровней
					"USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
					"DELAY" => "N",	// Откладывать выполнение шаблона меню
					"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
					),
					false
				);?>                   
		</div><!-- /.b-searchresult-->
	<div class=" rel b-adduserinner">
		<?$APPLICATION->IncludeComponent(
			"neb:main.profile.edit",
			"",
			Array(
				"USER_PROPERTY_NAME" => "",
				"SET_TITLE" => "Y",
				"AJAX_MODE" => "N",
				"USER_PROPERTY" => array(),
				"SEND_INFO" => "N",
				"LK_USER" => "Y",
				"CHECK_RIGHTS" => "N",
				"AJAX_OPTION_JUMP" => "N",
				"AJAX_OPTION_STYLE" => "Y",
				"AJAX_OPTION_HISTORY" => "N",
				"USER_ID" => $arResult["VARIABLES"]["USER_ID"],
				"LIBRARY_ID" => $arResult['LIBRARY']["ID"],
			),
			$component
		);?>
		</div><!-- /.-->
	</div><!-- /.b-mainblock -->
	<div class="b-side right b-sidesmall">
		<a href="<?=$arResult['LIBRARY']['DETAIL_PAGE_URL']?>" class="b-btlibpage">Страница библитеки<br>на портале</a>
		<?
			$APPLICATION->IncludeComponent(
				"neb:library.right_counter",
				"",
				Array(
					"IBLOCK_ID" => Bitrix\NotaExt\Iblock\IblockTools::getIBlockId(IBLOCK_CODE_LIBRARY),
					"LIBRARY_ID" => $arResult['LIBRARY']["ID"],
					"CACHE_TIME" => $arParams["CACHE_TIME"],
				),
				$component
			);
		?>
	</div><!-- /.b-side -->
</section>