<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
global $APPLICATION;
?>
<div class="wrapper">
	<section class="b-inner clearfix map-inner">
		<div class="clearfix onelib js_oneheight">
			<div class="b-onelib_info b-titlefz b-funds_add">
				<h1>Добавление книги<!--span class="h-step">Шаг 1: поиск книги в базе ЕИСУБ</span--></h1>
				<div class="b-actionmenu bookfondadd">
					<a class="button_blue bbox btadd popup_opener ajax_opener closein" data-loadtxt="загрузка..." id="byHand" href="<?=$this->__folder . '/popup_funds_add.php?'.bitrix_sessid_get()?>" data-width="920">Добавить вручную</a>
				</div>
				<base target="_self">
				<div class="addBookContent">
					<div class="b_bookfond_popup_content js_scroll">
						<?
							global $navParent;
							$navParent = '_self';
						?>
						<?
							$nebUser = new nebUser();
							$lib = $nebUser->getLibrary();
							
							CModule::IncludeModule("highloadblock"); 
							use Bitrix\Highloadblock as HL; 
							use Bitrix\Main\Entity; 

							if(!empty($lib['PROPERTY_LIBRARY_LINK_VALUE']))
							{
								$hlblock = HL\HighloadBlockTable::getById(HIBLOCK_LIBRARY)->fetch();
								$entity = HL\HighloadBlockTable::compileEntity($hlblock); 
								$entity_data_class = $entity->getDataClass();
								$rsData = $entity_data_class::getById($lib['PROPERTY_LIBRARY_LINK_VALUE']);
								$arData = $rsData->Fetch();
							}
						?>
						<?
						$APPLICATION->IncludeComponent(
							"neb:catalog.search",
							"addBook",
							Array(
								"IBLOCK_TYPE" => "library",
								"IBLOCK_ID" => "10",
								"ELEMENT_SORT_FIELD" => "sort",
								"ELEMENT_SORT_ORDER" => "asc",
								"ELEMENT_SORT_FIELD2" => "id",
								"ELEMENT_SORT_ORDER2" => "desc",
								"HIDE_NOT_AVAILABLE" => "N",
								"PAGE_ELEMENT_COUNT" => "30",
								"LINE_ELEMENT_COUNT" => "3",
								"PROPERTY_CODE" => array("ISBN","AUTHOR","LIBRARIES","PUBLISH_YEAR","PART_TITLE","PUBLISH","BBK","PUBLISH_PLACE","PART_NO","VOLUME","SERIA",""),
								"OFFERS_LIMIT" => "5",
								"SECTION_URL" => "",
								"DETAIL_URL" => "",
								"BASKET_URL" => "/personal/basket.php",
								"ACTION_VARIABLE" => "action",
								"PRODUCT_ID_VARIABLE" => "id",
								"PRODUCT_QUANTITY_VARIABLE" => "quantity",
								"PRODUCT_PROPS_VARIABLE" => "prop",
								"SECTION_ID_VARIABLE" => "SECTION_ID",
								"AJAX_MODE" => "N",
								"AJAX_OPTION_JUMP" => "N",
								"AJAX_OPTION_STYLE" => "Y",
								"AJAX_OPTION_HISTORY" => "N",
								"CACHE_TYPE" => "A",
								"CACHE_TIME" => "36000000",
								"DISPLAY_COMPARE" => "N",
								"PRICE_CODE" => array(),
								"USE_PRICE_COUNT" => "N",
								"SHOW_PRICE_COUNT" => "1",
								"PRICE_VAT_INCLUDE" => "Y",
								"PRODUCT_PROPERTIES" => array(),
								"USE_PRODUCT_QUANTITY" => "N",
								"CONVERT_CURRENCY" => "N",
								"RESTART" => "N",
								"NO_WORD_LOGIC" => "N",
								"USE_LANGUAGE_GUESS" => "Y",
								"CHECK_DATES" => "N",
								"PAGER_TEMPLATE" => ".default",
								"DISPLAY_TOP_PAGER" => "N",
								"DISPLAY_BOTTOM_PAGER" => "Y",
								"PAGER_TITLE" => "Товары",
								"PAGER_SHOW_ALWAYS" => "N",
								"PAGER_DESC_NUMBERING" => "N",
								"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
								"PAGER_SHOW_ALL" => "N",
								"DO_NOT_SHOW_TEMPLATE" => "Y",
								'ID_LIBRARY' => $arData['UF_ID']
							),
							false
						);?>
					</div>
				</div>
			</div><!-- /.b-onelib_info -->
		</div><!-- /.b-clearfix -->
	</section>
</div>
