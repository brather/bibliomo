<?require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
global $APPLICATION;
if ($USER->IsAdmin())
	$role = 'admin';
else
{
	$uobj = new nebUser();
	$role = $uobj->getRole();
}
if (!($role == 'library_admin' || $role == 'admin'))
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
if(!check_bitrix_sessid())
{
	$arErrors[] = GetMessage("IBLOCK_CML2_ACCESS_DENIED");
}
?>
<section class="innersection innerwrapper clearfix">
	<div class="b_bookfond_popup_add js_scroll">
		<h2 class="mode">Добавление книги</h2>
		<form class="b-form b-form_common b-addbookform" name="iblock_add" action="/profile/#tab-funds" target="_top" method="post" enctype="multipart/form-data">
			<?=bitrix_sessid_post()?>
			<input type="hidden" name="action" value="addBookByHand" />
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock left-50">
					<label for="AUTHOR">Автор / коллективный автор</label>
					<div class="field validate">
						<input type="text" value="" id="AUTHOR" name="Author" class="input w50p" >
					</div>
				</div>
				<div class="fieldcell iblock right-50">
					<label for="NAME"><em class="hint">*</em>Заглавие</label>
					<div class="field validate">
						<input type="text" data-required="true" value="" id="NAME" name="Name" class="input" >
						<em class="error required">Поле обязательно для заполненияо</em>
					</div>
				</div>
			</div>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock left-50">
					<label for="settings22">Заглавие части</label>
					<div class="field validate">
						<input type="text" value="" id="settings22" name="SubName" class="input" >
					</div>
				</div>
				<div class="fieldcell iblock right-50">
					<label for="settings19">Место издания</label>
					<div class="field validate">
						<input type="text" value="" id="settings19" name="PublishPlace" class="input w50p" >
					</div>
				</div>
			</div>

			<div class="fieldrow nowrap">
				<div class="fieldcell iblock left-50">
					<label for="SERIA">Серия (название, выпуск)</label>
					<div class="field validate">
						<input type="text" value="" id="SERIA" name="SERIA" class="input" >
					</div>
				</div>
				<div class="fieldcell iblock mt0 w20pc right-50">
					<label for="VOLUME">Объём</label>
					<div class="field validate">
						<input type="text" value="" id="VOLUME" name="VOLUME" class="input b-yeartb" >
					</div>
				</div>
			</div>

			<div class="fieldrow nowrap">
				<div class="fieldcell iblock left-50">
					<label for="Publisher">Издательство</label>
					<div class="field validate">
						<input type="text" value="" id="Publisher" name="Publisher" class="input" >
					</div>
				</div>
				<div class="fieldcell iblock mt0 w20pc right-50">
					<label for="YEAR">Год издания</label>
					<div class="field validate">
						<input type="text"  value="" id="YEAR" name="PublishYear" class="input b-yeartb" >
						<em class="error validate">Поле заполнено неверно</em>
					</div>
				</div>
			</div>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock w50 left-50">
					<label for="settings07">Индекс ББК</label>
					<div class="field validate">
						<input type="text" value="" id="settings07" name="BBK" class="input " >
					</div>
				</div>
				<div class="fieldcell iblock mt10 right-50">
					<label for="settings18">Международный стандартный книжный номер (ISBN)</label>
					<div class="field validate">
						<input type="text" value="" id="settings18" name="ISBN" class="input w50p" >
					</div>
				</div>
			</div>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock w50 left-50">
					<label for="settings30">Укажите номер страницы обложки</label>
					<div class="field validate">
						<input type="text" value="" id="settings30" name="CoverNumberPage" class="input " >
						<em class="error validate">Поле заполнено неверно</em>
					</div>
				</div>
				<div class="fieldcell iblock mt10 right-50 XML_ID_TEXT" style="display: none;">
					<label for="settings18">Идентификатор книги в ЕИСУБ</label>
					<div class="field validate">
						<span id="XML_ID_TEXT"></span>
						<input type="hidden" name="XML_ID" value="" id="XML_ID"/>
					</div>
				</div>
			</div>
			<div class="fieldrow nowrap">
				<div class="fieldcell iblock b-pdfadd left-50">
					<?$APPLICATION->IncludeComponent(
						"notaext:plupload",
						"fond_add_books",
						array(
							"MAX_FILE_SIZE" => "512",
							"FILE_TYPES" => "pdf",
							"LINK_ID" => "file_id2",
							'CHUNK_SIZE' => '10kb',
							"UPLOADED_FILE" => "uploaded_file2",
							"FILE_PDF" => "FILE_PDF2",
							"DIR" => "tmp_books_pdf",
							"FILES_FIELD_NAME" => "books_pdf2",
							"MULTI_SELECTION" => "N",
							"CLEANUP_DIR" => "Y",
							"UPLOAD_AUTO_START" => "Y",
							"RESIZE_IMAGES" => "N",
							"UNIQUE_NAMES" => "Y",
							"ALREADY_UPLOADED_FILE" => $arResult['FORM']['pdfLink']
						),
						false
					);?>
				</div>
				<div class="fieldcell iblock right-50">
					<div class="field">
						<button class="formbutton bt_typelt" value="1" type="submit">Разместить произведение</button>
					</div>
				</div>
			</div>
		</form>				
	</div>
</section>
