<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?
	$APPLICATION->IncludeComponent(
		"bitrix:main.profile",
		"",
		Array(
			"USER_PROPERTY_NAME" => "",
			"SET_TITLE" => "Y",
			"AJAX_MODE" => "N",
			"USER_PROPERTY" => array(),
			"SEND_INFO" => "N",
			"LK_USER" => "Y",
			"CHECK_RIGHTS" => "N",
			"AJAX_OPTION_JUMP" => "N",
			"AJAX_OPTION_STYLE" => "Y",
			"AJAX_OPTION_HISTORY" => "N"
		),
		$component
	);
?>