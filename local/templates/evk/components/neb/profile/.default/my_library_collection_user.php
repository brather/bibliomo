<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?
	$resetBoofer = false;
	if(!empty($_REQUEST['dop_filter']) and $_REQUEST['dop_filter'] == 'Y' and $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')
	{
		$APPLICATION->RestartBuffer();
		$resetBoofer = true;
	}

?>
<section class="innersection innerwrapper clearfix" id="search_page_block">
	<div class="b-searchresult noborder">
		<?$APPLICATION->IncludeComponent("bitrix:menu", "lk_left", Array(
				"ROOT_MENU_TYPE" => "left",	// Тип меню для первого уровня
				"MENU_CACHE_TYPE" => "N",	// Тип кеширования
				"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
				"MENU_CACHE_USE_GROUPS" => "N",	// Учитывать права доступа
				"MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
					0 => "",
				),
				"MAX_LEVEL" => "2",	// Уровень вложенности меню
				"CHILD_MENU_TYPE" => "",	// Тип меню для остальных уровней
				"USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
				"DELAY" => "N",	// Откладывать выполнение шаблона меню
				"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
				),
				false
			);?>                   
	</div><!-- /.b-searchresult-->
	<div class="b-mainblock left">
		<?$arReturn = $APPLICATION->IncludeComponent("neb:user.favorites", "", Array('COLLECTION_ID' => $arResult['VARIABLES']['COLLECTION_ID']),false	);?> 
	</div><!-- /.b-mainblock -->
	<div class="b-side right">
		<?$APPLICATION->IncludeComponent("neb:user.right.menu", "", 
			Array(
				'TYPE' 				=> 'books',
				'COLLECTION_ID' 	=> $arResult['VARIABLES']['COLLECTION_ID'],
				'COLLECTION_URL' 	=> $arResult['FOLDER'].$arResult['URL_TEMPLATES']['my_library_collection'],
				'ALL_BOOKS_URL' 	=> $arResult['FOLDER'].$arResult['URL_TEMPLATES']['my_library'],
				'READING_BOOKS_URL' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['my_library_reading'],
				'READ_BOOKS_URL' 	=> $arResult['FOLDER'].$arResult['URL_TEMPLATES']['my_library_read'],
			)
			,false	);?>

		<?
			$APPLICATION->IncludeComponent(
				"exalead:search.page.viewer.right",
				"",
				Array(
					"PARAMS" => $arReturn['arParams'],
					"RESULT" => $arReturn['arResult'],
				),
				$component
			);
		?>

	</div><!-- /.b-side -->

</section>

<?
	if($resetBoofer === true){
		exit();
	}
?>