<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$APPLICATION->SetTitle('Заказ литературы в библиотеке');
?>
<style type="text/css">
    .inputRadio {
        -webkit-border-radius: 10px;
        border-radius: 10px;
        background: url('/local/templates/.default/markup/i/radio.png') left bottom no-repeat;
        width: 28px;
        height: 28px;
        cursor: pointer;
    }

    .active {
        background: url('/local/templates/.default/markup/i/radio.png') no-repeat 0 0;
    }

</style>
<form method="get" class="b-collection_search clearfix">
    <input type="text" class="b-collection_tb iblock" id="asearch-orders" value="<?= $_REQUEST['q'] ?>" name="q"
           placeholder="Поиск" autocomplete="off">
    <input type="submit" class="b-collection_bt bt_typelt iblock" value="Найти">
</form>

<? if (!empty($arResult['MESSAGE'])): ?>
    <div class="message-result"><?= $arResult['MESSAGE'] ?></div>

<? endif; ?>
<? if (count($arResult['ITEMS']) > 0): ?>
    <br/>
    <table class="b-usertable b-tbscan tsize">
        <tr>
            <th class="col-author">Автор</th>
            <th>Название</th>
            <th>Дата заказа</th>
            <th class="col-center">Заказ анул. польз.</th>
            <th class="col-center">Желаемая дата получения</th>
            <th class="col-center">Подтв-e желаемой даты получения</th>
            <th class="col-center">Дата выдачи</th>
            <th class="col-center">Дата возврата</th>
            <th class="col-center">Комментарий</th>
            <th class="col-center">Заказ анул. библ.</th>
            <th class="col-center"></th>
        </tr>
        <?
        if (!empty($arResult['ITEMS'])) {
            foreach ($arResult['ITEMS'] as $arItem) {
                if (!isset($arItem['BOOK_AUTHOR']) && !isset($arItem['BOOK_TITLE']))
                    continue;
                ?>
                <tr id="tr_<?=$arItem["ID"];?>" class="trBook" rel="<?=$arItem["ID"];?>">
                    <td class="col-author"><br/><?= $arItem['BOOK_AUTHOR'] ?></td>
                    <td>
                        <br/>
                        <a class="popup_opener ajax_opener coverlay" data-width="955"
                           href="/catalog/<?= $arItem['UF_BOOK_ID'] ?>/"><?= $arItem['BOOK_TITLE'] ?></a></td>
                    <td style="width: 180px;"><br/>
                        <? if ($arItem['UF_DATE_ORDER']) {
                            echo $arItem['UF_DATE_ORDER'];
                        } else {
                            global $DB;
                            echo date($DB->DateFormatToPHP(CSite::GetDateFormat("SHORT")), time());
                        }; ?>
                    </td>
                    <td <? if ($arItem['UF_DESIRE_DATE_STAT'] == 1): ?>style="opacity: 0.7;"<?endif; ?>>
                        <input data-val="<?=$arItem["ID"];?>" type="hidden" name="type" value="<?= $arItem['UF_ORDER_REVOKED_US']; ?>"
                               id="ORDER_REVOKED_US_<?= $arItem["ID"]; ?>"/>
                        <br/>

                        <div rel="ORDER_REVOKED_US_<?= $arItem["ID"]; ?>"
                             class="inputRadio <? if ($arItem['UF_DESIRE_DATE_STAT'] == 1): ?>ClickNo<?endif; ?> <? if ($arItem['UF_ORDER_REVOKED_US']): ?>active<? endif; ?>"></div>
                    </td>
                    <td style="width: 180px; padding-left: 0px;padding-right: 0px;"><br/>
                        <? if ($arItem['UF_DESIRE_DATE_STAT'] == 1): ?>
                            <? if ($arItem['UF_DESIRE_DATE']) {
                                echo $arItem['UF_DESIRE_DATE'];
                            } else {
                                echo 'Дата не установлена';
                            }; ?>
                        <?
                        else: ?>
                            <? $APPLICATION->IncludeComponent("bitrix:main.calendar", "profile", Array(
                                "SHOW_INPUT" => "Y",    // Показывать элемент управления
                                "FORM_NAME" => "",    // Имя формы
                                "INPUT_NAME" => "date_fld2_" . $arItem["ID"],    // Имя первого поля интервала
                                "INPUT_NAME_FINISH" => "",    // Имя второго поля интервала
                                "INPUT_VALUE" => $arItem['UF_DESIRE_DATE'],    // Значение первого поля интервала
                                "INPUT_VALUE_FINISH" => "",    // Значение второго поля интервала
                                "SHOW_TIME" => "N",    // Позволять вводить время
                                "HIDE_TIMEBAR" => "Y",    // Скрывать поле для ввода времени
                                "ITEM_ID" => $arItem["ID"]
                            ),
                                false
                            ); ?>
                        <?endif; ?>
                    </td>
                    <td style="opacity: 0.7; cursor: inherit">
                        <input type="hidden" name="type" value="<?= $arItem['UF_DESIRE_DATE_STAT']; ?>"
                               id="DESIRE_DATE_STAT_<?= $arItem["ID"]; ?>"/>
                        <br/>

                        <div rel="DESIRE_DATE_STAT_<?= $arItem["ID"]; ?>"
                             class="inputRadio ClickNo <? if ($arItem['UF_DESIRE_DATE_STAT']): ?>active<? endif; ?>"></div>
                    </td>
                    <td style="width: 180px;opacity: 0.7;"><br/>
                        <? if ($arItem['UF_DATE_RETURN']) {
                            echo $arItem['UF_DATE_OF_ISSUE'];
                        } else {
                            echo 'Дата не установлена';
                        }; ?>
                    </td>
                    <td style="width: 180px;opacity: 0.7;"><br/>
                        <? if ($arItem['UF_DATE_RETURN']) {
                            echo $arItem['UF_DATE_RETURN'];
                        } else {
                            echo 'Дата не установлена';
                        }; ?>
                    </td>
                    <td style="opacity: 0.7;">
                        <br/>
                        <? if ($arItem['UF_COMMENT']) {
                            echo $arItem['UF_COMMENT'];
                        } else {
                            echo 'Отсутствует';
                        }; ?>
                    </td>
                    <td style="opacity: 0.7;">
                        <input type="hidden" name="type" value="<?= $arItem['UF_ORDER_REVOKED_LI']; ?>"
                               id="ORDER_REVOKED_LI"/>
                        <br/>

                        <div rel="ORDER_REVOKED_LI" style="cursor: inherit"
                             class="inputRadio ClickNo <? if ($arItem['UF_ORDER_REVOKED_LI']): ?>active<? endif; ?>"></div>
                    </td>
                    <td <? if ($arItem['UF_DESIRE_DATE_STAT'] == 1): ?>style="opacity: 0.7;"<?endif; ?>>
                        <br/>
                        <input type="hidden" value="" id="md5sum_<?=$arItem["ID"];?>">
                        <? if ($arItem['UF_DESIRE_DATE_STAT'] == 1): ?>
                        <a href="/ajax/errorSave.php" class="popup_opener ajax_opener closein" style="cursor: pointer">Сохранить</a></td>
                    <? else:?>
                    <a href="/ajax/okSave.php?author=<?= $arItem['BOOK_AUTHOR'] ?>&title=<?= $arItem['BOOK_TITLE'] ?>" style="cursor: pointer" class="UpdateOrderUser popup_opener ajax_opener closein" rel="<?=$arItem["ID"];?>">Сохранить</a></td>
                    <?
                    endif; ?>
                </tr>
                <?
            }
        }
        ?>
    </table>
<? else: ?>
    <div class="forum-info-box forum-user-posts" style=" margin-bottom: 120px;">
        <div class="forum-info-box-inner">
            Заказов не найдено
        </div>
    </div>
<? endif; ?>

