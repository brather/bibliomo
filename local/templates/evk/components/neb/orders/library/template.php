<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$APPLICATION->SetTitle('Заказы литературы в библиотеке');
?>
	<style type="text/css">
		.inputRadio {
			-webkit-border-radius: 10px;
			border-radius: 10px;
			background: url('/local/templates/.default/markup/i/radio.png') left bottom no-repeat;
			width: 28px;
			height: 28px;
			cursor: pointer;
		}
		.active {
			background: url('/local/templates/.default/markup/i/radio.png') no-repeat 0 0;
		}

	</style>
<?if($arResult['SINGLE_USER']):?>
	<input type="hidden" value="<?=$_GET['uid']?>" id="SINGLE_USER"/>
	<div class="b-addsearchmode">
		<div class="b-search_field">
			<form method="get">
				<input type="text" class="b-search_fieldtb b-text" id="asearch-orders" value="<?=$_REQUEST['q_book']?>" name="q_book" placeholder="Поиск по книге" autocomplete="off">
				<input type="hidden" name="uid" value="<?=$_REQUEST['uid']?>">
				<input type="submit" class="b-search_bth bt_typelt bbox" value="Найти">
			</form>
		</div><!-- /.b-search_field-->
	</div>
	<div class="message-result"><?=$arResult['MESSAGE']?></div>
	<?if(count($arResult['ITEMS']) > 0):?>
		<br/>
	<table class="b-usertable b-libuser b-tbscan tsize">
		<tr>
			<th class="col-author">Автор</th>
			<th>Название</th>
			<th>Дата заказа</th>
			<th class="col-center">Заказ анул. польз.</th>
			<th class="col-center">Жел-я дата получ.</th>
			<th class="col-center">Подтв-e жел. даты получения</th>
			<th class="col-center">Дата выдачи</th>
			<th class="col-center">Дата возврата</th>
			<th class="col-center">Комментарий</th>
			<th class="col-center">Заказ анул.библ.</th>
			<th class="col-center"></th>
			<th class="col-center"></th>
		</tr>
		<?
		if(!empty($arResult['ITEMS']))
		{
			?>
           <div style="margin-top: 10px; margin-bottom: 10px">
			   <b>Читатель: <?=$arResult["USER_FIO"];?></b>
		   </div>
			<?
			foreach($arResult['ITEMS'] as $arItem)
			{
				if(!isset($arItem['BOOK_AUTHOR']) && !isset($arItem['BOOK_TITLE']))
					continue;
				?>
				<tr id="tr_<?=$arItem["ID"];?>" class="trBook" rel="<?=$arItem["ID"];?>">
					<td><br/><?=$arItem['BOOK_AUTHOR']?></td>
					<td><br/><a class="popup_opener ajax_opener coverlay" data-width="955" href="/catalog/<?=$arItem['UF_BOOK_ID']?>/"><?=$arItem['BOOK_TITLE']?></a></td>
					<td style="opacity: 0.7; padding-left: 0px;padding-right: 0px;">
						<br/>
						<?if($arItem['UF_DATE_ORDER']){
							echo $arItem['UF_DATE_ORDER'];
						}
						else{
							global $DB;
							echo date($DB->DateFormatToPHP(CSite::GetDateFormat("SHORT")), time());
						};?>
					</td>
					<td style="opacity: 0.7;">
						<input type="hidden" name="type" value="<?= $arItem['UF_ORDER_REVOKED_US']; ?>"
							   id="ORDER_REVOKED_US_<?=$arItem["ID"];?>"/>
						<br/>

						<div rel="ORDER_REVOKED_US_<?=$arItem["ID"];?>"
							 class="inputRadio ClickNo <? if ($arItem['UF_ORDER_REVOKED_US']): ?>active<?endif; ?>"></div>
					</td>
					<td style="opacity: 0.7; padding-left: 0px;padding-right: 0px;">
						<br/>
						<?if($arItem['UF_DESIRE_DATE']){
							echo $arItem['UF_DESIRE_DATE'];
						}
						else{
							echo 'Дата не установлена';
						};?>
					</td>
					<td style="padding-left: 25px padding-right: 0px;">
						<input type="hidden" name="type" value="<?= $arItem['UF_DESIRE_DATE_STAT']; ?>" data-val="<?=$arItem["ID"];?>"
							   id="DESIRE_DATE_STAT_<?=$arItem["ID"];?>"/>
						<br/>

						<div rel="DESIRE_DATE_STAT_<?=$arItem["ID"];?>"
							 class="inputRadio <? if ($arItem['UF_DESIRE_DATE_STAT']): ?>active<?endif; ?>"></div>
					</td>

					<td style="width: 180px;padding-left: 0px; padding-right: 0px;"><br/>
						<? $APPLICATION->IncludeComponent("bitrix:main.calendar", "profile", Array(
							"SHOW_INPUT" => "Y",    // Показывать элемент управления
							"FORM_NAME" => "",    // Имя формы
							"INPUT_NAME" => "date_fld1_".$arItem["ID"],    // Имя первого поля интервала
							"INPUT_NAME_FINISH" => "",    // Имя второго поля интервала
							"INPUT_VALUE" => $arItem['UF_DATE_OF_ISSUE'],    // Значение первого поля интервала
							"INPUT_VALUE_FINISH" => "",    // Значение второго поля интервала
							"SHOW_TIME" => "N",    // Позволять вводить время
							"HIDE_TIMEBAR" => "Y",    // Скрывать поле для ввода времени
							"ITEM_ID" => $arItem["ID"]
						),
							false
						); ?>
					</td>
					<td style="width: 180px; padding-right: 0px; "><br/>
						<? $APPLICATION->IncludeComponent("bitrix:main.calendar", "profile", Array(
							"SHOW_INPUT" => "Y",    // Показывать элемент управления
							"FORM_NAME" => "",    // Имя формы
							"INPUT_NAME" => "date_fld2_".$arItem["ID"],    // Имя первого поля интервала
							"INPUT_NAME_FINISH" => "",    // Имя второго поля интервала
							"INPUT_VALUE" => $arItem['UF_DATE_RETURN'],    // Значение первого поля интервала
							"INPUT_VALUE_FINISH" => "",    // Значение второго поля интервала
							"SHOW_TIME" => "N",    // Позволять вводить время
							"HIDE_TIMEBAR" => "Y",    // Скрывать поле для ввода времени
							"ITEM_ID" => $arItem["ID"]
						),
							false
						); ?>
					</td>
					<td><br/>
						<textarea data-val="<?=$arItem["ID"];?>" rows="3" cols="20" id="UF_COMMENT_<?=$arItem["ID"];?>"><?=$arItem['UF_COMMENT'];?></textarea>
					</td>
					<td style="cursor: pointer">
						<input type="hidden" name="type" value="<?= $arItem['UF_ORDER_REVOKED_LI']; ?>" data-val="<?=$arItem["ID"];?>"
							   id="ORDER_REVOKED_LI_<?=$arItem["ID"];?>"/>
						<br/>

						<div rel="ORDER_REVOKED_LI_<?=$arItem["ID"];?>" style="cursor: inherit"
							 class="inputRadio <? if ($arItem['UF_ORDER_REVOKED_LI']): ?>active<?endif; ?>"></div>
					</td>
					<td>
						<br/>
						<input type="hidden" value="" id="md5sum_<?=$arItem["ID"];?>">
						<a href="/ajax/okSave.php?author=<?= $arItem['BOOK_AUTHOR'] ?>&title=<?= $arItem['BOOK_TITLE'] ?>" style="cursor: pointer;padding-left: 0px; padding-right: 0px;" class="UpdateOrder popup_opener ajax_opener closein" rel="<?=$arItem["ID"];?>">Сохранить</a></td>
					<td>
						<br/>
						<a href="/ajax/okDel.php?author=<?= $arItem['BOOK_AUTHOR'] ?>&title=<?= $arItem['BOOK_TITLE'] ?>&order_id=<?=$arItem["ID"];?>" style="cursor: pointer" class="popup_opener ajax_opener closein" rel="<?=$arItem["ID"];?>">Удалить</a></td>
				</tr>
				<?
			}
		}
		?>
	</table>
		<?else:?>
		<div class="forum-info-box forum-user-posts" style=" margin-bottom: 50px;">
			<div class="forum-info-box-inner">
				Заказов не найдено	</div>
		</div>
		<?endif;?>
	<a class="button_blue left" href="/profile/#tab-orders">Вернуться к списку заказов</a>

<? else: ?>
	<div class="b-addsearchmode">
		<div class="b-search_field">
			<form method="get">
				<input type="text" class="b-search_fieldtb b-text" id="asearch-orders" value="<?=$_REQUEST['q_user']?>" name="q_user" placeholder="Поиск пользователя" autocomplete="off">
				<input type="submit" class="b-search_bth bt_typelt bbox" value="Найти">
			</form>
		</div><!-- /.b-search_field-->
	</div>
	<div class="message-result"><?=$arResult['MESSAGE']?></div>
	<table class="b-usertable b-tbscan tsize">
		<tr>
			<th>Фамилия, имя, отчество</th>
			<th>Кол-во книг</th>
			<th>Просмотреть заказ</th>
		</tr>
		<?
		if(!empty($arResult['ITEMS']))
		{
			foreach($arResult['ITEMS'] as $arItem)
			{
				if(!isset($arItem['USER_FIO']))
					continue;
				?>
				<tr>
					<td><?=$arItem['USER_FIO']?></td>
					<td><?=$arItem['COUNT']?></td>
					<td><a href="/profile/?uid=<?=$arItem['UF_USER_ID']?>#tab-orders" class="open_detail">Просмотреть</a></td>
				</tr>
				<?
			}
		}
		?>
	</table>
<? endif; ?>

<?if($_GET['uid']):?>
	<script type="text/javascript">
		$('.b-onelib_side').hide();
	</script>
<?endif;?>