<?
	if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
		die();
	use \Bitrix\Main\Localization\Loc as Loc;
	Loc::loadMessages(__FILE__);
?>
<?if(!empty($arResult['BOOKS'])):?>
	<div class="wrapper">
		<div class="rel">
			<section class="b-mainbanner bookblock bb-bookblock">
				<?foreach($arResult['SECTIONS'] as $sectKey=>$arSection):?>
					<div class="b-mainbanner_item bb-item">
						<img src="<?=$arSection['BACKGROUND']?>" alt="<?=$arResult['BOOKS'][$arSection["ID"]]["NAME"]?>">
						<div class="b_banner_info iblock bbox">
							<a href="/collections/" class="b-sectionlink"><?=Loc::getMessage('CATALOG_PROMO_TEMPLATE_COLLECTIONS')?></a>
							<?if(!empty($arSection["SHOW_NAME"])):?>
								<h1 style="<?=(!empty($arSection["UF_FONT_SIZE"]) && intval($arSection["UF_FONT_SIZE"]) > 0) ? sprintf('font-size: %dpx;', intval($arSection["UF_FONT_SIZE"])) : ''?><?=(!empty($arSection["UF_FONT_SIZE"])) ? sprintf('color: %s;', $arSection["UF_FONT_COLOR"]) : ''?>"><?=$arSection["NAME"]?></h1>
							<?endif;?>
							<?if(!empty($arSection["DESCRIPTION"])):?>
								<div class="b_booktext" style="<?=(!empty($arSection["UF_FONT_SIZE_DESCR"]) && intval($arSection["UF_FONT_SIZE_DESCR"]) > 0) ? sprintf('font-size: %dpx;', intval($arSection["UF_FONT_SIZE_DESCR"])) : ''?><?=(!empty($arSection["UF_FONT_SIZE_DESCR"])) ? sprintf('color: %s;', $arSection["UF_FONT_COLOR_DESCR"]) : ''?>"><?=$arSection['DESCRIPTION']?></div>
							<?endif;?>
						</div>
						<div class="b-banner_book iblock bbox">
							<div class="b-maintooklb"><?=Loc::getMessage("CATALOG_PROMO_TEMPLATE_MAIN_PLAYS")?>:</div>
							<ul class="b-mainbooks">
								<?foreach($arResult['BOOKS']["IDS"][$arSection["ID"]] as $arBook):?>
									<?$arBook = $arResult['BOOKS']["ITEMS"][$arBook]?>
									<?php if(!empty($arBook["IMAGE_URL_RESIZE"])):?>
										<li><a href="<?=$arBook["DETAIL_PAGE_URL"]?>"><img src="<?=sprintf('%s%s&p=%d', $arBook["IMAGE_URL_RESIZE"], '&w=124', ($arBook["COVER_NUMBER_PAGE"]>0)?$arBook["COVER_NUMBER_PAGE"]:1)?>" alt="<?=$arBook["NAME"]?>"></a></li>
									<?php endif;?>
								<?endforeach;?>
							</ul>
						</div>
						<div class="b-mainbanner_menu">
							<a href="#" class="prev js_bookblock_prev"></a>
							<ul class="b-mainbanner_list">
						<?foreach($arResult['SECTIONS'] as $sectKey2=>$arSection2):?>
							<li<?=($sectKey2==$sectKey)?' class="current"':'';?>><a href="#"><?=$arSection2["NAME"]?></a></li>
						<?endforeach;?>
							</ul>
							<a href="#" class="next js_bookblock_next"></a>
						</div>
					</div><!-- /.b-mainbanner_item -->
				<?endforeach;?>
			</section>
		</div>
	</div>
<?endif;?>