<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/**
 * @var CBitrixComponentSearchPageViewerLeft $component
 * @var Array $arParams
 * @var Array $arResult
 */
?>
<a name="nav_start"></a>
<div class="b-left_side">
	<div class="b-search-result">
		<?
		if(!empty($arResult['ITEMS']))
		{
			foreach($arResult['ITEMS'] as $index=>$arItem)
			{
				?>
				<div class="b-ebook<?if(!empty($arItem["IMAGE_URL"])) echo ' b-with-image';?>">
					<?if($arParams["IS_FULL_SEARCH"] && !empty($arItem["IMAGE_URL"])):?>
						<?if(!empty($arItem['DETAIL_PAGE_URL'])):?>
							<a href="http://xn--90ax2c.xn--p1ai<?=$arItem['DETAIL_PAGE_URL']?>" target="_blank">
						<?endif;?>
							<img alt="<?=htmlspecialchars(strip_tags($arItem["title"]))?>" src="/local/tools/exalead/thumbnail.php?url=<?=$arItem['URL']?>&source=<?=$arItem['source']?>&width=124&height=170">
						<?if(!empty($arItem['DETAIL_PAGE_URL'])):?>
							</a>
						<?endif;?>
					<?else:?>
						<?if(!empty($arItem['DETAIL_PAGE_URL'])):?>
							<a href="http://xn--90ax2c.xn--p1ai<?=$arItem['DETAIL_PAGE_URL']?>" target="_blank">
						<?endif;?>
						<img alt="<?=htmlspecialchars(strip_tags($arItem["title"]))?>" src="/local/images/book_empty.jpg">
						<?if(!empty($arItem['DETAIL_PAGE_URL'])):?>
							</a>
						<?endif;?>
					<?endif;?>
					<h4>
						<?if(!empty($arItem['DETAIL_PAGE_URL'])):?>
							<a href="http://xn--90ax2c.xn--p1ai<?=$arItem['DETAIL_PAGE_URL']?>" target="_blank">
						<?endif;?>
							<?=$arItem['title']?>
						<?if(!empty($arItem['DETAIL_PAGE_URL'])):?>
							</a>
						<?endif;?>
					</h4>
					<div class="text author">
						<?if(!empty($arItem['authorbook'])):?>
							<span><?=$arItem['authorbook']?></span>
						<?endif;?>
						<?if(!empty($arItem['publishyear'])):?>
							<span class="title"><?=$arItem['publishyear']?></span>
						<?endif;?>
						<?if(!empty($arItem['publisher'])):?>
							<span><?=$arItem['publisher']?></span>
						<?endif;?>
					</div>
					<?if(!empty($arItem["TEXT"])):?>
						<div class="text"><?=$arItem["TEXT"]?></div>
					<?endif;?>
					<?if(!empty($arItem['library'])):?>
						<div class="text"><span class="title">Источник:</span> <?=$arItem['library']?></div>
					<?endif;?>
					<?if(!empty($arItem['countpages'])):?>
						<div class="text"><span class="title">Кол-во страниц:</span> <?=$arItem['countpages']?></div>
					<?endif;?>
				</div>
				<?
			}
			if(!empty($arResult["NEXT_PAGE"]))
			{
			?>
				<div class="b-epublishing_more">
					<a class="b-morebuttton" href="<?=$arResult["NEXT_PAGE"]?>">Показать ещё</a>
				</div>
			<?
			}
		}
		?>
	</div>
</div>