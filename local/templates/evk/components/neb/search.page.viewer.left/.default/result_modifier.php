<?
use Bitrix\Main\Loader;
use Evk\Books\CSearchSphinx;
use Evk\Books\CSearchFullText;

if (!Loader::includeModule("evk.books") || !Loader::includeModule("iblock")) {
    return false;
}
$searchInstance = CSearchFullText::GetInstance();
if ($searchInstance instanceof CSearchSphinx) {
    $SearchRow = CSearchFullText::GetInstance()->getRowFormatter();
    foreach ($arResult['ITEMS'] as $index => $arItem) {
        if ($arItem['SEARCH_ITEM']['GETMORF'] == 'Y') {
            if (!empty($arItem['SEARCH_ITEM']['ID'])) {
                $t = $SearchRow->formatRowCautom($arItem['SEARCH_ITEM']['ID'], $arParams["q"]);
                if (!empty($t["TITLE_FORMATED"])) {
                    $arResult['ITEMS'][$index]["NAME"] = $t["TITLE_FORMATED"];
                }
                if (!empty($t["BODY_FORMATED"])) {
                    $arResult['ITEMS'][$index]["TEXT"] = $t["BODY_FORMATED"];
                }
            }
        }
    }
}