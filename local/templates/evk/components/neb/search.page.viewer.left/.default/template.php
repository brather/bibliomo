<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/**
 * @var CBitrixComponentSearchPageViewerLeft $component
 * @var Array $arParams
 * @var Array $arResult
 */
?>
<a name="nav_start"></a>
<div class="b-left_side">
	<div class="b-search-result">
		<?
		if(!empty($arResult['ITEMS']))
		{
			foreach($arResult['ITEMS'] as $index=>$arItem)
			{
				?>
				<div class="b-ebook b-with-image">
					<?if(!empty($arItem["IMAGE_URL"])):?>
						<?if(!empty($arItem['DETAIL_PAGE_URL'])):?>
							<a href="<?=$arItem['DETAIL_PAGE_URL']?>">
						<?endif;?>
							<img alt="<?=htmlspecialchars($arItem["NAME"])?>" src="<?=$arItem["IMAGE_URL"]?>">
						<?if(!empty($arItem['DETAIL_PAGE_URL'])):?>
							</a>
						<?endif;?>
					<?php else:?>
						<?if(!empty($arItem['DETAIL_PAGE_URL'])):?>
							<a href="<?=$arItem['DETAIL_PAGE_URL']?>">
						<?endif;?>
						<img alt="<?=htmlspecialchars($arItem["NAME"])?>" src="/local/images/book_empty.jpg">
						<?if(!empty($arItem['DETAIL_PAGE_URL'])):?>
							</a>
						<?endif;?>
					<?endif;?>
					<h4>
						<?if(!empty($arItem['DETAIL_PAGE_URL'])):?>
							<a href="<?=$arItem['DETAIL_PAGE_URL']?>">
						<?endif;?>
							<?=$arItem['NAME']?>
						<?if(!empty($arItem['DETAIL_PAGE_URL'])):?>
							</a>
						<?endif;?>
					</h4>
					<div class="text author">
						<?if(!empty($arItem['PROPERTY_AUTHOR_VALUE'])):?>
							<span><a href="<?=$component->GetLink("AUTHOR", $arItem['PROPERTY_AUTHOR_VALUE']);?>"><?=$arItem['PROPERTY_AUTHOR_VALUE']?></a></span>
						<?endif;?>
						<?if(!empty($arItem['PROPERTY_PUBLISH_YEAR_VALUE'])):?>
							<span class="title"><?=$arItem['PROPERTY_PUBLISH_YEAR_VALUE']?></span>
						<?endif;?>
						<?if(!empty($arItem['PROPERTY_PUBLISH_VALUE'])):?>
							<span><a href="<?=$component->GetLink("PUBLISH", $arItem['PROPERTY_PUBLISH_VALUE']);?>"><?=$arItem['PROPERTY_PUBLISH_VALUE']?></a></span>
						<?endif;?>
					</div>
					<?if(!empty($arItem["TEXT"])):?>
						<div class="text"><?=$arItem["TEXT"]?></div>
					<?endif;?>
					<?
					$i = 1;
					$c = count($arItem["LIBS"]);
					if($c > 0)
					{
						?>
						<div class="text"><span class="title">Источник:</span>
							<?foreach($arItem["LIBS"] as $LIB):?>
								<?if($LIB['DETAIL_PAGE_URL']):?>
									<a class="link-lib" href="<?=$LIB['DETAIL_PAGE_URL']?>"><?=$LIB['NAME']?></a>
								<?else:?>
									<?=$LIB['NAME']?>
								<?endif;?>
								<?if($i++<$c) echo "<br/>";?>
							<?endforeach;?>
						</div>
					<?
					}
					if(intval($arItem['PROPERTY_VOLUME_VALUE']) > 0)
					{
						?>
						<div class="text"><span class="title">Кол-во страниц:</span> <?=$arItem['PROPERTY_VOLUME_VALUE']?></div>
						<?
					}
					?>
					</div>
				<?
			}
			if(!empty($arResult["NEXT_PAGE"]))
			{
			?>
				<div class="b-epublishing_more">
					<a class="b-morebuttton" href="<?=$arResult["NEXT_PAGE"]?>">Показать ещё</a>
				</div>
			<?
			}
		}
		?>
	</div>
</div>