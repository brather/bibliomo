<?php
$MESS['LIBRARY_DETAIL_ADDRESS'] = 'Почтовый адрес';
$MESS['LIBRARY_DETAIL_ADDRESS_MAP'] = 'Адрес';
$MESS['LIBRARY_DETAIL_WORK'] = 'График работы';
$MESS['LIBRARY_DETAIL_PHONE'] = 'Телефон';
$MESS['LIBRARY_DETAIL_EMAIL'] = 'Электронная почта';
$MESS['LIBRARY_DETAIL_CONTACT'] = 'Контактное лицо';
$MESS['LIBRARY_DETAIL_USER'] = 'Участник';
$MESS['LIBRARY_RIGHT_COUNTER_FUND'] = 'В фонде библиотеки';

$MESS['LIBRARY_RIGHT_COUNTER_PUB_5'] = 'изданий';
$MESS['LIBRARY_RIGHT_COUNTER_PUB_1'] = 'издание';
$MESS['LIBRARY_RIGHT_COUNTER_PUB_2'] = 'издания';

$MESS['LIBRARY_RIGHT_COUNTER_INC'] = 'Библиотека собрала';

$MESS['LIBRARY_RIGHT_COUNTER_COL_5'] = 'коллекций';
$MESS['LIBRARY_RIGHT_COUNTER_COL_1'] = 'коллекцию';
$MESS['LIBRARY_RIGHT_COUNTER_COL_2'] = 'коллекции';

$MESS['LIBRARY_RIGHT_COUNTER_VIEW_ALL'] = 'посмотреть все';
$MESS['LIBRARY_RIGHT_COUNTER_READER'] = 'Количество читателей';
$MESS['LIBRARY_RIGHT_COUNTER_VIEWERS'] = 'Количество просмотров';

$MESS['LIBRARY_RIGHT_COUNTER_USERS_5'] = 'читателей';
$MESS['LIBRARY_RIGHT_COUNTER_USERS_1'] = 'читатель';
$MESS['LIBRARY_RIGHT_COUNTER_USERS_2'] = 'читателя';