<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
//\Evk\Books\Books::p($arResult, $arParams);
$rsUserMessage = CUser::GetList(($by='id'), ($order='desc'), array(
	'ACTIVE' => "Y",
	"UF_LIBRARY" => $arResult["ITEM"]["ID"],
));
$arResult["SEND_MESSAGE_LINK"] = false;
if($arUserMessage = $rsUserMessage->Fetch())
{
	$arResult["SEND_MESSAGE_LINK"] = sprintf('/forum/pm/folder0/message0/user%d/new/', $arUserMessage["ID"]);
}

if(!preg_match('@^(http|https)://@', $arResult['ITEM']['PROPERTY_URL_VALUE']))
{
	$arResult['ITEM']['PROPERTY_URL_VALUE'] = sprintf('http://%s', $arResult['ITEM']['PROPERTY_URL_VALUE']);
}

if(preg_match('@^(http|https)://(.*)$@', $arResult['ITEM']['PROPERTY_URL_VALUE'], $match))
{
	$arResult['ITEM']['~PROPERTY_URL_VALUE'] = $match[2];
}
