<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use \Bitrix\Main\Localization\Loc as Loc;
Loc::loadMessages(__DIR__.'/template.php');
?>
<div class="wrapper">
	<section class="b-inner clearfix map-inner">
		<div class="clearfix onelib js_oneheight">
			<div class="b-onelib_side bbox">
				<?if(!empty($arResult['ITEM']['PREVIEW_PICTURE']['src'])):?>
					<img src="<?=$arResult['ITEM']['PREVIEW_PICTURE']['src']?>" alt="<?=$arResult['ITEM']["NAME"]?>">
				<?endif;?>
				<?if(!empty($arResult['ITEM']['PROPERTY_USERS_VALUE'])
					|| !empty($arResult['ITEM']['PROPERTY_PUBLICATIONS_VALUE'])
					|| !empty($arResult['ITEM']['PROPERTY_COLLECTIONS_VALUE'])
				):?>
				<div class="b-libstatistic">
				<?endif;?>
					<?if(!empty($arResult['ITEM']['PROPERTY_PUBLICATIONS_VALUE'])):?>
						<div class="b-libstatistic_item books">
							<div class="b-libstatistic_tit">
								<?=number_format($arResult['ITEM']['PROPERTY_PUBLICATIONS_VALUE'], 0, '', ' ')?> <?=GetEnding($arResult['ITEM']['PROPERTY_PUBLICATIONS_VALUE'], Loc::getMessage('LIBRARY_RIGHT_COUNTER_PUB_5'), Loc::getMessage('LIBRARY_RIGHT_COUNTER_PUB_1'), Loc::getMessage('LIBRARY_RIGHT_COUNTER_PUB_2'))?>
							</div>
							<span>В фонде библиотеки</span>
						</div>
					<?endif;?>
					<?if(!empty($arResult['ITEM']['PROPERTY_COLLECTIONS_VALUE'])):?>
						<div class="b-libstatistic_item collection">
							<div class="b-libstatistic_tit"><?=number_format($arResult['ITEM']['PROPERTY_COLLECTIONS_VALUE'], 0, '', ' ')?> <?=GetEnding($arResult['ITEM']['PROPERTY_COLLECTIONS_VALUE'], Loc::getMessage('LIBRARY_RIGHT_COUNTER_COL_5'), Loc::getMessage('LIBRARY_RIGHT_COUNTER_COL_1'), Loc::getMessage('LIBRARY_RIGHT_COUNTER_COL_2'))?></div>
							<span>Собрала библиотека</span>
							<a href="<?=$arResult['ITEM']["COLLECTIONS_LINK"]?>" class="green tdu">Смотреть коллекции</a>
						</div>
					<?endif;?>
					<?if(!empty($arResult['ITEM']['PROPERTY_USERS_VALUE'])):?>
						<div class="b-libstatistic_item reader">
							<div class="b-libstatistic_tit"><?=number_format($arResult['ITEM']['PROPERTY_USERS_VALUE'], 0, '', ' ')?> <?=GetEnding($arResult['ITEM']['PROPERTY_USERS_VALUE'], Loc::getMessage('LIBRARY_RIGHT_COUNTER_USERS_5'), Loc::getMessage('LIBRARY_RIGHT_COUNTER_USERS_1'), Loc::getMessage('LIBRARY_RIGHT_COUNTER_USERS_2'))?></div>
						</div>
					<?endif;?>
				<?if(!empty($arResult['ITEM']['PROPERTY_USERS_VALUE'])
				|| !empty($arResult['ITEM']['PROPERTY_PUBLICATIONS_VALUE'])
				|| !empty($arResult['ITEM']['PROPERTY_COLLECTIONS_VALUE'])
				):?>
				</div>
				<?endif;?>
				<div class="b-libsinfo_item">
					<div class="ico_address ico_label">Адрес</div>
					<?
					$arPos = explode(",", $arResult['ITEM']["PROPERTY_MAP_VALUE"]);
					?>
					<p><?=$arResult['ITEM']['PROPERTY_ADDRESS_VALUE']?> <a href="#" onclick="showAddress();" class="green tdu nowrap">Смотреть на карте</a></p>
					<script>
						function showAddress()
						{
							var destination  = $('#showAddress').offset();
							console.log(destination.top);
							$('html, body').animate({"scrollTop": destination.top },"slow");
							$('#showAddress').trigger('click');
							return false;
						}
					</script>
				</div>
				<div class="b-libsinfo_item">
					<div class="ico_graph ico_label">График работы</div>
					<div class="b-libtime">
						<div class="b-libtime_item iblock"><span class="label">Пн</span><div style="width: 100%"></div></div>
						<div class="b-libtime_item iblock"><span class="label">Вт</span><div style="width: 100%"></div></div>
						<div class="b-libtime_item iblock"><span class="label">Ср</span><div style="width: 100%"></div></div>
						<div class="b-libtime_item iblock"><span class="label">Чт</span><div style="width: 100%"></div></div>
						<div class="b-libtime_item iblock"><span class="label">Пт</span><div style="width: 100%"></div></div>
						<div class="b-libtime_item iblock"><span class="label">Сб</span><div style="width: 80%"></div></div>
						<div class="b-libtime_item iblock"><span class="label">Вс</span><div style="width: 0"></div></div>
					</div>
					<?=$arResult['ITEM']['~PROPERTY_SCHEDULE_VALUE']?>
				</div>
				<?if(!empty($arResult['ITEM']['PROPERTY_PHONE_VALUE'])):?>
					<div class="b-libsinfo_item">
						<div class="ico_phone ico_label">Телефоны</div>
							<?foreach($arResult['ITEM']['PROPERTY_PHONE_VALUE'] as $phone):?>
								<p><?=$phone?></p>
							<?endforeach;?>
					</div>
				<?endif;?>
				<?if(!empty($arResult['ITEM']['PROPERTY_EMAIL_VALUE'])):?>
					<div class="b-libsinfo_item">
						<div class="ico_mail ico_label">Электронная почта</div>
						<a href="mailto:<?=$arResult['ITEM']['PROPERTY_EMAIL_VALUE']?>" class="green tdu"><?=$arResult['ITEM']['PROPERTY_EMAIL_VALUE']?></a>
					</div>
				<?endif;?>
				<?if(!empty($arResult['ITEM']['PROPERTY_CONTACTS_VALUE'])):?>
					<div class="b-libsinfo_item">
						<div class="ico_contact ico_label">Контактное лицо</div>
						<?foreach($arResult['ITEM']['PROPERTY_CONTACTS_VALUE'] as $contactID=>$contact):?>
							<div class="b-contact_name"><?=$contact?></div>
							<div class="b-contact_status"><?=$arResult['ITEM']['PROPERTY_CONTACTS_DESCRIPTION'][$contactID]?></div>
						<?endforeach;?>
					</div>
				<?endif;?>
				<?$APPLICATION->IncludeComponent("bitrix:voting.current", "right", Array(
					"CHANNEL_SID" => "LIBRARY_".$arResult['ITEM']['ID'],	// Группа опросов
					"VOTE_ID" => "",	// ID опроса
					"VOTE_ALL_RESULTS" => "N",	// Показывать варианты ответов для полей типа Text и Textarea
					"AJAX_MODE" => "Y",	// Включить режим AJAX
					"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
					"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
					"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
					"CACHE_TYPE" => "A",	// Тип кеширования
					"CACHE_TIME" => "3600",	// Время кеширования (сек.)
					"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
				),
					false
				);?>
			</div><!-- /.b-onelib_side -->
			<div class="b-onelib_info" style="padding-top: 0;">
				<div class="example3">
					<?
					if($arResult["ITEM"]["DETAIL_PICTURE"]):
					$image_resize = CFile::ResizeImageGet($arResult["ITEM"]["DETAIL_PICTURE"], array("width" => 820, "height" => 410), BX_RESIZE_IMAGE_EXACT, true);?>
					<img src="<?=$image_resize['src'];?>" class="example_beauty" />
					<div class="example_text">
						<h1><?=$arResult['ITEM']['NAME']?></h1>
					</div>
					<?else:?>
						<h1><?=$arResult['ITEM']['NAME']?></h1>
					<?endif;?>
				</div>
				<h1><?//=$arResult['ITEM']['NAME']?></h1>
				<div class="b-libmain_info">
					<div class="b-libmain_status iblock">
						<?//php if($arResult["ITEM"]["PROPERTY_STATUS_ENUM_ID"] == 1):?>
							<!--<img src="/local/templates/.default/markup/i/neb_b.png" alt="<?//=htmlspecialchars($arResult["ITEM"]["PROPERTY_STATUS_VALUE"])?>">-->
						<?//php endif;?>
						<?//php if(!empty($arResult["ITEM"]["PROPERTY_STATUS_VALUE"])):?>
							<!--<span><?//=$arResult["ITEM"]["PROPERTY_STATUS_VALUE"]?></span>-->
						<?//php endif;?>
						<?if(!empty($arResult['ITEM']['PROPERTY_URL_VALUE'])):?>
							<span class="b-libmain_site">Сайт библиотеки: <a href="<?=$arResult['ITEM']['PROPERTY_URL_VALUE']?>" class="tdu green"><?=$arResult['ITEM']['~PROPERTY_URL_VALUE']?></a></span>
						<?endif;?>
					</div>
				</div>
				<div class="tabs b-onelibtabs">
					<?if(!empty($arResult["SEND_MESSAGE_LINK"])):?>
						<a href="<?=$arResult["SEND_MESSAGE_LINK"]?>" class="bt_typelt">Отправить сообщение</a>
					<?endif;?>
					<?$APPLICATION->IncludeComponent("bitrix:menu", "lk_left", Array(
						"ROOT_MENU_TYPE" => "left",	// Тип меню для первого уровня
						"MENU_CACHE_TYPE" => "N",	// Тип кеширования
						"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
						"MENU_CACHE_USE_GROUPS" => "N",	// Учитывать права доступа
						"MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
							0 => "",
						),
						"MAX_LEVEL" => "2",	// Уровень вложенности меню
						"CHILD_MENU_TYPE" => "",	// Тип меню для остальных уровней
						"USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
						"DELAY" => "N",	// Откладывать выполнение шаблона меню
						"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
					),
						false
					);?>
					<div id="tabs-news">
						<?
						$sorts = ['active_from', 'created'];
						global $arrFilterLibraryNews;
						$arrFilterLibraryNews = array('PROPERTY_LIBRARY' => $arResult['ITEM']['ID'], '!PROPERTY_LIBRARY' => false);
						$by = 'active_from';
						$order = 'desc';
						if(isset($_REQUEST['tag'])) {
							$arrFilterLibraryNews['?TAGS'] = $_REQUEST['tag'];
						}
						if(isset($_REQUEST['by'])) {
							$by = $_REQUEST['by'];
						}
						if(isset($_REQUEST['order'])) {
							$order = $_REQUEST['order'];
						}
						$APPLICATION->IncludeComponent("bitrix:news.list", "", Array(
							"by" => $by,
							"order" => $order,
							"sorts" => $sorts,
							"IBLOCK_TYPE" => "news",	// Тип информационного блока (используется только для проверки)
							"IBLOCK_ID" => "3",	// Код информационного блока
							"NEWS_COUNT" => "10",	// Количество новостей на странице
							"SORT_BY1" => $by,	// Поле для первой сортировки новостей
							"SORT_ORDER1" => $order,	// Направление для первой сортировки новостей
							"FILTER_NAME" => "arrFilterLibraryNews",	// Фильтр
							"FIELD_CODE" => array(	// Поля
								"NAME",
								"PREVIEW_TEXT",
								'ACTIVE',
								'TAGS',
							),
							"PROPERTY_CODE" => array(	// Свойства
							),
							"CHECK_DATES" => "N",	// Показывать только активные на данный момент элементы
							"DETAIL_URL" => str_replace('#CODE#', $arResult['ITEM']["CODE"], $arParams['NEWS_DETAIL_URL']),	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
							"AJAX_MODE" => "N",	// Включить режим AJAX
							"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
							"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
							"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
							"CACHE_TYPE" => "A",	// Тип кеширования
							"CACHE_TIME" => $arParams["CACHE_TIME"],	// Время кеширования (сек.)
							"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
							"CACHE_GROUPS" => "N",	// Учитывать права доступа
							"PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
							"ACTIVE_DATE_FORMAT" => "d.m.y",	// Формат показа даты
							"SET_TITLE" => "Y",	// Устанавливать заголовок страницы
							"SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
							"SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
							"SET_META_DESCRIPTION" => "N",	// Устанавливать описание страницы
							"SET_STATUS_404" => "N",	// Устанавливать статус 404, если не найдены элемент или раздел
							"INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
							"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
							"HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
							"PARENT_SECTION" => "",	// ID раздела
							"PARENT_SECTION_CODE" => "",	// Код раздела
							"INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
							"DISPLAY_DATE" => "Y",	// Выводить дату элемента
							"DISPLAY_NAME" => "Y",	// Выводить название элемента
							"DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
							"DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
							"PAGER_TEMPLATE" => "",	// Шаблон постраничной навигации
							"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
							"DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
							"PAGER_TITLE" => "Новости",	// Название категорий
							"PAGER_SHOW_ALWAYS" => "Y",	// Выводить всегда
							"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
							"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
							"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
							"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
						),
							false//$component
						);?>
					</div>
					<div id="tabs-1">
						<?=$arResult['ITEM']['~PREVIEW_TEXT']?>
						<?
						$APPLICATION->IncludeComponent(
							"neb:library.new.list",
							".default",
							array(
								"DATE" => "60",
								"COUNT" => "5",
								"PAGE_URL" => "/catalog/#BOOK_ID#/",
								"CACHE_TYPE" => "A",
								"CACHE_TIME" => "36001",
								"LIBRARY_ID" => $arResult['ITEM']['ID'],
							),
							$component
						);
						?>
					</div>
					<div id="tabs-2">
						<?
						$APPLICATION->IncludeComponent(
							"neb:library.funds",
							"",
							Array(
								"IBLOCK_ID" => $arParams["IBLOCK_ID"],
								"CODE" => $arParams["CODE"],
								"CACHE_TIME" => $arParams["CACHE_TIME"],
								"LIST_URL" => $arParams["LIST_URL"],
								"COLLECTION_URL" => $arParams["COLLECTION_URL"],
							),
							$component
						);
						?>
					</div>
					<div id="tabs-3">
						<?
						$APPLICATION->IncludeComponent(
							"neb:collections.list",
							"user",
							array(
								"PAGE_URL" => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['section'],
								"CACHE_TYPE" => "A",
								"CACHE_TIME" => "3600",
								"IBLOCK_ID" => IBLOCK_ID_COLLECTION,
								"LIBRARY_ID" => $arResult['ITEM']['ID'],
								"SHOW_EMPTY_COLLECTION" => false,
							),
							$component
						);
						?>
					</div>
				</div><!-- /.tabs -->
			</div><!-- /.b-onelib_info -->
			<div class="clearfix b-quizmap">
				<div class="b-map rel mappage one_library">
					<div class="b-nebmap">
						<a nane="yandex_map"></a>
						<div class="ymap" id="ymap" data-path="<?=$templateFolder."/ajax.php"?>" data-lat="<?=$arResult['ITEM']['MAP'][0]?>" data-lng="<?=$arResult['ITEM']['MAP'][1]?>" data-zoom="16"></div>
						<span class="marker_lib hidden" style="position: absolute; top: 0;left: 0; padding: 70px 30px 30px;  width: 240px;"></span>
					</div>
				</div><!-- /.b-map -->
			</div><!-- /.b-querymap -->
		</div><!-- /.b-clearfix -->
	</section>
</div>
