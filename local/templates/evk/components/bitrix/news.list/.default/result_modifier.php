<?php
\Bitrix\Main\Loader::includeModule('evk.books');
use Evk\Books\PageNavigation;

$arResult["NEWS_AJAX_PAGE"] = (isset($_REQUEST["NEWS_AJAX_PAGE"]) && $_REQUEST["NEWS_AJAX_PAGE"] == "Y");
$arResult["NEXT_NEWS_PAGE"] = PageNavigation::GetNextPage($arResult["NAV_RESULT"], 'NEWS_AJAX_PAGE');
$arResult["NEXT_NEWS_PAGE"] = str_replace('index.php', '', $arResult["NEXT_NEWS_PAGE"]);
foreach ($arResult['ITEMS'] as &$item) {
    $item['TAGS'] = explode(',', $item['TAGS']);
    $item['TAGS'] = array_map('trim', $item['TAGS']);
    $item['TAGS'] = array_filter($item['TAGS']);
}
unset($item);

