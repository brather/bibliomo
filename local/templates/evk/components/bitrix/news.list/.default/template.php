<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/**
 * @var array $arResult
 * @var CMain $APPLICATION;
 */
use \Bitrix\Main\Localization\Loc as Loc;
Loc::loadMessages(__FILE__);
global $APPLICATION;
?>
Сортировать:
<? foreach ($arParams['sorts'] as $sort) {
	$order = null;
	if ($sort == $arParams['by']) {
		$order = (
		$sort === $arParams['by']
			? ('asc' === $arParams['order'] ? 'desc' : 'asc')
			: 'asc'
		);
	}
	?>
<a <?= SortingExalead($sort, false, 'by', 'order', 'nav_start', [], false) ?>>
	<?= Loc::getMessage('LIB_NEWS_SORT_' . $sort) ?>
	</a><?= (isset($order) ? '&nbsp;' . ('desc' === $order ? '&darr;' : '&uarr;') : '') ?>&nbsp;|&nbsp;
<? } ?>
	<br>
	<br>

<?if(empty($arResult["ITEMS"]) && !isset($arParams["DETAIL_EXIST"])):?>
	<p>Новости отсутствуют</p>
<?else:?>
	<?if($arResult["NEWS_AJAX_PAGE"])
		$APPLICATION->RestartBuffer();
	?>
	<script type="text/javascript">
		$(function(){
			$('#tabs-news a.ajax_load').unbind('click').click(function(e){
				BX.showWait($(this).context);
				$('#tabs-news').load($(this).attr('href'), 'AJAX_NEWS_DETAIL=Y', function(){
					$.scrollTo('.ui-tabs-nav');
					BX.closeWait();
				});
				e.preventDefault();
			});
		});
	</script>
	<section class="b-news-list">
		<?php foreach($arResult["ITEMS"] as $arItem):?>
			<?
			$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => 'Будет удалена вся информация, связанная с этой записью. Продолжить?'));
			?>
			<div class="b-news-item<?php if(!empty($arItem["PREVIEW_PICTURE"])) echo ' b-news-with-image'?>"  id="<?=$this->GetEditAreaId($arItem['ID']);?>">
				<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="ajax_load">
					<?php if(!empty($arItem["PREVIEW_PICTURE"])):
						$image_resize = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"], array("width" => 250, "height" => 188), BX_RESIZE_IMAGE_PROPORTIONAL, true); ?>
						<img src="<?=$image_resize["src"]?>" alt="<?=$arItem["NAME"]?>">
					<?php endif;?>
					<div class="b-news-content">
						<div class="b-news-time"><?=$arItem["DISPLAY_ACTIVE_FROM"]?></div>
						<h4><?=$arItem["NAME"]?></h4>
						<div class="b-news-text">
							<?
							if(strlen($arItem["PREVIEW_TEXT"]) > 300){
								echo substr($arItem["PREVIEW_TEXT"], 0, strpos($arItem["PREVIEW_TEXT"], ' ', 300)).'...';
							}
							else{
								echo $arItem["PREVIEW_TEXT"];
							}
							?>
						</div>
						<? if (!empty($arItem['TAGS'])) { ?>
							<br>
							Теги:
							<div class="tags">
								<? foreach ($arItem['TAGS'] as $tag) { ?>
									<a class="tdu" href="<?= $APPLICATION->GetCurPageParam(
										"tag=" . $tag,
										['NEWS_AJAX_PAGE'],
										false
									); ?>"><?= $tag ?></a>,
								<? } ?>
							</div>
						<? } ?>
					</div>
				</a>
			</div>
		<?php endforeach;?>
	</section>
	<?php if(!empty($arResult["NEXT_NEWS_PAGE"])):?>
		<div class="b-wrap_more">
			<a href="<?=$arResult["NEXT_NEWS_PAGE"]?>" class="ajax_add b-morebuttton">Показать еще</a>
		</div>
	<?php endif;?>
	<?if($arResult["NEWS_AJAX_PAGE"])
		exit;
	?>
<?endif;?>