<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use \Bitrix\Main\Localization\Loc as Loc;
Loc::loadMessages(__FILE__);
?>
<?
	if(!empty($arResult["QUESTIONS"]))
	{
	?>
		<div class="b-quiz_box bbox">
			<?=ShowError($arResult["ERROR_MESSAGE"]);?>
			<?=ShowNote($arResult["OK_MESSAGE"]);?>
			<form action="<?=POST_FORM_ACTION_URI?>" method="post" class="b-form b-form_common b-formquiz">
				<h3>опрос</h3>
				<input type="hidden" name="vote" value="Y">
				<input type="hidden" name="PUBLIC_VOTE_ID" value="<?=$arResult["VOTE"]["ID"]?>">
				<input type="hidden" name="VOTE_ID" value="<?=$arResult["VOTE"]["ID"]?>">
				<?=bitrix_sessid_post()?>
				<?
				foreach ($arResult["QUESTIONS"] as $arQuestion)
				{
					?>
					<p><?=$arQuestion["QUESTION"]?></p>
					<ul class="b-quiz_answers scroll-pane">
					<?
					foreach ($arQuestion["ANSWERS"] as $arAnswer)
					{
						switch ($arAnswer["FIELD_TYPE"])
						{
							case 0://radio
								$value=(isset($_REQUEST['vote_radio_'.$arAnswer["QUESTION_ID"]]) &&
									$_REQUEST['vote_radio_'.$arAnswer["QUESTION_ID"]] == $arAnswer["ID"]) ? 'checked="checked"' : '';
								break;
							case 1://checkbox
								$value=(isset($_REQUEST['vote_checkbox_'.$arAnswer["QUESTION_ID"]]) &&
									array_search($arAnswer["ID"],$_REQUEST['vote_checkbox_'.$arAnswer["QUESTION_ID"]])!==false) ? 'checked="checked"' : '';
								break;
						}
						switch ($arAnswer["FIELD_TYPE"])
						{
							case 0://radio
								?>
								<li><input class="radio" type="radio" name="vote_radio_<?=$arAnswer["QUESTION_ID"]?>" value="<?=$arAnswer["ID"]?>" <?=$arAnswer["~FIELD_PARAM"]?> id="cb<?=$arAnswer['ID']?>" <?=$value?>>
								<label for="cb<?=$arAnswer['ID']?>"><?=$arAnswer["MESSAGE"]?></label></li>
								<?
								break;
							case 1:
								?>
								<li><input class="radio" type="checkbox" name="vote_checkbox_<?=$arAnswer["QUESTION_ID"]?>[]"  id="cb<?=$arAnswer['ID']?>" <?=$value?> value="<?=$arAnswer["ID"]?>" <?=$arAnswer["~FIELD_PARAM"]?>>
								<label for="cb<?=$arAnswer['ID']?>"><?=$arAnswer["MESSAGE"]?></label></li>
								<?
								break;
						}
					}
					?>
					</ul>
					<?
				}
				?>
				<input type="submit" class="bt_typedrk" value="Голосовать">
			</form>
		</div><!-- /.b-query_box -->
	<?
	}
?>
