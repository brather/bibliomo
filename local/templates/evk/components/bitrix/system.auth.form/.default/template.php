<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
	use \Bitrix\Main\Localization\Loc as Loc;
	Loc::loadMessages(__FILE__);
?>

<?if($_GET['PASSWORD_CHANGED'] == 'Y'):?>
<!--Этот параметр подставляется после успешного изменения пароля пользователем.-->
<script>
    $(function() {
        $('.b_login_popup').addClass('change_log_pass');
        $('.b-headernav_login_lnk').trigger('click');
    });
</script>
<?endif;?>

<div class="b-headernav_login right">
	<? 
		if($arResult["FORM_TYPE"] == "login")
		{
		?>
		<?/*/?><a href="/registration/" class="b-headernav_login_lnk <?=strpos($APPLICATION->GetCurPage(), '/registration') !== false ? 'current':''?> b-header_lock" data-width="255" data-height="170"><?//=Loc::getMessage('AUTH_REISTER_BUTTON');?></a><?/*/?>
		<a href="#" id="login-link" class="b-headernav_login_lnk <?=strpos($APPLICATION->GetCurPage(), '/registration') === false ? 'current':''?> popup_opener_login b-header_lock" data-height="265"  data-width="320" ><?//=Loc::getMessage('AUTH_LOGIN_BUTTON')?></a> <!-- data-width="550"  -->
		<div class="b_login_popup popup" style="display: none;">
			<div class="b_login_popup_in bbox">
				<?/*<div class="b-login_txt hidden"><a href="#">вход для библиотек</a> <a href="#">для правообладателей</a></div>*/?>

				<div class="b-loginform iblock rel">
					<div class="register-message">Если у вас еще нет аккаунта, <a href="/registration/" class="popup_opener ajax_opener closein" data-width="920">зарегистрируйтесь</a></div>
                    <!--p class="change_pass_mess"><?=Loc::getMessage('AUTH_PASSWORD_CHANGED')?></p-->
					<form action="<?=$arResult["AUTH_URL"]?>" class="b-form b-formlogin" name="system_auth_form<?=$arResult["RND"]?>" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">

						<?if($arResult["BACKURL"] <> '') {?>
							<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
							<?}?>

						<?foreach ($arResult["POST"] as $key => $value) {?>
							<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
							<?}?>

						<input type="hidden" name="AUTH_FORM" value="Y" />
						<input type="hidden" name="TYPE" value="AUTH" />
						<input type="hidden" name="USER_REMEMBER" value="Y" />

						<div class="fieldrow nowrap">
							<div class="fieldcell iblock">
								<div class="field validate">
									<input type="text" data-required="required" value="<?=$_REQUEST['USER_LOGIN']?>" id="settings11" data-maxlength="50" data-minlength="2" name="USER_LOGIN" class="input" placeholder="<?=Loc::getMessage('AUTH_MAIL')?>">
								</div>
							</div>

						</div>
						<div class="fieldrow nowrap">
							<div class="fieldcell iblock">
								<div class="field validate">
									<input type="password" data-required="required" value="<?=$_REQUEST['USER_PASSWORD']?>" id="settings10" data-maxlength="30" data-minlength="2" name="USER_PASSWORD" class="input" placeholder="<?=Loc::getMessage('AUTH_PASS')?>">										
								</div>
							</div>
						</div>
						<?
							if(!empty($arResult['ERROR_MESSAGE']['MESSAGE']) ||
								(is_string($arResult['ERROR_MESSAGE']) && !empty($arResult['ERROR_MESSAGE'])))
							{
								if(is_array($arResult['ERROR_MESSAGE']) && !empty($arResult['ERROR_MESSAGE']['MESSAGE']))
								{
									$arResult['ERROR_MESSAGE'] = $arResult['ERROR_MESSAGE']['MESSAGE'];
								}
								?>
								<em class="error_message"><?=$arResult['ERROR_MESSAGE'];?></em>
								<script type="text/javascript">
									$(function() {
										$('#login-link').click();
									});
								</script>
								<?
							}
						?>
						<div class="fieldrow nowrap fieldrowaction">
							<div class="fieldcell ">
								<div class="field clearfix">
									<button class="formbutton bt_typelt left" value="1" type="submit"><?=Loc::getMessage('AUTH_LOGIN_BUTTON')?></button>
									<a href="#" class="formlink right js_passrecovery"><?=Loc::getMessage('AUTH_FORGOT_PASS')?></a>
								</div>
							</div>
						</div>
					</form>
					<div class="b-passrform" style="display: none;">								
						<form action="/auth/?forgot_password=yes" method="post" class="b-passrecoveryform b-form">
							<input type="hidden" name="AUTH_FORM" value="Y">
							<input type="hidden" name="TYPE" value="SEND_PWD">
							<input type="hidden" name="backurl" value="/">
							<input type="hidden" name="send" value="1">
							<input type="hidden" name="IS_AJAX" value="Y">
							<div class="fieldrow">
								<div class="fieldcell iblock">
									<div class="b-warning hidden"><?=Loc::getMessage('AUTH_PASS_RECOVERY_EMAIL_NOT_FOUND')?></div>
									<label for="settings11"><?=Loc::getMessage('AUTH_PASS_RECOVERY_EMAIL_NOT_FOUND_DESC')?></label>
									<div class="field validate">
										<input type="text" data-required="required" value="" data-validate="email" id="settings11" data-maxlength="50" data-minlength="2" name="USER_EMAIL" class="input">
										<em class="error required" style="display: none"><?=Loc::getMessage('AUTH_PASS_RECOVERY_REQUIRED_FIELD')?></em>									
									</div>
								</div>
								<div class="fieldrow nowrap fieldrowaction">
									<div class="fieldcell ">
										<div class="field clearfix">
											<button class="formbutton js-recovery-submit bt_typelt" value="1" type="submit"><?=Loc::getMessage('AUTH_PASS_RECOVERY_RECOVER')?></button>
										</div>
									</div>
								</div>
							</div>
						</form>
						<p class="b-passconfirm hidden"><?=Loc::getMessage('AUTH_PASS_RECOVERY_SUCCESS')?></p>
					</div>
				</div><!-- /.b-loginform -->
				<?/*<div class="b-login_social iblock">
					<h4><?=Loc::getMessage('AUTH_SOCSERVICES');?></h4>
					<?foreach ($arResult['AUTH_SERVICES'] as $authService) {?>
						<a href="javascript:void(0)" onclick="<?=$authService['AUTH_LINK']?>" class="b-login_slnk <?=$authService['CSS_CLASS']?>"><?=$authService['NAME']?></a>
						<?}?>
				</div><!-- /.b-login_social -->*/?>
			</div>
		</div> 	<!-- /.b_login_popup -->
		<?
		}
		else 
		{?>
			<a href="#" id="login-link" class="b-headernav_login_lnk b-header_lock popup_opener_login b-header_lock current" data-height="265" data-width="320"></a>
			<div class="b_login_popup popup" style="display: none;">
				<div class="b_login_popup_in bbox">
					<a href="/profile/" class="logout  b-headernav_login_lnk">Личный кабинет</a>
					<a href="/?logout=yes" class="logout right b-headernav_login_lnk">Выйти</a>
				</div>
			</div>
			<!--a href="/profile/" id="login-link" class="b-headernav_login_lnk b-header_lock" ></a-->
			<?
			/*
		?>

		<a href="/profile/" class="b-headernav_login_lnk <?=strpos($APPLICATION->GetCurPage(), '/profile') !== false ? 'current':''?>"><?=Loc::getMessage('AUTH_LINK_PERSONAL');?></a>
		<a href="/?logout=yes" id="auth_login" class="b-headernav_login_lnk <?=strpos($APPLICATION->GetCurPage(), '/profile') === false ? 'current':''?>"><?=Loc::getMessage('AUTH_EXIT');?></a>
		<?
			*/
		}
		if($_REQUEST['login'] == 'yes' and $arResult['ERROR'])
		{
		?>
		<script>
			$( document ).ready(function() {
				$('#auth_login').click();
			});
		</script>
		<?
		}
	?>
</div>
