<?
$MESS["AUTH_REISTER_BUTTON"] = "Registration";
$MESS["AUTH_LOGIN_BUTTON"] = "Sign in";
$MESS["AUTH_TITLE"] = 'log in personal account';
$MESS["AUTH_NUMBER"] = "Library card number";
$MESS["AUTH_LOGIN"] = "Sign in";
$MESS["AUTH_BIRTDAY"] = "Birthday";
$MESS["AUTH_PASS"] = "Password";
$MESS["AUTH_FORGOT_PASS"] = "Forgot password?";
$MESS["AUTH_EXIT"] = "Sign out";
$MESS["AUTH_LINK_PERSONAL"] = "Profile";
$MESS["AUTH_MAIL"] = "E-mail / Login / EECHB";

$MESS["AUTH_SOCSERVICES"] = "through social networks";

$MESS['AUTH_PASS_RECOVERY_EMAIL_NOT_FOUND'] = 'E-mail not found';
$MESS['AUTH_PASS_RECOVERY_EMAIL_NOT_FOUND_DESC'] = 'Enter the e-mail specified at registration';
$MESS['AUTH_PASS_RECOVERY_REQUIRED_FIELD'] = 'Field is required';
$MESS['AUTH_PASS_RECOVERY_RECOVER'] = 'Restore';
$MESS['AUTH_PASS_RECOVERY_SUCCESS'] = 'You will receive an email with a link to reset your password';

$MESS["AUTH_REISTER_AS"] = "Registered as a";
$MESS["AUTH_READER"] = "Reader";
$MESS["AUTH_OR"] = "or";
$MESS["AUTH_CLOSE"] = "Close window";

$MESS["AUTH_PASSWORD_CHANGED"] = 'Ваш пароль успешно изменен. <br/>Для продолжения работы с порталом заново авторизуйтесь.';
?>