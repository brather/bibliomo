<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

function prepareMenuLink($sLink) {
	
	if (substr($sLink, 0, 1) == '#') {
		$arSections = ['/profile/form-6nk/', '/profile/statistic/', '/profile/accounting/'];
		foreach ($arSections as $sItem) {
			if (false !== strpos($_SERVER['REQUEST_URI'], $sItem))
				$sLink = '/profile/' . $sLink;
		}
	}

	return $sLink;
}

?>
<?if (!empty($arResult)):?>
	<ul>
		<?foreach($arResult as $arItem):?>
			<li><a<?=(isset($arItem["PARAMS"]["NnTabs"]) && $arItem["PARAMS"]["NnTabs"] == "Y") ? ' disabled="disabled"' : '';?>
					<?=(substr($arItem["LINK"], 0, 1) !== "#") ? 'class="notab"' : ''?>
					href="<?=prepareMenuLink($arItem["LINK"])?>"><?=$arItem["TEXT"]?></a></li>
		<?endforeach?>
	</ul>
	<script type="text/javascript">
		$(function(){
			$('div.tabs a[disabled]').unbind('click');
		});
	</script>
<?endif?>