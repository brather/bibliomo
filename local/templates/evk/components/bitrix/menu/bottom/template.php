<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (!empty($arResult)):?>
	<ul class="b-footer_nav">
	<?foreach($arResult as $arItem):?>
		<? if($arItem["LINK"] !== '/feedback/') : ?>
			<li><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
		<? else : ?>
			<li><a href="<?=$arItem["LINK"]?>" class="popup_opener ajax_opener closein" data-width="920"><?=$arItem["TEXT"]?></a></li>
		<? endif; ?>
	<?endforeach?>
	</ul>
<?endif?>

