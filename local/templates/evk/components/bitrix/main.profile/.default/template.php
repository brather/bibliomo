<?
	/**
	 * @global CMain $APPLICATION
	 * @param array $arParams
	 * @param array $arResult
	 */
	if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
		die();

	/*
	$date = explode('.', $arResult['arUser']['PERSONAL_BIRTHDAY']);

	if (!empty($arResult['arUser']['PERSONAL_BIRTHDAY']))
	{
		foreach($date as $dates)
		{
			$arDate[] = intval($dates);
		}
		$birthday = $arDate[2]."-".sprintf("%02d", $arDate[1])."-".sprintf("%02d", $arDate[0]);
	}
	else
		$birthday = "";
	*/
?>

<section class="wrapper">
	<h1 class="tilte_edit_profile">Настройки профиля</h1>
	<div id="edit_profile">
	<?
	// pre($arResult,1);?>
		<?ShowError($arResult["strProfileError"]);?>
			<?
				if ($arResult['DATA_SAVED'] == 'Y')
					ShowNote("Данные успешно сохранены!");
			?>			
		<fieldset class="right close">
			<legend class="b-form_header">Смена пароля</legend>
			<?$APPLICATION->IncludeComponent("neb:main.password.reset", "", Array());?>
		</fieldset>
   
		<fieldset>
			<legend class="b-form_header">Основные настройки</legend>
			<form method="post" action="<?=$arResult["FORM_TARGET"]?>" class="b-form b-form_common b-profile_setform" name="form1" enctype="multipart/form-data">
				<?=$arResult["BX_SESSION_CHECK"]?>
				<input type="hidden" name="lang" value="<?=LANG?>" />
				<input type="hidden" name="ID" value=<?=$arResult["ID"]?> />
				<input type="hidden" name="LOGIN" value="<?=$arResult['arUser']['LOGIN']?>">
				<table>
					<tr>
						<th>Отображаемое имя</th>
						<td>
							<input class="input" type="text" name="NAME"  value="<?=$arResult['arUser']['NAME']?>" data-maxlength="30" data-required="true" data-validate="alpha" data-minlength="5" />
							<em class="error required">&#171;Отображаемое имя&#187; не может быть пустым</em>
						</td>  
					</tr>
					<tr>
						<th>Адрес электронной почты</th>
						<td>
							<input class="input" type="email" name="EMAIL" value="<?=$arResult['arUser']['EMAIL']?>" data-validate="email"  data-required="true" />
							<em class="error required">Нам очень нужен ваш E-mail адрес</em>
						</td>
					</tr>
					<tr>
						<th>Пол</th>
						<td>
							<select name="PERSONAL_GENDER" class="js_select smallwidth"  data-required="true">									
							<option <?if($arResult['arUser']['PERSONAL_GENDER'] == 'M'){?> selected="selected" <?}?> value="M">мужской</option>
							<option <?if($arResult['arUser']['PERSONAL_GENDER'] == 'F'){?> selected="selected" <?}?> value="F">женский</option>
							</select>
						</td>
					</tr>
					<tr>
						<th>
							Дата рождения
							<dnf>Вам должно быть не меньше 12 лет</dnf>
						</th>
						<td>
							<input class="input datepicer" type="text" name="PERSONAL_BIRTHDAY" data-required="true" data-type="date" value="<?=$arResult['arUser']['PERSONAL_BIRTHDAY']?>" />
							<em class="error required">У книг тоже есть возрастные ограничения</em>
						</td>
					</tr>
					<tr>
						<th>Привязка к библиотеке</th>
						<td>
							<!--select name="library" data-required="required" class="js_select fullwidth">
								<option value="">выберите</option>
								<option value="42">Архангельская областная научная библиотека имени Н.А.Добролюбова</option>
								<option value="56">Центральная районная библиотека МУК «Егорьевская районная межпоселенческая центральная библиотека»</option>
								<option value="58">Центральная  библиотека  им. Д.Б. Кедрина МБУК «Библиотечно-информационный центр городского поселения Мытищи»</option>
								<option value="57">МУК "Центральная городская библиотека им. А.С. Горловского" городского поселения Сергиев Посад</option>
								<option selected="selected" value="55">Центральная  районная межпоселенческая библиотека МБУК «Клинская ЦБС» Клинского  муниципального района</option>
							</select-->
							<select name="UF_LIBRARY" data-required="required" class="js_select fullwidth">
								<option value="">выберите</option>
								<?
									$arUF_LIBRARY = nebLibrary::getLibs();
									if(!empty($arUF_LIBRARY))
									{
										foreach($arUF_LIBRARY['ITEMS'] as $arItem)
										{
										?>
											<option <?=$arResult['arUser']['UF_LIBRARY'] == $arItem['ID'] ? 'selected="selected"' : ''?> value="<?=$arItem['ID']?>"><?=$arItem['NAME']?></option>
										<?
										}
									}                                   
								?>
							</select>
						</td>
					</tr>
					<tr>
						<th colspan="2">Немного о себе</th>
					</tr>
					<tr>
						<td colspan="2">
						<textarea class="textarea input" name="PERSONAL_NOTES"  data-minlength="2" data-maxlength="800" ><?=$arResult['arUser']['PERSONAL_NOTES']?></textarea>
						<em class="error maxlength">Будет приятно если вы расскажите больше о себе</em>
						</td>
					</tr>
					<tr>
						<td colspan="2">
						<button name="save" class="formbtn bt_typelt" type="submit" value="1">Сохранить</button>
						<!--button name="save" class="formbtn" value="1" type="submit"><?=GetMessage('SAVE');?></button-->
						</td>
					</tr>
				</table>
			</form>
		</fieldset>
	</div>
</section>








<?/*
<section class="innersection innerwrapper clearfix">
	<div class="b-searchresult" style="border: medium none;">
		<?$APPLICATION->ShowViewContent('menu_top')?>
	</div>
	<div class="b-mainblock left">
		<div class="b-profile_set b-profile_brd rel">
			<h2><?=GetMessage('SETTING');?></h2>

			<?ShowError($arResult["strProfileError"]);?>
			<?
				if ($arResult['DATA_SAVED'] == 'Y')
					ShowNote(GetMessage('PROFILE_DATA_SAVED'));
			?>
			<?
				if($arResult['arUser']['EXTERNAL_AUTH_ID'] != 'socservices'){
				?>

                <?$APPLICATION->IncludeComponent("neb:main.password.reset", "", Array());?>

				<?
				}

            //тип регистрации ЕСИА? Скорем атрибут name у инпутов. ДЛя ЕСИА пользователей нельзя обновлять некоторые поля
            $isESIAUser = $arResult['arUser']['UF_REGISTER_TYPE'] == 40;
			?>

			<form method="post" name="form1" action="<?=$arResult["FORM_TARGET"]?>" class="b-form b-form_common b-profile_setform" enctype="multipart/form-data">
				<?=$arResult["BX_SESSION_CHECK"]?>
				<input type="hidden" name="lang" value="<?=LANG?>" />
				<input type="hidden" name="ID" value=<?=$arResult["ID"]?> />
				<div class="b-form_header"><?=GetMessage('GENERAL_CONST');?></div>

				<div class="fieldrow nowrap">
					<div class="fieldcell iblock">
						<label for="settings03"><?=GetMessage('LAST_NAME');?></label>
						<div class="field validate">
							<input type="text" data-required="required" value="<?=$arResult['arUser']['LAST_NAME']?>" id="settings03" data-maxlength="30" data-minlength="2" <?if($isESIAUser):?>disabled="disabled"<?else:?>name="LAST_NAME"<?endif;?> class="input">
						</div>
					</div>
				</div>				
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock">
						<label for="settings03"><?=GetMessage('NAME');?></label>
						<div class="field validate">
							<input type="text" data-required="required" value="<?=$arResult['arUser']['NAME']?>" id="settings03" data-maxlength="30" data-minlength="2" <?if($isESIAUser):?>disabled="disabled"<?else:?>name="NAME"<?endif;?> class="input">
						</div>
					</div>
				</div>
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock">
						<label for="settings03"><?=GetMessage('SECOND_NAME');?></label>
						<div class="field validate">
							<input type="text" data-required="false" value="<?=$arResult['arUser']['SECOND_NAME']?>" id="settings03" data-maxlength="30" data-minlength="2" <?if($isESIAUser):?>disabled="disabled"<?else:?>name="SECOND_NAME"<?endif;?> class="input">
						</div>
					</div>
				</div>
				<?
					if($arResult['arUser']['EXTERNAL_AUTH_ID'] != 'socservices'){
					?>
					<div class="fieldrow nowrap">
						<div class="fieldcell iblock">
							<label for="settings04"><?=GetMessage('MAIL');?></label>
							<div class="field validate">
								<input type="text" class="input" name="LOGIN" data-minlength="2" data-maxlength="50" id="settings04" data-validate="email" value="<?=$arResult['arUser']['LOGIN']?>" data-required="required">
								<input type="hidden" id='mail' class="input" value="<?=$arResult['arUser']['LOGIN']?>" name="EMAIL">									

							</div>
						</div>
					</div>
					<?
					}
				?>
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock">
						<label for="settings05"><?=GetMessage('POL');?></label>
						<div class="field validate">
							<select <?if($isESIAUser):?>disabled="disabled"<?else:?>name="PERSONAL_GENDER"<?endif;?> id="" data-required="required" class="js_select w270">
								<option <?if($arResult['arUser']['PERSONAL_GENDER'] == 'M'){?> selected="selected" <?}?> value="M"><?=GetMessage('MEN');?></option>
								<option <?if($arResult['arUser']['PERSONAL_GENDER'] == 'F'){?> selected="selected" <?}?> value="F"><?=GetMessage('WOMEN');?></option>
							</select>									
						</div>
					</div>
				</div>
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock">
						<label for="settings06"><?=GetMessage('DATE');?></label>
						<div class="field validate iblock seldate">
							<em class="error dateformat"><?=GetMessage('ERROR_DATE_FORMAT');?></em>
							<em class="error required"><?=GetMessage('CHOSE_DATE');?></em>
							<em class="error yearsrestrict"><?=GetMessage('12_OLD');?></em>
							<input type="hidden" class="realseldate" data-required="true" value="" id="settings06" <?if($isESIAUser):?>disabled="disabled"<?else:?>name="PERSONAL_BIRTHDAY"<?endif;?>>
							<select <?if($isESIAUser):?>disabled="disabled"<?else:?>name="birthday"<?endif;?> id="PERSONAL_BIRTHDAY_D"  class="js_select w102 sel_day"  data-required="true">
								<option value=""><?=GetMessage('DAY');?></option>
								<?for($i = 1; $i <= 31; $i++)
									{?>
									<option <?if($arDate['0'] == $i){?> selected='selected' <?}?> value="<?=$i?>"><?=$i?></option>
									<?}?>
							</select>
							<select <?if($isESIAUser):?>disabled="disabled"<?else:?>name="birthmonth"<?endif;?> id="PERSONAL_BIRTHDAY_M" class="js_select w165 sel_month"  data-required="true" >
								<option value=""><?=GetMessage('MONTH');?></option>
								<? for($i=1;$i<=12;$i++) { ?>
									<option <?if($arDate['1'] == $i){?> selected='selected' <?}?> value="<?=$i?>"><?=FormatDate('f', MakeTimeStamp('01.'.$i.'.'.date('Y')))?></option>
									<? } ?>
							</select>
							<select <?if($isESIAUser):?>disabled="disabled"<?else:?>name="birthyear"<?endif;?> id="PERSONAL_BIRTHDAY_Y"  class="js_select w102 sel_year"  data-required="true">
								<option value=""><?=GetMessage('YEAR');?></option>
								<? for($i=(date('Y')-14);$i>=(date('Y')-95);$i--)
									{?>
									<option <?if($arDate['2'] == $i){?> selected='selected' <?}?> value="<?=$i?>"><?=$i?></option>
									<?}?>
							</select>
						</div>									
					</div>
				</div>
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock">
						<label for="settings07"><?=GetMessage('ABOUT');?></label>
						<div class="field validate">
							<textarea class="textarea" name="PERSONAL_NOTES" id="settings07" data-minlength="2" data-maxlength="800" ><?=$arResult['arUser']['PERSONAL_NOTES']?></textarea>							
						</div>
					</div>
				</div>
                <?php $APPLICATION->IncludeComponent("bitrix:socserv.auth.split", ".default", array(
                        "SHOW_PROFILES" => "Y",
                        "ALLOW_DELETE" => "Y"
                    ),
                    false
                );?>
                <?$APPLICATION->IncludeComponent(
                    "notaext:plupload",
                    "scan_passport1",
                    array(
                        "MAX_FILE_SIZE" => "24",
                        "FILE_TYPES" => "jpg,jpeg,png",
                        "DIR" => "tmp_register",
                        "FILES_FIELD_NAME" => "profile_file",
                        "MULTI_SELECTION" => "N",
                        "CLEANUP_DIR" => "Y",
                        "UPLOAD_AUTO_START" => "Y",
                        "RESIZE_IMAGES" => "Y",
                        "RESIZE_WIDTH" => "110",
                        "RESIZE_HEIGHT" => "110",
                        "RESIZE_CROP" => "Y",
                        "RESIZE_QUALITY" => "98",
                        "UNIQUE_NAMES" => "Y",
                        "PERSONAL_PHOTO" => (intval($arResult['arUser']["UF_SCAN_PASSPORT1"]) > 0)? CFile::GetPath($arResult['arUser']["UF_SCAN_PASSPORT1"]):'',
                        "FULL_NAME" => $arResult['arUser']['NAME'].' '.$arResult['arUser']['LAST_NAME'],
                        "USER_EMAIL" => $arResult['arUser']['EMAIL'],
                        "DATE_REGISTER" => $arResult['arUser']['DATE_REGISTER'],
                        "UF_STATUS" => $arResult['arUser']['UF_STATUS'],
                    ),
                    false
                );?>
                <?$APPLICATION->IncludeComponent(
                    "notaext:plupload",
                    "scan_passport2",
                    array(
                        "MAX_FILE_SIZE" => "24",
                        "FILE_TYPES" => "jpg,jpeg,png",
                        "DIR" => "tmp_register",
                        "FILES_FIELD_NAME" => "profile_file",
                        "MULTI_SELECTION" => "N",
                        "CLEANUP_DIR" => "Y",
                        "UPLOAD_AUTO_START" => "Y",
                        "RESIZE_IMAGES" => "Y",
                        "RESIZE_WIDTH" => "110",
                        "RESIZE_HEIGHT" => "110",
                        "RESIZE_CROP" => "Y",
                        "RESIZE_QUALITY" => "98",
                        "UNIQUE_NAMES" => "Y",
                        "PERSONAL_PHOTO" => (intval($arResult['arUser']["UF_SCAN_PASSPORT2"]) > 0)? CFile::GetPath($arResult['arUser']["UF_SCAN_PASSPORT2"]):'',
                        "FULL_NAME" => $arResult['arUser']['NAME'].' '.$arResult['arUser']['LAST_NAME'],
                        "USER_EMAIL" => $arResult['arUser']['EMAIL'],
                        "DATE_REGISTER" => $arResult['arUser']['DATE_REGISTER'],
                        "UF_STATUS" => $arResult['arUser']['UF_STATUS'],
                    ),
                    false
                );?>
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock">
						<label for="settings05">Привязка к библиотеке</label>
						<div class="field validate">
							<select name="UF_LIBRARY" id="" data-required="required" class="js_select w270">
								<option value="">выберите</option>
								<?
									$arUF_LIBRARY = nebLibrary::getLibs();
									if(!empty($arUF_LIBRARY))
									{
										foreach($arUF_LIBRARY['ITEMS'] as $arItem)
										{
										?>
											<option <?=$arResult['arUser']['UF_LIBRARY'] == $arItem['ID'] ? 'selected="selected"' : ''?> value="<?=$arItem['ID']?>"><?=$arItem['NAME']?></option>
										<?
										}
									}                                   
								?>
							</select>
						</div>
					</div>
				</div>
				<div class="b-form_header"><?=GetMessage('SETTINGS');?></div>
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock b-srch_numset">
						<label for="settings08"><?=GetMessage('COUNT_SEARCH');?></label>
						<div class="field validate searchnum_set">
							<a href="#" data='15' class="cnts b-search_numset iblock <?if($arResult['arUser']['UF_SEARCH_PAGE_COUNT'] == 15){?> current <?}?>">15</a>	
							<a href="#" data='30' class="cnts b-search_numset iblock <?if($arResult['arUser']['UF_SEARCH_PAGE_COUNT'] == 30){?> current <?}?>">30</a>	
							<a href="#" data='45' class="cnts b-search_numset iblock <?if($arResult['arUser']['UF_SEARCH_PAGE_COUNT'] == 45){?> current <?}?>">45</a>								
						</div>
						<input type="hidden" class="" value="10" id="cnts" name="UF_SEARCH_PAGE_COUNT">

					</div>
					<?/**
						 <div class="fieldcell iblock hidden">
						 <label for="settings09"><?=GetMessage('MAX_COUNT');?></label>
						 <div class="field validate">
						 <input type="text" value="" id="settings09" data-maxlength="300" data-minlength="2" name="pass" class="input docnum">										
						 </div>
						 </div>
					 *//*?>
				</div>

				<div class="fieldrow nowrap fieldrowaction">
					<div class="fieldcell ">
						<div class="field clearfix">
							<button name="save" class="formbtn" value="1" type="submit"><?=GetMessage('SAVE');?></button>
						</div>
					</div>
				</div>

			</form>
		</div><!-- /.b-profilesettings-->
	</div><!-- /.b-mainblock -->
	<?#pre($arResult['arUser'],1);?>
	<?$APPLICATION->IncludeComponent(
			"notaext:plupload", 
			"profile", 
			array(
				"MAX_FILE_SIZE" => "24",
				"FILE_TYPES" => "jpg,jpeg,png",
				"DIR" => "tmp_register",
				"FILES_FIELD_NAME" => "profile_file",
				"MULTI_SELECTION" => "N",
				"CLEANUP_DIR" => "Y",
				"UPLOAD_AUTO_START" => "Y",
				"RESIZE_IMAGES" => "Y",
				"RESIZE_WIDTH" => "110",
				"RESIZE_HEIGHT" => "110",
				"RESIZE_CROP" => "Y",
				"RESIZE_QUALITY" => "98",
				"UNIQUE_NAMES" => "Y",
				"PERSONAL_PHOTO" => (intval($arResult['arUser']['PERSONAL_PHOTO']) > 0)? CFile::GetPath($arResult['arUser']['PERSONAL_PHOTO']):'',
				"FULL_NAME" => $arResult['arUser']['NAME'].' '.$arResult['arUser']['LAST_NAME'],
                "USER_EMAIL" => $arResult['arUser']['EMAIL'],
				"DATE_REGISTER" => $arResult['arUser']['DATE_REGISTER'],
				"UF_STATUS" => $arResult['arUser']['UF_STATUS'],
				"UF_REGISTER_TYPE" => $arResult['arUser']['UF_REGISTER_TYPE'],
				"UF_RGB_CARD_NUMBER" => $arResult['arUser']['UF_RGB_CARD_NUMBER'],
			),
			false
		);?>
</section>
</div><!-- /.homepage -->*/?>
