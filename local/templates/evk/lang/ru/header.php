<?
$MESS ['LEARNING_COURSE_DESCRIPTION'] = "Описание курса";
$MESS ['LEARNING_FORWARD'] = "Вперед";
$MESS ['LEARNING_BACK'] = "Назад";
$MESS ['LEARNING_EXPAND'] = "Раскрыть главу";
$MESS ['LEARNING_COLLAPSE'] = "Скрыть главу";
$MESS ['LEARNING_PRINT_PAGE'] = "Версия для печати";
$MESS ['LEARNING_ALL_COURSE_CONTENTS'] = "Все материалы курса";
$MESS ['LEARNING_GRADEBOOK'] = "Журнал";
$MESS ['LEARNING_LOGO_TEXT'] = "Учебный курс";
$MESS ['LEARNING_PASS_TEST'] = "Пройти сертификацию";
$MESS ['LEARNING_ALL_LESSONS'] = "Всего уроков";
$MESS ['LEARNING_CURRENT_LESSON'] = "Пройдено уроков";
$MESS ['WARNING_TEST_MODE'] = "Портал работает в тестовом режиме. В случае обнаружения технических проблем или некорректных данных";
$MESS ['WARNING_TEST_MODE_FEEDBACK'] = "сообщите нам";
$MESS ['WARNING_PROGRAM_VIEWING'] = "Внимание! Для чтения изданий, ограниченных авторским правом, необходимо установить на компьютере";
$MESS ['WARNING_PROGRAM_VIEWING2'] = "программу просмотра";
$MESS ['WARNING_POPUP_MESSAGE'] = "Вы пользуетесь устаревшей версией браузера. <br/>Не все функции будут работать в нем корректно. <br/>Смените на более новую версию";
$MESS ['SCANNED_BOOKS'] = "Отсканированные книги российских библиотек";
$MESS ['UNION_INFORMATION_SYSTEM'] = 'БИБЛИОТЕЧНЫЙ ПОРТАЛ <br/>МОСКОВСКОЙ ОБЛАСТИ';


?>
