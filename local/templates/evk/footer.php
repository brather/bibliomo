<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
Use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>
<?if(defined("STATIC_PAGE") && STATIC_PAGE):?>
	</div>
</section>
		<?endif;?>
</div><!-- /.homepage -->
<footer>
	<div class="wrapper">
		<div class="clearfix">
			<?$APPLICATION->IncludeFile("/local/include_areas/copyright.php", Array(), Array(
				"MODE" => "text",
			));?>
			<?$APPLICATION->IncludeComponent("bitrix:menu", "bottom", array(
				"ROOT_MENU_TYPE" => "bottom1_" . LANGUAGE_ID,
				"MENU_CACHE_TYPE" => "N",
				"MENU_CACHE_TIME" => "3600",
				"MENU_CACHE_USE_GROUPS" => "Y",
				"MENU_CACHE_GET_VARS" => array(
				),
				"MAX_LEVEL" => "1",
				"CHILD_MENU_TYPE" => "left",
				"USE_EXT" => "N",
				"DELAY" => "N",
				"ALLOW_MULTI_SELECT" => "N"
			),
				false
			);?>
			<?$APPLICATION->IncludeComponent("bitrix:menu", "bottom", array(
				"ROOT_MENU_TYPE" => "bottom2_" . LANGUAGE_ID,
				"MENU_CACHE_TYPE" => "N",
				"MENU_CACHE_TIME" => "3600",
				"MENU_CACHE_USE_GROUPS" => "Y",
				"MENU_CACHE_GET_VARS" => array(
				),
				"MAX_LEVEL" => "1",
				"CHILD_MENU_TYPE" => "left",
				"USE_EXT" => "N",
				"DELAY" => "N",
				"ALLOW_MULTI_SELECT" => "N"
			),
				false
			);?>
			<?$APPLICATION->IncludeComponent("bitrix:menu", "bottom", array(
				"ROOT_MENU_TYPE" => "bottom3_" . LANGUAGE_ID,
				"MENU_CACHE_TYPE" => "N",
				"MENU_CACHE_TIME" => "3600",
				"MENU_CACHE_USE_GROUPS" => "Y",
				"MENU_CACHE_GET_VARS" => array(
				),
				"MAX_LEVEL" => "1",
				"CHILD_MENU_TYPE" => "left",
				"USE_EXT" => "N",
				"DELAY" => "N",
				"ALLOW_MULTI_SELECT" => "N"
			),
				false
			);?>
		</div>
		<div class="b-footer_row clearfix">
			<div class="b-copyrule"><?=Loc::getMessage("MAIN_FOOTER_COPY_RULE")?></div>
			<a target="_blank" href="http://mosreg.ru/" class="b-footer_support">
				<?=Loc::getMessage("DEVELOPER_INFORMATION")?>
			</a>
		</div>
	</div><!-- /.wrapper -->
</footer>
<script src="http://api-maps.yandex.ru/2.0-stable/?load=package.full&lang=ru-RU" type="text/javascript"></script>
<? if ($_SERVER['SERVER_NAME'] == "bibliomo.ru"):?>
<!-- Yandex.Metrika counter --> <script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter40972134 = new Ya.Metrika({ id:40972134, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks"); </script> <noscript><div><img src="https://mc.yandex.ru/watch/40972134" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->
<? endif; ?>
</body>
</html>