<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
	Use \Bitrix\Main\Localization\Loc;
	Loc::loadMessages(__FILE__);
?>
<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="ru"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="ru"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="ru"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="ru"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<script src="<?=MARKUP?>js/libs/modernizr.min.js"></script>
	<meta name="viewport" content="width=device-width"/>
	
	<title><?$APPLICATION->ShowTitle()?></title>
	<?$APPLICATION->ShowHead();?>
	<link rel="icon" href="<?=MARKUP?>favicon.ico" type="image/x-icon" />
	<?$APPLICATION->SetAdditionalCSS(MARKUP.'css/style.css');?>
	<?$APPLICATION->AddHeadScript(MARKUP.'js/libs/jquery.min.js');?>
	<?$APPLICATION->AddHeadScript('/local/templates/.default/js/script.js');?>
	<?$APPLICATION->AddHeadScript(MARKUP.'js/slick.min.js');?>
	<?$APPLICATION->AddHeadScript(MARKUP.'js/plugins.js');?>
	<?$APPLICATION->AddHeadScript(MARKUP.'js/jquery.knob.js');?>
	<?$APPLICATION->AddHeadScript(MARKUP.'js/jquery.cookie.js');?>
	<?$APPLICATION->AddHeadScript(MARKUP.'js/blind.js');?>
	<?$APPLICATION->AddHeadScript(MARKUP.'js/script.js');?>
	<?$APPLICATION->AddHeadScript(MARKUP.'js/jquery.md5.js');?>
	<?$APPLICATION->AddHeadScript(MARKUP.'js/libs/jquery.ui.touch-punch.min.js');?>
	<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
	<?if($APPLICATION->GetCurPage() == '/'){?>
		<script>
			$(function() {
				$('html').addClass('mainhtml'); // это для морды
			}); // DOM loaded
		</script>
	<?}?>
	<script type="text/javascript" src="//vk.com/js/api/openapi.js?115"></script>
	<script type="text/javascript">VK.init({apiId: 4525156, onlyWidgets: true});</script>
	
</head>
<body class="blind <?=$APPLICATION->ShowProperty('view_theme');?>">
	<div class="oldie hidden"><?=GetMessage("WARNING_POPUP_MESSAGE")?></div>
	<div id="panel">
		<?$APPLICATION->ShowPanel();?>
	</div>
	<div class="homepage"> 
		<?//$APPLICATION->ShowViewContent("library_city_select");?>
		<header id="header" class="mainheader">
			<div class="wrapper clearfix">
				<?
					$APPLICATION->IncludeComponent("bitrix:system.auth.form", "main", 
						Array(
							"FORGOT_PASSWORD_URL" => "/auth/",
							"SHOW_ERRORS" => "N"
						),
						false
					);
				?>
				<?$APPLICATION->IncludeComponent(
					"bitrix:menu",
					"top",
					array(
						"ROOT_MENU_TYPE" => "top_".LANGUAGE_ID,
						"MENU_CACHE_TYPE" => "A",
						"MENU_CACHE_TIME" => "3600",
						"MENU_CACHE_USE_GROUPS" => "N",
						"MENU_CACHE_GET_VARS" => array(
						),
						"MAX_LEVEL" => "1",
						"CHILD_MENU_TYPE" => "left_".LANGUAGE_ID,
						"USE_EXT" => "N",
						"DELAY" => "N",
						"ALLOW_MULTI_SELECT" => "N"
					),
					false
				);?>
				
			</div>	<!-- /.wrapper -->			
			
		</header>
		<section class="asdasd <?=$APPLICATION->GetProperty('SECTION_HEADER')?>" >	
			<?
				if($APPLICATION->GetCurPage() == '/')
				{
				?>
				<div class="b-portalinfo clearfix wrapper">
					<?$APPLICATION->IncludeFile("/local/include_areas/book_count.php", Array(), Array(
							"MODE"      => "text",                                           
						));?>
					<?$APPLICATION->IncludeFile("/local/include_areas/logo.php", Array(), Array(
							"MODE"      => "text",                                           
						));?>
				</div>
				<?
				}
			?>		

			<!-- /.b-portalinfo -->
			<?$APPLICATION->IncludeComponent(
				"neb:search.form",
				"",
				Array(
					"PAGE" => ($APPLICATION->GetCurPage() == '/' ? 'MAIN': $APPLICATION->GetCurPage()),
				)
			);?>
		</section>
<?
if(defined('STATIC_PAGE')){
?>
<section class="innersection innerwrapper searchempty clearfix">
	<div class="b-mainblock left">
		<?$APPLICATION->IncludeComponent(
				"bitrix:menu", 
				"left", 
				array(
					"ROOT_MENU_TYPE" => "left",
					"MENU_CACHE_TYPE" => "A",
					"MENU_CACHE_TIME" => "3600",
					"MENU_CACHE_USE_GROUPS" => "N",
					"MENU_CACHE_GET_VARS" => array(
					),
					"MAX_LEVEL" => "1",
					"CHILD_MENU_TYPE" => "left",
					"USE_EXT" => "N",
					"DELAY" => "N",
					"ALLOW_MULTI_SELECT" => "N"
				),
				false
			);?>
		<div class="b-plaintext">
			<h2><?$APPLICATION->ShowTitle(false)?></h2>

<?
}
?>