<?
$MESS ['LEARNING_COURSE_DESCRIPTION'] = "Course description";
$MESS ['LEARNING_FORWARD'] = "Forward";
$MESS ['LEARNING_BACK'] = "Back";
$MESS ['LEARNING_LOGO_TEXT'] = "e-Learning";
$MESS ['LEARNING_PRINT_PAGE'] = "Print this page";
$MESS ['LEARNING_ALL_COURSE_CONTENTS'] = "All the course contents";
$MESS ['LEARNING_PASS_TEST'] = "Take a test";
$MESS ['LEARNING_ALL_LESSONS'] = "Total lessons";
$MESS ['LEARNING_GRADEBOOK'] = "Gradebook";
$MESS ['LEARNING_CURRENT_LESSON'] = "Completed lessons";
$MESS ['LEARNING_EXPAND'] = "Expand chapter";
$MESS ['LEARNING_COLLAPSE'] = "Hide chapter";
$MESS ['WARNING_TEST_MODE'] = "Portal is working in test mode. In case of technical problems or incorrect data";
$MESS ['WARNING_TEST_MODE_FEEDBACK'] = "let us know";
$MESS ['WARNING_PROGRAM_VIEWING'] = "Warning! To read publications, copyright restrictions, you must install";
$MESS ['WARNING_PROGRAM_VIEWING2'] = "the program on the computer viewing";
$MESS ['WARNING_POPUP_MESSAGE'] = "You are using an outdated browser version. <br/> Not all features will work correctly in it. <br/> Change to a newer version";
$MESS ['SCANNED_BOOKS'] = "Scanned books Russian libraries";
?>