<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?
if(defined('STATIC_PAGE')){
?>
		</div><!-- /.b-plaintext -->
	</div><!-- /.b-mainblock -->
</section>
<?
}
?>

</div><!-- /.homepage -->
<?/*$APPLICATION->IncludeComponent(
	"neb:personal.panel.small",
	"",
	Array(
	
	)
);*/?>
<footer>
	<div class="wrapper">
		<div class="b-footer_row clearfix">
			<?$APPLICATION->IncludeFile("/local/include_areas/logo_bottom.php", Array(), Array(
				"MODE" => "text",
			));?>
			<div class="footer-container">
			<?$APPLICATION->IncludeComponent("bitrix:menu", "bottom", array(
				"ROOT_MENU_TYPE" => "bottom1_" . LANGUAGE_ID,
				"MENU_CACHE_TYPE" => "N",
				"MENU_CACHE_TIME" => "3600",
				"MENU_CACHE_USE_GROUPS" => "Y",
				"MENU_CACHE_GET_VARS" => array(
				),
				"MAX_LEVEL" => "1",
				"CHILD_MENU_TYPE" => "left",
				"USE_EXT" => "N",
				"DELAY" => "N",
				"ALLOW_MULTI_SELECT" => "N"
				),
				false
			);?>
			<?$APPLICATION->IncludeComponent("bitrix:menu", "bottom", array(
				"ROOT_MENU_TYPE" => "bottom2_" . LANGUAGE_ID,
				"MENU_CACHE_TYPE" => "N",
				"MENU_CACHE_TIME" => "3600",
				"MENU_CACHE_USE_GROUPS" => "Y",
				"MENU_CACHE_GET_VARS" => array(
				),
				"MAX_LEVEL" => "1",
				"CHILD_MENU_TYPE" => "left",
				"USE_EXT" => "N",
				"DELAY" => "N",
				"ALLOW_MULTI_SELECT" => "N"
				),
				false
			);?>
			<?$APPLICATION->IncludeComponent("bitrix:menu", "bottom", array(
				"ROOT_MENU_TYPE" => "bottom3_" . LANGUAGE_ID,
				"MENU_CACHE_TYPE" => "N",
				"MENU_CACHE_TIME" => "3600",
				"MENU_CACHE_USE_GROUPS" => "Y",
				"MENU_CACHE_GET_VARS" => array(
				),
				"MAX_LEVEL" => "1",
				"CHILD_MENU_TYPE" => "left",
				"USE_EXT" => "N",
				"DELAY" => "N",
				"ALLOW_MULTI_SELECT" => "N"
				),
				false
			);?>
			<?$APPLICATION->IncludeFile("/local/include_areas/ministr.php", Array(), Array(
				"MODE"      => "text",                                           
			));?> 			
			</div>
		</div>

		</div><!-- /.wrapper -->
	</footer>
	
	<!--popup добавить в подборки-->
	<div class="selectionBlock">
		<a href="#" class="close"></a>
		<span class="selection_lb">Добавить в </span><a href="#" class="b-openermenu">мои подборки</a>
		<form class="b-selectionadd collection" action="<?=ADD_COLLECTION_URL?>add.php" method="post">
			<input type="hidden" name="t" value="books"/>
			<input type="submit" class="b-selectionaddsign" value="+">
			<span class="b-selectionaddtxt">Cоздать подборку</span>
			<input type="text" name="name" class="input hidden">
		</form>
	</div><!-- /.selectionBlock -->
	<!--/popup добавить в подборки-->

	<?
		$ALERT_REMOVE_MESSAGE = $APPLICATION->GetPageProperty("ALERT_REMOVE_MESSAGE");
	?>
	<!--popup Удалить из подборки--><div class="b-removepopup hidden"><p><?=empty($ALERT_REMOVE_MESSAGE)? 'Удалить книгу из Избранного?' : $ALERT_REMOVE_MESSAGE?></p><a href="#" class="formbutton btremove">Удалить</a><a href="#" class="formbutton gray">Оставить</a></div><!--/popup Удалить из подборки-->

    <div class="b-message hidden">
        <a href="#" class="closepopup">Закрыть окно</a>
        <p>Уважаемый читатель. В связи с изменением регламента доступа к изданиям, защищенным авторским правом, их просмотр возможен только со стационарных компьютеров до момента выхода новой версии ЕИСУБ для мобильных устройств. Приносим Вам свои извинения.</p>
    </div>

	
	<?$APPLICATION->IncludeFile("/local/include_areas/consultant.php", Array(), Array());?>
	
	   <script type="text/javascript">
		$('a[href^="spd"]').click(function (link) {
			//link.preventDefault();
			//console.log("111");
			//alert("!!!");
			
			/*var w = window.open(link, 'xyz', 'status=0,toolbar=0, menubar =0, height=0, width =0, top= -10, left=-10 ');
			if(w == null) {            
				//Work Fine
				link.preventDefault();
			}
			else {
				//link.preventDefault();
				w.close();
				if (confirm('Для чтения этой книги необходимо установить дополнительное программное обеспечение')) {
					window.location = 'SetupCustomProtocol.exe'; //Url for installer
				}
			}*/
		})
		</script>  
		  
	<style>
		.spd-warning{
			display:none;
			padding:10px;
			border:1px solid #000;
			position:absolute;
			z-index:9999;
			white-space:nowrap;
		}
	</style>
	
	<script type="text/javascript">
		/*$('a[href^="spd"]').after('<div class="spd-warning">Убедитесь что у вас установлена <a href="#">СПД</a></div>');
		$('a[href^="spd"]').on("mouseover", function (link) {
				
			$(this).next(".spd-warning").show();
			
			$(this).next(".spd-warning").css({
				'top': $(this).position().top,
				'left': $(this).position().left + $(this).width() + 6
			})
			
		})
		$('a[href^="spd"]').on("mouseleave", function (link) {
			$(this).next(".spd-warning").hide();
		})
		$('a[href^="spd"]').next(".spd-warning").on("mouseleave", function (link) {
			$(this).hide();
		})*/		
	</script>

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
	(w[c] = w[c] || []).push(function() {
		try {
			w.yaCounter27774831 = new Ya.Metrika({id:27774831,
					clickmap:true,
					trackLinks:true,
					accurateTrackBounce:true});
		} catch(e) { }
	});

	var n = d.getElementsByTagName("script")[0],
		s = d.createElement("script"),
		f = function () { n.parentNode.insertBefore(s, n); };
	s.type = "text/javascript";
	s.async = true;
	s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

	if (w.opera == "[object Opera]") {
		d.addEventListener("DOMContentLoaded", f, false);
	} else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/27774831" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
</body>
</html>