<?
	require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

	if(check_bitrix_sessid() and !empty($_POST['file']) and $USER->IsAuthorized())
	{
		$user = new CUser;
		$arFile = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"].$_REQUEST['file']);
		$arFile["MODULE_ID"] = "main";

		$arFields = array(
			'PERSONAL_PHOTO' => $arFile
		);

		$user->Update($USER->GetID(), $arFields);

	}
?>