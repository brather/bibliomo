<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
	$arTemplateParameters["UPLOAD_AUTO_START"] = array(
		"NAME" => 'Автоматический старт заугрзки файла',
		"TYPE" => "CHECKBOX",
		"MULTIPLE" => "N",
		"DEFAULT" => "N",
	);	

	$arTemplateParameters["UNIQUE_NAMES"] = array(
		"NAME" => 'Задавать уникальное имя для файла',
		"TYPE" => "CHECKBOX",
		"MULTIPLE" => "N",
		"DEFAULT" => "N",
	);

    $arTemplateParameters["ALREADY_UPLOADED_FILE"] = array(
        "NAME" => 'Уже загруженный файл',
        "TYPE" => "STRING",
        "MULTIPLE" => "N",
        "DEFAULT" => "N",
    );

	$arTemplateParameters["LINK_ID"] = array(
		"NAME" => 'Идентификатор прогресса',
		"TYPE" => "STRING",
		"MULTIPLE" => "N",
		"DEFAULT" => "add_file",
	);

	$arTemplateParameters["UPLOADED_FILE"] = array(
		"NAME" => 'Идентификатор ссылки',
		"TYPE" => "STRING",
		"MULTIPLE" => "N",
		"DEFAULT" => "uploaded_file",
	);

	$arTemplateParameters["FILE_PDF"] = array(
		"NAME" => 'Идентификатор позя загрузки',
		"TYPE" => "STRING",
		"MULTIPLE" => "N",
		"DEFAULT" => "FILE_PDF",
	);

	$arTemplateParameters["CHUNK_SIZE"] = array(
		"NAME" => 'Размер части',
		"TYPE" => "STRING",
		"MULTIPLE" => "N",
		"DEFAULT" => "1mb",
	);

?>