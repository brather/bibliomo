<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();?>
<?
	\CUtil::InitJSCore(array("jquery"));
	$APPLICATION->AddHeadString('<script src="'.$component->__path.'/lib/plupload/js/plupload.full.min.js"></script><script src="'.$component->__path.'/lib/plupload/js/i18n/'.LANGUAGE_ID.'.js"></script>',true);
?>
<div class="left">
	<label>Ссылка на pdf-версию книги</label>
    <?if (!empty($arParams['ALREADY_UPLOADED_FILE'])) {?>
        <span class="" id="<?=$arParams['UPLOADED_FILE']?>"><a target="_blank" href="<?=$arParams['ALREADY_UPLOADED_FILE']?>">файл загружен (скачать)</a></span>
    <?} else {?>
        <span class="" id="<?=$arParams['UPLOADED_FILE']?>"><a target="_blank" href=""></a></span>
    <?}?>
    <span class="file-progress hidden" id="<?=$arParams['LINK_ID']?>_progress"></span>
	<div class="field validate addpdfield" id="<?=$arParams['LINK_ID']?>_error">
		<input type="text" data-required="false" value="<?=$arParams['ALREADY_UPLOADED_FILE']?>" id="<?=$arParams['FILE_PDF']?>" name="<?=$arParams['FILE_PDF']?>" class="input hidden" />
		<!--em class="error required">Поле обязательно для заполнения</em-->	
		<em class="error upload"></em>	
	</div>
</div>

<a href="#" class="tdu bt_typelt bbox" id="<?=$arParams['LINK_ID']?>">Добавить pdf</a>
<div id="container_<?=$arParams['RAND_STR']?>" class="pl_button" style="display: none;"></div>
<pre style="display: none;" id="plupload_console_<?=$arParams['RAND_STR']?>"></pre>

<script type="text/javascript">
	$(function() {
		var uploader_<?=$arParams['RAND_STR']?> = new plupload.Uploader(
			{
				runtimes : 'html5,flash,html4',
				browse_button : '<?=$arParams['LINK_ID']?>',
				drop_element: "drop_upload",
				container: document.getElementById('container_<?=$arParams['RAND_STR']?>'), 
				<?
					if(!empty($arParams['UNIQUE_NAMES']) and $arParams['UNIQUE_NAMES'] == 'Y')
					{
					?>
					unique_names: true,
					<?
					}
				?>

				url : '<?=$component->__path?>/ajax.php',
				filters : {
					max_file_size : '<?=$arParams['MAX_FILE_SIZE']?>mb',
					prevent_duplicates: true
					<?
						if(!empty($arParams['FILE_TYPES'])){
						?>
						,
						mime_types: [
							{title : "Mine files", extensions : '<?=$arParams['FILE_TYPES']?>'}
						]
						<?
						}
					?>
				},
				<?
					if(!empty($arParams['RESIZE_IMAGES']) and $arParams['RESIZE_IMAGES'] == 'Y')
					{
					?>
					resize: {
						width: <?=intval($arParams['RESIZE_WIDTH'])?>,
						height: <?=intval($arParams['RESIZE_HEIGHT'])?>,
						<?
							if($arParams['RESIZE_CROP'] == 'Y')
							{
							?>
							crop: true,
							<?
							}
						?>
						quality: <?=intval($arParams['RESIZE_QUALITY'])?>
					},
					<?
					}
				?>
				max_file_size : '<?=$arParams['MAX_FILE_SIZE']?>mb',
				chunk_size: '<?=$arParams['CHUNK_SIZE']?>',
				flash_swf_url : '<?=$component->__path?>/lib/plupload/js/Moxie.swf',

				multipart_params: {
					plupload_ajax: 'Y',
					sessid: '<?=str_replace('sessid=', '', bitrix_sessid_get())?>',
					aFILE_TYPES : '<?=$arParams['FILE_TYPES']?>',
					aDIR : '<?=$arParams['DIR']?>',
					aMAX_FILE_SIZE : '<?=$arParams['MAX_FILE_SIZE']?>',
					aMAX_FILE_AGE : '<?=$arParams['MAX_FILE_AGE']?>',
					aFILES_FIELD_NAME : '<?=$arParams['FILES_FIELD_NAME']?>',
					aMULTI_SELECTION : '<?=$arParams['MULTI_SELECTION']?>',
					aCLEANUP_DIR : '<?=$arParams['CLEANUP_DIR']?>',
					aRAND_STR : '<?=$arParams['RAND_STR']?>'
				},
				multi_selection: false,
				init: {

					FilesAdded: function(up, files) {
						uploader_<?=$arParams['RAND_STR']?>.start();

						$('#<?=$arParams['LINK_ID']?>_progress').addClass('hidden');
						$('#<?=$arParams['LINK_ID']?>_error').removeClass('error');
						$('#<?=$arParams['LINK_ID']?>_error em.error').hide();
                        $('#<?=$arParams['UPLOADED_FILE']?>').addClass('hidden');

					},
					FileUploaded: function(up, file, response) {
						$('#<?=$arParams['LINK_ID']?>_error em.error').hide();
						$('#<?=$arParams['LINK_ID']?>_progress').addClass('hidden');
						var result = response.response;
						if (result) 
						{
							var obResponse = JSON.parse(result);
							$('#<?=$arParams['FILE_PDF']?>').val(obResponse.file);
                            $('#<?=$arParams['UPLOADED_FILE']?>').removeClass('hidden');
                            $('#<?=$arParams['UPLOADED_FILE']?> > a')
	                            .attr('href', obResponse.file)
	                            .text('файл загружен (скачать)');
						}
					},
					UploadProgress: function(up, file) {
						$('#<?=$arParams['LINK_ID']?>_progress').removeClass('hidden');
						$('#<?=$arParams['LINK_ID']?>_progress').text(file.percent+ '%');
					},

					Error: function(up, err) {
						$('#<?=$arParams['LINK_ID']?>_error em.error').hide();
						
						document.getElementById('plupload_console_<?=$arParams['RAND_STR']?>').innerHTML += err.message + '<br>';
						if(err.code == -601)
						{
							$('#<?=$arParams['LINK_ID']?>_error').addClass('error');
							$('#<?=$arParams['LINK_ID']?>_error em.error.upload').text('Книга должна быть в PDF формате');
							$('#<?=$arParams['LINK_ID']?>_error em.error.upload').show();

						}
					}
				}
			}
		);

		uploader_<?=$arParams['RAND_STR']?>.init();
	});
</script>
