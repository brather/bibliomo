<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();?>
<?
	\CUtil::InitJSCore(array("jquery"));
	$APPLICATION->AddHeadString('<script src="'.$component->__path.'/lib/plupload/js/plupload.full.min.js"></script><script src="'.$component->__path.'/lib/plupload/js/i18n/'.LANGUAGE_ID.'.js"></script>',true);
?>

<div class="fieldrow nowrap">
	<div class="fieldcell iblock w700 mt25">
		<label for="settings24">Прикрепить файл к новости</label>
		<div class="field validate">
			<div class="b-filechoose mt5">
				<input type="hidden" class="photofile" name="FILE_old" value="<?=$arParams['FILE']['SRC']?>">
				<input type="hidden" class="photofile" name="FILE_ID_old" value="<?=$arParams['FILE']['ID']?>">
				<input type="hidden" class="photofile" name="FILE_VALUE_ID" value="<?=$arParams['FILE']['VALUE_ID']?>">
				<input type="hidden" class="photofile" name="FILE" value="">
				<a id="setphoto_FILE" class="button_mode black" href="#">Выбрать файл</a>
				<span class="b-fileformat">форматы: .jpg / .png  / .doc  / .ppt  / .xls  / .pdf </span>
				<div class="checkwrapper file-del <?=empty($arParams['FILE'])?'hidden' : ''?>">
					<input class="gal_del checkbox" type="checkbox" name="FILE_del" value="Y"><label class="lite">Удалить</label>
				</div>
				<span id="FILE_fileinfo" class="library_file_wrapper <?=empty($arParams['FILE'])?'hidden' : ''?>">
					<a class="file-link" href="<?=$arParams['FILE']['SRC']?>"><?=$arParams['FILE']['FILE_NAME']?></a>
					<div class="field">
						<input type="text" name="FILE_DESCRIPTION" class="input description" value="<?=$arParams['FILE']['DESCRIPTION']?>" />
					</div>
				</span>
				<div class="progressbar" id="progressbar_FILE" style="display: none;">
					<div class="progress">
						<span style="width:0px;"></span>
					</div>
					<div class="text">Загружается <span class="num">0</span>%</div>
				</div>
			</div>
			<em class="error errformat">Файл данного формата не поддерживается</em>
		</div>
	</div>
</div>

<div style="display: none;" id="container_<?=$arParams['RAND_STR']?>" class="pl_button"></div>
<pre style="display: none;" id="plupload_console_<?=$arParams['RAND_STR']?>"></pre>

<script type="text/javascript">
	$(function() {
		var uploader_<?=$arParams['RAND_STR']?> = new plupload.Uploader(
			{
				runtimes : 'html5,flash,html4',
				browse_button : 'setphoto_FILE',
				drop_element: "setphoto_FILE",
				container: document.getElementById('container_<?=$arParams['RAND_STR']?>'), 
				<?
					if(!empty($arParams['UNIQUE_NAMES']) and $arParams['UNIQUE_NAMES'] == 'Y')
					{
					?>
					unique_names: true,
					<?
					}
				?>

				url : '<?=$component->__path?>/ajax.php',
				filters : {
					max_file_size : '<?=$arParams['MAX_FILE_SIZE']?>mb',
					prevent_duplicates: true
					<?
						if(!empty($arParams['FILE_TYPES'])){
						?>
						,
						mime_types: [
							{title : "Mine files", extensions : '<?=$arParams['FILE_TYPES']?>'}
						]
						<?
						}
					?>
				},
				<?
					if(!empty($arParams['RESIZE_IMAGES']) and $arParams['RESIZE_IMAGES'] == 'Y')
					{
					?>
					resize: {
						width: <?=intval($arParams['RESIZE_WIDTH'])?>,
						height: <?=intval($arParams['RESIZE_HEIGHT'])?>,
						<?
							if($arParams['RESIZE_CROP'] == 'Y')
							{
							?>
							crop: true,
							<?
							}
						?>
						quality: <?=intval($arParams['RESIZE_QUALITY'])?>
					},
					<?
					}
				?>
				max_file_size : '<?=$arParams['MAX_FILE_SIZE']?>mb',
				chunk_size: '100kb',
				flash_swf_url : '<?=$component->__path?>/lib/plupload/js/Moxie.swf',

				multipart_params: {
					plupload_ajax: 'Y',
					sessid: '<?=str_replace('sessid=', '', bitrix_sessid_get())?>',
					aFILE_TYPES : '<?=$arParams['FILE_TYPES']?>',
					aDIR : '<?=$arParams['DIR']?>',
					aMAX_FILE_SIZE : '<?=$arParams['MAX_FILE_SIZE']?>',
					aMAX_FILE_AGE : '<?=$arParams['MAX_FILE_AGE']?>',
					aFILES_FIELD_NAME : '<?=$arParams['FILES_FIELD_NAME']?>',
					aMULTI_SELECTION : '<?=$arParams['MULTI_SELECTION']?>',
					aCLEANUP_DIR : '<?=$arParams['CLEANUP_DIR']?>',
					aRAND_STR : '<?=$arParams['RAND_STR']?>'

				},
				multi_selection: false,
				init: {

					FilesAdded: function(up, files) {
						up.start();
					},
					FileUploaded: function(up, file, response) {
						var result = response.response;
						if (result) {
							var obResponse = JSON.parse(result);
							$('input[name="FILE"]').val(obResponse.file);

							$('#progressbar_FILE').hide();
							$('#FILE_fileinfo').removeClass('hidden');
							$('a.file-link', '#FILE_fileinfo').attr('href', obResponse.file).text(file.name);
							$('input.description', '#FILE_fileinfo').val(file.name);
							$('.file-del').removeClass('hidden');
						}
					},
					UploadProgress: function(up, file) {
						$('#progressbar_FILE').show();
						$('#progressbar_FILE .progress span').css('width' , file.percent+"%");
						$('#progressbar_FILE .text .num').text(file.percent);
					},
					Error: function(up, err) {
						document.getElementById('plupload_console_<?=$arParams['RAND_STR']?>').innerHTML += err.message + '<br>';
					}
				}
			}
		);

		uploader_<?=$arParams['RAND_STR']?>.init();
	});
</script>