<?
	require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

	if(check_bitrix_sessid() and !empty($_POST['file']) and $USER->IsAuthorized())
	{
		if(isset($_REQUEST["USER_ID"]) && intval($_REQUEST["USER_ID"]) > 0)
		{
			CModule::IncludeModule('nota.userdata');
			$USER_ID = intval($_REQUEST["USER_ID"]);
			$obCUser = new nebUser(); // Текущий пользователь
			$obEUser = new nebUser($USER_ID); // Редактируемый пользователь
			$aCLibrary = $obCUser->getLibrary();
			$aELibrary = $obEUser->getLibrary();
			if($aCLibrary["ID"] != $aELibrary["ID"])
			{
				die();
			}
		}
		else
		{
			$USER_ID = $USER->GetID();
		}
		$user = new CUser;
		$arFile = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"].$_REQUEST['file']);
		$arFile["MODULE_ID"] = "main";

		$arFields = array(
			'PERSONAL_PHOTO' => $arFile
		);

		$user->Update($USER_ID, $arFields);

	}
?>