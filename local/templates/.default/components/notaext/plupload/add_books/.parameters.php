<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
	$arTemplateParameters["UPLOAD_AUTO_START"] = array(
		"NAME" => 'Автоматический старт заугрзки файла',
		"TYPE" => "CHECKBOX",
		"MULTIPLE" => "N",
		"DEFAULT" => "N",
	);	

	$arTemplateParameters["UNIQUE_NAMES"] = array(
		"NAME" => 'Задавать уникальное имя для файла',
		"TYPE" => "CHECKBOX",
		"MULTIPLE" => "N",
		"DEFAULT" => "N",
	);

$arTemplateParameters["IS_NOT_REQUIRED"] = array(
	"NAME" => 'Необязательно для заполнения',
	"TYPE" => "CHECKBOX",
	"MULTIPLE" => "N",
	"DEFAULT" => "N",
);
?>