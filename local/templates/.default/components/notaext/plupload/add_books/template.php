<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();?>
<?
	\CUtil::InitJSCore(array("jquery"));
	$APPLICATION->AddHeadString('<script src="'.$component->__path.'/lib/plupload/js/plupload.full.min.js"></script><script src="'.$component->__path.'/lib/plupload/js/i18n/'.LANGUAGE_ID.'.js"></script>',true);
?>
<div class="left">
	<p>Ссылка на pdf-версию книги</p>
	<div class="b-pdfaddwrapper" id='add-file-block'>
		<a href="#" class="tdu" id="add_file"><?=empty($arParams['ALREADY_UPLOADED_FILE']) ? 'Добавить' : 'Обнивить'?> pdf</a> <em class="errorformat file hidden" id="error-format">Ошибка. Неверный формат файла</em>
	</div> 

	<div class="field validate addpdfield" style="top:11px;">
		<input type="text"<?=(empty($arParams['IS_NOT_REQUIRED']) || $arParams['IS_NOT_REQUIRED']=="N" ? ' data-required="required"' : '')?> value="<?=$arParams['ALREADY_UPLOADED_FILE']?>" id="FILE_PDF" data-maxlength="300" data-minlength="2" name="FILE_PDF" class="input hidden" />
		<em class="error required">Загрузите файл</em>
	</div>	
	<div class="b-pdfwrapper hidden" id="pdfwrapper">
		<div class="b-fondtitle b-filename"><span class="b-fieldtext">file.pdf</span><a class="b-fieldeedit" href="#" id="fieldeedit"></a></div>	
		<div class="b-loaded">Загружено: 100%</div>
		<em class="errorformat file hidden">Ошибка. Неверный формат файла</em>
	</div>

	<div class="progressbar hidden" id="progressbar"><div class="progress"><span id="file-procent" style="width:50px;"></span></div><div class="text">Загружено: <span class="num">33</span>%</div></div>
</div>
<div id="container_<?=$arParams['RAND_STR']?>" class="pl_button" style="display: none;"></div>
<pre style="display: none;" id="plupload_console_<?=$arParams['RAND_STR']?>"></pre>
<style type="text/css">
	.b-pdfwrapper .errorformat{	margin-left: -118px;}
</style>
<script type="text/javascript">
	$(function() {
		var uploader_<?=$arParams['RAND_STR']?> = new plupload.Uploader(
			{
				runtimes : 'html5,flash,html4',
				browse_button : 'add_file',
				drop_element: "drop_upload",
				container: document.getElementById('container_<?=$arParams['RAND_STR']?>'), 
				<?
					if(!empty($arParams['UNIQUE_NAMES']) and $arParams['UNIQUE_NAMES'] == 'Y')
					{
					?>
					unique_names: true,
					<?
					}
				?>

				url : '<?=$component->__path?>/ajax.php',
				filters : {
					max_file_size : '<?=$arParams['MAX_FILE_SIZE']?>mb',
					prevent_duplicates: true
					<?
						if(!empty($arParams['FILE_TYPES'])){
						?>
						,
						mime_types: [
							{title : "Mine files", extensions : '<?=$arParams['FILE_TYPES']?>'}
						]
						<?
						}
					?>
				},
				<?
					if(!empty($arParams['RESIZE_IMAGES']) and $arParams['RESIZE_IMAGES'] == 'Y')
					{
					?>
					resize: {
						width: <?=intval($arParams['RESIZE_WIDTH'])?>,
						height: <?=intval($arParams['RESIZE_HEIGHT'])?>,
						<?
							if($arParams['RESIZE_CROP'] == 'Y')
							{
							?>
							crop: true,
							<?
							}
						?>
						quality: <?=intval($arParams['RESIZE_QUALITY'])?>
					},
					<?
					}
				?>
				max_file_size : '<?=$arParams['MAX_FILE_SIZE']?>mb',
				chunk_size: '1mb',
				flash_swf_url : '<?=$component->__path?>/lib/plupload/js/Moxie.swf',

				multipart_params: {
					plupload_ajax: 'Y',
					sessid: '<?=str_replace('sessid=', '', bitrix_sessid_get())?>',
					aFILE_TYPES : '<?=$arParams['FILE_TYPES']?>',
					aDIR : '<?=$arParams['DIR']?>',
					aMAX_FILE_SIZE : '<?=$arParams['MAX_FILE_SIZE']?>',
					aMAX_FILE_AGE : '<?=$arParams['MAX_FILE_AGE']?>',
					aFILES_FIELD_NAME : '<?=$arParams['FILES_FIELD_NAME']?>',
					aMULTI_SELECTION : '<?=$arParams['MULTI_SELECTION']?>',
					aCLEANUP_DIR : '<?=$arParams['CLEANUP_DIR']?>',
					aRAND_STR : '<?=$arParams['RAND_STR']?>'
				},
				multi_selection: false,
				//autostart: true,
				init: {

					FilesAdded: function(up, files) {
						up.start();

						$('#pdfwrapper .b-fieldtext').text(files[0].name);

						$('.errorformat').addClass('hidden');
						$('.addpdfield .error').hide();
						$('.addpdfield').removeClass('error');
						$('#pdfwrapper').addClass('hidden');
						$('#progressbar').addClass('hidden');

					},
					FileUploaded: function(up, file, response) {
						$('#progressbar').addClass('hidden');						
						$('#pdfwrapper').removeClass('hidden');
						$('.b-loaded').removeClass('hidden');

						var result = response.response;

						if (result) 
						{
							var obResponse = JSON.parse(result);
							$('#FILE_PDF').val(obResponse.file);
						}
						if(up.files[0])
							up.removeFile(up.files[0].id);
						up.setOption('browse_button', 'fieldeedit');
						up.refresh();							
					},
					UploadProgress: function(up, file) {
						$('#add-file-block').addClass('hidden');
						$('#progressbar').removeClass('hidden');
						$('#progressbar #file-procent').css('width', file.percent+ '%');
						$('#progressbar .num').text(file.percent);
					},

					Error: function(up, err) {
						$('.addpdfield .error').hide();
						$('.addpdfield').removeClass('error');
						console.log(err);
						document.getElementById('plupload_console_<?=$arParams['RAND_STR']?>').innerHTML += err.message + '<br>';
						if(err.code == -601)
						{
							$('.errorformat').removeClass('hidden');
							$('.b-loaded').addClass('hidden');
						}
					}
				}
			}
		);

		uploader_<?=$arParams['RAND_STR']?>.init();
	});
</script>