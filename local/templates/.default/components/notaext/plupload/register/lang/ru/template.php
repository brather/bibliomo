<?php

$MESS['BROWSER_NOT_SUPPORTED'] = 'Ваш браузер не поддерживает загрузку файлов :-(';
$MESS['NO_FILE_SELECTED'] = 'Файл не выбран';
$MESS['SELECT_FILE'] = 'Выбрать файл';
$MESS['SELECT_FILES'] = 'Выбрать файлы';
$MESS['UPLOAD'] = 'Загрузить';

$MESS["PLUPLOAD_REGISTER_SET"] = 'Установить фотографию';
$MESS["PLUPLOAD_REGISTER_DROP"] = 'или перенесите изображение на это поле';