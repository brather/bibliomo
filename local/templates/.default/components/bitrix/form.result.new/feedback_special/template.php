<?
	if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

	$arResult["FORM_HEADER"] = str_replace('<form', '<form class="b-form b-form_common b-feedbackform"', $arResult["FORM_HEADER"]);

	#	pre($arResult,1);

	if($USER->IsAuthorized() and empty($arResult['arrVALUES']['form_email_1']))
		$arResult['arrVALUES']['form_email_1'] = $USER->GetEmail();

?>
<section class="innersection innerwrapper searchempty clearfix">
	<div class="b-mainblock left">
		<div class="b-plaintext">
			<h2>Обратная связь</h2>

			<?if ($arResult["isFormErrors"] == "Y"):?><?=$arResult["FORM_ERRORS_TEXT"];?><?endif;?>
			<?=$arResult["FORM_HEADER"]?>

			<div class="fieldrow nowrap">
				<div class="fieldcell iblock">
					<em class="hint">*</em>
					<label for="fb_email">Электронная почта</label>
					<div class="field validate">
						<input type="text" class="input" name="form_email_1" data-minlength="2" data-maxlength="50" id="fb_email" data-validate="email" value="<?=$arResult['arrVALUES']['form_email_1']?>" data-required="required">
					</div>
				</div>
			</div>

			<div class="fieldrow nowrap ptop">
				<div class="fieldcell iblock">
					<label for="fb_theme">Тема:</label>
					<div class="field">
						<?=SelectBoxFromArray("form_dropdown_SIMPLE_QUESTION_805", $arResult['arDropDown']['SIMPLE_QUESTION_805'], $arResult['arrVALUES']['form_dropdown_SIMPLE_QUESTION_805'], "", ' id="fb_theme" data-required="required" class="js_select w370"')?>
					</div>
				</div>
			</div>

			<div class="fieldrow nowrap">
				<div class="fieldcell iblock mt10">
					<em class="hint">*</em>
					<label for="fb_message">Ваше сообщение:</label>
					<div class="field validate txtarea">
						<textarea id="fb_message" data-maxlength="1500" data-required="required" data-minlength="2" name="form_textarea_6" class="input" ><?=$arResult['arrVALUES']['form_textarea_6']?></textarea>											
					</div>
				</div>
			</div>

            <?if($this->__name == 'feedback_special'):?>
                <input type="hidden" name="captcha_sid" value="<?=htmlspecialcharsbx($arResult["CAPTCHACode"]);?>" />
                <input type="hidden" name="captcha_word" value="<?=$arResult["CAPTCHA_CODE"];?>" />
            <?else:?>
                <input type="hidden" name="captcha_sid" value="<?=htmlspecialcharsbx($arResult["CAPTCHACode"]);?>" />
                <div class="fieldrow nowrap">
                    <div class="fieldcell iblock">
                        <em class="hint">*</em>
                        <label for="bt_captcha">Код с картинки:</label>
                        <div class="field validate captcha">
                            <input type="text" class="input" name="captcha_word" id="bt_captcha" value="" data-required="required">
                            <img src="/bitrix/tools/captcha.php?captcha_sid=<?=htmlspecialcharsbx($arResult["CAPTCHACode"]);?>" width="180" height="40" />
                        </div>
                    </div>
                </div>
            <?endif;?>

			<div class="fieldrow nowrap fieldrowaction">
				<div class="fieldcell ">
					<div class="field clearfix">
						<button class="formbutton" value="1" type="submit" name="web_form_submit">Отправить письмо!</button>

					</div>
				</div>
			</div>

			<?=$arResult["FORM_FOOTER"]?>
		</div><!-- /.b-plaintext -->
	</div><!-- /.b-mainblock -->
</section>
