<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

//в версии для слабовидящих капчу не выводим
//получим ответ на капчу сразу и вставим его в скрытый инпут на страницу
if($this->__name == 'feedback_special'){
    $captcha = new CCaptcha();
    $captcha->InitCode($arResult["CAPTCHACode"]);
    $arResult['CAPTCHA_CODE'] = $captcha->code;
}