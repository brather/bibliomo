<?
	if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

	$arResult["FORM_HEADER"] = str_replace('<form', '<form class="b-form b-form_common b-feedbackform"', $arResult["FORM_HEADER"]);

	#	pre($arResult,1);

	if($USER->IsAuthorized() and empty($arResult['arrVALUES']['form_email_1']))
		$arResult['arrVALUES']['form_email_1'] = $USER->GetEmail();
?>
<section class="innersection innerwrapper searchempty clearfix">
	<div class="b-mainblock left">
		<div class="b-plaintext">
			<h2><?=GetMessage("FORM_FEEDBACK_TITLE");?></h2>			

			<?if ($arResult["isFormErrors"] == "Y"):?><?=$arResult["FORM_ERRORS_TEXT"];?><?endif;?>
			<?=$arResult["FORM_HEADER"]?>
			<?=$arResult["arForm"]["DESCRIPTION"]
			/*
			<div class="feedback-contacts">
				<h3>Контактные данные службы поддержки</h3>
				<div class="feedback-address">
			 <span class="contacts-description">Адрес</span>
					Пр. Королева 24, г. Королев, Россия, 129128
				</div>
				<div class="feedback-phone">
			 <span class="contacts-description">Телефоны</span>
					+7 (499) 557 0470
				</div>
				<div class="feedback-email">
			 <span class="contacts-description">Электронная почта</span> <a href="mailto:info@demolib.ru">info@demolib.ru</a>
				</div>
			</div>
			*/
			?>
			<div class="feedback-formblock">
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock feedback-field-email">
						<em class="hint">*</em>
						<label for="fb_email"><?=GetMessage("FORM_FIELD_EMAIL")?></label>
						<div class="field validate">
							<input type="text" class="input" name="form_email_1" data-minlength="2" data-maxlength="50" id="fb_email" data-validate="email" value="<?=$arResult['arrVALUES']['form_email_1']?>" data-required="required">
						</div>
					</div>

					<div class="fieldcell iblock feedback-field-subject">
						<label for="fb_theme"><?=GetMessage("FORM_FIELD_SUBJECT")?></label>
						<div class="field">
							<?
							$arDropDown = array();
							foreach ($arResult['arDropDown']['SIMPLE_QUESTION_805']["reference"] as $subject)
							{
								if (strrpos($subject, "/") !== false)
								{
									$arSubjects = explode("/", $subject);
									if (LANGUAGE_ID == 'ru')
									{
										$arDropDown[] = $arSubjects[0];
									}
									elseif (LANGUAGE_ID == 'en')
									{
										$arDropDown[] = $arSubjects[1];
									}
								}
								else
								{
									$arDropDown[] = $subject;
								}
							}
							if (!empty($arDropDown))
							{
								$arResult['arDropDown']['SIMPLE_QUESTION_805']["reference"] = $arDropDown;
							}
							?>
							<?=SelectBoxFromArray("form_dropdown_SIMPLE_QUESTION_805", $arResult['arDropDown']['SIMPLE_QUESTION_805'], $arResult['arrVALUES']['form_dropdown_SIMPLE_QUESTION_805'], "", ' id="fb_theme" data-required="required" class="js_select w370"')?>
						</div>
					</div>
				</div>

				<div class="fieldrow nowrap">
					<div class="fieldcell iblock mt10 feedback-field-message">
						<em class="hint">*</em>
						<label for="fb_message"><?=GetMessage("FORM_FIELD_MESSAGE")?></label>
						<div class="field validate txtarea">
							<textarea id="fb_message" data-maxlength="1500" data-required="required" data-minlength="2" name="form_textarea_6" class="input" ><?=$arResult['arrVALUES']['form_textarea_6']?></textarea>											
						</div>
					</div>
				</div>
				
				<input type="hidden" name="captcha_sid" value="<?=htmlspecialcharsbx($arResult["CAPTCHACode"]);?>" />
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock feedback-field-captcha">
						<em class="hint">*</em>
						<label for="bt_captcha"><?=GetMessage("FORM_FIELD_CAPTCHA_CODE")?></label>
						<div class="field validate captcha">
							<input type="text" class="input" name="captcha_word" id="bt_captcha" value="" data-required="required">
							<img src="/bitrix/tools/captcha.php?captcha_sid=<?=htmlspecialcharsbx($arResult["CAPTCHACode"]);?>" width="180" height="40" />
						</div>
					</div>
					<div class="fieldcell iblock feedback-field-submit">
						<div class="field clearfix">
							<button class="formbutton bt_typelt" value="1" type="submit" name="web_form_submit"><?=GetMessage("FORM_BUTTON_SUBMIT")?></button>

						</div>
					</div>
				</div>
            </div>
			<?=$arResult["FORM_FOOTER"]?>
		</div><!-- /.b-plaintext -->
	</div><!-- /.b-mainblock -->
</section>
