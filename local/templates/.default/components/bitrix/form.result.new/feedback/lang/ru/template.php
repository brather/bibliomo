<?
$MESS ['FORM_REQUIRED_FIELDS'] = "Поля, обязательные для заполнения";
$MESS ['FORM_APPLY'] = "Применить";
$MESS ['FORM_ADD'] = "Добавить";
$MESS ['FORM_ACCESS_DENIED'] = "Не хватает прав доступа к веб-форме.";
$MESS ['FORM_DATA_SAVED1'] = "Спасибо!<br><br>Ваша заявка №";
$MESS ['FORM_DATA_SAVED2'] = " принята к рассмотрению.";
$MESS ['FORM_MODULE_NOT_INSTALLED'] = "Модуль веб-форм не установлен.";
$MESS ['FORM_NOT_FOUND'] = "Веб-форма не найдена.";
$MESS ['FORM_FEEDBACK_TITLE'] = "Обратная связь";
$MESS ['FORM_FIELD_EMAIL'] = "E-mail";
$MESS ['FORM_FIELD_SUBJECT'] = "Тема";
$MESS ['FORM_FIELD_MESSAGE'] = "Ваше сообщение";
$MESS ['FORM_FIELD_CAPTCHA_CODE'] = "Код с картинки";
$MESS ['FORM_BUTTON_SUBMIT'] = "Отправить письмо";
?>
