<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use \Bitrix\Main\Localization\Loc as Loc;
Loc::loadMessages(__FILE__);

if ($_POST['IS_AJAX'] == 'Y' && $_POST['AUTH_FORM'] == 'Y')
{
	global $APPLICATION;
	$APPLICATION->RestartBuffer();
	
	if ($arParams['AUTH_RESULT']['TYPE'] == 'OK')
	{
		$result = array(
			'error_message' => '',
			'description' => Loc::getMessage('AUTH_PASS_RECOVERY_EMAIL_NOT_FOUND')
		);
	}
	else
	{
		$result = array(
			'error_message' => Loc::getMessage('AUTH_PASS_RECOVERY_EMAIL_NOT_FOUND'),
			'description' => Loc::getMessage('AUTH_PASS_RECOVERY_TRY_AGAIN')
		);
	}
	
	echo json_encode($result);
	
	die;
}
