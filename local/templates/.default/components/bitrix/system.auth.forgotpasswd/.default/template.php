<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="wrapper">
<section class="innersection innerwrapper clearfix">
	<div class="b-formsubmit" style='display: block!important'>
		<?if($_REQUEST['send']){?>
			<p><? ShowMessage($arParams["~AUTH_RESULT"]); ?></p>
		<?}?>
		
	</div>
	<div class="b-passform rel">
			<h2 class="mode"><?=GetMessage('RECOVERY');?></h2>
			<form name="bform" class="b-form b-form_common b-passchge" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">
				<?if (strlen($arResult["BACKURL"]) > 0)
				{?>
					<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
				<?}?>
					<input type="hidden" name="AUTH_FORM" value="Y">
					<input type="hidden" name="TYPE" value="SEND_PWD">
					<p>
					<?=GetMessage("AUTH_FORGOT_PASSWORD_1")?>
					</p>
				<hr>
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock">
						<em class="hint">*</em>
						<label for="settings55"><?=GetMessage("AUTH_EMAIL")?></label>
						<div class="field validate">
							<input type="text" data-required="required" value="" id="settings55" data-maxlength="30" data-minlength="2" name="USER_LOGIN" class="input" data-validate="password">										
							<em class="error"><?=GetMessage('REQUIRED');?></em>
						</div>
					</div>
				</div>
				<div class="fieldrow nowrap fieldrowaction">
					<div class="fieldcell ">
						<div class="field clearfix">
							<button class="formbutton" name="send" value="1" type="submit"><?=GetMessage('SEND');?></button>
						</div>
					</div>
				</div>
			</form>
		</div><!-- /.b-registration-->
	</section>
</div><!-- /.homepage -->