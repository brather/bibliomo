<?
$MESS ['AUTH_FORGOT_PASSWORD_1'] = "If you have forgotten your password, enter your Email.<br/> The check line for password change and your registration data will be sent to your Email.";
$MESS ['AUTH_GET_CHECK_STRING'] = "The check line and your registration data have been sent to your Email.<br/> Please, wait for the message as the check line changes with each request.";
$MESS ['AUTH_SEND'] = "Send";
$MESS ['AUTH_AUTH'] = "Authorization";
$MESS ['AUTH_LOGIN'] = "Login:";
$MESS ['AUTH_OR'] = "or";
$MESS ['AUTH_EMAIL'] = "E-Mail:";
$MESS ['RECOVERY'] = "Password recovery";


$MESS['AUTH_PASS_RECOVERY_EMAIL_NOT_FOUND'] = 'Email is not found';
$MESS['AUTH_PASS_RECOVERY_EMAIL_NOT_FOUND_DESC'] = 'Enter the e-mail specified at registration';
$MESS['AUTH_PASS_RECOVERY_TRY_AGAIN'] = 'Try enter another email';
?>