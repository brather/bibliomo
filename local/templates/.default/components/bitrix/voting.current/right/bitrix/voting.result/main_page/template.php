<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

	if (empty($arResult["VOTE"]) || empty($arResult["QUESTIONS"]))
		return true;

?>
<div class="b-quiz">
	<form class="b-form b-form_common b-formquiz">
		<?
			foreach ($arResult["QUESTIONS"] as $arQuestion)
			{
			?>
			<div class="b-quizico"></div>
			<h2><?=$arQuestion["QUESTION"]?></h2>
			<?
				foreach ($arQuestion["ANSWERS"] as $arAnswer)
				{
				?>
				<div class="b-quizitem">
					<div class="b-quizresult"><div style="width:<?=(round($arAnswer["BAR_PERCENT"]))?>%;"></div></div>
					<div class="iblock b-quizpersnt"><?=$arAnswer["PERCENT"]?>%</div>
					<label class="black"><?=$arAnswer["MESSAGE"]?></label>
				</div>
				<?
				}
			}
		?>
	</form>
</div><!-- /.b-quiz -->