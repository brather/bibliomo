<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use \Bitrix\Main\Localization\Loc as Loc;
Loc::loadMessages(__FILE__);
?>

<?=ShowError($arResult["ERROR_MESSAGE"]);?>
<?=ShowNote($arResult["OK_MESSAGE"]);?>

<?
	if(!empty($arResult["QUESTIONS"])){
	?>

	<div class="b-quiz">
		<form action="<?=POST_FORM_ACTION_URI?>" method="post" class="b-form b-form_common b-formquiz">
			<input type="hidden" name="vote" value="Y">
			<input type="hidden" name="PUBLIC_VOTE_ID" value="<?=$arResult["VOTE"]["ID"]?>">
			<input type="hidden" name="VOTE_ID" value="<?=$arResult["VOTE"]["ID"]?>">
			<?=bitrix_sessid_post()?>
			<?
				foreach ($arResult["QUESTIONS"] as $arQuestion)
				{
				?>
				<div class="b-quizico"></div>
				<h2><?=$arQuestion["QUESTION"]?></h2>
				<?
					foreach ($arQuestion["ANSWERS"] as $arAnswer)
					{

						switch ($arAnswer["FIELD_TYPE"])
						{
							case 0://radio
								$value=(isset($_REQUEST['vote_radio_'.$arAnswer["QUESTION_ID"]]) && 
									$_REQUEST['vote_radio_'.$arAnswer["QUESTION_ID"]] == $arAnswer["ID"]) ? 'checked="checked"' : '';
								break;
							case 1://checkbox
								$value=(isset($_REQUEST['vote_checkbox_'.$arAnswer["QUESTION_ID"]]) && 
									array_search($arAnswer["ID"],$_REQUEST['vote_checkbox_'.$arAnswer["QUESTION_ID"]])!==false) ? 'checked="checked"' : '';
								break;
						}
						switch ($arAnswer["FIELD_TYPE"])
						{
							case 0://radio

							?>
							<div class="fieldrow nowrap">
								<div class="fieldcell iblock">
									<div class="field validate">
										<div class="checkwrapper ">
											<input class="radio" type="radio" name="vote_radio_<?=$arAnswer["QUESTION_ID"]?>" value="<?=$arAnswer["ID"]?>" <?=$arAnswer["~FIELD_PARAM"]?> id="cb<?=$arAnswer['ID']?>" <?=$value?>>
											<label for="cb<?=$arAnswer['ID']?>" class="black"><?=$arAnswer["MESSAGE"]?></label>
										</div>
									</div>
								</div>
							</div>
							<?
								break;
							case 1:
							?>
							<div class="fieldrow nowrap">
								<div class="fieldcell iblock">
									<div class="field validate">
										<div class="checkwrapper ">
											<input class="checkbox agreebox" type="checkbox" name="vote_checkbox_<?=$arAnswer["QUESTION_ID"]?>[]"  id="cb<?=$arAnswer['ID']?>" <?=$value?> value="<?=$arAnswer["ID"]?>" <?=$arAnswer["~FIELD_PARAM"]?>>
											<label for="cb<?=$arAnswer['ID']?>" class="black"><?=$arAnswer["MESSAGE"]?></label>
										</div>
									</div>
								</div>
							</div>
							<?
								break;
						}
					}				
				}
			?>
			<div class="fieldrow nowrap fieldrowaction">
				<div class="fieldcell w700  mt25">
					<div class="field clearfix">
						<button class="b-search_bth" value="1" type="submit" name="vote"><?=Loc::getMessage('VOTING_LIBRARY_VOTE_BUTTON');?></button>
					</div>
				</div>
			</div>
		</form>
	</div>	
	<?
	}
?>
