<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
	/** @var array $arParams */
	/** @var array $arResult */
	/** @global CMain $APPLICATION */
	/** @global CUser $USER */
	/** @global CDatabase $DB */
	/** @var CBitrixComponentTemplate $this */
	/** @var string $templateName */
	/** @var string $templateFile */
	/** @var string $templateFolder */
	/** @var string $componentPath */
	/** @var CBitrixComponent $component */
	$this->setFrameMode(true);

	if((int)$arResult['NavPageCount'] <= 1)
		return false;

	if(!$arResult["NavShowAlways"])
	{
		if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
			return;
	}

	$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
	$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");
	$strNavQueryString = str_replace('dop_filter=', 'df=', $strNavQueryString);

	# target="_parent"
	global $navParent;
	if(empty($navParent))
		$navParent = '_parent';
?>
<div class="b-paging">
	<div class="b-paging_cnt">
		<?if ($arResult["NavPageNomer"] > 1):?>

			<?if($arResult["bSavePage"]):?>
				<a target="<?=$navParent?>" class="b-paging_prev iblock" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>"></a>

				<?else:?>
				<?if ($arResult["NavPageNomer"] > 2):?>
					<a target="<?=$navParent?>" class="b-paging_prev iblock" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>"></a>
					<?else:?>
					<a target="<?=$navParent?>" class="b-paging_prev iblock" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"></a>
					<?endif?>

				<?endif?>

			<?else:?>
			<a href="" onclick="return false;" class="b-paging_prev iblock"></a>	
			<?endif?>

		<?while($arResult["nStartPage"] <= $arResult["nEndPage"]):?>

			<?if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):?>
				<a target="<?=$navParent?>" href="#" onclick="return false;" class="b-paging_num current iblock"><?=$arResult["nStartPage"]?></a>
				<?elseif($arResult["nStartPage"] == 1 && $arResult["bSavePage"] == false):?>
				<a target="<?=$navParent?>" class="b-paging_num iblock" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"><?=$arResult["nStartPage"]?></a>
				<?else:?>
				<a target="<?=$navParent?>" class="b-paging_num iblock" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["nStartPage"]?>"><?=$arResult["nStartPage"]?></a>
				<?endif?>
			<?$arResult["nStartPage"]++?>
			<?endwhile?>


		<?if($arResult["NavPageNomer"] < $arResult["NavPageCount"]):?>
			<a target="<?=$navParent?>" class="b-paging_next iblock" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>"></a>
			<?else:?>
			<a href="" onclick="return false;" class="b-paging_next iblock"></a>
			<?endif?>


	</div>
</div>
