<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
	use \Bitrix\Main\Localization\Loc as Loc;
	Loc::loadMessages(__FILE__);
?><div class="b-headernav_login right wrapper">
	<? if($arResult["FORM_TYPE"] == "login"){ ?>
		<div class="b_login_popup popup" >
		Необходимо авторизоваться 
			<div class="b_login_popup_in bbox">
				<div class="b-loginform iblock rel">
                 
					<form action="<?=$arResult["AUTH_URL"]?>" class="b-form b-formlogin" name="system_auth_form<?=$arResult["RND"]?>" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">

						<?if($arResult["BACKURL"] <> '') {?>
							<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
							<?}?>

						<?foreach ($arResult["POST"] as $key => $value) {?>
							<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
							<?}?>

						<input type="hidden" name="AUTH_FORM" value="Y" />
						<input type="hidden" name="TYPE" value="AUTH" />
						<input type="hidden" name="USER_REMEMBER" value="Y" />
						<input type=hidden name=REQUESTED_FROM value='/catalog2/'>
						<input type=hidden name=REQUESTED_BY value='GET'>

						<div class="fieldrow nowrap">
							<div class="fieldcell iblock">
								<label for="settings11"><?=Loc::getMessage('AUTH_MAIL')?></label>
								<div class="field validate">
									<input type="text" data-required="required" value="<?=$_REQUEST['USER_LOGIN']?>" id="settings11" data-maxlength="50" data-minlength="2" name="USER_LOGIN" class="input">
								</div>
							</div>

						</div>
						<div class="fieldrow nowrap">

							<div class="fieldcell iblock">
								<label for="settings10"><?=Loc::getMessage('AUTH_PASS')?></label>
								<div class="field validate">
									<input type="password" data-required="required" value="<?=$_REQUEST['USER_PASSWORD']?>" id="settings10" data-maxlength="30" data-minlength="2" name="USER_PASSWORD" class="input">										
								</div>
							</div>
						</div>
						<?
							if(!empty($arResult['ERROR_MESSAGE']['MESSAGE']))
							{
							?>
							<em class="error_message"><?=$arResult['ERROR_MESSAGE']['MESSAGE']?></em>
							<script type="text/javascript">
								$(function() {
									$('#login-link').click();
								});
							</script>
							<?
							}
						?>
						<div class="fieldrow nowrap fieldrowaction">
							<div class="fieldcell ">
								<div class="field clearfix">
									<button class="formbutton left" value="1" type="submit"><?=Loc::getMessage('AUTH_LOGIN_BUTTON')?></button>
									
								</div>
							</div>
						</div>
					</form>
					<div class="b-passrform">								
						<a href="#" class="closepopup"><?=Loc::getMessage('AUTH_CLOSE')?></a>
						<form action="/auth/?forgot_password=yes" method="post" class="b-passrecoveryform b-form">
							<input type="hidden" name="AUTH_FORM" value="Y">
							<input type="hidden" name="TYPE" value="SEND_PWD">
							<input type="hidden" name="backurl" value="/catalog2/">
							<input type="hidden" name="send" value="1">
							<input type="hidden" name="IS_AJAX" value="Y">


							
						</form>
					</div>
				</div><!-- /.b-loginform -->
				<div class="b-login_social iblock">
					<h4><?=Loc::getMessage('AUTH_SOCSERVICES');?></h4>
					<?foreach ($arResult['AUTH_SERVICES'] as $authService) {?>
						<a href="javascript:void(0)" onclick="<?=$authService['AUTH_LINK']?>" class="b-login_slnk <?=$authService['CSS_CLASS']?>"><?=$authService['NAME']?></a>
						<?}?>
				</div><!-- /.b-login_social -->

			</div>

		</div> 	<!-- /.b_login_popup -->
<div class="close-window"></div>
		<?
		} 
		else 
		{
		?>
	
		<?
		}
	?>
</div>