<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
/*
загружаем фотографию
*/
if(intval($arResult['VALUES']['USER_ID']) > 0 and !empty($_REQUEST['scan1']) and !empty($_REQUEST['scan2']))
{
	$user = new CUser;
	$arFile = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"].$_REQUEST['scan1']);
	$arFile["MODULE_ID"] = "main";

	$arFile2 = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"].$_REQUEST['scan2']);
	$arFile2["MODULE_ID"] = "main";

	$arFields = array(
		'UF_SCAN_PASSPORT1' => $arFile,
		'UF_SCAN_PASSPORT2' => $arFile2
	);

	$user->Update($arResult['VALUES']['USER_ID'], $arFields);
}

$bDesignMode = $APPLICATION->GetShowIncludeAreas() && is_object($USER) && $USER->IsAdmin();
if($USER->IsAuthorized() and !$bDesignMode){
	CUser::SendUserInfo($arResult['VALUES']['USER_ID'], 's1', "Приветствуем Вас как нового пользователя ЕИСУБ!");
	#	LocalRedirect("/");
	#	exit();
}
?>
