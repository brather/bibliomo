<?
	error_reporting(E_ALL ^ E_NOTICE);

	$password = $_POST['password'];
	$idType = $_POST['idType'];
	$value = $_POST['value'];

	$url = 'http://195.68.154.68:20280/GetPrivateDataWebPage/Default.aspx?login_type='.urlencode($idType).'&login='.urlencode($value).'&password='.urlencode($password);

	$error = true;
	$result = array();
	$arJson = array();

	if($_SERVER['REQUEST_METHOD'] == 'POST' and !empty($password) and !empty($idType) and !empty($value))
	{
		set_time_limit('60'); 

		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_FAILONERROR, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 60);
		$response = curl_exec($curl);
		#$info = curl_getinfo($curl);
		curl_close($curl);

		if(!empty($response) and strpos($response, 'error>') === false)
		{
			$response = str_replace("utf-16", "utf-8", $response);
			$xml = simplexml_load_string($response);

			if(!empty($xml))
			{
				$result = (array)$xml;

				# запишем логи, для анализа адресов
				/*
				$dirLog = $_SERVER['DOCUMENT_ROOT'].'/upload/ecia/';
				$fileLog = $dirLog.'log456.txt';
				$current = '';
				if(file_exists($dirLog))
					$current = file_get_contents($fileLog);
				else
					mkdir($dirLog, 0775);

				$current .= "\n";
				$current .= "\n";
				$current .= print_r($result,1);
				file_put_contents($fileLog, $current);
				*/

				if(!empty($result['MobilePhone7Digits']))
				{
					$result['MobilePhone7Digits'] = substr_replace($result['MobilePhone7Digits'], '-', 3, 0);
					$result['MobilePhone7Digits'] = substr_replace($result['MobilePhone7Digits'], '-', 6, 0);
				}

				if(!empty($result['RegistrationAddress']))
				{
					$arAdress = explode(',', $result['RegistrationAddress']);
					$result['Registration']['index'] = trim($arAdress[0]); 
					$result['Registration']['city'] = trim(str_replace(array('город'), '', $arAdress[1])); 
					$result['Registration']['street'] = trim(str_replace(array('улица'), '', $arAdress[2])); 
					$result['Registration']['house'] = trim(str_replace(array('д.'), '', $arAdress[3])); 
					$result['Registration']['korp'] = trim(str_replace(array('корп.'), '', $arAdress[4])); 
					$result['Registration']['kv'] = trim(str_replace(array('кв.'), '', $arAdress[5])); 
				}

				if(!empty($result['LivingAddress']))
				{
					$arAdress = explode(',', $result['LivingAddress']);
					$result['Living']['index'] = trim($arAdress[0]); 
					$result['Living']['city'] = trim(str_replace(array('город'), '', $arAdress[1])); 
					$result['Living']['street'] = trim(str_replace(array('улица'), '', $arAdress[2])); 
					$result['Living']['house'] = trim(str_replace(array('д.'), '', $arAdress[3])); 
					$result['Living']['korp'] = trim(str_replace(array('корп.'), '', $arAdress[4])); 
					$result['Living']['kv'] = trim(str_replace(array('кв.'), '', $arAdress[5])); 
				}

				if(!empty($result['BirthDate']))
				{
					$timestamp = strtotime($result['BirthDate']);
					$result['arBirthDate']['d'] = intval(date('d', $timestamp));
					$result['arBirthDate']['m'] = intval(date('m', $timestamp));
					$result['arBirthDate']['Y'] = intval(date('Y', $timestamp));
				}

				$error = false;
			}
			else
			{
				$error = true;
			}
		}
		else
		{
			$error = true;
		}

	}

	$arJson['error'] = $error; 
	$arJson['result'] = $result; 

	echo json_encode($arJson);
?>