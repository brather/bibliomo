<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
	/*
	загружаем фотографию
	*/
	if(intval($arResult['VALUES']['USER_ID']) > 0 and !empty($_REQUEST['PERSONAL_PHOTO']))
	{
		$user = new CUser;
		$arFile = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"].$_REQUEST['PERSONAL_PHOTO']);
		$arFile["MODULE_ID"] = "main";

		$arFields = array(
			'PERSONAL_PHOTO' => $arFile
		);

		$user->Update($arResult['VALUES']['USER_ID'], $arFields);

	}

	$bDesignMode = $APPLICATION->GetShowIncludeAreas() && is_object($USER) && $USER->IsAdmin();
	if($USER->IsAuthorized() and !$bDesignMode){
		CUser::SendUserInfo($arResult['VALUES']['USER_ID'], 's1', "Приветствуем Вас как нового пользователя!");
		LocalRedirect("/");
		exit();
	}
?>
