<?
$MESS["AUTH_REGISTER"] = "Регистрация";
$MESS["REGISTER_CAPTCHA_TITLE"] = "Защита от автоматической регистрации";
$MESS["REGISTER_CAPTCHA_PROMT"] = "Введите слово на картинке";
$MESS["AUTH_REQ"] = "Поля, обязательные для заполнения.";
$MESS["USER_DONT_KNOW"] = "(неизвестно)";
$MESS["USER_MALE"] = "Мужской";
$MESS["USER_FEMALE"] = "Женский";
$MESS["REGISTER_FIELD_LOGIN"] = "Логин (мин. 3 символа)";
$MESS["REGISTER_FIELD_EMAIL"] = "Адрес e-mail";
$MESS["REGISTER_FIELD_PASSWORD"] = "Пароль";
$MESS["REGISTER_FIELD_CONFIRM_PASSWORD"] = "Подтверждение пароля";
$MESS["REGISTER_FIELD_NAME"] = "Имя";
$MESS["REGISTER_FIELD_SECOND_NAME"] = "Отчество";
$MESS["REGISTER_FIELD_LAST_NAME"] = "Фамилия";
$MESS["REGISTER_FIELD_AUTO_TIME_ZONE"] = "Часовой пояс";
$MESS["REGISTER_FIELD_PERSONAL_PROFESSION"] = "Профессия";
$MESS["REGISTER_FIELD_PERSONAL_WWW"] = "WWW-страница";
$MESS["REGISTER_FIELD_PERSONAL_ICQ"] = "ICQ";
$MESS["REGISTER_FIELD_PERSONAL_GENDER"] = "Пол";
$MESS["REGISTER_FIELD_PERSONAL_BIRTHDAY"] = "Дата рождения";
$MESS["REGISTER_FIELD_PERSONAL_PHOTO"] = "Фотография";
$MESS["REGISTER_FIELD_PERSONAL_PHONE"] = "Телефон";
$MESS["REGISTER_FIELD_PERSONAL_FAX"] = "Факс";
$MESS["REGISTER_FIELD_PERSONAL_MOBILE"] = "Мобильный";
$MESS["REGISTER_FIELD_PERSONAL_PAGER"] = "Пейджер";
$MESS["REGISTER_FIELD_PERSONAL_STREET"] = "Улица, дом";
$MESS["REGISTER_FIELD_PERSONAL_MAILBOX"] = "Почтовый ящик";
$MESS["REGISTER_FIELD_PERSONAL_CITY"] = "Город";
$MESS["REGISTER_FIELD_PERSONAL_STATE"] = "Область / край";
$MESS["REGISTER_FIELD_PERSONAL_ZIP"] = "Почтовый индекс";
$MESS["REGISTER_FIELD_PERSONAL_COUNTRY"] = "Страна";
$MESS["REGISTER_FIELD_PERSONAL_NOTES"] = "Дополнительные заметки";
$MESS["REGISTER_FIELD_WORK_COMPANY"] = "Наименование компании";
$MESS["REGISTER_FIELD_WORK_DEPARTMENT"] = "Департамент / Отдел";
$MESS["REGISTER_FIELD_WORK_POSITION"] = "Должность";
$MESS["REGISTER_FIELD_WORK_WWW"] = "WWW-страница (работа)";
$MESS["REGISTER_FIELD_WORK_PHONE"] = "Телефон (работа)";
$MESS["REGISTER_FIELD_WORK_FAX"] = "Факс (работа)";
$MESS["REGISTER_FIELD_WORK_PAGER"] = "Пейджер (работа)";
$MESS["REGISTER_FIELD_WORK_STREET"] = "Улица, дом (работа)";
$MESS["REGISTER_FIELD_WORK_MAILBOX"] = "Почтовый ящик (работа)";
$MESS["REGISTER_FIELD_WORK_CITY"] = "Город (работа)";
$MESS["REGISTER_FIELD_WORK_STATE"] = "Область / край (работа)";
$MESS["REGISTER_FIELD_WORK_ZIP"] = "Почтовый индекс (работа)";
$MESS["REGISTER_FIELD_WORK_COUNTRY"] = "Страна (работа)";
$MESS["REGISTER_FIELD_WORK_PROFILE"] = "Направления деятельности";
$MESS["REGISTER_FIELD_WORK_LOGO"] = "Логотип компании";
$MESS["REGISTER_FIELD_WORK_NOTES"] = "Дополнительные заметки (работа)";
$MESS["REGISTER_SUCCESSFUL"] = "Вы были успешно зарегистрированы.";
$MESS["REGISTER_EMAIL_WILL_BE_SENT"] = "На указанный E-mail было выслано письмо с информацией о подтверждении регистрации.";
$MESS["MAIN_REGISTER_AUTH"] = "Вы зарегистрированы на сервере и успешно авторизованы.";
$MESS["main_profile_time_zones_auto"] = "Автоматически определять часовой пояс:";
$MESS["main_profile_time_zones_auto_def"] = "(по умолчанию)";
$MESS["main_profile_time_zones_auto_yes"] = "Да, определить по браузеру";
$MESS["main_profile_time_zones_auto_no"] = "Нет, выбрать из списка";
$MESS["main_profile_time_zones_zones"] = "Часовой пояс:";
$MESS["AUTH_SECURE_NOTE"] = "Перед отправкой формы пароль будет зашифрован в браузере. Это позволит избежать передачи пароля в открытом виде.";
$MESS["AUTH_NONSECURE_NOTE"] = "Пароль будет отправлен в открытом виде. Включите JavaScript в браузере, чтобы зашифровать пароль перед отправкой.";

$MESS["MAIN_REGISTER_REQ"] = 'Поле обязательно для заполнения';

$MESS["MAIN_REGISTER_PORTAL_REGISTER"] = 'регистрация на портале';
$MESS["MAIN_REGISTER_PRIVATE_INFO"] = 'Личная информация';
$MESS["MAIN_REGISTER_TICKET"] = 'Электронный читательский билет (ЕЭЧБ)';
$MESS["MAIN_REGISTER_LASTNAME"] = 'Фамилия';
$MESS["MAIN_REGISTER_HOW_TO_GET"] = 'Как получить?';
$MESS["MAIN_REGISTER_EXIST"] = 'У меня есть ЕЭЧБ';
$MESS["MAIN_REGISTER_TICKET_NUM"] = 'Номер электронного читательского билета';
$MESS["MAIN_REGISTER_FIRSTNAME"] = 'Имя';
$MESS["MAIN_REGISTER_MIDNAME"] = 'Отчество';
$MESS["MAIN_REGISTER_LOGIN"] = 'Вход на сайт';
$MESS["MAIN_REGISTER_EMAIL"] = 'Электронная почта <strong>(для входа на сайт)</strong>';
$MESS["MAIN_REGISTER_EMAIL_FORMAT"] = 'Неверный формат электронной почты';
$MESS["MAIN_REGISTER_PASSWORD_SECURE"] = 'Защищенность пароля';
$MESS["MAIN_REGISTER_PASSWORD"] = 'Пароль';
$MESS["MAIN_REGISTER_PASSWORD_LENGTH_MIN"] = 'Пароль менее 6 символов';
$MESS["MAIN_REGISTER_PASSWORD_LENGTH_MAX"] = 'Пароль более 30 символов';
$MESS["MAIN_REGISTER_PASSWORD_CONFIRM"] = 'Подтвердить пароль';
$MESS["MAIN_REGISTER_PASSWORD_MISMATCH"] = 'Пароль не совпадает с введенным ранее';
$MESS["MAIN_REGISTER_DATA"] = 'Анкетные данные';
$MESS["MAIN_REGISTER_BIRTHDAY"] = 'Дата рождения';
$MESS["MAIN_REGISTER_WRONG_FORMAT"] = 'Неверный формат даты';
$MESS["MAIN_REGISTER_FILL"] = 'Заполните';
$MESS["MAIN_REGISTER_AGE"] = 'Вам должно быть<br>не меньше 12 лет';
$MESS["MAIN_REGISTER_DAY"] = 'день';
$MESS["MAIN_REGISTER_MONTH"] = 'месяц';
$MESS["MAIN_REGISTER_YEAR"] = 'год';
$MESS["MAIN_REGISTER_SEX"] = 'Пол';
$MESS["MAIN_REGISTER_MALE"] = 'мужской';
$MESS["MAIN_REGISTER_FEMALE"] = 'женский';
$MESS["MAIN_REGISTER_AGREE"] = 'Согласен с <a href="/user-agreement/" target="_blank">условиями использования</a> портала ЕИСУБ';
$MESS["MAIN_REGISTER_REGISTER"] = 'Зарегистрироваться';

$MESS["MAIN_REGISTER_MORE_30_SYMBOLS"] = 'Более 30 символов';
$MESS["MAIN_REGISTER_WRONG_FORMAT_CLEAR"] = 'Неверный формат';
?>