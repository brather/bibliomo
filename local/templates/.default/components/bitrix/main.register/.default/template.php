<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
	use \Bitrix\Main\Localization\Loc as Loc;
	Loc::loadMessages(__FILE__);
?>
<?
/**
 * Нам не нужна куча блоков при успешной регистрации, а также
 * будет не очень хорошо, если мы по USE_EMAIL_CONFIRMATION будем писать сообщение и закрывать section
 * потому я вынес это сюда
 */
if ( (int)$arResult['VALUES']['USER_ID'] > 0 && $arResult["USE_EMAIL_CONFIRMATION"] === "Y" )
{ ?>
	<section class="innersection innerwrapper clearfix">
		<div class="b-formsubmit">
			<span class="b-successlink"><?=GetMessage("REGISTER_SUCCESSFUL")?></span>
			<p><?=GetMessage("REGISTER_EMAIL_WILL_BE_SENT")?></p>
		</div>
	</section>
<?
	return;
}?>

<section class="innersection innerwrapper clearfix">
	<div class="b-registration rel">
		<script type="text/javascript">
			function setlogin(){
				$('input#LOGIN').val($('input#settings04').val());
			}
		</script>
		<h2 class="mode"><?=Loc::getMessage('MAIN_REGISTER_PORTAL_REGISTER');?></h2>
		<?
			if (count($arResult["ERRORS"]) > 0){
				foreach ($arResult["ERRORS"] as $key => $error){
					if (intval($key) == 0 && $key !== 0) {
						$arResult["ERRORS"][$key] = str_replace("#FIELD_NAME#", "&quot;".GetMessage("REGISTER_FIELD_".$key)."&quot;", $error);
					}
				}

				ShowError(implode("<br />", $arResult["ERRORS"]).'<br /><br />');

			}elseif($arResult["USE_EMAIL_CONFIRMATION"] === "Y" && (int)$arResult['VALUES']['USER_ID'] > 0 ){
			?>
			<p><?echo GetMessage("REGISTER_EMAIL_WILL_BE_SENT")?></p>
			<?
			}
		?>

		<form method="post" action="<?=POST_FORM_ACTION_URI?>" name="regform" enctype="multipart/form-data" class="b-form b-form_common b-regform" onsubmit="setlogin()">
			<?
				if($arResult["BACKURL"] <> ''){
				?>
				<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
				<?
				}
			?>
			<input type="hidden" name="REGISTER[LOGIN]" id="LOGIN" value="" />

            <div class="wrapfield">

                <div class="fieldrow nowrap">
                    <div class="b-form_header"><span class="iblock"><?=Loc::getMessage('MAIN_REGISTER_PRIVATE_INFO');?></span></div>
                    <div class="fieldcell iblock">
                        <em class="hint">*</em>
                        <label for="settings01"><?=Loc::getMessage('MAIN_REGISTER_LASTNAME');?></label>
                        <div class="field validate">
                            <input type="text" data-validate="fio" data-required="required" value="<?=$arResult["VALUES"]['LAST_NAME']?>" id="settings01" data-maxlength="30" data-minlength="2" name="REGISTER[LAST_NAME]" class="input">
                            <em class="error required"><?=Loc::getMessage('MAIN_REGISTER_REQ');?></em>
                            <em class="error validate"><?=Loc::getMessage('MAIN_REGISTER_WRONG_FORMAT_CLEAR');?></em>
                            <em class="error maxlength"><?=Loc::getMessage('MAIN_REGISTER_MORE_30_SYMBOLS');?></em>
                        </div>
                    </div>
                </div>
                <div class="fieldrow nowrap">
                    <div class="fieldcell iblock">
                        <em class="hint">*</em>
                        <label for="settings02"><?=Loc::getMessage('MAIN_REGISTER_FIRSTNAME');?></label>
                        <div class="field validate">
                            <input type="text" data-validate="fio" data-required="required" value="<?=$arResult["VALUES"]['NAME']?>" id="settings02" data-maxlength="30" data-minlength="2" name="REGISTER[NAME]" class="input" >
                            <em class="error required"><?=Loc::getMessage('MAIN_REGISTER_REQ');?></em>
                            <em class="error validate"><?=Loc::getMessage('MAIN_REGISTER_WRONG_FORMAT_CLEAR');?></em>
                            <em class="error maxlength"><?=Loc::getMessage('MAIN_REGISTER_MORE_30_SYMBOLS');?></em>

                        </div>
                    </div>
                </div>
                <div class="fieldrow nowrap">
                    <div class="fieldcell iblock">
                        <label for="settings22"><?=Loc::getMessage('MAIN_REGISTER_MIDNAME');?></label>
                        <div class="field validate">
                            <input type="text" data-validate="fio" value="<?=$arResult["VALUES"]['SECOND_NAME']?>" id="settings22" data-maxlength="30" data-minlength="2" name="REGISTER[SECOND_NAME]" class="input" >
                            <em class="error required"><?=Loc::getMessage('MAIN_REGISTER_REQ');?></em>
                            <em class="error validate"><?=Loc::getMessage('MAIN_REGISTER_WRONG_FORMAT_CLEAR');?></em>
                            <em class="error maxlength"><?=Loc::getMessage('MAIN_REGISTER_MORE_30_SYMBOLS');?></em>
                        </div>
                    </div>
                </div>
                <div class="b-form_header"><?=Loc::getMessage('MAIN_REGISTER_LOGIN');?></div>

                <div class="fieldrow nowrap">
                    <div class="fieldcell iblock">
                        <em class="hint">*</em>
                        <label for="settings04"><?=Loc::getMessage('MAIN_REGISTER_EMAIL');?></label>
                        <div class="field validate">
                            <input type="text" class="input" name="REGISTER[EMAIL]" data-minlength="5" data-maxlength="50" id="settings04" data-validate="email" value="<?=$arResult["VALUES"]['EMAIL']?>" data-required="required">
                            <em class="error validate"><?=Loc::getMessage('MAIN_REGISTER_EMAIL_FORMAT');?></em>
                            <em class="error required"><?=Loc::getMessage('MAIN_REGISTER_REQ');?></em>
                        </div>
                    </div>
                </div>
                <div class="fieldrow nowrap">
                    <div class="fieldcell iblock">
                        <em class="hint">*</em>
                        <div class="passecurity">
                            <div class="passecurity_lb"><?=Loc::getMessage('MAIN_REGISTER_PASSWORD_SECURE');?></div>

                        </div>
                        <label for="settings05"><?=Loc::getMessage('MAIN_REGISTER_PASSWORD');?></label>
                        <div class="field validate">
                            <input type="password" data-validate="password" data-required="required" value="<?=$arResult["VALUES"]['PASSWORD']?>" id="settings05" data-maxlength="30" data-minlength="6" name="REGISTER[PASSWORD]" class="pass_status input">
                            <em class="error required"><?=Loc::getMessage('MAIN_REGISTER_REQ');?></em>
                            <em class="error validate"><?=Loc::getMessage('MAIN_REGISTER_PASSWORD_LENGTH_MIN');?></em>
                            <em class="error maxlength"><?=Loc::getMessage('MAIN_REGISTER_PASSWORD_LENGTH_MAX');?></em>
                        </div>
                    </div>
                </div>
                <div class="fieldrow nowrap">
                    <div class="fieldcell iblock">
                        <em class="hint">*</em>
                        <label for="settings55"><?=Loc::getMessage('MAIN_REGISTER_PASSWORD_CONFIRM');?></label>
                        <div class="field validate">
                            <input data-identity="#settings05" type="password" data-required="required" value="<?=$arResult["VALUES"]['CONFIRM_PASSWORD']?>" id="settings55" data-maxlength="30" data-minlength="6" name="REGISTER[CONFIRM_PASSWORD]" class="input" data-validate="password">
                            <em class="error identity "><?=Loc::getMessage('MAIN_REGISTER_PASSWORD_MISMATCH');?></em>
                            <em class="error required"><?=Loc::getMessage('MAIN_REGISTER_REQ');?></em>
                            <em class="error validate"><?=Loc::getMessage('MAIN_REGISTER_PASSWORD_LENGTH_MIN');?></em>
                            <em class="error maxlength"><?=Loc::getMessage('MAIN_REGISTER_PASSWORD_LENGTH_MAX');?></em>
                        </div>
                    </div>
                </div>
                <div class="b-form_header"><?=Loc::getMessage('MAIN_REGISTER_DATA');?></div>

                <?$APPLICATION->IncludeComponent(
                        "notaext:plupload",
                        "register",
                        array(
                            "MAX_FILE_SIZE" => "24",
                            "FILE_TYPES" => "jpg,jpeg,png",
                            "DIR" => "tmp_register",
                            "FILES_FIELD_NAME" => "register_file",
                            "MULTI_SELECTION" => "N",
                            "CLEANUP_DIR" => "Y",
                            "UPLOAD_AUTO_START" => "Y",
                            "RESIZE_IMAGES" => "Y",
                            "RESIZE_WIDTH" => "110",
                            "RESIZE_HEIGHT" => "110",
                            "RESIZE_CROP" => "Y",
                            "RESIZE_QUALITY" => "98",
                            "UNIQUE_NAMES" => "Y"
                        ),
                        false
                    );?>
                <div class="fieldrow nowrap">
                    <div class="fieldcell iblock">
                        <em class="hint">*</em>
                        <label for="settings06"><?=Loc::getMessage('MAIN_REGISTER_BIRTHDAY');?></label>
                        <div class="field validate iblock seldate" data-yearsrestrict="12">
                            <em class="error dateformat"><?=Loc::getMessage('MAIN_REGISTER_WRONG_FORMAT');?></em>
                            <em class="error required"><?=Loc::getMessage('MAIN_REGISTER_FILL');?></em>
                            <em class="error yearsrestrict"><?=Loc::getMessage('MAIN_REGISTER_AGE');?></em>
                            <input type="hidden" class="realseldate" data-required="true" value="<?=$arResult["VALUES"]['PERSONAL_BIRTHDAY']?>" id="settings06" name="REGISTER[PERSONAL_BIRTHDAY]">

                            <select name="birthday" id="PERSONAL_BIRTHDAY_D"  class="js_select w102 sel_day"  data-required="true">
                                <option value="-1"><?=Loc::getMessage('MAIN_REGISTER_DAY');?></option>
                                <?
                                    for($i=1;$i<=31;$i++)
                                    {
                                    ?>
                                    <option value="<?=$i?>"><?=$i?></option>
                                    <?
                                    }
                                ?>
                            </select>

                            <select name="birthmonth" id="PERSONAL_BIRTHDAY_M" class="js_select w165 sel_month"  data-required="true" >
                                <option value="-1"><?=Loc::getMessage('MAIN_REGISTER_MONTH');?></option>
                                <?
                                    for($i=1;$i<=12;$i++)
                                    {
                                    ?>
                                    <option value="<?=$i?>"><?=FormatDate('f', MakeTimeStamp('01.'.$i.'.'.date('Y')))?></option>
                                    <?
                                    }
                                ?>
                            </select>

                            <select name="birthyear" id="PERSONAL_BIRTHDAY_Y"  class="js_select w102 sel_year"  data-required="true">
                                <option value="-1"><?=Loc::getMessage('MAIN_REGISTER_YEAR');?></option>
                                <?
                                    for($i=(date('Y')-12);$i>=(date('Y')-95);$i--)
                                    {
                                    ?>
                                    <option value="<?=$i?>"><?=$i?></option>
                                    <?
                                    }
                                ?>
                            </select>
                        </div>

                    </div>
                </div>
                <div class="fieldrow nowrap ptop">
                    <div class="fieldcell iblock">
                        <em class="hint">*</em>
                        <label for="settings07"><?=Loc::getMessage('MAIN_REGISTER_SEX');?></label>
                        <div class="field validate">
                            <select name="REGISTER[PERSONAL_GENDER]" id="settings07" data-required="required" class="js_select w270">
                                <option value="M"<?=$arResult["VALUES"]['PERSONAL_GENDER'] == "M" ? " selected=\"selected\"" : ""?>><?=Loc::getMessage('MAIN_REGISTER_MALE');?></option>
                                <option value="F"<?=$arResult["VALUES"]['PERSONAL_GENDER'] == "F" ? " selected=\"selected\"" : ""?>><?=Loc::getMessage('MAIN_REGISTER_FEMALE');?></option>
                            </select>
                        </div>
                    </div>
                </div>

            </div>

            <div class="wrapfield">
                <div class="b-form_header"><span class="iblock"><?=Loc::getMessage('MAIN_REGISTER_TICKET');?></span></div>
                <div class="fieldrow nowrap">
                    <div class="fieldcell iblock">
                        <div class="checkwrapper">
                            <a href="/how-to-get-a-library-card/" target="_blank" class="right ecblink"><?=Loc::getMessage('MAIN_REGISTER_HOW_TO_GET');?></a>
                            <input class="checkbox" <?=!empty($_REQUEST['UF_NUM_ECHB'])? 'checked="checked"':''?> type="checkbox" name="check_echb" id="cb22"><label for="cb22" class="black"><?=Loc::getMessage('MAIN_REGISTER_EXIST');?></label>
                        </div>
                        <label for="settings21" <?=empty($_REQUEST['UF_NUM_ECHB'])? 'class="disable"':''?>><?=Loc::getMessage('MAIN_REGISTER_TICKET_NUM');?></label>
                        <div class="field validate">
                            <input type="text" <?=empty($_REQUEST['UF_NUM_ECHB'])? 'disabled="disabled"':''?> data-validate="number" value="<?=htmlspecialcharsEx($_REQUEST['UF_NUM_ECHB'])?>" id="settings21" data-maxlength="30" data-minlength="2" name="UF_NUM_ECHB" class="input">
                        </div>
                    </div>
                </div>
            </div>

			<hr>
			<div class="checkwrapper ">
				<input class="checkbox agreebox" type="checkbox" name="agreebox" id="cb3">
				<label for="cb3" class="black"><?=Loc::getMessage('MAIN_REGISTER_AGREE');?></label>
			</div>
			<div class="fieldrow nowrap fieldrowaction">
				<div class="fieldcell ">
					<div class="field clearfix">
						<button class="formbutton btdisable" value="1" type="submit" disabled="disabled"><?=Loc::getMessage('MAIN_REGISTER_REGISTER');?></button>
						<input type="hidden" name="register_submit_button" value="<?=GetMessage("AUTH_REGISTER")?>" />
					</div>
				</div>
			</div>
		</form>
	</div><!-- /.b-registration-->
</section>