<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
	/**
	 * Если кейс: "SUCCESSFUL_CONFIRMATION" - E06 - отправляем пользователю приветственное письмо
	 * Повторный переход по этой ссылке приведёт к "READY_CONFIRM" - E03, так что повторной отправки опасаться не стоит
	 */
	CModule::IncludeModule('nota.userdata');
	use Nota\UserData\Rgb; 

	if( $arResult['MESSAGE_CODE'] == 'E06' && (int)$arResult['USER']['ID'] > 0 )
	{
		/*	 Добавляем пользователя в РГБ 	*/
		CUser::SendUserInfo($arResult['USER']['ID'], 's1', "Приветствуем Вас как нового пользователя ЕИСУБ!");
		if(!empty($arResult['USER']['UF_PASSPORT_NUMBER']) or !empty($arResult['USER']['UF_RGB_USER_ID']))
		{
			$res = Rgb::addUserBx($arResult['USER']['ID']);
			LocalRedirect("/auth/ok.php");
		}

	}
?>
