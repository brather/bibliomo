<?
$MESS ['CT_BSAC_CONFIRM'] = "Confirm";
$MESS ['CT_BSAC_LOGIN'] = "Login";
$MESS ['CT_BSAC_CONFIRM_CODE'] = "Confirmation Code";

$MESS['ACCOUNT_ACTIVATION'] = 'Account activation.';
$MESS['USER_NOT_FOUND'] = 'User not found.';
$MESS['SUCCESSFULLY_AUTHORIZED'] = 'Successfully authorized.';
$MESS['READY_CONFIRM'] = 'Account activated.';
$MESS['MISSED_CODE'] = 'Missed confirmation code.';
$MESS['WRONG_CODE'] = 'Wrong confirmation code.';
$MESS['SUCCESSFUL_CONFIRMATION'] = 'Account activated successfully.';
$MESS['SOME_ERROR'] = 'Some error occurred during confirmation.';
?>