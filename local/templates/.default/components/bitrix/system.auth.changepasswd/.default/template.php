<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="wrapper">
	<section class="innersection innerwrapper clearfix">
		<?if($_REQUEST['change_pwd']){?>
			<div class="b-formsubmit" style='display: block!important'>
				<p><? ShowMessage($arParams["~AUTH_RESULT"]); ?></p>
			</div>
		<?}?>
		<div class="b-passform rel">
			<h2 class="mode"><?=GetMessage('RECOVERY'); ?></h2>
			<form name="bform" class="b-form b-form_common b-passchge" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">
				<?if (strlen($arResult["BACKURL"]) > 0): ?>
				<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
				<? endif ?>
				<input type="hidden" name="AUTH_FORM" value="Y">
				<input type="hidden" name="TYPE" value="CHANGE_PWD">
				<input type="hidden" name="USER_LOGIN" maxlength="50" value="<?=$arResult["LAST_LOGIN"]?>" class="bx-auth-input" />
				<input type="hidden" name="USER_CHECKWORD" maxlength="50" value="<?=$arResult["USER_CHECKWORD"]?>" class="bx-auth-input" />
				<hr>
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock">
						<em class="hint">*</em>
						<label for="settings05"><span><?=GetMessage("SIX")?></span><?=GetMessage("AUTH_NEW_PASSWORD")?></label>
						<div class="field validate">
							<input type="password" data-validate="password" data-required="required" value="" id="settings05" data-maxlength="30" data-minlength="2" name="USER_PASSWORD" class="pass_status input">										
							<em class="error required"><?=GetMessage("REQUIRED")?></em>
							<em class="error validate">Пароль от 6 до 30 символов</em>
						</div>
                        <div class="passecurity">
                            <div class="passecurity_lb"><?=GetMessage('DEFENSE'); ?></div>
                            <div class="passecurity_view">
                                <span class="iblock"></span>
                                <span class="iblock"></span>
                                <span class="iblock"></span>
                                <span class="iblock"></span>
                                <span class="iblock"></span>
                            </div>
                        </div>
					</div>
				</div>
				<div class="fieldrow nowrap">
					<div class="fieldcell iblock">
						<em class="hint">*</em>
						<label for="settings55"><?=GetMessage("AUTH_NEW_PASSWORD_CONFIRM")?></label>
						<div class="field validate">
							<input type="password" data-required="required" value="" id="settings55" data-maxlength="30" data-minlength="2" name="USER_CONFIRM_PASSWORD" class="pass_status input" data-validate="password">										
							<em class="error validate">Пароль не совпадает с введенным ранее</em>
							<em class="error required"><?=GetMessage("REQUIRED")?></em>
						</div>
					</div>
				</div>
				<div class="fieldrow nowrap fieldrowaction">
					<div class="fieldcell ">
						<div class="field clearfix">
							<button name="change_pwd" class="formbutton bt_typelt" value="1" type="submit"><?=GetMessage("SEND")?></button>
						</div>
					</div>
				</div>
			</form>
		</div><!-- /.b-registration-->
	</section>
</div>