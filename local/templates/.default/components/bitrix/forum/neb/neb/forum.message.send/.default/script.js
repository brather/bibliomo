function ValidateForm(form)
{
	if (typeof form != "object")
		return false;
	//window.oLHE.SaveContent();

	var
		errors = "",
		//Message = window.oLHE.GetContent(),
		MessageMax = 64000,
		MessageLength = form.MESSAGE.value.length;

	if (form.SUBJECT && (form.SUBJECT.value.length < 2))
		errors += window["oErrors"]['no_topic_name'];

	if (MessageLength < 2)
		errors += window["oErrors"]['no_message'];
	else if ((MessageMax !== 0) && (MessageLength > MessageMax))
		errors += window["oErrors"]['max_len'].replace("#MAX_LENGTH#", MessageMax).replace("#LENGTH#", MessageLength);

	if (errors !== "")
	{
		alert(errors);
		return false;
	}
	
	var arr = form.getElementsByTagName("input");
	for (var i=0; i < arr.length; i++)
	{
		var butt = arr[i];
		if (butt.getAttribute("type") == "submit")
			butt.disabled = true;
	}
	return true;
}

var suggest_count = 0;
var input_initial_value = '';
var suggest_selected = 0;

$(window).load(function(){
	$('body').on('click', '#send_test', function () {
		fSearch();
		if($('#UID_U').val() == '' && $('#USER_ID').val() == ''){
			alert('Не верно выбран адресат');
			return false;
		}
	});
	// читаем ввод с клавиатуры
	$("#search_box").keyup(function(I){
		// определяем какие действия нужно делать при нажатии на клавиатуру
		switch(I.keyCode) {
			// игнорируем нажатия на эти клавишы
			case 13:  // enter
			case 27:  // escape
			case 38:  // стрелка вверх
			case 40:  // стрелка вниз
				break;

			default:
				// производим поиск только при вводе более 2х символов
				if($(this).val().length>2){

					input_initial_value = $(this).val();
					// производим AJAX запрос к /ajax/ajax.php, передаем ему GET query, в который мы помещаем наш запрос
					$.get("/ajax/SearchAdress.php", { "query":$(this).val() },function(data){
						var list;
						$("#search_advice_wrapper").html("").show();
						list = JSON.parse(data);
						suggest_count = data.length;
						if(suggest_count > 0) {
							for (var i in list) {
								$('#search_advice_wrapper').append('<div class="advice_variant" data="' + list[i]["NAME"] + '"rel="' + list[i]["ID"] + '">' + list[i]["EMAIL"] + '</div>');
							}
						}
						/*var list = eval("("+data+")");
						suggest_count = list.length;
						if(suggest_count > 0){
							// перед показом слоя подсказки, его обнуляем
							$("#search_advice_wrapper").html("").show();
							for(var i in list){
								if(list[i] != ''){
									// добавляем слою позиции
									//["email": "prombez57@mail.ru","name": "Лилия ","id": "6036"]
									$('#search_advice_wrapper').append('<div class="advice_variant">'+list[i]["email"]+'</div>');
								}
							}
						}*/
					}, 'html');
				}
				break;
		}
	});

	//считываем нажатие клавишь, уже после вывода подсказки
	$("#search_box").keydown(function(I){
		switch(I.keyCode) {
			// по нажатию клавишь прячем подсказку
			case 13: // enter
			case 27: // escape
				$('#search_advice_wrapper').hide();
				return false;
				break;
			// делаем переход по подсказке стрелочками клавиатуры
			case 38: // стрелка вверх
			case 40: // стрелка вниз
				I.preventDefault();
				if(suggest_count){
					//делаем выделение пунктов в слое, переход по стрелочкам
					key_activate( I.keyCode-39 );
				}
				break;
		}
	});

	// делаем обработку клика по подсказке
	$('.advice_variant').live('click',function(){
		// ставим текст в input поиска
		$('#search_box').val($(this).text());
		fSearch();
		// прячем слой подсказки
		$('#search_advice_wrapper').fadeOut(350).html('');
        $('#div_USER_ID').html('[<noindex><a title="" href="/forum/user/' + $(this).attr('rel') + '" rel="nofollow">' + $(this).attr('data') + '</a></noindex>]');
	    $('#USER_ID').val($(this).attr('rel'));
	    $('#UID_U').val($(this).attr('rel'));
	});

	// если кликаем в любом месте сайта, нужно спрятать подсказку
	$('html').click(function(){
		$('#search_advice_wrapper').hide();
	});
	// если кликаем на поле input и есть пункты подсказки, то показываем скрытый слой
	$('#search_box').click(function(event){
		//alert(suggest_count);
		if(suggest_count)
			$('#search_advice_wrapper').show();
		event.stopPropagation();
	});
});

function key_activate(n){
	$('#search_advice_wrapper div').eq(suggest_selected-1).removeClass('active');

	if(n == 1 && suggest_selected < suggest_count){
		suggest_selected++;
	}else if(n == -1 && suggest_selected > 0){
		suggest_selected--;
	}

	if( suggest_selected > 0){
		$('#search_advice_wrapper div').eq(suggest_selected-1).addClass('active');
		$("#search_box").val( $('#search_advice_wrapper div').eq(suggest_selected-1).text() );
	} else {
		$("#search_box").val( input_initial_value );
	}
}