<?
$MESS ['F_TITLE_ICQ'] = "Сообщение icq для пользователя ";
$MESS ['F_TITLE_MAIL'] = "Письмо e-mail для пользователя ";
$MESS ['F_NAME'] = "Ваше имя";
$MESS ['F_ICQ'] = "Ваш ICQ";
$MESS ['F_EMAIL'] = "Ваш E-Mail";
$MESS ['F_TOPIC'] = "Тема сообщения";
$MESS ['F_TEXT'] = "Текст собщения";
$MESS ['F_SEND'] = "Отправить";
$MESS ['F_CAPTCHA_TITLE'] = "Защита от автоматических сообщений";
$MESS ['F_CAPTCHA_PROMT'] = "Код подтверждения";
$MESS ['F_HEAD_TO'] = "Адресат (поиск пользователя по e-mail)";
$MESS ['F_FIND_USER'] = "найти адресата";
$MESS ['PM_NOT_FINED'] = "не  найден";
$MESS ['JERROR_NO_TOPIC_NAME'] = "Вы должны ввести название темы. ";
$MESS ['JERROR_NO_MESSAGE'] = "Вы должны ввести сообщение. ";
?>