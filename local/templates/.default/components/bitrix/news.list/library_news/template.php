<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use \Bitrix\Main\Localization\Loc as Loc;
Loc::loadMessages(__FILE__);
?>
<?
	if(!empty($arResult["ITEMS"]))
	{
	?>
	<div class="b-viewhnews">
		<h2><?=Loc::getMessage('NEWS_LIST_LIBRARY_TITLE')?></h2>
		<div class="b-newslider js_fourslider slidetype">
			<?
				foreach($arResult["ITEMS"] as $arItem)
				{
				?>
				<div>
					<div class="b-newsslider_item">
						<div class="date"><?=$arItem["DISPLAY_ACTIVE_FROM"]?></div>	
						<a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>	
						<p><?=$arItem["~PREVIEW_TEXT"]?></p>		
					</div>
				</div>
				<?
				}
			?>
		</div>
	</div>


	<?
	}
?>