<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if(count($arResult["ITEMS"])){?>
	<div class="b-mainslider js_slider_single">
		<?foreach($arResult["ITEMS"] as $arItem){?>

				<div>
					<div class="sliderinner">
						<div class="b-mainslider_photo iblock">

							<?if (trim($arItem['PROPERTIES']['LINK_HEAD']['VALUE']) != ''):?>
								<a href="<?=$arItem['PROPERTIES']['LINK_HEAD']['VALUE']?>">
							<?else:?>
							<?if($arItem['DETAIL_TEXT']){?>
								<a href="<?=$arItem['DETAIL_PAGE_URL']?>">
							<?}?>
							<?endif;?>
							<?if($arItem['PREVIEW_PICTURE_RESIZE']){?>
								<img src="<?=$arItem['PREVIEW_PICTURE_RESIZE']?>" alt="<?=$arItem['NAME']?>">
							<?}?>	
							<?if($arItem['DETAIL_TEXT'] || $arItem['PROPERTIES']['LINK_HEAD']['VALUE']){?>
								</a>
							<?}?>	
						</div>
						<div class="b-mainslider_descr iblock">
							<h3>
								<?if (trim($arItem['PROPERTIES']['LINK_HEAD']['VALUE']) != ''):?>
									<a href="<?=$arItem['PROPERTIES']['LINK_HEAD']['VALUE']?>">
								<?else:?>
								<?if($arItem['DETAIL_TEXT']){?>
									<a href="<?=$arItem['DETAIL_PAGE_URL']?>">
								<?}?>
								<?endif;?>
								<?=$arItem['NAME']?>
								<?if($arItem['DETAIL_TEXT'] || $arItem['PROPERTIES']['LINK_HEAD']['VALUE']){?>
									</a>
								<?}?>
							</h3>
							<p><?=$arItem['~PREVIEW_TEXT']?></p>
						</div>
					</div>
				</div>
			
		<?}?>
	</div> <!-- /.b-mainslider -->
<?}?>