<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use \Bitrix\Main\Localization\Loc as Loc;
Loc::loadMessages(__FILE__);
?>

<div class="b-createcoll">
	<a href="add/" class="b-btcreatenews b-btcreate">Создать новость</a>
</div>

<? if ( !empty($arResult["ITEMS"]) ) { ?>
	<ul class="b-newslist">
		<? foreach($arResult["ITEMS"] as $arItem) { ?>
			<li>
				<div class="b-newsdate iblock"><?=$arItem["DISPLAY_ACTIVE_FROM"]?></div>
				<div class="b-newsinner iblock">
					<h2><?=$arItem["NAME"]?></h2>
					<p><?=$arItem["~PREVIEW_TEXT"]?></p>
					<div class="b-newsaction">
					<span class="b-newspublic public_checkbox">
						<span class="field checkbox <?=($arItem['ACTIVE'] == 'Y') ? 'checked' : ''?>">
							<span>
								<input class="checkbox news-active custom" data-news_id="<?=$arItem['ID'];?>" id="news-id<?=$arItem['ID'];?>"  <?=($arItem['ACTIVE'] == 'Y') ? 'checked="checked"' : ''?> type="checkbox" name="active[<?=$arItem['ID'];?>]">
							</span>
						</span>
						<label onclick="return false;" class="public-label" for="news-id<?=$arItem['ID'];?>">Опубликована</label>
					</span>
						<a data-width="970" class="b-editcollection" href="edit/<?=$arItem['ID']?>/">Редактировать новость</a>
						<a class="right button_red news_delete" href="/local/tools/library_news_lk/actions.php?action=delete&id=<?=$arItem['ID'];?>&back_url=<?=$APPLICATION->GetCurPageParam()?>">Удалить новость</a>
					</div>
				</div>
			</li>
		<? } ?>
	</ul>

	<div class="b-removepopup removenews hidden">
		<p><?=Loc::getMessage('REMOVE_MESSAGE')?></p>
		<a href="#" class="formbutton btremove"><?=Loc::getMessage('REMOVE_BUTTON')?></a>
		<a href="#" class="formbutton gray"><?=Loc::getMessage('CANCEL_BUTTON')?></a>
	</div>

	<div class="b-removepopup public_new hidden">
		<p><?=Loc::getMessage('APPROVE_ACTION')?></p>
		<a href="#" class="formbutton public"><?=Loc::getMessage('APPROVE_BUTTON')?></a>
		<a href="#" class="formbutton gray"><?=Loc::getMessage('CANCEL_BUTTON')?></a>
	</div>
<? } ?>

<?=$arResult['NAV_STRING']?>