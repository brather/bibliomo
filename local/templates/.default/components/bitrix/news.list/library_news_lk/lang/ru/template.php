<?php
$MESS['NEWS_LIST_LIBRARY_TITLE'] = 'новости библиотеки';
$MESS['REMOVE_MESSAGE'] = 'Действительно удалить новость из библиотеки?';
$MESS['REMOVE_BUTTON'] = 'Удалить';
$MESS['CANCEL_BUTTON'] = 'Оставить';
$MESS['APPROVE_ACTION'] = 'Для выполнения этого действия требуется подтверждение';
$MESS['APPROVE_BUTTON'] = 'Подтверждаю';
$MESS['DISAPPROVE_BUTTON'] = 'Отмена';