$(document).on('change', '.news-active', function(){
	var newsID = $(this).data('news_id');
	var active = $(this).is(':checked') ? 'Y' : 'N';

	$.ajax({
		url: '/local/tools/library_news_lk/actions.php',
		data: {'id':newsID, 'action':'active', 'active':active}
	});
});

$(document).on('click', '.public_checkbox', function(){
	var checkbox 		= $('input', $(this));
	var active 			= checkbox.is(':checked') ? true : false;
	var topPos 			= $(this).offset();
	var fakeCheckBox 	= $('.checkbox', $(this));

	// Сперва спрашиваем
	// клонируем блок с утверждением
	var publickpopup = $('.public_new').clone(true);
	$('body').append(publickpopup);

	publickpopup.css('top', topPos.top).show().removeClass('hidden');


	var bt_remove = $('.public', publickpopup),
		bt_cancel = $('.gray', publickpopup);

	bt_remove.click(function(e){
		e.preventDefault();

		// Если таки согласился - нужно изменить вид и отправить ajax
		if ( active )
		{
			fakeCheckBox.removeClass('checked');
			checkbox.removeAttr('checked').change();
		}
		else
		{
			fakeCheckBox.addClass('checked');
			checkbox.attr('checked', true).change();
		}

		publickpopup.remove();
	});
	bt_cancel.click(function(e){
		e.preventDefault();
		publickpopup.remove();
	});
});

$(document).on('click', '.news_delete', function(e){
	e.preventDefault();

	// Показываем блок с утверждением.
	var link 		= $(this);
	var topPos 		= link.offset();
	var newsBlock 	= link.closest('li');

	// клонируем блок с утверждением
	var removepopup = $('.removenews').clone(true);
	$('body').append(removepopup);

	removepopup.css('top', topPos.top).show().removeClass('hidden');


	var bt_remove = $('.btremove', removepopup),
		bt_cancel = $('.gray', removepopup);

	bt_remove.click(function(e){
		e.preventDefault();

		newsBlock.fadeTo('slow', 0.5, function() { // удаляем книжку
			$.ajax({
				url: link.attr('href'),
				complete: function(){
				}
			});

			$(this).remove();
		});

		removepopup.remove();
	});
	bt_cancel.click(function(e){
		e.preventDefault();
		removepopup.remove();
	});


	return false;
});

// Что б найти этот кусок
function findNewsListLibrary()
{

}