<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<?
	if(!empty($arResult["ITEMS"]))
	{
	?>
	<div class="b-viewhnews">
		<h2>другие новости</h2>
		<div class="b-newslider slidetype">
			<?
				foreach($arResult["ITEMS"] as $arItem)
				{
				?>
				<div>
					<div class="b-newsslider_item">
						<div class="date"><?=$arItem["DISPLAY_ACTIVE_FROM"]?></div>	
						<a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>	
						<p><?=$arItem["~PREVIEW_TEXT"]?></p>		
					</div>
				</div>
				<?
				}
			?>
		</div>
	</div>


	<?
	}
?>