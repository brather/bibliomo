<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(SITE_TEMPLATE_ID == "special" && isset($arResult["ITEMS"]) && !empty($arResult["ITEMS"]))
{
    foreach($arResult["ITEMS"] as $key=>$arItem)
    {
        $arResult["ITEMS"][$key]["DETAIL_PAGE_URL"] = "/special".$arItem["DETAIL_PAGE_URL"];
    }
}