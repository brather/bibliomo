<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if(count($arResult["ITEMS"])){?>
	<h2 class="b-mainslider_tit"><?=GetMEssage('TITLE');?></h2>
	<div class="b-mainslider js_slider_single">
		<?foreach($arResult["ITEMS"] as $arItem){?>
			<?if($arItem['PREVIEW_PICTURE_RESIZE']){?>
				<div>
					<div class="sliderinner">
						<div class="b-mainslider_photo iblock">
							<?if($arItem['PROPERTIES']['LINK']['VALUE']){?>
								<a href="<?=$arItem['PROPERTIES']['LINK']['VALUE']?>">
							<?}?>
							<?if($arItem['PREVIEW_PICTURE_RESIZE']){?>
								<img src="<?=$arItem['PREVIEW_PICTURE_RESIZE']?>" alt="<?=$arItem['NAME']?>">
							<?}?>	
							<?if($arItem['PROPERTIES']['LINK']['VALUE']){?>
								</a>
							<?}?>	
						</div>
						<div class="b-mainslider_descr iblock">
							<h3>
								<?if($arItem['PROPERTIES']['LINK']['VALUE']){?>
									<a href="<?=$arItem['PROPERTIES']['LINK']['VALUE']?>">
								<?}?>
								<?=$arItem['NAME']?>
								<?if($arItem['PROPERTIES']['LINK']['VALUE']){?>
									</a>
								<?}?>
							</h3>
							<p><?=$arItem['~PREVIEW_TEXT']?></p>
						</div>
					</div>
				</div>
			<?}?>
		<?}?>
	</div> <!-- /.b-mainslider -->
<?}?>