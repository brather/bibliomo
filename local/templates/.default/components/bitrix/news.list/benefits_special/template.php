<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if(count($arResult["ITEMS"])):?>
<!--<h2 class="b-mainslider_tit"><a href="/special/advantages/" title="преимущества ЕИСУБ">преимущества ЕИСУБ</a></h2>-->
<h2 class="b-mainslider_tit">преимущества ЕИСУБ</h2>
<div class="b-mainslider">
    <?foreach($arResult["ITEMS"] as $slide):?>
        <div>
            <div class="sliderinner">
                <div class="b-mainslider_photo iblock"><img class="loadingimg" src="<?=$slide['PREVIEW_PICTURE_RESIZE']?>" alt=""></div>
                <div class="b-mainslider_descr iblock">
                    <h3><?=$slide['NAME']?></h3>
                    <p><?=$slide['~PREVIEW_TEXT']?></p>
                </div>
            </div>
        </div>
    <?endforeach;?>
</div> <!-- /.b-mainslider -->
<?endif;?>