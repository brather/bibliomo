<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
?>
<div class="wrapper">
<?
if($arResult['ERROR_MESSAGE'])
	ShowMessage($arResult['ERROR_MESSAGE']);
?>
<?
$arServices = $arResult["AUTH_SERVICES_ICONS"];
if(!empty($arResult["AUTH_SERVICES"]))
{
	?>
	<div class="b-form_header"><?=GetMessage("SS_GET_COMPONENT_INFO")?></div>
	<?
	$APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "",
		array(
			"AUTH_SERVICES"=>$arResult["AUTH_SERVICES"],
			"CURRENT_SERVICE"=>$arResult["CURRENT_SERVICE"],
			"AUTH_URL"=>$arResult['CURRENTURL'],
			"POST"=>$arResult["POST"],
			"SHOW_TITLES"=>'N',
			"FOR_SPLIT"=>'Y',
			"AUTH_LINE"=>'N',
		),
		$component,
		array("HIDE_ICONS"=>"Y")
	);
	?>
	<?
}

if(isset($arResult["DB_SOCSERV_USER"]) && $arParams["SHOW_PROFILES"] != 'N')
{
	?>
	<table cellpadding="0" cellspacing="0" class="soc-table">
		<?
		foreach($arResult["DB_SOCSERV_USER"] as $key => $arUser)
		{
			if(!$icon = htmlspecialcharsbx($arResult["AUTH_SERVICES_ICONS"][$arUser["EXTERNAL_AUTH_ID"]]["ICON"]))
				$icon = 'openid';
			$authID = ($arServices[$arUser["EXTERNAL_AUTH_ID"]]["NAME"]) ? $arServices[$arUser["EXTERNAL_AUTH_ID"]]["NAME"] : $arUser["EXTERNAL_AUTH_ID"];
			?>
			<tr>
				<td>
					<?if ($arUser["PERSONAL_LINK"] != ''):?>
					<a class="<?=$icon?>" target="_blank" href="<?=$arUser["PERSONAL_LINK"]?>"></a>
						<?else:?>
						<a class="<?=$icon?>" href="javascript:void(null)"></a>
				<?endif;?>
				</td>
				<td>
					<?if (in_array($arUser["ID"], $arResult["ALLOW_DELETE_ID"])):?>
						<a class="split-delete-item" href="<?=htmlspecialcharsbx($arUser["DELETE_LINK"])?>" onclick="return confirm('<?=GetMessage("SS_PROFILE_DELETE_CONFIRM")?>')" title="<?=GetMessage("SS_DELETE")?>"><?=GetMessage("SS_DELETE")?></a>
					<?endif;?>
				</td>
			</tr>
			<?
		}
		?>
	</table>
	<?
}
?>
</div>