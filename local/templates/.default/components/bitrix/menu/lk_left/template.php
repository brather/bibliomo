<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
#pre($arResult,1);
?>
<?if (!empty($arResult)):?>
<ul class="b-profile_nav <?if($arParams['IS_LIBRARY']):?>lib-profile_nav<?endif;?>">

<?
$previousLevel = 0;
foreach($arResult as $arItem):?>

	<?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
		<?=str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
	<?endif?>

	<?if ($arItem["IS_PARENT"]):?>

		<?if ($arItem["DEPTH_LEVEL"] == 1):?>
			<li><a href="<?=$arItem["LINK"]?>" class="<?=$arItem["SELECTED"]? 'current ':''?><?=$arItem['PARAMS']['CLASS']?>"><?=$arItem["TEXT"]?></a>
				<ul class="b-profile_subnav">
		<?endif?>

	<?else:?>

		<?if ($arItem["PERMISSION"] > "D"):?>

			<?if ($arItem["DEPTH_LEVEL"] == 1):?>
				<li><a href="<?=$arItem["LINK"]?>" class="<?=$arItem["SELECTED"]? 'current ':''?><?=$arItem['PARAMS']['CLASS']?>"><?if($arItem['PARAMS']['STRONG'] === true){?><strong><?}?><?=$arItem["TEXT"]?><?if($arItem['PARAMS']['STRONG'] === true){?></strong><?}?></a></li>
			<?else:?>
				<li><span class="b-profile_subnav_border"><?if(!empty($arItem['PARAMS']['MSGNUM'])){?><span class="b-profile_msgnum"><?=$arItem['PARAMS']['MSGNUM']?></span><?}?><a href="<?=$arItem["LINK"]?>" class="<?=$arItem["SELECTED"]? 'current ':''?><?=$arItem['PARAMS']['CLASS']?>"><?if($arItem['PARAMS']['STRONG'] === true){?><strong><?}?><?=$arItem["TEXT"]?><?if($arItem['PARAMS']['STRONG'] === true){?></strong><?}?></a></span></li>
			<?endif?>
		<?endif?>

	<?endif?>

	<?$previousLevel = $arItem["DEPTH_LEVEL"];?>

<?endforeach?>

<?if ($previousLevel > 1)://close last item tags?>
	<?=str_repeat("</ul></li>", ($previousLevel-1) );?>
<?endif?>

</ul>

<div class="b-razdel">
    <select class="js_select">
        <option value="-1">Выберите раздел</option>
        <?foreach($arResult as $arItem):?>
            <option value="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></option>
        <?endforeach;?>
    </select>
</div>

<div class="menu-clear-left"></div>
<?endif?>