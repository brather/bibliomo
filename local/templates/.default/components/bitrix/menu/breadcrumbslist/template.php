<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)) {?>
    <ul class="b-breadcrumblist">
        <?foreach ($arResult as $item) {?>
            <li><a href="<?=$item['LINK']?>" <?=($arItem['SELECTED'] ? ' class="current"' : '')?>><?=$item['TEXT']?></a></li>
        <?}?>
    </ul>
<?}?>