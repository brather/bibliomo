<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use \Bitrix\Main\Localization\Loc as Loc;
Loc::loadMessages(__FILE__);
?>

<section class="innersection innerwrapper clearfix">
<div class="b-profile_set b-profile_brd rel">
<h2 class="mode"><?=Loc::getMessage('MAIN_REGISTER_TITLE');?></h2>

<?ShowError($arResult["strProfileError"]);?>
<?
if ($arResult['DATA_SAVED'] == 'Y')
    ShowNote(Loc::getMessage('MAIN_REGISTER_SUCCESS_NOTE'));
?>
<p class="note">
    <?=Loc::getMessage('MAIN_REGISTER_NOTE');?>
</p>
<form method="post" action="<?=$arResult["FORM_TARGET"]?>" class="b-form b-form_common b-profile_setform" name="form1" enctype="multipart/form-data" id="regform">
    <?
    if (count($arResult["ERRORS"]) > 0){
        foreach ($arResult["ERRORS"] as $key => $error){
            if (intval($key) == 0 && $key !== 0) {
                $arResult["ERRORS"][$key] = str_replace("#FIELD_NAME#", "&quot;".GetMessage("REGISTER_FIELD_".$key)."&quot;", $error);
            }
        }

        //ShowError(implode("<br />", $arResult["ERRORS"]).'<br /><br />');
        ?>
        <p class="error_message"><?=implode("<br />", $arResult["ERRORS"]).'<br /><br />'?></p>
    <?

    }
    ?>
<?
if($arResult["BACKURL"] <> ''){
    ?>
    <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
<?
}

$date = explode('.', $arResult['arUser']['PERSONAL_BIRTHDAY']);
foreach($date as $dates)
{
    $arDate[] = intval($dates);
}
?>
<?=$arResult["BX_SESSION_CHECK"]?>
<input type="hidden" name="lang" value="<?=LANG?>" />
<input type="hidden" name="ID" value=<?=$arResult["ID"]?> />
<input type="hidden" class="input" name="LOGIN" value="<?=$arResult['arUser']['LOGIN']?>">
<input type="hidden" id='mail' class="input" value="<?=$arResult['arUser']['EMAIL']?>" name="EMAIL">

<div class="wrapfield">
    <div class="b-form_header"><span class="iblock"><?=Loc::getMessage('MAIN_REGISTER_PASSPORT_DETAILS');?></span></div>
    <div class="fieldrow nowrap">
        <div class="fieldcell iblock">
            <em class="hint">*</em>
            <label for="settings01"><?=Loc::getMessage('MAIN_REGISTER_LASTNAME');?></label>
            <div class="field validate">
                <em class="error required"><?=Loc::getMessage('MAIN_REGISTER_REQ');?></em>
                <em class="error validate"><?=Loc::getMessage('MAIN_REGISTER_WRONG_FORMAT_CLEAR');?></em>
                <em class="error maxlength"><?=Loc::getMessage('MAIN_REGISTER_MORE_30_SYMBOLS');?></em>
                <input type="text" data-validate="fio" data-required="required" value="<?=$arResult['arUser']['LAST_NAME']?>"  id="settings01" data-maxlength="30" data-minlength="2" name="LAST_NAME" class="input">
            </div>
        </div>
    </div>
    <div class="fieldrow nowrap">
        <div class="fieldcell iblock">
            <em class="hint">*</em>
            <label for="settings02"><?=Loc::getMessage('MAIN_REGISTER_FIRSTNAME');?></label>
            <div class="field validate">
                <em class="error required"><?=Loc::getMessage('MAIN_REGISTER_REQ');?></em>
                <em class="error validate"><?=Loc::getMessage('MAIN_REGISTER_WRONG_FORMAT_CLEAR');?></em>
                <em class="error maxlength"><?=Loc::getMessage('MAIN_REGISTER_MORE_30_SYMBOLS');?></em>
                <input type="text" data-required="required" data-validate="fio" value="<?=$arResult['arUser']['NAME']?>" id="settings02" data-maxlength="30" data-minlength="2" name="NAME" class="input" >
            </div>
        </div>
    </div>
    <div class="fieldrow nowrap">
        <div class="fieldcell iblock">
            <label for="settings22"><?=Loc::getMessage('MAIN_REGISTER_SECNAME');?></label>
            <div class="field validate">
                <em class="error required"><?=Loc::getMessage('MAIN_REGISTER_REQ');?></em>
                <em class="error validate"><?=Loc::getMessage('MAIN_REGISTER_WRONG_FORMAT_CLEAR');?></em>
                <em class="error maxlength"><?=Loc::getMessage('MAIN_REGISTER_MORE_30_SYMBOLS');?></em>
                <input type="text" data-validate="fio" value="<?=$arResult['arUser']['SECOND_NAME']?>" id="settings22" data-maxlength="30" data-minlength="2"data-required="false"  name="SECOND_NAME" class="input" >
            </div>
        </div>
    </div>
    <div class="fieldrow nowrap">
        <div class="fieldcell iblock">
            <em class="hint">*</em>
            <label for="settings06"><?=Loc::getMessage('MAIN_REGISTER_BIRTHDAY');?></label>
            <div class="field validate iblock seldate" data-yearsrestrict="12">
                <em class="error dateformat"><?=Loc::getMessage('MAIN_REGISTER_WRONG_FORMAT');?></em>
                <em class="error required"><?=Loc::getMessage('MAIN_REGISTER_FILL');?></em>
                <em class="error yearsrestrict"><?=Loc::getMessage('MAIN_REGISTER_AGE');?></em>
                <input type="hidden" class="realseldate" data-required="true" value="" id="settings06" name="PERSONAL_BIRTHDAY">
                <select name="birthday" id="PERSONAL_BIRTHDAY_D"  class="js_select w102 sel_day"  data-required="true">
                    <option value=""><?=GetMessage('DAY');?></option>
                    <?for($i = 1; $i <= 31; $i++)
                    {?>
                        <option <?if($arDate['0'] == $i):?>selected='selected'<?endif;?> value="<?=$i?>"><?=$i?></option>
                    <?}?>
                </select>
                <select name="birthmonth" id="PERSONAL_BIRTHDAY_M" class="js_select w165 sel_month"  data-required="true" >
                    <option value=""><?=GetMessage('MONTH');?></option>
                    <? for($i=1;$i<=12;$i++) { ?>
                        <option <?if($arDate['1'] == $i){?> selected='selected' <?}?> value="<?=$i?>"><?=FormatDate('f', MakeTimeStamp('01.'.$i.'.'.date('Y')))?></option>
                    <? } ?>
                </select>
                <select name="birthyear" id="PERSONAL_BIRTHDAY_Y"  class="js_select w102 sel_year"  data-required="true">
                    <option value=""><?=GetMessage('YEAR');?></option>
                    <? for($i=(date('Y')-14);$i>=(date('Y')-95);$i--)
                    {?>
                        <option <?if($arDate['2'] == $i){?> selected='selected' <?}?> value="<?=$i?>"><?=$i?></option>
                    <?}?>
                </select>
            </div>

        </div>
    </div>
    <div class="fieldrow nowrap">
        <div class="fieldcell iblock">
            <em class="hint">*</em>
            <label for="settings011"><?=Loc::getMessage('MAIN_REGISTER_PASSPORT_NUMBER');?></label>
            <div class="field validate">
                <em class="error required"><?=Loc::getMessage('MAIN_REGISTER_REQ');?></em>
                <div class="field iblock"><input type="text"  data-required="required" value="<?=$arResult['arUser']['UF_PASSPORT_SERIES']?>" id="settings011" name="UF_PASSPORT_SERIES" class="input w110"></div>
                <div class="field iblock"><input type="text"  data-required="required" value="<?=$arResult['arUser']['UF_PASSPORT_NUMBER']?>" id="settings022" name="UF_PASSPORT_NUMBER" class="input w190"></div>
            </div>
        </div>
    </div>
</div>

<div class="wrapfield">
    <div class="b-form_header"><span class="iblock"><?=Loc::getMessage('MAIN_REGISTER_SPECIALIZATION');?></span></div>
    <div class="fieldrow nowrap">
        <div class="fieldcell iblock">
            <em class="hint">*</em>
            <label for="settings10"><?=Loc::getMessage('MAIN_REGISTER_PLACE_OF_EMPLOYMENT');?></label>
            <div class="field validate">
                <em class="error required"><?=Loc::getMessage('MAIN_REGISTER_REQ');?></em>
                <input type="text" value="<?=$arResult['arUser']['WORK_COMPANY']?>" id="settings10" data-maxlength="60" data-minlength="2" data-required="true" name="WORK_COMPANY" class="input" >
            </div>
        </div>
    </div>
    <div class="fieldrow nowrap">
        <div class="fieldcell iblock">
            <em class="hint">*</em>
            <label for="settings08"><?=Loc::getMessage('MAIN_REGISTER_KNOWLEDGE');?></label>
            <div class="field validate iblock">
                <em class="error required"><?=Loc::getMessage('MAIN_REGISTER_REQ');?></em>
                <select name="UF_BRANCH_KNOWLEDGE" id="settings08"  class="js_select w370"  data-required="true">
                    <option value=""><?=Loc::getMessage('MAIN_REGISTER_SELECT');?></option>
                    <?
                    $arUF_BRANCH_KNOWLEDGE = nebUser::getFieldEnum(array('USER_FIELD_NAME' => 'UF_BRANCH_KNOWLEDGE'));
                    if(!empty($arUF_BRANCH_KNOWLEDGE))
                    {
                        foreach($arUF_BRANCH_KNOWLEDGE as $arItem)
                        {
                            ?>
                            <option <?=$arResult["arUser"]['UF_BRANCH_KNOWLEDGE'] == $arItem['ID'] ? 'selected="selected"':''?> value="<?=$arItem['ID']?>"><?=$arItem['VALUE']?></option>
                        <?
                        }
                    }
                    ?>
                </select>
            </div>
        </div>
    </div>
    <div class="fieldrow nowrap">
        <div class="fieldcell iblock">
            <em class="hint">*</em>
            <label for="settings09"><?=Loc::getMessage('MAIN_REGISTER_EDUCATION');?></label>
            <div class="field validate iblock">
                <em class="error required"><?=Loc::getMessage('MAIN_REGISTER_REQ');?></em>
                <select name="UF_EDUCATION" id="settings09"  class="js_select w370"  data-required="true">
                    <option value=""><?=Loc::getMessage('MAIN_REGISTER_SELECT');?></option>
                    <?
                    $arUF_EDUCATION = nebUser::getFieldEnum(array('USER_FIELD_NAME' => 'UF_EDUCATION'));
                    if(!empty($arUF_EDUCATION))
                    {
                        foreach($arUF_EDUCATION as $arItem)
                        {
                            ?>
                            <option <?=$arResult["arUser"]['UF_EDUCATION'] == $arItem['ID'] ? 'selected="selected"':''?> value="<?=$arItem['ID']?>"><?=$arItem['VALUE']?></option>
                        <?
                        }
                    }
                    ?>
                </select>
            </div>
        </div>
    </div>
    <div class="fieldrow nowrap">
        <div class="fieldcell iblock">
            <em class="hint">*</em>
            <label for="settings09">Гражданство</label>
            <div class="field validate iblock">
                <em class="error required"><?=Loc::getMessage('MAIN_REGISTER_REQ');?></em>
                <select name="UF_CITIZENSHIP" id="settings12"  class="js_select w370"  data-required="true">
                    <option value="">выберите</option>
                    <?
                    $arUF_CITIZENSHIP = nebUser::getFieldEnum(array('USER_FIELD_NAME' => 'UF_CITIZENSHIP'));
                    if(!empty($arUF_CITIZENSHIP))
                    {
                        foreach($arUF_CITIZENSHIP as $arItem)
                        {
                            if($arItem['DEF']=="Y")
                            {
                                $ruCitizenship = $arItem['ID'];
                            }
                            ?>
                            <option <?=$arResult["arUser"]['UF_CITIZENSHIP'] == $arItem['ID'] ? 'selected="selected"':''?> value="<?=$arItem['ID']?>"><?=$arItem['VALUE']?></option>
                        <?
                        }
                    }
                    ?>
                </select>
                <script type="text/javascript">
                    $(function(){
                        var copyEducation = $('select[name="UF_EDUCATION"]').clone().removeClass("custom");
                        function correctEducationField()
                        {
                            var cit = $('select[name="UF_CITIZENSHIP"] option:selected').val();
                            var parentEducation = $('select[name="UF_EDUCATION"]').closest("div.field");
                            parentEducation.children("span").remove();
                            parentEducation.append(copyEducation.clone());
                            $('#UF_EDUCATIONopts').remove();
                            if(cit == <?php echo $ruCitizenship;?>)
                            {
                                $('select[name="UF_EDUCATION"] option').filter('[VALUE="33"],[VALUE="34"],[VALUE="35"]').remove();
                            }
                            else
                            {
                                $('select[name="UF_EDUCATION"] option').not('[VALUE=""],[VALUE="33"],[VALUE="34"],[VALUE="35"]').remove();
                            }
                            $('select[name="UF_EDUCATION"]').selectReplace();
                        }
                        correctEducationField();
                        $('select[name="UF_CITIZENSHIP"]').change(function(){
                            correctEducationField();
                        });
                    });
                </script>
            </div>
        </div>
    </div>
</div>


<div class="wrapfield">
    <div class="b-form_header"><span class="iblock"><?=Loc::getMessage('REGISTER_GROUP_ADDRESS_REGISTERED');?></span></div>
    <div class="fieldrow nowrap">
        <div class="fieldcell iblock">
            <em class="hint">*</em>
            <label for="settings03"><?=Loc::getMessage('REGISTER_FIELD_PERSONAL_ZIP');?> <span class="minscreen"><?=Loc::getMessage('REGISTER_GROUP_ADDRESS_REG_NOTE');?></span></label>
            <div class="field validate w140">
                <em class="error required"><?=Loc::getMessage('MAIN_REGISTER_FILL');?></em>
                <em class="error number"><?=Loc::getMessage('REGISTER_FIELD_PERSONAL_ZIP_INCORRECT');?></em>
                <input type="text" data-validate="number" data-required="required" value="<?=$arResult['arUser']['PERSONAL_ZIP']?>" id="settings03" name="PERSONAL_ZIP" class="input ">
            </div>
        </div>
    </div>
    <div class="fieldrow nowrap">
		<div class="fieldcell iblock">
			<em class="hint">*</em>
			<label for="settings033"><?=Loc::getMessage('MAIN_REGISTER_REGION');?> <span class="minscreen"><?=Loc::getMessage('REGISTER_GROUP_ADDRESS_REG_NOTE');?></span></label>
			<div class="field validate">
				<em class="error required"><?=Loc::getMessage('MAIN_REGISTER_FILL');?></em>
				<input type="text" data-required="required" value="<?=$arResult["arUser"]['UF_REGION']?>" id="settings033" data-maxlength="30" data-minlength="2" name="UF_REGION" class="input">
			</div>
		</div>
	</div>
	<div class="fieldrow nowrap">
		<div class="fieldcell iblock">
			<label for="settings013"><?=Loc::getMessage('MAIN_REGISTER_AREA');?> <span class="minscreen"><?=Loc::getMessage('REGISTER_GROUP_ADDRESS_REG_NOTE');?></span></label>
			<div class="field validate">
				<input type="text" value="<?=$arResult["arUser"]['UF_AREA']?>" id="settings013" data-maxlength="30" data-minlength="2" name="UF_AREA" class="input">
			</div>
		</div>
	</div>
	<div class="fieldrow nowrap">
		<div class="fieldcell iblock">
			<em class="hint">*</em>
			<label for="settings055"><?=Loc::getMessage('REGISTER_FIELD_PERSONAL_CITY');?> <span class="minscreen"><?=Loc::getMessage('REGISTER_GROUP_ADDRESS_REG_NOTE');?></span></label>
			<div class="field validate">
				<em class="error required"><?=Loc::getMessage('MAIN_REGISTER_FILL');?></em>
				<input type="text" data-required="required" value="<?=$arResult["arUser"]['PERSONAL_CITY']?>" id="settings055" data-maxlength="30" data-minlength="2" name="REGISTER[PERSONAL_CITY]" class="input" >										
			</div>
		</div>
	</div>
    <div class="fieldrow nowrap">
        <div class="fieldcell iblock">
            <em class="hint">*</em>
            <label for="settings07"><?=Loc::getMessage('REGISTER_FIELD_PERSONAL_STREET');?> <span class="minscreen"><?=Loc::getMessage('REGISTER_GROUP_ADDRESS_REG_NOTE');?></span></label>
            <div class="field validate">
				<em class="error required"><?=Loc::getMessage('MAIN_REGISTER_FILL');?></em>
                <input type="text" value="<?=$arResult['arUser']['PERSONAL_STREET']?>" id="settings07" data-maxlength="30" data-minlength="2" data-required="true"  name="PERSONAL_STREET" class="input">
            </div>
        </div>
    </div>
    <div class="fieldrow nowrap">
        <div class="fieldcell iblock">
            <em class="hint">*</em>
            <div class="field iblock fieldthree ">
                <label for="settings09"><?=Loc::getMessage('REGISTER_FIELD_PERSONAL_HOUSE');?>:</label>
                <div class="field validate">
                    <input type="text" class="input" name="UF_CORPUS" data-maxlength="20" id="settings09" value="<?=$arResult['arUser']['UF_CORPUS']?>"  data-required="true"/>
                    <em class="error required"><?=Loc::getMessage('MAIN_REGISTER_FILL');?></em>
                </div>
            </div>
            <div class="field iblock fieldthree">
                <label for="settings10"><?=Loc::getMessage('REGISTER_FIELD_PERSONAL_BUILDING');?>: </label>
                <div class="field">
                    <input type="text" class="input" name="UF_STRUCTURE" data-maxlength="20" maxlength="4" id="settings10" value="<?=$arResult["arUser"]['UF_STRUCTURE']?>" />
                </div>
            </div>
            <div class="field iblock fieldthree">
                <label for="settings11"><?=Loc::getMessage('REGISTER_FIELD_PERSONAL_APARTMENT');?>: </label>
                <div class="field">
                    <input type="text" class="input" name="UF_FLAT" data-maxlength="20" maxlength="4" id="settings11" value="<?=$arResult["arUser"]['UF_FLAT']?>" />
                </div>
            </div>
        </div>
    </div>
</div>
<div class="wrapfield">
    <div class="b-form_header">
        <span class="iblock"><?=Loc::getMessage('REGISTER_GROUP_ADDRESS_RESIDENCE');?></span>
        <div class="checkwrapper">
            <input class="checkbox addrindently" type="addr" name="UF_PLACE_REGISTR" value="1" id="cb3r"><label for="cb3r" class="black"><?=Loc::getMessage('REGISTER_GROUP_ADDRESS_CHECKBOX_NOTE');?></label>
            <?
            if($arResult["arUser"]['UF_PLACE_REGISTR'] == '1')
            {
                ?>
                <script type="text/javascript">
                    $( document ).ready(function() {
                        $('input[name="UF_PLACE_REGISTR"]').click();
                    });
                </script>
            <?

            }
            ?>

        </div>
    </div>
    <div class="fieldrow nowrap">
        <div class="fieldcell iblock inly">
            <label for="settings044"><?=Loc::getMessage('REGISTER_FIELD_PERSONAL_ZIP');?> <span class="minscreen"><?=Loc::getMessage('REGISTER_GROUP_ADDRESS_RES_NOTE');?></span></label>
            <div class="field validate w140">
                <input type="text" data-validate="number" value="<?=$arResult["arUser"]['WORK_ZIP']?>" id="settings044" name="WORK_ZIP" class="input">
            </div>
        </div>
    </div>
    <div class="fieldrow nowrap">
		<div class="fieldcell iblock inly">
			<label for="settings033r"><?=Loc::getMessage('MAIN_REGISTER_REGION');?> <span class="minscreen"><?=Loc::getMessage('REGISTER_GROUP_ADDRESS_RES_NOTE');?></span></label>
			<div class="field validate">
				<input type="text" value="<?=$arResult["arUser"]['UF_REGION2']?>" id="settings033r" data-maxlength="30" data-minlength="2" name="UF_REGION2" class="input">
			</div>
		</div>
	</div>
	<div class="fieldrow nowrap">
		<div class="fieldcell iblock inly">
			<label for="settings013r"><?=Loc::getMessage('MAIN_REGISTER_AREA');?> <span class="minscreen"><?=Loc::getMessage('REGISTER_GROUP_ADDRESS_RES_NOTE');?></span></label>
			<div class="field validate">
				<input type="text" value="<?=$arResult["arUser"]['UF_AREA2']?>" id="settings013r" data-maxlength="30" data-minlength="2" name="UF_AREA2" class="input">
			</div>
		</div>
	</div>
	<div class="fieldrow nowrap">
		<div class="fieldcell iblock inly">
			<label for="settings066"><?=Loc::getMessage('REGISTER_FIELD_PERSONAL_CITY');?> <span class="minscreen"><?=Loc::getMessage('REGISTER_GROUP_ADDRESS_RES_NOTE');?></span></label>
			<div class="field validate">
				<input type="text" value="<?=$arResult["arUser"]['WORK_CITY']?>" id="settings066" data-maxlength="30" data-minlength="2" name="REGISTER[WORK_CITY]" class="input" >
			</div>
		</div> 
	</div>
    <div class="fieldrow nowrap">
        <div class="fieldcell iblock inly">
            <label for="settings08r"><?=Loc::getMessage('REGISTER_FIELD_PERSONAL_STREET');?> <span class="minscreen"><?=Loc::getMessage('REGISTER_GROUP_ADDRESS_RES_NOTE');?></span></label>
            <div class="field validate">
                <input type="text" value="<?=$arResult["arUser"]['WORK_STREET']?>" id="settings08r" data-maxlength="30" data-minlength="2" name="WORK_STREET" class="input">
            </div>
        </div>
    </div>
    <div class="fieldrow nowrap">
        <div class="fieldcell iblock inly">
            <div class="field iblock fieldthree ">
                <label for="settings09r"><?=Loc::getMessage('REGISTER_FIELD_PERSONAL_HOUSE');?>:</label>
                <div class="field validate">
                    <input type="text" class="input" name="UF_HOUSE2" data-maxlength="20" id="settings09r" value="<?=$arResult["arUser"]['UF_HOUSE2']?>"/>
                </div>
            </div>
            <div class="field iblock fieldthree">
                <label for="settings10"><?=Loc::getMessage('REGISTER_FIELD_PERSONAL_BUILDING');?>: </label>
                <div class="field">
                    <input type="text" class="input" name="UF_STRUCTURE2" data-maxlength="20" maxlength="4" id="settings10" value="<?=$arResult["arUser"]['UF_STRUCTURE2']?>" />
                </div>
            </div>
            <div class="field iblock fieldthree">
                <label for="settings11"><?=Loc::getMessage('REGISTER_FIELD_PERSONAL_APARTMENT');?>: </label>
                <div class="field">
                    <input type="text" class="input" name="UF_FLAT2" data-maxlength="20" maxlength="4" id="settings11" value="<?=$arResult["arUser"]['UF_FLAT2']?>" />
                </div>
            </div>
        </div>
    </div>
</div>

<hr>

<?$APPLICATION->IncludeComponent(
    "notaext:plupload",
    "scan_passport1",
    array(
        "MAX_FILE_SIZE" => "24",
        "FILE_TYPES" => "jpg,jpeg,png",
        "DIR" => "tmp_register",
        "FILES_FIELD_NAME" => "profile_file",
        "MULTI_SELECTION" => "N",
        "CLEANUP_DIR" => "Y",
        "UPLOAD_AUTO_START" => "Y",
        "RESIZE_IMAGES" => "Y",
        "RESIZE_WIDTH" => "4000",
        "RESIZE_HEIGHT" => "4000",
        "RESIZE_CROP" => "Y",
        "RESIZE_QUALITY" => "98",
        "UNIQUE_NAMES" => "Y",
        "SCAN1_PHOTO" => (intval($arResult['arUser']['UF_SCAN_PASSPORT1']) > 0)? CFile::GetPath($arResult['arUser']['UF_SCAN_PASSPORT1']):'',
    ),
    false
);?>


<?$APPLICATION->IncludeComponent(
    "notaext:plupload",
    "scan_passport2",
    array(
        "MAX_FILE_SIZE" => "24",
        "FILE_TYPES" => "jpg,jpeg,png",
        "DIR" => "tmp_register",
        "FILES_FIELD_NAME" => "profile_file",
        "MULTI_SELECTION" => "N",
        "CLEANUP_DIR" => "Y",
        "UPLOAD_AUTO_START" => "Y",
        "RESIZE_IMAGES" => "Y",
        "RESIZE_WIDTH" => "4000",
        "RESIZE_HEIGHT" => "4000",
        "RESIZE_CROP" => "Y",
        "RESIZE_QUALITY" => "98",
        "UNIQUE_NAMES" => "Y",
        "SCAN1_PHOTO" => (intval($arResult['arUser']['UF_SCAN_PASSPORT2']) > 0)? CFile::GetPath($arResult['arUser']['UF_SCAN_PASSPORT2']):'',
    ),
    false
);?>

<hr>

<div class="checkwrapper">
	<label><?=Loc::getMessage('MAIN_REGISTER_AGREE_FOR_POPUP');?> </label> <a href="/local/components/neb/registration/templates/.default/ajax_agreement.php" target="_blank" class="popup_opener ajax_opener closein" data-width="955" data-height="auto"><?=Loc::getMessage('MAIN_REGISTER_AGREE_FOR_POPUP_TERMS');?></a>
</div>

<div class="fieldrow nowrap fieldrowaction">
    <div class="fieldcell ">
        <div class="field clearfix divdisable">
            <button disabled="disabled" name="save" type="submit" value="1" class="formbutton btdisable"><?=Loc::getMessage('MAIN_REGISTER_UPDATE');?></button>
			<div class="b-hint"><?=Loc::getMessage('MAIN_REGISTER_READ_TERMS');?></div>
        </div>
    </div>
</div>
</form>
</div><!-- /.b-registration-->
</section>
