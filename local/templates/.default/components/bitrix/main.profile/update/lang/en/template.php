<?
$MESS ['AUTH_FORGOT_PASSWORD_1'] = "Send for email";
$MESS ['AUTH_EMAIL'] = "E-Mail:";
$MESS ['RECOVERY'] = "recovery password";
$MESS ['REQUIRED'] = "Required";
$MESS ['SEND'] = "Send";
$MESS["MAIN_REGISTER_AGREE_FOR_POPUP"] = 'Before you register, you must read';
$MESS["MAIN_REGISTER_AGREE_FOR_POPUP_TERMS"] = 'the Terms of Use';
$MESS["MAIN_REGISTER_READ_TERMS"] = 'Read the Terms of Use';
$MESS["MAIN_REGISTER_UPDATE"] = 'Update';
$MESS["MAIN_REGISTER_TITLE"] = 'Updating Data on the portal';
$MESS["MAIN_REGISTER_SUCCESS_NOTE"] = 'Specified in the questionnaire data is successfully verified.
Within a few hours of your profile will be fully tested by the operator group recording readers NEB.
The result of verification will be sent to your e-pochtu.V case of inconsistencies your access may be blocked. <br /> <br />';
$MESS["MAIN_REGISTER_NOTE"] = 'In this section, you can update your registration data to get full access to the services of the portal.';
$MESS["MAIN_REGISTER_SPECIALIZATION"] = 'Area of Specialization';
$MESS["MAIN_REGISTER_PLACE_OF_EMPLOYMENT"] = 'Place of work / study';
$MESS["MAIN_REGISTER_KNOWLEDGE"] = 'Branch of knowledge';
$MESS["MAIN_REGISTER_SELECT"] = 'select';
$MESS["MAIN_REGISTER_EDUCATION"] = 'Education. Academic degree';
$MESS["MAIN_REGISTER_CITIZENSHIP"] = 'Citizenship';
$MESS["MAIN_REGISTER_REGION"] = 'Region';
$MESS["MAIN_REGISTER_AREA"] = 'Area';

$MESS["REGISTER_GROUP_ADDRESS_REGISTERED"] = "Registered address";
$MESS["REGISTER_GROUP_ADDRESS_RESIDENCE"] = "Residence address";
$MESS["REGISTER_GROUP_ADDRESS_REG_NOTE"] = "registered";
$MESS["REGISTER_GROUP_ADDRESS_RES_NOTE"] = "residence";
$MESS["REGISTER_GROUP_ADDRESS_CHECKBOX_NOTE"] = "the place of registration";
$MESS["REGISTER_FIELD_PERSONAL_STREET"] = "Street";
$MESS["REGISTER_FIELD_PERSONAL_HOUSE"] = "House";
$MESS["REGISTER_FIELD_PERSONAL_BUILDING"] = "Building";
$MESS["REGISTER_FIELD_PERSONAL_APARTMENT"] = "Apartment";
$MESS["REGISTER_FIELD_PERSONAL_CITY"] = "City";
$MESS["REGISTER_FIELD_PERSONAL_ZIP"] = "Zip code";
$MESS["REGISTER_FIELD_PERSONAL_ZIP_INVALID"] = "Invalid postcode";
$MESS["REGISTER_FIELD_PERSONAL_COUNTRY"] = "Country";
$MESS["REGISTER_FIELD_PERSONAL_NOTES"] = "Notes";
$MESS["MAIN_REGISTER_PASSPORT_DETAILS"] = "Passport data";
$MESS["MAIN_REGISTER_PASSPORT_NUMBER"] = "Passport number";
$MESS["MAIN_REGISTER_FILL"] = 'Fill in';
$MESS["MAIN_REGISTER_LASTNAME"] = 'Last Name';
$MESS["MAIN_REGISTER_FIRSTNAME"] = 'First Name';
$MESS["MAIN_REGISTER_SECNAME"] = 'Second Name';
$MESS["MAIN_REGISTER_BIRTHDAY"] = 'Date of birth';
$MESS["MAIN_REGISTER_REQ"] = 'Must be filled in';
$MESS["MAIN_REGISTER_MORE_30_SYMBOLS"] = 'More than 30 characters';
$MESS["MAIN_REGISTER_WRONG_FORMAT_CLEAR"] = 'Invalid format';
?>