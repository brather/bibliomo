<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

//меняем пользователю тип регистрации
if($arResult['DATA_SAVED'] == 'Y' &&
    $arResult['arUser']['UF_SCAN_PASSPORT1'] &&
    $arResult['arUser']['UF_SCAN_PASSPORT2'] &&
    $arResult['arUser']['UF_PASSPORT_SERIES'] &&
    $arResult['arUser']['UF_PASSPORT_NUMBER'])
{
    global $USER;
    $USER->Update($USER->GetID(), Array('UF_REGISTER_TYPE' => '38'));
}