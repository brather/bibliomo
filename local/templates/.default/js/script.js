function checkIsAvailablePdf(){
	$( "div.search-result" ).each(function() {
		var book_item = this;

		if(!$(book_item).data('CheckIsAvailable'))
		{
			$(this).find('.b-result-type_txt').each(function() {
				if($(this).data('ext') == 'pdf'){
					var obPdf = this;
					$.getJSON( "/local/tools/viewer/isavailable.php", { book_id: book_item.id }, function( data ) {
						if(data.isAvailable){
							$(obPdf).show();
						}
						else
						{
							//$(obPdf).remove();
						}

						$(book_item).data('CheckIsAvailable', true);
					});
				}
			});
		}
	});
}