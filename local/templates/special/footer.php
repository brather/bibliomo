<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?
if(defined('STATIC_PAGE'))
{
    ?>
		</div><!-- /.b-plaintext -->
	</div><!-- /.b-mainblock -->
</section>
<?
}
?>
</div><!-- /.homepage -->
<footer>
    <div class="wrapper">
        <div class="b-footer_row clearfix">
            <?$APPLICATION->IncludeFile("/local/include_areas/logo_bottom.php", Array(), Array("MODE" => "text"));?>
            <div class="footer-container">
                <?$APPLICATION->IncludeComponent("bitrix:menu", "bottom", array(
                    "ROOT_MENU_TYPE" => "bottom1_" . LANGUAGE_ID,
                    "MENU_CACHE_TYPE" => "N",
                    "MENU_CACHE_TIME" => "3600",
                    "MENU_CACHE_USE_GROUPS" => "Y",
                    "MENU_CACHE_GET_VARS" => array(
                    ),
                    "MAX_LEVEL" => "1",
                    "CHILD_MENU_TYPE" => "left",
                    "USE_EXT" => "N",
                    "DELAY" => "N",
                    "ALLOW_MULTI_SELECT" => "N"
                ),
                    false
                );?>
                <?$APPLICATION->IncludeComponent("bitrix:menu", "bottom", array(
                    "ROOT_MENU_TYPE" => "bottom2_" . LANGUAGE_ID,
                    "MENU_CACHE_TYPE" => "N",
                    "MENU_CACHE_TIME" => "3600",
                    "MENU_CACHE_USE_GROUPS" => "Y",
                    "MENU_CACHE_GET_VARS" => array(
                    ),
                    "MAX_LEVEL" => "1",
                    "CHILD_MENU_TYPE" => "left",
                    "USE_EXT" => "N",
                    "DELAY" => "N",
                    "ALLOW_MULTI_SELECT" => "N"
                ),
                    false
                );?>
                <?$APPLICATION->IncludeComponent("bitrix:menu", "bottom", array(
                    "ROOT_MENU_TYPE" => "bottom3_" . LANGUAGE_ID,
                    "MENU_CACHE_TYPE" => "N",
                    "MENU_CACHE_TIME" => "3600",
                    "MENU_CACHE_USE_GROUPS" => "Y",
                    "MENU_CACHE_GET_VARS" => array(
                    ),
                    "MAX_LEVEL" => "1",
                    "CHILD_MENU_TYPE" => "left",
                    "USE_EXT" => "N",
                    "DELAY" => "N",
                    "ALLOW_MULTI_SELECT" => "N"
                ),
                    false
                );?>
            </div>
        </div>
        <div class="b-footer_row clearfix">
            <?$APPLICATION->IncludeFile("/local/include_areas/copyright.php", Array(), Array("MODE" => "text"));?>
            <?$APPLICATION->IncludeFile("/local/include_areas/ministr.php", Array(), Array("MODE" => "text"));?>
        </div>
    </div><!-- /.wrapper -->
</footer>
</body>
</html>