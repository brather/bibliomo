<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="ru"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="ru"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="ru"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="ru"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title><?$APPLICATION->ShowTitle()?></title>
    <script src="<?=MARKUP?>js/libs/modernizr.min.js"></script>
    <?$APPLICATION->ShowHead();?>

    <meta name="viewport" content="width=device-width">

    <link rel="icon" href="<?=MARKUP?>favicon.ico" type="image/x-icon" />
    <?$APPLICATION->SetAdditionalCSS(MARKUP.'css/style.css');?>
    <?$APPLICATION->SetAdditionalCSS(MARKUP.'css/low_vision.css');?>
    <?$APPLICATION->AddHeadScript(MARKUP.'js/libs/jquery.min.js');?>
    <?$APPLICATION->AddHeadScript(MARKUP.'js/slick.min.js');?>
    <?$APPLICATION->AddHeadScript(MARKUP.'js/plugins.js');?>
    <?$APPLICATION->AddHeadScript(MARKUP.'js/jquery.cookie.js');?>
    <?$APPLICATION->AddHeadScript(MARKUP.'js/blind.js');?>
    <?$APPLICATION->AddHeadScript(MARKUP.'js/script.js');?>
    <?$APPLICATION->AddHeadScript(MARKUP.'js/jquery.md5.js');?>

    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>

    <script>
        $(function() {
            $('html').addClass('mainhtml'); // это для морды
        }); // DOM loaded
    </script>

</head>
<body class="blind">
<div class="oldie hidden">Вы пользуетесь устаревшей версией браузера. <br/>Не все функции будут работать в нем корректно. <br/>Смените на более новую версию</div>
<div class="homepage">
    <?$APPLICATION->ShowViewContent("library_city_select");?>
    <?$APPLICATION->IncludeFile("include/header_panel.php",array(),array("SHOW_BORDER" => false));?>
    <header id="header" class="mainheader">
        <div class="wrapper clearfix">
            <?
            $APPLICATION->IncludeComponent("bitrix:system.auth.form", "main_special",
                Array(
                    "FORGOT_PASSWORD_URL" => "/auth/",
                    "SHOW_ERRORS" => "N"
                ),
                false
            );
            ?>
            <?$APPLICATION->IncludeFile("/local/include_areas/lang.php", Array(), Array(
                "MODE"      => "text",
            ));?>
            <?/*?>
            <div class="b-headernav-apps right">
                <a href="#" class="b-headerapps_lnk"  title="Приложения для моб. устройств">Приложения для моб. устройств</a>
            </div>
            <?*/?>
            <?$APPLICATION->IncludeComponent(
                "bitrix:menu",
                "top",
                array(
                    "ROOT_MENU_TYPE" => "top_".LANGUAGE_ID,
                    "MENU_CACHE_TYPE" => "A",
                    "MENU_CACHE_TIME" => "3600",
                    "MENU_CACHE_USE_GROUPS" => "N",
                    "MENU_CACHE_GET_VARS" => array(
                    ),
                    "MAX_LEVEL" => "1",
                    "CHILD_MENU_TYPE" => "left_".LANGUAGE_ID,
                    "USE_EXT" => "N",
                    "DELAY" => "N",
                    "ALLOW_MULTI_SELECT" => "N"
                ),
                false
            );?>
            <span class="testmode mBot">Портал работает в тестовом режиме. В случае обнаружения технических проблем или некорректных данных - <a href="/special/feedback/">сообщите нам</a>.</span>
            <span class="testmode no_PTop"> Внимание! Для чтения изданий, ограниченных авторским правом, необходимо установить на компьютере <a target="_blank" href="http://нэб.рф/distribs/NEB_0.0.14.msi">программу просмотра</a>.</span>
        </div>	<!-- /.wrapper -->

    </header>

    <section class="mainsection" >
        <?/*if($APPLICATION->GetCurPage() == '/special/'):*/?>
            <div class="b-portalinfo clearfix wrapper">
                <?$APPLICATION->IncludeFile("/local/include_areas/book_count.php", Array(), Array("MODE" => "text"));?>
                <?$APPLICATION->IncludeFile("/local/include_areas/logo_special.php", Array(), Array("MODE" => "text"));?>
            </div>
        <?/*endif;*/?>

        <?$APPLICATION->IncludeComponent(
            "exalead:search.form",
            "evk_special",
            Array(
                "PAGE" => ($APPLICATION->GetCurPage() == '/special/' ? 'MAIN': $APPLICATION->GetCurPage()),
            )
        );?>

    </section>
    <?
    if(defined('STATIC_PAGE')){
    ?>
    <section class="innersection innerwrapper searchempty clearfix">
        <div class="b-mainblock left">
            <?$APPLICATION->IncludeComponent(
                "bitrix:menu",
                "left",
                array(
                    "ROOT_MENU_TYPE" => "left",
                    "MENU_CACHE_TYPE" => "A",
                    "MENU_CACHE_TIME" => "3600",
                    "MENU_CACHE_USE_GROUPS" => "N",
                    "MENU_CACHE_GET_VARS" => array(
                    ),
                    "MAX_LEVEL" => "1",
                    "CHILD_MENU_TYPE" => "left",
                    "USE_EXT" => "N",
                    "DELAY" => "N",
                    "ALLOW_MULTI_SELECT" => "N"
                ),
                false
            );?>
            <div class="b-plaintext">
                <h2><?$APPLICATION->ShowTitle(false)?></h2>

<?
}
?>