<div class="blindmenu">
    <div class="access wrapper">

        <dl class="a-fontsize">
            <dt>Размер шрифта:</dt>
            <dd><a class="a-fontsize-small" rel="fontsize-small" href="#"></a></dd>
            <dd><a class="a-fontsize-normal" href="#" rel="fontsize-normal"></a></dd>
            <dd><a class="a-fontsize-big" rel="fontsize-big" href="#"></a></dd>
        </dl>
        <dl class="a-colors">
            <dt>Цвета сайта</dt>
            <dd><a class="a-color1" rel="color1" href="#"></a></dd>
            <dd><a class="a-color2" rel="color2" href="#"></a></dd>
            <dd><a class="a-color3" rel="color3" href="#"></a></dd>
        </dl>
        <dl class="a-images">
            <dt>Изображения</dt>
            <dd><a class="a-imagesoff" href="#" rel="imagesoff"></a></dd>
        </dl>
        <p class="a-search"><a href="/special/search/" title="поиск">Поиск</a></p>
        <p class="a-settings"><a href="#">Настройки</a></p>

        <div class="popped">
            <h2>Настройки шрифта:</h2>

            <p class="choose-font-family">Выберите шрифт <a href="#" rel="sans-serif" id="sans-serif" class="font-family">Arial</a> <a href="#" id="serif" rel="serif" class="font-family">Times New Roman</a></p>
            <p class="choose-letter-spacing">Интервал между буквами <span>(Кернинг)</span>: <a href="#" rel="spacing-small" id="spacing-small" class="letter-spacing">Стандартный</a> <a href="#" id="spacing-normal" class="letter-spacing" rel="spacing-normal">Средний</a> <a href="#" id="spacing-big" class="letter-spacing" rel="spacing-big">Большой</a></p>

            <h2>Выбор цветовой схемы:</h2>
            <ul class="choose-colors">
                <li id="color1"><a href="#" rel="color1"><span>&mdash;</span>Черным по белому</a></li>
                <li id="color2"><a href="#" rel="color2"><span>&mdash;</span>Белым по черному</a></li>
                <li id="color3"><a href="#" rel="color3"><span>&mdash;</span>Темно-синим по голубому</a></li>
                <li id="color4"><a href="#" rel="color4"><span>&mdash;</span>Коричневым по бежевому</a></li>
                <li id="color5"><a href="#" rel="color5"><span>&mdash;</span>Зеленым по темно-коричневому</a></li>
            </ul>
            <p class="saveit"><a href="#" class="closepopped"><span>Закрыть панель</span></a> <a href="#" class="default"><span>Вернуть стандартные настройки</span></a></p>
        </div>

    </div>
</div>