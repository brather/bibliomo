$(document).ready(function(){

});

$(window).load(function () {
    sliderInit();
    sliderSmallInit();
});

function sliderInit(){
    var $el = $(".js-slider");

    if($el.length>0){
        $el.bxSlider({
            slideSelector: ".s__item",
            pager: false
        })
    }
}

function sliderSmallInit(){
    var $el = $(".js-slider-small");

    if($el.length>0){
        $el.bxSlider({
            slideSelector: ".slider__small__item",
            controls: false
        })
    }
}