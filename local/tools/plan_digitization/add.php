<?
	define("STOP_STATISTICS", true);
	define("NOT_CHECK_PERMISSIONS", true);

	require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

	$book_id = trim(urldecode($_REQUEST['BOOK_ID']));
	
	if (!$USER->IsAuthorized())
		exit();

	CModule::IncludeModule("highloadblock"); 
	use Bitrix\Highloadblock as HL; 
	use Bitrix\Main\Entity; 
	use Bitrix\Main\Type\DateTime;

    \CModule::IncludeModule('nota.journal');
    use Nota\Journal\J;

	if($_SERVER['REQUEST_METHOD'] == 'POST' and check_bitrix_sessid() and !empty($book_id))
	{
				   
		$obUser = new nebUser();
		if(!$obUser->isLibrary())
			exit();

		$Library = $obUser->getLibrary();
		if(empty($Library['ID']))
			exit();
				   
		$hlblock = HL\HighloadBlockTable::getById(HIBLOCK_PLAN_DIGIT)->fetch();
		$entity = HL\HighloadBlockTable::compileEntity($hlblock); 
		$entity_data_class = $entity->getDataClass(); 

		$rsData = $entity_data_class::getList(array(
			"select" => array("ID"),
			"filter" => array('=UF_LIBRARY' => $Library['ID'], '=UF_EXALEAD_BOOK' => $book_id)
		));

		if($arData = $rsData->Fetch()){
			echo 'ID'. $arData['ID'];
			exit();
		}
			
		$dt = new DateTime();
		$dtPlus = new DateTime();
		$dtPlus->add('+'.PLAN_DIGIT_PERIOD_FINISH.' days');

		$arFields = array(
			'UF_LIBRARY' 		=>$Library['ID'], 
			'UF_EXALEAD_BOOK' 	=>$book_id, 
			'UF_DATE_ADD' 		=>$dt, 
			'UF_DATE_FINISH' 	=>$dtPlus
		);

        J::add('plandigital', 'add', Array('BOOK_ID' => $book_id, 'DATE' => $dtPlus));

		$result = $entity_data_class::add($arFields);
		$ID = $result->getId();

		global $CACHE_MANAGER;
		$CACHE_MANAGER->ClearByTag("hlblock_id" . HIBLOCK_PLAN_DIGIT);
		echo 'ID='.$ID;
	}
	else
	{
		echo 'error';
	}
?>