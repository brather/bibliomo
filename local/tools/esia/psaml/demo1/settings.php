<?php

    $spBaseUrl = 'https://demo.neb.elar.ru'; //or http://<your_domain>

    $settingsInfo = array (
        'sp' => array (
            //'entityId' => $spBaseUrl.'/local/tools/esia/psaml/demo1/metadata.php',
            'entityId' => 'https://demo.neb.elar.ru',
            'assertionConsumerService' => array (
                'url' => $spBaseUrl.'/local/tools/esia/psaml/demo1/index.php?acs',
            ),
            'singleLogoutService' => array (
                'url' => $spBaseUrl.'/local/tools/esia/psaml/demo1/index.php?sls',
            ),
            //'NameIDFormat' => 'urn:oasis:names:tc:SAML:2.0:nameid-format:unspecified',
        ),
        'idp' => array (
            'entityId' => 'https://esia-portal1.test.gosuslugi.ru/idp/shibboleth',
            'singleSignOnService' => array (
                'url' => 'https://esia-portal1.test.gosuslugi.ru/idp/profile/SAML2/Redirect/SSO',
            ),
            'singleLogoutService' => array (
                'url' => '',
            ),
            'x509cert' => 'MIIDZjCCAk6gAwIBAgIVAL+4AgPOS/Zzl1W8pwv4vVX67x/HMA0GCSqGSIb3DQEB BQUAMCkxJzAlBgNVBAMTHmVzaWEtcG9ydGFsMS50ZXN0Lmdvc3VzbHVnaS5ydTAe Fw0xNDAxMjQwNjE5NTZaFw0zNDAxMjQwNjE5NTZaMCkxJzAlBgNVBAMTHmVzaWEt cG9ydGFsMS50ZXN0Lmdvc3VzbHVnaS5ydTCCASIwDQYJKoZIhvcNAQEBBQADggEP ADCCAQoCggEBAIEc1E0yT40pLXPW9umLA31r4GeYdCtCxcoQOxXtHwh3s1FgQbtf L60OxLGNfdrk/9kJYwQtToWiGrDYVI7ugXud9IALwXK4kRzb6z21q+MDxW19qOeG 6GNKEobXvRxAKB6jzvgpa5KKJoOwaFuW4dW5iZ+6Ke7zQNpIAOOgRNFbad9AkMoL ghukND8jsjjVnUPsHdGRca6hYSxycL4zEGoICJsaFi/RN8apXFsv0lM1ApV2kRqt nl9lEHD0zFbLVBSAa6SbBEy5H3ulR3vn+nm3zK6c4KAZJxQHN67oSGTC99vKm1lS DTksq97KEiCWmZBn1KeofOmXMcRNLkzeowcCAwEAAaOBhDCBgTBgBgNVHREEWTBX gh5lc2lhLXBvcnRhbDEudGVzdC5nb3N1c2x1Z2kucnWGNWh0dHBzOi8vZXNpYS1w b3J0YWwxLnRlc3QuZ29zdXNsdWdpLnJ1L2lkcC9zaGliYm9sZXRoMB0GA1UdDgQW BBSZOKUZHjjJGbl3Elf1MHiu+YitgzANBgkqhkiG9w0BAQUFAAOCAQEAFXGNeV7P WiYvhYBDbyJTG9lfiTZxYFJNUdBAtkdmgpGoLkZ8XGEsgTd947y9ZdkmBRzQ08ru zY3l2Khv8Z6TeFOvBweciQm1mbJgoCCwbHbMYwTfCEcZoVz+4Ksqgfvtm5MQ4xhl 5CqtXGp29bsggd3emyafhXwnRvhkSwddKadmjgjlgvfrv5hTMwqUfm8ZvzudkQVN wuBlGCkdLmU05zoPAPHqb7JfAEKSwOjTYTv7UR9Dns3SCobi9VPE+OEC9djbWt7V C3C67J5au1btd+Z7HuNk2+6oMgkAekmDUUmqhFEebknykuNQ1exq7pyWKnd8a54m Z90gBHuH43XAuQ==',
        ),
    );
