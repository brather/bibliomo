<?php

$settings = array (
    // If 'strict' is True, then the PHP Toolkit will reject unsigned
    // or unencrypted messages if it expects them signed or encrypted
    // Also will reject the messages if not strictly follow the SAML
    // standard: Destination, NameId, Conditions ... are validated too.
    'strict' => false,

    // Enable debug mode (to print errors)
    'debug' => true,

    'security' => array(
        'authnRequestsSigned' => true,
        'logoutResponseSigned' => true,
        'logoutRequestSigned' => true
    ),

    // Service Provider Data that we are deploying
    'sp' => array (
        // Identifier of the SP entity  (must be a URI)
        'entityId' => 'https://demo.neb.elar.ru',
        // Specifies info about where and how the <AuthnResponse> message MUST be
        // returned to the requester, in this case our SP.
        'assertionConsumerService' => array (
            // URL Location where the <Response> from the IdP will be returned
            //'url' => 'https://demo.neb.elar.ru/local/tools/esia/psaml/demo1/index.php?acs1',
            'url' => 'https://demo.neb.elar.ru/WebServices/Esia.php?act=login',
            // SAML protocol binding to be used when returning the <Response>
            // message.  Onelogin Toolkit supports for this endpoint the
            // HTTP-Redirect binding only
            'binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
        ),
        // Specifies info about where and how the <Logout Response> message MUST be
        // returned to the requester, in this case our SP.
        'singleLogoutService' => array (
            // URL Location where the <Response> from the IdP will be returned
            'url' => 'https://demo.neb.elar.ru/WebServices/Esia.php?act=logout',
            // SAML protocol binding to be used when returning the <Response>
            // message.  Onelogin Toolkit supports for this endpoint the
            // HTTP-Redirect binding only
            'binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
        ),
        // Specifies constraints on the name identifier to be used to
        // represent the requested subject.
        // Take a look on lib/Saml2/Constants.php to see the NameIdFormat supported
        'NameIDFormat' => 'urn:oasis:names:tc:SAML:2.0:nameid-format:transient',

        // Usually x509cert and privateKey of the SP are provided by files placed at
        // the certs folder. But we can also provide them with the following parameters
        //'x509cert' => '',
        //'privateKey' > '',
    ),

    // Identity Provider Data that we want connect with our SP
    'idp' => array (
        // Identifier of the IdP entity  (must be a URI)
        'entityId' => 'https://esia-portal1.test.gosuslugi.ru/idp/shibboleth',
        // SSO endpoint info of the IdP. (Authentication Request protocol)
        'singleSignOnService' => array (
            // URL Target of the IdP where the SP will send the Authentication Request Message
            'url' => 'https://esia-portal1.test.gosuslugi.ru/idp/profile/SAML2/Redirect/SSO',
            //'url' => 'https://esia-portal1.test.gosuslugi.ru/',
            // SAML protocol binding to be used when returning the <Response>
            // message.  Onelogin Toolkit supports for this endpoint the
            // HTTP-POST binding only
            'binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
        ),
        // SLO endpoint info of the IdP.
        'singleLogoutService' => array (
            // URL Location of the IdP where the SP will send the SLO Request
            'url' => 'https://esia-portal1.test.gosuslugi.ru/idp/profile/SAML2/Redirect/SLO',
            // SAML protocol binding to be used when returning the <Response>
            // message.  Onelogin Toolkit supports for this endpoint the
            // HTTP-Redirect binding only
            'binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
        ),
        // Public x509 certificate of the IdP
        'x509cert' => 'MIIDZjCCAk6gAwIBAgIVAL+4AgPOS/Zzl1W8pwv4vVX67x/HMA0GCSqGSIb3DQEB
BQUAMCkxJzAlBgNVBAMTHmVzaWEtcG9ydGFsMS50ZXN0Lmdvc3VzbHVnaS5ydTAe
Fw0xNDAxMjQwNjE5NTZaFw0zNDAxMjQwNjE5NTZaMCkxJzAlBgNVBAMTHmVzaWEt
cG9ydGFsMS50ZXN0Lmdvc3VzbHVnaS5ydTCCASIwDQYJKoZIhvcNAQEBBQADggEP
ADCCAQoCggEBAIEc1E0yT40pLXPW9umLA31r4GeYdCtCxcoQOxXtHwh3s1FgQbtf
L60OxLGNfdrk/9kJYwQtToWiGrDYVI7ugXud9IALwXK4kRzb6z21q+MDxW19qOeG
6GNKEobXvRxAKB6jzvgpa5KKJoOwaFuW4dW5iZ+6Ke7zQNpIAOOgRNFbad9AkMoL
ghukND8jsjjVnUPsHdGRca6hYSxycL4zEGoICJsaFi/RN8apXFsv0lM1ApV2kRqt
nl9lEHD0zFbLVBSAa6SbBEy5H3ulR3vn+nm3zK6c4KAZJxQHN67oSGTC99vKm1lS
DTksq97KEiCWmZBn1KeofOmXMcRNLkzeowcCAwEAAaOBhDCBgTBgBgNVHREEWTBX
gh5lc2lhLXBvcnRhbDEudGVzdC5nb3N1c2x1Z2kucnWGNWh0dHBzOi8vZXNpYS1w
b3J0YWwxLnRlc3QuZ29zdXNsdWdpLnJ1L2lkcC9zaGliYm9sZXRoMB0GA1UdDgQW
BBSZOKUZHjjJGbl3Elf1MHiu+YitgzANBgkqhkiG9w0BAQUFAAOCAQEAFXGNeV7P
WiYvhYBDbyJTG9lfiTZxYFJNUdBAtkdmgpGoLkZ8XGEsgTd947y9ZdkmBRzQ08ru
zY3l2Khv8Z6TeFOvBweciQm1mbJgoCCwbHbMYwTfCEcZoVz+4Ksqgfvtm5MQ4xhl
5CqtXGp29bsggd3emyafhXwnRvhkSwddKadmjgjlgvfrv5hTMwqUfm8ZvzudkQVN
wuBlGCkdLmU05zoPAPHqb7JfAEKSwOjTYTv7UR9Dns3SCobi9VPE+OEC9djbWt7V
C3C67J5au1btd+Z7HuNk2+6oMgkAekmDUUmqhFEebknykuNQ1exq7pyWKnd8a54m
Z90gBHuH43XAuQ==',
        /*
         *  Instead of use the whole x509cert you can use a fingerprint
         *  (openssl x509 -noout -fingerprint -in "idp.crt" to generate it)
         */
        // 'certFingerprint' => '',
    ),
);
