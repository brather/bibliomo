<?
	/*
	скрипт обновления количества издания, которые выводятся на главной странице в правом углу
	запускать раз в сутки, в 00Ж00
	*/
	define("STOP_STATISTICS", true);
	define("NOT_CHECK_PERMISSIONS", true);

	$_SERVER["DOCUMENT_ROOT"] = str_replace('/local/tools/cron', '', __DIR__);

	require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

	if(!CModule::IncludeModule('nota.exalead'))
		return false;

	use Nota\Exalead\BooksSend;

	$arBooks = BooksSend::getList();
	if(!empty($arBooks))
	{
		foreach($arBooks as $book)
		{
			$arRes = BooksSend::send($book);
			if(!empty($arRes))
				BooksSend::setInfo($book['Id'], $arRes);
		}
	}
?>