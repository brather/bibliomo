<?php
use Bitrix\Main\Loader;
use Evk\Books\Books;
/*
Подготовка данных дял просмотрщика PDF-файлов
*/
define("STOP_STATISTICS", true);
define("NOT_CHECK_PERMISSIONS", true);
$_SERVER["DOCUMENT_ROOT"] = str_replace('/local/tools/cron', '', __DIR__);
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
set_time_limit(0);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if(Loader::includeModule("iblock") && Loader::includeModule("evk.books"))
{
	/*
	$arFilter = array(
		"IBLOCK_TYPE" => "library",
		"IBLOCK_ID" => IBLOCK_ID_BOOKS,
		"!PROPERTY_FILE" => false,
		"PROPERTY_STATUS" => 0,
	);
	if($rsBook = CIBlockElement::GetList(array(), $arFilter, false, false, array("ID", "IBLOCK_ID", "PROPERTY_FILE")))
	{
		$arBooks = array();
		while($arBook = $rsBook->Fetch())
		{
			$arBooks[] = $arBook;
		}
		foreach($arBooks as $arBook)
		{
			CIBlockElement::SetPropertyValuesEx($arBook["ID"], $arBook["IBLOCK_ID"], array("STATUS" => 1));
		}
		foreach($arBooks as $arBook)
		{
			$book = new Books($arBook["PROPERTY_FILE_VALUE"]);
			$book->createFilesForViewer();
			CIBlockElement::SetPropertyValuesEx($arBook["ID"], $arBook["IBLOCK_ID"], array("STATUS" => 2));
		}
		if (defined('BX_COMP_MANAGED_CACHE'))
		{
			\Evk\Books\Cache::ClearCache();
		}
	}
	*/
}
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>