<?php
use Bitrix\Main\Loader;
use Evk\Books\Books;
// Подготовка данных дял просмотрщика PDF-файлов
define("STOP_STATISTICS", true);
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
define("NO_AGENT_CHECK", true);
define("NO_AGENT_STATISTIC", true);
$_SERVER["DOCUMENT_ROOT"] = str_replace('/local/tools/cron', '', __DIR__);
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
set_time_limit(0);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if(!Loader::includeModule("iblock") || !Loader::includeModule("evk.books"))
	return false;

\Evk\Books\UpdateSearch::GetBookAndReIndex();

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>