<?php
use Bitrix\Main\Loader;
use Evk\Books\Books;
/*
Обновление счётчиков для ББК Каталога
*/
define("STOP_STATISTICS", true);
define("NOT_CHECK_PERMISSIONS", true);
$_SERVER["DOCUMENT_ROOT"] = str_replace('/local/tools/cron', '', __DIR__);
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
set_time_limit(0);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
if(Loader::includeModule("iblock") && Loader::includeModule("evk.books"))
{
	$obSection = \Evk\Books\BBKIndexContainer::Create();
	$indexNames = $obSection->GetBBKIndexList();
	foreach($indexNames['arID'] as $id)
	{
		$el = new CIBlockSection();
		if($indexNames['arSection'][$id]['COUNT_BOOKS'] > 0)
		{
			$arFilter = array(
				'UF_COUNT' => intval($indexNames['arSection'][$id]['COUNT_BOOKS']),
			);
			$el->Update($id, $arFilter);
		}
	}
}
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>