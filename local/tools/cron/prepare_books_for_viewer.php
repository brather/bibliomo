<?php
use Bitrix\Main\Loader;
use Evk\Books\Books;

// Подготовка данных дял просмотрщика PDF-файлов

define("STOP_STATISTICS", true);
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
define("NO_AGENT_CHECK", true);
define("NO_AGENT_STATISTIC", true);
$_SERVER["DOCUMENT_ROOT"] = str_replace('/local/tools/cron', '', __DIR__);
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
set_time_limit(0);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if(!Loader::includeModule("iblock") || !Loader::includeModule("evk.books"))
	return false;

$runtime = time();
$checkFile = dirname(__FILE__)."/.check_update";
// проверяем, запущен ли скрипт
if(file_exists($checkFile))
{
	$diffTimeSeconds = $runtime-filemtime($checkFile);
	// если сшишком долго, более 1 часа, то помечаем ошибку для книги и запускаем новый скрипт.
	if($diffTimeSeconds >= 3600)
	{
		$bookID = intval(file_get_contents($checkFile));
		if($bookID > 0)
		{
			$arFilter = array(
				"IBLOCK_TYPE" => "library",
				"IBLOCK_ID" => IBLOCK_ID_BOOKS,
				"!PROPERTY_FILE" => false,
			);
			if($rsBook = CIBlockElement::GetList(array(), $arFilter, false, array("nPageSize"=>1), array(
				"ID",
				"IBLOCK_ID",
			)))
			{
				if($arBook = $rsBook->Fetch())
				{
					$PROPS = array(
						"COUNT_PAGES" => 0,
						"STATUS" => 4, // timeout
					);
					CIBlockElement::SetPropertyValuesEx($arBook["ID"], IBLOCK_ID_BOOKS, $PROPS);
				}
			}
		}
		unlink($checkFile);
	}
	else
	{
		return false;
	}
}



$arFilter = array(
	"IBLOCK_TYPE" => "library",
	"IBLOCK_ID" => IBLOCK_ID_BOOKS,
	"!PROPERTY_FILE" => false,
	array(
		"LOGIC" => "OR",
		array("PROPERTY_STATUS" => 0),
		array("PROPERTY_STATUS" => false),
	),
);
if($rsBook = CIBlockElement::GetList(array(), $arFilter, false, array("nPageSize"=>1), array(
	"ID",
	"IBLOCK_ID",
	"PROPERTY_FILE",
	"PROPERTY_COUNT_PAGES",
)))
{
	touch($checkFile);
	$arBooks = array();
	if($arBook = $rsBook->Fetch())
	{
		file_put_contents($checkFile, $arBook["ID"]);
		CIBlockElement::SetPropertyValuesEx($arBook["ID"], $arBook["IBLOCK_ID"], array("STATUS" => 1));
		if(!isset($arBook["PROPERTY_COUNT_PAGES_VALUE"]) || strlen($arBook["PROPERTY_COUNT_PAGES_VALUE"])==0)
		{
			$f = new CFile();
			$filename = $_SERVER["DOCUMENT_ROOT"].$f->GetFileSRC($f->GetFileArray($arBook["PROPERTY_FILE_VALUE"]));
			if(file_exists($filename) && NotaMediaBooks::checkFIleMeta($filename, $pdfInfo))
			{
				$md5_file = md5_file($filename);
				$PROPS = array(
					"FILE_HASH" => $md5_file,
				);
				if(array_key_exists("Pages", $pdfInfo) && is_numeric($pdfInfo["Pages"]))
				{
					$PROPS["COUNT_PAGES"] = intval($pdfInfo["Pages"]);
					$PROPS["STATUS"] = 0;
				}
				else
				{
					$PROPS["COUNT_PAGES"] = 0;
					$PROPS["STATUS"] = 3;
				}
				$PROPS["FILE_META"] = serialize($pdfInfo);
				CIBlockElement::SetPropertyValuesEx($arBook["ID"], IBLOCK_ID_BOOKS, $PROPS);
			}
			else
			{
				CIBlockElement::SetPropertyValuesEx($arBook["ID"], IBLOCK_ID_BOOKS, array(
					"STATUS" => 3,
					"COUNT_PAGES" => 0,
				));
			}
			unlink($checkFile);
			return false;
		}
		$book = new Books($arBook["PROPERTY_FILE_VALUE"]);
		if($book->createFilesForViewer())
		{
			CIBlockElement::SetPropertyValuesEx($arBook["ID"], $arBook["IBLOCK_ID"], array(
				"STATUS" => 2,
			));
		}
		else
		{
			CIBlockElement::SetPropertyValuesEx($arBook["ID"], $arBook["IBLOCK_ID"], array(
				"STATUS" => 3,
			));
		}
	}
	\Evk\Books\Cache::ClearCache();
	unlink($checkFile);
}

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>