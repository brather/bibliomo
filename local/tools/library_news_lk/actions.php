<?php
define("STOP_STATISTICS", true);
define("NOT_CHECK_PERMISSIONS", true);
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule('iblock');

$ibEl 		= new CIBlockElement();
$backUrl 	= '/profile/news/'; //default action
$action 	= isset($_REQUEST['action']) 	? $_REQUEST['action'] : null;
$newsID 	= isset($_REQUEST['id']) 		? $_REQUEST['id'] : null;
$active 	= isset($_REQUEST['active']) 	? $_REQUEST['active'] : 'Y';
$nebUser 	= new nebUser();
$error 		= false;

if ( isset($_REQUEST['back_url']) && !empty($_REQUEST['back_url']) )
	$backUrl = $_REQUEST['back_url'];

// Проверяем, относится ли новость к библиотеке пользователя
$userLibrary = $nebUser->getLibrary();
if ( !$userLibrary )
	$error = true;
// Теперь находим новость
$rsItem = $ibEl->GetList(
	array(),
	array(
		'IBLOCK_ID' 		=> IBLOCK_ID_LIBRARY_NEWS,
		'ID' 				=> $newsID,
		'PROPERTY_LIBRARY' 	=> $userLibrary['ID']
	), false, false,
	array()
);
if ( $rsItem->SelectedRowsCount() <= 0 )
	$error = true;

// process action
if ( (bool)$action && (bool)$newsID && !$error )
{
	switch ( $action )
	{
		case 'delete':
			$ibEl->Delete($newsID);
			break;
		case 'active':
			$ibEl->Update($newsID, array('ACTIVE' => $active));
			break;
	}
}

if ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' )
	exit(); // Если понадобится какой-то результат - возвращаем
else
	LocalRedirect($backUrl);
?>