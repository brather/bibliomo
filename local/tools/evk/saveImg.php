<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
Bitrix\Main\Loader::includeModule("evk.books");
use Evk\Books\Quotes;
use Evk\Books\Books;
use Evk\Books\Notes;

if (!$GLOBALS["USER"]->IsAuthorized())
	exit();

$quo_id = intval($_REQUEST['quo_id']);
$note_id = intval($_REQUEST['note_id']);

// удаление цитат
if ($quo_id > 0 )
{
	Quotes::deleteWithTest($quo_id);
}
// удаление заметок
if ($note_id > 0 )
{
	Notes::deleteWithTest($note_id);
}

$book_id = intval($_REQUEST['book_id']);
if(empty($book_id))
	exit();

$page = intval($_REQUEST['p']);
$citeTop = intval($_REQUEST['top']);
$citeLeft = intval($_REQUEST['left']);
$citeWidth = intval($_REQUEST['w']);
$citeHeight = intval($_REQUEST['h']);
$width = round(floatval($_REQUEST['w_img']));
$Viewer = new Books(0,0);
$img = $Viewer->CropImageForQuote(
	$width,
	$citeTop,
	$citeLeft,
	$citeWidth,
	$citeHeight
);
if(file_exists($img))
{
	$imageSize = getimagesize($img);
	$imageData = base64_encode(file_get_contents($img));
	$imageHTML = "data:{$imageSize['mime']};base64,{$imageData}";
	if($_REQUEST['type'] != 'note')
	{
		$arFields = array(
			'BOOK_ID' 	=> $book_id,
			'TEXT' 		=> '',
			'IMG_DATA' 	=> $imageHTML,
			'PAGE' 		=> $page,
			'TOP' 		=> $citeTop,
			'LEFT' 		=> $citeLeft,
			'WIDTH' 	=> $citeWidth,
			'HEIGHT' 	=> $citeHeight,
		);
		$id = Quotes::add($arFields);
		$quo = Quotes::getById($id);
		$html = '<div><a p='.$quo["UF_PAGE"].'>
	<span>'.$quo["UF_PAGE"].' страница</span>
	<img src='.$quo['UF_IMG_DATA'].'>
	</a>
	<span class="delete" id='.$quo["ID"].'></span>
		</div>';
	}
	else
	{
		//заметки
		$note = trim($_REQUEST['note']);
		$arFields = array(
			'BOOK_ID' 	=> $book_id,
			'NUM_PAGE' 	=> $page,
			'NOTE' 		=> $note,
			'NOTE_AREA' => '',
			'IMG_DATA' 	=> $imageHTML,
			'TOP' 		=> $citeTop,
			'LEFT' 		=> $citeLeft,
			'WIDTH' 	=> $citeWidth,
			'HEIGHT' 	=> $citeHeight,
		);
		$id = Notes::addFromArray($arFields);
		$note = Notes::getById($id);
		$html = '<div><a p='.$note["UF_NUM_PAGE"].'>
	<span>'.$note["UF_NUM_PAGE"].' страница</span>
	<span>'.$note['UF_TEXT'].'</span>
	</a>
	<span class="delete" id='.$note["ID"].'></span>
		</div>';
	}
	unlink($img);
	echo $html;
}
?>