<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
use Bitrix\Main\Loader;
use Evk\Books\Books;
Loader::includeModule("evk.books");
$BOOK = new Books(0, 0);
$width = (isset($_REQUEST["width"]) && intval($_REQUEST["width"]) > 0) ? intval($_REQUEST["width"]) : ((isset($_REQUEST["w"]) && intval($_REQUEST["w"]) > 0) ? intval($_REQUEST["w"]) : 0);
$height = (isset($_REQUEST["height"]) && intval($_REQUEST["height"]) > 0) ? intval($_REQUEST["height"]) : ((isset($_REQUEST["h"]) && intval($_REQUEST["h"]) > 0) ? intval($_REQUEST["h"]) : 0);
$BOOK->ShowImageResize($width, $height);