<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
use Bitrix\Main\Loader;
use Evk\Books\Books;
Loader::includeModule("evk.books");
if(isset($_REQUEST["FILE_ID"]) && intval($_REQUEST["FILE_ID"]) > 0)
	Books::DownloadPDFByFileID(intval($_REQUEST["FILE_ID"]));
elseif(isset($_REQUEST["BOOK_ID"]) && intval($_REQUEST["BOOK_ID"]) > 0)
	Books::DownloadPDFByBookID(intval($_REQUEST["BOOK_ID"]));