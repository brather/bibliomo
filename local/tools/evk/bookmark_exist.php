<?
define("STOP_STATISTICS", true);
define("NOT_CHECK_PERMISSIONS", true);
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
Bitrix\Main\Loader::includeModule("evk.books");
use Evk\Books\Bookmarks;

if (!$USER->IsAuthorized())
	exit();

$book_id = intval($_REQUEST['book_id']);
if(empty($book_id))
	exit();
$result = Bookmarks::getMark($book_id, $page);
echo $result['ID'];
?>