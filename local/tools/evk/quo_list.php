<?
	define("STOP_STATISTICS", true);
	define("NOT_CHECK_PERMISSIONS", true);
	require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
	Bitrix\Main\Loader::includeModule("evk.books");
	use Evk\Books\Notes;
	use Evk\Books\Bookmarks;
	use Evk\Books\Quotes;

	if (!$USER->IsAuthorized())
		exit();

	$book_id = intval($_REQUEST['book_id']);
	if(empty($book_id))
		exit();

	$arQuo = Quotes::getListBook($book_id);

	header('Content-type: application/json');

	$arResJson = array();
	if(!empty($arQuo))
	{
		foreach($arQuo as $quo)
		{
			$arResJson[] = array(
				'id' 		=> $quo['ID'],
				'citearea' 	=> $quo['UF_TEXT'],
				'citeimg' 	=> $quo['UF_IMG_DATA'],
				'citepage' 	=> $quo['UF_PAGE'],
				'citetop' 	=> $quo['UF_TOP'],
				'citeleft' 	=> $quo['UF_LEFT'],
				'citewidth' => $quo['UF_WIDTH'],
				'citeheight'=> $quo['UF_HEIGHT'],
			);
		}
	}

	// Так же нам нужно добрать массивы заметок и закладок
	$notesResJson 		= Notes::getNotesListJSON($book_id);
	$bookmarksResJson 	= Bookmarks::getBookmarksListJSON($book_id);

	// Инициализируем и наполняем результирующий массив
	// Нужно так же учитывать особенность вёрстки, если один из массивов будет пуст - то с ним произойдёт ошибка
	$arResultData = array(
		'cites' 	=> array(),
		'notes' 	=> array(),
		'bookmarks' => array(),
	);
	if ( (bool)$arResJson && !empty($arResJson) )
		$arResultData['cites'] = $arResJson;
	if ( (bool)$notesResJson && !empty($notesResJson) )
		$arResultData['notes'] = $notesResJson;
	if ( (bool)$bookmarksResJson && !empty($bookmarksResJson) )
		$arResultData['bookmarks'] = $bookmarksResJson;

	echo json_encode($arResultData);
?>