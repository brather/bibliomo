<?
// сохранение страницы 
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
Bitrix\Main\Loader::includeModule("evk.books");
use Evk\Books\Books;
/*
if ($_COOKIE["isAvailable"] != 'open')
{
	echo 'Доступ к изданию запрещен';
	exit();
}
*/
$BOOK = new Books(0,0);
$rwJpg = $BOOK->ShowImage(true, 1200);
header('Accept-Ranges: bytes', true);
header('Content-Disposition: attachment; filename="'.$BOOK->GetBOOK_ID().'_'.$BOOK->GetPAGE_ID().'.png"');
header($_SERVER['SERVER_PROTOCOL'].' 206 Partial Content',true);
echo $rwJpg;
?>