<?
define("STOP_STATISTICS", true);
define("NOT_CHECK_PERMISSIONS", true);
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

Bitrix\Main\Loader::includeModule("evk.books");
use Evk\Books\Bookmarks;

if (!$USER->IsAuthorized())
	exit();
	
$mark_id = intval($_REQUEST['mark_id']);

// удаление закладки
if ($mark_id > 0)
{
	Bookmarks::deleteWithTest($mark_id);
}
else
{
	$book_id = intval($_REQUEST['book_id']);
	if(empty($book_id))
		exit();

	$page = intval($_REQUEST['p']);
	$arFields = array(
		'BOOK_ID' 	=> $book_id,
		'NUM_PAGE' 	=> $page,
		'PREVIEW' 	=> '',
	);
	$id = Bookmarks::addFromArray($arFields);
	echo $id;
}
?>