<?
	define("STOP_STATISTICS", true);
	define("NOT_CHECK_PERMISSIONS", true);
	require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
	$bookID = htmlspecialcharsEx($_REQUEST['BOOK_ID']);
	OrderTable::addUserBook($bookID);
	echo "OK";
	exit;
