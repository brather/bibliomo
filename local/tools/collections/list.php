<?
	define("STOP_STATISTICS", true);
	define("NOT_CHECK_PERMISSIONS", true);

	require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

	if (!$USER->IsAuthorized())
		exit();

	CModule::IncludeModule("evk.books");
	use Evk\Books\Collections;
	use Evk\Books\Books;
	use Evk\Books\IBlock\Element;

	$idObject = intval($_REQUEST['id']);
	$idCollection = intval($_REQUEST['idCollection']);
	$isaddLink = (!empty($_REQUEST['action']) and $_REQUEST['action'] == 'checked');
	$type = htmlspecialcharsbx(trim($_REQUEST['t']));
	$select = (isset($_REQUEST['a']) && $_REQUEST['a'] == "select");
	$multiple = isset($_REQUEST['multiple']);

	if($_SERVER["REQUEST_METHOD"] == "post")
		$select = false;

	if(!empty($idObject) and !empty($idCollection) and !empty($type) and !$select)
	{
		if($type == 'books_collection') # добавление\удаление связи книг скопом из коллекции
		{
			$arBooks = Element::getList(	
				array('IBLOCK_CODE' => IBLOCK_CODE_COLLECTIONS, 'SECTION_ID' => (int)$idObject, '!PROPERTY_BOOK_ID_VALUE' => false), 
				false, 
				array('PROPERTY_BOOK_ID', 'IBLOCK_ID', 'skip_other')
			);
			if(!empty($arBooks['ITEMS']))
			{
				foreach($arBooks['ITEMS'] as $arItem)
				{
					if($isaddLink)
						Collections::addLinkObject($arItem['PROPERTY_BOOK_ID_VALUE'], $idCollection, 'books');
					else
						Collections::removeLinkObject($arItem['PROPERTY_BOOK_ID_VALUE'], $idCollection, 'books');
				}
			}
		}
		else
		{
			if($isaddLink)
			{
				if(!$select)
				{
					if(!$multiple) {
						$delID = Collections::removeLinkObject($idObject, 0, $type);
					}
					if($delID)
						$fd = "r".$delID;
					else
						$fd = "a";
				}

				Collections::addLinkObject($idObject, $idCollection, $type);

				if(!$select)
					echo $idCollection.$fd;
			}
			else
				Collections::removeLinkObject($idObject, $idCollection, $type);
		}
		exit();
	}
	if(!empty($idObject) and !empty($type) and empty($idCollection) and !$select and ($type != 'books') )
	{
		echo Collections::removeLinkObject($idObject, $idCollection, $type)."d";
		exit();
	}
	# если вызвали диалог из объекта книги, значит добавим ее в подборку
	if($type == 'books')
	{
		$saveID = Books::add($idObject);
		if(isset($_REQUEST["get"]) && $_REQUEST["get"] == "id")
		{
			echo $saveID;
			exit;
		}
	}
	elseif($type == 'books_collection') # добавление книг скопом из коллекции
	{
		$arBooks = Element::getList(	
			array('IBLOCK_CODE' => IBLOCK_CODE_COLLECTIONS, 'SECTION_ID' => (int)$idObject, '!PROPERTY_BOOK_ID_VALUE' => false), 
			false, 
			array('PROPERTY_BOOK_ID', 'IBLOCK_ID', 'skip_other')
		);
		if(!empty($arBooks['ITEMS']))
		{
			foreach($arBooks['ITEMS'] as $arItem)
				Books::add($arItem['PROPERTY_BOOK_ID_VALUE']);
		}

		$idObject = false;
	}

	$arCollections = Collections::getListCurrentUser($idObject, $type, $select);

	if($arCollections !== false)
	{
		?>
			<ul class="b-selectionlist">
				<?
				foreach($arCollections as $arData)
				{
					?>
					<li class="checkwrapper">
						<input class="checkbox InCollection" type="checkbox" name="collections[]" value="<?=$arData['ID']?>" id="col<?=$arData['ID']?>"<?=!empty($arData['CHECKED'])? ' checked="checked"' : ''?> ><label for="col<?=$arData['ID']?>" class="black"><?=$arData['UF_NAME']?></label>
					</li>
				<?
				}
				?>
			</ul>
		<?
	}
?>
