<?
define("STOP_STATISTICS", true);
define("NOT_CHECK_PERMISSIONS", true);

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

if (!$USER->IsAuthorized()) {
    exit();
}


if (empty($_REQUEST['right_menu_collections']) and !in_array($_REQUEST['right_menu_collections'])) {
    exit();
}

$arRmCollectionID = $_REQUEST['right_menu_collections'];

CModule::IncludeModule("nota.userdata");
use Nota\UserData\Collections;

$collections = new Collections();

$delete = false;
foreach ($arRmCollectionID as $id) {
    if (false !== $collections->delete($id)) {
        $delete = true;
    }
}
if (false === $delete) {
    http_response_code(500);
}