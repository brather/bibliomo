<?
define("STOP_STATISTICS", true);
define("NOT_CHECK_PERMISSIONS", true);

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

if (!$USER->IsAuthorized())
	exit();

CModule::IncludeModule("evk.books");
use Evk\Books\Books;
use Evk\Books\IBlock\Element;

$type = htmlspecialcharsbx(trim($_REQUEST['t']));
$idObject = htmlspecialcharsbx(trim($_REQUEST['id']));

if(empty($idObject))
	exit();

if($type == 'books_collection')
{
	$arBooks = Element::getList(
		array('IBLOCK_CODE' => IBLOCK_CODE_COLLECTIONS, 'SECTION_ID' => (int)$idObject, '!PROPERTY_BOOK_ID_VALUE' => false),
		false,
		array('PROPERTY_BOOK_ID', 'IBLOCK_ID', 'skip_other')
	);
	if(!empty($arBooks['ITEMS']))
	{
		foreach($arBooks['ITEMS'] as $arItem)
			Books::deleteWithTest($arItem['PROPERTY_BOOK_ID_VALUE']);
	}
}
else
{
	echo "Delete Book";
	Books::deleteWithTest($idObject);
}
?>