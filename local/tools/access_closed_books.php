<?
	define("STOP_STATISTICS", true);
	define("NOT_CHECK_PERMISSIONS", true);

	require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

	CModule::IncludeModule('nota.userdata');
	use Nota\UserData\Rgb; 

	$error = true;
	$result = array();
	$arJson = array();

	$login = trim($_POST['login']);
	$password = trim($_POST['password']);

	if($_SERVER['REQUEST_METHOD'] == 'POST' and !empty($password) and !empty($login) and check_bitrix_sessid())
	{
		/*		
		Делаем по просьбе клиента проверку по мылу и паролю
		*/
		$arUser = array();

		$arFilter = array(
			'=EMAIL' => $login,
			'ACTIVE' => 'Y'
		); 

		$rsUsers = CUser::GetList(($by="id"), ($order="desc"), $arFilter, array('FIELDS'=> array('ID', 'PASSWORD', 'NAME', 'LAST_NAME', 'EMAIL', 'DATE_REGISTER', 'SECOND_NAME'), 'SELECT' => array('UF_SCAN_PASSPORT1', 'UF_RGB_USER_ID', 'UF_REGISTER_TYPE')));
		$arUser = $rsUsers->Fetch();

		if(empty($arUser))
		{
			$arFilter = array(
				'LOGIN_EQUAL' => $login,
				'ACTIVE' => 'Y'
			); 

			$rsUsers = CUser::GetList(($by="id"), ($order="desc"), $arFilter, array('FIELDS'=> array('ID', 'PASSWORD', 'NAME', 'LAST_NAME', 'EMAIL', 'DATE_REGISTER', 'SECOND_NAME'), 'SELECT' => array('UF_SCAN_PASSPORT1', 'UF_RGB_USER_ID', 'UF_REGISTER_TYPE')));
			$arUser = $rsUsers->Fetch();
		}

		if(!empty($arUser))
		{
			/*
			Проверяем валидность пароля
			*/
			if(strlen($arUser["PASSWORD"]) > 32)
			{
				$salt = substr($arUser["PASSWORD"], 0, strlen($arUser["PASSWORD"]) - 32);
				$db_password = substr($arUser["PASSWORD"], -32);
			}
			else
			{
				$salt = "";
				$db_password = $arUser["PASSWORD"];
			}

			$user_password =  md5($salt.$password);
			if($db_password === $user_password)
			{
				/* Если статус не верифицирован, не пускаем пользователя */
				if(!empty($arUser['UF_SCAN_PASSPORT1']) or !empty($arUser['UF_RGB_USER_ID']) or intval($arUser['UF_REGISTER_TYPE']) == 39 or intval($arUser['UF_REGISTER_TYPE']) == 37)
				{
					$USER->Authorize($arUser['ID']);
                    //пользователь на упрощенной регистрации?
                    if(intval($arUser['UF_REGISTER_TYPE']) == 37){
                        $redirect = '/profile/?FULL_REG_REQ=Y';
                    }
					$error = false;
				}
				else
				{
					$error = true;
				}
			}
			else{
				$error = true;
			}


		}


		if(0)
		{
			$res = Rgb::getUserByCredential($login, $password, 'reader_number');
			if(!empty($res['uid']))
			{
				$arFilter = array(
					"ACTIVE" 			=> "Y",
					"UF_RGB_USER_ID" 	=> $res['uid']
				);
				$rsUsers = CUser::GetList(($by="id"), ($order="desc"), $arFilter); 
				$arUser = $rsUsers->Fetch();
				if(!empty($arUser))
				{
					/*				
					Если найденый пользователь не сопостовим с текущим авторизованным, сигналим об ошибке
					*/
					if($USER->IsAuthorized() and $arUser['ID'] != $USER->GetID())
						$error = true;
					else
					{
						$error = false;
						$USER->Authorize($arUser['ID']);
					}
				}
				else
				{
					/*			
					[middlename] => userovi4
					[lastname] => usersov
					[uid] => 696220
					[firstname] => user
					*/
					$user = new CUser;
					$arFields = Array(
						"NAME"              => $res['firstname'],
						"LAST_NAME"         => $res['middlename'],
						"SECOND_NAME"         => $res['lastname'],
						"EMAIL"             => $login."@elar.ru",
						"LOGIN"             => $login,
						"LID"               => "ru",
						"ACTIVE"            => "Y",
						"PASSWORD"          => $password,
						"CONFIRM_PASSWORD"  => $password,
						"UF_RGB_USER_ID"  	=> $res['uid'],

					);

					$ID = $user->Add($arFields);
					if (intval($ID) > 0)
					{
						$error = false;
						$USER->Authorize($ID);
						$nebUser = new nebUser($ID);
						$nebUser->setEICHB();

						$result = $ID; 
					}
					else
						$result = $user->LAST_ERROR;
				}
			}
		}
	}

	$arJson['error'] = $error; 
	$arJson['redirect'] = isset($redirect) ? $redirect : false;
	$arJson['result'] = $result;

	echo json_encode($arJson);
?>