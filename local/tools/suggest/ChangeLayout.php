<?php
header('Content-Type: text/html; charset=utf-8');

require_once ('ReflectionTypeHint.php');
require_once ('Text/LangCorrect.php');
require_once ('UTF8.php');

$corrector = new Text_LangCorrect();
echo $corrector->parse($_GET['word'], $corrector::KEYBOARD_LAYOUT);
?>