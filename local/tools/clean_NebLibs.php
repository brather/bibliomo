<?
	$_SERVER['DOCUMENT_ROOT'] = realpath(__DIR__ . '/../..');
	$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
	require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
	
	define('NO_KEEP_STATISTIC', true);
	define('NOT_CHECK_PERMISSIONS',true);
	define('BX_CRONTAB', true);
	define('BX_NO_ACCELERATOR_RESET', true);
	
	\CModule::IncludeModule("highloadblock");
	\CModule::IncludeModule("nota.exalead");
	use Bitrix\Highloadblock as HL;
	use Bitrix\Main\Entity;
	use Nota\Exalead\NebLibsCityTable as cityTable;
	use Bitrix\NotaExt\Iblock\Element as IB;
	
	$libraryHLBlock = HL\HighloadBlockTable::getById(HIBLOCK_LIBRARY)->fetch();
	$libraryEntity = HL\HighloadBlockTable::compileEntity($libraryHLBlock);
	$libraryEntity = $libraryEntity->getDataClass();
	
	$arNeedLibs = array(199,201,202,207,219);
	$step = 100;
	
	if (!isset($_REQUEST["page"]))
	{
		$_REQUEST["page"] = 0;
	}
	
	$navDelStart = $_REQUEST["page"]*$step + 1;
	$navDelEnd = $_REQUEST["page"]*$step + $step;
	for ($i = $navDelStart; $i <= $navDelEnd; $i++)
	{
		if (in_array($i, $arNeedLibs))
			continue;
		$result = $libraryEntity::Delete($i);
		if(!$result->isSuccess())
		{
			echo $result->getErrorMessages();
		}
		else
		{
			echo 'Элемент с ID='.$i.' успешно удален<br />';
		}
	}
	if ($i < 5880)
	{?>
		<script>
			window.location.href = '?page=<?=$_REQUEST["page"]+1?>';
		</script>
	<?}
	
	
	
?>