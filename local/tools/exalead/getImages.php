<?
	require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
	header('Content-Type: image/jpeg;charset=utf-8');
	CModule::IncludeModule('nota.exalead');
	use Nota\Exalead\RslViewer;
	if ($_COOKIE["isAvailable"] == 'open'){
		$book_id = trim($_REQUEST['book_id']);
		$page = intval($_REQUEST['p']);
		$search = urlencode(str_replace(" ","|",trim($_REQUEST['search'])));
		$Viewer = new RslViewer($book_id);
	
		if (!$search){
			$jpg = $Viewer->getDocumentPage(1000, $page);
		}else{
			$jpg = $Viewer->getDocumentPageSearch(1000, $page, $search);
	
		}
		echo $jpg;
	}
?>