<?php
if ($_SERVER['HTTP_X_REQUESTED_WITH'] != 'XMLHttpRequest') die();

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

$server_ip = \COption::GetOptionString("nota.exalead", "viewer_ip");
$server_port = \COption::GetOptionString("nota.exalead", "viewer_port");

echo file_get_contents("http://$server_ip:$server_port/{$_REQUEST['BOOK_ID']}/document/type");