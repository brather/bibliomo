<?
	require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
	header('Content-Type: image/jpeg;charset=utf-8');
	CModule::IncludeModule('nota.exalead');
	use Nota\Exalead\RslViewer;
	
	$book_id = trim($_REQUEST['book_id']);
	$page = trim($_REQUEST['p']);

	$Viewer = new RslViewer($book_id);
	$jpg = $Viewer->getPageThumbl(100, $page);
	echo $jpg;
?>