<?
	require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
	if ($_COOKIE["isAvailable"] != 'open'){
		echo 'Доступ к изданию запрещен';
		exit();
	}
	
	CModule::IncludeModule('nota.exalead');
	use Nota\Exalead\RslViewer;
	
	$book_id = trim($_REQUEST['book_id']);
	
	$Viewer = new RslViewer($book_id);
	$Viewer -> getFile();
	
?>