<?
// сохранение страницы 
	require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
	
	if ($_COOKIE["isAvailable"] != 'open'){
		echo 'Доступ к изданию запрещен';
		exit();
		}
	CModule::IncludeModule('nota.exalead');
	use Nota\Exalead\RslViewer;
	
	$book_id = trim($_REQUEST['book_id']);
	$page = trim($_REQUEST['p']);
	
	$Viewer = new RslViewer($book_id);
	
	list($width) = explode("x",$Viewer->getGeometry($page));
	
	$rwJpg = $Viewer->getDocumentPage(floor($width), $page);
	header('Accept-Ranges: bytes', true);
	header('Content-Disposition: attachment; filename="'.$book_id.'_'.$page.'.png"'); 
	header($_SERVER['SERVER_PROTOCOL'].' 206 Partial Content',true);
	echo $rwJpg;
	
?>