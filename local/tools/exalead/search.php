<?
	require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

	CModule::IncludeModule('nota.exalead');
	use Nota\Exalead\RslViewer;

	$book_id = trim($_REQUEST['book_id']);
	$search = urlencode(str_replace(" ","|",trim(urldecode($_REQUEST['search_text']))));

	$Viewer = new RslViewer($book_id);
	$result = $Viewer->getPageSearch($search);

	$result = json_decode($result,true);

?>
<?if (count($result[0]['pages'])> 0):?>
<?sort($result[0]['pages']);?>
<?foreach ($result[0]['pages'] as $page):?>

<div><a p='<?=$page?>' ><span><?=$page?></span> страница</a></div>

<?endforeach;?>
<?else:?>
<div>Не найдено ничего.</div>
<?endif;?>