<?
	define("STOP_STATISTICS", true);
	define("NOT_CHECK_PERMISSIONS", true);

	if(isset($_SERVER['HTTP_IF_MODIFIED_SINCE'])){ 
		// if the browser has a cached version of this image, send 304 
		header('Last-Modified: '.$_SERVER['HTTP_IF_MODIFIED_SINCE'],true,304); 
		exit; 
	} 	
	$url = '';
	if(!empty($_REQUEST['url']))
		$url = htmlspecialchars(urldecode($_REQUEST['url']));

	$source = '';
	if(!empty($_REQUEST['source']))
		$source = htmlspecialchars(urldecode($_REQUEST['source']));

	if(empty($source))
		$source = 'JapanConnector';

	@$width = intval($_REQUEST['width']);
	@$height = intval($_REQUEST['height']);
	if(empty($url))
		die('Empty url');

	if($width == 0)
		$width = 124;

	if($height == 0)
		$height = 170;

	/*
	Проверем существование файла, минуя подключение ядра и вызова функций
	*/
	$thumbnailFolder = "/upload/tmp_thumbnail/".substr($url, -3).'/';	
	$fileName = $thumbnailFolder.$url."w".$width."h".$height.".png";
	if(file_exists($_SERVER["DOCUMENT_ROOT"].$fileName)){

		#header('Location: '.$fileName); #тестируем этот вариант
		#exit();

		header('Content-Type: image/png');

		#header("Cache-Control: private, max-age=10800, pre-check=10800"); 
		#header("Pragma: private"); 
		// Set to expire in 2 days 
		#header("Expires: " . date(DATE_RFC822,strtotime(" 2 day"))); 

		echo file_get_contents($_SERVER['DOCUMENT_ROOT'].$fileName);
		exit();
	}

	/*$thumbnailFolder = '/upload/tmp_thumbnail_empty/';
	$fileName = $thumbnailFolder."w".$width."h".$height.".png";
	if(file_exists($_SERVER["DOCUMENT_ROOT"].$fileName)){
	header('Content-Type: image/png');

	header("Cache-Control: private, max-age=10800, pre-check=10800"); 
	header("Pragma: private"); 
	// Set to expire in 2 days 
	header("Expires: " . date(DATE_RFC822,strtotime(" 2 day"))); 

	echo file_get_contents($_SERVER['DOCUMENT_ROOT'].$fileName);
	exit();
	}*/

	require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
	session_write_close(); 

	CModule::IncludeModule('nota.exalead');
	use Nota\Exalead\SearchQuery;
	use Nota\Exalead\RslViewer;

	$resultThumbnail = false;

	/*	
	Получаем картинку из сервиса доступа
	*/
	if($source == 'OUR_SERVICE')
	{
		/*	
		временное решение для показа
		*/

		if(strpos($url, '_') !== false)
		{
			$url = explode('_', $url);
			
			if(strlen($url[2]) < 11)
				$url = '01'.$url[2];
			else
				$url = $url[2];
		}

		$rsl = new RslViewer($url);
		//$rsl->setConnectParams('https://relar.rsl.ru', 443, '99Apaf8YwtAskq5sdfyNMnbD3SDU7qUH', 'app=nXphmdrl1wxKJ2sTlPAGLDqs');
		$tmp = $rsl->getResources();
		#pre($url,1);pre($tmp,1);exit();

		$width_ = ($width < 100? 100 : $width);
		$rawJpg = $rsl->getDocumentPage($width_, 1);

		if(!empty($rawJpg)){
			RewriteFile($_SERVER["DOCUMENT_ROOT"].$fileName, $rawJpg);
			/*		
			конвертируем в PNG
			*/
			$im = new Imagick($_SERVER["DOCUMENT_ROOT"].$fileName);
			$im->setImageFormat('png');
			$im->stripImage(); 
			$im->writeImage($_SERVER["DOCUMENT_ROOT"].$fileName);

			/*			
			Если нужно, то конвертируем
			*/
			if($width_ != $width)
			{
				$arFile = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"].$fileName);
				CFile::ResizeImage($arFile, array('width' => $width, 'height' => $height));
				CopyDirFiles($arFile['tmp_name'], $_SERVER["DOCUMENT_ROOT"].$fileName);
			}

			$resultThumbnail = $fileName;
		}
	}
	else
	{
		$query = new SearchQuery();
		$resultThumbnail = $query->getThumbnail($url, $source, $width, $height);
	}

	header('Content-Type: image/png');

	if($resultThumbnail !== false){
		/*		
		Устанавливаем кешь только если картинка получена
		*/
		#header("Cache-Control: private, max-age=10800, pre-check=10800"); 
		#header("Pragma: private"); 
		// Set to expire in 2 days 
		#header("Expires: " . date(DATE_RFC822,strtotime(" 2 day"))); 

	}else{

		$thumbnailFolder = '/upload/tmp_thumbnail_empty/';
		$fileName = $thumbnailFolder."w".$width."h".$height.".png";

		if(!file_exists($_SERVER['DOCUMENT_ROOT'].$fileName)){
			$resultThumbnail = '/local/images/book_empty.jpg';

			$arFile = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"].$resultThumbnail);

			CFile::ResizeImage($arFile, array('width' => $width, 'height' => $height));

			CopyDirFiles($arFile['tmp_name'], $_SERVER["DOCUMENT_ROOT"].$fileName);
		}

		$resultThumbnail = $fileName;
	}

	if($resultThumbnail !== false){
		echo file_get_contents($_SERVER['DOCUMENT_ROOT'].$resultThumbnail);
	}
?>