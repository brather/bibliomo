<?
define('STATIC_PAGE', true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("For libraries");
?>
    <p>Coming into the NEL-Project, libraries get the following advantages:</p>
    <ul class="b-commonlist">
        <li>a possibility to use the total NEL-fund free of charge, including funds with limited access;</li>
        <li>a possibility to create your own library website free of charge on the basis of a standard library website. A NEL-participant is provided free of charge with a prototype of a website, a cloud source for allocation;</li>
        <li>a possibility to allocate an electronic user and depositary fund in the cloud store free of charge. Access to the user’s fund is performed through the NEL-website or the library website;</li>
        <li>increase of index of attendance of the library by means of automatic accounting of visits of the library website, including accounting of views of the library catalogue and the electronics funds of the library;</li>
        <li>direct access to the Central Catalogue of Libraries of the Russian Federation (CCLRF) and for the goals of search, editing and additions. Editing functions and additions are provided for the agreement with the organization responsible for maintaining of the Central Catalogue of Libraries of the Russian Federation. Changing and additions in the Central Catalogue of Libraries of the Russian Federation are controlled by the organization responsible for maintaining of the Central Catalogue of Libraries of the Russian Federation;</li>
        <li>an interlibrary exchange system for the purpose of enlarging of your own collections on a mutually beneficial basis;</li>
        <li>participating in formation of plans on funds digitalization and getting budget funds for their implementation.</li>
    </ul>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>